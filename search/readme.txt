
           RiSearch Pro

web search engine, version 3.2
(c) Sergej Tarasov, 2000-2004
homepage: http://risearch.org/
Questions send to: risearch@risearch.org




Introduction

RiSearch Pro is a commercial version of search script RiSearch.
This script has all features of free script and several new features.
RiSearch Pro is intended for small and medium sites with 1000-10000
files and total volume about 10-100 Mb. 

Features

1) RiSearch does not use any libraries or database systems, just pure Perl.
   Therefore, it could be used on any server where you have account with CGI. 

2) Script is able to work with different languages. 

3) Simple and convenient query language. 

4) RiSearch uses platform independent binary files which can be used on any system.
   Therefore, it is possible to index local copy of your site on your home computer
   under Windows, and then use produced files on remote server. 

5) Files can be indexed via the local filesystem or via http. 

6) A user-configurable list of stopwords. 

7) Template driven output. 

8) Searching in whole site, one part or several parts of site. 

9) Results sorting by relevancy, file size, date. 

10) Keywords highlighting in output. 

11) Advanced search, with possibility to use logical operators (AND, OR, NOT). 

12) Indexing of other files formats (PDF, DOC, PS) via external parsers. 

13) Incremental indexing.

14) Fuzzy search.


History

Ver. 3.2.08 - 19.01.2005.
    Spider can use ROBOTS.TXT file now.
    Group of pages can be removed.
Ver. 3.2.08 - 21.12.2004.
    Fixed bug in SSI in template files.
    Fixed bug in substring search with results caching.
    Fixed bug with extra spaces in "Show terms".
    "max_files" parameter added to limit number of indexed files.
Ver. 3.2.08 - 06.09.2004.
    Fixed bug in zone search with results caching.
    Several non-critical errors fixed.
    Improved external parsers support.
    Search type can be changed through search form.
Ver. 3.2.07 - 23.08.2004.
    Character translation function for multilanguage sites.
Ver. 3.2.06 - 22.05.2004.
    Fixed bug in terms highlighting code.
    Binary file type added (only URL will be indexed for this filetype).
    Length of the URL can be limited in results page.
Ver. 3.2.06 - 13.05.2004.
    Misspelled keyword correction.
Ver. 3.2.05 - 05.05.2004.
    Highlighting of cached copies of documents.
Ver. 3.2.04 - 28.04.2004.
    Fuzzy search.
    Terms highlighting code was rewritten.
    File content filter added.
Ver. 3.2.03 - 02.02.2004. 
    Incremental indexing via browser. 
Ver. 3.2.03 - 21.01.2004. 
    Substring search. 
Ver. 3.2.02 - 08.12.2003. 
    Several non-critical bugs fixed. 
Ver. 3.2.02 - 21.11.2003. 
    PHP frontend added. 
Ver. 3.2.02 - 13.11.2003. 
    Fixed bug in comparison operations in advanced search. 
    Fixed bug in "add.pl". 
    Fixed bug in spider restarting code. 
Ver. 3.2.02 - 04.11.2003. 
    Fixed bug in "update.pl". 
    Depth of spidering can be limited. 
Ver. 3.2.02 - 30.10.2003. 
    Search results caching. 
Ver. 3.2.01 - 20.10.2003 
    Advanced search (search with attributes). 
    Multiple templates. 
Ver. 3.1.02 - 11.09.2003. 
    Indexing scripts can be stopped and restarted later. 
Ver. 3.1 - 21.08.2003 
    More efficient index format. 
    Reduced memory requirements during indexing. 
    Archives indexing. 
Ver. 3.0.005 - 24.04.2003 
    New commands in filter rules - Index, NoIndex, Follow, NoFollow. 
    Few bugs fixed. 
Ver. 3.0.005 - 10.02.2003 
    Fixed bug in stopwords. 
    Implemented phrase search. 
    Function "Search in results". 
Ver. 3.0 - 23.01.2003 
    Incremental indexing. 
Ver. 2.2 - 13.11.2002 
    Results sorting by word's distance. 
    Keywords highlighting in whole document. 
    Reserve copy of old index can be created during indexing. 
    Default query language is changed - now script will search full word by default. 
Ver. 2.1 - 09.07.2002 
    Possibility to create temporary index for separate site. 
Ver. 2.0 - 30.04.2002 
    Code cleaning. Script now works with 'use strict'. 
    Templates functionality is extended. 
    Fixed bug in spider.pl. 
    Query statistics script is rewritten. 8 kinds of reports is available. 
Ver. 1.0 - 03.11.2001 


For more information please look at documentation.