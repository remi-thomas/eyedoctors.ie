#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


use lib::index_lib;
use lib::common_lib;
use lib::spider_lib;
use riconfig;
use strict;
no warnings;

my %param;
my $query_str = "";
$param{'action'} = "index";
foreach my $arg (@ARGV) {
    $arg =~ m|-(.*?)=(["']?)(.*)\2|;
    $param{$1} = $3;
}



if (exists($ENV{'GATEWAY_INTERFACE'})) {

print "Content-Type: text/html\n";
print "Expires: Mon, 26 Jul 1997 05:00:00 GMT\n";
print "Last-Modified: Mon, 26 Jul 2997 05:00:00 GMT\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";

    print "<PRE>\n";

    if($ENV{'REQUEST_METHOD'} eq 'GET') { 
        $query_str=$ENV{'QUERY_STRING'};
    } elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
        read(STDIN, $query_str, $ENV{'CONTENT_LENGTH'});
        if ($ENV{'QUERY_STRING'} ne "") {
            $query_str .= "&".$ENV{'QUERY_STRING'};
        }
    }

    my @formfields=split /&/,$query_str;
    foreach(@formfields){
        if (/^(.*?)=(.*)/) {$param{$1}=urldecode($2) if $2}
    }

    if (! $param{'passw'}) {
        print qq~
            Please enter password!<BR><BR>
            <FORM ACTION="spider.pl?$query_str" METHOD="POST">
            <INPUT TYPE="password" NAME="passw" SIZE="30">
            <INPUT TYPE="Submit" VALUE="Submit!">
            </FORM>
        ~;
        exit;
    } else {
        if (! check_password($param{'passw'})) {
            print "Wrong password\n";
            exit;
        }
    }

}


if (exists $param{'rules'}) {
    open RULES, $param{'rules'};
    local $/;
    $param{'rules'} = <RULES>;
    close(RULES);
}
my $rules = $param{'rules'}?$param{'rules'}:$cfg{'rules'};
my $login = $param{'login'}?$param{'login'}:$cfg{'login'};
my $password = $param{'password'}?$param{'password'}:$cfg{'password'};


my %to_visit = ();
if (exists $param{'url'}) {
    $to_visit{$param{'url'}} = 1;
} elsif (exists $param{'list'}) {
    open LIST, $param{'list'};
    foreach my $url (<LIST>) {
        chomp($url);
        $to_visit{$url} = 1;
    }
    close(LIST);
} else {
    foreach my $url (@{$cfg{start_url}}) {
        $to_visit{$url} = 1;
    }
}


unless ( -e "db" && -d "db" ) {
    mkdir("db",0777) or &my_die("Died: could not create directory 'db' - $!");
    print "Directory 'db' created\n";
}


my $time1 = time;
my @time=localtime($time1);
my $time="$time[2]:$time[1]:$time[0]";
print "Scan started: $time\n";
$cfg{start_time} = $time1;

start(%param);

start_spider(\%to_visit,\&parse_file, $rules, $login, $password, $param{'action'});

my $time2 = time;
@time=localtime($time2);
$time="$time[2]:$time[1]:$time[0]";
print "Scan finished: $time\n";
print "Creating databases. Please wait, this can take several minuts.\n";


my $time3 = time;
@time=localtime($time3);
$time="$time[2]:$time[1]:$time[0]";
print "Building HASH - $time\n";


finish(%param);

my $time4 = time;
@time=localtime($time4);
$time="$time[2]:$time[1]:$time[0]";
print "\nIndexing finished: $time\n";

@time=gmtime($time2-$time1);
$time="$time[2]:$time[1]:$time[0]";
print "\nScaning took $time sec.\n";

@time=gmtime($time3-$time2);
$time="$time[2]:$time[1]:$time[0]";
print "Writing words took $time sec.\n";

@time=gmtime($time4-$time3);
$time="$time[2]:$time[1]:$time[0]";
print "Building hash took $time sec.\n";

@time=gmtime($time4-$time1);
$time="$time[2]:$time[1]:$time[0]";
print "Total time: $time sec.\n";

print "\n$cfn files are indexed, $cwn unique words are stored in database\n";

if ( $cfg{stop_script} == 1 && exists($ENV{'GATEWAY_INTERFACE'})) {
    
    my $rnd = rand(100000);
    my $delay = $cfg{restart_delay} * 1000;
    my $dum = qq~
        Script will restart automatically after $cfg{restart_delay} seconds.
        <FORM ACTION="spider.pl?action=restart&rnd=$rnd" METHOD="POST">
        <INPUT TYPE="hidden" NAME="passw" VALUE="$param{passw}">
        <INPUT TYPE="Submit" VALUE="Restart!">
        </FORM>
        <SCRIPT LANGUAGE="JavaScript"><!--
            function formsubmit() {
                document.forms[0].submit()
            }
            window.setTimeout("formsubmit()",$delay)
        --></SCRIPT>
    ~;
    print $dum;
}


if ($cfg{create_substring_index} eq "YES" && $cfg{stop_script} != 1 ) {
    $param{action} = "substring";
    add_substring_index(%param);
}


#=====================================================================
