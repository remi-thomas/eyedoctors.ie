<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'Get the search terms'
dim query, SortOrder, oq
query=Replace(left(Request.QueryString("q"),200)," ","+")
SortOrder=left(Request.QueryString("s"),1)
if len(SortOrder)=0 Then
	SortOrder="R"
end if
oq=Replace(left(Request.QueryString("oq"),200)," ","+")
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|

MetaAbstract      = query & " | Search | Eye doctors"
Metadescription   = "Results for '"& query &"' on  " & DomainName
ArticleURL        = DomainName & "Health-Policy/clinical-programme.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Search Results for '"& query &"' | Eye Doctors &copy;" & Year(Now())
SiteSection       = "search" ' highlight correct menu tab'
SiteSubSection    = "result"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Search"
BreadCrumbArr (0,1) = ""
BreadCrumbArr (1,0) = "<em>" & query & "</em>"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'



dim stype, stpos
stpos=left(Request.QueryString("stpos"),150)
stype=left(Request.QueryString("stype"),150)
stpos = replace(stpos, ",", "")
stpos = trim(stpos)

dim strURL 
strURL = DomainName &"search/search.pl?q=" & query & "&stpos=" & stpos & "&s=" & SortOrder & "&stype=" & stype & "&oq=" &  Server.URLencode(oq)
if fusio() then
	response.write "<P> Fusio IP only orseri" & strURL
end if

dim SearchResults

'open the template'
'dim HttpObj
'Set HttpObj = CreateObject("AspHTTP.Conn")
'HttpObj.Url = URLencode(oq)
'HttpObj.FollowRedirects = true
'HttpObj.RequestMethod = "GET"
'HttpObj.UserAgent = "Mozilla/2.0 (compatible; MSIE 3.0B; Windows NT-Fusio)"
'SearchResults = HttpObj.GetURL
dim objXmlHttp
SET objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
objXmlHttp.open "GET",strURL,False
objXmlHttp.send
SearchResults = objXmlHttp.responseText


SearchResults=Replace(SearchResults,"search.pl","search.asp")
SearchResults=Replace(SearchResults,"RiSearch Pro - please register<img src='http://risearch.org/test2.gif' width=1 height=1>::","")

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
			<%
			 Response.Write SearchResults
			%>

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">

            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>