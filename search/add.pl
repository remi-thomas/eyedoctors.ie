#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


use lib::index_lib;
use lib::common_lib;
use lib::spider_lib;
use lib::admin_lib;
use riconfig;
use strict;
no warnings;

   print "Content-Type: text/html\n\n" if exists $ENV{'GATEWAY_INTERFACE'};


my %to_visit = ();

my %param;
foreach my $arg (@ARGV) {
    $arg =~ m|-(.*?)=(["']?)(.*)\2|;
    $param{$1} = $3;
}
$param{'action'} = "add_page";

my $query;
if($ENV{'REQUEST_METHOD'} eq 'GET') { 
   $query=$ENV{'QUERY_STRING'};
} elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
   read(STDIN, $query, $ENV{'CONTENT_LENGTH'});
}
my @formfields=split /&/,$query;
foreach(@formfields){
    if(/^(url)=(.*)/ && $cfg{'allow_visitor_add_page'}) {$param{$1}=urldecode($2)}
}


if (not defined $param{'type'}) {
    $param{'type'} = "P";
}

if (exists $param{'rules'}) {
    open RULES, $param{'rules'};
    local $/;
    $param{'rules'} = <RULES>;
    close(RULES);
}
my $rules = $param{'rules'}?$param{'rules'}:$cfg{'rules'};
my $login = $param{'login'}?$param{'login'}:$cfg{'login'};
my $password = $param{'password'}?$param{'password'}:$cfg{'password'};


my %templates = read_template($cfg{add_template});
print print_template($templates{"header"}) if exists $ENV{'GATEWAY_INTERFACE'};

if ($param{'url'}) {

    $param{'url'} =~ s/^\s+|\s+$//g;
    if ($param{'url'} !~ m|http://[a-zA-Z0-9/#~:.?+=&%@!\-]+|) {
        print "Wrong URL - $param{'url'}\n";
        print print_template($templates{"empty_query"}) if exists $ENV{'GATEWAY_INTERFACE'};;
        print print_template($templates{"footer"}) if exists $ENV{'GATEWAY_INTERFACE'};;
        exit;    
    }


    start(%param);
    if ($param{'type'} eq "S") {
        $to_visit{$param{'url'}} = 1;
        
        start_spider(\%to_visit,\&update_page, $rules, $login, $password);
    } elsif ($param{'type'} eq "P") {
        index_page($param{'url'},\&update_page, $login, $password);

    }
    finish(%param);

    print print_template($templates{"results_header"}) if exists $ENV{'GATEWAY_INTERFACE'};;
    print print_template($templates{"footer"}) if exists $ENV{'GATEWAY_INTERFACE'};;
    exit;
} 

if ($param{'list'}) {
    if (!open LIST, $param{'list'}) {
        print "Can't open file $param{'list'}: $!\n";
        print print_template($templates{"footer"}) if exists $ENV{'GATEWAY_INTERFACE'};;
        exit;
    }
    my $url;

    start(%param);


    if ($param{'type'} eq "S") {

        foreach my $url (<LIST>) {
            chomp($url);
            $to_visit{$url} = 1;
        }
        $to_visit{$param{'url'}} = 1;
        start_spider(\%to_visit,\&update_page, $rules, $login, $password);

    } elsif ($param{'type'} eq "P") {

        while ($url = <LIST>) {
            chomp($url);
            if ($url !~ m|http://[a-zA-Z0-9/#~:.?+=&%@!\-]+|) {
                print "Incorrect URL: $url\n";
            }
            index_page($url,\&update_page, $login, $password);
        }

    }


    finish(%param);
    close(LIST);

    
    print print_template($templates{"results_header"}) if exists $ENV{'GATEWAY_INTERFACE'};;
    print print_template($templates{"footer"}) if exists $ENV{'GATEWAY_INTERFACE'};;
    exit;
}

    print print_template($templates{"empty_query"}) if exists $ENV{'GATEWAY_INTERFACE'};;
    print print_template($templates{"footer"}) if exists $ENV{'GATEWAY_INTERFACE'};;



#===================================================================





