<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'Get the search terms'
dim query
query=Replace(left(Request.QueryString("q"),200)," ","+")

'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|

MetaAbstract      = query & " | Search | Eye doctors"
Metadescription   = "Results for '"& query &"' on  " & DomainName
ArticleURL        = DomainName & "Health-Policy/clinical-programme.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Search Results for '"& query &"' | Eye Doctors &copy;" & Year(Now())
SiteSection       = "search" ' highlight correct menu tab'
SiteSubSection    = "result"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Search"
BreadCrumbArr (0,1) = ""
BreadCrumbArr (1,0) = "<em>" & query & "</em>"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'


%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->

<script>
  (function() {
    var cx = '004303662319623167863:zpryonm7mmw';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>



</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row GAresults">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

                <gcse:search></gcse:search>


          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">

            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>