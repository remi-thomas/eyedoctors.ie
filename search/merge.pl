#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


use lib::merge_lib;
use lib::index_lib;
use lib::common_lib;
use riconfig;
use strict;
no warnings;

if (exists($ENV{'GATEWAY_INTERFACE'})) {
    print "Content-Type: text/html\n\n";
    print "You are not allowed to run this script";
    die;
}


my $time1 = time;
my @time=localtime($time1);
my $time="$time[2]:$time[1]:$time[0]";
print "Start merging: $time\n";

merge();

my $time2 = time;
@time=localtime($time2);
$time="$time[2]:$time[1]:$time[0]";
print "Finished: $time\n";


@time=gmtime($time2-$time1);
$time="$time[2]:$time[1]:$time[0]";
print "Total time: $time sec.\n";



