#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package riconfig;
use strict;
no warnings;
use Exporter;
use vars qw(@ISA @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(%cfg @zone @zone_name %stop_words %index_words
                  @attr_def @attr_conf @attr_weight);
use vars qw(%cfg @zone @zone_name %stop_words %index_words @attr_def @attr_conf @attr_weight);
%cfg = ();
@zone = ();
@zone_name = ();
%stop_words = ();
%index_words = ();
@attr_def = ();
@attr_conf = ();
@attr_weight = ();
%cfg = (


#===================================================================#
#===================================================================#
#                                                                   #
#                                                                   #
#                    Set variables below                            #
#                                                                   #
#                                                                   #
#===================================================================#
#===================================================================#

# Directory where your html files are located
# In most cases you may use path relative to the location of script
# Or use absolute path
# Type "./" for the current directory
base_dir => "/",

# Base URL of your site
base_url => "http://eyedoctors.fusio.net/",

# Search script name
search_script => "search.pl",

# Highlighting script name
show_script => "show.pl",

# Starting URL (used by spider)
start_url => [qw(
http://eyedoctors.fusio.net/
)],


# Filter rules
rules => q(

Disallow *
Allow http://eyedoctors.fusio.net/*
Disallow NoCase */img/* 
Disallow NoMatch *.asp* *.htm *.html *.txt *.pdf *.doc */ 
Disallow */viewdoc.*
),


# Page fetching delay
spider_delay => 0,

# Maximal spidering depth
max_depth => 20,

# Maximal number of files to index (0 - no limit)
max_files => 0,

# HTTP authorization
login => "",
password => "",

# HTTP proxy ( in the form "http://server.com:port/" or "http://user:password@server.com:port/" )
proxy => "",

# Your site visitors will be able to add page to your search engine
# Only entered page will be indexed, if you need to add entire site,
# use admin panel.
allow_visitor_add_page => 0,

#===================================================================
#
#     All other variables are optional. Script should work fine
#  with default settings.
#     These variables controls the indexing process.
#
#===================================================================

# During indexing process script will print info for every 100th file ("0")
# or for every file ("1")
verbose_output => 1,



# Minimum word length to index
min_length => 3,

# Maximum word length to index. Longer words will be truncated
max_length => 64,

# Maximum document size (bigger files will be truncated)
max_doc_size => 5000000,

# Index or not numbers (set   $numbers = ""   if you don't want to index numbers)
# You may add here other non-letter characters, which you want to index
numbers => '0-9',


# Cut default filenames from URL ("YES" or "NO")
cut_default_filenames => 'YES',
default_filenames => 'index.htm index.html default.htm default.asp',

# Convert URL to lower case ("YES" or "NO")
url_to_lower_case => 'NO',

# Index META tags ("YES" or "NO")
use_META => "YES",

# Index IMG ALT tag ("YES" or "NO")
use_ALT => "NO",

# Index URL ("YES" or "NO")
index_url => "YES",

# Delete hyphen at the end of strings ("YES" or "NO")
del_hyphen => "NO",

# List of stopwords ("YES" or "NO")
use_stop_words => "YES",
stop_words => [ qw(
and any are but can had has have her here him his
how its not our out per she some than that the their them then there
these they was were what you ireland &euro; �
) ],


# List of short words, which should be indexed
index_words => [ qw(
tv u2
) ],



# Write queries to log file ("YES" or "NO")
# Please note, you should create directory "log" by hands
# script will not check, if there exist such directory
# Please edit some parameters in file "stat.pl"
create_log => "YES",


# Date format: "md" - month first, "dm" - day first
date_format => "dm",


#===================================================================
#
#     File parsing configuration.
#
#===================================================================


# list of extensions, where script should not remove HTML tags
non_parse_ext => 'txt',

# External parsers configuration (see file external.htm for more info)
# ext_parser_ext => 'pdf doc ps zip gz',
ext_parser_ext => 'pdf doc rtf',
ext_parser_conf => {
'pdf' => 'd:\\PerlScripts\\PDF2TXT\\pdftotext.exe %file% -',
'doc' => 'c:\\antiword\\antiword.exe %file%',
'gz'  => 'gzip -cd %file%',
'rtf' => 'parser/rtftohtm/win32/rtf2html %file% %out_file% %temp_dir%',
},

# Parsers for these extensions produce HTML code in output
# (HTML tags will be remowed in this case)
ext_parser_ext_html => 'rtf',


# Archives extensions (see file external.htm for more info)
arch_ext => 'zip rar arj',
arch_conf => {
'zip' => 'pkunzip -e -d %file% %temp_dir%',
'rar' => 'rar x -r %file% %temp_dir%',
'arj' => 'arj x -t %file% %temp_dir%',
},

# Binary files extensions (these files will not be indexed, but
# URL will be indexed)
bin_ext => 'ppt xls',


# Parts of documents, which should not be indexed
# Set "YES" and edit, if you want to use this feature
use_selective_indexing => "YES",
no_index_strings => {
    q[<!-- No index start 1 -->] => q[<!-- No index end 1 -->],
    q[<!-- No index start 2 -->] => q[<!-- No index end 2 -->],
    q[<!-- No index start 3 -->] => q[<!-- No index end 3 -->],
    
    
},


#===================================================================
#
#     Relevance control variables
#
#===================================================================


# These variables controls the possible variants of results sorting
# 0 - "NO", 1 - "YES"
allow_sort_by_rating => 1,
allow_sort_by_date   => 1,
allow_sort_by_size   => 1,

# Set the weight for rating calculation
weight_title    => 5,
weight_heading  => 3,
weight_bold     => 2,
weight_italic   => 1,
weight_link     => 3,
weight_keywords => 1,
weight_descr    => 1,

# use word frequency
word_freq => 0,

# use word distance
word_dist => 1,
weight_dist => 5,


#===================================================================
#
#     These variables controls the script output.
#
#===================================================================

# Default search type
# 0 - search by beginning (only with INDEXING_SCHEME => 2)
# 1 - exact word search
# 2 - substring search
# 3 - search all forms (Russian version only)
# 4 - fuzzy search
def_search_type => 1,

# Default search mode
# OR (any word)
# AND (all words)
def_search_mode => "AND",

# Number of results per page
res_num => 10,

# Maximal number of results (0 - no limit)
max_res_found => 0,

# Truncate TITLE of page to 64 characters ("YES" or "NO")
truncate_title => "NO",

# Define length of page description in output
# and use META description ("YES") or first "n" characters of page ("NO")
stored_descr_size => 150,
descr_size => 150,
use_META_descr => "YES",

# Delete these characters from description
del_descr_chars => "",

# URL length limit in output (0 - no limit)
url_length_limit => 0,

# Script will try to read file from disk for highlighting
read_file_from_local_disk => "NO",

# Read only N bytes (0 - no limit)
read_file_from_local_disk_limit => 50000,

# look of highlighted terms is defined by these parameters 
mark_start => "<b style='color:black;background-color:#ffff66'>",
mark_end   => "</b>",

# parsee SSI directives in results template
parse_SSI => 1,

# template files
search_template => "templates/template.htm",
admin_template => "templates/admin.htm",
config_template => "templates/config.htm",
stat_template => "templates/stat_template.htm",
add_template => "templates/add_template.htm",
template_1 => "templates/template_1.htm",


#===================================================================
#
#     Results caching.
#
#===================================================================

enable_cache => "YES",

check_cache => "YES",

# cache will be used if any of these conditions are met 
min_doc_found => 1000,
min_search_time => 0.2,

# delete cache automaticaly
delete_cache => "YES",
delete_cache_delay => 3600,

#===================================================================
#
#     Page caching.
#
#===================================================================

enable_page_cache => 0,

# installed module Compress::Zlib required for this option
compress_cache => 0,

#===================================================================
#
#     Fuzzy search.
#
#===================================================================

# Allow fuzzy search (0 or 1)
allow_fuzzy_search => 1,

# Number or errors allowed in fuzzy search
min_distance => 2,

#===================================================================
#
#     File content filter.
#     Allows to exclude file from indexing by content.
#
#===================================================================

# Use Command-Tag option (0 or 1)
use_command_tag     => 0,

# If use Command-Tag, the START of the command-tag
start_command_tag     => '<!-- RiCom',

# If use Command-Tag, the END of the command-tag
end_command_tag     => 'RiCom -->',

# ONLY pages where "1 OR MORE" of the codes/words
# are found in the Command-Tag WILL BE indexed
# separate each word or number-code by a space
index_command_tag     => 'news data article',

# pages where "1 OR MORE" of the here-under codes is found
# in the Command-Tag will absolutely NOT BE INDEXED
# separate each word or number-code by a space
noindex_command_tag     => 'private test ssipage',


#===================================================================
#
#   Change below only if you need multilanguage support
#   With default settings script will work with
#   English, Russian (win1251 encoding) and most European languages
#
#===================================================================

# Capital letters
CAP_LETTERS => '\xC0-\xDF\xA8',

# Lower case letters
LOW_LETTERS => '\xE0-\xFF\xB8',


# Translate escape chars (like &Egrave; or &#255;) ("YES" or "NO")
use_esc => "YES",

# If you use Unicode characters in your site in the form
# &#NNNN; (where NNNN>255), uncomment and edit below
# Samples for different languages can be found in file unicode.txt

code2char => {
# 1040 => "�",
# 1041 => "�",
# 1042 => "�",
# 1043 => "�",
},


#===================================================================
#
#    Automatic recoding configuration
#
#===================================================================

recode_by_http_header => {
#    'windows-1251' => \&lib::common_lib::win2koi,
#    'koi8-r' => \&lib::common_lib::koi2win,
},

recode_by_meta_tag => {
#    'windows-1251' => \&lib::common_lib::win2koi,
#    'koi8-r' => \&lib::common_lib::koi2win,
#    'utf-8' => \&lib::common_lib::utf8_to_win1251,
},

recode_by_url => {
#    '/dir1/' => \&lib::common_lib::win2koi,
#    '/dir2/' => \&lib::common_lib::koi2win,
},


#===================================================================
#
#    Characters translation configuration
#
#===================================================================

allow_characters_translation => 1,

# Example of configuration for German language
translation_rules => {

    '�' => 'ae',
    '�' => 'oe',
    '�' => 'ue',
    '�' => 'ss',

},

#===================================================================
#
#   Parameters for index configuration
#   
#===================================================================

# database files
# Path to database files
HASH      => "0_hash",
FINFO     => "0_finfo",
FINFO_IND => "0_finfo_ind",
WORD_IND  => "0_word_ind",
RATING    => "0_rating",
FDATE     => "0_fdate",
FSIZE     => "0_fsize",
ZONE_INFO => "0_zone_info",
DIST      => "0_dist",

HASH_INC      => "0_hash_inc",
WORD_IND_INC  => "0_word_ind_inc",

HASH_URL      => "0_hash_url",
HASH_URL_IND  => "0_hash_url_ind",
URL_DEL       => "0_deleted_url",

PASS          => "0_pass",

# directory for database files
db     => "db",

# new database will be created in "db_new" directory and then 
# old database will be replaced by new
create_new_database => "YES",

# old database will not be deleted after new database is created
delete_old_database => "NO",

# Hash table size
# For big site use larger value
# 1 - Tiny    ~1Mb
# 2 - Medium  ~10Mb
# 3 - Big     ~50Mb
# 4 - Large   >100Mb
site_size => 4,

# Indexing scheme
# Whole word - 1
# Beginning of the word - 2
INDEXING_SCHEME => 1,

# create additional index for substring search automatically ("YES" or "NO")
create_substring_index => "YES",

# 0 - slow indexing, less memory used
# 1 - fast indexing, more memory used
indexing_speed => 1,

# index format (compact index can be used up to 65536 documents)
compact_index => 1,

# memory block size for temporary index
temp_db_size => 10000000,


#===================================================================
#
#   Automatic restart configuration
#   
#===================================================================

# server timeout ( 0 - no time limit )
server_timeout => 0,
# delay between restarts
restart_delay => 10,


);

#===================================================================
#
#   Site zones definition
#   
#===================================================================


# Site zones definition
$zone[1] = 'news';
$zone[2] = 'arch';
$zone[3] = 'docs';


# Site zones names
$zone_name[1] = 'News';
$zone_name[2] = 'Archive';
$zone_name[3] = 'Docs';

#===================================================================
#
#
#   Advanced search configuration
#   These features are disabled by default
#   Please read html_docs/advanced.htm
#
#   
#===================================================================


# Attributes definition (attributes numbers are started from 0)

#$attr_def[0] = [ '<title>' , '</title>' ];
#$attr_def[1] = [ 'DATE' ];
#$attr_def[2] = [ 'SIZE' ];
#$attr_def[3] = [ '<META NAME="Author" CONTENT="', '">' ];
#$attr_def[4] = [ '<META NAME="Subject" CONTENT="', '">' ];
#$attr_def[5] = [ '<META NAME="callnumber" CONTENT="', '">' ];
#$attr_def[6] = [ '<META NAME="date" CONTENT="', '">' ];
#$attr_def[7] = [ '<META NAME="date" CONTENT="', '">' ];


# Attributes configuration
# 1 - attribute will be indexed
# 2 - attribute can be printed in results output
# 4 - integer attribute
# 8 - float attribute
# 16 - date attribute

$attr_conf[0] = 1;
$attr_conf[1] = 4;
$attr_conf[2] = 4;
$attr_conf[3] = 3;
$attr_conf[4] = 3;
$attr_conf[5] = 3;
$attr_conf[6] = 3;
$attr_conf[7] = 16;



# Attributes weight

$attr_weight[0] = 0;
$attr_weight[1] = 0;
$attr_weight[2] = 0;


#===================================================================#
#===================================================================#
#                                                                   #
#                                                                   #
#                --- end of configuration ---                       #
#                                                                   #
#  Please do not edit below this line unless you know what you do   #
#                                                                   #
#                                                                   #
#===================================================================#
#===================================================================#





1;