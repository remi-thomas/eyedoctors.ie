#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


use lib::admin_lib;
use lib::common_lib;
use riconfig;
use strict;
no warnings;



my $query;
my $cgi = exists($ENV{'GATEWAY_INTERFACE'}) ? 1 : 0;

foreach my $arg (@ARGV) {
    $arg =~ m|-(.*?)=(["']?)(.*)\2|;
    $param{$1} = $3;
}


if (exists($ENV{'GATEWAY_INTERFACE'})) {

print "Content-Type: text/html\n";
print "Expires: Mon, 26 Jul 1997 05:00:00 GMT\n";
print "Last-Modified: Mon, 26 Jul 2997 05:00:00 GMT\n";
print "Cache-Control: no-cache\n";
print "Pragma: no-cache\n\n";


    if($ENV{'REQUEST_METHOD'} eq 'GET') { 
        $query=$ENV{'QUERY_STRING'};
    } elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
        read(STDIN, $query, $ENV{'CONTENT_LENGTH'});
        if ($ENV{'QUERY_STRING'} ne "") {
            $query .= "&".$ENV{'QUERY_STRING'};
        }
    }
    
    
    my @formfields=split /&/,$query;
    foreach(@formfields){
        if(/^(.*?)=(.*)/) {$param{$1}=urldecode($2)}
    }

    if ($param{'action'} eq "password") {
        change_password();
        exit;
    }


    if (! $param{'pass'} && scalar(keys %param) ) {
        print qq~
            Please enter password!<BR><BR>
            <FORM ACTION="admin.pl?$query" METHOD="POST">
            <INPUT TYPE="password" NAME="pass" SIZE="30">
            <INPUT TYPE="Submit" VALUE="Submit!">
            </FORM>
        ~;
        exit;
    } else {
        if (! check_password($param{'pass'}) && scalar(keys %param) ) {
            print "Wrong password\n";
            exit;
        }
    }
    
    
    
}


if ($param{'action'}) {
#    print "<PRE>";
    
    if ($param{'action'} eq "config_show") {
        if (check_password($param{'pass'})) {
            config_show();
        } else {
            print "Wrong password\n";
        }
    }
 
    if ($param{'action'} eq "config_update") {
        if (check_password($param{'pass'})) {
            config_update();
        } else {
            print "Wrong password\n";
        }
    }
        
    
    if ($param{'action'} eq "add_page") {
        if (check_password($param{'pass'})) {
            print "<PRE>\n" if $cgi;
            add_page();
            print "</PRE>\n" if $cgi;
        } else {
            print "Wrong password\n";
        }
    }
    
    if ($param{'action'} eq "index") {
        if (check_password($param{'pass'})) {
            print "<PRE>\n" if $cgi;
            reindex();
            print "</PRE>\n" if $cgi;
        } else {
            print "Wrong password\n";
        }
    }
    
    if ($param{'action'} eq "update") {
        if (check_password($param{'pass'})) {
            print "<PRE>\n" if $cgi;
            update();
            print "</PRE>\n" if $cgi;
        } else {
            print "Wrong password\n";
        }
    }
    

    if ($param{'action'} eq "delete_page") {
        if (check_password($param{'pass'})) {
            print "<PRE>\n" if $cgi;
            if ($param{'url'} =~ m|\*$|) {
                del_pages();
            } else {
                del_page();
            }
            print "</PRE>\n" if $cgi;
        } else {
            print "Wrong password\n";
        }
    }
    
    if ($param{'action'} eq "password") {
        change_password();
        exit;
    }
    
    
#    print "</PRE>";
    exit;
}


if (exists($ENV{'GATEWAY_INTERFACE'})) {
    
    my %templates = read_template($cfg{admin_template});
    
    print print_template($templates{"header"});
    
    print print_template($templates{"add_page"});
    print print_template($templates{"reindex"});
    print print_template($templates{"update"});
    print print_template($templates{"delete_page"});
    print print_template($templates{"password"});
    
    
    print print_template($templates{"footer"});

} else {
    print "\nRiSearch Pro admin utility\n";
    print "Usage: perl admin.pl -[key]=[val]\n";
    print "Examples:\n\n";
    print "perl admin.pl -action=update -type=L -pass=password\n";
    print "perl admin.pl -action=delete_page -url=http://www.server.com/index.htm -pass=password\n";
    print "perl admin.pl -action=add_page -url=http://www.server.com/infrx.htm -pass=password\n";
    print "perl admin.pl -action=add_page -file=../html/index.htm -pass=password\n";
}

#===================================================================


