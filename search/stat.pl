#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


use riconfig;
use lib::common_lib;
no warnings;


BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


# Directory, where log files will be located
# You should create this directory by hands
$log_dir = "log";


#======================================================

print "Content-Type: text/html\n\n";

my $day_sec = 60 * 60 * 24;

my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime)[3,4,5,2,1];
my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;

my @month = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');



if($ENV{'REQUEST_METHOD'} eq 'GET') { 
   $query=$ENV{'QUERY_STRING'};
} elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
   read(STDIN, $query, $ENV{'CONTENT_LENGTH'});
}


my $type = undef;
my $num = undef;
my $d = undef;
my $m = undef;
my $y = undef;
my $total = undef;
my $unique = undef;
my $month = undef;

@formfields=split /&/,$query;

$stype = "AND";
foreach(@formfields){
   if(/^type=(.*)/)  {$type=$1}
   if(/^num=(.*)/)   {$num=$1}
   if(/^d=(.*)/)     {$d=$1}
   if(/^m=(.*)/)     {$m=$1}
   if(/^y=(.*)/)     {$y=$1}
}

if (! defined $type) {
    $num = 20;
    $d = 20;
}

if (defined $m) { $month = $month[$m] }


my %query = ();
$query{m} = $m;
$query{d} = $d;
$query{y} = $y;
$query{num} = $num;
$query{type} = $type;
$query{month} = $month;

my %templates = read_template($cfg{stat_template});

print print_template($templates{"header"},%query);

if ($type eq "top" or !defined $type) {
    $res = get_top($num, $d, $m, $y);

    @k = sort { $$res{$b} <=> $$res{$a} } keys %{$res};
    $unique = scalar keys %{$res};

    my %data = ();
    $data{total} = $total;
    $data{unique} = $unique;

    print print_template($templates{"top_header"},%query,%data);
    for $i (0..$num) {
        if ($i > scalar(@k)-1) { last }
        $data{query} = $k[$i];
        $data{number} = $$res{$k[$i]};
        $data{percent} = $data{number}/$unique*100;
        $data{proc} = sprintf ("%.2f", $data{percent});
        $data{len} = int ($$res{$k[$i]} / $$res{$k[0]} * 200);
        print print_template($templates{"top"},%query,%data);
    }
    print print_template($templates{"top_footer"},%query,%data);

}

if ($type eq "num" or !defined $type) {
    $res = get_num($num, $d, $m, $y);

    @k = sort { $$res{$b} <=> $$res{$a} } keys %{$res};
    
    my %data = ();
    $data{total} = $total;

    print print_template($templates{"num_header"},%query,%data);
    foreach $date (reverse sort keys %{$res}) {
        $data{date} = $date;
        $data{number} = $$res{$date};
        $data{percent} = $data{number}/$total*100;
        $data{proc} = sprintf ("%.2f", $data{percent});
        $data{len} = int ($$res{$date} / $$res{$k[0]} * 200);
        print print_template($templates{"num"},%query,%data);
    }
    print print_template($templates{"num_footer"},%query,%data);

}

if ($type eq "ip" or !defined $type) {
    $res = get_ip($num, $d, $m, $y);

    @k = sort { $$res{$b} <=> $$res{$a} } keys %{$res};
    
    my %data = ();
    $data{total} = $total;

    print print_template($templates{"ip_header"},%query,%data);
    foreach $date (reverse sort keys %{$res}) {
        $data{date} = $date;
        $data{number} = $$res{$date};
        $data{percent} = $data{number}/$total*100;
        $data{proc} = sprintf ("%.2f", $data{percent});
        $data{len} = int ($$res{$date} / $$res{$k[0]} * 200);
        print print_template($templates{"ip"},%query,%data);
    }
    print print_template($templates{"ip_footer"},%query,%data);

}


if ($type eq "time" or !defined $type) {
    $res = get_time($num, $d, $m, $y);
    

    my $max = 0;
    foreach (@{$res}) {
        $max = $_ if $_ > $max;
    }
    
    my %data = ();
    $data{total} = $total;

    print print_template($templates{"time_header"},%query,%data);
    for (my $i=0; $i < 24; $i++) {
        my $hh = $i+1;
        $data{'time'} = "$i - $hh";
        $data{date} = $date;
        $data{number} = $$res[$i];
        $data{percent} = $data{number}/$total*100;
        $data{proc} = sprintf ("%.2f", $data{percent});
        $data{len} = int ($$res[$i] / $max * 200);
        print print_template($templates{"time"},%query,%data);
    }
    print print_template($templates{"time_footer"},%query,%data);

}


if ($type eq "last" or !defined $type) {
    @res = get_last($num);

    my %data = ();

    print print_template($templates{"last_header"},%query,%data);
    foreach $query (@res) {
        my ($query,$resnum,$ip,$time) = split /\|/,$query;
        $data{resnum} = $resnum;
        $data{query} = $query;
        print print_template($templates{"last"},%query,%data);
    }
    print print_template($templates{"last_footer"},%query,%data);
}

if ($type eq "all") {
    @res = get_all($num, $d, $m, $y);
    my %data = ();
    print print_template($templates{"all_header"},%query,%data);
    foreach $query (@res) {
        $data{query} = $query;
        print print_template($templates{"all"},%query,%data);
    }
    print print_template($templates{"all_footer"},%query,%data);
}



print print_template($templates{"footer"});


#=====================================================================
#
#    Function: get_last
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_last {
    my $num = shift;
    
    open IN, "$log_dir/$date";
    my @res = <IN>;
    @res = reverse @res;
    if (scalar @res < $num) { return @res[0..$#res] }
    return @res[0..$num-1]
}
    
#=====================================================================
#
#    Function: get_all
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_all {
    my ($num, $d, $m, $y) = @_;
    
    my @res = ();
    my %res = ();
    my @t_files = ();
    my @files = ();
    my $file;
    $total = 0;

    $y = $YEAR if not defined $y;
    my $ttime = time;
    $ttime = $ttime - $day_sec * $num;
    my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime($ttime))[3,4,5,2,1];
    my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;
    
    opendir(dir,"$log_dir");
    @t_files=grep { /\d\d\d\d\.\d\d\.\d\d/ } readdir(dir);
    
    if (defined $num) {
    	foreach $file (@t_files) {
    	    if ($file ge $date) { push @files, $file; }
    	}
    } elsif (defined $d && defined $m) {
        my $filename = sprintf "%04d.%02d.%02d", $y+1900, $m+1, $d;
        @files = ($filename);
    } elsif (defined $m) {
        my $mm = sprintf "%02d", $m+1;
        @files = grep { /\d\d\d\d\.$mm\.\d\d/} @t_files;
    } else {
        @files = ($date);
    }
    
    foreach my $file (@files) {
        open IN, "$log_dir/".$file;
        @res = <IN>;
        @res{ @res } = undef;
        close(IN);
    }
    
    return keys %res;
        
}    
#=====================================================================
#
#    Function: get_num
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_num {
    my ($num, $d, $m, $y) = @_;
    
    my @res = ();
    my %res = ();
    my @t_files = ();
    my @files = ();
    my $file;
    $total = 0;

    my $ttime = time;
    $ttime = $ttime - $day_sec * $num;
    my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime($ttime))[3,4,5,2,1];
    my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;
    
    opendir(dir,"$log_dir");
    @t_files=grep { /\d\d\d\d\.\d\d\.\d\d/ } readdir(dir);
    
    if (defined $num) {
    	foreach $file (@t_files) {
    	    if ($file ge $date) { push @files, $file; }
    	}
    } elsif (defined $m) {
        my $mm = sprintf "%02d", $m+1;
        @files = sort grep { /\d\d\d\d\.$mm\.\d\d/} @t_files;
    }
    
    foreach my $file (@files) {
        open IN, "$log_dir/".$file;
        @res = <IN>;
        $res{ $file } = scalar(@res);
        $total += scalar(@res);
        close(IN);
    }
    
    return  \%res;
        
}    
#=====================================================================
#
#    Function: get_ip
#    Last modified: 14.04.2004 16:06
#
#=====================================================================

sub get_ip {
    my ($num, $d, $m, $y) = @_;
    
    my @res = ();
    my %res = ();
    my @t_files = ();
    my @files = ();
    my $file;
    $total = 0;

    my $ttime = time;
    $ttime = $ttime - $day_sec * $num;
    my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime($ttime))[3,4,5,2,1];
    my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;
    
    opendir(dir,"$log_dir");
    @t_files=grep { /\d\d\d\d\.\d\d\.\d\d/ } readdir(dir);
    
    if (defined $num) {
    	foreach $file (@t_files) {
    	    if ($file ge $date) { push @files, $file; }
    	}
    } elsif (defined $m) {
        my $mm = sprintf "%02d", $m+1;
        @files = sort grep { /\d\d\d\d\.$mm\.\d\d/} @t_files;
    }
    
    my %tip = ();
    foreach my $file (@files) {
        open IN, "$log_dir/".$file;
        @res = <IN>;
        my %ip = ();
        foreach $rec (@res) {
            my ($query,$resnum,$ip,$time) = split /\|/,$query;
            $ip{$ip}++;
            $tip{$ip}++;
        }
        $res{ $file } = scalar keys %ip;
        close(IN);
    }
    $total = scalar keys %tip;
    if ($total == 0) { $total = 1 }
        
    return  \%res;
        
}    
#=====================================================================
#
#    Function: get_time
#    Last modified: 14.04.2004 16:06
#
#=====================================================================

sub get_time {
    my ($num, $d, $m, $y) = @_;
    
    my @res = ();
    my %res = ();
    my @t_files = ();
    my @files = ();
    my $file;
    $total = 0;

    my $ttime = time;
    $ttime = $ttime - $day_sec * $num;
    my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime($ttime))[3,4,5,2,1];
    my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;
    
    opendir(dir,"$log_dir");
    @t_files=grep { /\d\d\d\d\.\d\d\.\d\d/ } readdir(dir);
    
    if (defined $num) {
    	foreach $file (@t_files) {
    	    if ($file ge $date) { push @files, $file; }
    	}
    } elsif (defined $m) {
        my $mm = sprintf "%02d", $m+1;
        @files = sort grep { /\d\d\d\d\.$mm\.\d\d/} @t_files;
    }
    
    my @time = ();
    foreach my $file (@files) {
        open IN, "$log_dir/".$file;
        @res = <IN>;
        foreach $rec (@res) {
            my ($query,$resnum,$ip,$time) = split /\|/,$rec;
            $time = (localtime($time))[2];
            $time[$time]++;
            $total++
        }
        close(IN);
    }
    
    return  \@time;
        
}    
#=====================================================================
#
#    Function: get_top
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_top {
    my ($num, $d, $m, $y) = @_;
    
    my @res = ();
    my %res = ();
    my @t_files = ();
    my @files = ();
    my $file;
    $total = 0;

    my $ttime = time;
    $ttime = $ttime - $day_sec * $d;
    my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime($ttime))[3,4,5,2,1];
    my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;

    
    opendir(dir,"$log_dir");
    @t_files = grep { /\d\d\d\d\.\d\d\.\d\d/ } readdir(dir);
    
    if (defined $d) {
    	foreach $file (@t_files) {
    	    if ($file ge $date) { push @files, $file; }
    	}
    } elsif (defined $m) {
        my $mm = sprintf "%02d", $m+1;
        @files = sort grep { /\d\d\d\d\.$mm\.\d\d/} @t_files;
    }
    
    foreach my $file (@files) {
        open IN, "$log_dir/".$file;
        @res = <IN>;
        $total += scalar(@res);
        foreach $rec (@res) {
            chomp($rec);
            my ($query,$resnum,$ip,$time) = split /\|/,$rec;
            if ($query eq "") {next}
            $query =~ s/\s+/ /g;
            $query =~ s/^\s+//;
            $query =~ s/\s+$//;
            $res{ "$query - $resnum" }++;
        }
        close(IN);
    }
    
    return  \%res;
        
}    
#=====================================================================
