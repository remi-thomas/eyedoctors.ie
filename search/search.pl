#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


use riconfig;
use lib::common_lib;
use lib::search_lib;
use strict;
no warnings;


$| = 1;

print "Content-Type: text/html\n\n" if exists($ENV{'GATEWAY_INTERFACE'});




my $query_str = "";

if (exists $ENV{'REQUEST_METHOD'}) {

    if($ENV{'REQUEST_METHOD'} eq 'GET') { 
       $query_str=$ENV{'QUERY_STRING'};
    } elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
       read(STDIN, $query_str, $ENV{'CONTENT_LENGTH'});
    }

} else {

    foreach my $arg (@ARGV) {
        $arg =~ m|-(.*?)=(["']?)(.*)\2|;
        $query_str .= "$1=$3&";
    }
    chop($query_str);
    
}    

my %query = parse_query($query_str);


my $query_not_empty = 0;
foreach my $key (keys %query) {
    $query_not_empty = 1 if $key =~ /^q\d?$/;
    $query_not_empty = 1 if $key =~ /^a\d+[lgen][teq]$/;
}


my %templates = ();
if (not defined $query{t}) { 
    %templates = read_template($cfg{search_template});
} else {
    %templates = read_template($cfg{"template_".$query{t}});
}



if ($query_not_empty) {
    foreach my $db (keys %{$query{d}}) {
        $query{db} = $db;
        %query = search(%query);
    }
}


    eval { ($query{search_time},undef,undef,undef) = times };
    if ($@) { $query{search_time} = $@ }



print print_template($templates{"header"},%query);

if ($query_not_empty) {
    
    if ($query{rescount}>0) {
        print print_template($templates{"results_header"},%query);
        print_results(%query);
        print print_template($templates{"results_footer"},%query);
    } else {
        print print_template($templates{"no_results"},%query);
    }
} else {
    print print_template($templates{"empty_query"},%query);
}
print print_template($templates{"footer"},%query);



#=====================================================================
#
#    Function: print_results
#    Last modified: 02.09.2004 09:40
#
#=====================================================================

sub print_results {
    
    my %query = @_;
  
    my $strpos; my $key; my $val;
    
    for (my $i=$query{stpos}; $i<$query{stpos}+$cfg{res_num}; $i++) {
        
        if ( $i == scalar(@{$query{res}}) ) {last};
        my $res = ${$query{res}}[$i];
        
        print "<BR>Res_num: $res<BR>" if $cfg{debug_level} == 5;
        
        my %url_info = read_data($query{db},$res);
        $url_info{score}  = ${$query{sorting_data}}[$res];
        $url_info{enc_url} = urlencode($url_info{url});
        $url_info{ksize} = int($url_info{size}/1024+0.5);
        
    	my ($type) = ($url_info{url} =~ m|\.([^.]+)(?:[?#].*)?$|);
    	if ($type !~ m|$cfg{ext_parser_ext_reg}|i || $type =~ m|$cfg{ext_parser_ext_html_reg}|io) {
    	    $url_info{show_terms_link} = "$cfg{show_script}?url=$url_info{enc_url}&words=$query{q}";
    	}

        if ( $cfg{read_file_from_local_disk} ne "YES" || $type =~ m|$cfg{ext_parser_ext_reg}|i || $url_info{url}=~m|/$|) {
            $url_info{description} = &high_light_query($url_info{description},%query);
        } else {
            (my $filename = $url_info{url}) =~ s|$cfg{base_url}|$cfg{base_dir}/|;
            local $/;
            my $text;
            open FILE, $filename;
            if ($cfg{'read_file_from_local_disk_limit'}) {
                read(FILE,$text,$cfg{'read_file_from_local_disk_limit'});
            } else {
                $text = <FILE>;
            }
            if ($cfg{use_selective_indexing} eq "YES") {
                foreach $key (keys %{$cfg{no_index_strings}}) {
                    $val = $cfg{no_index_strings}{$key};
                    $text =~ s/$key.*?$val/ /gs; 
                }
            }
            $text =~ s/<!--.*?-->/ /gs;
            $text =~ s/<[Ss][Cc][Rr][Ii][Pp][Tt].*?<\/[Ss][Cc][Rr][Ii][Pp][Tt]>/ /gs;
            $text =~ s/<[Ss][Tt][Yy][Ll][Ee].*?<\/[Ss][Tt][Yy][Ll][Ee]>/ /gs;
            $text =~ s/<[Tt][Ii][Tt][Ll][Ee]>\s*(.*?)\s*<\/[Tt][Ii][Tt][Ll][Ee]>/ /gs;
            $text =~ s/<[^>]*>/ /gs;
            $text =~ s/(&[a-zA-Z0-9#]*?;)/&esc2char($1)/egs;
            $text =~ s/\s+/ /gs;
            if ($text ne "") {
                $url_info{description} = &high_light_query($text,%query);
            } else {
                $url_info{description} = &high_light_query($url_info{description},%query);
            }

        }
        print print_template($templates{"results"},%url_info,%query);
        
        
    }; 
    
}
#===================================================================

