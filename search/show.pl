#!/usr/bin/perl
#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org


BEGIN {
    use CGI::Carp qw(fatalsToBrowser);
    my $scriptname = $0;
    $scriptname =~ tr|\\|/|;
    my($dir) = $scriptname =~ /(.*\/)/; 
    chdir($dir) if defined $dir && $dir ne '';
}


use LWP::UserAgent;
use HTML::TokeParser;
use URI::URL;
use strict;
use riconfig;
use lib::common_lib;
no warnings;


print "Content-Type: text/html\n\n";




my $query; my $url; my $words;

    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS}.$cfg{numbers};
    my $notlet = "[^$let]";

my $code = "\${\$_[0]} =~ tr/a-zA-Z$cfg{CAP_LETTERS}$cfg{LOW_LETTERS}$cfg{numbers}*?/ /cs;";
my $remove_non_alphabetic = eval "sub { $code }";

$code = "\${\$_[0]} =~ tr/A-Z$cfg{CAP_LETTERS}/a-z$cfg{LOW_LETTERS}/;";
my $to_lower_case = eval "sub { $code }";

$code = "\${\$_[0]} =~ tr/A-Z$cfg{LOW_LETTERS}/a-z$cfg{CAP_LETTERS}/;";
my $to_capital_case = eval "sub { $code }";

$code = "my \$dum = shift; \$dum =~ tr/a-z$cfg{LOW_LETTERS}/A-Z$cfg{CAP_LETTERS}/; return \$dum";
my $to_capital_case_ret = eval "sub { $code }";


my @mark_start = ();
my @mark_end   = ();
$mark_start[0] = '<b style="color:black;background-color:#ffff66">';
$mark_end[0]   = '</b>';
$mark_start[1] = '<b style="color:black;background-color:#a0ffff">';
$mark_end[1]   = '</b>';
$mark_start[2] = '<b style="color:black;background-color:#99ff99">';
$mark_end[2]   = '</b>';
$mark_start[3] = '<b style="color:black;background-color:#ff9999">';
$mark_end[3]   = '</b>';


if($ENV{'REQUEST_METHOD'} eq 'GET') { 
   $query=$ENV{'QUERY_STRING'};
} elsif($ENV{'REQUEST_METHOD'} eq 'POST') {
   read(STDIN, $query, $ENV{'CONTENT_LENGTH'});
}

my @formfields=split /&/,$query;

foreach(@formfields){
   if(/^url=(.*)/)   {$url=$1}
   if(/^words=(.*)/) {$words=$1}
}
$url = urldecode($url);
$words = urldecode($words);
&$remove_non_alphabetic(\$words);
&$to_lower_case(\$words);
$words =~ s/(&.*?;)/&esc2char($1)/eg;



#################################################################
#                                                               #
#    Restrict access to indexed URLs only                       #
#                                                               #
#################################################################

my ($url_num,$chsum) = get_url_num($url);
if ( ! defined $url_num) { print "<B>Access to this page denied</B>"; exit; }


# Or you can use "Filter rules" to filter pages
#{
#    my $rules = create_rule($cfg{'rules'});
#    my ($index,$follow) = &$rules($url);
#    if ( ! $index) { print "<B>Access to this page denied</B>"; exit; }
#}

#################################################################


my @dum = split / /,$words;
my @query = ();
foreach my $dum (@dum) {
    if (exists($stop_words{$dum})) { next }
    if (length($dum) < $cfg{min_length}) { next }
    $dum = trans($dum);
    $dum =~ s|\?|[$let]|g;
    $dum =~ s|\*|[$let]*?|g;
    $query[$#query+1] = $dum;
}

my $data;

if ($cfg{enable_page_cache}) {
    $data->{text} = get_cached_page($url);
}
if (! $cfg{enable_page_cache} ||  $data->{text} eq "") {
    $data = get_page($url,5);
}
$data->{url} = $url;

if ($data->{text} eq "") {
    print "Unable to fetch requested page<BR>";
    print $data->{message};
    exit;
}


if (1) {
    if ($data->{content_type} eq "application/msword") {
        $data->{type} = "doc";
    }
    if ($data->{content_type} eq "application/rtf") {
        $data->{type} = "rtf";
    }
    if ($data->{content_type} eq "application/pdf") {
        $data->{type} = "pdf";
    }
    if (! $data->{type}) {
        if ($data->{filename}) {
    	    ($data->{type}) = ($data->{filename} =~ m|\.([^.]+)$|);
    	} elsif ($data->{url} =~ m|/$|) {
    	    $data->{type} = "html";
    	} else {
    	    my $t_url = $data->{url};
    	    $t_url =~ s|[?#].*||;
    	    ($data->{type}) = ($t_url =~ m|\.([^.]+)$|);
    	}
    }
    $data->{type} = lc $data->{type};
    if ($data->{type} eq "") { $data->{type} = "html" }

    if ($data->{type} =~ m|$cfg{ext_parser_ext_reg}|io) {
        call_parser($data);
    }
}



my $html_text = $data->{text};

if ($html_text ne "") {
    #$html_text = "<BASE HREF=$url>".$html_text;
    if ($html_text =~ m|^(.*?</[Hh][Ee][Aa][Dd]>)(.*)$|s) {
        print $1;
        $html_text = $2;
    }
    my $p = HTML::TokeParser->new( \$html_text );
    while (my $token = $p->get_token) {
        if ($$token[0] eq "T") {
            my $text = $$token[1];
            for (my $i=0; $i<scalar(@query); $i++) {
                my $word = $query[$i];
                my $num = $i % 4;
                $text = &high_light_query($text,$word,$mark_start[$num],$mark_end[$num]);
            }
            print $text;
        }
        if ($$token[0] eq "C" or $$token[0] eq "D") {
            print $$token[1];
        }
        if ($$token[0] eq "S") {
            print $$token[4];
        }
        if ($$token[0] eq "E" or $$token[0] eq "PI") {
            print $$token[2];
        }
    }

}


#=====================================================================
#
#    Function: high_light_query
#    Last modified: 21.12.2004 18:25
#
#=====================================================================

sub high_light_query {
    my ($text,$word,$hl_start,$hl_end) = @_;
    
    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS}.$cfg{numbers};
    my $notlet = "[^$let]";
    
    my $r1 = "";
    my $r2 = "";
    
	if ($cfg{INDEXING_SCHEME} == 1) { $r1 = "$notlet";$r2="$notlet" }
	elsif ($cfg{INDEXING_SCHEME} == 2) { $r1 = "$notlet" }
	if (length($word) < 4) { $r1 = "$notlet";$r2="$notlet" }

	$text =~ s/($r1|^)($word)($r2|$)/$1$hl_start$2$hl_end$3/gs;

    return $text;
}    	

#=====================================================================
#
#    Function: trans
#    Last modified: 13.04.2004 14:19
#
#=====================================================================

sub trans {
    my $word = shift;

    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS};
    
    $word =~ s|([$let])|"[$1".&$to_capital_case_ret($1)."]"|ge;
    return $word;
    
}
#===================================================================

