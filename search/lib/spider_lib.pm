#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::spider_lib;
use lib::common_lib;
use lib::index_lib;
use LWP::UserAgent;
use URI::URL;
use HTML::LinkExtor;
use strict;
use riconfig;
use vars qw(@ISA @EXPORT);
no warnings;


use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(
             &extract_links &start_spider &index_page
             
             );

use vars  qw(
             );




#=====================================================================
#
#    Function: start_spider
#    Last modified: 21.12.2004 16:16
#
#=====================================================================

sub start_spider {
    
    my ($to_visit, $func_ref, $rules, $login, $password, $action) = @_;
    my %to_visit = %{$to_visit};
    
    $rules = create_rule($rules);

    my $get_url; my $hdrs; my $url; my $req; my $ua; my $resp;
    my $data; my $lastmodified; my $BASE;
    my %visited_url;

    my $index; my $follow;
    
    
    if ($action eq "restart") {
        restart(\%to_visit, \%visited_url);
    } else {
        print "Spider started\n";
    }    
    
    $SIG{INT} = sub { print "Ctrl-C pressed\n\n"; $cfg{stop_script} = 1; };

while (1) {
    
    my $cur_depth = 0;
    
    ($get_url,$cur_depth) = each %to_visit;
    
    
    if ( ! defined $get_url ) { last }
    if ($get_url eq "") { next }
    if (exists($visited_url{$get_url})) { next }
    
    ($index,$follow) = &$rules($get_url);
        
    if ( ! $index && ! $follow ) { next }
    
    my $resp = get_page($get_url,30,$login,$password);
    my $BASE = $$resp{'base'};
    print "$BASE\n" if $cfg{'verbose_output'};
    $visited_url{$get_url}++;
    if ( ($BASE ne $get_url)  &&  exists($visited_url{$BASE})) { 
        print "Already visited\n\n";
        delete $to_visit{$get_url};
        next;
    }
    if ($BASE ne $get_url) { $visited_url{$BASE}++; $get_url = $BASE }

    
    if ($$resp{'text'} && $cfg{use_command_tag}) {
        if ( $$resp{'text'} =~ m|$cfg{start_command_tag}\s*(.*?)\s*$cfg{end_command_tag}|i) {
            my $tag = $1;
            if ($tag !~ m|($cfg{index_command_tag})|i) { next }
            if ($tag =~ m|($cfg{noindex_command_tag})|i) { next }
        } else {
            next;
        }
    }

    if ($$resp{'text'}) {
        if ( $follow && $cur_depth < $cfg{max_depth} ) {
            foreach my $new_link (extract_links( $$resp{'text'}, $BASE, $rules )) {
                if (! exists($visited_url{$new_link})) {

                    my ($tindex,$tfollow) = &$rules($new_link);
                    if ( ! $tindex && ! $tfollow ) { next }
                    if (exists $to_visit{$new_link}) {
                        $to_visit{$new_link} = $cur_depth+1 if $to_visit{$new_link} > $cur_depth+1;
                    } else {
                        $to_visit{$new_link} = $cur_depth+1;
                    }
                    
                }
            }
        }
        
        if ($index) {
            $resp->{url} = $get_url;
            &$func_ref( $resp );
        }

    } else {
        print $$resp{'message'},"\n";
    }
    delete $to_visit{$get_url};
    
    scalar keys %to_visit;
    
    
    print "Done\n\n" if $cfg{'verbose_output'};
    
    if ( $cfg{server_timeout} != 0 && time - $cfg{start_time} > $cfg{server_timeout}*0.7 ) {
        $cfg{stop_script} = 1;
    }
    # Stop if "max_files" reached
    if ($cfg{max_files} && $cfg{max_files} < $cfg{cfn}+1) {
        $cfg{stop_script} = 1;
    }
    if ($cfg{stop_script} == 1) {
        save(\%to_visit, \%visited_url);
        last;
    }
    
    sleep($cfg{'spider_delay'});
}

} 
#=====================================================================
#
#    Function: index_page
#    Last modified: 02.09.2004 09:57
#
#=====================================================================

sub index_page {
 
    my ($get_url, $func_ref, $login, $password) = @_;
    
    my $resp = get_page($get_url,30,$login,$password);
    my $BASE = $$resp{'base'};

    if ($$resp{'text'}) {
                
            $resp->{url} = $get_url;
            &$func_ref( $resp );
        
    } else {
        print $$resp{'message'},"\n"; 
    }
   
}
#=====================================================================
#
#    Function: extract_links
#    Last modified: 09.06.2004 16:17
#
#=====================================================================

sub extract_links {
    my $data = shift;
    my $BASE = shift;
    my $rules = shift;
    
    my $linkarray; my $allow_url; 
    my @urls;
   
    
    my $parser = HTML::LinkExtor->new(undef,$BASE);
    $parser->parse($data)->eof;
    my @links = $parser->links;

    foreach $linkarray (@links) {
        my @element = @$linkarray;
        my $el_type = shift @element;
        while (@element) {
            my ($attr_name, $attr_value) = splice(@element, 0, 2);
  
            $attr_value =~ s/#.*?$//;
            if ($cfg{cut_default_filenames} eq 'YES') { $attr_value =~ s|$cfg{default_filenames_reg}|/|io }
            
            if ($el_type eq "a" && $attr_name eq "href") {
            	push(@urls,$attr_value);
            	next;
            }
            if ($el_type eq "frame" && $attr_name eq "src") {
            	push(@urls,$attr_value);
            	next;
            }
            if ($el_type eq "iframe" && $attr_name eq "src") {
            	push(@urls,$attr_value);
            	next;
            }
            if ($el_type eq "area" && $attr_name eq "href") {
            	push(@urls,$attr_value);
            }
        }
    }
    
    return @urls;
    
}

#=====================================================================
#
#    Function: save
#    Last modified: 13.11.2003 09:45
#
#=====================================================================

sub save {
    
    my ($to_visit_ref, $visited_ref) = @_;
    
    print "Script is preparing to save data for restart. Press 'Ctrl-C' again to stop script without saving.\n\n";
    
    my $to_visit_num = scalar keys %{$to_visit_ref};
    my $visited_num = scalar keys %{$visited_ref};
    
    print "$visited_num URLs were indexed, $to_visit_num URLs in queue\n\n";
    
    open OUT, ">restart_URL_visited" or print "Can't create restart file: $!\n";
    binmode(OUT);
    foreach (keys %{$visited_ref}) {
        print OUT "$_\x0A";
    }
    close(OUT);

    open OUT, ">restart_URL_to_visit" or print "Can't create restart file: $!\n";
    binmode(OUT);
    foreach my $url ( keys %{$to_visit_ref}) {
        print OUT "$url - ${$to_visit_ref}{$url}\x0A";
    }
    close(OUT);
    

}
#=====================================================================
#
#    Function: restart
#    Last modified: 02.02.2004 13:23
#
#=====================================================================

sub restart {
    
    my ($to_visit_ref, $visited_ref) = @_;
    
    print "Script is preparing for restart.\n";
    
    open IN, "restart_URL_visited" or print "No list of visited URLs is found - $!\n";
    binmode(IN);
    foreach my $str (<IN>) {
        chomp($str);
        ${$visited_ref}{$str} += 1;
    }
    close(IN);
    unlink("restart_URL_visited");

    open IN, "restart_URL_to_visit" or print "No list of URLs to visit is found - $!\n";
    binmode(IN);
    foreach my $str (<IN>) {
        chomp($str);
        my $url = "";
        my $depth = 1;
        if ($str =~ m|(.*) - (\d+)|) {
            $url = $1;
            $depth = $2;
        } else {
            $url = $str;
        }
        ${$to_visit_ref}{$url} = $depth;
    }
    close(IN);
    unlink("restart_URL_to_visit");

    my $to_visit_num = scalar keys %{$to_visit_ref};
    my $visited_num = scalar keys %{$visited_ref};
    
    print "$visited_num URLs were indexed, $to_visit_num URLs in queue\n\n";

}
#=====================================================================


1;