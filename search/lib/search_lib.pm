#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::search_lib;
use lib::common_lib;
use strict;
use riconfig;
no warnings;


use vars qw(@ISA @EXPORT);


use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(
             &search &parse_query
             &read_data &high_light_query
             &make_links
             
             );


use vars  qw(
             @query $rescount @res @sorting_data $res_num
             $db
             );


my @res_vec = ();
my $resall_vec = "";
my $dum = "";
my @dum = ();
my @dist_data = ();
my %dist_data_phrase = ();
my @attr_data = ();
my %fuzzy_words = ();
my %all_wordforms = ();
    
    
    my $open_db_files_inc = 0;
    my $FH_HASH = undef;
    my $FH_WORD_IND = undef;
    my $FH_HASH_INC = undef;
    my $FH_WORD_IND_INC = undef;
    my $FH_DIST = undef;
    my @FINFO = ();
    my @FINFO_IND = ();


$res_num = $cfg{res_num};


my ($DAY, $MONTH, $YEAR, $HOUR, $MINUTE) = (localtime)[3,4,5,2,1];
my $date = sprintf "%04d.%02d.%02d", $YEAR+1900, $MONTH+1, $DAY;

my $LETTERS = "-a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS}.$cfg{numbers};

my $code = "\${\$_[0]} =~ tr/-+!*?#'\"a-zA-Z$cfg{CAP_LETTERS}$cfg{LOW_LETTERS}$cfg{numbers}/ /cs;";
my $remove_non_alphabetic = eval "sub { $code }";

$code = "\${\$_[0]} =~ tr/A-Z$cfg{CAP_LETTERS}/a-z$cfg{LOW_LETTERS}/;";
my $to_lower_case = eval "sub { $code }";

$code = "\${\$_[0]} =~ tr/A-Z$cfg{LOW_LETTERS}/a-z$cfg{CAP_LETTERS}/;";
my $to_capital_case = eval "sub { $code }";

$code = "my \$dum = shift; \$dum =~ tr/a-z$cfg{LOW_LETTERS}/A-Z$cfg{CAP_LETTERS}/; return \$dum";
my $to_capital_case_ret = eval "sub { $code }";



#=====================================================================
#
#    Function: search
#    Last modified: 04.12.2004 21:07
#
#=====================================================================

sub search {
    
    my %query = @_;

    print "sub search<BR>\n" if $cfg{debug_level} > 1;
   

    $cfg{db} = "." if $cfg{db} eq "";
    $db = "$cfg{db}/db_$query{db}";


    if ($cfg{enable_cache} eq "YES" && $cfg{check_cache} eq "YES" && ! exists $query{cache}) {
        my $filename = hash($query{query_str});
        if (-e "cache/$filename") {
            $query{cache} = $filename;
        }
    }
    
    if (exists $query{cache}) {
        my $ok = 0;
        ($ok,%query) = cache_get(%query);
        print "cache_get: $ok<BR>" if $cfg{debug_level} > 1;
        if ($ok > 0) {
            %query = prepare_query(%query);
            %query = make_links(%query);
            open_db_files($query{db});
            
            $query{rescount} = scalar(@res);
            $query{res} = \@res;
            $query{sorting_data} = \@sorting_data;
            
            return %query;
        } else {
            delete($query{cache});
            delete($query{cache_str});
        }
    }
        

    
    
    
    my $ex_query = 0;

    
    my $old_vec = "";
    if ($query{oq}) {
        my %oquery = parse_query(urldecode($query{oq}));
        %oquery = prepare_query(%oquery);
        %oquery = get_results(%oquery);
        boolean(%oquery);
        dist(%oquery);
        attr_comp(%query);
        $old_vec = $resall_vec;
    }
    
    
    
    my $sep_vec = "";
    foreach my $key (keys %query) { 
        if ($key =~ m|q(\d+)|) {
            my $a = $1;
            my %temp_query = ();
            $temp_query{q} = $query{$key};
            ${$temp_query{a}}{$a}++;
            if (exists $query{"stype$a"}) {
                $temp_query{stype} = $query{"stype$a"};
            }
            %temp_query = prepare_query(%temp_query);
            %temp_query = get_results(%temp_query);
            boolean(%temp_query);

            if ($sep_vec eq "") {
                $sep_vec = $resall_vec;
            } else {
                $sep_vec = $sep_vec & $resall_vec;
            }
            
            $ex_query = 1;

        }
    }

    
    %query = prepare_query(%query);
    if (exists $query{q}) { $ex_query = 1 }
    if ($query{q}) {    
        %query = get_results(%query);
        boolean(%query);
        dist(%query);
    }
    
    
    if ($query{oq}) { $resall_vec = $resall_vec & $old_vec }

    if ($sep_vec ne "") { $resall_vec = $resall_vec & $sep_vec }

    if (! $ex_query) {
        open_db_files($query{db});
        my $total_doc_num = (-s $FINFO_IND[$query{db}])/4;
        open_db_files($query{db});
        for (my $i=0; $i < $total_doc_num; $i++) {
            vec($resall_vec,$i,1) = 1;
        }
    }
    
    attr_comp(%query);

    
    %query = sorting(%query);
    
    if ($cfg{max_res_found} > 0) {
        $#res = scalar(@res) < $cfg{max_res_found}?scalar(@res)-1:$cfg{max_res_found}-1;
    }
    $query{rescount} = scalar(@res);
    
    
    if ( ($query{stpos} == 0) && ($cfg{create_log} eq "YES") && ! exists($query{old}) ) {
        open QUERY, ">>log/$date";
        my $query = $query{query};
        $query =~ s/\|//g;
        print QUERY "$query|$query{rescount}|$ENV{REMOTE_ADDR}|",time,"\n";
        close(QUERY);
    }


    
    %query = cache_put(%query);
    
    %query = make_links(%query);

    $query{res} = \@res;
    $query{sorting_data} = \@sorting_data;
    
    
    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS};
    ($query{ucfirstquery} = $query{query}) =~ s/(^|[^$let])([$let])/"$1".&$to_capital_case_ret($2)/ge;

    
    
    if ($cfg{debug_level} >= 3) {
        foreach my $key (keys %query) {
            print "$key =&gt; $query{$key}<BR>\n";
        }
        print "<BR><BR>";
    }

    
    return %query;

    
}
#=====================================================================
#
#    Function: parse_query
#    Last modified: 03.01.2005 22:37
#
#=====================================================================

sub parse_query {
    
    my $query_str = shift;

    print "sub parse_query<BR>\n" if $cfg{debug_level} >= 1;
   

my %query;

$query_str =~ s|'|%27|g;
$query_str =~ s|"|%22|g;

my @formfields=split /&/,$query_str;

foreach(@formfields){
    if (/^z=(.*)/)   { ${$query{z}}{$1}++ if ($1 != 0); $query{"z$1"} = 1 if ($1); $query{zones_str} .= "&$_"; next }
    if (/^z(\d+)=(.*)/)   { ${$query{z}}{$1}++ if ($1 != 0);  $query{"z$1"} = $2; $query{zones_str} .= "&$_"; next }
    if (/^d=(.*)/)   { ${$query{d}}{$1}++; next }
    if (/^a=(.*)/)   { ${$query{a}}{$1}++; next }
    if (/^dst=(.*)/)   { $cfg{def_search_type} = $1; $query{dst} = $1; next }
    if (/^(q\d+)=(.*)/)   { $query{$1} .= " $2"; next }
    if (/^(.*?)=(.*)/) {$query{$1}=$2 if $2 ne "" && $2 ne "*" }
}

if (scalar keys %{$query{d}} == 0) {
    ${$query{d}}{0} = 1;
}
if (exists $query{cache} && $query{cache} !~ m|^\d+$|) {
    delete $query{cache};
}
if (exists $query{t} && $query{t} !~ m|^\d+$|) {
    delete $query{t};
}

    if (defined $query{z}) {
#        $query{zones_str} = join "", map { "&z=".$_ } keys %{$query{z}};
        $query{zones_names} = join ", ", map { $zone_name[$_] } keys %{$query{z}};
    }


if (defined $query{debug_level}) {
    $cfg{debug_level} = $query{debug_level};
}

%query = parse_date_comp(%query);

$query{query_str} = $query_str;
$query{query_str_enc} = urlencode($query_str);
$query{ndquery} = $query{q};
($query{queryunderscore} = $query{q}) =~ s|\+|_|g;
    


$query_str =~ s|\&stpos=\d+||;
$query_str =~ s|\&s=(\w+)?||;
$query_str =~ s|\&o=R?||;
$query_str =~ s|\&z=(\d+)?||g;
$query_str =~ s|\&z\d+=(\d+)?||g;

$query{short_query_str} = $query_str;


if ($cfg{debug_level} >= 3) {
    foreach my $key (keys %query) {
        print "$key =&gt; $query{$key}<BR>\n";
    }
}

return %query;
    
}
#=====================================================================
#
#    Function: parse_date_comp
#    Last modified: 04.12.2004 21:08
#
#=====================================================================

sub parse_date_comp {

    my %query = @_;

    print "sub parse_date_comp<BR>\n" if $cfg{debug_level} > 1;
   

foreach my $key (keys %query) {
    if ($key =~ m|^y(\d+)(.*)|) {
        print "Parse date query: $key<BR>\n" if $cfg{debug_level} >= 5;
        my $attr = $1;
        my $action = $2;
        my $year = $query{$key};
        my $month = undef;
        my $day = undef;
        my $k1 = "m$attr$action";
        my $k2 = "d$attr$action";
        if (exists $query{$k1}) {
            $month = $query{$k1};
        }
        if (exists $query{$k2}) {
            $day = $query{$k2};
        }
        if (defined $month && defined $day) {
            my $k = "a$attr$action";
            my $v = $cfg{date_format} eq "dm" ? "$day.$month.$year" : "$month.$day.$year";
            $query{$k} = $v;
            next;
        }
        if ($action eq "eq") {
            if (! defined $month) {
                my $k1 = "a$attr"."ge";
                my $v1 = "01.01.$year";
                my $k2 = "a$attr"."le";
                my $v2 = $cfg{date_format} eq "dm" ? "31.12.$year" : "12.31.$year";
                $query{$k1} = $v1;
                $query{$k2} = $v2;
                next;
            } else {
                my $k1 = "a$attr"."ge";
                my $v1 = $cfg{date_format} eq "dm" ? "01.$month.$year" : "$month.01.$year";
                $query{$k1} = $v1;
                my $k2 = "a$attr"."lt";
                my $m2 = $month+1 == 13 ? "01" : $month+1;
                my $y2 = $year;
                if ($m2 eq "01") {
                    $y2 = $year+1 == 100 ? $2 = "00" : $year+1;
                }
                my $v2 = $cfg{date_format} eq "dm" ? "01.$m2.$y2" : "$m2.01.$y2";
                $query{$k2} = $v2;
                next;
            }
        } else {
            if (! defined $month) {
                my $k = "a$attr$action";
                my $v = "";
                if ($action eq "lt" || $action eq "ge") {
                    $v = "01.01.$year";
                } elsif ($action eq "le" || $action eq "gt") {
                    $v = $cfg{date_format} eq "dm" ? "31.12.$year" : "12.31.$year";
                }
                $query{$k} = $v;
                next;
            }  
        }
    }
}

return %query;
    
}
#=====================================================================
#
#    Function: prepare_query
#    Last modified: 04.12.2004 21:10
#
#=====================================================================

sub prepare_query {
    
    my %query = @_;

    print "sub prepare_query<BR>\n" if $cfg{debug_level} > 1;
   

    if (! exists($query{query})) { $query{query} = urldecode($query{q}) }
    if (! exists($query{stype})) { $query{stype} = $cfg{def_search_mode} }
    if (! exists($query{stpos})) { $query{stpos} = 0 }
    if (! exists($query{s})) { $query{s} = "N" }
    $query{ignored_words} = "";
    

    if (defined $query{z} && defined $query{z}{0}) { $query{z} = undef }
    
 



$query{query} =~ s/\s+/ /;
if ($query{query} =~ /\bAND\b|\bOR\b|\bNOT\b/) {
    $query{query} =~ s/\bAND +NOT\b/& ~/g;
    $query{query} =~ s/\bAND\b/&/g;
    $query{query} =~ s/\bNOT\b/& ~/g;
    $query{query} =~ s/\bOR\b/|/g;
    $query{logical} = 1;
    &$to_lower_case(\$query{query});
    $query{query} =~ s/(&[a-zA-Z0-9#]*?;)/&esc2char($1)/eg;
    $query{boolean_query} = $query{query};
} else {
    &$to_lower_case(\$query{query});
    $query{query} =~ s/(&[a-zA-Z0-9#]*?;)/&esc2char($1)/eg;
}


&$remove_non_alphabetic(\$query{query});

@{$query{phrase_arr}} = ();

while ($query{query} =~ m|(["'])(.*?)\1|g) {
    my $phr = $2;
    my @dum = split / /,$phr;
    next if scalar(@dum) < 2;
    $phr = "";
    foreach my $dum (@dum) {
        if (exists($stop_words{$dum}) || (length($dum) < $cfg{min_length} && ! exists $index_words{$dum}) ) {
             next;
        }
        ${$query{phrase_hash}}{$dum} = undef;
        $phr .= " $dum";
    }
    $phr =~ s/^ //;
    push @{$query{phrase_arr}}, $phr if $phr =~ m|\S+\s+\S+|;
}
$query{query} =~ s|['"]| |g;
$query{query} =~ s|^\s+||;
$query{query} =~ s|\s+$||;

@dum = split /\s+/,$query{query};
@{$query{query_arr}} = ();

foreach $dum (@dum) {
    if (exists($stop_words{$dum}) || (length($dum) < $cfg{min_length} && ! exists $index_words{$dum}) ) {
        $query{ignored_words} .= " $dum";
        next;
    }
    if (length($dum) >= $cfg{min_length} || exists $index_words{$dum}) { push @{$query{query_arr}}, $dum }
}

# Search type:
# 0 - search by beginning (only with INDEXING_SCHEME => 2)
# 1 - exact word search
# 2 - substring search
# 3 - search all forms (Russian version only)
# 4 - fuzzy search

for (my $i=0; $i<scalar(@{$query{query_arr}}); $i++) {
    ${$query{wholeword}}[$i] = $cfg{def_search_type};
    if (${$query{query_arr}}[$i] =~ /^[^*]+\*$/ && $cfg{INDEXING_SCHEME} == 2)   { 
        ${$query{wholeword}}[$i] = 0;
        ${$query{query_arr}}[$i] =~s/[\*]//g;
    } elsif (${$query{query_arr}}[$i] =~ /[*?]/) {
        ${$query{wholeword}}[$i] = 2;
    } 
    if (${$query{query_arr}}[$i] =~ /\!$/)   {
        ${$query{wholeword}}[$i] = 1;
        ${$query{query_arr}}[$i] =~s/\!//g;
    } 
    if (${$query{query_arr}}[$i] =~ /#$/)   {
        ${$query{wholeword}}[$i] = 4;
        ${$query{query_arr}}[$i] =~s/\#//g;
    } 
    if ($query{stype} eq "AND")     { ${$query{querymode}}[$i] = 2;} 
    if (${$query{query_arr}}[$i] =~ /^\-/) { ${$query{querymode}}[$i] = 1;} 
    if (${$query{query_arr}}[$i] =~ /^\+/) { ${$query{querymode}}[$i] = 2;} 
    ${$query{query_arr}}[$i] =~s/^[\+\- ]//g;
}

if ($query{stpos} <0) {$query{stpos} = 0};





return %query;

}
#=====================================================================
#
#    Function: get_results
#    Last modified: 04.12.2004 21:53
#
#=====================================================================

sub get_results {
    
    my %query = @_;

    print "sub get_results<BR>\n" if $cfg{debug_level} > 1;
   

   open_db_files($query{db});


   open_db_files_inc();

   @res_vec = ();
   $resall_vec = "";
   @sorting_data = ();
   %dist_data_phrase = ();
   @dist_data = ();
   

my $query = "";
my $j; my $dum1; my $word;
my ($doc, $rat, $doc_all, $rat_all, $dist, $dist_all, $attr, $attr_all);
my (@word_ind, @rating);
my $zone_vec;

if (defined $query{z}) { $zone_vec = get_zone_info(%query) }

my $attr_vec = "";
if (defined $query{a}) {
    foreach(keys %{$query{a}}) {
        vec($attr_vec,$_,1) = 1;
    }
}


my $total_doc_num = (-s $FINFO_IND[$query{db}])/4;

for ($j=0; $j<scalar(@{$query{query_arr}}); $j++) {
    $query = ${$query{query_arr}}[$j];
    

    
    $doc_all = "";
    $rat_all = "";
    $dist_all = "";
    $attr_all = "";
    $res_vec[$j] = "\x00";


    my @q_words;
    $all_wordforms{$query} = ${$query{wholeword}}[$j];

    if ($cfg{'allow_characters_translation'}) {
        @q_words = characters_translation($query);
        foreach my $word (@q_words) {
            $all_wordforms{$word} = ${$query{wholeword}}[$j];
        }
    } else {
        push @q_words, $query;
        $all_wordforms{$query} = ${$query{wholeword}}[$j];
    }
    
    
    foreach my $query ( @q_words ) { 
        
        my $smode = ${$query{wholeword}}[$j];
        print "<BR>Get_data for '$query', mode - $smode\n<BR>" if $cfg{debug_level} == 5;
    
        if (${$query{wholeword}}[$j] == 2) {
            print "Substring search<BR>\n" if $cfg{debug_level} > 3;
            ($doc, $rat, $dist, $attr) = get_data_substr($FH_HASH, $FH_WORD_IND, $query, scalar(@{$query{query_arr}}));
            $doc_all .= $doc;
            $rat_all .= $rat;
            $dist_all .= $dist;
            $attr_all .= $attr;
        } elsif ($cfg{allow_fuzzy_search} && ${$query{wholeword}}[$j] == 4) {
            print "Fuzzy search<BR>\n" if $cfg{debug_level} > 3;
            ($doc, $rat, $dist, $attr) = get_data_fuzzy($FH_HASH, $FH_WORD_IND, $query, scalar(@{$query{query_arr}}));
            $doc_all .= $doc;
            $rat_all .= $rat;
            $dist_all .= $dist;
            $attr_all .= $attr;
        } else {    
            if (defined $FH_HASH) {
                ($doc, $rat, $dist, $attr) = get_data($FH_HASH, $FH_WORD_IND, $query, ${$query{wholeword}}[$j], scalar(@{$query{query_arr}}));
                $doc_all .= $doc;
                $rat_all .= $rat;
                $dist_all .= $dist;
                $attr_all .= $attr;
            }
            if (defined $FH_HASH_INC) {
                ($doc, $rat, $dist, $attr) = get_data($FH_HASH_INC, $FH_WORD_IND_INC, $query, ${$query{wholeword}}[$j], scalar(@{$query{query_arr}}));
                $doc_all .= $doc;
                $rat_all .= $rat;
                $dist_all .= $dist;
                $attr_all .= $attr;
            }
        }

    }
    

    @word_ind = unpack("$cfg{'index_format'}*",$doc_all);
    @rating = unpack("$cfg{sformat_rating}*",$rat_all) if (defined $query{s} && $query{s} eq "R" && $cfg{allow_sort_by_rating});
    
    
    if (scalar(@attr_def) && defined $query{a}) {
        for (my $k=0; $k<=$#word_ind; $k++) {
            my $ddd =  substr($attr_all,$k*$cfg{attr_l},$cfg{attr_l});
            
            
            if ( ($attr_vec & $ddd) gt "\0\0\0") {
                vec($res_vec[$j],$word_ind[$k],1) = 1;
            } 

               
        }
    } else {    

        for (my $k=0; $k<=$#word_ind; $k++) {
            vec($res_vec[$j],$word_ind[$k],1) = 1;
        }

    }




    if (defined $query{z} && defined $zone_vec) { $res_vec[$j] = $res_vec[$j] & $zone_vec }
    ${$query{query_res_count}}[$j] = count_vec($res_vec[$j]);
    $query{query_statistics} .= " $query-${$query{query_res_count}}[$j],\n";

    my $ratio = log(($total_doc_num+1)/(${$query{query_res_count}}[$j]+1))/log($total_doc_num+1);
    if (defined $query{s} && $query{s} eq "R" && $cfg{allow_sort_by_rating}) {
        for (my $k=0; $k<=$#word_ind; $k++) {
            
            if ($cfg{'word_freq'}) {
                $sorting_data[$word_ind[$k]] += int($rating[$k]*$ratio);
            } else {
                $sorting_data[$word_ind[$k]] += $rating[$k];
            }

            if ($cfg{'word_dist'} && scalar(@{$query{query_arr}}) > 1 && ${$query{querymode}}[$j] != 1) {
                $dist_data[$word_ind[$k]] .= substr($dist_all,$k*4,4);
            }
            
        }
    }

    if ($cfg{'word_dist'} && scalar(@{$query{query_arr}}) > 1 && 
        ${$query{querymode}}[$j] != 1 && exists $query{phrase_hash} && exists(${$query{phrase_hash}}{$query})) {

        for (my $k=0; $k<=$#word_ind; $k++) {
            $dist_data_phrase{$query}[$word_ind[$k]] .= substr($dist_all,$k*4,4);
        }

    }

    
    if (${$query{query_res_count}}[$j] == 0) {
        my $word = get_data_correct($FH_HASH,$query);
        if (! exists $query{corrected_query} && $word) {
            $query{corrected_query} = $query{query};
        }
        my $mquery = $query;
        $mquery =~ s|([?*])|\\$1|g;
        $query{corrected_query} =~ s/$mquery/$word/g;
    }


}; 


    vec($resall_vec,0,1) = 0;
    for ($j=0; $j<scalar(@{$query{query_arr}}); $j++) {
        $resall_vec = $resall_vec | $res_vec[$j];
    }
    $query{query_statistics} =~ s/,$//;


    
    return %query;
    	
}
#=====================================================================
#
#    Function: get_results_date
#    Last modified: 04.12.2004 21:10
#
#=====================================================================

sub get_results_date {

    my %query = @_;

    print "sub get_results_date<BR>\n" if $cfg{debug_level} > 1;
   

    my $FILE = my $format = my $format_length = "";
    if ($query{s} eq "D") {
    	$FILE = "$db/$cfg{FDATE}";
    	$format = $cfg{sformat_date};
    	$format_length = $cfg{sformat_date_length};
    } else {
    	$FILE = "$db/$cfg{FSIZE}";
    	$format = $cfg{sformat_size};
    	$format_length = $cfg{sformat_size_length};
    }
    open FILE, "$FILE" or &my_die("Died: could not open $FILE - $!",__LINE__,__FILE__);
    binmode(FILE);
    local $/;
    my $data = <FILE>;
    @sorting_data = unpack("$format*", $data);
    close(FILE);
}
#=====================================================================
#
#    Function: get_data
#    Last modified: 04.12.2004 21:11
#
#=====================================================================

sub get_data {

    my ($HASH, $WORD_IND, $query, $wholeword, $scalar_query) = @_;
    

    print "sub get_data<BR>\n" if $cfg{debug_level} > 1;
   

    my ($substring_length, $hash_value, $dum, $hashwords_pos, $sitewords_pos, $word_ind_pos, $next_word_ind_pos);
    my ($word, $word_ind_length, $sorting_length, $word_l);

    my $doc = "";
    my $rat = "";
    my $dist = "";
    my $attr = "";
    my $doc_all = "";
    my $rat_all = "";
    my $dist_all = "";
    my $attr_all = "";

    if ($cfg{INDEXING_SCHEME} == 1) {
    	$substring_length = length($query)
    } else {
    	$substring_length = 4
    }
    $hash_value = &hash(substr($query,0,$substring_length));
    seek($HASH,$hash_value*4,0);
    read($HASH,$dum,4);
    $hashwords_pos = unpack("N", $dum);
    while ($hashwords_pos) {
    	seek($HASH,$hashwords_pos,0);
        read($HASH,$dum,9);
        ($hashwords_pos, $word_ind_pos, $word_l) = unpack("NNC", $dum);
        read($HASH,$word,$word_l);
        if ( ($wholeword==1) && ($word ne $query) ) {$word = ""};
        if (index($word,$query)>=0){
            while ($word_ind_pos) {
                seek($WORD_IND,$word_ind_pos,0);
                read($WORD_IND,$dum,8);
                $word_ind_pos += 8;
                ($next_word_ind_pos,$word_ind_length) = unpack("NN",$dum);
                read($WORD_IND,$doc,$word_ind_length*$cfg{'index_format_l'});
                $word_ind_pos += $word_ind_length*$cfg{'index_format_l'};

                $sorting_length =  $word_ind_length * $cfg{sformat_rating_length};
                if ($cfg{allow_sort_by_rating}) {
                    read($WORD_IND,$rat,$sorting_length);
                }
                $word_ind_pos += $sorting_length if $cfg{allow_sort_by_rating};

                if ($cfg{word_dist} && $scalar_query > 1) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$dist,$word_ind_length*4);
                }
                $word_ind_pos += $word_ind_length*4 if $cfg{word_dist};

                if (scalar(@attr_def)) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$attr,$word_ind_length*$cfg{attr_l});
                }
                
                $doc_all .= $doc;
                $rat_all .= $rat;
                $dist_all .= $dist;
                $attr_all .= $attr;
                
                $word_ind_pos = $next_word_ind_pos;
            }

        }
    }
    
    
    return($doc_all, $rat_all, $dist_all, $attr_all);
}
#=====================================================================
#
#    Function: get_data_substr
#    Last modified: 04.12.2004 21:11
#
#=====================================================================

sub get_data_substr {

    my ($HASH, $WORD_IND, $query, $scalar_query) = @_;
    

    print "sub get_data_substr<BR>\n" if $cfg{debug_level} > 1;
   

    my $doc = "";
    my $rat = "";
    my $dist = "";
    my $attr = "";
    my $doc_all = "";
    my $rat_all = "";
    my $dist_all = "";
    my $attr_all = "";
    
    if (! -e "$db/sub_hash") {
        return($doc_all, $rat_all, $dist_all, $attr_all);
    }
    
    print "Start substring search<BR>\n" if $cfg{debug_level} == 5;
    
    open HASH, "$db/sub_hash" or my_die("Can't open '$db/sub_hash' - $!",__LINE__,__FILE__);
    binmode(HASH);
    open DIC, "$db/dic_ind" or my_die("Can't open '$db/dic_ind' - $!",__LINE__,__FILE__);
    binmode(DIC);
    
    my $query_reg = $query;
    $query_reg =~ s|\*|.*|g;
    $query_reg =~ s|\?|.|g;
    if ($query =~ m|[*?]|) {
        $query_reg = "^".$query_reg."\$";
    }
    
    print "Q: $query - $query_reg<BR>\n" if $cfg{debug_level} == 5;
    
    my @n_gram = ();
    foreach my $dum (split /[*?]+/,$query) {
        if (length($dum) < $cfg{substr_len}) { next }
        my $subbound = length($dum) - $cfg{substr_len} + 1;
        for (my $i=0; $i<$subbound; $i++){
            push @n_gram, substr($dum,$i,$cfg{substr_len});
    	}
    }
    
    my @words_pos = ();
    my $dum;
    my $hash_pos;
    my $first = 1;
    my $len = "";
    my $data = "";
    my $all_vec = "";
    foreach my $n_gram (@n_gram) {
        print "n_gram: $n_gram<BR>\n" if $cfg{debug_level} == 5;
        my $found = 0;
        $data = "";
        my $hash_value = hash($n_gram) % $cfg{substr_hash_size};
        seek(HASH,$hash_value*4,0);
        read(HASH,$dum,4);
        $hash_pos = unpack("N",$dum);
        
        if ($hash_pos == 0) {
            return($doc_all, $rat_all, $dist_all, $attr_all);
        } else {
            while ($hash_pos != 0) {
                seek(HASH,$hash_pos,0);
                read(HASH,$dum,8);
                ($hash_pos,$len) = unpack("NN",$dum);
                read(HASH,$dum,$cfg{substr_len});
                if ($n_gram eq $dum) {
                    read(HASH,$data,$len);
                    $found = 1;
                    last;
                }
            }
            if ($found == 0) { return($doc_all, $rat_all, $dist_all, $attr_all) }
        }
        print "n_gram '$n_gram' found<BR>\n" if $cfg{debug_level} == 5;
        my $tvec = "";
        foreach my $wnum (unpack("N*",$data)) {
            vec($tvec,$wnum,1) = 1;
        }
        if ($first) {
            $all_vec = $tvec;
            $first = 0;
        } else {
            $all_vec = $all_vec & $tvec;
        }
    }
    
    my @words = bitvec_to_list($all_vec);
    print "Words: @words<BR>\n" if $cfg{debug_level} == 5;
    for (my $i=0; $i<=$#words; $i++){
        seek(DIC,$words[$i]*4,0);
        read(DIC,$dum,4);
        $words[$i] = unpack("N",$dum);
    }
    
    my $word_l;
    my $word;
    my $word_ind_pos = "";
    my $next_word_ind_pos = "";
    my $word_ind_length = "";
    foreach my $pos (@words) {
    	seek($HASH,$pos,0);
        read($HASH,$dum,9);
        ($dum, $word_ind_pos, $word_l) = unpack("NNC", $dum);
        read($HASH,$word,$word_l);
        print "Word: $word - $pos<BR>\n" if $cfg{debug_level} == 5;
        if ($word =~ m|$query_reg|) {
            while ($word_ind_pos) {
                seek($WORD_IND,$word_ind_pos,0);
                read($WORD_IND,$dum,8);
                $word_ind_pos += 8;
                ($next_word_ind_pos,$word_ind_length) = unpack("NN",$dum);
                read($WORD_IND,$doc,$word_ind_length*$cfg{'index_format_l'});
                $word_ind_pos += $word_ind_length*$cfg{'index_format_l'};

                my $sorting_length =  $word_ind_length * $cfg{sformat_rating_length};
                if ($cfg{allow_sort_by_rating}) {
                    read($WORD_IND,$rat,$sorting_length);
                }
                $word_ind_pos += $sorting_length if $cfg{allow_sort_by_rating};

                if ($cfg{word_dist} && $scalar_query > 1) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$dist,$word_ind_length*4);
                }
                $word_ind_pos += $word_ind_length*4 if $cfg{word_dist};

                if (scalar(@attr_def)) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$attr,$word_ind_length*$cfg{attr_l});
                }
                
                $doc_all .= $doc;
                $rat_all .= $rat;
                $dist_all .= $dist;
                $attr_all .= $attr;
                
                $word_ind_pos = $next_word_ind_pos;
            }
        }
    }
    
    
    return($doc_all, $rat_all, $dist_all, $attr_all);
}
#=====================================================================
#
#    Function: get_data_fuzzy
#    Last modified: 04.12.2004 21:11
#
#=====================================================================

sub get_data_fuzzy {

    my ($HASH, $WORD_IND, $query, $scalar_query) = @_;
    

    print "sub get_data_fuzzy<BR>\n" if $cfg{debug_level} > 1;
   

    my $doc = "";
    my $rat = "";
    my $dist = "";
    my $attr = "";
    my $doc_all = "";
    my $rat_all = "";
    my $dist_all = "";
    my $attr_all = "";
    
    if (! -e "$db/sub_hash") {
        return($doc_all, $rat_all, $dist_all, $attr_all);
    }
    
    print "Start substring search<BR>\n" if $cfg{debug_level} == 5;
    
    open HASH, "$db/sub_hash" or my_die("Can't open '$db/sub_hash' - $!",__LINE__,__FILE__);
    binmode(HASH);
    open DIC, "$db/dic_ind" or my_die("Can't open '$db/dic_ind' - $!",__LINE__,__FILE__);
    binmode(DIC);
    
    my $query_reg = $query;
    $query_reg =~ s|\*|.*|g;
    $query_reg =~ s|\?|.|g;
    $query_reg = "^".$query_reg."\$";
    
    print "Fuzzy search, Q: $query<BR>\n" if $cfg{debug_level} == 5;
    
    my @n_gram = ();
    my $subbound = length($query) - $cfg{substr_len} + 1;
    for (my $i=0; $i<$subbound; $i++){
        push @n_gram, substr($query,$i,$cfg{substr_len});
    }
    
    my @words_pos = ();
    my $dum;
    my $hash_pos;
    my $first = 1;
    my $len = "";
    my $data = "";
    my $all_vec = "";
    foreach my $n_gram (@n_gram) {
        print "n_gram: $n_gram<BR>\n" if $cfg{debug_level} == 5;
        my $found = 0;
        $data = "";
        my $hash_value = hash($n_gram) % $cfg{substr_hash_size};
        seek(HASH,$hash_value*4,0);
        read(HASH,$dum,4);
        $hash_pos = unpack("N",$dum);
        
        if ($hash_pos == 0) {
            next;
        } else {
            while ($hash_pos != 0) {
                seek(HASH,$hash_pos,0);
                read(HASH,$dum,8);
                ($hash_pos,$len) = unpack("NN",$dum);
                read(HASH,$dum,$cfg{substr_len});
                if ($n_gram eq $dum) {
                    read(HASH,$data,$len);
                    $found = 1;
                    last;
                }
            }
            if ($found == 0) { next; }
        }
        print "n_gram '$n_gram' found<BR>\n" if $cfg{debug_level} == 5;
        my $tvec = "";
        foreach my $wnum (unpack("N*",$data)) {
            vec($tvec,$wnum,1) = 1;
        }
        if ($first) {
            $all_vec = $tvec;
            $first = 0;
        } else {
            $all_vec = $all_vec | $tvec;
        }
    }
    
    my @words = bitvec_to_list($all_vec);
    print "Fuzzy search. Words number: ",scalar @words,"<BR>\n" if $cfg{debug_level} > 2;
    print "Words: @words<BR>\n" if $cfg{debug_level} == 5;
    for (my $i=0; $i<=$#words; $i++){
        seek(DIC,$words[$i]*4,0);
        read(DIC,$dum,4);
        $words[$i] = unpack("N",$dum);
    }
    
    my $word_l;
    my $word;
    my $word_ind_pos = "";
    my $next_word_ind_pos = "";
    my $word_ind_length = "";
    foreach my $pos (@words) {
    	seek($HASH,$pos,0);
        read($HASH,$dum,9);
        ($dum, $word_ind_pos, $word_l) = unpack("NNC", $dum);
        read($HASH,$word,$word_l);
        if (lev_distance_test($query,$word,$cfg{min_distance}) != -1) {
            print "Levenshtein: $query,$word,$cfg{min_distance}<BR>\n" if $cfg{debug_level} > 2;
            $all_wordforms{$word} = 4;
            while ($word_ind_pos) {
                seek($WORD_IND,$word_ind_pos,0);
                read($WORD_IND,$dum,8);
                $word_ind_pos += 8;
                ($next_word_ind_pos,$word_ind_length) = unpack("NN",$dum);
                read($WORD_IND,$doc,$word_ind_length*$cfg{'index_format_l'});
                $word_ind_pos += $word_ind_length*$cfg{'index_format_l'};

                my $sorting_length =  $word_ind_length * $cfg{sformat_rating_length};
                if ($cfg{allow_sort_by_rating}) {
                    read($WORD_IND,$rat,$sorting_length);
                }
                $word_ind_pos += $sorting_length if $cfg{allow_sort_by_rating};

                if ($cfg{word_dist} && $scalar_query > 1) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$dist,$word_ind_length*4);
                }
                $word_ind_pos += $word_ind_length*4 if $cfg{word_dist};

                if (scalar(@attr_def)) {
                    seek($WORD_IND,$word_ind_pos,0);
                    read($WORD_IND,$attr,$word_ind_length*$cfg{attr_l});
                }
                
                $doc_all .= $doc;
                $rat_all .= $rat;
                $dist_all .= $dist;
                $attr_all .= $attr;
                
                $word_ind_pos = $next_word_ind_pos;
            }
        }
    }
    
    
    return($doc_all, $rat_all, $dist_all, $attr_all);
}
#=====================================================================
#
#    Function: get_data_correct
#    Last modified: 04.12.2004 21:12
#
#=====================================================================

sub get_data_correct {
 
    my ($HASH, $query) = @_;

    print "sub get_data_correct<BR>\n" if $cfg{debug_level} > 1;
   

    my @n_gram = ();
    my %n_gram = ();
    my $subbound = length($query) - $cfg{substr_len} + 1;
    for (my $i=0; $i<$subbound; $i++){
        push @n_gram, substr($query,$i,$cfg{substr_len});
    }

    open HASH, "$db/sub_hash" or return;
    binmode(HASH);
    open DIC, "$db/dic_ind" or return;
    binmode(DIC);

    my $dum;
    my $hash_pos;
    my $len = "";
    my $data = "";
    foreach my $n_gram (@n_gram) {
        print "n_gram: $n_gram<BR>\n" if $cfg{debug_level} == 5;
        $data = "";
        my $hash_value = hash($n_gram) % $cfg{substr_hash_size};
        seek(HASH,$hash_value*4,0);
        read(HASH,$dum,4);
        $hash_pos = unpack("N",$dum);
        
        if ($hash_pos == 0) {
            next;
        } else {
            while ($hash_pos != 0) {
                seek(HASH,$hash_pos,0);
                read(HASH,$dum,8);
                ($hash_pos,$len) = unpack("NN",$dum);
                read(HASH,$dum,$cfg{substr_len});
                if ($n_gram eq $dum) {
                    read(HASH,$data,$len);
                    last;
                }
            }
        }
        print "n_gram '$n_gram' found<BR>\n" if $cfg{debug_level} == 5;
        my $tvec = "";
        foreach my $wnum (unpack("N*",$data)) {
            vec($tvec,$wnum,1) = 1;
        }
        $n_gram{$n_gram} = $tvec;
    }

    
    my $vec = "";
    for (my $i=0; $i < length($query); $i++) {
        my $q1 = substr($query,0,$i);
        my $q2 = substr($query,$i+1);
        
        @n_gram = ();
        my $subbound = length($q1) - $cfg{substr_len} + 1;
        for (my $i=0; $i<$subbound; $i++){
            push @n_gram, substr($q1,$i,$cfg{substr_len});
        }
        my $subbound = length($q2) - $cfg{substr_len} + 1;
        for (my $i=0; $i<$subbound; $i++){
            push @n_gram, substr($q2,$i,$cfg{substr_len});
        }
        my $tvec = $n_gram{$n_gram[0]};
        foreach my $n_gram (@n_gram) {
            $tvec = $tvec & $n_gram{$n_gram};
        }
        $vec = $vec | $tvec;
    }

    my @words = bitvec_to_list($vec);
    print "Fuzzy search. Words number: ",scalar @words,"<BR>\n" if $cfg{debug_level} > 2;
    print "Words: @words<BR>\n" if $cfg{debug_level} == 5;
    for (my $i=0; $i<=$#words; $i++){
        seek(DIC,$words[$i]*4,0);
        read(DIC,$dum,4);
        $words[$i] = unpack("N",$dum);
    }


    my $word_ind_pos;
    my $word_l;
    my $word;
    foreach my $pos (@words) {
    	seek($HASH,$pos,0);
        read($HASH,$dum,9);
        ($dum, $word_ind_pos, $word_l) = unpack("NNC", $dum);
        read($HASH,$word,$word_l);
        my $dis = lev_distance_test($query,$word,1);
        if ($dis != -1 && $dis != 0) {
            print "Levenshtein: $query,$word - $dis<BR>\n" if $cfg{debug_level} > 2;
            return $word;
        }
    }
    return;
   
}
#=====================================================================
#
#    Function: boolean
#    Last modified: 04.12.2004 21:12
#
#=====================================================================

sub boolean {
    
    my %query = @_;

    print "sub boolean<BR>\n" if $cfg{debug_level} > 1;
   

    my $zero_vec = $resall_vec & ~ $resall_vec;

    if (defined $query{logical} && $query{logical} == 1) {
        for (my $i=0; $i<scalar(@{$query{query_arr}}); $i++) {
            $res_vec[$i] = $res_vec[$i] | $zero_vec;
            $query{boolean_query} =~ s/${$query{query_arr}}[$i]/\$res_vec[$i]/;
        }
        $query{boolean_query} = '$resall_vec = ' . $query{boolean_query};
        eval $query{boolean_query};
    } else {
        for (my $i=0; $i<scalar(@{$query{query_arr}}); $i++) {
            if (${$query{querymode}}[$i] == 1) {              
                $resall_vec = $resall_vec & ~ ($res_vec[$i] | $zero_vec)
            }
            if (${$query{querymode}}[$i] == 2) {               
                $resall_vec = $resall_vec & $res_vec[$i]
            }
        }
    }
    
    
    if (-e "$db/$cfg{URL_DEL}") {
        foreach (unpack("N*",get_deleted_url())) {
            vec($resall_vec,$_,1) = 0;
        }
    }

}
#=====================================================================
#
#    Function: attr_comp
#    Last modified: 07.02.2004 19:47
#
#=====================================================================

sub attr_comp {

    my %query = @_;

    my @t_res = ();
    my $t_vec = "";
    
    foreach my $key (keys %query) {
        if ($key =~ m|a(\d+)(.+)|) {
            my $f_num = $1;
            my $action = $2;
            my $data = $query{$key};

            my $format = "";
            my $format_l = 0;
            
            #   integer attribute
            if (("$attr_conf[$f_num]" & "\x04") ne "\x00") {
                $format = $cfg{int_attr_format};
                $format_l = $cfg{int_attr_format_l};
            }
            #   float attribute
            if (("$attr_conf[$f_num]" & "\x08") ne "\x00") {
                $format = $cfg{float_attr_format};
                $format_l = $cfg{float_attr_format_l};
            }
            #   date attribute
            if (("$attr_conf[$f_num]" & "\x10") ne "\x00") {
                $format = "l";
                $format_l = 4;
                $data = convert_date($data,$cfg{date_format});
            }
            if ($format eq "") { next }
            
            print "sub attr_comp: Data = $data<BR>\n" if $cfg{debug_level} >= 5;
            
            $t_vec = "" if (! defined $query{ctype} || $query{ctype} ne "OR");
            @t_res = bitvec_to_list($resall_vec) if scalar(@t_res) == 0;
            
            if (not defined $attr_data[$f_num]) {
                open IN, "$db/attr_$f_num" or print "Can't open file '$db/attr_$f_num' - $!<BR>";
                binmode(IN);
                local $/;
                $attr_data[$f_num] = <IN>;
                close(IN);
            }
            
            for (my $i = 0; $i < scalar(@t_res); $i++) {
                
                if ($action eq "lt") {
                    if (unpack("$format",substr($attr_data[$f_num],$t_res[$i]*$format_l,$format_l)) < $data) {
                        vec($t_vec,$t_res[$i],1) = 1;
                    }
                } elsif ($action eq "gt") {
                    if (unpack("$format",substr($attr_data[$f_num],$t_res[$i]*$format_l,$format_l)) > $data) {
                        vec($t_vec,$t_res[$i],1) = 1;
                    }
                } elsif ($action eq "le") {
                    if (unpack("$format",substr($attr_data[$f_num],$t_res[$i]*$format_l,$format_l)) <= $data) {
                        vec($t_vec,$t_res[$i],1) = 1;
                    }
                } elsif ($action eq "ge") {
                    if (unpack("$format",substr($attr_data[$f_num],$t_res[$i]*$format_l,$format_l)) >= $data) {
                        vec($t_vec,$t_res[$i],1) = 1;
                    }
                } elsif ($action eq "eq") {
                    if (unpack("$format",substr($attr_data[$f_num],$t_res[$i]*$format_l,$format_l)) == $data) {
                        vec($t_vec,$t_res[$i],1) = 1;
                    }
                }
            }
            $resall_vec = $resall_vec & $t_vec if (! defined $query{ctype} || $query{ctype} ne "OR");

        }
    }
    $resall_vec = $resall_vec & $t_vec if (defined $query{ctype} && $query{ctype} eq "OR");
    
    
}
#=====================================================================
#
#    Function: dist
#    Last modified: 28.07.2004 10:06
#
#=====================================================================

sub dist {
    
    my %query = @_;

if ( scalar(@{$query{query_arr}}) > 1 && $cfg{word_dist} && 
    ( ( $query{s} eq "R" && $cfg{allow_sort_by_rating} ) || @{$query{phrase_arr}} ) ) {
    
    @res = bitvec_to_list($resall_vec);
    
    foreach my $res (@res) {
        
        my $dist_vec = "";
        my $ok = 1;
        if (defined @{$query{phrase_arr}}) {
            my %phrase_vec;
            foreach my $phrase (keys %{$query{phrase_hash}}) {
                my @dist_pos = unpack("N*",$dist_data_phrase{$phrase}[$res]);
                my $dum;
                foreach my $pos (@dist_pos) {
                    seek($FH_DIST,$pos,0);
                    read($FH_DIST,$dum,2);
                    $dum = unpack("n",$dum);
                    read($FH_DIST,$dum,$dum);
                    foreach ( unpack("n*",$dum)) {
                        vec($dist_vec,$_,1) = 1;
                        vec($phrase_vec{$phrase},$_,1) = 1;
                    }
                }
            }
            foreach my $phrase (@{$query{phrase_arr}}) {
                my @words = split /\s+/, $phrase;
                my @pos = bitvec_to_list($phrase_vec{$words[0]});
                foreach my $pos (@pos) {
                    $ok = 1;
                    for(my $i=1; $i < scalar(@words); $i++) {
                        if (vec($phrase_vec{$words[$i]},$pos+$i,1) != 1) {
                            $ok = 0;
                            last;
                        }
                    }
                    last if $ok;
                }
                if (! $ok) {
                    vec($resall_vec,$res,1) = 0;
                    last;
                }
            }
        }
        next if ! $ok;
        next if $query{s} ne "R";
        
        my @dist_pos = unpack("N*",$dist_data[$res]);
        my $dum;
        foreach my $pos (@dist_pos) {
            seek($FH_DIST,$pos,0);
            read($FH_DIST,$dum,2);
            $dum = unpack("n",$dum);
            read($FH_DIST,$dum,$dum);
            foreach ( unpack("n*",$dum)) {
                vec($dist_vec,$_,1) = 1;
            }
        }
        
        my $bits = unpack "b*", $dist_vec;
        my @mm = ($bits =~ m|111|g);
        $sorting_data[$res] += scalar(@mm)*4*$cfg{weight_dist};
        @mm = ($bits =~ m|101|g);
        $sorting_data[$res] += scalar(@mm)*2*$cfg{weight_dist};
        @mm = ($bits =~ m|11|g);
        $sorting_data[$res] += scalar(@mm)*3*$cfg{weight_dist};
    }
    
}

}
#=====================================================================
#
#    Function: sorting
#    Last modified: 07.12.2003 17:39
#
#=====================================================================

sub sorting {
    
    my %query = @_;

    @res = bitvec_to_list($resall_vec);

    if (! defined $query{s}) {
        return %query;
    }

#   Sort by attribute

    if (exists  $query{s} && $query{s} =~ m|\d+|) {
        

        my $format = "";
        my $number = 0;
        
        #   integer attribute
        if (("$attr_conf[$query{s}]" & "\x04") ne "\x00") {
            $format = $cfg{int_attr_format};
            $number = 1;
        }
        #   float attribute
        if (("$attr_conf[$query{s}]" & "\x08") ne "\x00") {
            $format = $cfg{float_attr_format};
            $number = 1;
        }
        #   date attribute
        if (("$attr_conf[$query{s}]" & "\x10") ne "\x00") {
            $format = "l";
            $number = 1;
        }

        if ($number) {
            if (not defined $attr_data[$query{s}]) {
                open IN, "$db/attr_$query{s}" or print "Can't open file '$db/attr_$query{s}' - $!<BR>";
                binmode(IN);
                local $/;
                $attr_data[$query{s}] = <IN>;
                close(IN);
            }
            @sorting_data = unpack("$format*",$attr_data[$query{s}]);
            @res = sort { $sorting_data[$a] <=> $sorting_data[$b] } @res;
        } else {
            foreach my $res (@res) {
                my %inf = read_data($query{db},$res);
                if ($query{s} =~ m|\d+_\d+|) {
                    my @sf = split(/_/,$query{s});
                    foreach my $sf (@sf) {
                        $sorting_data[$res] .= $inf{"attr_".$sf}."\x00";
                    }
                } else {
                    $sorting_data[$res] = $inf{"attr_".$query{s}};
                }
            }
            @res = sort { $sorting_data[$a] cmp $sorting_data[$b] } @res;
        }

        
    }


#   Sort by score, title, date, size

    if ( $query{s} eq "D" && $cfg{allow_sort_by_date} ) { &get_results_date(%query) }
    if ( $query{s} eq "S" && $cfg{allow_sort_by_size} ) { &get_results_date(%query) }


if ($query{s} eq "R" && $cfg{allow_sort_by_rating}) {
    @res = sort { $sorting_data[$b] <=> $sorting_data[$a] } @res;
} elsif ($query{s} eq "D" && $cfg{allow_sort_by_date}) {
    @res = sort { $sorting_data[$b] <=> $sorting_data[$a] } @res;
} elsif ($query{s} eq "S" && $cfg{allow_sort_by_size}) {
    @res = sort { $sorting_data[$b] <=> $sorting_data[$a] } @res;
} elsif ($query{s} eq "T") {
    foreach my $res (@res) {
        my %inf = read_data($query{db},$res);
        $sorting_data[$res] = $inf{title};
    }
    @res = sort { $sorting_data[$a] cmp $sorting_data[$b] } @res;
}

if (defined $query{s} && defined $query{o} && $query{o} eq "R") { @res = reverse @res }



return %query;

}
#=====================================================================
#
#    Function: make_links
#    Last modified: 17.11.2004 1:00
#
#=====================================================================

sub make_links {
    
    my %query = @_;



    my $zone;
    my $rescount = $query{rescount};
    my $next_results;
    my $prev_pages;
    my $next_pages;
    my $query_string;
    my $prev_page;
    my $next_page;
    my $page_number;
    
    $query{q_str} = "";
    foreach my $key (keys %query) {
        if ($key =~ m|q(\d+)?|) {
            $query{q_str} .= "\&".$key."=".$query{$key};
        }
    }

    
#    unless (scalar keys %{$query{d}} == 1 && defined ${$query{d}}{0}) {
#        $query{db_string} = join "", map { "&d=".$_ } keys %{$query{d}};
#    }


    $query_string = $query{short_query_str};
    $query_string .= $query{zones_str} if defined $query{zones_str};
    $query_string .= "\&s=".$query{s} if defined $query{s};
    $query_string .= "\&o=".$query{o} if defined $query{o};
    $query_string .= $query{cache_str};


    if ($rescount <= $res_num) {
        $next_results = "";
        $query{next_results} = $next_results;
        $query{prev_pages} = $prev_pages;
        $query{next_pages} = $next_pages;
        $query{prev_page} = $prev_page;
        $query{next_page} = $next_page;
        $query{res_num} = $res_num;
        return %query;
    }
    
    my $mhits = 20 * $res_num;
    my $pos2 = $query{stpos} - $query{stpos} % $mhits;
    my $pos1 = $pos2 - $mhits;
    my $pos3 = $pos2 + $mhits;

    if ($pos1 < 0) { my $prev_pages = "" }
    else {
        $prev_pages = "$cfg{search_script}?$query_string\&stpos=$pos1";
    }

    if ($pos3 > $rescount) { my $next_pages = "" }
    else {
        $next_pages = "$cfg{search_script}?$query_string\&stpos=$pos3";
    }

    $next_results .=  " |\n";
    for (my $i=$pos2; $i<$pos3; $i += $res_num) {
       if ($i >= $rescount) {last}
       $page_number = $i/$res_num+1;
       if ( $i != $query{stpos} ) {
           $next_results .=  "<A HREF=$cfg{search_script}?$query_string\&stpos=$i";
           $next_results .=  ">".$page_number."</A> |\n";
       } else {
           $next_results .=  "<B>".$page_number."</B> |\n";
       }
    }
    
    $prev_page = ""; $next_page = "";
    if ($query{stpos} - $res_num >= 0) {
        $dum = $query{stpos} - $res_num;
        $prev_page = "$cfg{search_script}?$query_string\&stpos=$dum";
    }
    if ($query{stpos} + $res_num < $rescount) {
        $dum = $query{stpos} + $res_num;
        $next_page = "$cfg{search_script}?$query_string\&stpos=$dum";
    }

    $query{next_results} = $next_results;
    $query{prev_pages} = $prev_pages;
    $query{next_pages} = $next_pages;
    $query{prev_page} = $prev_page;
    $query{next_page} = $next_page;
    $query{res_num} = $res_num;
    
    
    return %query;
    
}
#=====================================================================
#
#    Function: read_data
#    Last modified: 15.06.2004 13:56
#
#=====================================================================

sub read_data {
    
    my ($db_num, $res) = @_;
    
    my $dum; my $strpos;
    my %url_info;
    
    
        seek($FINFO_IND[$db_num],$res*4,0) or print "SEEK failed (sub read_data): $!\n";
        read($FINFO_IND[$db_num],$dum,4);
        $strpos = unpack("N",$dum);
        seek($FINFO[$db_num],$strpos,0) or print "SEEK failed (sub read_data): $!\n";
        $dum = readline $FINFO[$db_num];
        $dum =~ s/\x0A//;
        $dum =~ s/\x0D//;
        my ($url, $size, $mdate, $chsum, $title, $description, %zzz) = split(/::/,$dum);
    
    $url_info{url} = $url;
    $url_info{size} = $size;
    $url_info{date} = $mdate;
    $url_info{chsum} = $chsum;
    $url_info{title} = $title;
    $url_info{description} = $description;
    
    if ($cfg{url_length_limit} && length($url) > $cfg{url_length_limit}) {
        $url_info{short_url} = substr($url,0,$cfg{url_length_limit}/2)."...".substr($url,-$cfg{url_length_limit}/2);
    } else {
        $url_info{short_url} = $url;
    }
    
# Here you can cut constant part of the page title, so that it will not
# be displayed in results page.
# Uncomment next line and replace "part_of_title" string by other
# string you want to delete

#    $url_info{title} =~ s/part_of_title//i;
    
    
    foreach my $key (keys %zzz) {
        $url_info{"attr_".$key} = $zzz{$key};
    }
    
    return %url_info;

}
#=====================================================================
#
#    Function: open_db_files_inc
#    Last modified: 30.10.2003 23:04
#
#=====================================================================

sub open_db_files_inc {
    
if ( -e "$db/$cfg{HASH_INC}") {

    $FH_HASH_INC = return_fh();  
    $FH_WORD_IND_INC = return_fh();  

    open $FH_HASH_INC, "$db/$cfg{HASH_INC}" or &my_die("Died: could not open $db/$cfg{HASH_INC} - $!",__LINE__,__FILE__);
    binmode($FH_HASH_INC);

    open $FH_WORD_IND_INC, "$db/$cfg{WORD_IND_INC}" or &my_die("Died: could not open $db/$cfg{WORD_IND_INC} - $!",__LINE__,__FILE__);
    binmode($FH_WORD_IND_INC);

}

}
#=====================================================================
#
#    Function: open_db_files
#    Last modified: 30.10.2003 23:04
#
#=====================================================================

sub open_db_files {

    my $db_num = shift; 
    
if ( -e  "$db/$cfg{HASH}") { 
    
    $FH_HASH = return_fh();  
    $FH_WORD_IND = return_fh();  

    open $FH_HASH, "$db/$cfg{HASH}" or &my_die("Died: could not open $db/$cfg{HASH} - $!",__LINE__,__FILE__);
    binmode($FH_HASH);
    open $FH_WORD_IND, "$db/$cfg{WORD_IND}" or &my_die("Died: could not open $db/$cfg{WORD_IND} - $!",__LINE__,__FILE__);
    binmode($FH_WORD_IND);
}

if ($cfg{'word_dist'}) {
    $FH_DIST = return_fh();
    open $FH_DIST, "$db/$cfg{DIST}" or &my_die("Died: could not open $db/$cfg{DIST} - $!",__LINE__,__FILE__);
    binmode($FH_DIST);
}

    $FINFO[$db_num] = return_fh();
    $FINFO_IND[$db_num] = return_fh();

    open $FINFO[$db_num], "$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO} - $!",__LINE__,__FILE__);
    open $FINFO_IND[$db_num], "$db/$cfg{FINFO_IND}" or &my_die("Could not open $db/$cfg{FINFO_IND} - $!",__LINE__,__FILE__);
    binmode($FINFO_IND[$db_num]);
    

}
#=====================================================================
#
#    Function: get_zone_info
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_zone_info {
    my %query = @_;
    my $zone_vec = "";
    my $zone; my $pos; my $length; my $vec; my $dum;
    open ZONE_INFO, "$db/$cfg{ZONE_INFO}" or return undef;
    binmode(ZONE_INFO);
    foreach $zone (keys %{$query{z}}) {
        if ($zone > scalar(@zone)-1) { next }
        seek(ZONE_INFO,8*$zone,0);
        read(ZONE_INFO,$dum,8);
        ($pos,$length) = unpack("NN",$dum);
        seek(ZONE_INFO,$pos,0);
        read(ZONE_INFO,$vec,$length);
        $zone_vec = $zone_vec | $vec;
    }
    close(ZONE_INFO);
    return $zone_vec;
}
#=====================================================================
#
#    Function: get_deleted_url
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_deleted_url {
    
    open URL_DEL, "$db/$cfg{URL_DEL}" or return undef;
    binmode(URL_DEL);
    my $size = (stat(URL_DEL))[7];
    my $url_list;
    {
        local $/;
        $url_list = <URL_DEL>;
    }
    close(URL_DEL);
    return $url_list;
    
}
#=====================================================================
#
#    Function: high_light_query
#    Last modified: 17.11.2004 23:12
#
#=====================================================================

sub high_light_query {
    my ($text,%query) = @_;
    my $description = " ".$text." ";
    my $queryword = "";
    
    print "sub high_light_query<BR>" if $cfg{debug_level} >3;

    my $hl_start = $cfg{mark_start};
    my $hl_end   = $cfg{mark_end};
    my $descr_len = $cfg{descr_size};
    my @query_pos = ();
    my $regexp;
    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS}.$cfg{numbers};
    my $notlet = "[^$let]";

    my $full_regexp = ""; 

    foreach $queryword (keys %all_wordforms) {
    	&$to_lower_case(\$queryword);
    	my $queryword_trans = trans($queryword);
    	
    	print "$queryword - $all_wordforms{$queryword}<BR>" if $cfg{debug_level} == 10;
    	
	    if ($all_wordforms{$queryword} == 2) {
	        if ($queryword_trans !~ m|[*?]|) {
	            $queryword_trans = "*".$queryword_trans."*";
	        }
	        $queryword_trans =~ s|\?|[$let]|g;
	        $queryword_trans =~ s|\*|[$let]*?|g;
	        $regexp = "$notlet$queryword_trans$notlet";
	        print "<BR>RegExp: $regexp<BR>\n" if $cfg{debug_level} == 10;
	    } elsif ($all_wordforms{$queryword} == 1 or $cfg{INDEXING_SCHEME} == 1) {
	        $queryword_trans =~ s|\?|[$let]|g;
	        $queryword_trans =~ s|\*|[$let]*?|g;
	        $regexp = "$notlet$queryword_trans$notlet";
	    } elsif ($all_wordforms{$queryword} == 4) {
	        $regexp = "$notlet$queryword_trans$notlet";
	    } elsif ($cfg{INDEXING_SCHEME} == 2) {
	        $regexp = "$notlet$queryword_trans";
	        $queryword_trans = $queryword_trans."[$let]*?";
	    } else {
	        $queryword_trans =~ s|\?|[$let]|g;
	        $queryword_trans =~ s|\*|[$let]*?|g;
	        $regexp = "$queryword_trans.";
	    }
	    
	    $full_regexp .= "$queryword_trans|";
	    
	    if (length($queryword) < 4) { $regexp = "$notlet$queryword_trans$notlet" }
    	while ( $description =~ /$regexp/gs ) {
    	    push (@query_pos, pos($description));
    	}
    }
    
    chop($full_regexp);
    print "RegExp: $full_regexp<BR>\n" if $cfg{debug_level} == 10;

    @query_pos = sort { $a <=> $b } @query_pos;
    
    my $start = my $end = my $max = my $cmax = my $cend = 0;
    for ( my $i=0; $i<scalar(@query_pos); $i++) {
    	$cmax = 0;
        for ( my $j=$i; $j<scalar(@query_pos); $j++) {
            if ($query_pos[$j] < $query_pos[$i]+$descr_len/2) {
            	$cmax++;
            } else { last }
        }
        if ($cmax > $max) {
            $start = $query_pos[$i];
            $max = $cmax;
        }
    }
    my $delta = int ($descr_len / 2);
    $start -= $delta;
    $end = $start + $descr_len;
    if ($start < 0) { $end -= $start; $start = 0 }
    
    $start = int($start);
    $end = int($end);
    
    my $dl = $end-$start+10;
    if ($start > 30000) {
        $description = substr($description,$start-100);
        $start = 100;
    }
    $description =~ m|.{0,$start}$notlet(.{1,$dl})$notlet|;
    my $pre_descr = " ".$1." ";
    
    $pre_descr =~ s/($notlet)($full_regexp)(?=$notlet)/$1$hl_start$2$hl_end/gs;
    
    $description = "... ".$pre_descr." ...";
    return $description;
}    	

#=====================================================================
#
#    Function: trans
#    Last modified: 10.04.2004 22:59
#
#=====================================================================

sub trans {
    my $word = shift;

    my $let = "a-zA-Z".$cfg{CAP_LETTERS}.$cfg{LOW_LETTERS};
    
    $word =~ s|([$let])|"[$1".&$to_capital_case_ret($1)."]"|ge;
    return $word;
    
}
#=====================================================================
#
#    Function: characters_translation
#    Last modified: 26.07.2004 19:26
#
#=====================================================================

sub characters_translation {
    my $word = shift;
    
    my %trans_rules = %{$cfg{translation_rules}};
    my %trans_rules_2 = reverse %trans_rules;
    my %res = ();
    $res{$word} = 1;
    
    my $dword = $word;
    foreach my $key (keys %trans_rules) {
        $dword =~ s|$key|$trans_rules{$key}|g;
    }
    $res{$dword} = 1;
    $dword = $word;
    foreach my $key (keys %trans_rules_2) {
        $dword =~ s|$key|$trans_rules_2{$key}|g;
    }
    $res{$dword} = 1;
    
    return keys %res;
    
}    
#=====================================================================
#
#    Function: cache_put
#    Last modified: 03.01.2005 22:41
#
#=====================================================================

sub cache_put {
 
    my %query = @_;

   
    
    my $s_time = 0;
    eval { ($s_time,undef,undef,undef) = times };
    if ($@) { $s_time = 0 }

    
    if (exists $query{cache}) { return %query }
    if ($cfg{enable_cache} ne "YES") { return %query }
    if ($query{rescount} < $cfg{min_doc_found} && $s_time < $cfg{min_search_time}) { return %query }

    print "sub cache_put<BR>\n" if $cfg{debug_level} > 1;
    
    
    my $filename = hash($query{query_str});
    open OUT, ">cache/$filename" or return %query;
    binmode(OUT);
    
    
    $query{cache_str} .= "&cache=$filename";
    my %q = %query;
    delete($q{query_arr});
    delete($q{phrase_arr});
    delete($q{phrase_hash});
    delete($q{stpos});
    delete($q{d});
    delete($q{a});
    delete($q{z});
    delete($q{wholeword});
    delete($q{querymode});
    delete($q{query_res_count});
    
    if (scalar keys %all_wordforms) {
        $q{all_wordforms} = join "|", keys %all_wordforms;
    }
    
    my $query_str = join ":~:",%q;
    print OUT pack("NN",length($query_str),$query{rescount});
    print OUT $query_str;
    print OUT pack("N*",@res);
    if ($query{s} eq "R") {
        for (my $i=0; $i < scalar(@res); $i++) {
            print OUT pack("N ",$sorting_data[$res[$i]])
        }
    }

    close(OUT);
    
    
    return %query;
}
#=====================================================================
#
#    Function: cache_get
#    Last modified: 04.12.2004 22:22
#
#=====================================================================

sub cache_get {
 
    my %query = @_;

    print "sub cache_get<BR>\n" if $cfg{debug_level} > 1;
   
    my $filename = $query{cache};
    open IN, "cache/$filename" or return (-1,%query);
    binmode(IN);
    
    my $dum = "";
    read(IN,$dum,8);
    my ($str_len,$res_count) = unpack("NN",$dum);
    
    my $query_str = "";
    read(IN,$query_str,$str_len);
    my %tquery = split /:~:/,$query_str;
    
    if (exists $tquery{all_wordforms}) {
        my @temp = split /\|/,$tquery{all_wordforms};
        @all_wordforms{@temp} = ();
        delete $tquery{all_wordforms};
    }
        


    
        
    my $res = "";
#    read(IN,$res,$res_count*4) or return (-2,%query);
    read(IN,$res,$res_count*4);
    return (-2,%query) if length($res) != $res_count*4;
    @res = unpack("N*",$res);
    
    if ($query{s} eq "R") {
        my $sort = "";
#        read(IN,$sort,$res_count*4) or return (-3,%query);
        read(IN,$sort,$res_count*4);
        return (-3,%query) if length($sort) != $res_count*4;
        for (my $i=0; $i < scalar(@res); $i++) {
            $sorting_data[$res[$i]] = unpack("N",substr($sort,$i*4,4));
        }
    }
    close(IN);


    %query = (%query, %tquery);

    
    if ($cfg{delete_cache} eq "YES") {
        
        opendir(DIR,"cache");
 
        while (my $item = readdir(DIR)) {
            
            
            if ($item =~ /^\./) { next }
            if ( -f "cache/$item") {
                my $filename = "cache/$item";
                my $age = time - (stat($filename))[9];
                if ($age > $cfg{delete_cache_delay}) {
                    unlink($filename);
                }
            }
        }
        closedir(DIR);
    }
    
    
    return (1,%query);
}
#===================================================================



1;

