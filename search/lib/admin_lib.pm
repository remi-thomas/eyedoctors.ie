#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::admin_lib;
use lib::common_lib;
use lib::spider_lib;
use lib::index_lib;
use strict;
use riconfig;
use vars qw(@ISA @EXPORT);
no warnings;


use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(
             &read_template &print_template
             &add_page &reindex &del_page &del_pages &change_password &update &update_page
             &config_show &config_update
             
             %param
             );

use vars  qw(
             %param
             );


%param = ();

#=====================================================================
#
#    Function: add_page
#    Last modified: 09.02.2004 16:41
#
#=====================================================================

sub add_page {
    
    
    if (exists $param{'file'}) {
        my %data = ();
        $data{filename} = $param{'file'};
        $data{url} = $param{'file'};
        $data{url} =~ s/^$cfg{base_dir}\///o;
        $data{url} = $cfg{base_url}.$data{url};

        start(%param);
        update_file(\%data);
        finish(%param);

    } else {
        $param{'url'} =~ s/^\s+|\s+$//g;
        if ($param{'url'} !~ m|http://[a-zA-Z0-9/#~:.?+=&%@!\-]+|) {
            print "Wrong URL - $param{'url'}\n";
            return;   
        }
        print "Add page: $param{'url'}\n";
        if ($param{'type'} ne "S") {
            start(%param);
            index_page($param{'url'},\&update_page,$cfg{'login'},$cfg{'password'});
            finish(%param);
        } else {
            my %to_visit = ();
            $to_visit{$param{'url'}} = 1;
            my $rules = $param{'rules'}?$param{'rules'}:$cfg{'rules'};
            start(%param);
            start_spider(\%to_visit,\&update_page, $rules,$cfg{'login'},$cfg{'password'});
            finish(%param);
        }
    }

}
#=====================================================================
#
#    Function: reindex
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub reindex {

    if ($param{'type'} eq "L") {
         open(PROC, "perl index.pl -passw=$param{'pass'} |") or print "Can't run program: $!\n";
         while(<PROC>) {
             print $_;
         }
         close(PROC);
    } else {
         open(PROC, "perl spider.pl -passw=$param{'pass'} |") or print "Can't run program: $!\n";
         while(<PROC>) {
             print $_;
         }
         close(PROC);
    }

}
#=====================================================================
#
#    Function: update
#    Last modified: 05.11.2003 22:52
#
#=====================================================================

sub update {


    if ($param{'type'} eq "L") {
        my $rules = $cfg{'rules'};
        $rules = create_rule($rules);
        start(%param);
        scan_files($cfg{'base_dir'}, $cfg{'base_url'}, $rules, \&update_file);
        finish(%param);
    } else {
        my $rules = $cfg{'rules'};
        my $login = $cfg{'login'};
        my $password = $cfg{'password'};
        my %to_visit = ();
        foreach my $url (@{$cfg{start_url}}) {
            $to_visit{$url} = 1;
        }
        start(%param);
        start_spider(\%to_visit,\&update_page, $rules, $login, $password);
        finish(%param);
    }

}
#=====================================================================
#
#    Function: update_file
#    Last modified: 12.01.2004 20:14
#
#=====================================================================

sub update_file {
    
    my $data = shift;

    open FILE, $data->{filename};
    ($data->{date}) = (stat(FILE))[9];

        my ($url_num,$chsum) = check_url_HD($data->{url});
        if ($url_num  ne undef) {
    	    my $old_date = get_doc_date($url_num);
    	    if ($old_date == $data->{date}) {
    	        print "$data->{url} - file was not changed\n" if $cfg{'verbose_output'};
    	        return;
    	    } else {
    	        print "\n$data->{url} - file was changed and will be reindexed\n";
                delete_url($data->{url});
                print "Url N.$url_num ($data->{url}) deleted\n" if $cfg{'verbose_output'};
    	    }
        } else {
            print "$data->{url} - new file\n" if $cfg{'verbose_output'};
        }


    local $/;
    $data->{text} = <FILE>;
    close(FILE);

    parse_file($data);
    
}
#=====================================================================
#
#    Function: update_page
#    Last modified: 12.01.2004 20:20
#
#=====================================================================

sub update_page {

    my $data = shift;

    my $new_chsum = unpack("%32C*",$data->{text});
    
        my ($url_num,$chsum) = check_url_HD($data->{url});
        
        if ($url_num  ne undef) {
    	    if ($new_chsum == $chsum) {
    	        print "$data->{url} - file was not changed\n";
    	        return;
    	    } else {
    	        print "\n$data->{url} - file was changed and will be reindexed\n";
                delete_url($data->{url});
                print "Url N.$url_num ($data->{url}) deleted\n" if $cfg{'verbose_output'};
    	    }
        } else {
            print "$data->{url} - new file\n" if $cfg{'verbose_output'};
        }
        
     parse_file($data);
    
}
#=====================================================================
#
#    Function: del_page
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub del_page {
    
    $param{'url'} =~ s/^\s+|\s+$//g;
    start(%param);
        my ($url_num,$chsum) = check_url_HD($param{'url'});
        if (defined $url_num) {
            delete_url($param{'url'});
            print "Url N.$url_num ($param{'url'}) deleted\n";
        } else {
            print "No such page\n";
        }

}
#=====================================================================
#
#    Function: del_pages
#    Last modified: 19.01.2005 16:39
#
#=====================================================================

sub del_pages {
    
    $param{'url'} =~ s/^\s+|\s+$//g;
    $param{'url'} =~ s/\*$//g;

    start(%param);

        open FINFO_L, "$cfg{db_cur}/$cfg{FINFO}" or &my_die("Died: could not open $cfg{db_cur}/$cfg{FINFO}",__LINE__,__FILE__);
        open FINFO_IND_L, "$cfg{db_cur}/$cfg{FINFO_IND}" or &my_die("Died: could not open $cfg{db_cur}/$cfg{FINFO_IND}",__LINE__,__FILE__);
        binmode(FINFO_IND_L);
        
        my $count = 0;
        my $count_del = 0;
        while (1) {
            
            my $pos = 0;
            read(FINFO_IND_L,$pos,4) or last;
            $pos = unpack("N",$pos);
            
            seek(FINFO_L,$pos,0);
            my $str = <FINFO_L>;
            $str =~ s/\x0A//;
            $str =~ s/\x0D//;
            my ($url, $size, $mdate, $chsum, $title, $description, %zzz) = split(/::/,$str);
            
            $count++;

            if ($url !~ m|^$param{'url'}|) { next }
            
            my ($url_num,$chsum) = check_url_HD($url);
            if (defined $url_num) {
                delete_url($url);
                print "Url N.$url_num ($url) deleted\n";
                $count_del++;
            }
            
        }
        
        close(FINFO_L);
        close(FINFO_IND_L);
        
        print "Done!<BR>";
        print "$count_del URLs deleted.";


}
#=====================================================================
#
#    Function: change_password
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub change_password {

    if ($param{'pass1'} ne $param{'pass2'}) {
        print "Retype new passwords\n";
    } else {
        if (-e "0_passwd") {
            open IN, "0_passwd" or print "Can't open password file - $!\n";
            my $old_passwd = <IN>;
            close(IN);
            if (crypt($param{'pass3'}, $old_passwd) eq $old_passwd) {
                my $cr_pass = crypt($param{'pass1'}, join '', ('.', '/', 0..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64]);
                open OUT, ">0_passwd" or print "Can't open password file - $!\n";
                print OUT $cr_pass;
                close(OUT);
                print "Password is changed\n";
            } else {
                print "Type correct old password\n";
            }
        } else {
            my $cr_pass = crypt($param{'pass1'}, join '', ('.', '/', 0..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64]);
            open OUT, ">0_passwd" or print "Can't open password file - $!\n";
            print OUT $cr_pass;
            close(OUT);
            print "Password is changed\n";
        }
    }
    
}
#=====================================================================
#
#    Function: config_show
#    Last modified: 04.10.2003 10:24
#
#=====================================================================

sub config_show {

my %templates = read_template($cfg{config_template});

print print_template($templates{"header"},%cfg);

$cfg{rules} =~ s|(\x0D?\x0A)+|\x0A|sg;

print print_template($templates{"config"},%cfg);

print print_template($templates{"footer"},%cfg);

    
}
#=====================================================================
#
#    Function: config_update
#    Last modified: 29.09.2003 18:27
#
#=====================================================================

sub config_update {
    
    
    my $orig_conf = "";
    my $new_conf  = "";
    open CONF, "riconfig.pm" or my_die("Died: can't open riconfig.pm: $!",__LINE__,__FILE__);
    my $old_size = (stat(CONF))[7];
    read(CONF, $orig_conf, $old_size);
    $new_conf = $orig_conf;
    
    while ( my ($k,$v) = each %param) {
        $new_conf =~ s|^(\s*)($k)(\s*=>\s*["']?)(.*?)(['"]?,\s)|$1$k$3$v$5|m;
    }
    
    my $k = 'rules';
    my $v = $param{rules};
    $v =~ s|(\x0D?\x0A)+|\x0A|sg;

    $new_conf =~ s|(\x0A\s*)($k)(\s*=>\s*q\(\s*)(.*?)\s*(\),\s)|$1$k$3$v\x0A$5|s;
    
    open CONF, ">riconfig.pm" or my_die("Died: can't open riconfig.pm: $!",__LINE__,__FILE__);
    print CONF $new_conf;
    close(CONF);
    
    %cfg = (%cfg, %param);
    

    config_show();
    
}
#===================================================================



1;