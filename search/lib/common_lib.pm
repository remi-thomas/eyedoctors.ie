#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::common_lib;
use strict;
use riconfig;
use vars qw(@ISA @EXPORT);
no warnings;

use Exporter;
@ISA = ('Exporter');
@EXPORT      = qw(&hash &esc2char &bitvec_to_list &count_vec
                  &my_die
                  &create_rule
                  &urldecode &urlencode
                  &check_password
                  &scan_files &del_tree &call_parser
                  &get_doc_date &get_url_num &get_cached_page
                  &get_attribute &convert_date
                  &get_page
                  &return_fh
                  &read_template &print_template &parse_SSI
                  &lev_distance_test
                  );

my %html_esc = ();

#===================================================================
#
#   Private configuration parameters
#   Please do not change unless you know what you do
#   
#===================================================================

$cfg{substr_len} = 3;
$cfg{substr_hash_size} = 100001;
$cfg{debug_level} = 0;

#===================================================================

if ($cfg{site_size} == 1) { $cfg{HASHSIZE} = 20001 }
elsif ($cfg{site_size} == 3) { $cfg{HASHSIZE} = 100001 }
elsif ($cfg{site_size} == 4) { $cfg{HASHSIZE} = 300001 }
else { $cfg{HASHSIZE} = 50001 }

#===================================================================

sub prepare_string {
    my $str = shift;
    if (! defined $str) { $str = "" }
    $str =~ s/^\s+|\s+$//;
    $str =~ s/\s+/|/g;
    $str =~ s/\./\\\./g;
    $str = "(".$str.")";
    return $str;
}


$cfg{non_parse_ext_reg} = '^'.prepare_string( $cfg{non_parse_ext} ).'$';

$cfg{ext_parser_ext_reg} = '^'.prepare_string( $cfg{ext_parser_ext} ).'$';

$cfg{ext_parser_ext_html_reg} = '^'.prepare_string( $cfg{ext_parser_ext_html} ).'$';

$cfg{arch_ext_reg} = '^'.prepare_string( $cfg{arch_ext} ).'$';

$cfg{bin_ext_reg} = '^'.prepare_string( $cfg{bin_ext} ).'$';

$cfg{default_filenames_reg} = '/'.prepare_string( $cfg{default_filenames} ).'$';


#===================================================================

$cfg{sformat_rating} = $cfg{sformat_date} = $cfg{sformat_size} = "";
if ($cfg{allow_sort_by_rating}) {
	$cfg{sformat_rating} = "C";
	$cfg{sformat_rating_length} = length(pack($cfg{sformat_rating},0))
}
if ($cfg{allow_sort_by_date}) {
	$cfg{sformat_date} = "N";
	$cfg{sformat_date_length} = length(pack($cfg{sformat_date},0))
}
if ($cfg{allow_sort_by_size}) {
	$cfg{sformat_size} = "N";
	$cfg{sformat_size_length} = length(pack($cfg{sformat_size},0))
}

if ($cfg{'compact_index'}) {
    $cfg{'index_format'} = "n";
    $cfg{'index_format_l'} = 2;
} else {
    $cfg{'index_format'} = "N";
    $cfg{'index_format_l'} = 4;
}


if (scalar @attr_def) {
    

    $cfg{attr} = "";
    vec($cfg{attr},scalar(@attr_def)-1,1) = 0;
    $cfg{attr_l} = length($cfg{attr});
    for (my $i=0; $i < scalar(@attr_conf); $i++) {
        $attr_conf[$i] = chr($attr_conf[$i]);
    }

}


$cfg{int_attr_format} = "l";
$cfg{int_attr_format_l} = length(pack("$cfg{int_attr_format}",0));
$cfg{float_attr_format} = "f";
$cfg{float_attr_format_l} = length(pack("$cfg{float_attr_format}",0));


#===================================================================

if ($cfg{use_stop_words} eq "YES") {
    foreach my $word (@{$cfg{stop_words}}) { $stop_words{$word} = "" }
}
foreach my $word (@{$cfg{index_words}}) { $index_words{$word} = "" }

#===================================================================

$cfg{index_command_tag} =~ s/\s+/|/g;
$cfg{index_command_tag} =~ s/^\||\|$//g;

$cfg{noindex_command_tag} =~ s/\s+/|/g;
$cfg{noindex_command_tag} =~ s/^\||\|$//g;

#=====================================================================
#
#    Function: hash
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub hash {
    my $h = 0x00000000;
    my $f = 0xF0000000;
    my $char; my $g;
    foreach (unpack("C*", $_[0])) {
        $h = ($h << 4) + $_;
        if ($g = $h & $f) { $h ^= $g >> 24 };
        $h &= ~$g;
    }
    $h =  $h % $cfg{HASHSIZE};
    return $h
}
#=====================================================================
#
#    Function: bitvec_to_list
#    Last modified: 18.11.2003 19:56
#
#=====================================================================

sub bitvec_to_list {
    my $vec = shift;
    my @ints;

        use integer;
        my $i;
        # This method is faster with mostly null-bytes
        while($vec =~ /[^\0]/g ) {
            $i = -9 + 8 * pos $vec;
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
            push @ints, $i if vec($vec, ++$i, 1);
        }

    return @ints;
}
#=====================================================================
#
#    Function: count_vec
#    Last modified: 22.11.2003 22:39
#
#=====================================================================

sub count_vec {
    return unpack("%32b*",shift);
}
#=====================================================================
#
#    Function: esc2char
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub esc2char {
    my ($esc) = @_;
    my $char = "";
    if ($esc =~ /&[a-zA-Z]*;/) { $char = $html_esc{$esc} if exists $html_esc{$esc} }
    elsif ($esc =~ /&#([0-9]*);/) {
    	if ($1 <= 255) { $char = chr($1) }
    	else { $char = $cfg{code2char}{$1} if $cfg{code2char}{$1}}
    } elsif ($esc =~ /&#x([0-9a-fA-F]*);/i) {
    	my $code = hex($1);
    	if ($code <= 255) { $char = chr($code) }
    	else { $char = $cfg{code2char}{$code} if $cfg{code2char}{$1}}
    }	
    return $char;
}
#=====================================================================
#
#    Function: create_rule
#    Last modified: 08.09.2003 15:38
#
#=====================================================================

sub create_rule {
    my $rules = shift;

my @rules = split /[\x0D\x0A]+/, $rules;
my $rule_func .= "sub {\n";
$rule_func .= "my \$url = shift;\n";
$rule_func .= "my \$index = 0;\n";
$rule_func .= "my \$follow = 0;\n";

foreach my $rule (@rules) {
    my $index = 1;
    my $follow = 1;
    my $match = "=~";
    my $case  = "i";
    my $reg   = 0;
    if ($rule =~ /^\s*$/) { next }
    if ($rule =~ s|Disallow | |i) { $index = 0; $follow = 0 }
    if ($rule =~ s|Allow | |i) { $index = 1; $follow = 1 }
    if ($rule =~ s|NoIndex | |i) { $index = 0 }
    if ($rule =~ s|Index | |i) { $index = 1 }
    if ($rule =~ s|NoFollow | |i) { $follow = 0 }
    if ($rule =~ s|Follow | |i) { $follow = 1 }
    if ($rule =~ s|NoMatch | |i) { $match = "!~" }
    if ($rule =~ s|Match | |i) { $match = "=~" }
    if ($rule =~ s|NoCase | |i) { $case  = "i" }
    if ($rule =~ s|Case | |i) { $case  = "" }
    if ($rule =~ s|String | |i) { $reg  = 0 }
    if ($rule =~ s|RegExp | |i) { $reg  = 1 }
    if ($rule =~ s|RegEx | |i) { $reg  = 1 }
    $rule =~ s|^\s+||;
    if ( not $reg) {
        $rule =~ s|([.(){}\[\]])|\\$1|g;
        $rule =~ s|\?|.|g;
        $rule =~ s|\*|.*|g;
    }
    my $mask = join "|", map { "^".$_."\$" } split /\s+/, $rule;
    $rule_func .= "if (\$url $match m#$mask#$case) { \$index = $index; \$follow = $follow };\n";

}
$rule_func .= "return \$index, \$follow;\n";
$rule_func .= "}\n";

my $rule_ref = eval "$rule_func";
if ($@) { print $@ }
return $rule_ref;

}
#=====================================================================
#
#    Function: scan_files
#    Last modified: 21.12.2004 16:15
#
#=====================================================================

sub scan_files {

    my ($base_dir, $base_url, $rules, $func, $dirs, $action) = @_;
    my (@dirs,@files,$filename,$newdir,$list,$url,$item);
    
    my $index; my $follow;
    my $last_filename = "";
    my $skip = 1;
    
    if (! $dirs) {
       $dirs = ["$base_dir"];
    }


    if ($action eq "restart") {
        ($dirs,$last_filename) = restart();
    }
        

    $SIG{INT} = sub { print "Ctrl-C pressed\n\n"; $cfg{stop_script} = 1; };


    while (scalar @{$dirs}) {


        my $dir = pop @{$dirs};
        print "$dir - directory\n" if $cfg{verbose_output};
        my $DIR = return_fh();
        opendir($DIR,$dir) or (print "Cannot open $dir: $!\n");
        while ($item = readdir($DIR)) {
            
            if ($action eq "restart" && $skip == 1) {
                if ($last_filename eq "$dir/$item") { $skip = 0 }
                next;
            }
            
            if ($item =~ /^\./) { next }
            if ( -d "$dir/$item") {
                push(@{$dirs}, $dir."/".$item);
            }
            if ( -f "$dir/$item") {
                $filename=$dir."/".$item;
                ($url = $filename) =~ s/^$base_dir\///o;
                $url = $base_url.$url;
                ($index,$follow) = &$rules($url);
                if ($index) {
                    if ($cfg{cut_default_filenames} eq 'YES') { $url =~ s|$cfg{default_filenames_reg}|/|io }
                    if ($cfg{url_to_lower_case} eq 'YES') { $url = lc($url) }
#                    &$func(undef, $url, $filename, undef, undef);
                    &$func( { url => $url, filename => $filename } );
                }
            }

            if ( $cfg{server_timeout} != 0 && time - $cfg{start_time} > $cfg{server_timeout}*0.7 ) {
                $cfg{stop_script} = 1;
            }
            # Stop if "max_files" reached
            if ($cfg{max_files} && $cfg{max_files} < $cfg{cfn}+1) {
                $cfg{stop_script} = 1;
            }
            if ($cfg{stop_script} == 1 && $action ne "archive") {
                save($dirs,$dir,$filename);
                last;
            }
            
            
        }
        closedir($DIR) or (print "Cannot close $dir: $!\n");
        last if ($cfg{stop_script} == 1 && $action ne "archive");
    }
    return 1;
}
#=====================================================================
#
#    Function: save
#    Last modified: 07.10.2003 20:53
#
#=====================================================================

sub save {
    
    my ($dirs,$dir,$filename) = @_;
    
    print "\n\n###################################################\n\n";
    print "Script is preparing to save data for restart.\nPress 'Ctrl-C' again to stop script without saving.";
    print "\n\n###################################################\n\n";
    
    
    open OUT, ">restart_DIR" or print "Can't create restart file: $!\n";
    binmode(OUT);
    foreach (@{$dirs}) {
        print OUT "$_\x0A";
    }
    print OUT "$dir\x0A";
    close(OUT);

    open OUT, ">restart_FILE" or print "Can't create restart file: $!\n";
    binmode(OUT);
    print OUT "$filename";
    close(OUT);

}
#=====================================================================
#
#    Function: restart
#    Last modified: 02.02.2004 13:13
#
#=====================================================================

sub restart {

    my @dirs = ();
    my $last_filename = "";
    
    print "Script is preparing for restart.\n";
    
    
    open IN, "restart_DIR" or print "No restart file is found - $!\n";
    binmode(IN);
    foreach my $str (<IN>) {
        chomp($str);
        push @dirs, $str;
    }
    close(IN);
    unlink("restart_DIR");

    open IN, "restart_FILE" or print "No restart file is found - $!\n";
    binmode(IN);
    $last_filename = <IN>;
    close(IN);
    unlink("restart_FILE");
    
    return \@dirs, $last_filename;

}
#=====================================================================
#
#    Function: call_parser
#    Last modified: 30.08.2004 20:04
#
#=====================================================================

sub call_parser {
    
    my $data = shift;
    
    my ($in_file,$out_file,$temp_dir);
    my $parser = $cfg{ext_parser_conf}{$data->{type}};
    
    if ($data->{filename}) {
        ($data->{size},$data->{date}) = (stat($data->{filename}))[7,9];
        $in_file = $data->{filename};
        if ($parser =~ m/catdoc/i) {
            #################################################
            #                                               #
            #    Catdoc does not support long filenames.    #
            #    So, here is simple hack.                   #
            #                                               #
            #################################################
            open FILE, $data->{filename};
            binmode(FILE);
            read(FILE, $data->{text}, $data->{size});
            close(FILE);
            $in_file = $cfg{'db'}."/temp";
            open TEMP, ">$in_file";
            binmode(TEMP);
            print TEMP $data->{text};
            close(TEMP);
    	    $parser =~ s/%file%/$in_file/;
    	    $data->{text} = `$parser`;
            unlink("$in_file");
            return $data;
        }
    } else {
        $in_file = $cfg{'db'}."/temp";
        open TEMP, ">$in_file";
        binmode(TEMP);
        print TEMP $data->{text};
        close(TEMP);
    }

    if ($parser =~ m/%out_file%/) {
        $out_file = $cfg{'db'}."/temp2";
        $parser =~ s/%out_file%/$out_file/;
    }
    if ($parser =~ /%temp_dir%/) {
        $temp_dir = $cfg{'db'}."/temp_dir";
        $parser =~ s/%temp_dir%/$temp_dir/;
        mkdir("$temp_dir",0777) or print "Can't create tempdir: $!\n";
    }

    $parser =~ s/%file%/$in_file/;
    $data->{text} = `$parser`;
    if ($out_file) {
        open ZZZ, $out_file;
        {
            local $/;
            $data->{text} = <ZZZ>;
        }
        close(ZZZ);
        unlink("$out_file") if $out_file;
    }
    unlink("$in_file") if !$data->{filename};
    del_tree($temp_dir) if $temp_dir;
    
    return $data;

}
#=====================================================================
#
#    Function: del_tree
#    Last modified: 30.08.2004 19:25
#
#=====================================================================

sub del_tree {
    
    my $dir = shift;
    my $DIR = return_fh();
    opendir($DIR,$dir) or (print "Cannot open '$dir': $!\n");
    while (my $item = readdir($DIR)) {
        if ($item =~ /^\./) { next }
        if ( -d "$dir/$item") { 
            del_tree("$dir/$item");
        }
        if ( -f "$dir/$item") {
            unlink("$dir/$item");
        }
    }
    closedir($DIR);
    rmdir("$dir") or (print "Cannot delete '$dir': $!\n");    
}    
#=====================================================================
#
#    Function: check_password
#    Last modified: 28.08.2003 22:17
#
#=====================================================================

sub check_password {
    
    my ($pass) = @_;
    
    if (-e "0_passwd") {
        open IN, "0_passwd" or print "Can't open password file - $!\n";
        my $old_passwd = <IN>;
        close(IN);
        if (crypt($pass, $old_passwd) eq $old_passwd) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
#=====================================================================
#
#    Function: get_doc_date
#    Last modified: 04.11.2003 12:33
#
#=====================================================================

sub get_doc_date {
    my $url_num = shift;
    my $format = $cfg{sformat_date};
    my $format_length = $cfg{sformat_date_length};
    my $FILE = "$cfg{db_cur}/$cfg{FDATE}";
    my $date;
    open FDATE, $FILE;
    binmode(FDATE);
    seek(FDATE,$url_num*$format_length,0);
    read(FDATE,$date,$format_length);
    $date = unpack("$format",$date);
    return $date;
}
#=====================================================================
#
#    Function: get_url_num
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_url_num {
    my $url = shift;
    
    my $db = "db/db_0";
   
    if (! -e "$db/$cfg{HASH_URL}") {
        print "URL database does not exists\n";
        return undef;
    }
        
open HASH_URL_L, "$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL}",__LINE__,__FILE__);
binmode(HASH_URL_L);

open HASH_URL_IND_L, "$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND}",__LINE__,__FILE__);
binmode(HASH_URL_IND_L);

    my $dum; my $dum2; my $cmp_url; my $chsum; my $prev_pos; my $strpos;
    my $url_num;

    my $hash_val = hash($url) % 10001;
    
    seek(HASH_URL_L, $hash_val*4, 0) or print "Seek failed<BR>";
    read(HASH_URL_L,$dum2,4);
    $dum = unpack("N",$dum2);
    
    if ($dum == 0) {
        return undef;
    } else {

        open FINFO_L, "$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO}",__LINE__,__FILE__);
        open FINFO_IND_L, "$db/$cfg{FINFO_IND}" or &my_die("Died: could not open $db/$cfg{FINFO_IND}",__LINE__,__FILE__);
        binmode(FINFO_IND_L);

        while ($dum) {
            $prev_pos = $dum;
            seek(HASH_URL_IND_L,$dum,0);
            read(HASH_URL_IND_L,$dum2,8);
            ($dum,$url_num) = unpack("NN",$dum2);
            seek(FINFO_IND_L,$url_num*4,0);
            read(FINFO_IND_L,$dum2,4);
            $strpos = unpack("N",$dum2);
            seek(FINFO_L,$strpos,0);
            $dum2 = <FINFO_L>;
            ($cmp_url,$chsum) = (split(/::/,$dum2))[0,3];
            if ($url eq $cmp_url) {
                close(FINFO_IND_L);
                close(FINFO_L);
                close(HASH_URL_L);
                close(HASH_URL_IND_L);
                
                return $url_num, $chsum;
            }

        }

        close(FINFO_IND_L);
        close(FINFO_L);

    }
    
    return undef;
    
    close(HASH_URL_L);
    close(HASH_URL_IND_L);
    
}
#=====================================================================
#
#    Function: get_cached_page
#    Last modified: 05.05.2004 19:24
#
#=====================================================================

sub get_cached_page {
    my $url = shift;

    my $db = "db/db_0";
    my $html_text = "";

    my ($url_num,$chsum) = get_url_num($url);
    open PCACHE_IND, "db/db_0/0_page_cache" or &my_die("Died: could not open db/db_0/0_page_cache - $!",__LINE__,__FILE__);
    binmode(PCACHE_IND);
    open PCACHE_DATA, "db/db_0/0_page_cache_data" or &my_die("Died: could not open db/db_0/0_page_cache_data - $!",__LINE__,__FILE__);
    binmode(PCACHE_DATA);
    seek(PCACHE_IND,$url_num*4,0);
    my $dum = "";
    read(PCACHE_IND,$dum,4);
    seek(PCACHE_DATA,unpack("N",$dum),0);
    read(PCACHE_DATA,$dum,4);
    read(PCACHE_DATA,$html_text,unpack("N",$dum));
    if ($cfg{compress_cache}) {
        require Compress::Zlib;
        import Compress::Zlib;
        $html_text = uncompress($html_text);
    }
    
    return $html_text;

}
#=====================================================================
#
#    Function: get_attribute
#    Last modified: 12.01.2004 17:24
#
#=====================================================================

sub get_attribute {
    
    my $data = shift;
    
    my @attr = ();
    
    for (my $i=0; $i < scalar(@attr_def); $i++) {
        
        if (${$attr_def[$i]}[0] eq "DATE") {
            $attr[$i] = $data->{date};
            next;
        }
        
        if (${$attr_def[$i]}[0] eq "SIZE") {
            $attr[$i] = $data->{size};
            next;
        }
        
        my $s1 = ${$attr_def[$i]}[0];
        my $s2 = ${$attr_def[$i]}[1];
        if ($data->{text} =~ m|$s1(.*?)$s2|is) {
            $attr[$i] = $1;
            $attr[$i] =~ s|[\x0A\x0D]| |g;
            $attr[$i] =~ s|:+|:|g;
        }
        
        
    }

#        print "Attr: @attr\n";
    
    return @attr;
}
#=====================================================================
#
#    Function: convert_date
#    Last modified: 02.10.2003 08:11
#
#=====================================================================

sub convert_date {
    
    my ($str,$format) = @_;
    
#    print "convert_date: $str, $format\n";
    
    require Time::Local;
    import Time::Local;
    
    my $day=0;
    my $month = 0;
    my $year = 0;
    
    my %m_hash = ("Jan",1,"Feb",2,"Mar",3,"Apr",4,"May",5,"Jun",6,"Jul",7,"Aug",8,"Sep",9,"Oct",10,"Nov",11,"Dec",12);
    my $m_hash_keys = join "|",keys %m_hash;
        
#   02.04.98
    if ($str =~ m#(\d{1,2})[./](\d{1,2})[./](\d{2,4})#) {
        $day = $format eq "dm"?$1:$2;
        $month = $format eq "dm"?$2:$1;
        $year = $3;
#   Jan 02 1994
    } elsif ($str =~ m#($m_hash_keys)\s+(\d{1,2}),?\s+(\d{4})#) {
        $day = $2;
        $month = $m_hash{$1};
        $year = $3;
    }
    
    $month -= 1;
    if ($year >= 1900) {
        $year = $year - 1900;
    }
    if ($year < 50) {
        $year += 100;
    }

#    print "convert_date: $day.$month.$year\n";
    
    my $time = timelocal(0,0,0,$day,$month,$year);
    return $time
    
}
#=====================================================================
#
#    Function: read_template
#    Last modified: 02.09.2004 09:55
#
#=====================================================================

sub read_template {
    my ($filename) = @_;
    
    my %templates = ();
    my $template = "";
    
    if ($filename =~ /http:\/\//i) {

        require LWP::UserAgent;
        require URI::URL;
        import LWP::UserAgent;
        import URI::URL;
        
        my $resp = get_page($filename,5,"","");
        if ($$resp{'text'}) {
            $template = $$resp{'text'};
        } else {
            print $$resp{'message'},"\n"; 
        }
    } else {
        open TEMPLATE, $filename or print "Could not find template";
        local $/;
        $template = <TEMPLATE>;
        close(TEMPLATE);
    }
        
    while ( $template =~ m|<!-- RiSearch::([^:]+?)::start -->(.*?)<!-- RiSearch::\1::end -->|gs) {
    	$templates{$1} = $2;
    }   
    return %templates;
}
#=====================================================================
#
#    Function: print_template
#    Last modified: 21.10.2003 12:38
#
#=====================================================================

sub print_template {
    my $template = shift;
    my %data = @_;
    

    $data{rand_number} = int (rand(256)) if not exists $data{rand_number};

    $template =~ s|<% DO %>(.*?)<% ENDDO %>|print_template($1,%data)|gseee;
    
    $template =~ s|(<%\s+IF\s+(?:.(?!%>))*?)==((?:.(?!%>))*?\s+%>)|$1 eq $2|gs;
    $template =~ s|(<%\s+IF\s+(?:.(?!%>))*?)!=((?:.(?!%>))*?\s+%>)|$1 ne $2|gs;
    $template =~ s|<%\s+IF\s+%(\w+?)%((?:.(?!<%\s+ENDIF\s+%>))*?)\s+%>((?:.(?!<%\s+ENDIF\s+%>))*?)<%\s+ELSE\s+%>(.*?)<%\s+ENDIF\s+%>|'"'.$data{$1}.'"'.$2.'?$3:$4'|gsee;

    $template =~ s|<%\s+IF\s+%(\w+?)%((?:.(?!<%\s+ENDIF\s+%>))*?)\s+%>(.*?)<%\s+ENDIF\s+%>|'"'.$data{$1}.'"'.$2.'?$3:""'|gsee;
    
    $template =~ s|%right_form\((.*?)\)%|&right_form($1,$data{rescount})|egs;
    $template =~ s|%(\w+?)%|defined $data{$1}?$data{$1}:""|ge;  
    $template = parse_SSI($template,$ENV{'SCRIPT_FILENAME'}) if $cfg{'parse_SSI'};   


    return $template;
}
#=====================================================================
#
#    Function: right_form
#    Last modified: 24.09.2003 21:11
#
#=====================================================================

sub right_form {
    my ($words,$rescount) = @_;
    my $dum = $rescount % 10;
    $words =~ /"([^"]+)","([^"]+)","([^"]+)"/;
    my $w1 = $1; my $w2 = $2; my $w3 = $3;
    my $f = $w3;
    if ($dum == 1) {$f = $w1};
    if (($dum>1) && ($dum<5)) {$f = $w2};
    $dum = $rescount % 100;
    if (($dum>10) && ($dum<15)) {$f = $w3};
    return $f;
}
#=====================================================================
#
#    Function: parse_SSI
#    Last modified: 21.12.2004 17:59
#
#=====================================================================

sub parse_SSI {
    my ($text,$base) = @_;
    my $root = $ENV{'DOCUMENT_ROOT'};
    my $host = $ENV{'HTTP_HOST'};
    $base =~ s|/[^/]+$|| if defined $base;
    
    while (1) {
        if ($text =~ /<!--#include virtual=(['"]?)(.*?)\1\s*-->/i) {
            my $filename = $2;
            my $include;
            if ($filename =~ m{^/?cgi}) {

                require LWP::UserAgent;
                require URI::URL;
                import LWP::UserAgent;
                import URI::URL;

                my $url = "http://".$host."/".$filename;
                my $resp = get_page($url,2);
                $include = $$resp{'text'}?$$resp{'text'}:"[an error occured while processing this directive]";
            } else {
                if ($filename =~ m|^/|) {
                    $filename = $root.$filename;
                } else {
                    $filename = $base."/".$filename;
                }
                {
                    open MY_IN, $filename or my $error = 1;
                    local $/;
                    $include = <MY_IN>;
                    close(MY_IN);
                    if ($error) { $include = "[an error occured while processing this directive]" }
                }
            }
            $include = parse_SSI($include,$filename);
            $text =~ s/<!--#include virtual=(['"]?)(.*?)\1\s*-->/$include/i;
        } else {
            last;
        }
    }
    return $text;
}
#=====================================================================
#
#    Function: get_page
#    Last modified: 02.09.2004 10:12
#
#=====================================================================

sub get_page {
    
    my ($get_url,$timeout,$login,$password) = @_;

    my $hdrs; my $url; my $req; my $ua; my $resp;
    my $data; my $lastmodified;
    
    my %resp;

    
    if ($get_url eq "") { return undef }
    if ($timeout eq "") { $timeout = 30 }
    
#    $hdrs = new HTTP::Headers(Accept => 'text/html');
    
    $url = new URI::URL($get_url);

    $req = new HTTP::Request("GET", $url, $hdrs);
    $req->authorization_basic($login,$password) if $login;
    $ua = new LWP::UserAgent;
    $ua->agent("RiSpider/1.0");
    $ua->timeout($timeout);
    $ua->proxy('http', $cfg{proxy}) if $cfg{proxy};

    $resp = $ua->request($req);
    
    if ($resp->is_success) {

        $resp{"text"} = $resp->content;
        $resp{"last_modified"} = $resp->last_modified;
        if (! $resp{"last_modified"}) { $resp{"last_modified"} = time }
        $resp{"date"} = $resp{"last_modified"};
        $resp{"code"} = $resp->code;
        $resp{"base"} = $resp->base;
        $resp{"content_type"} = $resp->content_type;
        $resp{"content_encoding"} = $resp->content_encoding;
       
        
    } else {
        $resp{"message"} = $resp->message;
    }
    
    return \%resp;
}
#=====================================================================
#
#    Function: lev_distance_test
#    Last modified: 27.04.2004 16:29
#
#=====================================================================

sub lev_distance_test {
    
    my $min = $_[2];
    my @n; my @m;
    if (abs(length($_[0])-length($_[1]))>$min) { return -1 }
    if (length($_[0]) > length($_[1])) {
        @n = split //, $_[1];
        @m = split //, $_[0];
    } else {
        @n = split //, $_[0];
        @m = split //, $_[1];
    }    
    
    my @d=(0..@m);
    my ($i, $j, $cur, $next);
    for $i (0..$#n){
        $cur=$i+1;
        for $j (0..$#m){
        
            $next = $d[$j+1]+1 < $cur+1 ?
                        ( $d[$j+1]+1 < ($n[$i] ne $m[$j])+$d[$j] ? $d[$j+1]+1 : ($n[$i] ne $m[$j])+$d[$j] ) :
                        ( $cur+1 < ($n[$i] ne $m[$j])+$d[$j] ? $cur+1 : ($n[$i] ne $m[$j])+$d[$j] );
            
            $d[$j]=$cur;
            $cur=$next;
        }
        $d[@m]=$next;
        if ($d[$i+@m-@n+1]>$min) { return -1 }
    }
    return $next;
}
#=====================================================================
#
#    Function: my_die
#    Last modified: 23.08.2004 12:33
#
#=====================================================================

sub my_die {
   my ($str,$line,$file) = @_;
   print "$str\n";
   print "Line: $line\n" if $line;
   print "File: $file\n" if $file;
   die
}
#===================================================================

%html_esc = (
        "&Agrave;" => chr(192),
        "&Aacute;" => chr(193),
        "&Acirc;" => chr(194),
        "&Atilde;" => chr(195),
        "&Auml;" => chr(196),
        "&Aring;" => chr(197),
        "&AElig;" => chr(198),
        "&Ccedil;" => chr(199),
        "&Egrave;" => chr(200),
        "&Eacute;" => chr(201),
        "&Eirc;" => chr(202),
        "&Euml;" => chr(203),
        "&Igrave;" => chr(204),
        "&Iacute;" => chr(205),
        "&Icirc;" => chr(206),
        "&Iuml;" => chr(207),
        "&ETH;" => chr(208),
        "&Ntilde;" => chr(209),
        "&Ograve;" => chr(210),
        "&Oacute;" => chr(211),
        "&Ocirc;" => chr(212),
        "&Otilde;" => chr(213),
        "&Ouml;" => chr(214),
        "&times;" => chr(215),
        "&Oslash;" => chr(216),
        "&Ugrave;" => chr(217),
        "&Uacute;" => chr(218),
        "&Ucirc;" => chr(219),
        "&Uuml;" => chr(220),
        "&Yacute;" => chr(221),
        "&THORN;" => chr(222),
        "&szlig;" => chr(223),
        "&agrave;" => chr(224),
        "&aacute;" => chr(225),
        "&acirc;" => chr(226),
        "&atilde;" => chr(227),
        "&auml;" => chr(228),
        "&aring;" => chr(229),
        "&aelig;" => chr(230),
        "&ccedil;" => chr(231),
        "&egrave;" => chr(232),
        "&eacute;" => chr(233),
        "&ecirc;" => chr(234),
        "&euml;" => chr(235),
        "&igrave;" => chr(236),
        "&iacute;" => chr(237),
        "&icirc;" => chr(238),
        "&iuml;" => chr(239),
        "&eth;" => chr(240),
        "&ntilde;" => chr(241),
        "&ograve;" => chr(242),
        "&oacute;" => chr(243),
        "&ocirc;" => chr(244),
        "&otilde;" => chr(245),
        "&ouml;" => chr(246),
        "&divide;" => chr(247),
        "&oslash;" => chr(248),
        "&ugrave;" => chr(249),
        "&uacute;" => chr(250),
        "&ucirc;" => chr(251),
        "&uuml;" => chr(252),
        "&yacute;" => chr(253),
        "&thorn;" => chr(254),
        "&yuml;" => chr(255),
        "&nbsp;" => " ",
        "&amp;" => "\&",
        "&quote;" => "\"",
        "&laquo;" => "\"",
        "&raquo;" => "\"",
        
    );

#=====================================================================
#
#    Function: urldecode
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub urldecode{    
 my ($val)=@_;  
 $val=~s/\+/ /g;
 $val=~s/%([0-9A-H]{2})/pack('C',hex($1))/ge;
 return $val;
}
#=====================================================================
#
#    Function: urlencode
#    Last modified: 30.09.2003 15:09
#
#=====================================================================

sub urlencode { 
    my ($str) = @_; 
    $str =~ s/([\x00-\x20"#%&+=;<>?{}|\\^~`\[\]\x7F-\xFF])/sprintf("%%%02X", ord($1))/ge; 
    $str =~ s/ /+/g; 
    return $str; 
} 
#=====================================================================
#
#    Function: return_fh
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub return_fh { 
    local *FH;
    return *FH;
}
#===================================================================

sub win2koi {
    my $str = shift;
    $str =~ tr[\xC0-\xFF\xA8\xB8][\xE1\xE2\xF7\xE7\xE4\xE5\xF6\xFA\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF2\xF3\xF4\xF5\xE6\xE8\xE3\xFE\xFB\xFD\xFF\xF9\xF8\xFC\xE0\xF1\xC1\xC2\xD7\xC7\xC4\xC5\xD6\xDA\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD2\xD3\xD4\xD5\xC6\xC8\xC3\xDE\xDB\xDD\xDF\xD9\xD8\xDC\xC0\xD1\xB3\xA3];
    return $str;
}
#===================================================================

sub koi2win {
    my $str = shift;
    $str =~ tr[\xE1\xE2\xF7\xE7\xE4\xE5\xF6\xFA\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF2\xF3\xF4\xF5\xE6\xE8\xE3\xFE\xFB\xFD\xFF\xF9\xF8\xFC\xE0\xF1\xC1\xC2\xD7\xC7\xC4\xC5\xD6\xDA\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD2\xD3\xD4\xD5\xC6\xC8\xC3\xDE\xDB\xDD\xDF\xD9\xD8\xDC\xC0\xD1\xB3\xA3][\xC0-\xFF\xA8\xB8];
    return $str;
}
#=====================================================================
#
#    Function: utf8_to_win1251
#    Converts UTF-8 to windows-1251 (for russian language, should be rewritten)
#    Last modified: 14.01.2004 12:53
#
#=====================================================================

sub utf8_to_win1251 {
    
    my $str = shift;
    
my %chars = (
"\xD0\xB0"=>"�", "\xD0\x90"=>"�",
"\xD0\xB1"=>"�", "\xD0\x91"=>"�",
"\xD0\xB2"=>"�", "\xD0\x92"=>"�",
"\xD0\xB3"=>"�", "\xD0\x93"=>"�",
"\xD0\xB4"=>"�", "\xD0\x94"=>"�",
"\xD0\xB5"=>"�", "\xD0\x95"=>"�",
"\xD1\x91"=>"�", "\xD0\x81"=>"�",
"\xD0\xB6"=>"�", "\xD0\x96"=>"�",
"\xD0\xB7"=>"�", "\xD0\x97"=>"�",
"\xD0\xB8"=>"�", "\xD0\x98"=>"�",
"\xD0\xB9"=>"�", "\xD0\x99"=>"�",
"\xD0\xBA"=>"�", "\xD0\x9A"=>"�",
"\xD0\xBB"=>"�", "\xD0\x9B"=>"�",
"\xD0\xBC"=>"�", "\xD0\x9C"=>"�",
"\xD0\xBD"=>"�", "\xD0\x9D"=>"�",
"\xD0\xBE"=>"�", "\xD0\x9E"=>"�",
"\xD0\xBF"=>"�", "\xD0\x9F"=>"�",
"\xD1\x80"=>"�", "\xD0\xA0"=>"�",
"\xD1\x81"=>"�", "\xD0\xA1"=>"�",
"\xD1\x82"=>"�", "\xD0\xA2"=>"�",
"\xD1\x83"=>"�", "\xD0\xA3"=>"�",
"\xD1\x84"=>"�", "\xD0\xA4"=>"�",
"\xD1\x85"=>"�", "\xD0\xA5"=>"�",
"\xD1\x86"=>"�", "\xD0\xA6"=>"�",
"\xD1\x87"=>"�", "\xD0\xA7"=>"�",
"\xD1\x88"=>"�", "\xD0\xA8"=>"�",
"\xD1\x89"=>"�", "\xD0\xA9"=>"�",
"\xD1\x8A"=>"�", "\xD0\xAA"=>"�",
"\xD1\x8B"=>"�", "\xD0\xAB"=>"�",
"\xD1\x8C"=>"�", "\xD0\xAC"=>"�",
"\xD1\x8D"=>"�", "\xD0\xAD"=>"�",
"\xD1\x8E"=>"�", "\xD0\xAE"=>"�",
"\xD1\x8F"=>"�", "\xD0\xAF"=>"�",
);

    $str =~ s|([\xD0\xD1].)|$chars{$1}|sg;
    
    return $str;
}
#===================================================================


1;

