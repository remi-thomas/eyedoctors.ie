#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::merge_lib;
use lib::common_lib;
use strict;
use riconfig;
use File::Copy;
use vars qw(@ISA @EXPORT);
no warnings;

use Exporter;
@ISA = ('Exporter');
@EXPORT      = qw(
                  &merge
                  );


use vars  qw(
             $db $db_old $db_new $db_cur
             );

#=====================================================================
#
#    Function: merge
#    Last modified: 14.01.2004 22:09
#
#=====================================================================

sub merge {

    my %param = @_;


    $cfg{db} = "." if $cfg{db} eq "";
    if (! defined $param{db}) { 
        $db_cur = $cfg{db}."/db_0";
    } else {
        $db_cur = $cfg{db}."/db_$param{db}";
    }
    $db_old = $db_cur."_old";
    $db_new = $db_cur."_new";  
    $db = $db_cur;  

    
print "DB=$db\n";


    if ( ! -e "$db/$cfg{HASH_INC}") { return }

    if ($cfg{delete_old_database} eq "NO") {
        print "Saving old database... ";
        copy_db($db,$db_old);
        print "Done.\n";
    }
    
    open_db_files();


my $dum;
my $hashwords_pos;
my $word_ind_pos;
my $word_l;
my $word;
my $substring_length;

my $hash_pos = $cfg{HASHSIZE}*4;
my $hash_end = -s HASH_INC;
my $end_hash_fin = -s HASH;
my $end_word_ind_fin = -s WORD_IND;

my $hash_size = $cfg{HASHSIZE}*4;
my $last_rate = 0;


while ($hash_pos < $hash_end) {
    
    seek(HASH_INC,$hash_pos,0);
    read(HASH_INC,$dum,9);
    ($hashwords_pos, $word_ind_pos, $word_l) = unpack("NNC", $dum);
    read(HASH_INC,$word,$word_l);
    $hash_pos += 9+$word_l;
    

    if ($cfg{INDEXING_SCHEME} == 1) {
    	$substring_length = length($word)
    } else {
    	$substring_length = 4
    }
    my $hash_value = &hash(substr($word,0,$substring_length));

    my $doc_all = "";
    my $rat_all = "";
    my $dist_all = "";
    my $attr_all = "";
    my $doc;
    my $rat;
    my $dist;
    my $attr;
    
        while ($word_ind_pos) {
            seek(WORD_IND_INC,$word_ind_pos,0);
            read(WORD_IND_INC,$dum,8);
            $word_ind_pos += 8;
            my ($next_word_ind_pos,$word_ind_length) = unpack("NN",$dum);
            
            read(WORD_IND_INC,$doc,$word_ind_length*$cfg{'index_format_l'});
            $word_ind_pos += $word_ind_length*$cfg{'index_format_l'};
            
            my $sorting_length =  $word_ind_length * $cfg{sformat_rating_length};
            if ($cfg{allow_sort_by_rating}) {
                read(WORD_IND_INC,$rat,$sorting_length);
                $word_ind_pos += $sorting_length;
            }

            if ($cfg{word_dist}) {
                seek(WORD_IND_INC,$word_ind_pos,0);
                read(WORD_IND_INC,$dist,$word_ind_length*4);
                $word_ind_pos += $word_ind_length*4;
            }


            if (scalar(@attr_def)) {
                seek(WORD_IND_INC,$word_ind_pos,0);
                read(WORD_IND_INC,$attr,$word_ind_length*$cfg{attr_l});
            }

            $doc_all .= $doc;
            $rat_all .= $rat;
            $dist_all .= $dist;
            $attr_all .= $attr;

            $word_ind_pos = $next_word_ind_pos;
        }


    

        my $hash_pos_fin;
        seek(HASH,$hash_value*4,0) or my_die("Can't SEEK HASH: $!\n");
        read(HASH,$hash_pos_fin,4);
        $hash_pos_fin = unpack("N",$hash_pos_fin);
        if ($hash_pos_fin == 0) {
            seek(HASH,$hash_value*4,0);
            print HASH pack("N",$end_hash_fin);
            seek(HASH,$end_hash_fin,0);
            print HASH pack("NNC",0,$end_word_ind_fin,$word_l).$word;
            seek(WORD_IND,$end_word_ind_fin,0);
            print WORD_IND pack("NN",0,length($doc_all)/$cfg{'index_format_l'});
            print WORD_IND $doc_all,$rat_all,$dist_all,$attr_all;
            $end_word_ind_fin += 8+length($doc_all)+length($rat_all)+length($dist_all)+length($attr_all);
            $end_hash_fin += 9+$word_l;
            
            
        } else {
            my $add_word = 0;
            my $last_hash_pos_fin;
            my $last_word_ind_pos_fin;
            while($hash_pos_fin) {
                my $dum;
                seek(HASH,$hash_pos_fin,0);
                read(HASH,$dum,9);
                my($next_hash_pos_fin, $word_ind_pos_fin, $word_l_fin) = unpack("NNC",$dum);
                my $word_fin;
                read(HASH,$word_fin,$word_l_fin);
                if ($word eq $word_fin) {
                    seek(HASH,$hash_pos_fin+4,0);
                    print HASH pack("N",$end_word_ind_fin);
                    seek(WORD_IND,$end_word_ind_fin,0);
                    print WORD_IND pack("NN",$word_ind_pos_fin,length($doc_all)/$cfg{'index_format_l'});
                    print WORD_IND $doc_all,$rat_all,$dist_all,$attr_all;
                    $end_word_ind_fin += 8+length($doc_all)+length($rat_all)+length($dist_all)+length($attr_all);
                    $add_word = 1;
                    last;
                }
                $last_hash_pos_fin = $hash_pos_fin;
                $hash_pos_fin = $next_hash_pos_fin;
            }
            if ($add_word == 0) {
                seek(WORD_IND,$end_word_ind_fin,0);
                print WORD_IND pack("NN",0,length($doc_all)/$cfg{'index_format_l'});
                print WORD_IND $doc_all,$rat_all,$dist_all,$attr_all;
                seek(HASH,$last_hash_pos_fin,0);
                print HASH pack("N",$end_hash_fin);
                seek(HASH,$end_hash_fin,0);
                print HASH pack("NNC",0,$end_word_ind_fin,$word_l).$word;
                $end_word_ind_fin += 8+length($doc_all)+length($rat_all)+length($dist_all)+length($attr_all);
                $end_hash_fin += 9+$word_l;
            }
        }


        if (1) {
            my $rate = int (($hash_pos-$hash_size)/($hash_end-$hash_size)*100);
            if ($rate %10 == 0 && $rate != $last_rate) { print "$rate %\n" }
            $last_rate = $rate;
        }

    
    
}

        close_db_files();
        delete_files($db);

}
#=====================================================================
#
#    Function: open_db_files
#    Last modified: 14.01.2004 21:21
#
#=====================================================================

sub open_db_files {
    
    if ( ! -e "$db/$cfg{HASH}") {
            
        open HASH, ">$db/$cfg{HASH}" or &my_die("Died: could not open $db/$cfg{HASH} - $!",__LINE__,__FILE__);
        binmode(HASH);
        my $hash = "\x00"; $hash x= ($cfg{HASHSIZE}*4);
        print HASH $hash;
        close(HASH);
    
        open WORD_IND, ">$db/$cfg{WORD_IND}" or &my_die("Died: could not open $db/$cfg{WORD_IND} - $!",__LINE__,__FILE__);
        binmode(WORD_IND);
        print WORD_IND pack("N",0);
        close(WORD_IND);
    }


    
open HASH, "+<$db/$cfg{HASH}" or &my_die("Died: could not open $db/$cfg{HASH} - $!",__LINE__,__FILE__);
binmode(HASH);
open WORD_IND, "+<$db/$cfg{WORD_IND}" or &my_die("Died: could not open $db/$cfg{WORD_IND} - $!",__LINE__,__FILE__);
binmode(WORD_IND);

        open HASH_INC, "$db/$cfg{HASH_INC}" or &my_die("Died: could not open $db/$cfg{HASH_INC} - $!",__LINE__,__FILE__);
        binmode(HASH_INC);

        open WORD_IND_INC, "$db/$cfg{WORD_IND_INC}" or &my_die("Died: could not open $db/$cfg{WORD_IND_INC} - $!",__LINE__,__FILE__);
        binmode(WORD_IND_INC);


}
#=====================================================================
#
#    Function: close_db_files
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub close_db_files {
    
    close(HASH);
    close(WORD_IND);

    close(HASH_INC);
    close(WORD_IND_INC);

    close(HASH_NEW);
    close(WORD_IND_NEW);
    
}
#=====================================================================
#
#    Function: copy_db
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub copy_db {
    my ($db, $db_old) = @_;
    
    if (! -e $db) { print "Directory '$db' does not exists\n"; }
    if (! -e $db_old) { 
        print "Directory '$db_old' does not exists\n";
        mkdir("$db_old",0777) or &my_die("Died: could not create directory '$db_old' - $!",__LINE__,__FILE__);
        print "Directory '$db_old' created\n";
    }
    
        opendir(DIR,$db_old) or (print "Cannot open $db_old: $!\n");
        my @files = grep {!(/^\./) &&  -f "$db_old/$_"} readdir(DIR);
        foreach my $file (@files) {
            unlink("$db_old/$file") or print "Can't delete old databases file - $!\n";
        }

        opendir(DIR,$db) or (print "Cannot open $db: $!\n");
        my @files = grep {!(/^\./) &&  -f "$db/$_"} readdir(DIR);
        foreach my $file (@files) {
            copy("$db/$file","$db_old/$file");
        }
    
    
}
#=====================================================================
#
#    Function: delete_files
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub delete_files {
    
    my $db = shift;
    
    unlink("$db/$cfg{HASH_INC}") or print "Can't delete database file - $!\n";
    unlink("$db/$cfg{WORD_IND_INC}") or print "Can't delete database file - $!\n";
    
}
#===================================================================


1;
