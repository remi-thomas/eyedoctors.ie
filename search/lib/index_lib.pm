#
#           RiSearch Pro
#
# web search engine, version 3.2.08
# (c) Sergej Tarasov, 2000-2004
#
# Homepage: http://risearch.org/
# email: risearch@risearch.org

package lib::index_lib;
use lib::common_lib;
use strict;
use riconfig;
use vars qw(@ISA @EXPORT);
no warnings;


use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&start &finish &parse_file
             &index_file_MEM
             &add_substring_index
             &check_url_HD &delete_url
             
             $cfn $cwn $kbcount
             );

use vars  qw(%words %seen %rating %dist_pos %dist %attr @attr_data @word_ind
             $cfn $cwn $kbcount
             $hash $end_hash $end_hashwords $word_ind $end_word_ind $to_print_word_ind_base
             $sitewords $end_sitewords $hashwords $end_hashwords
             $bold_text $heading_text $italic_text $link_text $title $keywords $description $max_term
             $hash_url $hash_url_ind
             $db $db_cur $db_old $db_new
             $starttime
             $tmp_hash $tmp_hash_end
             );

$cfn = 0;
$cwn = 0;
$kbcount = 0;

%words = ();
%seen = ();
%rating = ();
%dist_pos = ();
%dist = ();
%attr = ();
@attr_data = ();


$title = "";
$bold_text = "";
$heading_text = "";
$italic_text = "";
$link_text = "";
$keywords = "";
$description = "";
$max_term = 0;

$hash = "";
$end_hash = "";
$end_hashwords = 0;
$word_ind = "";
@word_ind = ();
$end_word_ind = 0;
$to_print_word_ind_base = "";
$sitewords = "";
$end_sitewords = 0;
$hashwords = "";
$end_hashwords = 0;
$hash_url = "";
$hash_url_ind = "";

my $dist_pos = 0;
my $index_written = 0;
my $end_hash_fin = 0;
my $end_word_ind_fin = 0;
my $action = "";
my $hash_url_ind_end = 0;

    my $fdate; my $fsize;
    my @zone_vec = ();
    for (my $i=1; $i<scalar(@zone); $i++) {
    	$zone_vec[$i] = "";
    }


my $code = "\${\$_[0]} =~ tr/-a-zA-Z$cfg{CAP_LETTERS}$cfg{LOW_LETTERS}$cfg{numbers}/ /cs;";
my $remove_non_alphabetic = eval "sub { $code }";

$code = "\${\$_[0]} =~ tr/A-Z$cfg{CAP_LETTERS}/a-z$cfg{LOW_LETTERS}/;";
my $to_lower_case = eval "sub { $code }";


#=====================================================================
#
#    Function: start
#    Last modified: 04.11.2003 12:32
#
#=====================================================================

sub start {

    my %param = @_;


    $cfg{db} = "." if $cfg{db} eq "";
    if (! defined $param{db}) { 
        $db_cur = $cfg{db}."/db_0";
    } else {
        $db_cur = $cfg{db}."/db_$param{db}";
    }
    $db_old = $db_cur."_old";
    $db_new = $db_cur."_new";  
    $db = $db_cur; 
    $cfg{db_cur} = $db_cur;
    $cfg{db_old} = $db_old;
    $cfg{db_new} = $db_new;
    
    unless ( -e "$db_cur" && -d "$db_cur" ) {
        mkdir("$db_cur",0777) or &my_die("Died: could not create directory '$db_cur' - $!",__LINE__,__FILE__);
        print "Directory '$db_cur' created\n";
    }

    if ($param{'action'} eq "index") {

        
        if ($cfg{create_new_database} eq "YES" && ! -e "$db_new") {
            mkdir("$db_new",0777) or &my_die("Died: could not create directory '$db_new' - $!",__LINE__,__FILE__);
            print "Directory '$db_new' created\n";
        }        
        
        if ($cfg{delete_old_database} eq "NO" && ! -e "$db_old") {
            mkdir("$db_old",0777) or &my_die("Died: could not create directory '$db_old' - $!",__LINE__,__FILE__);
            print "Directory '$db_old' created\n";
        }  
    
    }      

    
    if ($param{'action'} eq "index") {
        if ($cfg{create_new_database} eq "YES") { $db = $db_new }
        create_db_files_MEM();
        $cfn = 0;
        $action = "index";
    } elsif ($param{'action'} eq "add_page" or $param{'action'} eq "update" or $param{'action'} eq "restart") {
        create_db_files_HD();
        $cfn = ((stat(FINFO_IND))[7])/4;
        $action = "add";
    }
    
    $starttime = time;
    


}
#=====================================================================
#
#    Function: finish
#    Last modified: 10.09.2003 13:49
#
#=====================================================================

sub finish {
    
    my %param = @_;

    if ($param{'action'} eq "index") {
        if (! $index_written) {
            print "\nWritting index to disk\n\n";
            write_hash_MEM();
            $index_written = 1;
        } else {
            print "\nWritting index to disk\n\n";
            add_hash_MEM();
        }
        close_db_files();
    } elsif ($param{'action'} eq "add_page" or $param{'action'} eq "update" or $param{'action'} eq "restart") {
        if (! $index_written) {
            print "\nWritting index to disk\n\n";
            write_hash_MEM();
            $index_written = 1;
        } else {
            print "\nWritting index to disk\n\n";
            add_hash_MEM();
        }
        close_db_files_HD();
    }

    print "Finished\n";
    
}
#=====================================================================
#
#    Function: prep_data_1
#    Last modified: 08.12.2003 12:10
#
#=====================================================================

sub prep_data_1 {
    my ($html_text,$weight,$fill_dist,$attr_num) = @_;

    my $item; 
    my $count = 0;
    

    &$remove_non_alphabetic($html_text);
    if (length($$html_text) > $cfg{max_doc_size}) { substr($$html_text,$cfg{max_doc_size}) = "" }
    &$to_lower_case($html_text);
    my $wwd = join " ", ($$html_text =~ m/([^- ]+-[^ ]*[^- ])/gs);
    $$html_text =~ tr/-/ /;
    $$html_text .= " ".$wwd;

    foreach $item (split (/\s+/,$$html_text)) {
        
    	if (length($item) < $cfg{min_length} && ! exists $index_words{$item}) { next }

    	if (length($item) > $cfg{max_length}) {
    	    $item = substr($item,0,$cfg{max_length});
        }
    	if (exists($stop_words{$item})) { next } 
        $seen{$item} += $weight;
        if ($seen{$item} > $max_term) { $max_term = $seen{$item} }
        
        if ($cfg{word_dist} && $fill_dist) { $dist{$item} .= pack("n",$count); $count++; }

        if (defined $attr_num) {
            if (! exists $seen{$item}) {
                $attr{$item} = "\x00";
                vec($attr{$item},$attr_num,1) = 1;
            } else {
                vec($attr{$item},$attr_num,1) = 1;
            }
        }

         
    };
    
        
}

#=====================================================================
#
#    Function: prep_data_0
#    Last modified: 08.12.2003 12:10
#
#=====================================================================

sub prep_data_0 {
    my ($html_text,$weight,$fill_dist,$attr_num) = @_;

    my $item; 
    my $pos = 0;
    my $new_pos;
    my $count = 0;


    &$remove_non_alphabetic($html_text);
    if (length($$html_text) > $cfg{max_doc_size}) { substr($$html_text,$cfg{max_doc_size}) = "" }
    &$to_lower_case($html_text);
    my $wwd = join " ", ($$html_text =~ m/([^- ]+-[^ ]*[^- ])/gs);
    $$html_text =~ tr/-/ /;
    $$html_text .= " ".$wwd;
    $$html_text =~ s/\s+/ /gs;

    
  
    do {{
        
        $new_pos = index($$html_text," ",$pos);
        if ($new_pos == -1) {
            $item = substr($$html_text,$pos);
        } else {
            $item = substr($$html_text,$pos,$new_pos-$pos);
        }
        $pos = $new_pos+1;
 
      	if (length($item) < $cfg{min_length} && ! exists $index_words{$item}) { next }
    	if (length($item) > $cfg{max_length}) {
    	    $item = substr($item,0,$cfg{max_length});
        }
    	if (exists($stop_words{$item})) { next } 
    	
    	my $hash_val = hash($item) % 10001;
    	my $dum = unpack("N", substr($tmp_hash,$hash_val*4,4));
    	
    	if ($dum == 0) {
    	    substr($tmp_hash,$hash_val*4,4) = pack("N", $tmp_hash_end);
    	    substr($tmp_hash,$tmp_hash_end,6+length($item)) = pack("NCC",0,$weight,length($item)).$item;
    	    $tmp_hash_end += 6+length($item);
    	} else {
    	    my $add_word = 0;
    	    my $last_indpos;
    	    while ($dum) {
    	        my ($ind_pos,$terms,$len) = unpack("NCC",substr($tmp_hash,$dum,6));
    	        my $old_word = substr($tmp_hash,$dum+6,$len);
    	        if ($item eq $old_word) {
    	            substr($tmp_hash,$dum,6) = pack("NCC",$ind_pos,$terms+$weight,$len);
    	            $add_word = 1;
    	            if ($max_term < $terms+$weight) { $max_term = $terms+$weight }
    	            last;
    	        }
    	        $last_indpos = $dum;
    	        $dum = $ind_pos;
    	    }
    	    if ($add_word == 0) {
    	        substr($tmp_hash,$last_indpos,4) = pack("N", $tmp_hash_end);
    	        substr($tmp_hash,$tmp_hash_end,6+length($item)) = pack("NCC",0,$weight,length($item)).$item;
    	        $tmp_hash_end += 6+length($item);
    	    }
    	}
    	
 
        if ($cfg{word_dist} && $fill_dist) { $dist{$item} .= pack("n",$count); $count++; }

        if (defined $attr_num) {
            if (! exists $seen{$item}) {
                $attr{$item} = "";
                vec($attr{$item},$attr_num,1) = 1;
            } else {
                vec($attr{$item},$attr_num,1) = 1;
            }
        }
    
    }}   until ($new_pos == -1);
   
    
    
        
}

#=====================================================================
#
#    Function: index_file_MEM
#    Last modified: 21.12.2004 16:13
#
#=====================================================================

sub index_file_MEM {

    my $data = shift;
 
#    my ($html_text, $url, $TITLE, $descript, $date, $size, $type, $filename) = @_;


    
    my ($pos, $item, $key); 
    
    $data->{size} = length($data->{text}) if !$data->{size};
    $kbcount += $data->{size};
    $data->{chsum} = unpack("%32C*",$data->{text});
    

    my $attr_ind = "";
    my $attr_des = "";
    my @attr = ();
    if (scalar @attr_def) {
        
        @attr = get_attribute($data);
        
        for (my $i=0; $i < scalar(@attr_def); $i++) {
            

            #   attribute can be printed in results output
            if (("$attr_conf[$i]" & "\x02") ne "\x00") {
                $data->{attr_des} .= "::".$i."::".$attr[$i];
            }
            #   integer attribute
            if (("$attr_conf[$i]" & "\x04") ne "\x00") {
                my $data = defined $attr[$i]?$attr[$i]:"";
                $attr_data[$i] .= pack("$cfg{int_attr_format}",$data);
            }
            #   float attribute
            if (("$attr_conf[$i]" & "\x08") ne "\x00") {
                my $data = defined $attr[$i]?$attr[$i]:"";
                $attr_data[$i] .= pack("$cfg{float_attr_format}",$data);
            }
            #   date attribute
            if (("$attr_conf[$i]" & "\x10") ne "\x00") {
                my $data = defined $attr[$i]?$attr[$i]:"";
                if (${$attr_def[$i]}[0] ne "DATE") {
                    $data = convert_date($data,$cfg{date_format}) if $data;
                }
                $attr_data[$i] .= pack("l",$data);
            }
        
        }

    }
 
    
    if ($data->{type} =~ m|htm| || $data->{type} =~ m|$cfg{ext_parser_ext_html_reg}|io) {
        my ($ttTITLE, $ttdescript) = parse_html(\$data->{text});
        $data->{TITLE} = $data->{TITLE}?$data->{TITLE}:$ttTITLE;
        $data->{descript} = $data->{descript}?$data->{descript}:$ttdescript;
    }
    if ($data->{TITLE} eq "") { $data->{TITLE} = "No title" }
    if ($data->{descript} eq "") { $data->{descript} = substr($data->{text},0,$cfg{stored_descr_size}) }
    if ($cfg{del_descr_chars}) {
        $data->{descript} =~ s|[$cfg{del_descr_chars}]||gs;
    }
    
    $data->{TITLE} =~ s/:+/:/g;
    $data->{descript} =~ s/:+/:/g;
    $data->{TITLE} =~ s/\s+/ /g;
    $data->{descript} =~ s/\s+/ /g;

    my ($DAY, $MONTH, $YEAR) = (localtime($data->{date}))[3,4,5];
    $data->{dmdate} = "";
    if ($cfg{date_format} eq "dm") {
        $data->{dmdate} = sprintf "%02d.%02d.%04d", $DAY, $MONTH+1, $YEAR+1900;
    } else {
        $data->{dmdate} = sprintf "%02d.%02d.%04d", $MONTH+1, $DAY, $YEAR+1900;
    }        

    $pos = tell(FINFO);
    add_finfo($data, $pos);

    if ($action eq "index") {
        add_url($data->{url},$cfn);
    } elsif ($action eq "add") {
        add_url_HD($data->{url},$cfn);
    }

    if ($cfg{allow_sort_by_date}) {
    	$fdate .= pack($cfg{sformat_date},$data->{date});
    }
    if ($cfg{allow_sort_by_size}) {
    	$fsize .= pack($cfg{sformat_size},$data->{size});
    }

    if ($action eq "index") {
        check_zones($data->{url},$cfn);
    } elsif ($action eq "add") {
        check_zones_HD($data->{url},$cfn);
    }
    
    if ($cfg{index_url} eq "YES") { $data->{text} .= " $data->{url}" }
    
    
    if ($cfg{indexing_speed} == 0) {
        $max_term = 1;
        %dist = (); 
        %attr = ();
        $tmp_hash = "\x00";
        $tmp_hash x= 1*length($data->{text}) + 40004;
        $tmp_hash_end = 40004;
 
        prep_data_0(\$data->{text},1,1);
        add_terms_0() if ( $cfg{allow_sort_by_rating} && $data->{type} =~ m|htm| );
        if (scalar @attr_def) {
            for (my $i=0; $i < scalar(@attr_conf); $i++) {
                #   attribute will be indexed
                if (("$attr_conf[$i]" & "\x01") ne "\x00") {
                    prep_data_0(\$attr[$i],$attr_weight[$i],0,$i);
                }
            }
        }
        build_hash_MEM_0($cfn);
 
        undef($tmp_hash);
    } else {
        %seen = (); $max_term = 1;
        %dist = (); 
        %attr = ();
 
        prep_data_1(\$data->{text},1,1);
        add_terms_1() if ( $cfg{allow_sort_by_rating} && $data->{type} =~ m|htm| );
        if (scalar @attr_def) {
            for (my $i=0; $i < scalar(@attr_conf); $i++) {
                #   attribute will be indexed
                if (("$attr_conf[$i]" & "\x01") ne "\x00") {
                    prep_data_1(\$attr[$i],$attr_weight[$i],0,$i);
                }
            }
        }
        build_hash_MEM_1($cfn);
 
        undef(%seen);
    }

    
    undef($data);
    undef(%dist);

    
    $cfn++;
    $cfg{cfn} = $cfn;

    
}
#=====================================================================
#
#    Function: add_finfo
#    Last modified: 09.02.2004 17:59
#
#=====================================================================

sub add_finfo {
    
    my ($data, $pos) = @_;
    print FINFO $data->{url},"::",$data->{size},"::",$data->{dmdate},"::",$data->{chsum},"::",$data->{TITLE},"::",$data->{descript},$data->{attr_des},"\x0A";
    print FINFO_IND pack("N", $pos);
}
#=====================================================================
#
#    Function: build_hash_MEM_1
#    Last modified: 28.08.2003 14:37
#
#=====================================================================

sub build_hash_MEM_1 {

    (my $pos) = @_;

my $key;
my $value;
while(($key, $value) = each(%seen)) { 

    my $word = $key;
    my $rating = int( (0.5+0.5*$value/$max_term)*255 ) if $cfg{allow_sort_by_rating};
    my $dist = $dist_pos;


    if ($cfg{word_dist}) {
        print DIST pack("n",length($dist{$key})).$dist{$key};
        $dist_pos += 2+length($dist{$key});
        
    }
    


    my $hash_value = "";
    my $add_word = 0;
    my $to_print = "";
    my $last_hash_pos = 0;

    $to_print = pack($cfg{'index_format'},$pos);
    $to_print .= pack("C", $rating) if $cfg{allow_sort_by_rating};
    $to_print .= pack("N", $dist) if $cfg{'word_dist'};
    $to_print .= $attr{$key} | $cfg{attr} if @attr_def;
   
    
    my $w_ind = int($end_word_ind/2000000);
    my $w_pos = $end_word_ind % 2000000;

    if ($cfg{INDEXING_SCHEME} == 1) { $hash_value = &hash($word) % $cfg{HASHSIZE}; }
    if ($cfg{INDEXING_SCHEME} == 2) { $hash_value = &hash(substr($word,0,4)) % $cfg{HASHSIZE}; }
    
    my $hash_pos = unpack("N",substr($hash,$hash_value*4,4));
    
    if ($hash_pos == 0) {
                
        substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",0).$to_print;
        substr($hash,$end_hash,9+length($word)) = pack("NNC",0,$end_word_ind,length($word)).$word;
        substr($hash,$hash_value*4,4) = pack("N",$end_hash);
        $end_word_ind += 4+length($to_print);
        $end_hash += 9+length($word);
        $cwn++;

    } else {
        $add_word = 0;
        while ($hash_pos) {
            
            my($next_hash_pos, $word_ind_pos, $word_l) = unpack("NNC",substr($hash,$hash_pos,9));
            if ($word eq substr($hash,$hash_pos+9,$word_l)) {

                substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",$word_ind_pos).$to_print;
                substr($hash,$hash_pos+4,4) = pack("N",$end_word_ind);
                $end_word_ind += 4+length($to_print);
    	        $add_word = 1;
    	        last;
    	    }
    	    $last_hash_pos = $hash_pos;
    	    $hash_pos = $next_hash_pos;
        }
        if ($add_word == 0) {
            substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",0).$to_print;
            substr($hash,$end_hash,9+length($word)) = pack("NNC",0,$end_word_ind,length($word)).$word;
            substr($hash,$last_hash_pos,4) = pack("N",$end_hash);    
            $end_word_ind += 4+length($to_print);
            $end_hash += 9+length($word);
            $cwn++;
        }
    }
    if (length($hash) - $end_hash < 1000) {

    	$hash .= "\x00"x1000000;


    	print "Allocating new block - HASH - ",length($hash),"\n" if $cfg{verbose_output};
    }
    if (2000000 - $w_pos < 1000) {
    	$word_ind[$w_ind+1] = "\x00"; $word_ind[$w_ind+1] x= 2000000;
    	$end_word_ind = ($w_ind+1)*2000000;
    	print "Allocating new block - WORD_IND - ",$end_word_ind,"\n" if $cfg{verbose_output};
    }
    

}


    if ($end_word_ind + $end_hash > $cfg{'temp_db_size'}) {
        if (! $index_written) {
            print "\nWritting index to disk\n\n";
            write_hash_MEM();
            $index_written = 1;
        } else {
            print "\nWritting index to disk\n\n";
            add_hash_MEM();
            $end_word_ind = 0;
        }
        undef $hash;
        undef @word_ind;
        $hash = "\x00"; $hash x= ($cfg{HASHSIZE}*4 + 1000000);
        $end_hash = $cfg{HASHSIZE}*4;
        @word_ind = ();
        $word_ind[0] = "\x00"; $word_ind[0] x= 2000000;
        $end_word_ind = 4;
    }
        

};    
#=====================================================================
#
#    Function: build_hash_MEM_0
#    Last modified: 28.08.2003 14:37
#
#=====================================================================

sub build_hash_MEM_0 {
    (my $pos) = @_;

my $key;
my $value;



for (my $i = 0; $i < 10001; $i++) {
            
    my $tmp_hash_pos = unpack("N",substr($tmp_hash,$i*4,4));
    while ($tmp_hash_pos) {
        my ($ind_pos,$terms,$len) = unpack("NCC",substr($tmp_hash,$tmp_hash_pos,6));
        $key = substr($tmp_hash,$tmp_hash_pos+6,$len);
        $value = $terms;
        $tmp_hash_pos = $ind_pos;



    my $word = $key;
    my $rating = int( (0.5+0.5*$value/$max_term)*255 ) if $cfg{allow_sort_by_rating};
    my $dist = $dist_pos;

    if ($cfg{word_dist}) {
        print DIST pack("n",length($dist{$key})).$dist{$key};
        $dist_pos += 2+length($dist{$key});
    }
    

    my $hash_value = "";
    my $add_word = 0;
    my $to_print = "";
    my $last_hash_pos = 0;

    $to_print = pack($cfg{'index_format'},$pos);
    $to_print .= pack("C", $rating) if $cfg{allow_sort_by_rating};
    $to_print .= pack("N", $dist) if $cfg{'word_dist'};
    $to_print .= $attr{$key} | $cfg{attr} if @attr_def;
    my $w_ind = int($end_word_ind/2000000);
    my $w_pos = $end_word_ind % 2000000;

    if ($cfg{INDEXING_SCHEME} == 1) { $hash_value = &hash($word) % $cfg{HASHSIZE}; }
    if ($cfg{INDEXING_SCHEME} == 2) { $hash_value = &hash(substr($word,0,4)) % $cfg{HASHSIZE}; }
    
    my $hash_pos = unpack("N",substr($hash,$hash_value*4,4));
    
    if ($hash_pos == 0) {
                
        substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",0).$to_print;
        substr($hash,$end_hash,9+length($word)) = pack("NNC",0,$end_word_ind,length($word)).$word;
        substr($hash,$hash_value*4,4) = pack("N",$end_hash);
        $end_word_ind += 4+length($to_print);
        $end_hash += 9+length($word);
        $cwn++;

    } else {
        $add_word = 0;
        while ($hash_pos) {
            
            my($next_hash_pos, $word_ind_pos, $word_l) = unpack("NNC",substr($hash,$hash_pos,9));
            if ($word eq substr($hash,$hash_pos+9,$word_l)) {

                substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",$word_ind_pos).$to_print;
                substr($hash,$hash_pos+4,4) = pack("N",$end_word_ind);
                $end_word_ind += 4+length($to_print);
    	        $add_word = 1;
    	        last;
    	    }
    	    $last_hash_pos = $hash_pos;
    	    $hash_pos = $next_hash_pos;
        }
        if ($add_word == 0) {
            substr($word_ind[$w_ind],$w_pos,4+length($to_print)) = pack("N",0).$to_print;
            substr($hash,$end_hash,9+length($word)) = pack("NNC",0,$end_word_ind,length($word)).$word;
            substr($hash,$last_hash_pos,4) = pack("N",$end_hash);    
            $end_word_ind += 4+length($to_print);
            $end_hash += 9+length($word);
            $cwn++;
        }
    }
    if (length($hash) - $end_hash < 1000) {

    	$hash .= "\x00"x1000000;


    	print "Allocating new block - HASH - ",length($hash),"\n" if $cfg{verbose_output};
    }
    if (2000000 - $w_pos < 1000) {
    	$word_ind[$w_ind+1] = "\x00"; $word_ind[$w_ind+1] x= 2000000;
    	$end_word_ind = ($w_ind+1)*2000000;
    	print "Allocating new block - WORD_IND - ",$end_word_ind,"\n" if $cfg{verbose_output};
    }
    
    
    }  # while ($hash_pos)

}


    if ($end_word_ind + $end_hash > $cfg{'temp_db_size'}) {
        if (! $index_written) {
            print "\nWritting index to disk\n\n";
            write_hash_MEM();
            $index_written = 1;
        } else {
            print "\nWritting index to disk\n\n";
            add_hash_MEM();
            $end_word_ind = 0;
        }
        undef $hash;
        undef @word_ind;
        $hash = "\x00"; $hash x= ($cfg{HASHSIZE}*4 + 1000000);
        $end_hash = $cfg{HASHSIZE}*4;
        @word_ind = ();
        $word_ind[0] = "\x00"; $word_ind[0] x= 2000000;
        $end_word_ind = 4;
    }
        

};    
#=====================================================================
#
#    Function: write_hash_MEM
#    Last modified: 28.08.2003 14:38
#
#=====================================================================

sub write_hash_MEM {
    
    my $hash_fin = "\x00"; $hash_fin x= $end_hash;
    $end_hash_fin = $cfg{HASHSIZE}*4;
    $end_word_ind_fin = 4;
    
    for (my $i = 0; $i < $cfg{HASHSIZE}; $i++) {
        
        my $hash_pos = unpack("N",substr($hash,$i*4,4));
        if ($hash_pos != 0) {

            substr($hash_fin,$i*4,4) = pack("N",$end_hash_fin);
            my $prev_hash_pos_fin = 0;

            while ($hash_pos) {
                my($next_hash_pos, $word_ind_pos, $word_l) = unpack("NNC",substr($hash,$hash_pos,9));
                my $word = substr($hash,$hash_pos+9,$word_l);
                my $doc = "";
                my $rat = "";
                my $dist = "";
                my $attr = "";
                while ($word_ind_pos) {
                    my $w_ind = int($word_ind_pos/2000000);
                    my $w_pos = $word_ind_pos % 2000000;
                    my $next_word_ind_pos = unpack("N",substr($word_ind[$w_ind],$w_pos,4));
                    $w_pos += 4;
                    $doc .= substr($word_ind[$w_ind],$w_pos,$cfg{'index_format_l'});
                    $w_pos += $cfg{'index_format_l'};
                    if ($cfg{allow_sort_by_rating}) {
                        $rat .= substr($word_ind[$w_ind],$w_pos,1);
                        $w_pos += 1;
                    }
                    if ($cfg{'word_dist'}) {
                        $dist .= substr($word_ind[$w_ind],$w_pos,4);
                        $w_pos += 4;
                    }
                    if (scalar @attr_def) {
                        $attr .= substr($word_ind[$w_ind],$w_pos,$cfg{attr_l});
                    }
                    $word_ind_pos = $next_word_ind_pos;
                }
                print WORD_IND pack("NN",0,length($doc)/$cfg{'index_format_l'});
                print WORD_IND $doc,$rat,$dist,$attr;
                substr($hash_fin,$end_hash_fin,9+$word_l) = pack("NNC",0,$end_word_ind_fin,$word_l).$word;
                $end_word_ind_fin += 8+length($doc)+length($rat)+length($dist)+length($attr);
                if ($prev_hash_pos_fin) {
                    substr($hash_fin,$prev_hash_pos_fin,4) = pack("N",$end_hash_fin);
                }
                $prev_hash_pos_fin = $end_hash_fin;
                $end_hash_fin += 9+$word_l;
                $hash_pos = $next_hash_pos;
            }
        }
    }
    
    print HASH substr($hash_fin,0,$end_hash_fin);
                    

    
}
#=====================================================================
#
#    Function: add_hash_MEM
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub add_hash_MEM {

    
    for (my $i = 0; $i < $cfg{HASHSIZE}; $i++) {
        
        
        my $hash_pos = unpack("N",substr($hash,$i*4,4));
        if ($hash_pos != 0) {
            
            while ($hash_pos) {
                my($next_hash_pos, $word_ind_pos, $word_l) = unpack("NNC",substr($hash,$hash_pos,9));
                my $word = substr($hash,$hash_pos+9,$word_l);
                my $doc = "";
                my $rat = "";
                my $dist = "";
                my $attr = "";
                while ($word_ind_pos) {
                    my $w_ind = int($word_ind_pos/2000000);
                    my $w_pos = $word_ind_pos % 2000000;
                    my $next_word_ind_pos = unpack("N",substr($word_ind[$w_ind],$w_pos,4));
                    $w_pos += 4;
                    $doc .= substr($word_ind[$w_ind],$w_pos,$cfg{'index_format_l'});
                    $w_pos += $cfg{'index_format_l'};
                    if ($cfg{allow_sort_by_rating}) {
                        $rat .= substr($word_ind[$w_ind],$w_pos,1);
                        $w_pos += 1;
                    }
                    if ($cfg{'word_dist'}) {
                        $dist .= substr($word_ind[$w_ind],$w_pos,4);
                        $w_pos += 4;
                    }
                    if (scalar @attr_def) {
                        $attr .= substr($word_ind[$w_ind],$w_pos,$cfg{attr_l});
                    }
                    $word_ind_pos = $next_word_ind_pos;
                }
                my $hash_pos_fin;
                seek(HASH,$i*4,0);
                read(HASH,$hash_pos_fin,4);
                $hash_pos_fin = unpack("N",$hash_pos_fin);
                if ($hash_pos_fin == 0) {
                    seek(HASH,$i*4,0);
                    print HASH pack("N",$end_hash_fin);
                    seek(HASH,$end_hash_fin,0);
                    print HASH pack("NNC",0,$end_word_ind_fin,$word_l).$word;
                    seek(WORD_IND,$end_word_ind_fin,0);
                    print WORD_IND pack("NN",0,length($doc)/$cfg{'index_format_l'});
                    print WORD_IND $doc,$rat,$dist,$attr;
                    $end_word_ind_fin += 8+length($doc)+length($rat)+length($dist)+length($attr);
                    $end_hash_fin += 9+$word_l;
                    
                    
                } else {
                    my $add_word = 0;
                    my $last_hash_pos_fin;
                    my $last_word_ind_pos_fin;
                    while($hash_pos_fin) {
                        my $dum;
                        seek(HASH,$hash_pos_fin,0);
                        read(HASH,$dum,9);
                        my($next_hash_pos_fin, $word_ind_pos_fin, $word_l_fin) = unpack("NNC",$dum);
                        my $word_fin;
                        read(HASH,$word_fin,$word_l_fin);
                        if ($word eq $word_fin) {
                            seek(HASH,$hash_pos_fin+4,0);
                            print HASH pack("N",$end_word_ind_fin);
                            seek(WORD_IND,$end_word_ind_fin,0);
                            print WORD_IND pack("NN",$word_ind_pos_fin,length($doc)/$cfg{'index_format_l'});
                            print WORD_IND $doc,$rat,$dist,$attr;
                            $end_word_ind_fin += 8+length($doc)+length($rat)+length($dist)+length($attr);
                            $add_word = 1;
                            last;
                        }
                        $last_hash_pos_fin = $hash_pos_fin;
                        $hash_pos_fin = $next_hash_pos_fin;
                    }
                    if ($add_word == 0) {
                        seek(WORD_IND,$end_word_ind_fin,0);
                        print WORD_IND pack("NN",0,length($doc)/$cfg{'index_format_l'});
                        print WORD_IND $doc,$rat,$dist,$attr;
                        seek(HASH,$last_hash_pos_fin,0);
                        print HASH pack("N",$end_hash_fin);
                        seek(HASH,$end_hash_fin,0);
                        print HASH pack("NNC",0,$end_word_ind_fin,$word_l).$word;
                        $end_word_ind_fin += 8+length($doc)+length($rat)+length($dist)+length($attr);
                        $end_hash_fin += 9+$word_l;
                    }
                }
                $hash_pos = $next_hash_pos;
            }
        }
    }
                    
    
}
#=====================================================================
#
#    Function: add_substring_index
#    Last modified: 20.01.2004 23:16
#
#=====================================================================

sub add_substring_index {
    
    my %param = @_;

        require lib::merge_lib;
        import lib::merge_lib;


    print "\n\nSubstring index creation\n\n";

    my $time1 = time;
    my @time=localtime($time1);
    my $time="$time[2]:$time[1]:$time[0]";
    print "Start merging: $time\n";
    
    merge();
    
    my $time2 = time;
    @time=localtime($time2);
    $time="$time[2]:$time[1]:$time[0]";
    print "Finished: $time\n";
    @time=gmtime($time2-$time1);
    $time="$time[2]:$time[1]:$time[0]";
	print "Total time: $time sec.\n";
    
    start(%param);
    
    open HASH, "<$db_cur/$cfg{HASH}" or &my_die("Died: could not open $db_cur/$cfg{HASH} - $!",__LINE__,__FILE__);
    binmode(HASH);
    
    open DIC, ">$db_cur/dic_ind" or &my_die("Died: could not open $db_cur/dic_ind - $!",__LINE__,__FILE__);
    binmode(DIC);
    
    my $hash_pos = $cfg{HASHSIZE}*4;
    my $hash_end = (stat(HASH))[7];
    my $dum = "";
    my $word = "";
    my %n_gram = ();
    my $count = 0;
    my $last_rate = 0;
    
    
    print "\nPrepare index\n\n";
    
    while ($hash_pos < $hash_end) {
        seek(HASH,$hash_pos,0);
        read(HASH,$dum,9);
        my($next_hash_pos, $word_ind_pos, $word_l) = unpack("NNC",$dum);
        read(HASH,$word,$word_l);
        print DIC pack("N",$hash_pos);
        $hash_pos += 9+$word_l;
        $count++;
        if (length($word) < $cfg{substr_len}) { next }
        
        my $subbound = length($word) - $cfg{substr_len} + 1;
        for (my $i=0; $i<$subbound; $i++){
            my $substring = substr($word,$i,$cfg{substr_len});
            $n_gram{$substring} .= pack("N",$count-1);
    	}


        if ($count % 100 == 1) {
            my $rate = int (($hash_pos-$cfg{HASHSIZE}*4)/($hash_end-$cfg{HASHSIZE}*4)*100);
            if ($rate %10 == 0 && $rate != $last_rate) { print "$rate %\n" }
            $last_rate = $rate;
        }

    }

    close(HASH);
    open HASH, ">$db_cur/sub_hash" or &my_die("Died: could not open $db_cur/sub_hash - $!",__LINE__,__FILE__);
    close(HASH);
    open HASH, "+<$db_cur/sub_hash" or &my_die("Died: could not open $db_cur/sub_hash - $!",__LINE__,__FILE__);
    binmode(HASH);
    
    print "Total words: $count\n";
    print "N-grams count:",scalar keys %n_gram,"\n";
    
    print "\nBuilding substring index\n\n";
    my $time1 = time;
    my @time=localtime($time1);
    my $time="$time[2]:$time[1]:$time[0]";
    print "Started: $time\n";
    
    my $hash = "\x00"; $hash x= $cfg{substr_hash_size}*4;
    print HASH $hash;
    $hash_end = $cfg{substr_hash_size}*4;
    my $dum = "";
    my $last_hash_pos = "";
    
    foreach my $key (keys %n_gram) {
        my $hash_value = hash($key) % $cfg{substr_hash_size};
        seek(HASH,$hash_value*4,0);
        read(HASH,$dum,4);
        $hash_pos = unpack("N",$dum);
        
        if ($hash_pos == 0) {
            seek(HASH,$hash_value*4,0);
            print HASH pack("N",$hash_end);
            seek(HASH,$hash_end,0);
            print HASH pack("NN",0,length($n_gram{$key})),$key,$n_gram{$key};
            $hash_end += 8 + $cfg{substr_len} + length($n_gram{$key});
        } else {
            while ($hash_pos != 0) {
                seek(HASH,$hash_pos,0);
                read(HASH,$dum,4);
                $last_hash_pos = $hash_pos;
                $hash_pos = unpack("N",$dum);
            }
            seek(HASH,$last_hash_pos,0);
            print HASH pack("N",$hash_end);
            seek(HASH,$hash_end,0);
            print HASH pack("NN",0,length($n_gram{$key})),$key,$n_gram{$key};
            $hash_end += 8 + $cfg{substr_len} + length($n_gram{$key});
        }
    }

    my $time2 = time;
    @time=localtime($time2);
    $time="$time[2]:$time[1]:$time[0]";
    print "Finished: $time\n";
    @time=gmtime($time2-$time1);
    $time="$time[2]:$time[1]:$time[0]";
	print "Total time: $time sec.\n";
        
    
}
#=====================================================================
#
#    Function: parse_html
#    Last modified: 20.01.2004 23:17
#
#=====================================================================

sub parse_html {

    my ($html_text) = @_;
    
    my ($key, $val, $TITLE, $dum, $descript);
    my $alt = "";
    $title = "";
    
    # Delete parts of document, which should not be indexed
    foreach $key (keys %{$cfg{no_index_strings}}) {
    	$val = $cfg{no_index_strings}{$key};
        $$html_text =~ s/$key.*?$val/ /gs; 
    }

        $$html_text =~ s/<!--.*?-->/ /gs;
        $$html_text =~ s/<[Ss][Cc][Rr][Ii][Pp][Tt].*?<\/[Ss][Cc][Rr][Ii][Pp][Tt]>/ /gs;
        $$html_text =~ s/<[Ss][Tt][Yy][Ll][Ee].*?<\/[Ss][Tt][Yy][Ll][Ee]>/ /gs;

        $$html_text =~ s#<[Tt][Ii][Tt][Ll][Ee]>\s*(.*?)\s*</[Tt][Ii][Tt][Ll][Ee]># #s;
        $title = $1 if ($1);
        $title =~ s/\s+/ /gs;
        $TITLE = $title;
        if ($cfg{truncate_title} eq "YES") { $TITLE = substr($TITLE,0,64) }
        if ($TITLE eq "") {$TITLE = "No title"};

        if ($cfg{use_META} eq "YES") { ($keywords,$description) = get_META_info($html_text) }
        if ($cfg{use_ALT} eq "YES") {
    	     $alt = join " ", ($$html_text =~ m/<[Ii][Mm][Gg][^>]+[Aa][Ll][Tt]="([^"]*)"[^>]*>/gs );
        }
        if ($cfg{del_hyphen} eq "YES") { del_hyphen($html_text) }
        if ($cfg{use_esc} eq "YES") { $$html_text =~ s/(&[a-zA-Z0-9#]*?;)/esc2char($1);/gse; }

        parse_terms($html_text) if $cfg{allow_sort_by_rating};

        $$html_text =~ s/<\/?[bBiIuU]>//gs;
        $$html_text =~ s/<[^>]*>/ /gs;
        $$html_text =~ s/\s+/ /gs;
        
        if (($cfg{use_META_descr} eq "YES") && ($description ne "")) {
            $descript = substr($description,0,$cfg{stored_descr_size});
        } else {
            $descript = substr($$html_text,0,$cfg{stored_descr_size});
        }

        $$html_text .= " ".$title." ".$keywords." ".$description." ".$alt;
    return $TITLE, $descript; 
}
#=====================================================================
#
#    Function: parse_file
#    Last modified: 30.08.2004 20:10
#
#=====================================================================

sub parse_file {
    
    my $data = shift;
    

    my ($dum, $descript, $ext, $parser);
    my $size = undef;
    my $TITLE = "";
    $keywords = $description = $bold_text = $heading_text = $italic_text = $link_text = $title = "";
    

    if ($cfg{verbose_output}) {
        print "$cfn -> $data->{url}; totalsize -> $kbcount\n";
    } else {
    	if ( ($cfn % 100) == 0 ) { print "$cfn -> $data->{url}; totalsize -> $kbcount\n" }
    }


    if ($data->{content_type} eq "application/msword") {
        $data->{type} = "doc";
    }
    if ($data->{content_type} eq "application/rtf") {
        $data->{type} = "rtf";
    }
    if ($data->{content_type} eq "application/pdf") {
        $data->{type} = "pdf";
    }
    if (! $data->{type}) {
        if ($data->{filename}) {
    	    ($data->{type}) = ($data->{filename} =~ m|\.([^.]+)$|);
    	} elsif ($data->{url} =~ m|/$|) {
    	    $data->{type} = "html";
    	} else {
    	    my $t_url = $data->{url};
    	    $t_url =~ s|[?#].*||;
    	    ($data->{type}) = ($t_url =~ m|\.([^.]+)$|);
    	}
    }
    $data->{type} = lc $data->{type};
    if ($data->{type} eq "") { $data->{type} = "html" }
    
    

    if ($data->{type} =~ m|$cfg{non_parse_ext_reg}|io) {
    
        # Do nothing!    

    } elsif ($data->{type} =~ m|$cfg{bin_ext_reg}|io) {
    
        $data->{text} = "";   

    } elsif ($data->{type} =~ m|$cfg{ext_parser_ext_reg}|io) {
        
        print "External parser: $data->{type}\n" if $cfg{debug_level} > 3;
        
        call_parser($data);

    	if (! $data->{text}) { $data->{text} = " Text can not be extracted "; }

    } elsif ($data->{type} =~ m|$cfg{arch_ext_reg}|io) {   
        
        $parser = $cfg{arch_conf}{$data->{type}};
        my $temp_dir = "temp_dir".time.int(rand(1000));
        mkdir($temp_dir,0777) or &my_die("Died: could not create temp directory '$temp_dir' - $!",__LINE__,__FILE__);
        if (! $data->{filename}) {
            $data->{filename} = $temp_dir."risearch_temp_file.$data->{type}";
    	    open TEMP, ">$data->{filename}";
    	    binmode(TEMP);
    	    print TEMP $data->{text};
    	    close(TEMP);
        }    
        
        if ($^O =~ /win/i) {
            $data->{filename} =~ s|/|\\|;
        }

        $parser =~ s/%file%/$data->{filename}/;
        $parser =~ s/%temp_dir%/$temp_dir/;
        my $dum = `$parser`;
        
        my $rules = $cfg{'rules'};
        $rules = create_rule($rules);
        scan_files($temp_dir, $data->{url}."#", $rules, \&parse_file, undef, "archive");
        
        if ($data->{filename} =~ /risearch_temp_file/) { unlink("$data->{filename}") }
        del_tree($temp_dir);
        
        return;

    } else {
        
        $data->{type} = 'html';

    }


    if (! $data->{text} && $data->{type} !~ m|$cfg{bin_ext_reg}|io) {
        open FILE, $data->{filename};
        binmode(FILE);
        ($data->{size},$data->{date}) = (stat(FILE))[7,9];
        if ($data->{size} <= $cfg{max_doc_size}) {
            read(FILE, $data->{text}, $data->{size});
        } else {
            read(FILE, $data->{text}, $cfg{max_doc_size});
        }
        close(FILE);
    }

    if ($data->{type} =~ m|$cfg{bin_ext_reg}|io) {
        
        open FILE, $data->{filename};
        ($data->{size},$data->{date}) = (stat(FILE))[7,9];
        $data->{descript} = "Description is not available for this type of file.";
        close(FILE);

    }

    if ($data->{type} =~ m|$cfg{non_parse_ext_reg}|io) {
        
        ($dum = substr($data->{text},0,$cfg{stored_descr_size}*4)) =~ s/\s+/ /gs;
        $data->{descript} = substr($dum,0,$cfg{stored_descr_size});

    }

    if ($cfg{use_command_tag}) {
        if ( $data->{text} =~ m|$cfg{start_command_tag}\s*(.*?)\s*$cfg{end_command_tag}|i) {
            my $tag = $1;
            if ($tag !~ m|($cfg{index_command_tag})|i) { return }
            if ($tag =~ m|($cfg{noindex_command_tag})|i) { return }
        } else {
            return;
        }
    }
    
    # Automatic recoding
    
    recode_file($data); 
    
    if ($cfg{enable_page_cache} && $data->{type} eq "html") {
        page_cache($data);
    }

    index_file_MEM($data);


    
    if ( (($cfn+1) % 200) == 0 ) { 
        print "\nAverage speed - ",sprintf("%.1f", $kbcount/(time-$starttime+1)/1024)," kb/sec (",sprintf("%.1f", $kbcount/(time-$starttime+1)/1024*60/1024)," Mb/min)\n";
        my @time=gmtime(time-$starttime);
        my $time="$time[2]:$time[1]:$time[0]";
        print "Time elapsed: $time\n\n";
    }


}
#=====================================================================
#
#    Function: recode_file
#    Last modified: 12.01.2004 21:11
#
#=====================================================================

sub recode_file {
    
    my $data = shift;
    my $recoded = 0;
    $data->{content_encoding} = lc($data->{content_encoding});
    
    if (defined ${$cfg{recode_by_http_header}}{$data->{content_encoding}}) {
        $data->{text} = &{ ${$cfg{recode_by_http_header}}{$data->{content_encoding}} }($data->{text});
        $recoded = 1;
    }
    
    if (! $recoded && $data->{type} =~ m|htm|i) {
        my $charset = ($data->{text} =~ m/<\s*[Mm][Ee][Tt][Aa][^>]+?[Cc][Oo][Nn][Tt][Ee][Nn][Tt]=\"?[^>]*?charset=([^>]+?)\"?\s*>/s) ? $1 : '';
        $charset = lc($charset);
        if (defined ${$cfg{recode_by_meta_tag}}{$charset}) {
            $data->{text} = &{ ${$cfg{recode_by_meta_tag}}{$charset} }($data->{text});
            $recoded = 1;
        }
    }

    if (! $recoded ) {
        foreach my $dir (keys %{$cfg{recode_by_url}}) {
            last if $recoded;
            if ($data->{url} =~ m|$dir|) {
                $data->{text} = &{ ${$cfg{recode_by_url}}{$dir} }($data->{text});
                $recoded = 1;
            }
        }
    }

}
#=====================================================================
#
#    Function: parse_terms
#    Last modified: 13.05.2004 22:32
#
#=====================================================================

sub parse_terms {
    my ($text) = @_;

    if ($cfg{weight_bold}) {
    	$bold_text = join " ", ( $$text =~ m#<(?:[Bb]|[Ss][Tt][Rr][Oo][Nn][Gg])>(.*?)</(?:[Bb]|[Ss][Tt][Rr][Oo][Nn][Gg])>#gs );
    	$bold_text =~ s/<[^>]*>/ /gs;
    }
    if ($cfg{weight_heading}) {
        $heading_text = join " ", ( $$text =~ m|<[Hh][1-6]>(.*?)</[Hh][1-6]>|gs );
    	$heading_text =~ s/<[^>]*>/ /gs;
    }
    if ($cfg{weight_italic}) {
        $italic_text = join " ", ( $$text =~ m#<(?:[Ii]|[Ee][Mm])>(.*?)</(?:[Ii]|[Ee][Mm])>#gs );
    	$italic_text =~ s/<[^>]*>/ /gs;
    }
    if ($cfg{weight_link}) {
        $link_text = join " ", ( $$text =~ m|<[Aa] [Hh][Rr][Ee][Ff][^>]*?>(.*?)</[Aa]>|gs );
    	$link_text =~ s/<[^>]*>/ /gs;
    }

}
#=====================================================================
#
#    Function: add_terms_1
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub add_terms_1 {
    if ($cfg{weight_title})    { prep_data_1(\$title,$cfg{weight_title},0) }
    if ($cfg{weight_heading})  { prep_data_1(\$heading_text,$cfg{weight_heading},0) }
    if ($cfg{weight_bold})     { prep_data_1(\$bold_text,$cfg{weight_bold},0) }
    if ($cfg{weight_italic})   { prep_data_1(\$italic_text,$cfg{weight_italic},0) }
    if ($cfg{weight_link})     { prep_data_1(\$link_text,$cfg{weight_link},0) }
    if ($cfg{weight_keywords}) { prep_data_1(\$keywords,$cfg{weight_keywords},0) }
    if ($cfg{weight_descr})    { prep_data_1(\$description,$cfg{weight_descr},0) }
}
#=====================================================================
#
#    Function: add_terms_0
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub add_terms_0 {
    if ($cfg{weight_title})    { prep_data_0(\$title,$cfg{weight_title},0) }
    if ($cfg{weight_heading})  { prep_data_0(\$heading_text,$cfg{weight_heading},0) }
    if ($cfg{weight_bold})     { prep_data_0(\$bold_text,$cfg{weight_bold},0) }
    if ($cfg{weight_italic})   { prep_data_0(\$italic_text,$cfg{weight_italic},0) }
    if ($cfg{weight_link})     { prep_data_0(\$link_text,$cfg{weight_link},0) }
    if ($cfg{weight_keywords}) { prep_data_0(\$keywords,$cfg{weight_keywords},0) }
    if ($cfg{weight_descr})    { prep_data_0(\$description,$cfg{weight_descr},0) }
}
#=====================================================================
#
#    Function: check_zones
#    Last modified: 24.09.2003 18:07
#
#=====================================================================

sub check_zones {
    my ($url,$cfn) = @_;
    for (my $i=1; $i<scalar(@zone); $i++) {
    	if ($url =~ m|$zone[$i]|) { vec($zone_vec[$i],$cfn,1) = 1 }
    }
}
#=====================================================================
#
#    Function: check_zones_HD
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub check_zones_HD {
    my ($url,$cfn) = @_;

    my $update = 0;

    for (my $i=1; $i<scalar(@zone); $i++) {
    	if ($url =~ m|$zone[$i]|) {
    	    $update = 1;
    	}
    }

    if ($update) {
        open ZONE_INFO, "$db/$cfg{ZONE_INFO}" or return 0;
        binmode(ZONE_INFO);
        
        my $pos; my $length; my $dum;
        for (my $i=1; $i<scalar(@zone); $i++) {
            seek(ZONE_INFO,8*$i,0);
            read(ZONE_INFO,$dum,8);
            ($pos,$length) = unpack("NN",$dum);
            seek(ZONE_INFO,$pos,0);
            read(ZONE_INFO,$zone_vec[$i],$length);
        }
        
        for (my $i=1; $i<scalar(@zone); $i++) {
        	if ($url =~ m|$zone[$i]|) {
        	    vec($zone_vec[$i],$cfn,1) = 1;
        	}
        }
        
        close(ZONE_INFO);
        print_zone_info();
    }

}
#=====================================================================
#
#    Function: print_zone_info
#    Last modified: 10.09.2003 13:03
#
#=====================================================================

sub print_zone_info {
    open ZONE_INFO, ">$db/$cfg{ZONE_INFO}" or &my_die("Died: could not open $db/$cfg{ZONE_INFO} - $!",__LINE__,__FILE__);
    binmode(ZONE_INFO);
    print ZONE_INFO pack("NN", 0, 0);
    my $zone_pos = 8 + 8 * (scalar(@zone)-1);
    my $z1 = my $z2 = "";
    for (my $i=1; $i<scalar(@zone); $i++) {
    	$z1 .= pack("NN", $zone_pos, length($zone_vec[$i]));
    	$z2 .= $zone_vec[$i];
    	$zone_pos += length($zone_vec[$i]);
    }
    print ZONE_INFO $z1, $z2;
    close(ZONE_INFO);
}
#=====================================================================
#
#    Function: add_url
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub add_url {
    my ($url, $cfn) = @_;
    
    my $hash_val = hash($url) % 10001;
    my $dum = unpack("N", substr($hash_url,$hash_val*4,4));
    my $prev_pos = 0;

    if ($dum == 0) {
        substr($hash_url,$hash_val*4,4) = pack("N", length($hash_url_ind));
        $hash_url_ind .= pack("NN",0,$cfn);
    } else {
        while ($dum) {
            $prev_pos = $dum;
            $dum = unpack("N", substr($hash_url_ind,$dum,4));
        }
        substr($hash_url_ind,$prev_pos,4) = pack("N", length($hash_url_ind));
        $hash_url_ind .= pack("NN",0,$cfn);
    }
    
}
#=====================================================================
#
#    Function: add_url_HD
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub add_url_HD {
    my ($url, $cfn) = @_;

    my $dum; my $dum2; 
    

        open HASH_URL, "+<$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL}",__LINE__,__FILE__);
        binmode(HASH_URL);
        if ($^O !~ /win/i) {
            flock(HASH_URL,2)
        }

        open HASH_URL_IND, "+<$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND}",__LINE__,__FILE__);
        binmode(HASH_URL_IND);

    
    if ($hash_url_ind_end % 2 != 0 && $^O =~ /win/i) {
        seek(HASH_URL_IND,$hash_url_ind_end,0);
        print HASH_URL_IND "\x1A";
        $hash_url_ind_end += 1;
    }
    
    my $hash_val = hash($url) % 10001;
    seek(HASH_URL, $hash_val*4, 0) or print "sub 'add_url_HD': can't SEEK: $!\n";
    read(HASH_URL,$dum2,4);
    $dum = unpack("N",$dum2);
    my $prev_pos = 0;

    if ($dum == 0) {
        seek(HASH_URL, $hash_val*4, 0);
        print HASH_URL pack("N", $hash_url_ind_end);
        seek(HASH_URL_IND,$hash_url_ind_end,0);
        print HASH_URL_IND pack("NN",0,$cfn);
    } else {
        while ($dum) {
            $prev_pos = $dum;
            seek(HASH_URL_IND,$dum,0);
            read(HASH_URL_IND,$dum2,4);
            $dum = unpack("N",$dum2);
        }
        seek(HASH_URL_IND,$prev_pos,0);
        print HASH_URL_IND pack("N", $hash_url_ind_end);
        seek(HASH_URL_IND,$hash_url_ind_end,0);
        print HASH_URL_IND pack("NN",0,$cfn);
    }
    $hash_url_ind_end += 8;
    
}
#=====================================================================
#
#    Function: check_url_HD
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub check_url_HD {
    my $url = shift;
    
   
    if (! -e "$db/$cfg{HASH_URL}") {
        print "URL database does not exists\n";
        return undef;
    }
        
open HASH_URL_L, "$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL}",__LINE__,__FILE__);
binmode(HASH_URL_L);

open HASH_URL_IND_L, "$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND}",__LINE__,__FILE__);
binmode(HASH_URL_IND_L);

    my $dum; my $dum2; my $cmp_url; my $chsum; my $prev_pos; my $strpos;
    my $url_num;

    my $hash_val = hash($url) % 10001;
    
    seek(HASH_URL_L, $hash_val*4, 0) or print "Seek failed<BR>";
    read(HASH_URL_L,$dum2,4);
    $dum = unpack("N",$dum2);
    
    if ($dum == 0) {
        return undef;
    } else {

        open FINFO_L, "$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO}",__LINE__,__FILE__);
        open FINFO_IND_L, "$db/$cfg{FINFO_IND}" or &my_die("Died: could not open $db/$cfg{FINFO_IND}",__LINE__,__FILE__);
        binmode(FINFO_IND_L);

        while ($dum) {
            $prev_pos = $dum;
            seek(HASH_URL_IND_L,$dum,0);
            read(HASH_URL_IND_L,$dum2,8);
            ($dum,$url_num) = unpack("NN",$dum2);
            seek(FINFO_IND_L,$url_num*4,0);
            read(FINFO_IND_L,$dum2,4);
            $strpos = unpack("N",$dum2);
            seek(FINFO_L,$strpos,0);
            $dum2 = <FINFO_L>;
            ($cmp_url,$chsum) = (split(/::/,$dum2))[0,3];
            if ($url eq $cmp_url) {
                close(FINFO_IND_L);
                close(FINFO_L);
                close(HASH_URL_L);
                close(HASH_URL_IND_L);
                
                return $url_num, $chsum;
            }

        }

        close(FINFO_IND_L);
        close(FINFO_L);

    }
    
    return undef;
    
    close(HASH_URL_L);
    close(HASH_URL_IND_L);
    
}
#=====================================================================
#
#    Function: delete_url
#    Last modified: 30.10.2003 23:01
#
#=====================================================================

sub delete_url {
    my $url = shift;
    
    my $hash_url_ind_pos;
    my $prev_pos=0; my $next_pos;
    my $url_num_test;
    my $dum;
    
    my ($url_num,$chsum) = check_url_HD($url);
    if (defined $url_num) {

        open URL, ">>$db/$cfg{URL_DEL}" or &my_die("Died: could not open $db/$cfg{URL_DEL} - $!",__LINE__,__FILE__);
        binmode(URL);
        print URL pack("N",$url_num);
        close(URL);

        # Delete this record from URL database

        open HASH_URL, "+<$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL} - $!",__LINE__,__FILE__);
        binmode(HASH_URL);
        if ($^O !~ /win/i) {
            flock(HASH_URL,2)
        }

        open HASH_URL_IND, "+<$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND} - $!",__LINE__,__FILE__);
        binmode(HASH_URL_IND);


        my $hash_url_ind_end = (stat(HASH_URL_IND))[7];
        if ($hash_url_ind_end % 2 != 0 && $^O =~ /win/i) {
            seek(HASH_URL_IND,$hash_url_ind_end,0);
            print HASH_URL_IND "\x1A";
            $hash_url_ind_end += 1;
        }


        my $hash_val = hash($url) % 10001;
        seek(HASH_URL, $hash_val*4, 0) or print "Seek failed<BR>";
        read(HASH_URL,$hash_url_ind_pos,4);
        $hash_url_ind_pos = unpack("N",$hash_url_ind_pos);
        while ($hash_url_ind_pos) {
            seek(HASH_URL_IND,$hash_url_ind_pos,0);
            read(HASH_URL_IND,$dum,8);
            ($next_pos,$url_num_test) = unpack("NN",$dum);
            if ($url_num == $url_num_test) {
                if ($prev_pos == 0) {
                    # It is first entry
                    seek(HASH_URL, $hash_val*4, 0) or print "Seek failed<BR>";
                    print HASH_URL pack("N",$next_pos);
                } else {
                    seek(HASH_URL_IND,$prev_pos,0);
                    print HASH_URL_IND pack("N",$next_pos);
                }
                close(HASH_URL);
                close(HASH_URL_IND);
                return 1;
            }
            $prev_pos = $hash_url_ind_pos;
            $hash_url_ind_pos = $next_pos;
            
        }
    } else {
        print "No such URL in database\n";
    }
    
    close(HASH_URL);
    close(HASH_URL_IND);
    return 0;
    
}
#=====================================================================
#
#    Function: get_META_info
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub get_META_info {
    my ($html) = @_;
    my $keywords    = ($$html =~ s/<\s*[Mm][Ee][Tt][Aa]\s*[Nn][Aa][Mm][Ee]=\"?[Kk][Ee][Yy][Ww][Oo][Rr][Dd][Ss]\"?\s*[Cc][Oo][Nn][Tt][Ee][Nn][Tt]=\"?([^\"]*)\"?\s*>//s) ? $1 : '';
    my $description = ($$html =~ s/<\s*[Mm][Ee][Tt][Aa]\s*[Nn][Aa][Mm][Ee]=\"?[Dd][Ee][Ss][Cc][Rr][Ii][Pp][Tt][Ii][Oo][Nn]\"?\s*[Cc][Oo][Nn][Tt][Ee][Nn][Tt]=\"?([^\"]*)\"?\s*>//s) ? $1 : '';
    return ($keywords, $description)
}
#=====================================================================
#
#    Function: del_hyphen
#    Last modified: 28.08.2003 14:36
#
#=====================================================================

sub del_hyphen {
    my ($text) = @_;
    local $/;
    $$text =~ s/-\n//gs;
}
#=====================================================================
#
#    Function: create_db_files_HD
#    Last modified: 05.05.2004 16:58
#
#=====================================================================

sub create_db_files_HD {
    
    
    my $length;
    
    if ( ! -e "$db/$cfg{FINFO}") {
        

        open FINFO, ">$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO} - $!",__LINE__,__FILE__);
        open FINFO_IND, ">$db/$cfg{FINFO_IND}" or &my_die("Died: could not open $db/$cfg{FINFO_IND} - $!",__LINE__,__FILE__);
        binmode(FINFO_IND);
        open FDATE, ">$db/$cfg{FDATE}" or &my_die("Died: could not open $db/$cfg{FDATE} - $!",__LINE__,__FILE__);
        binmode(FDATE);
        open FSIZE, ">$db/$cfg{FSIZE}" or &my_die("Died: could not open $db/$cfg{FSIZE} - $!",__LINE__,__FILE__);
        binmode(FSIZE);

        open HASH_URL, ">$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL} - $!",__LINE__,__FILE__);
        binmode(HASH_URL);
        $hash_url = "\x00"; $hash_url x= 4*10001;

        open HASH_URL_IND, ">$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND} - $!",__LINE__,__FILE__);
        binmode(HASH_URL_IND);
        $hash_url_ind = pack("NN",0,0);

        print HASH_URL $hash_url;
        close(HASH_URL);

        print HASH_URL_IND $hash_url_ind;
        close(HASH_URL_IND);

        if ($cfg{enable_page_cache}) {
            open PCACHE_IND, ">$db/0_page_cache" or &my_die("Died: could not open $db/0_page_cache - $!",__LINE__,__FILE__);
            binmode(PCACHE_IND);
            open PCACHE_DATA, ">$db/0_page_cache_data" or &my_die("Died: could not open $db/0_page_cache_data - $!",__LINE__,__FILE__);
            binmode(PCACHE_DATA);
            close(PCACHE_IND);
            close(PCACHE_DATA);
        }


    }

open FINFO, ">>$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO} - $!",__LINE__,__FILE__);
binmode(FINFO);
seek(FINFO,0,2);
if ($^O !~ /win/i) {
   flock(FINFO,2)
}
open FINFO_IND, ">>$db/$cfg{FINFO_IND}" or &my_die("Died: could not open $db/$cfg{FINFO_IND} - $!",__LINE__,__FILE__);
binmode(FINFO_IND);


open FDATE, ">>$db/$cfg{FDATE}" or &my_die("Died: could not open $db/$cfg{FDATE} - $!",__LINE__,__FILE__);
binmode(FDATE);
open FSIZE, ">>$db/$cfg{FSIZE}" or &my_die("Died: could not open $db/$cfg{FSIZE} - $!",__LINE__,__FILE__);
binmode(FSIZE);


    if ( -e "$db/$cfg{HASH_INC}") {
        open HASH, "+<$db/$cfg{HASH_INC}" or &my_die("Died: could not open $db/$cfg{HASH_INC} - $!",__LINE__,__FILE__);
        binmode(HASH);
        $index_written = 1;
        $end_hash_fin = (stat(HASH))[7];
    } else {
        open HASH, ">$db/$cfg{HASH_INC}" or &my_die("Died: could not open $db/$cfg{HASH_INC} - $!",__LINE__,__FILE__);
        close(HASH);
        open HASH, "+<$db/$cfg{HASH_INC}" or &my_die("Died: could not open $db/$cfg{HASH_INC} - $!",__LINE__,__FILE__);
        binmode(HASH);
    }
    $hash = "\x00"; $hash x= ($cfg{HASHSIZE}*4 + 1000000);
    $end_hash = $cfg{HASHSIZE}*4;
    


    if ( -e "$db/$cfg{WORD_IND_INC}") {
        open WORD_IND, "+<$db/$cfg{WORD_IND_INC}" or &my_die("Died: could not open $db/$cfg{WORD_IND_INC} - $!",__LINE__,__FILE__);
        binmode(WORD_IND);
        $end_word_ind_fin = (stat(WORD_IND))[7];
    } else {
        open WORD_IND, ">$db/$cfg{WORD_IND_INC}" or &my_die("Died: could not open $db/$cfg{WORD_IND_INC} - $!",__LINE__,__FILE__);
        close(WORD_IND);
        open WORD_IND, "+<$db/$cfg{WORD_IND_INC}" or &my_die("Died: could not open $db/$cfg{WORD_IND_INC} - $!",__LINE__,__FILE__);
        binmode(WORD_IND);
        print WORD_IND pack("N",0);
    }
    $word_ind[0] = "\x00"; $word_ind[0] x= 2000000;
    $end_word_ind = 4;


open HASH_URL, "+<$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL} - $!",__LINE__,__FILE__);
binmode(HASH_URL);

open HASH_URL_IND, "+<$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND} - $!",__LINE__,__FILE__);
binmode(HASH_URL_IND);
$hash_url_ind_end = (stat(HASH_URL_IND))[7];

if ($cfg{'word_dist'}) {
    open DIST, ">>$db/$cfg{DIST}" or &my_die("Died: could not open $db/$cfg{DIST} - $!",__LINE__,__FILE__);
    binmode(DIST);
    $dist_pos = (stat(DIST))[7];
}

if ($cfg{enable_page_cache}) {
    open PCACHE_IND, ">>$db/0_page_cache" or &my_die("Died: could not open $db/0_page_cache - $!",__LINE__,__FILE__);
    binmode(PCACHE_IND);
    open PCACHE_DATA, ">>$db/0_page_cache_data" or &my_die("Died: could not open $db/0_page_cache_data - $!",__LINE__,__FILE__);
    binmode(PCACHE_DATA);
}



    
}
#=====================================================================
#
#    Function: create_db_files_MEM
#    Last modified: 05.05.2004 16:24
#
#=====================================================================

sub create_db_files_MEM {
    

open FINFO, ">$db/$cfg{FINFO}" or &my_die("Died: could not open $db/$cfg{FINFO} - $!",__LINE__,__FILE__);
binmode(FINFO);
if ($^O !~ /win/i) {
   flock(FINFO,2)
}

open FINFO_IND, ">$db/$cfg{FINFO_IND}" or &my_die("Died: could not open $db/$cfg{FINFO_IND} - $!",__LINE__,__FILE__);
binmode(FINFO_IND);

open FDATE, ">$db/$cfg{FDATE}" or &my_die("Died: could not open $db/$cfg{FDATE} - $!",__LINE__,__FILE__);
binmode(FDATE);
open FSIZE, ">$db/$cfg{FSIZE}" or &my_die("Died: could not open $db/$cfg{FSIZE} - $!",__LINE__,__FILE__);
binmode(FSIZE);


        open HASH, ">$db/$cfg{HASH}" or &my_die("Died: could not open $db/$cfg{HASH} - $!",__LINE__,__FILE__);
        close(HASH);
        open HASH, "+<$db/$cfg{HASH}" or &my_die("Died: could not open $db/$cfg{HASH} - $!",__LINE__,__FILE__);
        binmode(HASH);
        $hash = "\x00"; $hash x= ($cfg{HASHSIZE}*4 + 1000000);
        $end_hash = $cfg{HASHSIZE}*4;
    
        open WORD_IND, ">$db/$cfg{WORD_IND}" or &my_die("Died: could not open $db/$cfg{WORD_IND} - $!",__LINE__,__FILE__);
        close(WORD_IND);
        open WORD_IND, "+<$db/$cfg{WORD_IND}" or &my_die("Died: could not open $db/$cfg{WORD_IND} - $!",__LINE__,__FILE__);
        binmode(WORD_IND);
        print WORD_IND pack("N",0);
        $word_ind[0] = "\x00"; $word_ind[0] x= 2000000;
        $end_word_ind = 4;
    

open HASH_URL, ">$db/$cfg{HASH_URL}" or &my_die("Died: could not open $db/$cfg{HASH_URL} - $!",__LINE__,__FILE__);
binmode(HASH_URL);
$hash_url = "\x00"; $hash_url x= 4*10001;

open HASH_URL_IND, ">$db/$cfg{HASH_URL_IND}" or &my_die("Died: could not open $db/$cfg{HASH_URL_IND} - $!",__LINE__,__FILE__);
binmode(HASH_URL_IND);
$hash_url_ind = pack("NN",0,0);

if ($cfg{'word_dist'}) {
    open DIST, ">$db/$cfg{DIST}" or &my_die("Died: could not open $db/$cfg{DIST} - $!",__LINE__,__FILE__);
    binmode(DIST);
    $dist_pos = (stat(DIST))[7];
}

if ($cfg{enable_page_cache}) {
    open PCACHE_IND, ">$db/0_page_cache" or &my_die("Died: could not open $db/0_page_cache - $!",__LINE__,__FILE__);
    binmode(PCACHE_IND);
    open PCACHE_DATA, ">$db/0_page_cache_data" or &my_die("Died: could not open $db/0_page_cache_data - $!",__LINE__,__FILE__);
    binmode(PCACHE_DATA);
}


    
}
#=====================================================================
#
#    Function: close_db_files
#    Last modified: 05.05.2004 16:24
#
#=====================================================================

sub close_db_files {
    
    my @files; my $file;


close(HASH);
close(WORD_IND);

print FDATE $fdate;
close(FDATE);
print FSIZE $fsize;
close(FSIZE);


print HASH_URL $hash_url;
close(HASH_URL);

print HASH_URL_IND $hash_url_ind;
close(HASH_URL_IND);


print_zone_info();

close(FINFO_IND);
close(FINFO);

if ($cfg{'word_dist'}) {
    close(DIST);
}

if ($cfg{enable_page_cache}) {
    close(PCACHE_IND);
    close(PCACHE_DATA);
}

    if (scalar @attr_def) {
        
        for (my $i=0; $i < scalar(@attr_def); $i++) {
            
            my $pprint = 0;
            #   integer attribute
            if (("$attr_conf[$i]" & "\x04") ne "\x00") { $pprint = 1 }
            #   float attribute
            if (("$attr_conf[$i]" & "\x08") ne "\x00") { $pprint = 1 }
            #   date attribute
            if (("$attr_conf[$i]" & "\x10") ne "\x00") { $pprint = 1 }
            
            if ($pprint) {
                open ATTR, ">$db/attr_$i" or &my_die("Died: could not open $db/attr_$i - $!",__LINE__,__FILE__);;
                binmode(ATTR);
                print ATTR $attr_data[$i];
                close(ATTR);
            }
        }
    }



if ( ($cfg{delete_old_database} eq "NO") && ($cfg{create_new_database} eq "YES") ) {
    print "Storing old database files in $db_old\n";
    unless ( -e "$db_old" && -d "$db_old" ) {
        mkdir("$db_old",0777) or &my_die("Died: could not create directory '$db_old' - $!",__LINE__,__FILE__);
        print "Directory '$db_old' created\n";
    }
    opendir(DIR,$db_cur) or (print "Cannot open $db_cur: $!\n");
    @files = grep {!(/^\./) &&  -f "$db_cur/$_"} readdir(DIR);
    closedir(DIR);
    foreach $file (@files) {
        print "Move '$db_cur/$file' to '$db_old/$file'\n";
        rename("$db_cur/$file","$db_old/$file") or print "Can't rename file '$file' - $!\n";
    }
    print "\n";
}

if ($cfg{create_new_database} eq "YES") {
    print "Moving new database files to $db_cur\n";
    opendir(DIR,$db_new) or (print "Cannot open $db_new: $!\n");
    @files = grep {!(/^\./) &&  -f "$db_new/$_"} readdir(DIR);
    foreach $file (@files) {
        print "Move '$db_new/$file' to '$db_cur/$file'\n";
        rename("$db_new/$file","$db_cur/$file") or print "Can't rename file '$file' - $!\n";
    }
    closedir(DIR);
    rmdir($db_new) or (print "Cannot delete $db_new: $!\n");
    print "\n";
}




}
#=====================================================================
#
#    Function: close_db_files_HD
#    Last modified: 05.05.2004 16:24
#
#=====================================================================

sub close_db_files_HD {
    


close(HASH);
close(WORD_IND);

close(HASH_URL);
close(HASH_URL_IND);

close(FINFO_IND);
close(FINFO);

if ($cfg{'word_dist'}) {
    close(DIST);
}

if ($cfg{enable_page_cache}) {
    close(PCACHE_IND);
    close(PCACHE_DATA);
}

print FDATE $fdate;
close(FDATE);
print FSIZE $fsize;
close(FSIZE);


    if (scalar @attr_def) {
        
        for (my $i=0; $i < scalar(@attr_def); $i++) {
            
            my $pprint = 0;
            #   integer attribute
            if (("$attr_conf[$i]" & "\x04") ne "\x00") { $pprint = 1 }
            #   float attribute
            if (("$attr_conf[$i]" & "\x08") ne "\x00") { $pprint = 1 }
            #   date attribute
            if (("$attr_conf[$i]" & "\x10") ne "\x00") { $pprint = 1 }
            
            if ($pprint) {
                open ATTR, ">>$db/attr_$i" or &my_die("Died: could not open $db/attr_$i - $!",__LINE__,__FILE__);
                binmode(ATTR);
                print ATTR $attr_data[$i];
                close(ATTR);
            }
        }
    }



}
#=====================================================================
#
#    Function: page_cache
#    Last modified: 05.05.2004 16:25
#
#=====================================================================

sub page_cache {

    my $data = shift;
    
    if (! $cfg{page_cache_load_module} && $cfg{compress_cache}) {
        require Compress::Zlib;
        import Compress::Zlib;
        $cfg{page_cache_load_module} = 1;
    }
    
    if ($cfg{compress_cache}) {
        my $comp = compress($data->{text});
        my $pos = tell(PCACHE_DATA);
        print PCACHE_DATA pack("N",length($comp)),$comp;
        print PCACHE_IND pack("N",$pos);
    } else {
        my $pos = tell(PCACHE_DATA);
        print PCACHE_DATA pack("N",length($data->{text})),$data->{text};
        print PCACHE_IND pack("N",$pos);
    }
    
}
#=====================================================================




1;

