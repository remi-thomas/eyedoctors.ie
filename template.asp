
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "company-structure.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Bla | Eye Doctors "
SiteSection       = "About" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "About Us"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Bla"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
            <h1>Eye Am What I Eat</h1>
            <h2>Diabetic Eye Disease Awareness Campaign</h2>
            <p><img src="img/pic1.jpg" alt="" class="alignleft"> The Irish College of Ophthalmologists, along with Diabetes 
          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
        <!-- Side: Menu -->
          <dl>
            <dt><a href="#">News &amp; Events</a></dt>
            <dd>
              <ul>
                <li class="active"><a href="#">Press Releases</a></li>
                <li><a href="#">Events</a></li>
              </ul>
            </dd>
          </dl>
          <!-- Side: Content Box -->
          <h2><a href="#">Your Sight Our Vision</a></h2>
          <div class="box">
            <img src="img/sidepic1.jpg" alt=""/>
            <p>New campaign to hightlight the impact of lifestyle on our eye health.</p>
            <p><a href="#" class="btn">About the Campaign</a></p>
          </div> <!--{end}  box -->
          
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>