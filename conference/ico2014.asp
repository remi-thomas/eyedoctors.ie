<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "company-structure.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Annual Conference | 13-16 May 2014 | Eye Doctors: Irish College of Ophthalmologists "
SiteSection       = "Conference" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(0,1)
BreadCrumbArr (0,0) = "Conference"
BreadCrumbArr (0,1) = "/conference/ico2014.asp"
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
#conference { background:#e9f2fb; min-height:533px; padding:20px 25px; }
#conference p { margin:10px 0 15px 0; text-align:justify;}

#conference div {padding: 10px 5px; border:1px solid #fff}

#conference h2 {color:#0f375f; font-size:1.3em;font-weight:bold;}
#conference h3 {color:#0f375f; font-size:1.2em;font-weight:bold;padding:5px;}
#conference h4 {color:#0f375f; font-size:1.1em; font-weight:bold;display: inline }
#conference h5 {color:#0f375f; font-size:1em;padding:0;margin:0;display: inline }

table{width:100%;padding-left:15px; border-spacing:15px;}

span {color:#8398A5;display:inline;}



.tab {list-style:none;text-decoration:none; }
.tab ul {color:#0064aa; }
.tab li {float:left; font-size:1.1em; font-weight:bold;  padding:5px 5px; border:1px solid #fff;}
.tab li a { background:#e9f2fb; color:#0064aa; display:block; float:left;  }
.tab li a:hover {background:url(/images/topnav-bg-hover.gif) repeat-x #4b87b1; color:#fff; text-decoration:none }
.sel {background:url(/images/topnav-bg-hover.gif) repeat-x #4b87b1; color:#fff; text-decoration:none}
</style>
<script type="text/javascript" src="/ssi/js/jquery-1.3.2.min.js"></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
          <!--#include file ="ico2014.htm"-->
          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
            
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>