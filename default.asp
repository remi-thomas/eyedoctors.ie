<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "company-structure.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Eye Doctors - Irish College Of Ophthalmologists"
SiteSection       = "Home" ' highlight correct menu tab'
'______________________________________________________________________|'
'|                                                                     |'
'______________________________________________________________________|'
%>
<!doctype html>
<html lang="en">
<head>
  <!--#include virtual="/ssi/incl/metadata.asp" -->

<%
'if fusio() then 
  if Cdate(now()) > Cdate("10 March 2024 23:50") and Cdate(now()) < Cdate("17 March 2024 23:50") then 
    'blue: #00b0f0'
    'green: #00a246'
    'light green: #b0ecc8
    'light blue: #7ad3f3
%> 
  <style>
@media (min-width: 768px) { 
    .img-campaign {max-height: 40px;}
    .navbar-collapse .navbar-form input.form-control {width: 130px;}
    }
@media (min-width: 992px) { 
    .img-campaign {max-height: 60px;}
    }
@media (min-width: 1200px) { 
    .img-campaign {max-height: 80px;}
    }

    .navbar-collapse { background:#00a246; }
    .navbar-header .navbar-toggle {background:#00a246; }
    a.btn-arrow:hover, a.btn-arrow:focus { background:#00a246; }
    a.btn-arrow:hover:after, a.btn-arrow:focus:after { border-left-color:#00a246; }
    .navbar-collapse .navbar-main > li.dropdown .dropdown-menu { border:none; margin-top:0; padding:0; background:#d1d1d4;}
    .navbar-collapse .navbar-main > li > a { border-left:2px solid #b0ecc8 !important;}
    .navbar-collapse .navbar-main > li.dropdown .dropdown-menu > li > a:hover { background:#00b0f0; }
    .navbar-collapse .navbar-form p a.btn:hover { background:#00b0f0 !important; color:#fff; }

    .navbar-collapse .navbar-main > li > a:hover, .navbar-collapse .navbar-main > li > a:focus, .navbar-collapse .navbar-main > .active > a, .navbar-collapse .navbar-main > .active > a:hover, .navbar-collapse .navbar-default .navbar-main > .active > a:focus, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {background: #00b0f0;color: #fff;}

    .navbar-collapse .navbar-main > .active > a, .navbar-collapse .navbar-main > .active > a:hover, .navbar-collapse .navbar-default .navbar-main > .active > a:focus, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus { background:#00b0f0 !important; color:#fff; }

    .navbar-header .navbar-toggle:hover, .navbar-default .navbar-toggle:focus { background-color:#00b0f0 !important; }

    /* subnav color 
    .navbar-collapse .navbar-main > li.dropdown .dropdown-menu > li.dropdown-submenu .dropdown-menu{  border-top:20px; top:0; background:#7ad3f3;}
    .navbar-collapse .navbar-main > li.dropdown .dropdown-menu { border:none; margin-top:0; padding:0; background:#7ad3f3;}
    */
  
    footer { background:#00a246; color:#fff;}

    .homecols h2 a {color: #00b0f0}

    .homecols p a.btn, .sidecol p a.btn { background:#00a246;}
    .homecols p a.btn:after, .sidecol p a.btn:after { border-top:15px solid transparent; border-left:15px solid #00a246; border-bottom:15px solid transparent; content:''; display:inline-block; height:30px; position:absolute; right:-15px; top:0; width:15px; }
    .homecols p a.btn:hover, .sidecol p a.btn:hover { background:#00b0f0; }
    .homecols p a.btn:hover:after, .sidecol p a.btn:hover:after { border-left-color:#00b0f0; }

    .banner .box.events h2 { color:#00b0f0; font-size:22px; margin:4px 0 12px; }
    .banner .box.events h2 a { color:#00b0f0; }
    .banner .box.events p a.btn { background:#00a246; }  
    .banner .box.events p a.btn:after { border-top:15px solid transparent; border-left:15px solid #00a246; border-bottom:15px solid transparent; content:''; display:inline-block; height:30px; position:absolute; right:-15px; top:0; width:15px; }
    .banner .box.events p a.btn:hover { background:#00a246; }
    .banner .box.events p a.btn:hover:after { border-left-color:#00a246; }

    .carousel-inner .item .carousel-text h2 { color:#00b0f0; font-size:24px; margin:0 0 15px; }
    .carousel-inner .item .carousel-text h2 a:link {color:#00b0f0;}
    .carousel-inner .item .carousel-text h2 a:visited {color:#00b0f0;}
    .carousel-inner .item .carousel-text p { color:#00a246; font-size:16px; font-weight:bold; line-height: 1.1; margin:0 0 5px; }

    a { color: #00a246;}
    a:hover, a:focus { color: #4ee691;}
    </style>
<% 
  end if
'end if 
%>
</head>
<body>

<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->
<!-- HOMEPAGE: Banner [NEW] ================================== -->
<!--#include virtual="/ssi/fct/Slider2018Config.asp" -->
<% 
'call the function to build the slider'
BuildSlider(0)
dim AltBanner
%>

<div class="banner">
  <div class="container"> 
	<div class="row">
  	  <div class="col-md-8">
		<!-- HOMEPAGE: Carousel -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Carousel: Indicators -->
			<ol class="carousel-indicators">
        <% 
        dim i
        for i = 0 to TotalSliders

          response.write vbtab & "<li data-target=""#myCarousel"&i&""" data-slide-to="""&i&""" class=""" 
          if i = 0 then 
            response.write "active"
          end if
          response.write """></li>"  & vbnewline
        next
        %>
			  
			</ol>
			<!-- Carousel: Banners -->
			<div class="carousel-inner">
          <%
          for i = 0 to TotalSliders

          if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
            AltBanner = BannerTitle(i)
          else 
            if Len(BannerText(i)) Then
              AltBanner = BannerText(i)
            else
              AltBanner = "Irish College of Ophthalmologists - " & Year(Now())
            end if
          end if


          response.write  vbtab & "<div class=""item clearfix "
          if i = 0 then 
            response.write "active"
          end if
          response.write """"


          response.write ">" & vbnewline



          if  CurrentBannerID(i) = 196 then
            
            response.write "<a href=""#"" data-toggle=""modal"" data-target=""#myModal"" class=""carousel-video"" title=""Watch our video on a career pathway in Medical Ophthalmology""><img src=""/banners2018/trainee-video.jpg""/></a>" 
            response.write vbtab & vbtab & "<div class=""carousel-text""><h2>Thinking of a Career in Ophthalmology?</h2><p>Watch our video on a career pathway in Medical Ophthalmology</p></div>" & vbnewline

          else 

            if len(BannerImage(i)) Then
            response.write vbtab & "<a href="""& BannerLink(i) &""" title=""" & AltBanner &""" class=""carousel-img"">"
            response.write vbtab & "<img src=""" & replace(lcase(BannerImage(i)),"banners/","banners2018/") & """ alt=""" & AltBanner &""" title=""" & AltBanner &""">"
            response.write vbtab & "</a>"  & vbnewline
            end if


            response.write vbtab & vbtab & "<div class=""carousel-text"">"  & vbnewline
            if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
            response.write vbtab & vbtab & vbtab & "<h2"
            if len(BannerTextStyle(i)) then
              response.write " style=""" & BannerTitleStyle(i) & """"
            end if
            response.write "><a href="""& BannerLink(i) &""" "
            if len(BannerTextStyle(i)) then
              response.write " style=""" & BannerTitleStyle(i) & """"
            end if
            response.write " title=""" & RemoveAllHTMLTags(AltBanner) &""">"& BannerTitle(i) &"</a></h2>"
            end if

            response.write vbtab & vbtab & vbtab &  "<p"
            if Len(BannerTextStyle(i))  then 
              response.write " style="""& BannerTextStyle(i) &""""
            end if
            response.write ">"  & vbnewline
            Response.write vbtab & vbtab & vbtab & "<a href=""" & BannerLink(i) &""" title="""& RemoveAllHTMLTags(AltBanner) &""" "
            if Len(BannerTextStyle(i))  then 
              response.write " style="""& BannerTextStyle(i) &""""
            end if

           response.write">" & BannerText(i) & "</a>"   & vbnewline
            Response.write vbtab & vbtab & vbtab &  "</p>"  & vbnewline
          response.write vbtab & vbtab & "</div>"  & vbnewline
          end if

          response.write vbtab  & "</div>"  & vbnewline
          next
          %>



			</div>
			<!-- Carousel: Controls --> 
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" aria-label="Click to go to the previous slide" title="Previous Slide"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#myCarousel" role="button" aria-label="Click to go to next previous slide" data-slide="next" title="Next Slide"><span class="icon-next"  ></span></a> 
		</div>
	  </div>
  	  <div class="col-md-4">
      <!-- EVENTS ================================================== -->
	    <div class="box events">
		  <h2><a href="/news-events/" title="Meetings &amp; Events">Meetings &amp; Events</a></h2>
      <%
        set rs = sqlconnection.execute("exec usp_sel_forthcoming_events NULL")
        if not rs.eof then
          dim cntNews : cntNews = 0
        %>
        <ul>
          <% 
          do while not rs.eof and cntNews < 3
          cntNews = cntNews + 1
            response.write "<li class=""clearfix""><a href="""&  CreateNewsFriendyURL(rs("EventID"),FormatUSDateTime(rs("StartDate")),rs("Title")) & """ title="""& rs("Title") &""">" & _
            "<em class=""date""><span>"& Day(rs("StartDate")) &"</span> "& MonthName(Month(rs("StartDate")),true) &"</em>"   & _
            " <strong>"& TrimTitle(rs("Title"),50)& "</strong></a></li>" & vbnewline
          rs.movenext
          loop
          %>
        </ul>
        <%
        end if
        %>

		  <p><a href="/news-events/" class="btn" role="button" aria-label="List all forthcoming events" >View all Events</a></p>
  	    </div>
	  </div>
	</div>
  </div>
</div>

<!-- CONTENT AREA ============================================ -->
<div class="container"> 
  <!-- HOMEPAGE: News -->
  <div class="row homecols">


        <div class="col-sm-6 col-md-3">
      <h2><a href="/trainees/a-Career-as-an-Eye-Doctor.asp" title="Ophthalmology  Training Programmes">Ophthalmology  Training</a></h2>
      <div class="box"> <a href="/trainees/a-Career-as-an-Eye-Doctor.asp" title="Ophtalmology Training Programmes"><img src="/images/homepic5.jpg" alt="Ophtalmology  Training Programmes "></a>
        <p>Learn more about the ICO <br/>training programmes<br/>in Ophthalmology</p>
        <p><a href="/trainees/a-Career-as-an-Eye-Doctor.asp" class="btn" title="Ophtalmology  Training Programmes" role="button" aria-label="Ophtalmology  Training Programmes">Training Programmes</a></p>
      </div>
    </div>


    <!-- div class="col-sm-6 col-md-3">
      <h2><a href="/members/sub-menu.asp?SubMenu=guidelines">Guidelines for Doctors</a></h2>
      <div class="box list">
        <ul>
          <li class="clearfix"><a href="/members/Fitness-to-Drive-Guidelines.asp" title="Medical Fitness to Drive Guidelines">Medical Fitness to Drive Guidelines</a></li>
          <li class="clearfix"><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html" title="Guidelines for Refractive Eye Surgery">Guidelines for Refractive Eye Surgery</a></li>
          <li class="clearfix"><a href="/members/ICO-Guidelines-on-the-Consent-Process.asp" title="ICO Guidelines on the Consent Process">ICO Guidelines on the Consent Process</a></li>
          <li class="clearfix"><a href="/Health-Policy/Medical-Advertising-Standards.asp" title="Advertising and Marketing Guidelines">Advertising and Marketing Guidelines</a></li>
        </ul>
        <p><a href="/news-events/" class="btn">View all Guidelines</a></p>
      </div>
    </div -->

    <% if now() < cdate("19 May 2022 01:05:00") then %>

    <div class="col-sm-6 col-md-3">
      <h2><a href="/kilkenny2022/" title="ICO Annual Conference">ICO Annual Conference</a></h2>
      <div class="box"> <a href="/kilkenny2022/" title="ICO Annual Conference 2022"><img src="/images/meeting.jpg" alt="Kilkenny 2022" title="ICO Annual Conference - 16 - 18 May - Kilkenny"></a>
        <p>16 - 18 May 2022<br/>Kilkenny Convention Centre</p>
        <p><a href="/kilkenny2022/" class="btn" role="button" aria-label="ICO Annual Conference 2022" title="Kilkenny 2022">View  programme</a></p>
      </div>
    </div>
    <% else %>

      <div class="col-sm-6 col-md-3">
      <h2><a href="/your-eye-health/eye-conditions.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma">Eye Conditions</a></h2>
      <div class="box"> <a href="/your-eye-health/eye-conditions.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"><img src="/images/homepic1.jpg" alt="eye conditions" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"></a>
        <p>Learn about common conditions like Cataract, Conjunctivitis and Glaucoma.</p>
        <p><a href="/your-eye-health/eye-conditions.asp" class="btn" role="button" aria-label="What are the common eye conditions like Cataract, Conjunctivitis, Glaucoma etc.." title="Common eye conditions">Common Conditions</a></p>
      </div>
    </div>

    <% end if %>

    <div class="clearfix visible-sm-block"></div>
    <div class="col-sm-6 col-md-3">
      <h2><a href="/opthalmologists/" title="Eye Doctors Directory">Find an Eye Doctor</a></h2>
      <div class="box"> <a href="/opthalmologists/" title="Eye Doctors Directory"><img src="/images/homepic2.jpg" alt="Eye Doctors Directory"></a>
        <p>Our directory contains contact information for eye doctors practicing in Ireland. </p>
        <p><a href="/opthalmologists/" class="btn" title="Eye Doctors Directory" role="button" aria-label="Direcory of eye doctors">List of Eye Doctors</a></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <h2><a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"> Professional Competence</a></h2>
      <div class="box"> <a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" title="Learn more about the ICO Professional Competence Scheme and your PCS requirements"><img src="/images/cpd.jpg" alt="Learn more about the ICO Professional Competence Scheme" title="Learn more about the ICO Professional Competence Scheme"></a>
        <p> Learn more about the ICO Professional Competence Scheme and your PCS requirements</p>
        <p><a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" class="btn" role="button" aria-label="Learn more about the Professional Competence Scheme" title="Learn more about the ICO Professional Competence Scheme">Professional Competence </a></p>
      </div>
    </div>

  </div>
</div>

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ico" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          National Training Programme in Medical Ophthalmology
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
        </div>
        <div class="modal-body">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/605642188?dnt=1" allowfullscreen></iframe></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

  

<style>
/*https://codepen.io/JacobLett/pen/xqpEYE*/

.carousel-inner .item .carousel-video { display:block; margin:15px 15px 10px; }
.carousel-inner .item .carousel-video img { position:relative; }
@media (min-width: 768px) { 
  .carousel-inner .item .carousel-video { float:left; margin:0 20px 0 0; }
}
@media (min-width: 992px) {
  .carousel-inner .item .carousel-video { overflow: hidden; width: 450px; }
  .carousel-inner .item .carousel-video img { max-width:none; }
}
@media (min-width: 1200px) { 
  .carousel-inner .item .carousel-video { width: 450px; }
}



.videovimeo
{
background-color:#63bbbd;
}

.videovimeo img {
    display: block;
 }


.modal-dialog {
      max-width: 1000px;
      margin: 30px auto;
  }

.modal-body {
  /*position:relative;*/
  padding:0px;
}


.close {
  /*position:absolute;
  right:-30px;
  top:0;
  z-index:999;color:#fff;*/
  font-size:2rem;
  font-weight: normal;
  
  opacity:1;
}


.modal-backdrop.in {
    opacity: 0.9;
}

</style>

</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->