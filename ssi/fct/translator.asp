<%

function ValidateText(TextString)
	'make sure all the br tags are in small caps'
	TextString = trim(TextString)
	If len(TextString) > 0 then
		TextString = replace(TextString, "�", "'")
		TextString = replace(TextString, "`", "'")
		TextString = replace(TextString, "�", "'")
		TextString = Replace(TextString, "'", "''")
		TextString = Replace(TextString, "  ", " ")
		TextString = Replace(TextString, " &amp; ", " & ")
		TextString = Replace(TextString, "<br>", "<br>")
		TextString = Replace(TextString, " & ", " &amp; ")
		TextString = Replace(TextString, "<br>", "<br/>")
		TextString = Replace(TextString, "�", "&euro;")
		'TextString = replace(TextString, chr(34), "&quot;")
		TextString = replace(TextString, "�", "&163;")

		TextString = replace(TextString,"�","&aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&uacute;",vbTextCompare)
		TextString = replace(TextString,"�","&iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&oacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Uacute;",vbTextCompare)

	end if
	ValidateText = TextString
End Function

function ValidateText(byval TextString)
	'make sure all the br tags are in small caps'
	TextString = trim(TextString)
	If len(TextString) > 0 then
		TextString = replace(TextString, "�", "'")
		TextString = replace(TextString, "`", "'")
		TextString = replace(TextString, "�", "'")
		TextString = Replace(TextString, "'", "''")
		TextString = Replace(TextString, "  ", " ")
		TextString = Replace(TextString, " &amp; ", " & ")
		TextString = Replace(TextString, "<br>", "<br>")
		TextString = Replace(TextString, " & ", " &amp; ")
		TextString = Replace(TextString, "<br>", "<br/>")
		TextString = Replace(TextString, "�", "&euro;")
		'TextString = replace(TextString, chr(34), "&quot;")
		TextString = replace(TextString, "�", "&163;")

		TextString = replace(TextString,"�","&aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&uacute;",vbTextCompare)
		TextString = replace(TextString,"�","&iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&oacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Uacute;",vbTextCompare)

	end if
	ValidateText = TextString
End Function

Function EncodeAccent(byval TextString)
	If len(TextString) > 0 then
		TextString = replace(TextString,"�","&aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&uacute;",vbTextCompare)
		TextString = replace(TextString,"�","&iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&oacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Aacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Eacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Iacute;",vbTextCompare)
		TextString = replace(TextString,"�","&Uacute;",vbTextCompare)
	end if
	DecodeAccent = TextString
End function


Function DecodeAccent(byval TextString)
	If len(TextString) > 0 then
		TextString = replace(TextString,"&aacute;","�",vbTextCompare)
		TextString = replace(TextString,"&eacute;","�",vbTextCompare)
		TextString = replace(TextString,"&uacute;","�",vbTextCompare)
		TextString = replace(TextString,"&iacute;","�",vbTextCompare)
		TextString = replace(TextString,"&oacute;","�",vbTextCompare)
		TextString = replace(TextString,"&Aacute;","�",vbTextCompare)
		TextString = replace(TextString,"&Eacute;","�",vbTextCompare)
		TextString = replace(TextString,"&Iacute;","�",vbTextCompare)
		TextString = replace(TextString,"&Uacute;","�",vbTextCompare)

	end if
	DecodeAccent = TextString
End function



Function ValidateNumeric(NumValue)
	if IsNull(NumValue) then
		ValidateNumeric = 0
		exit function
	end if
	if Not IsNumeric(NumValue) or NumValue="" then
		ValidateNumeric = 0
	else
		ValidateNumeric = NumValue
	end if
End Function


function DisplayText(TextString)
	If len(TextString) > 1 then
		DisplayText = trim(TextString)
		DisplayText = Replace(DisplayText, "''", "'")
		'DisplayText = Replace(TextString,  "<br/>",vbCrLf)
	Else
		DisplayText = TextString
	End if
End Function

function xmlText(ThisString)
	xmlText	= ThisString
	'xmlText = replace(ThisString, "&", "")
	xmlText = replace(xmlText, "<", "")
	xmlText = replace(xmlText, "�", "&euro;")
	xmlText = replace(xmlText, "�", "&163;")
	xmlText = replace(xmlText, ">", "")
	xmlText = replace(xmlText, chr(34), "")
	xmlText = replace(xmlText, "�", "&39;")
	xmlText = replace(xmlText, "`", "&39;")
	xmlText = replace(xmlText, "�", "&39;")
	xmlText = replace(xmlText, "'", "&39;")
	xmlText = replace(xmlText, "�", "&163;")
	xmlText = replace(xmlText, "�", "e")
	xmlText = replace(xmlText, "�", "a")
	xmlText = replace(xmlText, "�", "i")
	xmlText = replace(xmlText, "�", "o")
	xmlText = replace(xmlText, "�", "u")
	xmlText = replace(xmlText, "�", "e")
	xmlText = replace(xmlText, "�", "&231;")
	xmlText = replace(xmlText, "�", "u")
	xmlText = replace(xmlText, "�", "a")
	xmlText = replace(xmlText, "�", "i")
	xmlText = replace(xmlText, "�", "o")
	xmlText = replace(xmlText, "�", "u")
	xmlText = replace(xmlText, "�", "E")
	xmlText = replace(xmlText, "�", "A")
	xmlText = replace(xmlText, "�", "I")
	xmlText = replace(xmlText, "�", "O")
	xmlText = replace(xmlText, "�", "u")
	xmlText = replace(xmlText, "/", "|")
end function

Function RemoveAllPs(ThisString)
	if len(ThisString) then
	RemoveAllPs = replace(ThisString, "<p>", "<br/>")
	RemoveAllPs = replace(RemoveAllPs, "</p>", vbnewline)
	RemoveAllPs = replace(RemoveAllPs, "</ p>", vbnewline)
	RemoveAllPs = replace(RemoveAllPs, "<p >", "<br/>")
	Else
		exit function
	end if
End Function


Function FormatTitle(ByVal strInput)
    Dim result, curWord
    Dim specialChars, Words
    Dim x, y

   'by default, result is the input itself:
    result=strInput
    'initialize special characters:
    specialChars=Array(".","_","-")
    'first make it all smaller case:
    strInput=LCase(strInput)
    ' get the words!
    Words=Split(strInput, " ")
   'loop through the words:

    For x=0 To UBound(Words)
       ' get current word:
	curWord=Words(x)
       ' first make the first letter capital:
        curWord=CapOneWord(curWord)
        'captilize special characters as well:
	For y=0 To UBound(specialChars)
		Call CapParts(curWord, specialChars(y))
	Next
        'current word is done for, update array:
        Words(x)=curWord
    Next

    'build the string back with the new words:
    result=Join(Words, " ")

    FormatTitle = result  'return the result
	FormatTitle = Replace(FormatTitle,"O'b","O'B") 'O'brien instead of O'Brien
	FormatTitle = Replace(FormatTitle,"Mcc","McC") 'McCullough
	FormatTitle = Replace(FormatTitle,"Rte","RT&Eacute;")

End Function


Function CapOneWord(strWord)
    Dim strLeftPart, strRightPart
    strLeftPart=""
    strRightPart=""
    If Len(strWord)>0 Then
        strLeftPart=UCase(Left(strWord, 1))
        If Len(strWord)>1 Then
            strRightPart=Right(strWord, Len(strWord)-1)
        End If
    End If
    CapOneWord = strLeftPart&strRightPart
End Function


Sub CapParts(ByRef theWord, delimeter)
    'first check if delimeter is there:
    If InStr(theWord, delimeter)=0 Then
        Exit Sub 'no need to do anything...
    End If

    Dim Parts, z, curPart
    'split to parts:
    Parts=Split(theWord, delimeter)
    'loop through parts:
    For z=0 To UBound(Parts)
	curPart=Parts(z)
        'capitilize first letter:
	curPart=CapOneWord(curPart)
        'current part is done for, update array:
        Parts(z)=curPart
    Next
    'build the word:
    theWord=Join(Parts, delimeter)
End Sub

Function RemoveAllHTMLTags(ThisContent)
	RemoveAllHTMLTags = ThisContent
	Dim lpp : lpp = 0
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Pattern =  "<[\w/]+[^<>]*>"
	objRegExp.global=true

	'check if such a treat exists (.test returns true|false)
	Do while objRegExp.Test(RemoveAllHTMLTags)
		lpp = lpp + 1
		RemoveAllHTMLTags = objRegExp.replace(RemoveAllHTMLTags,"")
		if lpp > 10 then
			RemoveAllHTMLTags = "{error -rh-"& lpp&"} "
			exit do
			exit function
		end if
	Loop
	'response.Write "<br/>RemoveAllHTMLTags out<hr>" & RemoveAllHTMLTags & "<hr>"
	'response.end
end function
' 
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
function EncodeText(ThisString)
	'EncodeText = replace(ThisString, "�", "&#39;",1,-1,0)
	'EncodeText = replace(EncodeText, "`", "&#39;",1,-1,0)
	'EncodeText = replace(EncodeText, "�", "&#39;",1,-1,0)
	'EncodeText = replace(EncodeText, "'", "&#39;",1,-1,0)

	EncodeText = replace(ThisString, "�","'",1,-1,0)
	EncodeText = replace(EncodeText, "`", "'",1,-1,0)
	EncodeText = replace(EncodeText, "�", "'",1,-1,0)


	EncodeText = replace(EncodeText, "�", "&euro;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&#163;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&#231;",1,-1,0)

	'-- e --
	EncodeText = replace(EncodeText, "�", "&eacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&egrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ecirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&euml;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Eacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Egrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Ecirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Euml;",1,-1,0)

	'-- a --
	EncodeText = replace(EncodeText, "�", "&aacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&agrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ecirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Aacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Agrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Acirc;",1,-1,0)

	'-- i --
	EncodeText = replace(EncodeText, "�", "&iacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Iacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&icirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Icirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&iuml;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Iuml;",1,-1,0)

	'-- o --
	EncodeText = replace(EncodeText, "�", "&oacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Oacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ocirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&�circ;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ouml;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Ouml;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&otilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Otilde;",1,-1,0)


	'-- u --
	EncodeText = replace(EncodeText, "�", "&uacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Uacute;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ucirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Ucirc;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ugrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Ugrave;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&uuml;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Uuml;",1,-1,0)

	'-- tilde --
	EncodeText = replace(EncodeText, "�", "&Atilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&atilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Ntilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ntilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Otilde;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&otilde;",1,-1,0)


	'-- weirdos --
	EncodeText = replace(EncodeText, "�", "&Ccedil;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&ccedil;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&szlig;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&aring;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&Aring;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&iquest;",1,-1,0)
	EncodeText = replace(EncodeText, "�", "&iexcl;",1,-1,0)

	EncodeText = replace(EncodeText, "�", "&copy;",1,-1,0)


	EncodeText = replace(EncodeText, "''", "'")

end Function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
Function RemoveXSS(Byval ThisString)
'function to prevent Cross Site Scripting
	if len(ThisString) then
		ThisString = Replace(ThisString, "&", "&amp;")
		ThisString = Replace(ThisString, "alert", "alrt")
		ThisString = Replace(ThisString, "script", "scrt")
		ThisString = Replace(ThisString, "prompt", "prmt")
		ThisString = Replace(ThisString, "javascript", "javasrpt")
		ThisString = Replace(ThisString, """", "&quot;")
		ThisString = Replace(ThisString,"href","hrf")
		ThisString = Replace(ThisString,"@"," at ")
		ThisString = Replace(ThisString,"--"," &mdash; ")
		ThisString = Replace(ThisString,"-"," &ndash; ")
		RemoveXSS = ThisString
	else
		RemoveXSS = ""
	end if
End Function

function DecodeText(ThisString)
	DecodeText = replace(ThisString, "&#39;", "'",1,-1,0)

	DecodeText = replace(DecodeText, "&euro;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&#163;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&#231;", "�", 1,-1,0)

	'-- e --
	DecodeText = replace(DecodeText, "&eacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&egrave;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&ecirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&euml;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Eacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Egrave;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Ecirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Euml;", "�", 1,-1,0)

	'-- a --
	DecodeText = replace(DecodeText, "&aacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&agrave;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ecirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Aacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Agrave;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&Acirc;","�", 1,-1,0)

	'-- i --
	DecodeText = replace(DecodeText, "&iacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Iacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&icirc;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&Icirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&iuml;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Iuml;", "�", 1,-1,0)

	'-- o --
	DecodeText = replace(DecodeText, "&oacute;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&Oacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ocirc;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&�circ;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ouml;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Ouml;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&otilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Otilde;", "�", 1,-1,0)


	'-- u --
	DecodeText = replace(DecodeText, "&uacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Uacute;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ucirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Ucirc;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ugrave;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Ugrave;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&uuml;", "�",1,-1,0)
	DecodeText = replace(DecodeText, "&Uuml;", "�", 1,-1,0)

	'-- tilde --
	DecodeText = replace(DecodeText, "&Atilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&atilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Ntilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ntilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Otilde;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&otilde;", "�", 1,-1,0)


	'-- weirdos --
	DecodeText = replace(DecodeText, "&Ccedil;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&ccedil;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&szlig;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&aring;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&Aring;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&iquest;", "�", 1,-1,0)
	DecodeText = replace(DecodeText, "&iexcl;", "�", 1,-1,0)

	DecodeText = replace(DecodeText, "&copy;", "�", 1,-1,0)


end Function


%>
