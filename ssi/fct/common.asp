<%
' -------------------------------------------------------------------- '
' --                                                                -- '
' --   copyright Fusio                                              -- '
' --                                                                -- '
' -------------------------------------------------------------------- '
' set default language '
Session.LCID = 6153 


'##############################################################################################
Dim BreadCrumbArr() : Dim BreadCrumbHTML
'
Dim SiteSection : SiteSection = ""
'
Dim SiteSubSection : SiteSubSection = ""
''
Dim MetaAbstract, Metadescription, ArticleURL, ImgSocialMediaURL, PageTitle
'
Dim DomainName
DomainName = "https://www.eyedoctors.ie/"

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' flickr 
dim API_KEY :	API_KEY		= "ea114a151e1386e015a875d79a5a1fe0"
dim secret : 	secret		= "dd451bdbe0b035d7"
dim user_id : 	user_id		= "55767928@N06"
'##############################################################################################


'@@@@@@ 3. fct ReadPage(MyPath)
Function ReadPage(MyPath)
	dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	ThisFileExists = fso.FileExists(Server.MapPath(MyPath))
	if ThisFileExists then
		strURL = DomainName & MyPath

		SET objXmlHttp=Server.CreateObject("Msxml2.ServerXMLHTTP")
		objXmlHttp.open"GET",strURL,False
		objXmlHttp.send
		ReadPage=objXmlHttp.responseText
	else
		ReadPage = "file " & MyPath & " does not exist"
	end if
	set fso = nothing
End Function



'###########################################################'
'##          E R R O R    M E S S A G E S                 ##'
'###########################################################'
Function DisplayErrMssge(ThisErr)
	response.write  "<div class=""error""><fieldset name=""error"" class=""fielderror"">" & VbNewline & _
	"<legend class=""legend"">Error</legend>"  & VbNewline & _
	"<img src=""/images/error.gif"" alt=""!"" />"  & _
	ThisErr & VbNewline & _
	"<br/>&nbsp;<br/><a href=""#"" class=""bluetext"" onClick=""history.back()"" title=""Go Back"">&laquo;&laquo; Go Back</a>"& VbNewline & _
	"</fieldset></div>" & VbNewline
End Function


Function BuildOrganicHeader(byval hValue)
 'Array(ArticleTitle,"eArticle",ArticleSmallContent,ShortenArticleURL(ArticleID),ImgSocialMediaURL)
 	Dim activePageURL
	activePageURL = hValue(3)

	BuildOrganicHeader = vbnewline & _
	vbtab & "<meta name=""og:title"" property=""og:title"" content="""& hValue(0) & """ />"& vbnewline & _
	vbtab & "<meta name=""og:type"" property=""og:type"" content="""& hValue(1) & """ />" & vbnewline & _
	vbtab & "<meta name=""og:description"" property=""og:description"" content="""& TrimTitle(RemoveAllHTMLTags(hValue(2)),180) & """ />" & vbnewline & _
	vbtab & "<meta name=""og:url"" property=""og:url"" content="""& activePageURL & """ />" & vbnewline
	If Len(hValue(4)) Then
		If ((Instr(hValue(4),"http") = 0) and (left(hValue(4),2) <> "//")) Then
			hValue(4) = "http:" & application("imageserver") & replace("/" & hValue(4),"//","/")
		Elseif (left(hValue(4),2) = "//") then
			hValue(4) = "http:" & hValue(4)
		End if
		BuildOrganicHeader = BuildOrganicHeader & _
		vbtab & "<meta name=""og:image"" property=""og:image"" content="""& hValue(4) & """ />" & vbnewline
	End if

	BuildOrganicHeader = BuildOrganicHeader & _
	vbtab & "<meta name=""og:site_name"" property=""og:site_name"" content=""entertainment.ie"" />" & vbnewline

	BuildOrganicHeader = BuildOrganicHeader & vbnewline & _
	vbtab & "<meta name=""twitter:card"" content=""summary_large_image"" />" & vbnewline & _
	vbtab & "<meta name=""twitter:title"" content="""& hValue(0) & """ />"& vbnewline & _
	vbtab & "<meta name=""twitter:description"" content="""& TrimTitle(RemoveAllHTMLTags(hValue(2)),180) & """ />" & vbnewline & _
	vbtab & "<meta name=""twitter:url"" content="""& activePageURL & """ />" & vbnewline
	If Len(hValue(4)) Then
		BuildOrganicHeader = BuildOrganicHeader & _
		vbtab & "<meta name=""twitter:image"" content="""& hValue(4) & """ />" & vbnewline
	End if

	BuildOrganicHeader = BuildOrganicHeader & _
	vbtab & "<meta name=""twitter:site"" content=""@eyedoctorsIRL"" />" & vbnewline & _
	vbtab & "<meta name=""twitter:site:id"" content=""821343739"" />" & vbnewline & _
	vbtab & "<meta name=""twitter:creator"" content=""@eyedoctorsIRL"" />" & vbnewline & _
	vbtab & "<meta name=""twitter:creator:id"" content=""821343739"" />" & vbnewline
End Function
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'~  FCT WriteBreadCrumb                  ~'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
Function WriteBreadCrumb (ByVal BreadCrumbArr)
	Dim b
	Dim crumbcount
	crumbcount = UBound(BreadCrumbArr)
	'
	WriteBreadCrumb = vbtab & vbtab & "<!-- Breadcrumb -->" & vbnewline & _
	vbtab & vbtab & vbtab & "<ol class=""breadcrumb"">" & vbnewline & _
	vbtab & vbtab & vbtab & vbtab & "<li><a href=""/"" title=""Back to Eye Doctors Home page"">Home</a></li>" & vbnewline
	For b = 0 To (crumbcount - 1)
		If Len(BreadCrumbArr(b,1)) then
			WriteBreadCrumb = WriteBreadCrumb & vbtab & vbtab & vbtab & vbtab & "<li><a href="""& BreadCrumbArr(b,1) & """ title="""&  BreadCrumbArr(b,0) & """ >"& BreadCrumbArr(b,0) &"</a></li>" & vbnewline
		Else
			WriteBreadCrumb = WriteBreadCrumb & vbtab & vbtab & vbtab & vbtab & "<li>"& BreadCrumbArr(b,0) &"</li>" & vbnewline
		End if
	Next
	WriteBreadCrumb = WriteBreadCrumb & "<li class=""active"">" & BreadCrumbArr(UBound(BreadCrumbArr),0) &"</li> " & vbnewline
	WriteBreadCrumb = WriteBreadCrumb & vbtab & vbtab & vbtab & "</ol>" & vbnewline & _
	vbtab & vbtab & "<!-- {end} Breadcrumb -->" & vbnewline
End Function
'
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
Function TrimString(Byval Highlight_Desc, Byval intLength, Byval  TrimLink)
	if IsNULL(Highlight_Desc) then
		exit function
	end if
	If len(Highlight_Desc) < intLength then
		TrimString = Highlight_Desc & "&hellip;"
		exit function
	end if
	Highlight_Desc = Trim(Highlight_Desc)
	Highlight_Desc = Replace(Highlight_Desc, vbCrLf, " ")
	Highlight_Desc = Replace(Highlight_Desc, "<br>", " ")
	Highlight_Desc = Replace(Highlight_Desc, "<br/>", " ")
	If Len(Highlight_Desc) > intLength Then
		Dim aryString, a
		Highlight_Desc = Left(Highlight_Desc, intLength)
		aryString = Split(Highlight_Desc, " ")
		Highlight_Desc = ""
		For a = 0 to UBound(aryString)-1
			Highlight_Desc = Highlight_Desc & " " & aryString(a)
		Next
	End If
	TrimString = Highlight_Desc & "&hellip;"
End Function
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
Function TrimTitle(Byval Title, Byval  intLength)
	If IsNULL(Title) then
		exit function
	end If
	Dim aryString
	if len(Title) < intLength then
		TrimTitle = title
	end if
	Title = Trim(Title)
	Title = Replace(Title, vbCrLf, " ")
	Title = Replace(Title, "<br>", " ")
	Title = Replace(Title, "<br/>", " ")
	If Len(Title) > intLength Then
		Title = Left(Title, intLength)
		aryString = Split(Title, " ")
		Title = ""
		Dim a
		For a = 0 to UBound(aryString)-1
			Title = Title & " " & aryString(a)
		Next
		Title = Title & "&hellip;"
	End If
	TrimTitle = Title
End Function
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
Function CreateURLFriendlyDoctor(byval MemberID, byval MemberName,byval MemberTitle)
	if not Isnumeric(MemberID) or MemberID ="" then
		Response.redirect "/opthalmologists/"
		Exit Function
	End If
	if MemberTitle <> "prof" then
		MemberTitle = "doctor"
	else
		MemberTitle = "professor"
	end if
	'CreateURLFriendlyDoctor = "/opthalmologists/eye-doctor.asp?MemberID=" & MemberID
	CreateURLFriendlyDoctor = "/opthalmologists/"& MemberTitle &"/" & MemberID & "/"& SanitiseURLInput(replace(xmlText(MemberName),"&39;"," ")) &"/"
End function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
Function CreateURLFriendlyPR(byval PRID, byval PRDate, byval PRtitle)
	if not Isnumeric(PRID) or PRID ="" then
		Response.redirect "/press-releases/"
		Exit Function
	End If
	If Len(PRtitle) Then
		PRtitle = SanitiseURLInput(TrimTitle(Replace(xmlText(RemoveAllHTMLTags(PRtitle))," ","-"),120))
	End if
	'CreateURLFriendlyPR = "/press-releases/display.asp?PRID=" & PRID
	CreateURLFriendlyPR = "/press-release/"& Replace(PRDate," ","-") &"/"& xmlText(PRtitle) &"/"&  PRID &".html"
End function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
' format: /latest-news/dd-mm-yy/title/Newsid.html
Function CreateNewsFriendyURL(ByVal NewsID ,ByVal PublicationDate, ByVal Title)
	If Not IsNumeric(NewsID) Or NewsID = "" Then
		Response.write "/news/"
		Exit Function
	End If
	'CreateNewsFriendyURL = "/news-events/news.asp?NewsID=" & NewsID
	'Exit Function
	If Not IsDate(PublicationDate) Then
		PublicationDate = now()
	End If
	If Len(Title) Then
		Title = SanitiseURLInput(TrimTitle(Replace(xmlText(RemoveAllHTMLTags(Title))," ","-"),120))
	End if
	CreateNewsFriendyURL = "/latest-news/"& xmlText(day(PublicationDate) &"-"& MonthName(month(PublicationDate),1) &"-"& Right(Year(PublicationDate),2)) &"/"& Title &"/"&  NewsID&".html"
End Function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
' format: /latest-news/dd-mm-yy/title/Newsid.html
Function CreateNewsArchiveFriendyURL(ByVal NewsID ,ByVal PublicationDate, ByVal Title)
	If Not IsNumeric(NewsID) Or NewsID = "" Then
		Response.write "/news/"
		Exit Function
	End If
	'CreateNewsArchiveFriendyURL = "/news-events/news.asp?NewsID=" & NewsID
	'Exit Function
	If Not IsDate(PublicationDate) Then
		PublicationDate = now()
	End If
	If Len(Title) Then
		Title = SanitiseURLInput(TrimTitle(Replace(xmlText(RemoveAllHTMLTags(Title))," ","-"),120))
	End if
	CreateNewsArchiveFriendyURL = "/news-archive/"& xmlText(day(PublicationDate) &"-"& MonthName(month(PublicationDate),1) &"-"& Right(Year(PublicationDate),2)) &"/"& Title &"/"&  NewsID&".html"
End Function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
Function FormatPRDateTime(Mydate)
	if not IsDate(Mydate) then
		exit function
	end If
	FormatPRDateTime = Day(Mydate)
	Select Case(right(day(Mydate),1)) 
		Case 1
			if Day(Mydate)  <> 11 then
				FormatPRDateTime = FormatPRDateTime &  "<sup>st</sup>"
			Else
				FormatPRDateTime = FormatPRDateTime &  "<sup>th</sup>"
			end if
		Case 2
			if Day(Mydate)  <> 12 then
				FormatPRDateTime = FormatPRDateTime &  "<sup>nd</sup>"
			Else
				FormatPRDateTime = FormatPRDateTime &  "<sup>th</sup>"
			end if
		Case 3
			if Day(Mydate)  <> 13 then
				FormatPRDateTime = FormatPRDateTime &  "<sup>rd</sup>"
			end if
		Case Else
			FormatPRDateTime = FormatPRDateTime &  "<sup>th</sup>"
	End Select
	FormatPRDateTime = FormatPRDateTime & " " & MonthName(month(Mydate)) & " " & year(Mydate)
End Function
''
Function FormatUSDateTime(Mydate)
	if not IsDate(Mydate) then
		exit function
	end If
	FormatUSDateTime = MonthName(month(Mydate)) & " " & Day(Mydate) & " " & year(Mydate)
End Function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
Function SanitiseURLInput(aURL)
	if len(aURL) then
		aURL = replace(aURL,"?","")
		aURL = replace(aURL,"#","-")
		aURL = replace(aURL," ","-")
		aURL = replace(aURL,"...","")
		aURL = replace(aURL,"..","")
		aURL = replace(aURL,".","")
		aURL = replace(aURL,"---","-")
		aURL = replace(aURL,"--","-")
	end if
	SanitiseURLInput = aURL
End Function 
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
function FormatDateTimeIRL(adate, aformat)
	if not isDate(adate) then
		FormatDateTimeIRL = ""
		exit function
	end if
	If not Isnumeric(aformat) then
		aformat = 1
	end if
	select case aformat
		case 1
			FormatDateTimeIRL = day(adate) & " " & MonthName(Month(adate)) & " " & year(adate)
		case 2
			FormatDateTimeIRL = day(adate) & " " & Month(adate) & " " & year(adate)
		case else 
			FormatDateTimeIRL = FormatDateTime(adate,aformat)
	end select 
end function
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'
Function Fusio()
	Fusio = FALSE
	if request.servervariables("REMOTE_ADDR") = "213.190.156.91" _
			Or request.servervariables("REMOTE_ADDR") = "89.101.164.184"  _
				Or request.servervariables("REMOTE_ADDR") = "86.46.243.160" _ 
					Or request.servervariables("REMOTE_ADDR") = "83.70.229.76"  _
						Or request.servervariables("REMOTE_ADDR") = "87.232.61.8"  _
							Or request.servervariables("REMOTE_ADDR") = "80.111.226.32"	Then
		Fusio = TRUE
	end if
End Function

%>