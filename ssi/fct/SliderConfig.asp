<%
Dim BannerTitle(5) : Dim BannerText(5) : Dim BannerLink(5) : Dim BannerImage(5)
Dim BannerButtonTitle(5) : Dim BannerTextStyle(5) : Dim BannerImageStyle(5)
Dim ShowTitleOnSlider(5)

'
BannerTitle(1)			= "Eye am what I eat"
BannerText(1)			= "Diabetic eye disease awareness campaign and recipe competition"
BannerLink(1)			= "http://www.eyedoctors.ie/documents/ICO_Vision book_LO-version3.pdf"
BannerImage(1)			= "/images/banner_yoursidght-ourvision.jpg"
BannerButtonTitle(1)	= ""
BannerTextStyle(1)		= ""
BannerImageStyle(1)		= ""
ShowTitleOnSlider(1)	= 1

'
BannerTitle(2)			= "Professional Competence"
BannerText(2)			= "All registered medial practitioners must participate in a professional competence scheme"
BannerLink(2)			= "/members/Professional-Competence.asp"
BannerImage(2)			= "/images/banner1.jpg"
BannerButtonTitle(2)	= ""
BannerTextStyle(2)		= ""
BannerImageStyle(2)		= ""
ShowTitleOnSlider(2)	= 1
'
BannerTitle(3)			= "Your Eyes"
BannerText(3)			= "Our sight is so important that carefully looking after your eyes is essential"
BannerLink(3)			= "/your-eye-health/looking-after-your-eyes.asp"
BannerImage(3)			= "/images/banner3.jpg"
BannerButtonTitle(3)	= "Read More"
BannerTextStyle(3)		= ""
BannerImageStyle(3)		= ""
ShowTitleOnSlider(3)	= 1
'
BannerTitle(4)			= "Your Sight, Our Vision"
BannerText(4)			= "The ICO health information booklet with tips and advice on how to look after your eye health"
BannerLink(4)			= "http://www.eyedoctors.ie/your-eye-health/Your-Sight-Our-Vision.asp"
BannerImage(4)			= "/images/banner_yoursidght-ourvision.jpg"
BannerButtonTitle(4)	= ""
BannerTextStyle(4)		= "padding-top:0px"
BannerImageStyle(4)		= ""
ShowTitleOnSlider(4)	= 1

BannerTitle(5)			= "Eye Am What I Eat"
BannerText(5)			= "Eating For Your Eye Health: Diabetic Eye Disease Awareness Campaign"
BannerLink(5)			= "http://www.eyedoctors.ie/campaigns/Eye-Am-What-I-Eat/"
BannerImage(5)			= "/Banners/eyeAmwhatiEat1.JPG"
BannerButtonTitle(5)	= ""
BannerTextStyle(5)		= ""
BannerImageStyle(5)		= ""
ShowTitleOnSlider(5)	= 1




sub BuildSlider(Byval Admin)
	dim SlotID : SlotID  = 0
	dim BS
	Set BS  = sqlconnection.execute("exec usp_SEL_BannersHP 0")
	Do While Not BS.eof
		If SlotID <>  bs("Slot") Then
			SlotID = bs("Slot")
			'response.write "<br/>sl " & SlotID & " " &   bs("BannerTitle")
			BannerTitle(SlotID)			= bs("BannerTitle")
			BannerText(SlotID)			= bs("BannerText")
			BannerLink(SlotID)			= bs("BannerLink")
			BannerImage(SlotID)			= bs("BannerImage")
			BannerButtonTitle(SlotID)	= bs("BannerButtonTitle")
			BannerTextStyle(SlotID)		= bs("BannerTextStyle")
			BannerImageStyle(SlotID)	= bs("BannerImageStyle")
			ShowTitleOnSlider(SlotID)	= bs("ShowTitleOnSlider")

		End if
	BS.movenext
	Loop
	Set BS = Nothing
End sub
%>