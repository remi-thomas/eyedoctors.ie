<%
Dim BannerTitle() : dim  BannerTitleStyle() : Dim BannerText() : Dim BannerLink() : Dim BannerImage()
Dim BannerButtonTitle() : Dim BannerTextStyle() : Dim BannerImageStyle()
Dim ShowTitleOnSlider() : Dim CurrentBannerID()
Dim TotalSliders : TotalSliders = 0

sub BuildSlider(Byval Admin)
	dim SlotID 		: SlotID  = 0
	dim BS : Set BS  = sqlconnection.execute("exec usp_SEL_BannersHP 0")
	Do While Not BS.eof
		If SlotID <>  bs("Slot") Then
			redim preserve BannerTitle(TotalSliders)
			redim preserve BannerTitleStyle(TotalSliders)	
			redim preserve BannerText(TotalSliders)
			redim preserve BannerLink(TotalSliders)
			redim preserve BannerImage(TotalSliders)
			redim preserve BannerButtonTitle(TotalSliders)
			redim preserve BannerTextStyle(TotalSliders)
			redim preserve BannerImageStyle(TotalSliders)
			redim preserve ShowTitleOnSlider(TotalSliders)
			redim preserve CurrentBannerID(TotalSliders)	

			SlotID = bs("Slot")
			'response.write "<br/>s" & TotalSliders & " " & SlotID & " " &   bs("BannerTitle")
			BannerTitle(TotalSliders)			= bs("BannerTitle")
			BannerText(TotalSliders)			= replace(replace(bs("BannerText"),"<p></p>","<p>&nbsp;</p>"),"<p> </p>","<p>&nbsp;</p>")
			if len(bs("BannerLink")) then
				if left(bs("BannerLink"),3) = "www" then
					BannerLink(TotalSliders)	= "https://" & bs("BannerLink")
				else
					BannerLink(TotalSliders)	= bs("BannerLink")
				end if
			else
				BannerLink(TotalSliders)		= "#"
			end if
			BannerImage(TotalSliders)			= bs("BannerImage")
			BannerButtonTitle(TotalSliders)		= bs("BannerButtonTitle")
			BannerTextStyle(TotalSliders)		= bs("BannerTextStyle")
			BannerImageStyle(TotalSliders)		= bs("BannerImageStyle")
			ShowTitleOnSlider(TotalSliders)		= bs("ShowTitleOnSlider")
			CurrentBannerID(TotalSliders)		= bs("BannerID")
			BannerTitleStyle(TotalSliders)		= bs("TitleStyle")
		TotalSliders = TotalSliders + 1
		End if
	BS.movenext
	Loop
	Set BS = Nothing
	TotalSliders = TotalSliders - 1
End sub
%>