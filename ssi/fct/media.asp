
<%
Function MediaTypeGraph(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
Select Case MediaType
	Case 1
		MediaTypeGraph = "<img src=""/images/media/photo.gif""    alt=""photo"" border=""0"" />"
	Case 2
		MediaTypeGraph =  "<img src=""/images/media/audio.gif""    alt=""audio"" border=""0"" />"
	Case 3
		MediaTypeGraph = "<img src=""/images/media/document.gif""    alt=""document"" border=""0"" />"
	Case 4
		MediaTypeGraph = "<img src=""/images/media/video.gif""    alt=""video"" border=""0"" />"
	Case 5
		MediaTypeGraph = "<img src=""/images/media/pczip.gif""    alt=""PC compressed file"" border=""0"" />"
	Case 6
		MediaTypeGraph = "<img src=""/images/media/macsit.gif""    alt=""Mac compressed file"" border=""0"	
	Case 8
		MediaTypeGraph = "<img src=""/images/media/pdf.gif""    alt=""Adobe&copy; Acrobat"" border=""0"" />"	
	End Select

End Function

Function DocTypeGraph(MedXtension)
	if isNull(MedXtension) or MedXtension = "" then
		exit function
	end if
	Select Case MedXtension
	Case "doc"
		DocTypeGraph = "<img src=""/images/media/word.gif"" width=""35"" height=""18"" alt=""Word Document"" border=""0"" />"
	Case "pdf"
		DocTypeGraph = "<img src=""/images/media/pdf.gif"" width=""29"" height=""12"" alt=""PDF Document"" border=""0"" />"
	Case "xls"
		DocTypeGraph =  "<img src=""/images/media/xls.gif"" width=""35"" height=""18"" alt=""Excel Document"" border=""0"" />"
	Case "ppt"
		DocTypeGraph = "<img src=""/images/media/ppt.gif"" width=""35"" height=""18"" alt=""Power Point document"" border=""0"" />"
	Case "txt","rtf"
		DocTypeGraph = "<img src=""/images/media/txt.gif"" width=""35"" height=""18"" alt=""Text Document"" border=""0"" />"				
	Case Else
		DocTypeGraph =  "<img src=""/images/media/document.gif""    alt=""document "" border=""0"" />"
	End Select

End Function

Function MediaTypeText(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
Select Case MediaType
	Case 1
		MediaTypeText = "IMAGES"
	Case 2
		MediaTypeText =  "AUDIO"
	Case 3
		MediaTypeText = "DOCUMENT"
	Case 4
		MediaTypeText = "VIDEO"						
	End Select
End Function

function DrawAttachedMedia (SupportID,section)
	if IsNumeric(SupportID) and SupportID <> "" then
	'response.write("usp_GET_MediaType " & SupportID &",'"& ucase(section) &"'")
		set rsmed = sqlconnection.execute("usp_GET_MediaType " & SupportID &",'"& ucase(section) &"'")
			if not rsmed.eof then
				DrawAttachedMedia = ""
				Do until rsmed.eof
					Select Case rsmed(0) 
						Case 1
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/images/media/photo.gif""    alt=""photo attached to this "& section &""" border=""0"" />"
						Case 2
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/images/media/audio.gif""    alt=""audio attached to this "& section &""" border=""0"" />"
						Case 3				
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/images/media/document.gif""    alt=""document attached to this Press Release"" border=""0"" />"
						Case 4
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/images/media/video.gif""    alt=""video attached to this "& section &""" border=""0"" />"
						Case 8
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/images/media/Pdf.gif""    alt=""Adobe&copy; Acrobat attached to this "& section &""" border=""0"" />"
						Case Else
							DrawAttachedMedia = DrawAttachedMedia & " "
					End Select
				rsmed.Movenext
				Loop
			end if
			set rsmed = nothing
	end if
End function

function DrawEventAttachedMediaDB (EventID)
	if not IsNumeric(EventID) or EventID = "" then
		exit function
	end if

	set rsmed = sqlconnection.execute("spGetEventMediaList " & EventID & ",1")
		if not rsmed.eof then
			DrawEventAttachedMediaDB = ""
			Do until rsmed.eof
				response.write "<tr>" & VbNewline & _
				"	<td colspan=""2""></td>" & VbNewline & _
				"	<td class=""dbText"">"
					
				response.write MediaTypeGraph(rsmed(5))
				
				response.write " &nbsp;<span class=""mainBlksml"">" & rsmed(2) & "</span> ("& rsmed(4) &" Kb)"  & _
				" " & TrimTitle(DisplayText(rsmed(5)),80)
				response.write "	</td>" & VbNewline & _
				"	<td valign=""top"">" & VbNewline & _
				"<a href=""remove_media.asp?MediaID="& rsmed(0)&"&amp;DocId="& EventID &"&amp;page=edit"" class=""dbText"" onclick=""return confirm('Are you sure you want to remove this media from this PR');""><img src=""/dbadmin/images/icon_delete.gif"" alt=""remove this media from this PR""  border=""0"" width=""15"" height=""15"" /></a>" & VbNewline & _
				"		</td>" & VbNewline & _
				"	</tr>" & VbNewline
			rsmed.Movenext
			Loop
		end if
	set rsmed = nothing
end function

Function GetMediaAnySupport(Section,SupportID,MediaType,maxdisplay,preview)
	if not IsNumeric(SupportID) or SupportID = "" then
		exit function
	end if
	if not IsNumeric(maxdisplay) or maxdisplay = "" then
		maxdisplay = 1
	end if
	if not IsNumeric(MediaType) or MediaType = "" then
		MediaType = 0
	end if
	if not IsNumeric(preview) or preview = "" then
		preview = 1
	end if
	if section = "" then
		exit function
	else
		section = Ucase(section)
	end if

	cntf = 0
	'response.write("sp_ShowMedia '"& section &"'," & SupportID & "," & MediaType)
	'exit function

'Response.Write "sp_ShowMedia '"& section &"'," & SupportID & "," & MediaType

	set rsmed = sqlconnection.execute("sp_ShowMedia '"& section &"'," & SupportID & "," & MediaType)
	'if not rsmed.eof And not rsmed.bof  then
	if not rsmed.eof then
		Do until rsmed.eof and cntf <= maxdisplay
				Caption = DisplayText(rsmed(4))
				if cint(rsmed(5)) = 1 then
					'image'
					if preview = 1 then
						if len(rsmed(3)) > 4 then
							ImgFile = DisplayText(rsmed(3))
						else
							ImgFile = DisplayText(rsmed(2))
						end if
						response.Write "<img src="""& DisplayText(rsmed(1)) & ImgFile & """ alt="""& Caption &""" border=""0"" style=""padding:0; margin:10px;"" />"
					else
						response.Write "<img src="""& DisplayText(rsmed(1)) & DisplayText(rsmed(2)) & """ alt="""& Caption &""" border=""0"" style=""padding:0; margin:10px;"" />"
					end if
				else
					'document or file ?'
					if IsNumeric(rsmed(5)) and rsmed(5) <> "" then
						Select Case cint(rsmed(5))
							Case 2
									track = track + 1
									response.Write "<a href=""" & rsmed(1) & rsmed(2) & """ target=""media"" class=""mainBlksml"" title="""& Caption &""">" & _
									MediaTypeGraph(2) 
									if len(Caption) > 3 then
										response.Write trimtitle(Caption,25)
									else
										response.Write "Listen To Track "& track 
									end if
									response.Write	"</a>" & vbnewline			
							Case 3
								'it s a text document - find correct icon (xls/ppt/pdf)'
								response.Write "<a href=""" & rsmed(1) & rsmed(2) & """ target=""media"" class=""mainBlksml"" title="""& Caption &""">"
								response.Write DocTypeGraph(Right(rsmed(2),3))
								Response.write "</a>"
							Case Else
								response.Write "<a href=""" & rsmed(1) & rsmed(2) & """ target=""media"" class=""mainBlksml"">" & _
								MediaTypeGraph(cint(rsmed(5))) & " " & MediaTypeText(cint(rsmed(5))) & "</a>" & vbnewline			
								response.write "<A HREF=""/media/download.asp?File=" & Server.urlEncode(rsmed(1) & rsmed(2)) & "&Name=" & Server.urlEncode(rsmed(4)) & "&Size=" &  rsmed(6) & """ onMouseOver=""self.status='" & replace(rsmed(4),"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;""><img src=""/images/general/download.gif"" alt=""Download "&  DisplayText(rsmed(4)) &""" width=""70"" height=""18"" border=""0"" alt=""download this media to your HardDrive""/></A>"	

						End Select
						end if
				end if
			rsmed.movenext
				if not rsmed.eof then
					response.Write "<br />"
				end if
			loop
	end if
End Function



Function DrawMediabyPR(DocID)
	if not IsNumeric(DocID) or DocID = "" then
		exit function
	end if
	dim cntf : 	cntf = 0
	dim rsmed 
	set rsmed = sqlconnection.execute("usp_Get_PRMediaList " & DocID & ",1")
	if not rsmed.eof then
		'when display 2 columns (eg: more than 1 record return) split in half'
		Do until rsmed.eof
			response.write "<div id=""MediaList"">" & vbnewline 
			response.write "<ul>" & vbnewline 
			cntf = cntf + 1
				response.write "<li>"
				if rsmed("MediaType") <> 3 then
					response.write MediaTypeGraph(rsmed("MediaType"))
				else
					response.Write DocTypeGraph(Right(rsmed(2),3))	
				end if
				response.write "</li>"
				response.write "<li>"
				response.write Ucase(Right(rsmed(2), 3))
				response.write " - " & rsmed(4) & "Kb"
			response.write "</li>"

			response.write "<li>"
			dim ImgDirectory
			if rsmed("MediaType") = 1 then
				response.write "<span><img src=""" &  rsmed(1) & rsmed(3) & """ alt=""" & trimtitle(rsmed(5),50)& """ /></span>"  & VbNewline 
				ImgDirectory = "jpg/"
			else
				ImgDirectory = ""
			end if

			response.write"<span><a href=""" & rsmed(1) & rsmed(2) & """ class=""mainBlksml"" target=""blank"">" & DisplayText(rsmed(5)) &  "</a></span>"
			response.write "</li>"

			'response.write "<li>"
			'response.write "<A HREF=""/media/download.asp?File=" _
			''	& Server.urlEncode(rsmed(1) & ImgDirectory & rsmed(2)) & "&Name=" & Server.urlEncode(rsmed(2)) & "&Size=" &  rsmed(4) & """ onMouseOver=""self.status='" & replace(rsmed(2),"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;""><img src=""/images/download.gif"" alt="""&  rsmed(2)&""" border=""0"" alt=""download this media to your HardDrive""/></A>"			
			'response.write "</li>"
			
		rsmed.movenext

		response.write "</ul>" & vbnewline 
		response.write "</div>" & vbnewline 
		response.write "<br style=""clear:both;"" />" & vbnewline 

		loop
	end if
End Function


Function DrawAssoMediaGallery(SupportID)
	CloseThisTable = false
	if not IsNumeric(SupportID) or SupportID = "" then
		exit function
	end if
	'get associated media '
	set rsmed = sqlconnection.execute("spMediaAssociated " & SupportID & ",1")
	'get not resized jpg file name'
	set rsjpg = sqlconnection.execute("spMediaDetails " & SupportID ) 
	if not rsjpg.eof then
			AddJPGMediaType	= rsjpg(4)
			AddJpgFullPath	= rsjpg(0) & "jpg/" & rsjpg(1) 
			AddJpgTitle		= replace(DisplayText(rsjpg(3)),"'","")
			AddJpgSize		= rsjpg(11)
	end if

	if (not rsmed.eof) or (not rsjpg.eof) then
		CloseThisTable = true
		response.write "<table style=""margin:2;bottom:2px;"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""98%"" align=""center"" class=""cellheader"">" & VbNewline & _
		"<tr>" & VbNewline & _
		"<td colspan=""6"" class=""cellheader""><img src=""/images/general/trans.gif"" width=""1"" height=""3"" alt="""" border=""0"" /></td>"  & VbNewline & _
		"</tr><tr>"& VbNewline 
	end if
		'AddJpg true/false
		'if AddJPG then
	if not rsjpg.eof  then
		'if len(rsjpg(1)) > 4 and not ISNULL(rsjpg(1)) then
			cntf = 1
			response.Write "<tr>" & VbNewline & _	
			"<td colspan=""6"" class=""celldetail""><img src=""/images/general/trans.gif"" width=""1"" height=""5"" alt="""" border=""0"" /></td>"  & VbNewline & _
			"</tr>" & VbNewline & _
			"<tr>" & VbNewline & _
			"<td class=""celldetail""><img src=""/images/general/trans.gif"" width=""5"" height=""25"" alt="""" border=""0"" /></td>" & VbNewline
			' - icon '
			response.write"			<td align=""left"" valign=""top"" class=""celldetail"">"  & MediaTypeGraph(AddJPGMediaType) & "</td>" & VbNewline

			' - name and link'
			response.write"			<td align=""left"" valign=""top"" class=""celldetail"">"  & VbNewline & _
			 "<A HREF=""/media/download.asp?File=" _
				& Server.urlEncode(AddJpgFullPath) & "&Name=" & Server.urlEncode(AddJpgTitle) & "&Size=" &  AddJpgSize & """ onMouseOver=""self.status='" & replace(AddJpgTitle,"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;"" class=""mainBlksml"">" & AddJpgTitle & "</A>" & VbNewline & _	
			 "</td>" & VbNewline

			' - size'
			response.write"		<td align=""left"" valign=""top"" class=""celldetail"">" &   AddJpgSize & "Kb" & "</td>" & VbNewline

			' - Type'
			response.write"		<td align=""left"" valign=""top"" class=""celldetail"">JPG</td>" & VbNewline


			' - download icon'
			response.write"			<td align=""left"" valign=""top"" class=""celldetail"">"  & VbNewline & _
			  "<A HREF=""/media/download.asp?File="	& Server.urlEncode(AddJpgFullPath) & "&Name=" & Server.urlEncode(AddJpgTitle) & "&Size=" &  AddJpgSize & """ onMouseOver=""self.status='" & replace(AddJpgTitle,"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;"" class=""mainBlksml""><img src=""/images/general/download.gif"" alt="""& AddJpgTitle &""" border=""0"" alt=""download this media to your HardDrive""/></A>" & VbNewline & _	
			 "</td>" & VbNewline & _
			 "</tr>" & VbNewline
		'end if a big jpg is stored
	end if	' not rsjpg.eof
	set rsjpg = nothing
	if not rsmed.eof  then
		Do until rsmed.eof

			If cntf mod 2 then
				tdclass="cellheader"		
			else
				tdclass="celldetail"
			end if

			response.Write "<tr>" & VbNewline & _
			"<td colspan=""6"" class="""& tdclass &"""><img src=""/images/general/trans.gif"" width=""1"" height=""5"" alt="""" border=""0"" /></td>"  & VbNewline & _
			"</tr>" & VbNewline & _
			"<tr>" & vbnewline


			cntf = cntf + 1
					'-spacer'
			response.write"			<td class="""& tdclass &"""><img src=""/images/general/trans.gif"" width=""5"" height=""25"" alt="""" border=""0"" /></td>" & VbNewline
					' - icon '
			response.write"			<td align=""left"" valign=""top"" class="""& tdclass &""">"  & VbNewline
				' call functions above to show doc type icons'
				if rsmed("MediaType") <> 3 then
					response.write MediaTypeGraph(rsmed("MediaType"))
				else
					response.Write DocTypeGraph(Right(rsmed(2),3))	
				end if
			response.write"</td>" & VbNewline

			' - name and link'
			response.write"			<td align=""left"" valign=""top"" class="""& tdclass &""">"  & VbNewline & _
			 "<A HREF=""/media/download.asp?File=" _
				& Server.urlEncode(rsmed(1) & rsmed(2)) & "&Name=" & Server.urlEncode(rsmed(2)) & "&Size=" &  rsmed(4) & """ onMouseOver=""self.status='" & replace(rsmed(5),"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;"" class=""mainBlksml"">" & DisplayText(rsmed(5)) & "</A>" & VbNewline & _	
			 "</td>" & VbNewline

			' - size'
			response.write"		<td align=""left"" valign=""top"" class="""& tdclass &""">" &   rsmed(4) & "Kb" & "</td>" & VbNewline

			' - Type'
			response.write"		<td align=""left"" valign=""top"" class="""& tdclass &""">" &  Ucase(Right(rsmed(2), 3)) & "</td>" & VbNewline


			' - download icon'
			response.write"			<td align=""left"" valign=""top"" class="""& tdclass &""">"  & VbNewline & _
			 "<A HREF=""/media/download.asp?File=" _
				& Server.urlEncode(rsmed(1) & rsmed(2)) & "&Name=" & Server.urlEncode(rsmed(2)) & "&Size=" &  rsmed(4) & """ onMouseOver=""self.status='" & replace(rsmed(5),"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;"" class=""mainBlksml""><img src=""/images/general/download.gif"" alt="""&  rsmed(5)&""" border=""0"" alt=""download this media to your HardDrive""/></A>" & VbNewline & _	
			 "</td>" & VbNewline

			response.write "</tr>" & vbnewline
			rsmed.movenext
			loop
	end if 'eof
	if CloseThisTable then			
		response.write "</table>"
	end if
End Function

function DrawAttachedMediaDB (SupportID,section)
	if not IsNumeric(SupportID) or SupportID = "" then
		exit function
	end if
	if section = "" then
		exit function
	end if
	if lcase(section) = "pr" then
		set rsmed = sqlconnection.execute("usp_Get_PRMediaList " & SupportID & ",1")
	elseif lcase(section) = "news" then
		set rsmed = sqlconnection.execute("spGetNewsMediaList " & SupportID & ",1")
	else 
		exit function
	end if


		if not rsmed.eof then
			DrawAttachedMediaDB = ""
			Do until rsmed.eof
				response.write "<tr>" & VbNewline & _
				"	<td>&nbsp;</td>" & VbNewline & _
				"	<td valign=""top"">" & VbNewline & _
				"		</td>" & VbNewline & _
				"	<td class=""dbText"" valign=""top"">"

				response.Write  MediaTypeGraph(rsmed("MediaType"))
				
				'online or offline media'
				if isNumeric(rsmed(9)) and rsmed(9) <> "" then
					if cint(rsmed(9)) = 0 then
						response.Write "<img src=""/dbadmin/images/cross.gif"" ALT=""This Media is currently offline - switch it on in the media section!"" width=""10"" height=""10"" border=""0"" class='black10u'>"
					elseif cint(rsmed(9)) = 1 then
						response.Write "<img src=""/dbadmin/images/tick.gif"" ALT=""This Media is currently online"" width=""10"" height=""10"" border=""0"" class='black10u'>"
					end if
				end if	
				
			
				
				response.write " &nbsp;<span class=""dbtext"">" & rsmed(2) & "</span> ("& rsmed(4) &" Kb)"  & _
				" <span class=""bluetext""><b>" & TrimTitle(DisplayText(rsmed(5)),80) &  "</b></span>" & VbNewline & _		
				"<a href=""/dbadmin/AttachMedia/remove_media.asp?MediaID="& rsmed(0)&"&amp;SupportID="& SupportID &"&amp;page=edit&amp;section="& section &""" class=""dbText"" onclick=""return confirm('Are you sure you want to remove this media from this event?');""><img src=""/dbadmin/images/delete.gif"" alt=""remove this media from this PR""  border=""0"" width=""13"" height=""16"" /></a>" & VbNewline & _
				"</td>" & VbNewline & _
				"	</tr>" & VbNewline
			rsmed.Movenext
			Loop
		end if
	set rsmed = nothing
end function


Function ThisFileExists(MyPath)
	dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	ThisFileExists = fso.FileExists(Server.MapPath(MyPath))
End Function

Function ReadPage(MyPath)
	if ThisFileExists(MyPath) then
		dim fso
		Set fso = CreateObject("Scripting.FileSystemObject")
		dim objFileContents, strFileContents
		set objFileContents = fso.OpenTextFile(server.mappath(MyPath), 1)
		if objFileContents.AtEndOfStream Then
			'Response.Write "file is empty"
			ReadPage = "file is empty"
		else
			strFileContents = objFileContents.ReadAll
		end if
		ReadPage = strFileContents
		Set objFileContents = Nothing
		Set strFileContents = Nothing
		Set fso = Nothing
	else
		response.Write "Error 404: " & MyPath & "not found ont his server"
	end if
end Function

Function ChkArtistInEvent(ArtistID,EventID)
	if not IsNumeric(ArtistID) or ArtistID = "" then
		ChkArtistInEvent  = false
		exit function
	end if
	if not IsNumeric(EventID) or EventID = "" then
		ChkArtistInEvent  = false
		exit function
	end if
	
	set chk = sqlconnection.execute("SELECT count(ID) FROM Artist_to_Event where ArtistID="& ArtistID &" and EventID=" &EventID)
	if chk(0) > 0 then
		ChkArtistInEvent  = true
	else
		ChkArtistInEvent  = false
	end if
	set chk = nothing
end function


function ChkSponsorInEvent(SponsorID,SupportID,section)
	if not IsNumeric(SupportID) or SupportID = "" then
		ChkSponsorInEvent  = false
		exit function
	end if
	if not IsNumeric(SponsorID) or SponsorID = "" then
		ChkSponsorInEvent  = false
		exit function
	end if
	If lcase(section) = "festival" then
		set chk = sqlconnection.execute("SELECT count(ID) FROM SponsorToFestival where SponsorID="& SponsorID &" and FestivalID=" &SupportID)
	else
		set chk = sqlconnection.execute("SELECT count(ID) FROM SponsorToEvent where SponsorID="& SponsorID &" and EventID=" &SupportID)
	end if

if chk(0) > 0 then
	ChkSponsorInEvent  = true
else
	ChkSponsorInEvent  = false
end if
set chk = nothing
end function
%>