
<footer>
  <div class="container">
    <p>&copy; <%=Year(now())%> Irish College of Ophthalmologists, CRO 151473, 121 Stephen’s Green, Dublin 2, D02 H903 <span>Phone: <a href="tel:35314022777">01 402 2777</a></span><span>Email: <a href="mailto:info@eyedoctors.ie">info@eyedoctors.ie</a></span></p>
    <p class="credit"><a href="http://fusio.net">Web Design &amp; Development by Fusio</a></p>
  </div>
</footer>

<!-- Jquery + Bootstrap JavaScript =========================== -->

<script src="/ssi/js/bootstrap.min.js"></script> 
<script src="/ssi/js/bootstrap-hover-dropdown.min.js"></script>

<script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-9905198-20"); pageTracker._trackPageview(); } catch(err) {}</script>
