


<script>
$(document).ready(function(){
 toggleDropdownSettings();
});

$(window).on('resize', function(){
toggleDropdownSettings();
  });

  function toggleDropdownSettings(){
    //alert("hello");
    var width = getWidth();
   if(width<768){
    $(".dropdown-toggle").attr("data-hover", "none");
    $("#ico-members-menu").attr("href", "/members/sub-menu.asp?SubMenu=members");
    $("#guidelines-menu").attr("href", "/members/sub-menu.asp?SubMenu=guidelines");
    $("#ico-training-menu").attr("href", "/members/sub-menu.asp?SubMenu=training");
  }
  else{
    $(".dropdown-toggle").attr("data-hover", "dropdown");
    $("#ico-members-menu").attr("href", "#");
    $("#ico-training-menu").attr("href", "#");
  }
  }

  function getWidth() {
  if (self.innerHeight) {
    return self.innerWidth;
  }

  if (document.documentElement && document.documentElement.clientHeight) {
    return document.documentElement.clientWidth;
  }

  if (document.body) {
    return document.body.clientWidth;
  }
}
  </script> 

 <header>
 <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
        <p class="navbar-brand"><a href="/" title="Irish College Of Ophthalmologists"><img  src="/images/ICO_logo_2020.png" alt="Irish College of Ophthalmologists - Protecting your vision" width="238" height="94"></a></p>
        <% if fusio() then %>
        <% if Cdate(now()) > Cdate("7 October 2020 23:50") and Cdate(now()) < Cdate("10 October 2020 23:50") then %> 
        <p class="navbar-brand"><img  src="/images/wsdBalloon.jpg" alt="World Sight Day 2020" title="World Sight Day 2020 | Eye Health in the time of COVID" height="94"></a></p>
        <% end if %>
        <% end if %>
      </div>
    </div>
    <div class="navbar-collapse collapse">
      <div class="container">
        <ul class="nav navbar-nav navbar-main">
          <li class="active"><a href="/" title="Go to the Eye Doctors Homepage"><span class="glyphicon glyphicon-home"></span> <span class="home">Home</span></a></li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Your Eye Health <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/your-eye-health/Patient-Information-Leaflets.asp">Patient Information Leaflets</a></li>
              <li><a href="/your-eye-health/looking-after-your-eyes.asp">Looking After Your Eyes</a></li>
              <li><a href="/your-eye-health/Your-Sight-Our-Vision.asp">Your Sight Our Vision</a></li>
              <li><a href="/your-eye-health/eye-conditions.asp">Eye Conditions</a></li>
              <li><a href="/your-eye-health/screening.asp">Screening</a></li>

          

              <li><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html">Refractive Surgery Guideline</a></li>
              <li><a href="/your-eye-health/faq.asp">Your Questions Answered (FAQ)</a></li>
              <li><a href="/your-eye-health/in-an-emergency.asp">In An Emergency</a></li>
              <li><a href="/your-eye-health/support-groups.asp">Patient Support</a></li>
              <li><a href="/your-eye-health/Low-Vision-services.asp">Low Vision Services</a></li>
            </ul>
          </li>
          <li><a  href="/opthalmologists/">Find an Eye Doctor</a></li>
          <li class="dropdown"><a id="test"  href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Health Policy <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
               <li><a href="/Health-Policy/HSE-Primary-Care-Eye-Services-Report.asp">HSE Primary Care Eye Services Report.</a></li>
              <li><a href="/Health-Policy/medical-fitness-to-drive.asp">Medical Fitness to Drive Guidelines</a></li>
              <li class="dropdown-submenu" >
                <a id="clinical-programme-menu" tabindex="-1" href="#"><b>Clinical Programme </b><span class="caret-right"></span></a>
                   <ul class=" dropdown-menu">
                      <!--#include virtual="/ssi/incl/menu-clinical-programme.asp" -->
                  </ul>
              </li>
              <li><a href="/Health-Policy/National-Strategy-for-Vision-Health.asp">National Strategy for Vision Health</a></li>
              <li><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html">Refractive Surgery Guideline</a></li>
              <li><a href="/Health-Policy/Economic-cost-of-Eye-Diseases.asp">The Economic Cost of Eye Diseases</a></li>
              <li><a href="/Health-Policy/Tobacco-Control.asp">Tobacco Control</a></li>
              <li><a href="/Health-Policy/Medical-Advertising-Standards.asp">Direct to Patient Advertising</a></li>
              <!-- li><a href="/Health-Policy/Refractive-Surgery-Guidelines.asp">Refractive Surgery Guidelines</a></li -->
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Trainees <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
               <li><a href="/trainees/a-Career-as-an-Eye-Doctor.asp">Career as an Eye Doctor</a></li>

              <li class="dropdown-submenu" >
                <a id="ico-medical-training-menu" tabindex="-1" href="#"><b>Medical Ophthalmology </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">
                    <!--#include virtual="/ssi/incl/menu-trainee-medical.asp" -->
                </ul>
              </li>

              <li class="dropdown-submenu" >
                <a id="ico-surgical-training-menu" tabindex="-1" href="#"><b>Surgical Ophthalmology  </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">
                    <!--#include virtual="/ssi/incl/menu-trainee-surgical.asp" -->
                </ul>
              </li>

                <!-- li><a href="/trainees/Examinations_Calendar_2018.asp">Examinations Calendar 2018</a></li -->

                 <li><a href="/trainees/CPD_Support_Scheme.asp">CPD - Support Scheme for NCHDs</a></li>
                <li><a href="/trainees/International-Medical-Graduates-Training-Initiative.asp" title="International Medical Graduates Training Initiative">International Medical Graduates Training Initiative</a></li>
                 <li><a href="/trainees/FAQ.asp">FAQ</a></li>
              
              <!-- li><a href="/trainees/International-Medical-Graduate-Programme.asp">International Medical Graduate Programme</a></li -->
            </ul>
          </li>

          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">For Doctors <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">

              <li class="dropdown-submenu" >
                <a id="ico-members-menu" tabindex="-1" href="#"><b>ICO Members </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">
                    <!--#include virtual="/ssi/incl/menu-doctors-members.asp" -->
                </ul>
              </li>

              <li class="dropdown-submenu" >
                <a id="guidelines-menu" tabindex="-1" href="#" ><b>Guidelines </b><span class="caret-right"></span></a>
                <ul class=" dropdown-menu" >
                    <!--#include virtual="/ssi/incl/menu-doctors-guidelines.asp" -->
                </ul>
              </li>
              
              <li><a href="/members/For-GP.asp">For GP's</span></a></li>
              
              <li><a href="/members/Retina-Screen.asp">Retina Screen</a></li>
              <!--<li><a href="/members/children-services.asp">Children's Services</a></li> -->
              <li><a href="/members/useful-links.asp">Useful Links</a></li>
              <li><a href="/members/referring.asp">Referring</a></li>
              <li><a href="/members/Photo-Gallery/">Photo Gallery</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">News &amp; Events <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
            <%
            if cdate(now()) < Cdate("21 May 2016 23:00") then
              response.write "<li><a href=""/killarney2016//"">ICO Conference 2016 - Killarney</a></li>"
            end if%>
          
              <li><a href="/press-releases/">Press Releases</a></li>
              <li><a href="/news-events/">Events</a></li>
            </ul>          
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">About Us <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/About-The-College-Of-Ophthalmologists/who-we-are.asp">Who We Are</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/ICO-Strategic-Plan.asp">Our Vision - Strategic Plan</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/our-structure.asp">Our Structure</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/governance.asp">Governance</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/annual-reports.asp">Annual Reports</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/celebrating-100-years.asp">Celebrating 100 Years</a></li>

              <li><a href="/About-The-College-Of-Ophthalmologists/Press-office.asp">Press Office</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-form" role="search" action="/search/results.asp" name="search" id="search">
          <p><a href="/members/login/" class="btn" target="login" title="Memebrs's Login">Login</a> <a href="/contact-us/" class="btn">Contact Us</a></p>
          <div class="form-group">
            <input type="text" name="q" id="q" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
  </div>
</header>
