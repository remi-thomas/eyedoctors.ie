<!--script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="ed6ba8e2-0196-4822-bcd8-1dabdbc82e00" data-blockingmode="auto" type="text/javascript"></script-->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8QVBJ7');</script>
<!-- End Google Tag Manager -->
<script>
window.addEventListener('CookiebotOnConsentReady', () => {
  Cookiebot.changed && document.location.reload();
})
</script>
<%
If Len(MetaAbstract)  = 0 Then
	MetaAbstract = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" - Protecting your vision "
End If
If Len(Metadescription) = 0 Then
	Metadescription ="The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
End If
If Len(ImgSocialMediaURL) = 0 Then
	ImgSocialMediaURL = DomainName & "/ios-icon.png"
End if
%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<%=Metadescription%>">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="icon" type="image/ico" href="<%=DomainName%>favicon.ico"/>
<link rel="shortcut icon" type="image/x-icon" href="<%=DomainName%>favicon.ico" />
<meta content="<%=DomainName%>favicon.ico" itemprop="image">
<link rel="apple-touch-icon" href="<%=DomainName%>ios-icon.png" />

<meta name="Abstract" content="<%=MetaAbstract%>"/>
<meta name="description" content="<%=Metadescription%>" />
<meta name="keywords" content="Ophthalmologists, College, Ireland, Eye, Doctors, Surgery, Glaucoma, Cataract, Conjunctivitis,Diabetic Retinopathy,Macular Degeneration, Eye Injuries " />
<meta name="AUTHOR" content="Irish College of Ophthalmogists (ICO) - https://eyedoctors.ie" />
<meta http-equiv="Pragma" content="no-cache" />


<%
Response.write BuildOrganicHeader(Array(PageTitle,"Article",Metadescription, ArticleURL,ImgSocialMediaURL))


If InStr(LCase(PageTitle),"eye") = 0 Then
	PageTitle = PageTitle & " | &copy;Eye doctors " & year(Now()) & " | Protecting your vision"
End if
%>
<title><% = PageTitle %></title>


    
<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<!-- Custom styles -->
<link href="/css/eyedoctors.css?version=5.1" rel="stylesheet">
<!-- Google Web Font: Roboto -->
<link href="//fonts.googleapis.com/css?family=Roboto:400,400italic,500,700,700italic" rel="stylesheet">
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/ssi/js/ie10-viewport-bug-workaround.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="/ssi/js/html5shiv.min.js"></script>
    <script src="/ssi/js/respond.min.js"></script>
<![endif]-->

<script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>

<!-- script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script -->
