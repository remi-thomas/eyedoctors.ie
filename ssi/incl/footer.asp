
<footer>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-md-10">
            <p>
            &copy; <%=Year(now())%> Irish College of Ophthalmologists, CRO 151473, 121 Stephen’s Green, Dublin 2, D02 H903 <span>
            <a href="tel:35314022777">01 402 2777</a></span>
            <span><a href="mailto:info@eyedoctors.ie">info@eyedoctors.ie</a></span>
            </p>
          </div>
          <div class="col-md-2">
            <p style="padding-right:1rem;">
            <span><a href="https://www.linkedin.com/company/irish-college-of-ophthalmologists/" target="ext"><img src="/images/LIn-white.png" class="img-responsive" style="max-width: 15px;float:right;margin-right:0.8rem;" alt="ICO is on linkedin"></a></span>
            <span><a href="https://twitter.com/eyedoctorsIRL"><img src="/images/x-white.png" class="img-responsive" style="max-width: 15px;float:right;margin-right: 0.8rem;" alt="ICO is on X"></a></span>
            <span><a href="https://www.instagram.com/eyedoctorsirl/"><img src="/images/Instagram_White.png" class="img-responsive" style="max-width: 15px;float:right;margin-right: 0.8rem;" alt="ICO is on Insta"></a></span>
          </p>
          </div>
      </div>
      <div class="col-md-8 col-sm-6">
        <p class="credit"><a href="https://www.fusio.net/?utm_source=ICO&utm_medium=Website&utm_campaign=ClientLinks" title="Website designed by Fusio" target="fusio">Web Design &amp; Development by Fusio</a></p>
      </div>
      <div class="col-md-4 col-sm-6 text-right" style="font-size:1.3rem">
        <a href="/about-The-College-Of-Ophthalmologists/cookie-policy.asp" title="Read the ICO cookie policy">Cookie Policy</a> | <a href="javascript: Cookiebot.renew()" title="Change your cookie preferences for the ICO website">Cookie Preferences</a>
      </div>
  </div>
</footer>

<!-- Jquery + Bootstrap JavaScript =========================== -->
<script src="/ssi/js/bootstrap.min.js"></script> 
<script src="/ssi/js/bootstrap-hover-dropdown.min.js"></script>

