<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8QVBJ7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<script>
$(document).ready(function(){
 toggleDropdownSettings();
});

$(window).on('resize', function(){
toggleDropdownSettings();
  });

  function toggleDropdownSettings(){
    //alert("hello");
    var width = getWidth();
   if(width<768){
    $(".dropdown-toggle").attr("data-hover", "none");
    $("#ico-members-menu").attr("href", "/members/sub-menu.asp?SubMenu=members");
    $("#guidelines-menu").attr("href", "/members/sub-menu.asp?SubMenu=guidelines");
    $("#ico-training-menu").attr("href", "/members/sub-menu.asp?SubMenu=training");
  }
  else{
    $(".dropdown-toggle").attr("data-hover", "dropdown");
    $("#ico-members-menu").attr("href", "#");
    $("#ico-training-menu").attr("href", "#");
  }
  }

  function getWidth() {
  if (self.innerHeight) {
    return self.innerWidth;
  }

  if (document.documentElement && document.documentElement.clientHeight) {
    return document.documentElement.clientWidth;
  }

  if (document.body) {
    return document.body.clientWidth;
  }
}
  </script> 

 <header>
<div id="skip"><a href="#content" title="Skip the navigation and go to the content directly" tabindex="0">Skip navigation</a></div>
 <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
        <p class="navbar-brand"><a href="/" title="Irish College Of Ophthalmologists"><img  src="/images/ICO_logo_2020.png" alt="Irish College of Ophthalmologists - Protecting your vision" width="238" height="94"></a></p>
        <% 'if request("banner") = 1 then %>
        <% if Cdate(now()) > Cdate("12 May 2024 00:50") and Cdate(now()) < Cdate("18 May 2024 00:50") then %> 
        <p class="navbar-brand"><a href="https://www.eyedoctors.ie/westport2024/"><img  src="/images/westportbanner3.jpg" alt="ICO Annual Conference" title="ICO Annual Conference" class="img-responsive img-campaign" style="display: inline-block"></a></p>
        <% end if %>

        <% if Cdate(now()) < Cdate("14 October 2024 00:50") then %> 
        <p class="navbar-brand"><a href="https://www.eyedoctors.ie/latest-news/10-Oct-24/Pan-Ireland-Ophthalmic-Conference-2024/674.html"><img  src="/images/WSD24-logo-Horizontal_2024.jpg" alt="World Sight Day" title="IWorld Sight Day" class="img-responsive img-campaign" style="display: inline-block;"></a></p>
        <% end if %>

        <%' end if %>
      </div>
    </div>
    <div class="navbar-collapse collapse">
      <div class="container">
        <ul class="nav navbar-nav navbar-main">
          <li class="active"><a href="/" title="Go to the Eye Doctors Homepage"><span class="glyphicon glyphicon-home"></span> <span class="home">Home</span></a></li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Your Eye Health <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/your-eye-health/Patient-Information-Leaflets.asp">Patient Information Leaflets</a></li>
              <li><a href="/your-eye-health/looking-after-your-eyes.asp">Looking After Your Eyes</a></li>
              <li><a href="/your-eye-health/eye-conditions.asp">Eye Conditions</a></li>
              <li><a href="/your-eye-health/screening.asp">Screening</a></li>
              <li><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html">Refractive Surgery Guideline</a></li>
              <li><a href="/your-eye-health/faq.asp">Your Questions Answered (FAQ)</a></li>
              <li><a href="/your-eye-health/in-an-emergency.asp">In An Emergency</a></li>
              <li><a href="/your-eye-health/support-groups.asp">Patient Support</a></li>
              <li><a href="/your-eye-health/Low-Vision-services.asp">Low Vision Services</a></li>
            </ul>
          </li>
          <li><a  href="/opthalmologists/">Find an Eye Doctor</a></li>
          <li class="dropdown"><a id="healthpolicy"  href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Health Policy <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
               <li><a href="/Health-Policy/HSE-Primary-Care-Eye-Services-Report.asp">HSE Primary Care Eye Services Report.</a></li>
              <li><a href="/Health-Policy/medical-fitness-to-drive.asp">Medical Fitness to Drive Guidelines</a></li>

              <li class="dropdown-submenu" >
                <a id="clinical-programme-menu"  href="#"><b>Clinical Programme </b><span class="caret-right"></span></a>
                   <ul class="dropdown-menu">
                    <li>
                      <a href="/Health-Policy/clinical-programme.asp">Model of Care</a>
                    </li>
                    <li>
                     <a href="/Health-Policy/Education-Series-for-the-Integrated-Eye-Care-Team.asp">National Education Series for the Integrated Eye Care Team</a>
                    </li>
                  </ul>
              </li>

              <li><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html">Refractive Surgery Guideline</a></li>
              <li><a href="/Health-Policy/Economic-cost-of-Eye-Diseases.asp">The Economic Cost of Eye Diseases</a></li>
              <li><a href="/Health-Policy/Tobacco-Control.asp">Tobacco Control</a></li>
              <li><a href="/Health-Policy/Medical-Advertising-Standards.asp">Direct to Patient Advertising</a></li>
              <!-- li><a href="/Health-Policy/Refractive-Surgery-Guidelines.asp">Refractive Surgery Guidelines</a></li -->
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Training <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
               <li><a href="/trainees/a-Career-as-an-Eye-Doctor.asp">Career as an Eye Doctor</a></li>

              <li class="dropdown-submenu" >
                <a id="ico-medical-training-menu" tabindex="-1" href="#"><b>Medical Ophthalmology </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">
                    <!--#include virtual="/ssi/incl/menu-trainee-medical.asp" -->
                </ul>
              </li>

              <li class="dropdown-submenu" >
                <a id="ico-surgical-training-menu" tabindex="-1" href="#"><b>Surgical Ophthalmology  </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">
                    <!--#include virtual="/ssi/incl/menu-trainee-surgical.asp" -->
                </ul>
              </li>


                 <!-- li><a href="/members/CPD_Support_Scheme/">CPD - Support Scheme for NCHDs</a></li -->
                <li><a href="/trainees/International-Medical-Graduates-Training-Initiative.asp" title="International Medical Graduates Training Initiative">International Medical Graduates Training Initiative</a></li>
            </ul>
          </li>

          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">For Doctors <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">

              <li class="dropdown-submenu" >
                <a id="ico-members-menu" tabindex="-1" href="#"><b>ICO Members </b><span class="caret-right"></span></a>

                <ul class=" dropdown-menu">


            <%
            if cdate(now()) < Cdate("2 October 2024 23:00") then
              response.write "<li><a href=""/eyecareinfocus/"" title=""eyecareinfocus""><img src=""/images/conference-icon_160X160.png"" height=""20"" alt=""eye care in focus"">Eye Care In Focus</a></li>"
            end if
            %>
                    <!--#include virtual="/ssi/incl/menu-doctors-members.asp" -->
                </ul>
              </li>

              <li class="dropdown-submenu" >
                <a id="guidelines-menu" tabindex="-1" href="#" ><b>Guidelines </b><span class="caret-right"></span></a>
                <ul class=" dropdown-menu" >



                    <!--#include virtual="/ssi/incl/menu-doctors-guidelines.asp" -->
                </ul>
              </li>
               <li><a href="/members/CPD_Support_Scheme/">CPD - Support Scheme for NCHDs</a></li>
              <li><a href="/members/For-GP.asp">For GP's</span></a></li>
              <li><a href="/working-in-ireland/">Working in Ireland</span></a></li>
              <li><a href="/sustainability-in-eye-care/">Sustainability Action in Eye Care</a></li>
              <li><a href="/members/Retina-Screen.asp">Retina Screen</a></li>
              <!--<li><a href="/members/children-services.asp">Children's Services</a></li> -->
              <li><a href="/members/useful-links.asp">Useful Links</a></li>
              <li><a href="/members/referring.asp">Referring</a></li>
              <li><a href="/members/Photo-Gallery/">Photo Gallery</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">News &amp; Events <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
            <%
            if cdate(now()) < Cdate("2 October 2024 23:00") then
              response.write "<li><a href=""/eyecareinfocus/"" title=""eyecareinfocus""><img src=""/images/conference-icon_160X160.png"" height=""20"" alt=""eye care in focus"">Eye Care In Focus</a></li>"
            end if
            %>
          
              <li><a href="/press-releases/">Press Releases</a></li>
              <li><a href="/news-events/">Events</a></li>
            </ul>          
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">About Us <span class="caret"></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/About-The-College-Of-Ophthalmologists/who-we-are.asp">Who We Are</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/ICO-Strategic-Plan.asp">Our Vision - Strategic Plan</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/our-structure.asp">Our Structure</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/governance.asp">Governance</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/annual-reports.asp">Annual Reports</a></li>
              <li><a href="/About-The-College-Of-Ophthalmologists/celebrating-100-years.asp">Celebrating 100 Years</a></li>

              <li><a href="/About-The-College-Of-Ophthalmologists/Press-office.asp">Press Office</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-form" role="search" action="/search/results.asp" name="search" id="search">
          <p>
            <a href="https://training.eyedoctors.ie" class="btn" target="login" title="Training ePortfolio">Training ePortfolio</a> 
            <a href="/members/login/" class="btn" target="login" title="Member's Login">Member's Login</a> <a href="/contact-us/" class="btn">Contact Us</a></p>
          <div class="form-group">
            <input type="text" name="q" id="q" class="form-control" placeholder="Search" aria-label="Search"/>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
  </div>
</header>
<a name="content"></a>