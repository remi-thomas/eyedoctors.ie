// script copyright Fusio //
// info [at] fusio.net //

function validate_form(form_name)
{
	var ThisMemberFirstName = document.searchmember.MemberFirstName.value;
	var ThisMemberName = form_name.MemberName.value;
	if ( ((ThisMemberName == "Last Name") || (ThisMemberName == ""))&& ( (ThisMemberFirstName == "First Name (optional)") || ThisMemberFirstName == "" ))
	{
	alert("To use this search system, enter a member\'s first name, last name or both")
	return false;
	}

	SetValue(form_name)
	return true;
}

function SetValue(form_name)
{
	var ThisMemberName = form_name.MemberName.value;
	if (ThisMemberName == "Last Name")
	{
	form_name.MemberName.value = "";
	}
	var ThisMemberFirstName = document.searchmember.MemberFirstName.value;
	if (ThisMemberFirstName == "First Name (optional)")
	{
	form_name.MemberFirstName.value = "";
	}
}

function rstfirstname()
{
	var ThisMemberFirstName = document.searchmember.MemberFirstName.value;
	var MemberFirstNameDefault = "First Name (optional)";
	if (ThisMemberFirstName == MemberFirstNameDefault)
	{
	document.searchmember.MemberFirstName.value = "";
	}
}

function rstname()
{
	var ThisMemberName = document.searchmember.MemberName.value;
	var MemberDefault = "Last Name";
	if (ThisMemberName == MemberDefault)
	{
	document.searchmember.MemberName.value = "";
	}
}

function stfirstname()
{
	var ThisMemberFirstName = document.searchmember.MemberFirstName.value;
	if (ThisMemberFirstName == "")
	{
	document.searchmember.MemberFirstName.value = "First Name (optional)";
	}
}

function stname()
{
	var ThisMemberName = document.searchmember.MemberName.value;
	if (ThisMemberName == "")
	{
	document.searchmember.MemberName.value = "Last Name";
	}
}
