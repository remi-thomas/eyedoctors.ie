
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
<title>Irish College of Ophthalmologists</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" href="/css/style.css" type="text/css" />
<style>
.database  { background:#ecf2f2; border:1px solid #d4e0e0; padding:1px 5px 5px; width:75% } 
.database  p { clear:left; font-size:14px; } 
.database  p label { display:block; float:left; font-weight:bold; padding-top:2px; width:120px; } 
.database p input { color:#006666; font-size:14px; padding:2px; width:250px } 
.database  p textarea { color:#006666; font-size:14px; padding:2px; width:350px } 
.database  div { margin-left:150px; } 
.database  div input { background:#006666; color:#fff; font-weight:bold; padding:3px 8px; }
.textinput { background:#d5e6f3; border:0; color:#1c598f; font-size:0.8em; }

#databasebg p { margin:10px 0 15px 0; }
#databasebg ul { margin-bottom:10px; }
#databasebg ul li { margin-bottom:6px; padding-left:18px; }
#databasebg dl { margin:15px 0; }
#databasebg dt { background:url(/images/arrow.gif) no-repeat 0 3px; font-size:1.05em; font-weight:bold; padding-left:20px; }
#databasebg dd { line-height:normal; margin-bottom:18px; padding:3px 0 0 20px; }




.practice {border:1px solid #d4e0e0; padding:1px 3px; list-style:none;}
.practice label { display:block; float:left; font-weight:bold; padding-top:2px; width:100px; }


</style>

<script language='JavaScript'>

//#######################################
//##  script copyright fusio.net 2010  ##
//##  by remi at fusio<.>net         ##
//#######################################


function validate_form(form_name)
{
	if (form_name.FirstName.value == "")
	{
		alert("Please enter the member\' first name");
		form_name.FirstName.select();
		return false;
	}

	if (form_name.LastName.value == "")
	{
		alert("Please enter the member\' last name");
		form_name.LastName.select();
		return false;
	}

	if (form_name.Website.value != "")
	{
		var URL = form_name.Website.value;
		//alert("test " + isUrl(URL))
		if (!isUrl(URL))
		{
			alert("The URL you're submitting is not correct.\nAlthough www. is not always necessary it must always start by http://");
			form_name.Website.select();
			return false;
		}
	}


	if (form_name.PrivateEmail.value != "")
	{
		
		var emailStr=form_name.PrivateEmail.value;
		var emailPat=/^(.+)@(.+)$/
		var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
		var validChars="\[^\\s" + specialChars + "\]"
		var quotedUser="(\"[^\"]*\")"
		var ipDomainPat=/^\[(\d{1,6})\.(\d{1,6})\.(\d{1,6})\.(\d{1,6})\]$/
		var atom=validChars + '+'
		var word="(" + atom + "|" + quotedUser + ")"
		var word2 = "(" + validChars + "*|" + quotedUser + ")";
		var userPat=new RegExp("^" + word + "(\\." + word2 + ")*$")

		//var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
		var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
		var matchArray=emailStr.match(emailPat)
		if (matchArray==null) {
			alert("Invalid email address! (error1)")
			form_name.PrivateEmail.select();
			return false;
		}
		var user=matchArray[1]
		var domain=matchArray[2]
		if (user.match(userPat)==null) {
			alert("Invalid email address! (error2)")
			form_name.PrivateEmail.select();
			return false;
		}
		var IPArray=domain.match(ipDomainPat)
		if (IPArray!=null) {
			  for (var i=1;i<=4;i++) {
				if (IPArray[i]>255) {
					alert("Invalid email address! (error3)")
				form_name.PrivateEmail.select();
				return false;
				}
			}
			return true;
		}
		var domainArray=domain.match(domainPat)
		if (domainArray==null) {
			alert("Invalid email address! (error4)")
			form_name.PrivateEmail.select();
			return false;
		}
		var atomPat=new RegExp(atom,"g")
		var domArr=domain.match(atomPat)
		var len=domArr.length
		if (domArr[domArr.length-1].length<2 ||
			domArr[domArr.length-1].length>7) {
		   alert("Invalid email address! (error5)")
		   form_name.PrivateEmail.select();
		   return false;
		}
		if (len<2) {
		   alert("Invalid email address! Address Too Short!")
		   form_name.PrivateEmail.select();
		   return false;
		}
	}


}

function moveIt(fbox, tbox) {
var arrFbox = new Array();
var arrTbox = new Array();
var arrLookup = new Array();
var i;
for (i = 0; i < tbox.options.length; i++) {
arrLookup[tbox.options[i].text] = tbox.options[i].value;
arrTbox[i] = tbox.options[i].text;
}
var fLength = 0;
var tLength = arrTbox.length;
for(i = 0; i < fbox.options.length; i++) {
arrLookup[fbox.options[i].text] = fbox.options[i].value;
if (fbox.options[i].selected && fbox.options[i].value != "") {
arrTbox[tLength] = fbox.options[i].text;
tLength++;
}
else {
arrFbox[fLength] = fbox.options[i].text;
fLength++;
   }
}
arrFbox.sort();
arrTbox.sort();
fbox.length = 0;
tbox.length = 0;
var c;
for(c = 0; c < arrFbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrFbox[c]];
no.text = arrFbox[c];
fbox[c] = no;
}
for(c = 0; c < arrTbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrTbox[c]];
no.text = arrTbox[c];
tbox[c] = no;
   }
}


function sendInfo(formVar,box)
	{
	  formVar.length = 0;
	    var strValue = new String();
	    for(var i=0; i<box.length; i++) {
	        strValue += box[i].value;
	        if (i < box.length-1) {
	            strValue += ",";
	        }
	    }
	    formVar.value = strValue
	}



function isUrl(s) 
{
var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return regexp.test(s);

}

function ShowMe(id)
{
	imgobj = eval('document.IMG' + id);

	Layerobj = document.getElementById("MoreLayer" + id);


	if (document.getElementById)
	{
		obj = document.getElementById(id);

		if (obj.style.display == "none")
		{
			obj.style.display = "";
		}
		else
		{
			obj.style.display = "none";
		}
	}
}


function SetData(AData,Afield)
{
	document.memberdata[Afield].value=AData;
}

function CheckBox(afield)
{
	afield[0].checked = true 
}

</script>


</head>
<body id="trainees">
<div id="pagewidth">
  <div id="header">
    <div id="logo"><img src="/images/ico-logo.gif" width="265" height="79" alt="Irish College of Ophthalmologists" /></div>
    <div id="search"><a href="#" class="larger">A+</a> <a href="#" class="smaller">A-</a> <input type="text" value="Enter Keyword" class="searchfield" /> <input type="submit" value="Search" class="submit" /></div>
    <ul>
      <li id="nHome"><a href="#">Home</a></li> 
      <li id="nAbout"><a href="#">About Us</a></li> 
      <li id="nNews"><a href="#">News &amp; Events</a></li> 
      <li id="nContact"><a href="#">Contact</a></li>
    </ul>
  </div>
  <div id="wrapper" class="clearfix">

      
    <div id="maincol">
      <div id="contentbox">
        <div id="databasebg">
          <h1>Welcome</h1>
          <p>Add yourself to our member database</p>
           <p>Most of the data supply will be shown on the public registry, only the ones clearly stated won't be listed.
		   <br/>Except your name, none of those fields are mandatory. 
		   <br/>The more you add the beter information we will be able to provide to the public.
			
		   </p>


<%
	formaction	="?mode=add"
	AdminUser	=""

%>
	<div style="width:90%;padding:10px;font-weight:bold" class="database">

		<form name="memberdata" action="process.asp method="post" style="margin:0;padding:0" OnSubmit="return validate_form(this)">
			<input type="hidden" name="SpecialtiesSelection" value="">
		<p>
		<label>First Name</label>
		<input name="FirstName" value="<%=FirstName%>" type="text" size="52" class="textinput" />
		</p>
		<p>
		<label>Last Name</label>
		<input name="LastName" value="<%=LastName%>" type="text" size="52" class="textinput" />
		</p>
		<p>
		<label>Title</label>
		<select name="title" class="textinput">
			<option value="<%=Title%>"><%=Title%></option>
			
			<option value="Mr">Mr</option>
			<option value="Ms">Ms</option>
			<option value="Mr">Dr</option>
			<option value="Pr">Pr</option>
			<option value="Mrs">Mrs</option>
			<option value="Miss">Miss</option>
		</select>
		</p>
		<p>
		<label>Diploma</label>
		<input name="Diploma" value="" type="text" size="52" class="textinput" /><font style="font-size:10px;">(<em>M.B., M.Med.Sc, F.R.C.Ophth. etc..</em>)</font>
		</p>
		<p>
		<label>Specialties</label>
		<table>
			<tr>
				<td>
					<select name="SpecialtiesFrom" size=10 style="width:140px;" class="textinput" multiple>
					<%
					
						set rs = sqlconnection.execute("SELECT SpecialtyID,SpecialtyTitle from specialties order by SpecialtyTitle")
					
					Do until rs.eof
						response.Write "<option value=""" & rs(0) & """>"& rs(1) & "</option>" & vbnewline
						rs.movenext
					loop
						%>
					</select>
				</td>
				<td valign="top" width="25">
					<a href="javascript:moveIt(document.memberdata.SpecialtiesFrom,document.memberdata.SpecialtiesTo)">
					<img border="0"  src="/dbadmin/images/icon_arrow_right.gif" width="21" height="16"></a>
					<br/>
					<a href="javascript:moveIt(document.memberdata.SpecialtiesTo,document.memberdata.SpecialtiesFrom)">
					<img border="0" src="/dbadmin/images/icon_arrow_left.gif" width="21" height="16"></a>
					
				</td>
				<td valign="top">
				<select name="SpecialtiesTo" multiple size=10 style="width:140px;" class="textinput">
				</select>
				</td>
			</tr>
			</table>
		</p>
		<p>
		<label>Biog</label>
		<textarea name="Biog" cols="60" rows="3" class="textinput"><%=Biog%></textarea>
		</p>
		<p>
		<label>Website</label>
		<input name="Website" value="<%=Website%>" type="text" size="52" class="textinput" />
		</p>
		<p>
		<label>Email (*)</label>
		<input name="PrivateEmail" value="<%=PrivateEmail%>" type="text" size="52" class="textinput" />
<br/><font style="font-size:12px;color:red">This will NOT be published on the web site</font>		</p>
		<p>
		<label>Phone Number (*)</label>
		<input name="PrivatePhoneNumber" value="<%=PrivatePhoneNumber%>" type="text" size="52" class="textinput" />
		<br/><font style="font-size:12px;color:red">This will NOT be published on the web site</font>
		</p>



		<%
		'count the number of practice for this member 
			'5 practices max contains 11 fields
			Dim PractArr(5,11)
			'set PrivatePublic and online to 0 by default (to pass on the cint check later) 
			For PracticeCounter = 0 to 4
					PractArr(PracticeCounter,10) = 0
					PractArr(PracticeCounter,11) = 0
			next


			
					PractArr(1,0) = 0 'PracticeID
					PractArr(1,1) = "" 'Address
					PractArr(1,2) = "" 'Telephone
					PractArr(1,3) = "" 'Fax
					PractArr(1,4) = "" 'Email
					PractArr(1,5) = "" 'Web
					PractArr(1,6) = "" 'Opening
					PractArr(1,7) = "" 'GeoLat
					PractArr(1,8) = "" 'GeoLong
					PractArr(1,9) = "" 'Notes
					PractArr(1,10) = 0 'PrivatePublic
					PractArr(1,11) = 0 'Online
					PractCount = 1
			
		%>
		<%
		For PracticeCounter = 0 to 4
		%>
			
			<ul class="practice" id="<%=PracticeCounter%>" style="display:none;">
			<li style="list-style:none;"><h3>Practice #<%=PracticeCounter+1%></h3></li>
		
			
			<input name="PracticeID<%=PracticeCounter%>" value="" type="hidden" />
		
			<li>
			
			</P>


			<label>Address:</label>
			<p style="font-size:10px;">(<em>Hospital already in our database</em><br/>
			<!-- Beaumont -->
			<a href="javascript:onClick=SetData('Beaumont Hospital\nBeaumont Road,\nDublin 9','Address<%=PracticeCounter%>');SetData('01 809 3000 ','Telephone<%=PracticeCounter%>');SetData('http://www.beaumont.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.390433','GeoLat<%=PracticeCounter%>');SetData('-6.223263','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Matter">Beaumont</a> |
			<!-- Crumlin -->
			<a href="javascript:onClick=SetData('Our Lady\'s Children\'s Hospital For Sick Children\nCooley Road\nCrumlin,\nDublin 12','Address<%=PracticeCounter%>');SetData('01 409 6100  ','Telephone<%=PracticeCounter%>'); SetData('http://www.olchc.ie','PracticeWeb<%=PracticeCounter%>'); SetData('53.325504','GeoLat<%=PracticeCounter%>'); SetData('-6.31812','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Crumlin">Crumlin</a>	|

			<!-- Eye&amp;Ear -->
			<a href="javascript:onClick=SetData('Royal Victoria Eye and Eay\nAdelaide Road\nDublin 2','Address<%=PracticeCounter%>');SetData('01 6644600','Telephone<%=PracticeCounter%>');SetData('http://www.rveeh.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.333017','GeoLat<%=PracticeCounter%>');SetData('-6.259509','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Eye & Ear">Eye &amp; Ear</a> |
			<!-- James's -->
			<a href="javascript:onClick=SetData('St. James\'s Hospital\nJames\'s Street,\nDublin 8','Address<%=PracticeCounter%>');SetData('01 410 3000','Telephone<%=PracticeCounter%>');SetData('http://www.stjames.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.357962','GeoLat<%=PracticeCounter%>');SetData('-6.238773','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Jame">James'</a> |
			<!-- Mater -->
			<a href="javascript:onClick=SetData('Mater Misericordia University Hospiatl\nEccles Street\nDublin 7','Address<%=PracticeCounter%>');SetData('01 8032000','Telephone<%=PracticeCounter%>');SetData('http://www.mater.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.359037','GeoLat<%=PracticeCounter%>');SetData('-6.268334','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Mater">Mater</a> |

			<!-- Temple St -->
			<a href="javascript:onClick=SetData('Childrens University Hospital\nTemple Street,\nDublin 1','Address<%=PracticeCounter%>');SetData('01 878 4200  ','Telephone<%=PracticeCounter%>');SetData('http://www.cuh.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.356832','GeoLat<%=PracticeCounter%>');SetData('-6.262207','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Temple Street">Temple Street</a> |

			<!-- Vincent's -->
			<a href="javascript:onClick=SetData('St. Vincent\'s University Hospital\nElm park,\nDublin 4','Address<%=PracticeCounter%>');SetData('01 221 4000','Telephone<%=PracticeCounter%>');SetData('http://www.stvincents.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.31695','GeoLat<%=PracticeCounter%>');SetData('-6.21247','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Vincent's">Vincent's</a> 
			<br/>

			<!-- Cork University -->
			<a href="javascript:onClick=SetData('Cork University Hospital\nWilton,\nCork','Address<%=PracticeCounter%>');SetData('021 454 6400','Telephone<%=PracticeCounter%>');SetData('http://www.ucc.ie','PracticeWeb<%=PracticeCounter%>');SetData('51.890','GeoLat<%=PracticeCounter%>');SetData('-8.475','GeoLong<%=PracticeCounter%>'); CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Cork">Cork University</a> |
			<!-- Cork Mercy -->
			<a href="javascript:onClick=SetData('Mercy University Hospital\nGrenville Place,\nCork','Address<%=PracticeCounter%>');SetData('021 427 1971 ','Telephone<%=PracticeCounter%>');SetData('http://www.muh.ie','PracticeWeb<%=PracticeCounter%>');SetData('51.899','GeoLat<%=PracticeCounter%>');SetData('-8.483','GeoLong<%=PracticeCounter%>'); CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Cork">Cork Mercy</a> |
			<!-- Galway -->
			<a href="javascript:onClick=SetData('Galway Univeristy Hospital\nNewcastle Road,\nGalway','Address<%=PracticeCounter%>');SetData('091 544 544','Telephone<%=PracticeCounter%>');SetData('http://www.hse.ie/eng/services/find_a_service/hospscancer/Galway_University_Hospitals/','PracticeWeb<%=PracticeCounter%>');SetData('53.27623','GeoLat<%=PracticeCounter%>');SetData('-9.06774','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Galway">Galway</a> |
			<!-- Letterkenny -->
			<a href="javascript:onClick=SetData('Letterkenny General Hospital\nOakland\'s Park,\nCork','Address<%=PracticeCounter%>');SetData('021 454 6400 ','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('54.9609','GeoLat<%=PracticeCounter%>');SetData('-7.73339','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Letterkenny">Letterkenny</a> |
			<!-- Limerick -->
			<a href="javascript:onClick=SetData('St. John\'s Hospital\nSt. John\'s Square\n,Limerick','Address<%=PracticeCounter%>');SetData('061 46 2222','Telephone<%=PracticeCounter%>');SetData('http://www.stjohnshospital.ie/','PracticeWeb<%=PracticeCounter%>');SetData('52.472302','GeoLat<%=PracticeCounter%>');SetData('-8.431498','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Sligo">Limerick</a> |
			<!-- Sligo -->
			<a href="javascript:onClick=SetData('Sligo General Hospital\nThe Mall,\nSligo','Address<%=PracticeCounter%>');SetData('071 917 1111 ','Telephone<%=PracticeCounter%>');SetData('http://www.sgh.ie','PracticeWeb<%=PracticeCounter%>');SetData('54.274448','GeoLat<%=PracticeCounter%>');SetData('-8.46494','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Sligo">Sligo</a> |
			<!-- Waterford -->
			<a href="javascript:onClick=SetData('Waterford General Hospital\nDunmore Road,\nWaterford','Address<%=PracticeCounter%>');SetData('051 848 000 ','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('52.248332','GeoLat<%=PracticeCounter%>');SetData('-7.079187','GeoLong<%=PracticeCounter%>');CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" class="bluetext" title="Waterford">Waterford</a> )
			
			<textarea cols="50" rows="4" name="Address<%=PracticeCounter%>" class="textinput"/></textarea>


			</li>
			<li>
			<label>Telephone</label>
			<input name="Telephone<%=PracticeCounter%>" value="" type="text" size="52"  class="textinput"/>
			</li>
			<li>
			<label>Fax</label>
			<input name="Fax<%=PracticeCounter%>" value="" type="text" size="52"  class="textinput"/>
			</li>
			<li>
			<label>Email(*)</label>
			<input name="PracticeEmail<%=PracticeCounter%>" value="" type="text" size="52"  class="textinput"/>
			<br/><font style="font-size:12px;color:red">This will be shown to the public beside the practice information</font>
			</li>
			<li>
			<label>Webiste(*)</label>
			<input name="PracticeWeb<%=PracticeCounter%>" value="" type="text" size="52"  class="textinput"/>
			<br/><font style="font-size:12px;color:red">This will be shown to the public beside the practice information</font>
			</li>
			<li>
			<label>Opening</label>
			<textarea name="Notes<%=PracticeCounter%>" cols="50" rows="2" class="textinput"></textarea>
			</li>
			<li>
			<label>Geo Latitude</label>
			<input name="GeoLat<%=PracticeCounter%>" value="" type="text" size="10"  class="textinput"/>
			<em><a href="http://www.gpsvisualizer.com/geocode?location=121 St Stephens Green West, DUBLIN 2, Ireland" target="new">Find your GPS coordinates</a></em>
			</li>
			<li>
			<label>Geo Longitude</label>
			<input name="GeoLong<%=PracticeCounter%>" value="" type="text" size="10"  class="textinput"/>
			</li>
			<li>
			<label>Notes</label>
			<textarea name="Notes<%=PracticeCounter%>" cols="50" rows="3" class="textinput"></textarea>
			</li>
			<li>
			<label>This Practice is a </label>
			<input name="PrivatePublic<%=PracticeCounter%>" value="1" type="radio" class="textinput"  />public hospital 
			or a <input name="PrivatePublic<%=PracticeCounter%>" value="2" type="radio" class="textinput" /> private room
				</li>
		
		
			<input name="Online<%=PracticeCounter%>" value="" type="hidden"   />
			

			<li>
			<%
			If PracticeCounter < 4 And PracticeCounter > 0 Then
			%>
			<br/><a href="JavaScript:onClick=ShowMe('<%=PracticeCounter+1%>')" class="bluetext"><img id="IMG<%=PracticeCounter%>" name="IMG<%=PracticeCounter%>" src="/dbadmin/images/icon_plus.gif" border="0" width="15" height="15" alt="more options?"><span ID="MoreLayer<%=PracticeCounter%>" style="margin:0;">Add another practice </span></a>
			<%
			End if
			%>
			</li>
			</ul> 

		<%
		Next 'PracticeCounter
		%>
		<p>
		<input type="submit" name="btn" value="Save data" class="textinput" onClick="sendInfo(document.memberdata.SpecialtiesSelection,document.memberdata.SpecialtiesTo);"/>
		</p>


		</form>



		<script language="JavaScript" TYPE="text/javascript">
		ShowMe('0')
		<%
		For PracticeCounter = 1 to 4
			If PractArr(PracticeCounter,0) > 0 then
		%>
		ShowMe('<%=PracticeCounter%>')
		<%
			Else
				Exit for
			End if
		Next
		%>
		ShowMe('<%=PracticeCounter%>')
		</script>

		</div>

			  </form>

		</table>



          </dl>
        </div>
      </div>
      
    </div>
    <div id="leftcol">
      <!-- start LEFT MENU -->
      <div id="leftnav">
        <div id="leftnavbg">
          <ul>
            <li id="nTrainees"><a href="#">For Trainees</a>
              <ul>
                <li><a href="#">BST</a></li>
                <li><a href="#">HST</a></li>
                <li class="last"><a href="#">Courses</a></li>
              </ul>
            </li>
            <li id="nMembers"><a href="#">For Members</a>
              <ul>
                <li><a href="#">CPD</a></li>
                <li><a href="#">Conferences</a></li>
                <li><a href="#">Specialist Registration</a></li>
                <li class="last"><a href="#">Dean�s Report</a></li>
              </ul>
            </li>
            <li id="nVisitors"><a href="#">For Visitors</a>
              <ul>
                <li><a href="#">Type of Eye Doctors</a></li>
                <li><a href="#">List of Eye Doctors</a></li>
                <li><a href="#">Eye Conditions</a></li>
                <li><a href="#">FAQs</a></li>
                <li class="last"><a href="#">Emergencies</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <!-- end LEFT MENU -->
      <div id="leftcontact">
        <p>Irish College of Ophthalmologists, <br />
          121 St. Stephens Green, Dublin 2 <br />
          <span>Tel: +353 1 402 2777<br />
          Fax: +353 1 402 2778<br />
          Email: <a href="mailto:ico@rcsi.ie">ico@rcsi.ie</a></span></p>
      </div>
    </div>
  </div>
  <div id="footer">
    <div id="footerbg" class="clearfix">
      <p>&copy; Irish College of Ophthalmologists 2009.  Web Design &amp; Development by Fusio</p>
      <div></div>
    </div>
  </div>
</div>
</body>
</html>
