<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Eye Doctors - Irish College Of Ophthalmologists"
SiteSection       = "Home" ' highlight correct menu tab'
'______________________________________________________________________|'
'|                                                                     |'
'______________________________________________________________________|'
%>
<!doctype html>
<html lang="en">
<head>
  <!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>

<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->
<!-- HOMEPAGE: Banner [NEW] ================================== -->
<!--#include virtual="/ssi/fct/Slider2018Config.asp" -->
<% 
'call the function to build the slider'
BuildSlider(0)
dim AltBanner
%>

<div class="banner">
  <div class="container"> 
	<div class="row">
  	  <div class="col-md-8">
		<!-- HOMEPAGE: Carousel -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Carousel: Indicators -->
			<ol class="carousel-indicators">
        <% 
        dim i
        for i = 0 to TotalSliders

        response.write vbtab & "<li data-target=""#myCarousel"&i&""" data-slide-to="""&i&""" class=""" 
          if i = 0 then 
            response.write "active"
          end if
          response.write """></li>"  & vbnewline
        next
        %>
			  
			</ol>
			<!-- Carousel: Banners -->
			<div class="carousel-inner">
          <%
          for i = 0 to TotalSliders

          if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
          AltBanner = BannerTitle(i)
          else 
          if Len(BannerText(i)) Then
          AltBanner = BannerText(i)
          else
          AltBanner = "Irish College of Ophthalmologists - " & Year(Now())
          end if
          end if


          response.write  vbtab & "<div class=""item clearfix "
          if i = 0 then 
            response.write "active"
          end if
          response.write """"

          if Len(BannerTextStyle(i))  then 
          response.write " style="""& BannerTextStyle(i) &""""
          end if

          response.write ">" & vbnewline



          if  CurrentBannerID(i) = 196 then
            response.write vbtab & vbtab & "<div class=""carousel-text videovimeo"">"  & vbnewline
            response.write "<a href=""#"" data-toggle=""modal"" data-target=""#myModal""><img src=""/banners2018/trainee-video.jpg""/></a>" 
            
            response.write "</div>" & vbnewline

          else 

          if len(BannerImage(i)) Then
          response.write vbtab & "<a href="""& BannerLink(i) &""" title=""" & AltBanner &""" class=""carousel-img"">"
          response.write vbtab & "<img src=""" & replace(lcase(BannerImage(i)),"banners/","banners2018/") & """ alt=""" & AltBanner &""" title=""" & AltBanner &""">"
          response.write vbtab & "</a>"  & vbnewline
          end if


            response.write vbtab & vbtab & "<div class=""carousel-text"">"  & vbnewline
            if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
            response.write vbtab & vbtab & vbtab & "<h2><a href="""& BannerLink(i) &""" title=""" & AltBanner &""">"& BannerTitle(i) &"</a></h2>"
            end if

            Response.write vbtab & vbtab & vbtab &  "<p>"  & vbnewline
            Response.write vbtab & vbtab & vbtab & "<a href=""" & BannerLink(i) &""">" & BannerText(i) & "</a>"   & vbnewline
            Response.write vbtab & vbtab & vbtab &  "</p>"  & vbnewline
          response.write vbtab & vbtab & "</div>"  & vbnewline
          end if

          response.write vbtab  & "</div>"  & vbnewline
          next
          %>



			</div>
			<!-- Carousel: Controls --> 
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" alt="Previous Slide"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" alt="Next Slide"><span class="icon-next"></span></a> 
		</div>
	  </div>
  	  <div class="col-md-4">
      <!-- EVENTS ================================================== -->
	    <div class="box events">
		  <h2><a href="/news-events/">Meetings &amp; Events</a></h2>
      <%
        set rs = sqlconnection.execute("exec usp_sel_forthcoming_events NULL")
        if not rs.eof then
          dim cntNews : cntNews = 0
        %>
        <ul>
          <% 
          do while not rs.eof and cntNews < 3
          cntNews = cntNews + 1
            response.write "<li class=""clearfix""><a href="""&  CreateNewsFriendyURL(rs("EventID"),FormatUSDateTime(rs("StartDate")),rs("Title")) & """ title="""& rs("Title") &""">" & _
            "<em class=""date""><span>"& Day(rs("StartDate")) &"</span> "& MonthName(Month(rs("StartDate")),true) &"</em>"   & _
            " <strong>"& TrimTitle(rs("Title"),50)& "</strong></a></li>" & vbnewline
          rs.movenext
          loop
          %>
        </ul>
        <%
        end if
        %>

		  <p><a href="/news-events/" class="btn">View all Events</a></p>
  	    </div>
	  </div>
	</div>
  </div>
</div>

<!-- CONTENT AREA ============================================ -->
<div class="container"> 
  <!-- HOMEPAGE: News -->
  <div class="row homecols">

    <div class="col-sm-6 col-md-3">
      <h2><a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"> Professional Competence</a></h2>
      <div class="box"> <a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" title="Learn more about the ICO Professional Competence Scheme and your PCS requirements"><img src="/images/cpd.jpg" alt="Learn more about the ICO Professional Competence Scheme" title="Learn more about the ICO Professional Competence Scheme"></a>
        <p> Learn more about the ICO Professional Competence Scheme and your PCS requirements</p>
        <p><a href="https://www.eyedoctors.ie/members/Professional-Competence.asp" class="btn">Professional Competence </a></p>
      </div>
    </div>

    <!-- div class="col-sm-6 col-md-3">
      <h2><a href="/members/sub-menu.asp?SubMenu=guidelines">Guidelines for Doctors</a></h2>
      <div class="box list">
        <ul>
          <li class="clearfix"><a href="/members/Fitness-to-Drive-Guidelines.asp" title="Medical Fitness to Drive Guidelines">Medical Fitness to Drive Guidelines</a></li>
          <li class="clearfix"><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html" title="Guidelines for Refractive Eye Surgery">Guidelines for Refractive Eye Surgery</a></li>
          <li class="clearfix"><a href="/members/ICO-Guidelines-on-the-Consent-Process.asp" title="ICO Guidelines on the Consent Process">ICO Guidelines on the Consent Process</a></li>
          <li class="clearfix"><a href="/Health-Policy/Medical-Advertising-Standards.asp" title="Advertising and Marketing Guidelines">Advertising and Marketing Guidelines</a></li>
        </ul>
        <p><a href="/news-events/" class="btn">View all Guidelines</a></p>
      </div>
    </div -->
    <div class="col-sm-6 col-md-3">
      <h2><a href="/your-eye-health/eye-conditions.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma">Eye Conditions</a></h2>
      <div class="box"> <a href="/your-eye-health/eye-conditions.asp" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"><img src="/images/homepic1.jpg" alt="eye conditions" title="Learn about common conditions like Cataract, Conjunctivitis and Glaucoma"></a>
        <p>Learn about common conditions like Cataract, Conjunctivitis and Glaucoma.</p>
        <p><a href="/your-eye-health/eye-conditions.asp" class="btn">Common Conditions</a></p>
      </div>
    </div>
    <div class="clearfix visible-sm-block"></div>
    <div class="col-sm-6 col-md-3">
      <h2><a href="/opthalmologists/" title="Eye Doctors Directory">Find an Eye Doctor</a></h2>
      <div class="box"> <a href="/opthalmologists/" title="Eye Doctors Directory"><img src="/images/homepic2.jpg" alt=""></a>
        <p>Our directory contains contact information for eye doctors practicing in Ireland. </p>
        <p><a href="/opthalmologists/" class="btn" title="Eye Doctors Directory">List of Eye Doctors</a></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <h2><a href="https://twitter.com/eyedoctorsIRL" target="twitter">Tweets</a></h2>

  
      <div class="box twitter">
        <a class="twitter-timeline" href="https://twitter.com/eyedoctorsIRL" data-widget-id="507485962692067328" data-theme="light" data-chrome="nofooter transparent noheader" height="240">Tweets by @eyedoctorsirl</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

      </div>



    </div>
  </div>
</div>

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ico" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          National Training Programme in Medical Ophthalmology
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
        </div>
        <div class="modal-body">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/605642188?dnt=1" allowfullscreen></iframe></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

  

<style>
/*https://codepen.io/JacobLett/pen/xqpEYE*/
.videovimeo
{
background-color:#63bbbd;
}

.videovimeo img {
    display: block;
 }


.modal-dialog {
      max-width: 1000px;
      margin: 30px auto;
  }

.modal-body {
  /*position:relative;*/
  padding:0px;
}


.close {
  /*position:absolute;
  right:-30px;
  top:0;
  z-index:999;color:#fff;*/
  font-size:2rem;
  font-weight: normal;
  
  opacity:1;
}


.modal-backdrop.in {
    opacity: 0.9;
}

</style>

</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->