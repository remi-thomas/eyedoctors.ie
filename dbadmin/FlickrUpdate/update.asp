<%
StatusRequired = 9
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->

<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp" -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script src="/ssi/scripts/members_search_lib.js" type="text/javascript"></script>

</head>

<body>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->




<%
API_KEY		= "ea114a151e1386e015a875d79a5a1fe0"
secret		= "dd451bdbe0b035d7"
PHOTOSET_ID	= "72157625355702404"
user_id		= "55767928@N06"
GaleryHTML	= ""

			ICOPhotoSetURL = "https://www.flickr.com/services/rest/?method=flickr.photosets.getList&api_key="&API_KEY &"&user_id="&User_ID
			Dim PhotoSetArr

			FlickrSets = flickrListPhotosets(ICOPhotoSetURL)
			if isArray(FlickrSets) then
				GaleryHTML = GaleryHTML &  "<ul class=""gallerysets"">" & vbnewline
				for st = 0 to UBound(FlickrSets)-1
					GaleryHTML = GaleryHTML & vbtab & "<li>" &  vbnewline
						'## Each FlickrSets(st) is made of 4 elements as per below
						'## |PhotoSetID|PhotosetTotal|PhotosetTitle|PhotosetDescription
						PhotoSetArr = Split(FlickrSets(st),",")

						GaleryHTML = GaleryHTML & "<h2>" & PhotoSetArr(2) & "</h2>" & vbnewline
						'GaleryHTML = GaleryHTML & PhotoSetArr(0)

						PHOTOSET_ID = PhotoSetArr(0)
						FlickrPhotos = flickrGetPhotosetsPhoto(flickrGetUrlPhotosets("",1,6,1,"photos",PHOTOSET_ID))
						if isArray(FlickrPhotos) then
							GaleryHTML = GaleryHTML & vbtab & vbtab & "<ul class=""clearfix"">" & vbnewline
							for i=Lbound(FlickrPhotos) to UBound(FlickrPhotos)
								if isArray(FlickrPhotos(i)) then
									d = FlickrPhotos(i)

									'GaleryHTML = GaleryHTML & "<br/>" &  FlickrSets(i)
									GaleryHTML = GaleryHTML & vbtab & vbtab & vbtab & "<li>"&_
										"<a href=""/members/photo-gallery/image.asp?PhotoSet_ID="& PHOTOSET_ID &"&amp;PhotoID="&d(4)&""" title="""+d(0)+""">"&_
										"<img src="""+d(2)+""" alt="""+d(0)+""" />"&_
										"</a>" &_
										"</li>" & vbnewline
								end if
							Next

							GaleryHTML = GaleryHTML & vbtab & vbtab & "</ul>" & vbnewline
						Else
							GaleryHTML = GaleryHTML & "error photoset #" & PhotoSetArr(0)
						end If

						GaleryHTML = GaleryHTML & "<p><em>" & PhotoSetArr(3) & "</em></p>"
						If CInt(PhotoSetArr(1)) > 5 Then
							GaleryHTML = GaleryHTML & vbtab & vbtab & vbtab & "<p>"&_
							"<a href=""display.asp?PhotoSet_ID=" & PHOTOSET_ID & """>This set contains " & CInt(PhotoSetArr(1))& " photos &raquo;" &_
							"</a>" &_
							"</p>" & vbnewline
						End if


					GaleryHTML = GaleryHTML & vbtab & "</li>" & vbnewline
				next
				GaleryHTML = GaleryHTML &  "</ul>" & vbnewline
			Else
				GaleryHTML = GaleryHTML & "<h2>error for https://www.flickr.com/services/rest/?method=flickr.photosets.getList</h2><br/>" & isArray(FlickrSets) & " - " & Now()
			end if


			GaleryHTML = GaleryHTML & vbnewline & vbnewline & vbtab & vbtab & "<!-- last update " & now() & " -->" & vbnewline 

		response.write GaleryHTML

		call Copy_file("/members/Photo-Gallery/Photo-Gallery.bck.html","/members/Photo-Gallery/Photo-Gallery.html")

		call Write_Page("Photo-Gallery.html", GaleryHTML, "/members/Photo-Gallery/")
%>
		
<%
' get the API url flickr.photosets.getList
function flickrListPhotosets(ICOPhotoSteURL)
	'GaleryHTML = GaleryHTML & URL

	dim PhotoSetsArr()

	dim xmlhttp,intTimeout
	intTimeout = 5000

	on error resume next
	set xmlhttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttp.Open "GET", ICOPhotoSteURL, false
	xmlhttp.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttp.Send
	if err <> 0 then
		GaleryHTML = GaleryHTML & "ERROR"
		exit function
	End if
	set source1 = Server.CreateObject("Microsoft.XMLDOM")
	source1.async = false
	source1.setProperty "SelectionLanguage", "XPath"
	source1.loadxml(xmlhttp.ResponseText)
	Set PhotosNodes = source1.selectNodes("/rsp/photosets/photoset")
	photoCount = 0

	For Each PhotoNodeItem in PhotosNodes
		'GaleryHTML = GaleryHTML & photoCount
		redim preserve PhotoSetsArr(photoCount+1)
		'Photoset ID | Secret | Photos ttl

		PhotoSetID		= PhotoNodeItem.getAttribute("id")
		PhotosetTotal	= PhotoNodeItem.getAttribute("photos")
		PhotosetTitle = PhotoNodeItem.getElementsByTagName("title").item(0).text
		PhotosetDescription = PhotoNodeItem.getElementsByTagName("description").item(0).text

		PhotoSetsArr(photoCount) =  PhotoSetID & "," & PhotosetTotal & "," & PhotosetTitle & "," & PhotosetDescription

		'GaleryHTML = GaleryHTML & "<br/> " & photoCount & ": " & PhotoSetID & " | " & PhotosetTotal  & " | " & PhotosetTitle  & " | " & PhotosetDescription

		'For i = 0 To UBound(PhotoSetsArr)
		'	GaleryHTML = GaleryHTML & "<br/>" & PhotoSetsArr(i)
		'next
		photoCount = photoCount + 1
	Next

	Set PhotoNodeItem = Nothing
	Set PhotosNodes = Nothing
	Set source1 = Nothing

	flickrListPhotosets = PhotoSetsArr
end function


' get the API url flickr.photosets.getPhotos
function flickrGetUrlPhotosets(extras,privacy_filter,per_page,page,media,PHOTOSET_ID)
	url = "https://www.flickr.com/services/rest/?method=flickr.photosets.getPhotos" &_
						   "&api_key="&API_KEY &_
						   "&photoset_id="&PHOTOSET_ID
	if not isEmpty(per_page) then url = url & "&per_page="&per_page
	if not isEmpty(page) then url = url & "&page="&page
	if not isEmpty(media) then url = url & "&media="&media
	if not isEmpty(privacy_filter) then url = url & "&privacy_filter="&privacy_filter
	if not isEmpty(extras) then url = url & "&extras="&extras

	'GaleryHTML = GaleryHTML & URL

	flickrGetUrlPhotosets = url

end function


' get the ASPI url flickr.photos.getInfo
function flickrGetUrlPhotoInfo(photo_id,secret)
	url = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo" &_
		"&api_key="&API_KEY &_
		"&photo_id="&photo_id &_
		 "&secret="&secret
	flickrGetUrlPhotoInfo = url
end function

' get the ASPI url flickr.photos.getInfo
function flickrGetUrlPhotoInfo(photo_id,secret)
	url = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo" &_
						   "&api_key="&API_KEY &_
						   "&photo_id="&photo_id &_
						   "&secret="&secret
	flickrGetUrlPhotoInfo = url
end Function

' retrive photo information
' return an array (title, description, thumb, preview, ID)
Function flickrGetPhotoInfo(flickrAPIUrl)

	dim xmlhttpPhoto,intTimeout
	intTimeout = 5000
	on error resume next
	set xmlhttpPhoto = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttpPhoto.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttpPhoto.Open "GET", flickrAPIUrl, false
	xmlhttpPhoto.Send
	if err <> 0 then exit function

	dim xmlPhotoDetail
	set xmlPhotoDetail = Server.CreateObject("Microsoft.XMLDOM")
	xmlPhotoDetail.async = false
	xmlPhotoDetail.setProperty "SelectionLanguage", "XPath"
	xmlPhotoDetail.loadxml(xmlhttpPhoto.ResponseText)

	dim dPhoto(5)

	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo/title")
	For Each photoDetailNode in photoNode
		dPhoto(0) = photoDetailNode.Text
	next

	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo/description")
	For Each photoDetailNode in photoNode
		dPhoto(1) = photoDetailNode.Text
	next

	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo")
	For Each photoDetailNode in photoNode
		photourl = "https://farm" & photoDetailNode.getAttribute("farm") & ".static.flickr.com/" &_
									photoDetailNode.getAttribute("server") & "/" &_
									photoDetailNode.getAttribute("id") & "_" &_
									photoDetailNode.getAttribute("secret")
		dPhoto(2) = photourl & "_s.jpg"
		dPhoto(3) = photourl & ".jpg"
		dPhoto(4) = photoDetailNode.getAttribute("id")
	next


	Set photoDetailNode = Nothing
	Set photoNode = Nothing
	Set xmlPhotoDetail = Nothing
	flickrGetPhotoInfo = dPhoto
End Function


' retrive photos in a photosets
' return array
function flickrGetPhotosetsPhoto(flickrAPIUrl)
	'url format
	dim result()

	dim xmlhttp,intTimeout
	intTimeout = 5000

	on error resume next
	set xmlhttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttp.Open "GET", flickrAPIUrl, false
	xmlhttp.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttp.Send
	if err <> 0 then exit function

	set source1 = Server.CreateObject("Microsoft.XMLDOM")
	source1.async = false
	source1.setProperty "SelectionLanguage", "XPath"
	source1.loadxml(xmlhttp.ResponseText)

	Set PhotosNodes = source1.selectNodes("/rsp/photoset/photo")

	photoCount = 0
	For Each PhotoNodeItem in PhotosNodes
		redim preserve result(photoCount+1)
		result(photoCount) = flickrGetPhotoInfo(flickrGetUrlPhotoInfo(PhotoNodeItem.getAttribute("id"),PhotoNodeItem.getAttribute("secret")))
		photoCount = photoCount + 1
	Next

	Set PhotoNodeItem = Nothing
	Set PhotosNodes = Nothing
	Set source1 = Nothing

	flickrGetPhotosetsPhoto = result

end function

%>


<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<p>
  <!--#include virtual="/ssi/dbclose.asp" -->
</p>
</body>
</html>
