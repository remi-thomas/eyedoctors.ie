<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->
<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="/css/admin.css" type="text/css">

<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/dbadmin/ssi/lib/jquery-ui-1.10.1.custom.min.js"></script>
<link href="/dbadmin/ssi/lib/ui-lightness/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />


<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>

<style>
div.ui-datepicker{
 font-size:10px;
}
</style>


<title>ICO - Website Administration </title>
</head>
<body class="body">

<script  language="JavaScript" type="text/javascript">
$(function() {
$( ".dpicker" ).datepicker({
changeMonth: true,
changeYear: true,
yearRange: '-2:+2',
maxDate: '+2y',
dateFormat: 'dd MM yy'
});
});
</script>


<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
'############### upload vars ########################################'
Dim PID,barref, UploadProgress
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = "/dbadmin/ssi/ImgUpl/framebar.asp?to=10&" & PID
%>
<script language="JavaScript" type="text/javascript">
function ShowProgress()
{
  strAppVersion = navigator.appVersion;
  if (document.UploadMap.Map.value != "")
  {
    if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
    {
      winstyle = "dialogWidth=375px; dialogHeight:130px; center:yes";
      window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
    }
    else
    {
      window.open('<% = barref %>&b=NN','','width=370,height=115', true);
    }
  }
  return true;
}
</script>



<%
BannerID = request.querystring("BannerID")


if IsNumeric(BannerID) and BannerID <> "" then
	Set rs = sqlconnection.execute("exec usp_GET_BannerDetails " & BannerID & ",1")
	'response.write "exec usp_SEL_EventBoard " &  Session("AdminID") & "," & BannerID & ",1"
	If not rs.eof then
		BannerTitle			= rs("BannerTitle")
		BannerText			= rs("BannerText")
		BannerLink			= rs("BannerLink")
		BannerImage			= rs("BannerImage")
		BannerOnline		= rs("BannerOnline")
		BannerTab			= rs("BannerTab")
		BannerButtonTitle	= rs("BannerButtonTitle")
	end if
else

end if
If not isDate(StartDate) then
	StartDate = date() + 1
	StartTime = date() & " 20:00:00"
end If
If not isDate(EndDate) then
	EndDate = date() + 1
	EndTime = date() & " 20:00:00"
end if
%>

<p>
<%
If IsNumeric(BannerID) and BannerID <> "" Then
	response.Write "<h1>Edit Banner</h1>" & vbnewline
	response.Write session("AdminUserName") & " you can edit your event by using the form below"
Else
	response.Write "<h1>Add new Banner</h1>" & vbnewline
	response.Write "To add a new event fill out the form below:"
end if
%>
</p>



<%
If IsNumeric(BannerID) and BannerID <> "" Then
%>
<h3>Banner Preview</h3>
<link rel="stylesheet" href="/css/slideshow.css" type="text/css" />
<style>
/* HOMEPAGE SPECIFIC */
#slideshow { border:1px solid #a7c9e2; margin-bottom:30px; padding:3px; }
#slideshowbg { background:#d6e7f4; height:223px; position:relative; width:662px; }
</style>
<div  style="padding-right:10px; width:680px;overflow:hidden;">
      <!-- start HOMEPAGE SLIDESHOW -->
      <div id="slideshow">
        <div id="slideshowbg">
          <div id="container" class="karmic_flow_container">
            <ul class="karmic_flow_slider">
				 <li class="karmic_flow_slides karmic_flow_slide_selected" id="slide1">

					<%
					'Is the whole image a link??

					If Len(BannerButtonTitle) < 1 And Len(BannerImage)>5 Then
						response.write "<a href="""&BannerLink&""" title="""& BannerTitle & """ />" & _
						"<img src=""" & BannerImage& """"
						If Len(BannerText) Then
							Response.write " alt="""& BannerText & """ title="""& BannerText & """ "
						Else
							Response.write " alt="""& BannerTitle & """ title="""& BannerTitle & """ "
						response.write " border=""0""></a>"
						End If
					Else
						response.write "<p>"
						' title
						If Len(BannerTitle) > 1 Then
							Response.write "<strong>" & BannerTitle & "</strong>"
							If Len(BannerText) Then Response.write "<br/>" & vbnewline
							AltImg = BannerTitle
						End If
						' text
						If Len(BannerText) Then
							Response.write BannerText & "<br/>" & vbnewline
							AltImg = BannerTitle
						end if
						' link
						If Len(BannerLink) Then
							If Len(BannerButtonTitle) < 2 Then
								BannerButtonTitle = "Read More"
							End if
							Response.write "<a href=""" & BannerLink &""">" & BannerButtonTitle & "<img src=""images/button-arrow.gif"" width=""8"" height=""13"" alt="""" /></a>"
						end if
						response.write "</p>"
						' image
						If Len(BannerImage)>5 THEN
							response.write "<img src="""&BannerImage & """ alt=""" & AltImg & """ />"
						End If
					End If
						%>
					</li>

            </ul>
          </div>
          <a class="karmic_flow_play_controller" href="#" target="container">Play/Pause</a>
			<span class="controller">
				<a class="karmic_flow_controller" href="#slide1" target="container"><%=BannerTab%></a>
		  		<a class="karmic_flow_controller" href="#slide2" target="container">Lorem Ipsum</a>
				<a class="karmic_flow_controller" href="#slide3" target="container">Quick Brown Fox</a>
				<a class="karmic_flow_controller" href="#slide4" target="container">Jumping Yankee</a>
			</span>
			</div>
      </div>

</div>
<br style="clear:both"/>

<%
End If
%>
<h3>Banner Details</h3>
<form name="events" action="process.asp?<% = PID %>" method="post"  ENCTYPE="multipart/form-data" OnSubmit="return ShowProgress();">
  <table cellpadding="3" cellspacing="5" border="0">
    <tr>
      <th scope="row">Title:</th>
      <td><input type="text" size="65" name="BannerTitle" class="TextInput" value="<%=BannerTitle%>" maxlength="60"/>
	</td>
	</tr>
    <tr>
      <th scope="row">Description</th>
      <td>
			<textarea name="BannerText" style="height:80px;width:400px;"><%=BannerText%></textarea>
      </td>
    </tr>
    <tr>
      <th scope="row">Link</th>
      <td>
			<input type="text" size="65" name="BannerLink" class="TextInput" value="<%=BannerLink%>" maxlength="60"/>
      </td>
    </tr>
    <tr>
      <th scope="row">RHS Tab</th>
      <td>
			<input type="text" size="65" name="BannerTab" class="TextInput" value="<%=BannerTab%>" maxlength="40"/>
      </td>
    </tr>
    <tr>
      <th scope="row" valign="top">Create a Button</th>
      <td>
			<input type="text" size="65" name="BannerButtonTitle" class="TextInput" value="<%=BannerButtonTitle%>" maxlength="40"/>
			<br/>Examples: <img src="button-example1.jpg" height="26"> &nbsp; <img src="button-example2.jpg" height="26"/>
			<br/><em>Leave blank if the whole image is a link</em>
      </td>
    </tr>
    <tr>
      <th scope="row">Online</th>
		<td>
		<input type="checkbox" name="online" value="1"  <%If BannerOnline=1 Then response.write "Checked"%> />
		</td>
    </tr>

    <tr>
      <th scope="row">Image:</th>
	  <td valign="top">
	  <%
		If Len(BannerImage) > 5 then
			Response.write "<a href=""removeimage.asp?BannerID="&BannerID&""" title=""Remove this image""><img src="""&BannerImage&""" height=""40px"" style=""float:left;""> Remove this image</a>"
		else
		%>
	  <input type="file" size="150" name="Image" id="Image" class="textinput"/></div> (max: 521X223px)

		<%
		End If
		%>

	  </td>
    </tr>

 </table>
<br/>&nbsp;
<br/>&nbsp;

<%
	If IsNumeric(BannerID) and BannerID <> "" Then

%>

<h3>Slots Details</h3>
  <table cellpadding="0" cellspacing="0" border="0">

	<%

		Set BannSlot = sqlconnection.execute("select BannerSlotId,Slot, StartDate,EndDate FROM BannersSlots where BannerID = " & BannerID & " order by StartDate")

		For cnt = 0 To 5
			Slot		= 0
			StartDateValue		= ""
			StartTimeValue		= ""
			EndDateValue		= ""
			EndTimeValue		= ""
			BannerSlotId		= 0

				If Not BannSlot.eof then
					BannerSlotId	= BannSlot("BannerSlotId")
					Slot			= BannSlot("Slot")
					StartDate		= BannSlot("StartDate")
					EndDate			= BannSlot("EndDate")

					If IsDate(StartDate) then
						StartDateValue		= FormatDateTimeIRL(StartDate,1)
						StartTimeValue		= FormatDateTimeIRL(StartDate,4)
						EndDateValue		= FormatDateTimeIRL(EndDate,1)
						EndTimeValue		= FormatDateTimeIRL(EndDate,4)
					End if

					BannSlot.movenext
				End If


		%>

		<tr>
		  <th scope="Slot">Slot</th>
		  <td colspan="2">
			<input type="hidden" name ="BannerSlotId<%=cnt%>" value="<%=BannerSlotId%>" />
			<select name="slot<%=cnt%>">
				<option value="">--</option>
				<option value="1" <% If slot=1 Then Response.write "selected"%>>1</option>
				<option value="2" <% If slot=2 Then Response.write "selected"%>>2</option>
				<option value="3" <% If slot=3 Then Response.write "selected"%>>3</option>
				<option value="4" <% If slot=4 Then Response.write "selected"%>>4</option>

			</select>

			<td class="bodytextsmall">
				&nbsp; From <input type="text" name="StartDate<%=cnt%>" class="dpicker" style="width:150px;" value="<%=StartDateValue%>"/>
				<select name="StartTime<%=cnt%>" class="TextInput">
				  <%
					For i=480 to 1410 step 30
						TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
						response.Write "<option value="""& TimVal &""""
						if StartTimeValue = TimVal then
							response.write " selected "
						end if
						response.Write ">"& TimVal &"</option>"& vbnewline
						Next
					%>
				</select>
			</td>

			<td class="bodytextsmall" >
				&nbsp; To  <input type="text"  name="EndDate<%=cnt%>" class="dpicker" style="width:115px;" value="<%=EndDateValue%>"/>
				<select name="EndTime<%=cnt%>" class="TextInput">
				<%
					For i=480 to 1410 step 30
						TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
						response.Write "<option value="""& TimVal &""""
						if EndTimeValue = TimVal then
							response.write " selected "
						end if
						response.Write ">"& TimVal &"</option>"& vbnewline
					Next

				%>
					</select>
			<em>(24hr clock)</em>
		  </td>
		</tr>
		<%
		Next
		%>

 </table>
<%
End If ' If IsNumeric(BannerID) and BannerID <> "" Then
%>



<br/>&nbsp;
<br/>&nbsp;
 <table cellpadding="0" cellspacing="0" border="0">

    <tr >
      <td>&nbsp;</td>
      <td>

	  <input type="hidden" name="BannerID" value="<%=BannerID%>"/>

        <div><input type='submit' class="TextInput" name='Submit' value='<% If BannerID > 0 Then response.write "Change" Else response.write "Add"%> Banner &raquo;&raquo;'/></div>
      </td>
    </tr>
  </table>
</form>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->

	<script>

		CKEDITOR.replace( 'eventdescription' );

	</script>

</body>
</html>
