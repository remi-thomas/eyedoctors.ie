<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->
<!--#include virtual="/ssi/fct/common.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="/css/admin.css" type="text/css">

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/dbadmin/ssi/lib/jquery-ui-1.10.1.custom.min.js"></script>
<link href="/dbadmin/ssi/lib/ui-lightness/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>

<style>
div.ui-datepicker{
 font-size:10px;
}


</style>


<title>ICO - Website Administration </title>
</head>
<body class="body">

<script  language="JavaScript" type="text/javascript">
$(function() {
$( ".dpicker" ).datepicker({
changeMonth: true,
changeYear: true,
yearRange: '-2:+2',
maxDate: '+2y',
dateFormat: 'dd MM yy'
});
});
</script>


<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
'############### upload vars ########################################'
Dim PID,barref, UploadProgress
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = "/dbadmin/ssi/ImgUpl/framebar.asp?to=10&" & PID
%>
<script language="JavaScript" type="text/javascript">
function ShowProgress()
{
  strAppVersion = navigator.appVersion;
  if (document.UploadMap.Map.value != "")
  {
    if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
    {
      winstyle = "dialogWidth=375px; dialogHeight:130px; center:yes";
      window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
    }
    else
    {
      window.open('<% = barref %>&b=NN','','width=370,height=115', true);
    }
  }
  return true;
}
</script>


<%
BannerID			= request.querystring("BannerID")

if IsNumeric(BannerID) and BannerID <> "" then
	Set rs = sqlconnection.execute("exec usp_GET_BannerDetails " & BannerID & ",1")
	'esponse.write "exec usp_GET_BannerDetails " & BannerID & ",1"
	If not rs.eof then
		BannerTitle				= DisplayText(rs("BannerTitle"))
		BannerText				= DisplayText(rs("BannerText"))
		BannerLink				= rs("BannerLink")
		BannerImage				= rs("BannerImage")
		BannerOnline			= rs("BannerOnline")
		BannerButtonTitle	= rs("BannerButtonTitle")
		BannerTextStyle		= rs("BannerTextStyle")
		BannerImageStyle	= rs("BannerImageStyle")
		ShowTitleOnSlider	= rs("ShowTitleOnSlider")
		TitleStyle 				= rs("TitleStyle")
	end if
else

end if
If not isDate(StartDate) then
	StartDate = date() + 1
	StartTime = date() & " 20:00:00"
end If
If not isDate(EndDate) then
	EndDate = date() + 1
	EndTime = date() & " 20:00:00"
end if
%>

<p>
<%
If IsNumeric(BannerID) and BannerID <> "" Then
	response.Write "<h1>Edit Banner</h1>" & vbnewline
	response.Write session("AdminUserName") & " you can edit your banner below"
Else
	response.Write "<h1>Add new Banner</h1>" & vbnewline
	response.Write "To add a new event fill out the form below:"
end if
%>
</p>

<%
If IsNumeric(BannerID) and BannerID <> "" Then
%>

<iframe src="iframe.asp?BannerID=<%=BannerID%>" style="width:100%;height:360px;border:collapse;overflow:hidden;"></iframe>

<br style="clear:both"/>

<%
End If
%>
<h3>Banner Details</h3>
<form name="events" action="process.asp?<% = PID %>" method="post"  ENCTYPE="multipart/form-data" OnSubmit="return ShowProgress();">
  <table cellpadding="3" cellspacing="5" border="0">
    <tr>
      <th scope="row">Title:</th>
      <td><input type="text" size="65" name="BannerTitle" class="TextInput" value="<%=BannerTitle%>" maxlength="200"/>
	  <br/><input type="checkbox" value="1" name="ShowTitleOnSlider" <% If ShowTitleOnSlider = 1 Then Response.write " checked"%> /> Show this title on the slider?
	</td>
	</tr>
    <tr>
      <th scope="row">Style</th>
      <td>
			Style=&quot;<input type="text" size="65" name="TitleStyle" class="TextInput" value="<%=TitleStyle%>" maxlength="200"/>&quot;
      </td>
    </tr>


    <tr>
      <th scope="row">Description</th>
      <td>
			<textarea name="BannerText" style="height:80px;width:400px;"><%=BannerText%></textarea>
      </td>
    </tr>
    <tr>
      <th scope="row">Style your text</th>
      <td>
			<textarea name="BannerTextStyle" style="height:40px;width:400px;"><%=BannerTextStyle%></textarea>
      </td>
    </tr>


    <tr>
      <th scope="row">Link</th>
      <td>
			<input type="text" size="65" name="BannerLink" class="TextInput" value="<%=BannerLink%>" maxlength="200"/>
      </td>
    </tr>
    
    <!-- tr>
      <th scope="row" valign="top">Create a Button</th>
      <td>
			<input type="text" size="65" name="BannerButtonTitle" class="TextInput" value="<%=BannerButtonTitle%>" maxlength="40"/>
			<br/>Examples: <img src="button-example1.jpg" height="26"> &nbsp; <img src="button-example2.jpg" height="26"/>
			<br/><em>Leave blank if the whole image is a link</em>
      </td>
    </tr -->




    <tr>
      <th scope="row">Image:</th>
	  <td valign="top">
	  <%
		If Len(BannerImage) > 5 then
			Response.write "<a href=""removeimage.asp?BannerID="&BannerID&""" title=""Remove this image""><img src="""&BannerImage&""" height=""40px"" style=""float:left;""> Remove this image</a>"
		else
		%>
	  <input type="file" size="150" name="Image" id="Image" class="textinput"/></div> (max: 554X223px)

		<%
		End If
		%>

	  </td>
    </tr>
    <tr>
      <th scope="row">Style your image</th>
      <td>
			<textarea name="BannerImageStyle" style="height:40px;width:400px;"><%=BannerImageStyle%></textarea>
      </td>
    </tr>






    <tr>
      <th scope="row">Online</th>
		<td>
		<input type="checkbox" name="online" value="1"  <%If BannerOnline=1 Then response.write "Checked"%> />
		</td>
    </tr>
 </table>
<br/>&nbsp;
<br/>&nbsp;

<%
	If IsNumeric(BannerID) and BannerID <> "" Then

%>

<h3>Slots Details</h3>
  <table cellpadding="0" cellspacing="0" border="0">

	<%

		Set BannSlot = sqlconnection.execute("select BannerSlotId,Slot, StartDate,EndDate FROM BannersSlots where BannerID = " & BannerID & " order by StartDate")

		For cnt = 0 To 3
			Slot		= 0
			StartDateValue		= ""
			StartTimeValue		= ""
			EndDateValue		= ""
			EndTimeValue		= ""
			BannerSlotId		= 0

				If Not BannSlot.eof then
					BannerSlotId	= BannSlot("BannerSlotId")
					Slot			= BannSlot("Slot")
					StartDate		= BannSlot("StartDate")
					EndDate			= BannSlot("EndDate")

					If IsDate(StartDate) then
						StartDateValue		= FormatDateTimeIRL(StartDate,1)
						StartTimeValue		= FormatDateTimeIRL(StartDate,4)
						EndDateValue		= FormatDateTimeIRL(EndDate,1)
						EndTimeValue		= FormatDateTimeIRL(EndDate,4)
					End if

					BannSlot.movenext
				End If


		%>

		<tr>
		  <th scope="Slot">Slot</th>
		  <td colspan="2">
			<input type="hidden" name ="BannerSlotId<%=cnt%>" value="<%=BannerSlotId%>" />
			<select name="slot<%=cnt%>">
				<option value="">--</option>
				<option value="1" <% If slot=1 Then Response.write "selected"%>>1</option>
				<option value="2" <% If slot=2 Then Response.write "selected"%>>2</option>
				<option value="3" <% If slot=3 Then Response.write "selected"%>>3</option>
				<option value="4" <% If slot=4 Then Response.write "selected"%>>4</option>
				<option value="5" <% If slot=5 Then Response.write "selected"%>>5</option>
			</select>

			<td class="bodytextsmall">
				&nbsp; From <input type="text" name="StartDate<%=cnt%>" class="dpicker" style="width:150px;" value="<%=StartDateValue%>"/>
				<select name="StartTime<%=cnt%>" class="TextInput">
				  <%
					For i=480 to 1410 step 30
						TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
						response.Write "<option value="""& TimVal &""""
						if StartTimeValue = TimVal then
							response.write " selected "
						end if
						response.Write ">"& TimVal &"</option>"& vbnewline
						Next
					%>
				</select>
			</td>

			<td class="bodytextsmall" >
				&nbsp; To  <input type="text"  name="EndDate<%=cnt%>" class="dpicker" style="width:115px;" value="<%=EndDateValue%>"/>
				<select name="EndTime<%=cnt%>" class="TextInput">
				<%
					For i=480 to 1410 step 30
						TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
						response.Write "<option value="""& TimVal &""""
						if EndTimeValue = TimVal then
							response.write " selected "
						end if
						response.Write ">"& TimVal &"</option>"& vbnewline
					Next

				%>
					</select>
			<em>(24hr clock)</em>
		  </td>
		  <td>&nbsp; <a href="removeslot.asp?BannerID=<%=rs(0)%>&BannerSlotId=<%=BannerSlotId%>" onclick="return confirm('Delete this slot?')"/><img src="/dbadmin/images/delete.gif" title="delete this slot" border="0"/></a></td>
		</tr>
		<%
		Next
		%>

 </table>
<%
End If ' If IsNumeric(BannerID) and BannerID <> "" Then
%>



<br/>&nbsp;
<br/>&nbsp;
 <table cellpadding="0" cellspacing="0" border="0">

    <tr >
      <td>&nbsp;</td>
      <td>

	  <input type="hidden" name="BannerID" value="<%=BannerID%>"/>

        <div><input type='submit' class="TextInput" name='Submit' value='<% If BannerID > 0 Then response.write "Change" Else response.write "Add"%> Banner &raquo;&raquo;'/></div>
      </td>
    </tr>
  </table>
</form>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->



</body>
</html>
