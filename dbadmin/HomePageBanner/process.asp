<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=UTF-8"
Response.CodePage = 65001
Response.CharSet = "UTF-8"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/cleantext_function.asp " -->
<%
Dim Upload : Set Upload = Server.CreateObject("Persits.Upload")
Upload.ProgressID = Request.QueryString("PID")
' we use memory uploads, so we must limit file size
Upload.SetMaxSize 4200000, True
' Save to memory. Path parameter is omitted'
Upload.Save

Dim File
BannerPath = "/Banners2018/"
CntImage = 0
Dim BannerImage
BannerImage = NULL
For Each File in Upload.Files
	'response.Write server.ImgPath(ImgPath & File.FileName)
	Dim BannerFileExtention  : BannerFileExtention =  File.ImageType
	BannerFileName = Left(ValidateText(StripSpecialChar(Left(File.FileName,Len(File.FileName)-4))),50)
	BannerFileName = Replace(BannerFileName,"'","") & "." & BannerFileExtention

	BannerImage = BannerPath & BannerFileName
	File.SaveAs server.MapPath(BannerImage)

	Dim ArtPicwidth : ArtPicwidth = File.ImageWidth
	Dim ArtPicHeight : ArtPicHeight =   File.ImageHeight

	'Response.Write File.Name & ": " & BannerFileName & "<BR>"
	'Response.Write File.Path & "<br/>" & vbnewline
	'Response.Write "<br />Size: " & File.Size &" bytes <BR>"
	'Response.Write "<br />Type: " & File.ImageType & "<BR>"
	'Response.Write "<br />Size: " & ArtPicwidth & " x " & ArtPicHeight
	'Response.Write "<hr width=50% align=center>"
Next



BannerTitle			= Upload.Form("BannerTitle")
BannerText			= Upload.Form("BannerText")
TitleStyle			= Upload.Form("TitleStyle")
BannerLink			= Upload.Form("BannerLink")
BannerID			= Upload.Form("BannerID")
BannerTab			= Upload.Form("BannerTab")
BannerButtonTitle	= Upload.Form("BannerButtonTitle")
BannerTextStyle		= Upload.Form("BannerTextStyle")
BannerImageStyle	= Upload.Form("BannerImageStyle")
online				= Upload.Form("online")
ShowTitleOnSlider	= Upload.Form("ShowTitleOnSlider")

If Not IsNumeric(online) Or online = "" Then
	online = 0
End if
If online > 1 Then
	online = 0
End If
If Not IsNumeric(ShowTitleOnSlider) Or ShowTitleOnSlider = "" Then
	ShowTitleOnSlider = 0
End if
If ShowTitleOnSlider > 1 Then
	ShowTitleOnSlider = 0
End if

If Not IsNumeric(BannerID) Or BannerID = "" Then
	BannerID = 0
End if

response.write "<br/>BannerTitle:<br/>" & BannerTitle

BannerTitle			= EncodeText(left(RemoveJavascript(RemoveMsWordTags(BannerTitle)),190))
BannerText			= EncodeText(replace(BannerText,"&nbsp;"," "))
BannerTab			= validateText(EncodeText(left(RemoveJavascript(RemoveMsWordTags(BannerTab)),190)))
BannerButtonTitle	= validateText(EncodeText(left(RemoveAllHTMLTags(RemoveJavascript(RemoveMsWordTags(BannerButtonTitle))),190)))
BannerLink			= validateText(RemoveMsWordTags(BannerLink))

response.write "<hr>BannerTitle "  & BannerTitle
'response.end


If Len(BannerLink) > 3  then
	If InStr(BannerLink,"http") = 0 And left(BannerLink,1) <> "/"  Then
		BannerLink = "http://" & BannerLink
	End If
End If
If BannerLink = "http://" Then
	BannerLink =""
End if

'response.write "<br/>BannerTitle:<br/>" & BannerTitle
'response.end

'sproc to insert data into Boardeventdescriptions
Set adocmd  = Server.CreateObject("ADODB.Command")
adocmd.ActiveConnection = sqlConnection
adocmd.CommandText = "usp_INS_BannerHP"
adocmd.CommandType = 4

Set prm1 = adocmd.createparameter("@UserID", 3, 1)
prm1.value = Session("AdminID")
adocmd.Parameters.append prm1

Set prm4 = adocmd.createparameter("@Title", 200, 1, 600)
prm4.value = BannerTitle
adocmd.Parameters.append prm4

Set prm15 = adocmd.createparameter("@TitleStyle", 200, 1, 400)
prm15.value = TitleStyle
adocmd.Parameters.append prm15

Set prm5 = adocmd.createparameter("@BannerImage", 200, 1, 600)
prm5.value = BannerImage
adocmd.Parameters.append prm5

Set prm6 = adocmd.createparameter("@BannerTab", 200, 1, 200)
prm6.value = BannerTab
adocmd.Parameters.append prm6

Set prm7 = adocmd.createparameter("@BannerButtonTitle", 200, 1, 200)
prm7.value = BannerButtonTitle
adocmd.Parameters.append prm7

Set prm8 = adocmd.createparameter("@BannerLink", 200, 1, 400)
prm8.value = BannerLink
adocmd.Parameters.append prm8

Set prm9 = adocmd.createparameter("@BannerText", 200, 1, 800)
prm9.value = BannerText
adocmd.Parameters.append prm9

Set prm10 = adocmd.createparameter("@BannerTextStyle", 200, 1, 800)
prm10.value = BannerTextStyle
adocmd.Parameters.append prm10

Set prm11 = adocmd.createparameter("@BannerImageStyle", 200, 1, 800)
prm11.value = BannerImageStyle
adocmd.Parameters.append prm11

Set prm12 = adocmd.createparameter("@BannerOnline", 3, 1)
prm12.value = online
adocmd.Parameters.append prm12


Set prm13 = adocmd.createparameter("@ShowTitleOnSlider", 3, 1)
prm13.value = ShowTitleOnSlider
adocmd.Parameters.append prm13

Set prm14 = adocmd.createparameter("@BannerID", 3, 1)
prm14.value = BannerID
adocmd.Parameters.append prm14

adocmd.Execute


If BannerID = 0 Then
	Set newid  = sqlconnection.execute("SELECT top 1 bannerID from BannersHP order by BannerID desc")
	BannerID = newid(0)
	Set newid  = nothing
End if



'#################### slot & date time ###############'
For cnt = 0 To 5
	StartDate		= Upload.Form("StartDate" & cnt)
	StartTime		= Upload.Form("StartTime" & cnt)
	slot			= Upload.Form("slot" & cnt)
	EndDate			= Upload.Form("EndDate" & cnt)
	EndTime			= Upload.Form("EndTime" & cnt)
	BannerSlotID	= Upload.Form("BannerSlotID" & cnt)

	If BannerSlotID = "" or Not IsNumeric(BannerSlotID) Then
		BannerSlotID = 0
	Else
		BannerSlotID = CInt(BannerSlotID)
	End if

	If slot <> "" And IsNumeric(slot) then
	If IsDate(StartDate) then
		StartDate	= StartDate & " " & StartTime
		EndDate	= EndDate & " " & EndTime

		Response.write "<br/> " & cnt & " slot #" & slot & " StartDate " &  StartDate  & " EndDate " & EndDate & " BannerSlotId " & BannerSlotId


		'sproc to insert data into Boardeventdescriptions
		Set adocmd  = Server.CreateObject("ADODB.Command")
		adocmd.ActiveConnection = sqlConnection
		adocmd.CommandText = "usp_INS_banners_by_slots"
		adocmd.CommandType = 4

		Set prm1 = adocmd.createparameter("@BannerID", 3, 1)
		prm1.value = BannerID
		adocmd.Parameters.append prm1

		Set prm2 = adocmd.createparameter("@SlotID", 3, 1)
		prm2.value = slot
		adocmd.Parameters.append prm2

		Set prm3 = adocmd.createparameter("@StartDate", 7, 1)
		prm3.value = StartDate
		adocmd.Parameters.append prm3

		Set prm4 = adocmd.createparameter("@EndDate", 7, 1)
		prm4.value = EndDate
		adocmd.Parameters.append prm4

		Set prm5 = adocmd.createparameter("@BannerSlotID", 3, 1)
		prm5.value = BannerSlotID
		adocmd.Parameters.append prm5

		Set prm6 = adocmd.createparameter("@UserID", 3, 1)
		prm6.value = Session("AdminID")
		adocmd.Parameters.append prm6


		adocmd.Execute


	End If ' isdate
	End If ' slot is numeric
Next ' loop fields


%>
<!-- #include virtual="/ssi/dbclose.asp" -->
<%
If IsNumeric(BannerID) And BannerID <> "" Then
	Response.redirect "/dbadmin/HomePageBanner/banner.asp?BannerID=" & BannerID
else
	response.redirect "default.asp"
End if
%>