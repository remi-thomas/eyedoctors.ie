<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "company-structure.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Eye Doctors - Irish College Of Ophtalmologists"
SiteSection       = "Home" ' highlight correct menu tab'
'______________________________________________________________________|'
'|                                                                     |'
'______________________________________________________________________|'
%>
<!doctype html>
<html lang="en">
<head>
  <!--#include virtual="/ssi/incl/metadata.asp" -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>

<!-- HOMEPAGE: Banner [NEW] ================================== -->
<!--#include virtual="/ssi/fct/Slider2018Config.asp" -->
<% 
'call the function to build the slider'
BuildSlider(0)
dim AltBanner

dim BannerID
BannerID = request.querystring("BannerID")
if isNumeric(BannerID) and BannerID <> "" then
  set rs = sqlconnection.execute ("SELECT BannerTitle, TitleStyle, ShowTitleOnSlider, BannerText, BannerLink, BannerImage,BannerButtonTitle, BannerTextStyle,BannerImageStyle, ShowTitleOnSlider from BannersHP where  BannerID =" & BannerID)
  if not rs.eof then

    BannerTitle(0)        = rs("BannerTitle")
    BannerTitleStyle(0)   = rs("TitleStyle")
    BannerText(0)         = rs("BannerText")
      if len(rs("BannerLink")) then
        if left(rs("BannerLink"),3) = "www" then
          BannerLink(0)  = "https://" & rs("BannerLink")
        else
          BannerLink(0)  = rs("BannerLink")
        end if
      else
        BannerLink(0)    = "#"
      end if
    BannerImage(0)        = rs("BannerImage")
    BannerButtonTitle(0)  = rs("BannerButtonTitle")
    BannerTextStyle(0)    = rs("BannerTextStyle")
    BannerImageStyle(0)   = rs("BannerImageStyle")
    ShowTitleOnSlider(0)  = rs("ShowTitleOnSlider")

  end if
end if

%>
<div class="banner">
  <div class="container"> 
  <div class="row">
      <div class="col-md-8">
    <!-- HOMEPAGE: Carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Carousel: Indicators -->
      <ol class="carousel-indicators">
        <% 
        dim i
        for i = 0 to TotalSliders

        response.write vbtab & "<li data-target=""#myCarousel"&i&""" data-slide-to="""&i&""" class=""" 
          if i = 0 then 
            response.write "active"
          end if
          response.write """></li>"  & vbnewline
        next
        %>
        
      </ol>
      <!-- Carousel: Banners -->
      <div class="carousel-inner">
          <%
          for i = 0 to TotalSliders

          if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
            AltBanner = BannerTitle(i)
          else 
            if Len(BannerText(i)) Then
              AltBanner = BannerText(i)
            else
              AltBanner = "Irish College of Ophthalmologists - " & Year(Now())
            end if
          end if


            response.write  vbtab & "<div class=""item clearfix "
            if i = 0 then 
              response.write "active"
            end if
            response.write """"



            response.write ">" & vbnewline

            if len(BannerImage(i)) then
              response.write vbtab & "<a href="""& BannerLink(i) &""" title=""" & AltBanner &""" class=""carousel-img"">"
              response.write vbtab & "<img src=""" & replace(lcase(BannerImage(i)),"banners/","banners2018/") & """ alt=""" & AltBanner &""" title=""" & AltBanner &""">"
              response.write vbtab & "</a>"  & vbnewline
            end if

            response.write vbtab & vbtab & "<div class=""carousel-text"">"  & vbnewline
            if Len(BannerTitle(i)) > 1 And ShowTitleOnSlider(i) = 1 Then
              response.write vbtab & vbtab & vbtab & "<h2"
              if len(BannerTextStyle(i)) then
                response.write " style=""" & BannerTitleStyle(i) & """"
              end if

             response.write">"& BannerTitle(i) &"</h2>"
            end if

            Response.write vbtab & vbtab & vbtab &  "<p"  & vbnewline
            if Len(BannerTextStyle(i))  then 
              response.write " style="""& BannerTextStyle(i) &""""
            end if
            response.write ">"

            Response.write vbtab & vbtab & vbtab & "<a href=""" & BannerLink(i) &""""
            'neet to style the a href too'
            if Len(BannerTextStyle(i))  then 
              response.write " style="""& BannerTextStyle(i) &""""
            end if

           Response.write ">" & BannerText(i) & "</a>"   & vbnewline
            Response.write vbtab & vbtab & vbtab &  "</p>"  & vbnewline

            response.write vbtab & vbtab & "</div>"  & vbnewline

            response.write vbtab & "</div>"  & vbnewline
          next
          %>



      </div>
      <!-- Carousel: Controls --> 
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="icon-next"></span></a> 
    </div>
    </div>
      <div class="col-md-4">
      <!-- EVENTS ================================================== -->
      <div class="box events">
      <h2><a href="#">Meetings &amp; Events</a></h2>
      <%
        set rs = sqlconnection.execute("exec usp_sel_forthcoming_events NULL")
        if not rs.eof then
          dim cntNews : cntNews = 0
        %>
        <ul>
          <% 
          do while not rs.eof and cntNews < 3
          cntNews = cntNews + 1
            response.write "<li class=""clearfix""><a href="""&  CreateNewsFriendyURL(rs("EventID"),FormatUSDateTime(rs("StartDate")),rs("Title")) & """ title="""& rs("Title") &""">" & _
            "<em class=""date""><span>"& Day(rs("StartDate")) &"</span> "& MonthName(Month(rs("StartDate")),true) &"</em>"   & _
            " <strong>"& TrimTitle(rs("Title"),50)& "</strong></a></li>" & vbnewline
          rs.movenext
          loop
          %>
        </ul>
        <%
        end if
        %>

      <p><a href="/news-events/" class="btn">View all Events</a></p>
        </div>
    </div>
  </div>
  </div>
</div>



<script src="/ssi/js/bootstrap.min.js"></script> 
<script src="/ssi/js/bootstrap-hover-dropdown.min.js"></script>


</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->