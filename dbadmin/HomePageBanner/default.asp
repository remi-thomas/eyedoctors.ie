<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/ssi/fct/common.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<%
Maxslot = 5
dim StartDay , EndDay
%>
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript1.2" src="/ssi/scripts/openwindow.js" type="text/javascript"></script>
<script language="JavaScript1.2" type="text/javascript">
<!--
// Copyright Fusio Ltd - Dublin
// by Remi for http://www.ICO.ie
// info[at]fusio.net

// menu function
	startList = function()
	{
	if (document.all&&document.getElementById)
	{
		navRoot = document.getElementById("dmenu");
		for (i=0; i<navRoot.childNodes.length; i++)
		{
			node = navRoot.childNodes[i];
			if (node.nodeName=="LI")
			{
				node.onmouseover=function()
				{
					this.className+=" over";
				}
				node.onmouseout=function()
				{
					this.className=this.className.replace(" over", "");
				}
			}
		}
	}
	}
window.onload=startList;
-->
</script>
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<style type="text/css">
th.calMonth {
	text-align:center;
	color:#FFF;
	font-size:14px;
	font-weight:bold;
	background:#5db1b2;
}
td.calSing {
	font-size :10px;
	text-align : center;
	color : #FFF;
	background : #5db1b2;
}
td.calWeek {
	color:#FFF;
	font-weight:bold;
	font-size:12px;
	background:#5db1b2;
}
td.calDay {
	font-size :10px;
	color:#006666;
	font-weight:bold;
	background : #cce4e5;
	height:30px;
	width:100px;
}
td.calDay A {
	font-size : 11px;
	color:#006666;
	font-weight:bold;
	text-decoration: underline;
	height:30px;
	width:100px;
}
td.calDay A:hover {
	font-size : 11px;
	color :#006666;
	font-weight:bold;
	text-decoration: underline;
	height:30px;
	width:100px;
}
td.calDayOver {
	font-size : 10px;
	color : #000000;
	background:#f2f2f2;
	height:30px;
	width:100px;
}
td.calDayOver A {
	font-size : 11px;
	color : #000000;
	background:#f2f2f2;
	height:30px;
	text-decoration: none;
}
td.calDayOver A:hover {
	font-size : 11px;
	color :#DD0000;
	background:#f2f2f2;
	height:30px;
	text-decoration: none;
}
/* menu requirements for functionality */

.topmenuul {
	list-style-type:none;
	margin:0;
	padding:0;
}
.topmenuli {
	list-style-type:none;
	float:left;
	margin:0; padding:0;
}
.topmenutitle {
	display:block;
}
.submenuul {
	list-style-type:none;
	position:absolute;
	margin:0;
	padding:0;
	display:none;
}
.submenuli a {
	display:block;
	width:55px;
}
li:hover ul, li.over ul { /* lists nested under hovered list items */
	display: block;
}
#dmenu li>ul {
	top: auto;
	left: auto;
}
/* menu design */

#dmenu {
	font-size:14px;
	color:#000099;
}
.topmenutitle {
	text-decoration:underline;
	border:0;
	padding:3px;
	color : #ffffff;
	background : #5db1b2;
	width: 100px;
	text-align: left;
}
.submenuul {
	margin-top:-4px;
	background-color: #5db1b2;
	background-position: top left;
	background-repeat: repeat-x;
	border-bottom:1px solid #fff;
	border-right:1px solid #fff;
	border-left:1px solid #fff;
	width: 100px;
	text-align: left;
}
.submenuli a {
	padding:1px;
	text-decoration: none;
	color: #cce4e5;
}
.submenuli a:hover {
	text-decoration: none;
	color: #fff;
}
</style>
</head>
<body class="body">

<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>HomePage banner management</h1>


<iframe src="iframe.asp?BannerID=<%=BannerID%>" style="width:100%;height:360px;border:collapse;overflow:hidden;"></iframe>
<br style="clear:both"/>

<%
ThisErr =  request.querystring("ThisErr")
If Len(ThisErr) > 3 Then
	response.write "<div style=""width:90%;background:#ddd;padding:10px;font-weight:bold""><img src=""/dbadmin/images/OK.gif"">&nbsp;" & ThisErr & "</div><br/>&nbsp;"& vbnewline

End If
%>
<%
' '''''''''''''''''''''''
' get the date from URL '
' use now by default    '
' ''''''''''''''''''''''
if request.querystring("date") = "" or not isDate(request.querystring("date")) then
  nDate = Date()
else
	nDate = cDate(request.querystring("date"))
	if cDate("1/" & month(nDate) & "/" & year(nDate)) < cDate("1/" & month(now()) & "/" & year(now())) then
		'response.Write "1/" & month(nDate) & "/" & year(nDate)
		nDate = DateAdd ("yyyy",1,nDate)
	end if
end if

nDatePrev = DateAdd("m", -1, nDate)
nDateSuiv = DateAdd("m", 1, nDate)
nDay = Day(nDate)
nMonth = Month(nDate)
nYear = Year(nDate)
nDate1 = DateSerial(nYear, nMonth, 1)
Dim nTemp
nTemp = DateAdd("m", 1, nDate1)
nDay2 = Day(DateAdd("d", -1, nTemp))
nDate2 =  DateSerial(nYear, nMonth, nDay2)
nStartDate = WeekDay(nDate1, vbMonday)
nEndDate =  WeekDay(nDate2, vbMonday)
nbrCase = Day(nDate2) + (nStartDate-1) + (7-nEndDate)
nNomJour = 1

%>
<table border="0" cellspacing="1" cellpadding="0" width="90%" id="eventscal" >
  <!-- Start month, year and button header  -->
  <tr>
    <%
		If nDate > now() then
	%>
    <th class="calMonth" valign="middle"> <a href="default.asp?date=<%=nDatePrev%>"><img src="b_prev.gif" width="14" height="13" border="0" alt="Previous Month"></a> </th>
    <%
		else
	%>
    <th class="calMonth">&nbsp;</th>
    <% end if %>
    <th colspan="5" class="calMonth" valign="middle" align="center">
      <div id="main">
        <ul class="topmenu" id="dmenu">
          <li class="topmenuli"><a href="#" class="topmenutitle"><%=MonthName(nMonth)%></a>
            <ul class="submenuul">
              <%
					For I = nMonth + 1 to 11 + nMonth
						If I > 12 then
							ThisMonth = abs(12 - I)
						else
							ThisMonth = I
						end if
					%>
              <li class="submenuli"><a href="default.asp?date=01/<%=ThisMonth%>/<%=nYear%>"><%=MonthName(ThisMonth)%></a></li>
              <% next %>
            </ul>
          </li>
          <li class="topmenuli"><a href="#" class="topmenutitle"><%=nYear%></a>
            <ul class="submenuul">
              <li class="submenuli"><a href="default.asp?date=01/<%=nMonth%>/<%=Year(now)%>"><%=Year(now)%></a></li>
              <li class="submenuli"><a href="default.asp?date=01/<%=nMonth%>/<%=Year(now)+1%>"><%=Year(now)+1%></a></li>
              <li class="submenuli"><a href="default.asp?date=01/<%=nMonth%>/<%=Year(now)+2%>"><%=Year(now)+2%></a></li>
            </ul>
          </li>
        </ul>
      </div>
    </th>
    <th class="calMonth" valign="middle"><a href="default.asp?date=<%=nDateSuiv%>"><img src="b_suiv.gif" width="14" height="13" border="0" alt="Next Month"></a></th>
  </tr>
  <!-- End month, year and button header  -->
  <!-- Start weekday header -->
  <tr>
    <% for n = 1 to 7 %>
    <td width="14%" align="center" valign="middle" class="calWeek"><%=WeekDayName(n, false, vbMonday)%></td>
    <% next ' Boucle des noms du jour / WeekName for %>
  </tr>
  <!-- End weekday header -->
  <!-- Start day's loop -->
  <%
Dim njTemp
For n = 1 to nbrCase
  njTemp = n - nStartDate+1
	if nNomJour = 1 then
		response.write "<tr>"
	end if


	if n <  (nStartDate) or n > (nDay2+nStartDate-1) then
		'no day on this cell
		response.write "<td align=""center"" bgcolor=""#ecf2f2"" valign=""middle"">"
			if n = 1 or (n mod 7 = 0) then
				displayaddbanner = displayaddbanner + 1
				response.Write "<h3><a href=""banner.asp""><img src=""/dbadmin/images/addevent.gif"" alt=""Add a new banner to calendar"" border=""0"" />  Add a new banner</a></h3>"
			end if
	else
		If IsThePast(njTemp,nMonth,nYear) then
			response.write "<td align=""left"" valign=""top"" class=""calDayOver"">"
		else
			response.write "<td align=""left"" valign=""top"" class=""calDay"">"

		end if
		response.write "<p align=""center"" style=""padding:0px;margin:0px;"">" & njTemp & "</p>" & vbnewline
		For SlotID = 1 To Maxslot

			StartDay = FormatDateTimeIRL(CDate(njTemp & " " &  nMonth & " " & nYear),1) &" 00:00:00"
			EndDay	 = FormatDateTimeIRL(CDate(njTemp & " " &  nMonth & " " & nYear),1) &" 23:59:00"

			'response.write "<br/> StartDay " & StartDay & " " & IsDate(StartDay)
			'response.write "<br/> EndDay " & EndDay & " " & IsDate(EndDay)

			sql ="usp_SEL_BannersHP_bySlot '"& StartDay & "','"& EndDay &"',"& SlotID &",0"
			'Response.write sql
			Set rs = sqlconnection.execute(sql)
			do while NOT rs.eof
				response.Write "<div style=""padding:0 4px 1px 4px""><em style=""background:#fff;padding-left:1px;padding-right:1px;"">" & SlotID & "</em> <a href=""banner.asp?BannerID="&rs(0)&""" title=""Running "&rs("StartDate")&" to "&rs("EndDate")&""">" & TrimTitle(replace(DisplayText(RemoveAllHTMLTags(rs(1))),","," "),22) & "</a></div>" &  vbnewline
			rs.movenext
			Loop
		Next
	end if

  response.write "</td>"
	if nNomJour = 7 then
		response.write "</tr>"
	end if
	if nNomJour = 7 then
		nNomJour = 1
	else
		nNomJour = nNomJour + 1
	end if
next ' Boucle des jour / Day for
%>
  <!-- End day's loop -->
  <tr>
    <td class="calSing" colspan="7">&nbsp;</td>
  </tr>
</table>

<%
if displayaddbanner < 2 then
	response.Write "<h3><a href=""banner.asp"" class=""red12""><img src=""/dbadmin/images/addevent.gif"" alt=""Add a new banner to calendar"" border=""0"" /> Add a new banner</a></h3>"
end if
%>



<%
'#######################################################'
'## Sliders offline                                   ##'
'#######################################################'
Set rs = sqlconnection.execute("Select b.BannerId ,b.BannerTitle, b.BannerText, b.BannerLink, b.BannerImage ,b.BannerOnline , b.RecordDate FROM BannersHP b where b.BannerOnline=0 order by b.RecordDate desc" )

If rs.eof Then
	Response.write "<p>No Sliders are set offline in the system</p>" & vbnewline
Else

	Response.write "<h3>Sliders Currently Offline</h3>"
	Response.write "<ul>" & vbnewline
	Do until rs.eof
		Response.write "<li><a href=""banner.asp?BannerID="&rs(0)&""" title=""" & rs("BannerTitle") & """>" & rs("BannerTitle") & "</a> <a href=""delete.asp?BannerID="& rs(0)& """ onclick=""return confirm('Delete the #"& Replace(rs("BannerTitle"),"'","") & "# slider?')""/><img src=""/dbadmin/images/delete.gif"" title=""delete " & rs("BannerTitle") & """ border=""0""/></a></li>" & vbnewline
	rs.movenext
	loop
	Response.write "</ul>" & vbnewline
End If
'#######################################################'
'## Past Sliders                                      ##'
'#######################################################'
Set rs = sqlconnection.execute("Select b.BannerId ,b.BannerTitle, b.BannerText, b.BannerLink, b.BannerImage ,b.BannerOnline , b.RecordDate, s.EndDate FROM BannersHP b left join BannersSlots s on s.BannerID = b.BannerID where s.EndDate < getdate() and  b.BannerOnline=1 order by s.EndDate,b.BannerTitle " )
If rs.eof Then
	'Response.write "<p>No banners are set offline in the system</p>" & vbnewline
Else

	Response.write "<h3>Sliders Set In the Past</h3>"
	Response.write "<ul>" & vbnewline
	Do until rs.eof
		Response.write "<li><a href=""banner.asp?BannerID="&rs(0)&""" title=""" & rs("BannerTitle") & """>" & rs("BannerTitle") & "</a> [Ended "&FormatDateTimeIRL(rs("EndDate"),1)&"] <a href=""delete.asp?BannerID="& rs(0)& """ onclick=""return confirm('Delete the #"& Replace(rs("BannerTitle"),"'","") & "# slider?')""/><img src=""/dbadmin/images/delete.gif"" title=""delete " & rs("BannerTitle") & """ border=""0""/></a> </li>" & vbnewline
	rs.movenext
	loop
	Response.write "</ul>" & vbnewline
End If
'#######################################################'
'## Next Sliders                                      ##'
'#######################################################'
Set rs = sqlconnection.execute("Select b.BannerId ,b.BannerTitle, b.BannerText, b.BannerLink, b.BannerImage ,b.BannerOnline , b.RecordDate, s.EndDate,s.StartDate FROM BannersHP b left join BannersSlots s on s.BannerID = b.BannerID where s.startdate> getdate() and s.endDate > getdate() order by s.StartDate,b.BannerTitle " )
If rs.eof Then
	'Response.write "<p>No banners are set offline in the system</p>" & vbnewline
Else

	Response.write "<h3>Sliders Coming Next</h3>"
	Response.write "<ul>" & vbnewline
	Do until rs.eof
		Response.write "<li><a href=""banner.asp?BannerID="&rs(0)&""" title=""" & rs("BannerTitle") & """>" & rs("BannerTitle") & "</a> [Starting  "&FormatDateTimeIRL(rs("StartDate"),1)&"] <a href=""delete.asp?BannerID="& rs(0)& """ onclick=""return confirm('Delete the #"& Replace(rs("BannerTitle"),"'","") & "# slider?')""/><img src=""/dbadmin/images/delete.gif"" title=""delete " & rs("BannerTitle") & """ border=""0""/></a> </li>" & vbnewline
	rs.movenext
	loop
	Response.write "</ul>" & vbnewline
End If
%>


<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>
<%
Function IsThePast(DayTemp,MonthTemp,YearTemp)


	if IsNumeric(MonthTemp) and MonthTemp <> "" then
			MonthTemp = MonthName(MonthTemp)
			dim tempdate : 	tempdate = DayTemp & " " & MonthTemp & " " & YearTemp
			if tempdate="" or not IsDate(tempdate) Then
				IsThePast = true
				exit Function
			else
				tempdate =  Cdate(tempdate) 
				tempdate = FormatDateTimeIRL(tempdate,2)
				'response.write "<br/> tempdate " & tempdate
				DateNow  = FormatDateTimeIRL(now(),2)
				'response.write "<br/> DateNow " & DateNow
				'response.write  DateDiff("d",DateNow,tempdate) & "<br />"
				if DateDiff("d",DateNow,tempdate) < 1 then
					IsThePast = true
					Exit Function
				end if
			end if
	else 
		IsThePast = true
		Exit Function
	end if
end function
%>
