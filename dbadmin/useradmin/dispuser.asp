<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/common.asp " -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<html>
<head>
<title>ICO - users -  Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<%
AdminID	= Request("AdminID")
search		= Request("search")

searchtrm = search
'patindex : change * to _'

if IsNumeric(AdminID) and AdminID<> "" then
	sql="Select email,Password,Status,SetupDate,FullName,notes from AdminUsers where AdminID=" & AdminID
	Set rsUser=sqlConnection.execute(sql)
		if rsUser.eof Then
			'There is no such user'
			Response.Redirect "/dbadmin/default.asp?ThisErr=You can not edit this record"
		end if
		UserName	= DisplayText(rsUser("FullName"))
		searchtrm = ValidateText(rsUser(0))
end if
%>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0"  class="dbtext">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<h1>User Administration</h1>

      <form name="edit" method="post" action="dispuser.asp" id="usersearch2">
        <p>Search User Database: <input type="text" name="search" value="<%=searchtrm%>" class="TextInput" width="120">
          <em>(Tip: Use the * symbol as a wildcard.)</em>
          <input type="submit" name="Submit" value="Show User &raquo;" id="submit"></p>
	</form>

<h3>User Details</h3>

<%
'Get data on this user'
If len(search) > 2  then
	'#####################'
	'## SEARCH RESULTS  ##'
	'#####################'
	search = ValidateText(search)
	search = Replace (search, ".", "_")
	search = Replace (search, "*", "_")
	Set rsUser=sqlConnection.execute("usp_SEL_SearchByName " & search)
	if rsUser.eof Then
		'There is no such user'
	else
		response.write"<table width='100%' border='0' cellspacing='0' cellpadding='0' class='userdetails'>" & vbnewline & _
		"	<tr>" & vbnewline & _
		"		<th width='100'><b>Email Address</b></th>" & vbnewline & _
		"		<th><b>Name</b></th>" & vbnewline & _
		"		<th><b>Organisation</b></th>" & vbnewline & _
		"		<th><b>Work Area</b></th>" & vbnewline & _
		"		<th><b>Job Title</b></th>" & vbnewline & _
		"		<th><b>Contact</b></th>" & vbnewline & _
		"		<th><b>Notes</b></th>" & vbnewline & _
		"		<th width='50'><b>Status</b></th>" & vbnewline & _
		"	</tr>" & vbnewline

		Do until rsUser.eof

			UserName	= DisplayText(rsUser("FullName"))
			AdminID	= rsUser("AdminID")

		response.Write VbNewline & _
		"	<tr>" & vbnewline & _
		"		<td width='100'><b>"& _
				"<a href=""dispuser.asp?AdminID=" & AdminID & """>" & _
				rsUser("email") &"</a></b></td>" & vbnewline & _
		"		<td><a href=""dispuser.asp?AdminID=" & rsUser(4) & """>" &UserName &"</a></td>" & vbnewline & _
		"		<td>"& rsUser("Organization") &"</td>" & vbnewline & _
		"		<td>"& rsUser("WorkArea") &"</td>" & vbnewline & _
		"		<td>"& rsUser("JobTitle") &"</td>" & vbnewline & _
		"		<td>"& rsUser("Phone") &"</td>" & vbnewline & _
		"		<td>"& rsUser("Notes") &"</td>" & vbnewline & _
		"		<td width='50'>"& rsUser("Status")  &"</td>" & vbnewline & _
		"	</tr>" & vbnewline

		rsUser.Movenext
		Loop

		response.Write "</table>" & vbnewline

	end if
	'response.end
elseif IsNumeric(AdminID) and len(AdminID) > 0 Then
	'#####################'
	'## USER ID LOOKUP  ##'
	'#####################'


else
	'QueryString is being messed with so redirect to homepage'
	Response.Redirect "/default.asp"
end if

%>
<p class="button"><a href="edituser.asp?ld=1&amp;ID=<%=AdminID%>">Edit User Details</a></p>

<%
ListDisplay=Request.QueryString("ld")
if ListDisplay=1 Then

	'List logins for this user'
	sql="Select Top 50 LoginDate,IPAddress,BrowserType from AdminLogins where AdminID=" & AdminID & "order by LoginDate desc"
	set rsLogins=sqlConnection.execute(sql)

	if not rsLogins.eof Then
%>
		<p class="bluetext">&nbsp; &nbsp; &raquo;&raquo; Last 50 Logins by <%=UserName%></p>
<table width="80%" border="0" cellspacing="1" cellpadding="1"  align="center" class="admintable">
	<tr>
		<td bgcolor="#FFFFFF" width="20">&nbsp;</td>
		<td bgcolor="#FFFFFF" class="bluetext" width="130"><b>Login Date</b></td>
		<td bgcolor="#FFFFFF" class="bluetext"><b>IP Address</b></td>
		<td bgcolor="#FFFFFF" class="bluetext"><b>Browser Type</b></td>
	</tr>
<%
		rec=0
		do until rsLogins.eof
		rec=rec+1
		if rec mod 2 = 0 then
			BgColor = "#D6E8F5"
		else
			BgColor = "#E4F1FA"
		end if
%>
			<tr>
				<td bgcolor="<%=BgColor%>" class="greytext "><%=rec %></td>
				<td bgcolor="<%=BgColor%>" class="bluetext"><%=FormatDateTimeIRL(rsLogins(0),1) %></td>
				<td bgcolor="<%=BgColor%>" class="dbtext"><%=rsLogins(1) %></td>
				<td bgcolor="<%=BgColor%>" class="dbtext"><%=rsLogins(2) %></td>
			</tr>
<%
			rsLogins.MoveNext
		Loop
		Response.Write "</table>"
	end if
else
	'list files owned by this user'
	Response.Write "<p class=""button""><a href='dispuser.asp?ld=1&amp;AdminID=" & AdminID & "'>Show Logins by " & UserName & "</a></p>"
	'List files for this user'
	
end if

set rsUser=nothing
%>
	</table>

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->

</body>
</html>

