<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>

<script language='JavaScript'>
<!--
var checkobj
function validate_form(form_name)
{
	// user name

	if (form_name.UserName.value == "")
	{
		alert("Please enter your name");
		form_name.UserName.select();
		return false;
	}

	if (form_name.UserName.value.length < 4)
	{
		alert("Your Member Name must be more than 4 characters in length!");
		form_name.UserName.select();
		return false;
	}


	if (form_name.UserName.value != "")
	{
		var string2check = form_name.UserName.value
		if (!AlphaNum(string2check))
		{
			alert("Sorry, Your nickname must only contain alphanumeric characters (abc.. 123..) and _ or - instead of space");
			form_name.UserName.focus();
			return false;
		}
	}


	// passwords
	if (form_name.psswrd.value == "")
	{
		alert("Please choose a password!");
		form_name.psswrd.select();
		return false;
	}


	if (form_name.v_psswrd.value == "")
	{
		alert("Please verify your password!");
		form_name.v_psswrd.select();
		return false;
	}


	if (form_name.v_psswrd.value != form_name.psswrd.value)
	{
		alert("Passwords don't match, please verify");
		form_name.v_psswrd.select();
		return false;
	}

	if (form_name.UserName.value == form_name.psswrd.value)
	{
		alert("For security reason, your password can't be the same as your User Name \nsorry!");
		form_name.psswrd.select();
		return false;
	}


	if (!AlphaNum(form_name.psswrd.value))
	{
		alert("Sorry, Your password must only contain alphanumeric characters (abc.. 123..) and _ or - instead of space!");
		form_name.psswrd.focus();
		return false;
	}

	if (form_name.psswrd.value != "")
	{
		var password = form_name.psswrd.value
		var common = "pass"

		if (password == common)
		{
		alert("For security reason, your password can't be <<pass>>!");
		form_name.psswrd.focus();
		return false;
		}

		var common = "password"
		if (password == common)
		{
		alert("For security reason, your password can't be <<password>>!");
		form_name.psswrd.focus();
		return false;
		}
	}


}

function AlphaNum(string2check)
{
	// allow ONLY alphanumeric keys, no symbols or punctuation
	// this can be altered for any "checkOK" string you desire
	var checkOK = "_-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var checkStr = string2check;
	var allValid = true;
	for (i = 0;  i < checkStr.length;  i++)
	{
		ch = checkStr.charAt(i);
		for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
		break;
		if (j == checkOK.length)
		{
			allValid = false;
			break;
		}
	}
	if (!allValid)
	{
		return (false);
	}
	else
	{
		return (true);
	}

}

//-->
</script>
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<br/> &nbsp;
<fieldset name="PageOption" class="fieldset" align="center">
    <legend class="legend">Your Account</legend>
	<img src="/dbadmin/images/bulb.gif" alt="!" align="left" border="0">
	<span class="dbtext"><%=session("AdminUserName")%>, you can change here your login details.
	</span>
</fieldset>

      <%
      'sql to get details '
      set rs=sqlconnection.Execute ("SELECT UserName,Password,FullName,Notes FROM AdminUsers where AdminID="& Session("AdminID"))
      %>
      <form name="update" method="post" action="updateuser.asp" onSubmit="return validate_form(this);">
     <input type="hidden" name="AdminUser" value="<%=Session("AdminID")%>">
	 <input type="hidden" name="UserStatus" value="<%=Session("AdminStatus")%>">
	 <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="admintable">
	 <tr>
            <td width="5%">&nbsp;</td>
            <td width="14%" class="dbtext">User Name</td>
            <td width="81%">
              <input type="text" name="UserName" value="<%=DisplayText(rs(0))%>" class="TextInput">
            </td>
          </tr>
	<tr>
            <td width="5%" height="16">&nbsp;</td>
            <td width="14%" height="16" class="dbtext">Full Name</td>
            <td width="81%" height="16">
              <input type="text" name="FullName" value="<%=rs(2)%>" class="TextInput">
            </td>
          </tr>
          <tr>
            <td width="5%">&nbsp;</td>
            <td width="14%" class="dbtext">Notes</td>
            <td width="81%">
              <textarea name="Notes" class="TextInput" cols=50 rows=3><%=DisplayText(rs(3))%></textarea>
            </td>
          </tr>

          <tr>
            <td width="5%">&nbsp;</td>
            <td width="14%" class="dbtext">Password</td>
            <td width="81%">
              <input type="Password" name="psswrd" value="<%=rs(1)%>" class="TextInput">
            </td>
          </tr>
          <tr>
            <td width="5%">&nbsp;</td>
            <td width="14%" class="dbtext">Confirm Password</td>
            <td width="81%">
              <input type="Password" name="v_psswrd" class="TextInput" value="">
            </td>
          </tr>
          <tr>
            <td width="5%">&nbsp;</td>
            <td width="14%">&nbsp;</td>
            <td width="81%">
              <input type="submit" name="Submit" value="Submit" class="textinput">
            </td>
          </tr>
        </table>
      </form>
      <p>&nbsp;</p>

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<p>
  <!--#include virtual="/ssi/dbclose.asp" -->
</p>
</body>
</html>
