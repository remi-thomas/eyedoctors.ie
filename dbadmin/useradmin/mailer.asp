<script language="VBScript" runat="SERVER">

'********** send a mail **************'
function SendMail (From, SendTo, Subject, Message)

	'On Error Resume Next

	Dim MyBody
	Dim ThisMail

	Set ThisMail = CreateObject("CDO.Message")
	ThisMail.From= From
	ThisMail.To= SendTo
	ThisMail.Subject=Subject
	ThisMail.HTMLBody= Message
	ThisMail.Send
	set ThisMail=nothing

	if Err then
		SendMail = false
	else
		SendMail = true
	end if

end function

'********** send a html based email **************'
function SendHTMLMail (From, SendTo, Subject, Message)

	'On Error Resume Next

	Dim ThisMail
	Dim HTML

	Set ThisMail = CreateObject("CDO.Message")

	HTMLTop = "<!DOCTYPE HTML PUBLIC'-//IETF//DTD HTML//EN'>" & VbNewline & _
	"<html>" & VbNewline & _
	"<head>" & VbNewline & _
	"<title>http://bdosx.ie - AutoEmail</title>" & VbNewline & _
	"</head>" & VbNewline & _
	"<body>"  & VbNewline
	ThisMail.From= From
	ThisMail.To=SendTo
	ThisMail.Subject=Subject
	ThisMail.HTMLBody= HTMLTop & Message & "</body></html>"
	ThisMail.Send
	set ThisMail=nothing

	if Err then
		SendHTMLMail = false
	else
		SendHTMLMail = true
	end if

end function

'********** send a mail with an attachment **************'
function SendAttachment (From, SendTo, Subject, Message, Attachment)

	'On Error Resume Next

    Dim MyBody
    Dim ThisMail
	Dim HTMLTop

	HTMLTop = "<!DOCTYPE HTML PUBLIC'-//IETF//DTD HTML//EN'>" & VbNewline & _
	"<html>" & VbNewline & _
	"<head>" & VbNewline & _
	"<title>BDOSX - AutoEmail</title>" & VbNewline & _
	"</head>" & VbNewline & _
	"<body>"  & VbNewline


    Set ThisMail= CreateObject("CDO.Message")
    ThisMail.From=From
    ThisMail.To=SendTo
    ThisMail.Subject=Subject

    ThisMail.AttachFile Server.MapPath(Attachment)

   	ThisMail.HTMLBody= HTMLTop & Message & "</body></html>"
    ThisMail.Send
    set ThisMail=nothing

	if Err then
		SendAttachment = false
	else
		SendAttachment = true
	end if

end function

</script>