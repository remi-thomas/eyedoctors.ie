<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<script language='JavaScript'>
<!--
var checkobj
function validate_form(form_name)
{
	// user name

	if (form_name.FullName.value == "")
	{
		alert("Please enter the Full Name of this user");
		form_name.FullName.select();
		return false;
	}

	if (form_name.FullName.value.length < 4)
	{
		alert("The Full Name must be more than 4 characters in length!");
		form_name.FullName.select();
		return false;
	}


	if (form_name.v_psswrd.value != form_name.psswrd.value)
	{
		alert("Your passwords do not match - please enter them again");
		form_name.v_psswrd.select();
		return false;
	}

	if (form_name.FullName.value == form_name.psswrd.value)
	{
		alert("For security reason, the password can't be the same as the User Name \nsorry!");
		form_name.psswrd.select();
		return false;
	}

	if (form_name.emailaddress.value == "")
	{
		alert("The email address field is required!");
		form_name.emailaddress.select();
		return false;
	}



	if (!AlphaNum(form_name.psswrd.value))
	{
		alert("Sorry, Your password must only contain alphanumeric characters (abc.. 123..) and _ or - instead of space!");
		form_name.psswrd.focus();
		return false;
	}

	if (form_name.psswrd.value != "")
	{
		var password = form_name.psswrd.value
		var common = "pass"

		if (password == common)
		{
		alert("For security reasons, the password can't be <<pass>>!");
		form_name.psswrd.focus();
		return false;
		}

		var common = "password"
		if (password == common)
		{
		alert("For security reasons, the password can't be <<password>>!");
		form_name.psswrd.focus();
		return false;
		}
	}


}

function AlphaNum(string2check)
{
	// allow ONLY alphanumeric keys, no symbols or punctuation
	// this can be altered for any "checkOK" string you desire
	var checkOK = "_-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var checkStr = string2check;
	var allValid = true;
	for (i = 0;  i < checkStr.length;  i++)
	{
		ch = checkStr.charAt(i);
		for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
		break;
		if (j == checkOK.length)
		{
			allValid = false;
			break;
		}
	}
	if (!allValid)
	{
		return (false);
	}
	else
	{
		return (true);
	}

}

//-->
</script>

<body >
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
StatusRequired = 9
ID = left(request("ID"),8)
if IsNumeric (ID) and ID <> "" then
	set rs=sqlconnection.Execute ("Select email,FullName,Notes,Status,AdminID,password from AdminUsers where AdminID="& ID)
	email		= DisplayText(rs(0))
	FullName	= DisplayText(rs("FullName"))
	Notes		= DisplayText(rs("Notes"))
	UserStatus	= rs("Status")
	AdminID	= rs("AdminID")
	password	= DisplayText(rs("password"))
	formaction	= ""
	Response.Write "<h1>User Administration - <i>" & FullName & "</i></h1>"
else
	formaction	="?mode=add"
	AdminUser	=""
	Response.Write "<h1>Add New User</h1>"
End if
      %>

	<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
	<%
	If IsNumeric (ID) and ID <> "" then
	%>
	Modify the user's details and click submit.
	<%else%>
	To register a new user to the ICO administrarion, enter the details into the form below and click submit.
	<%end if%>
"</div><br/>&nbsp;

<%
'status: 1 => update record
'status: 3 => read & publish
'status: 5 => modify
'status  9 => super admin
%>

<div id="details">
      <form name="update" method="post" action="updateuser.asp<%=formaction%>" onSubmit="return validate_form(this);">
    <input type="hidden" name="AdminID" class="TextInput" value="<%=AdminID%>">
    <p><label>Full Name</label> <input type="text" name="FullName" value="<%=FullName%>" class="TextInput"></p>
    <p><label>Password</label> <input type="password" name="psswrd" value="<%=password%>" class="TextInput"> (leave the password fields blank and we'll create a password for you)</p>
    <p><label>Repeat Password</label> <input type="password" name="v_psswrd" value="<%=password%>" class="TextInput"></p>
    <p><label>Email</label> <input type="text" name="emailaddress" value="<%=email%>" class="TextInput"></p>
    <p><label>Status</label>
	<select name="UserStatus">
		<option value="3" <% if UserStatus=3 then response.write " selected " %>>Read &amp; Publish</option>3
		<option value="5" <% if UserStatus=5 then response.write " selected " %>>Read, Publish &amp; Modify</option>
		<option value="9" <% if UserStatus=9 then response.write " selected " %>>Administrator</option>


	</select>
</p>

    <p><label>Notes</label> <textarea name="Notes" class="TextInput" cols=50 rows=3><%=Notes%></textarea></p>
    <div><input type="submit" name="Submit" value="Submit" class="textinput"></div>
  </form>
</div>

      </form>

</table>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<p>
  <!--#include virtual="/ssi/dbclose.asp" -->
</p>
</body>
</html>
