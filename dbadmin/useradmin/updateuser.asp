<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<%
'This file updates the details for a user'
AdminID	= Request.Form("AdminID")
email		= ValidateText(Request.Form("emailaddress"))
FullName	= ValidateText(Request.Form("FullName"))
Notes		= ValidateText(Request.Form("Notes"))
Password	= ValidateText(Request.Form("psswrd"))
Password1	= ValidateText(Request.Form("v_psswrd"))
UserStatus	= Request.Form("UserStatus")
Phone		= ValidateText(Request.Form("Phone"))
if Password <> Password1 Then
	Response.Redirect "edituser.asp?PWNOMATCHC&AdminID=" & AdminID
end if

'Password field left blank so create one for them'
if Password = "" then

	Password=GenerateNumber(FullName)

	function GenerateNumber(UserName)
		UserName = replace(UserName,"'"," ")
		dim ThisNameArr
		ThisNameArr = Split(UserName, " ")
		For i = 0 to ubound(ThisNameArr)
			GenerateNumber = GenerateNumber & left(ThisNameArr(i),1)
		Next
		GenerateNumber = GenerateNumber & GenerateRandomletter() & GenerateRandomletter() & day(now()) & GenerateRandomletter() &	right(year(now()),2) & GenerateRandomletter()
	end function


	Function GenerateRandomletter()
		randomize
		GenerateRandomletter = Chr(Int(26 * Rnd + 97))
	End Function

	Function GenerateRandom()
		randomize
		GenerateRandom = Int(999999 * Rnd)
	End Function

end if

if not IsNumeric(UserStatus) or UserStatus="" then
	UserStatus = 1
end if

If IsNumeric(AdminID) Then
	sqlconnection.Execute("UPDATE AdminUsers SET " & _
	"email='"& email& "', Password='"& Password&"', FullName='"& FullName & "'," & _
	"Notes='"&Notes &"', Status="& UserStatus & _
	" WHERE AdminID=" & AdminID)

ElseIf request("mode") = "add" then
	'Verify that there is not an account for this email address already'
	sql="Select AdminID from AdminUsers where email='" & email & "'"
	set rs=sqlconnection.execute(sql)
	if NOT rs.eof then
		AdminID = rs(0)
		sqlconnection.Execute("UPDATE AdminUsers SET " & _
		"email='"& email& "', Password='"& Password&"', FullName='"& FullName & "'," & _
		"Notes='"&Notes &"', Status="& UserStatus & _
		" WHERE AdminID=" & AdminID)
	else
		sql ="INSERT INTO AdminUsers(email,Password,Status,SetupDate,FullName,Notes,Phone)" & _
		"VALUES('"& email&"', '"& Password&"',"& UserStatus& ",getdate(),'" &FullName& "', '"& Notes&"', '"& Phone&"')"
		sqlconnection.Execute(sql)
		sqlNewID = SqlConnection.execute("select @@identity as NewRec")
		AdminID = sqlNewID(0)
	end if
end if

%>
<!--#include virtual="/ssi/dbclose.asp" -->
<%
if IsNumeric(AdminID) Then
	Response.Redirect "/dbadmin/useradmin/edituser.asp?ID=" & AdminID
else
	Response.Redirect "/dbadmin/useradmin/newuserconfirm.asp?ID=" & AdminID
end if
%>

