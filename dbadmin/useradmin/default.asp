<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>

<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<h1>User Administration</h1>
<p><img src="/dbadmin/images/bulb.gif" alt="!" align="left" border="0"> Select the user account you're looking for from the list below, or enter its name in the search box.</p>

	<%
	if Session("AdminStatus") => 9 then
		set rsuser = sqlconnection.execute("select AdminID, email, FullName,Notes from AdminUsers order by FullName")
	%>

	<form name="edit" method="post" action="dispuser.asp" id="usersearch">
              <p><label>Select User:</label> <select name="AdminID" class="TextInput">
                <option value="">-- Please Select User --</option>
                <%
                Do until rsuser.eof
                    response.Write "<option value=""" & rsuser(0) & """>"& rsuser(1) &" ("& rsuser(2) &" - "& rsuser(3) &")</option>" & vbnewline
                rsuser.Movenext
                Loop
                %>
              </select>
              </p>
              <p><label>Search User Database:</label> <input type="text" name="search" class="TextInput"><br>
                <em>(Tip: Use the * symbol in the search box as a wildcard.)</em></p>
              <div><input type="submit" name="Submit" value="Show User &raquo;"></div>
	</form>

    <%
	else
		response.Write "<br/> Sorry " & Session("ComCentreUserName") & " this is a restriced area.<br/>You can only change your own login/password not others."
    end If
    %>


<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->

</body>
</html>
