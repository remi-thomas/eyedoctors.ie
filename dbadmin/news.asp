<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>News - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/scripts/openwindow.js" TYPE="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<%
section = "News"

' include at the top '
mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 30
%>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>News</h1>
<%
sql = "exec usp_SEL_News NULL,1"
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open Sql , sqlConnection


if NOT rs.eof THEN
	cntM=0
	rec = 1
	rs.movefirst
	rs.pagesize = maxdisplay
	rs.absolutepage = mypage
	maxpages = cint(rs.pagecount)
	' display the total of records returned by sql query'
	intRecordCnt = rs.RecordCount

	cnt = 0
	do while not rs.eof and cnt<maxdisplay
	cnt = cnt+1
		HasEditingPriv = false
		If Session("AdminStatus") > 8 Then
			HasEditingPriv = true
		End if
		If Session("AdminID")= rs("RecordCreatedBy") Then
			HasEditingPriv = True
		End if

		'response.write "Session(AdminID) >> " & Session("AdminID") & " " & rs("RecordCreatedBy")
		response.Write	"<div class=""newslist"">" & VbNewline

		response.Write	"<h3>"

		If HasEditingPriv then
			response.Write	"<br/><a href=""/dbadmin/news/edit.asp?ID=" & rs(0) & """><img src=""/dbadmin/images/edit.gif"" width=""15"" height=""14"" border=""0"" align=""left""/></a>&nbsp; "
		End If

		response.Write	"<a href=""/dbadmin/news/display.asp?NewsID=" & rs("NewsID") & """>" & DisplayText(rs("Subject")) & "</a></h3>" & VbNewline
		response.Write	"<p>" & DisplayText(rs("Header")) & "</p>"  & VbNewline

		if rs("Media") > 0 then
			response.write "Media: " &  DrawAttachedMedia(rs("NewsID"),section)
		end if


		response.Write "<div><em> Published on " & FormatDateTime (rs("PublicationDate"), vblongdate) & " by " & rs("Author") & "</em></div>" & VbNewline
		response.Write "</div>"

	rs.MoveNext
	Loop


else
	response.Write "<p>There are no current News in the database</p>"

end if
set rs = nothing

if maxpages > 1 then
	call Paging
else
	response.write "&nbsp;"
end if
%>
<br/>&nbsp;
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "<span>Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a  href=""news.asp?mypage=" & counter &""">" & _
				counter & _
				"</a>"
			else
				Response.Write "<i >" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
		Response.Write "</span>"
	end if
end sub
%>
