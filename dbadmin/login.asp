<!--#include virtual="/ssi/dbconnect.asp"-->
<%
Set adocmd  = Server.CreateObject("ADODB.Command")
adocmd.ActiveConnection = sqlConnection
adocmd.CommandText = "usp_SAV_Administrator_UsersLogin"
adocmd.CommandType = 4


Set IP = adocmd.createparameter("@UserIP", 200, 1, 100)
IP.value = Request.ServerVariables("REMOTE_ADDR")
'IP.value = "000.000.000.000"
adocmd.Parameters.append IP

Set BrowserType = adocmd.createparameter("@BrowserType", 200, 1, 100)
BrowserType.value = left(Request.ServerVariables("HTTP_USER_AGENT"),100)
adocmd.Parameters.append BrowserType

Set prm2 = adocmd.createparameter("@email", 200, 1, 50)
prm2.value = replace(request("email"),"'","")
adocmd.Parameters.append prm2

Set prm3 = adocmd.createparameter("@Password", 200, 1, 50)
prm3.value = replace(request("pssword"),"'","")
adocmd.Parameters.append prm3

Set prm4 = adocmd.createparameter("@AdminUserName", 200,2, 100)
adocmd.Parameters.append prm4

Set prm5 = adocmd.createparameter("@ErrorCode", 3, 2)
adocmd.Parameters.append prm5

Set prm6 = adocmd.createparameter("@UserID", 3, 2)
adocmd.Parameters.append prm6

Set prm7 = adocmd.createparameter("@UserStatus", 3, 2)
adocmd.Parameters.append prm7


adocmd.Execute (path)

AdminUserName	= prm4.value
ErrorCode		= prm5.value
AdminID		= prm6.value
AdminStatus		= prm7.value

'response.Write "<br />ErrorCode " & ErrorCode & "<br />AdminID	= " & AdminID & "<br />AdminStatus	= " & AdminStatus
'status : 1 => update record
'status: 3 => read & publish
'status: 5 => modify
'status  9 => super admin


if IsNull(ErrorCode) then
	Session("AdminStatus")	= AdminStatus
	Session("AdminID")	= AdminID
	Session("AdminUserName")	= AdminUserName
	Session.Timeout = 20

	dim redirectURL : redirectURL = "/dbadmin/default.asp"
	dim referer : referer = request.querystring("referer")
	if len(referer) then
		redirectURL = referer
	end if

	Select Case (AdminStatus)
		Case 1
			response.redirect "/dbadmin/profile/default.asp?ld=1&ID=" & AdminID
		case 3
			response.redirect redirectURL
		case 9
			response.redirect redirectURL
		Case Else
			response.redirect redirectURL
	End Select




else

	response.redirect "/dbadmin/logon.asp?ErrorCode="& ErrorCode
end if
%>
<!--#include virtual="/ssi/dbclose.asp"-->
