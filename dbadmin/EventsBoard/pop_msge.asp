<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ICO - Website Administration </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body" onload="focus();">
<!-- #include virtual="/dbadmin/ssi/incl/top_pop.asp" -->
<div id="closebutton"><a href="javascript:window.close();" title="Close this window"><img src="/dbadmin/images/closewindow.gif" width="12" height="12" border="0" alt="Close"></a></div>

<%
 'Get ID number of event'
EventID=Request.QueryString("EventID")
'if it is numeric then request info from DB'
if IsNumeric(EventID) and EventID <> "" then

	set rs=SQLConnection.execute("Select e.StartDate, e.EndDate, e.Title, e.Description, e.AdminID, m.FullName from ComsCentre_EventsCalendar e , AdminUsers m Where e.AdminID=m.AdminID and e.EventID=" & EventID & " and e.AdminID=" & Session("AdminID") )
	'if we have found info then display it on the screen'
	if NOT rs.eof Then
		RecipNames = ""

		subject = "Event Reminder '"& DisplayText(rs(2)) &"' - " & FormatDateTimeIRL(rs(0),1)
		message = "Hi all, <br/>" & VbNewline & _
		"I would like to remind you about this event <b>" & DisplayText(rs(2)) & "</b><br/>" & VbNewline & _
		"taking place " & FormatDateTimeIRL(rs(0),1) & " @ " & FormatDateTimeIRL(rs(0),4) & "<br/>" & VbNewline & _
		Replace(rs(3),"''","'") & "<br/>" & VbNewline & _
		"I hope to see you all there.<br/>" & VbNewline & _
		session("AdminUserName")

			'Get Attendee list for this event'
			Set rsAttend=SQLConnection.execute("exec usp_SEL_WhosComingToEvent " & EventID)

			if not rsAttend.eof then
%>
<h1>Contact Everybody</h1>
      <p>You can send a reminder about your event to all participants:
        <%
				do until rsAttend.eof
					RecipNames = RecipNames & DisplayText(rsAttend(1))
				rsAttend.MoveNext
					if not rsAttend.eof then RecipNames = RecipNames &   ", "
				Loop
				response.Write RecipNames
			%>
      </p>
      <!-- content -->
      <form action="pop_send.asp" method="post" name="email" style="margin:0" id="eventmail">
        <table width="100%" cellspacing="1" border="0">
          <tr>
            <td width="80"><strong>To</strong></td>
            <td><strong><%=RecipNames%></strong></td>
          </tr>
          <tr>
            <td valign="top"><strong>Subject</strong></td>
            <td><input type="text" size="70" class="TextInput" name="subject" value="<%=subject%>" /></td>
          </tr>
          <tr>
            <td valign="top"><strong>Message</strong></td>
            <%
				If session("safari") then
			%>
            <td valign="top"><textarea cols="60" rows="10" name="p_message" id="p_message" tabindex="6" class="TextInput"><%=message%></textarea></td>
            <%
				else
  		    %>
            <td valign="top"><input type="hidden" id="p_format" name="p_format" value="html" class="" />
              <textarea cols="60" rows="10" name="p_message" id="p_message" tabindex="6" class="TextInput"><%=message%></textarea>
              <script language="javascript" type="text/javascript" src="/dbadmin/ssi/lib/mailtoolbar.js"></script>
              <script type="text/javascript">
						if (document.getElementById) {
								var tb = new dcToolBar(document.getElementById('p_message'),
								document.getElementById('p_format'),'/dbadmin/images/mailer/');

								tb.btStrong('Bold');
								tb.btEm('Italic');
								tb.btIns('Underline');
								tb.btDel('Stroke');
								//tb.btQ('Citation en ligne');
								//tb.btCode('Code');
								tb.addSpace(10);
								tb.btBr('Carriage Return');
								tb.addSpace(10);


								if (tb.wysiwygAble()) {
									tb.btBquote('Quote');
									tb.btList('List','ul');
									tb.btList('Numeric List','ol');
									tb.addSpace(10);
									tb.draw('\nYou can use the following formatting options.');
									/* la zone wysiwyg est construite apres le chargement de la page */
									addEvent(window, 'load', tb.drawWysiwyg);

									/* ajout du traitement de la zone wysiwyg lors de la soumission du billet,
									 * et si on quitte la page, tout en conservant les traitements precedents.*/
									addEvent(window, 'load',
										function() {addEvent(document.getElementById('email'), 'submit', tb.updateForPost)}
									);

									function addOnbeforeunload(e) {
										if (!e && window.event) e = window.event;
										var oldOnbeforeunload = window.onbeforeunload;

										if (typeof(oldOnbeforeunload) != 'function') {
											window.onbeforeunload = tb.updateForPost;
										} else {
											window.onbeforeunload = function() {
												tb.updateForPost();
												return oldOnbeforeunload(e);
											};
										}
									}
									addEvent(window, 'load', addOnbeforeunload);
								}
							}

						</script>
            </td>
            <%
				end if 'safari
		    %>
          </tr>
          <tr>
            <td colspan="2" style="padding-left:80px;"><input type="hidden" name="EventId" value="<%=EventID%>" />
              <input name="submit" type="submit" class="subscrsubmit" value="SEND MESSAGE &raquo;" /></td>
          </tr>
        </table>
      </form>
      <!-- [end] content -->
<%
			else
				response.Write "<br/>So far nobody has shown interest to your event ;-("
			end if 'rsAttend eof
	end if 'eof
end if 'is numeric
%>
<!-- #include virtual="/dbadmin/ssi/incl/base_pop.asp" -->
</body>
</html>
