<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->

<!--#include virtual="/ssi/dbconnect.asp"-->

<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


<%
' include at the top '
EventID = request.querystring("EventID")
if Not IsNumeric(EventID) or EventID ="" then
	response.redirect "/dbadmin/eventsboard/default.asp?ThisERR=can not process email-news id not numeric"
end If

'response.write "exec usp_SEL_EventBoard "& EventID &",1,null,null"

set News = sqlconnection.execute("exec usp_SEL_EventBoard "& EventID &",1,null,null")

if News.eof THEN
	response.redirect "/dbadmin/eventsboard/default.asp?ThisERR=can not process email- eof for news id " & EventID
end if
%>
<title><%=TitlePage%></title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<body class="body">

<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
			<%
			Set usr = sqlconnection.execute("select count(*) from (SELECT PrivateEmail as 'email'  FROM Members_details union SELECT PracticeEmail  as 'email'  FROM Members_practice) as B")
			%>
			<h1>Mail this event to all members (<%=usr(0)%>)</h1>

			<%
			Set usr =	nothing
					
			subject = trim(DisplayText(News("Title")))

			newstime = FormatDateTimeIRL(News("StartDate"),1) 
			If CDate(News("StartDate")) <> CDate(News("EndDate")) Then
				newstime = " - " & FormatDateTimeIRL(News("EndDate"),1) 
			End if	

			message = trim(DisplayText(News("Description")))


			%>
			<table>
			<form name="news" action="mail-exe.asp" method="post" onsubmit="return f_validate(this);">
			<tr>
				<td valign="top" class="bluetext">Subject:</td>
				<td><input type="text" name="subj" size="68" value="<%=subject%>" class="TextInput"> </td>
			</tr>
			<tr>
				<td valign="top" class="bluetext">Message</td>
				<td><textarea cols="80" name="mssge" rows="28" class="TextInput"><%response.write vbnewline & vbnewline & message%>
				<%
					response.write vbnewline & vbnewline &"<br/> Date: " & newstime
				%>
				</textarea></td>
			<tr>
				<td></td>
				<td><input type="submit" value="Send" class="TextInput"> </td>
			</tr>
			</form>

			</table>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
