<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/fct/translator.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/cleantext_function.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/mailer.asp " -->
<!--#include virtual="/ssi/fct/emailtemplate.asp " -->
<%
ReadReceipt	= 0
ReplytoMsgeID	= 0

message		= request.form("p_message")
subject		= validateText(request.form("subject"))
EventID		= Request.form("EventID")

'response.Write "EventID = " & EventID
'response.Write "subject = " & subject

'response.Write "<hr>message plain<br/>" &  message
message		= RemoveAllHTMLTags(RemoveJavascript(RemoveMsWordTags(message)))

'if it is numeric then request info from DB'
if IsNumeric(EventID) and EventID <> "" then
	Set rsAttend=SQLConnection.execute("exec usp_SEL_WhosComingToEvent " & EventID)
	if not rsAttend.eof then
		Do Until rsAttend.eof

			RecipientEMail	= DisplayText(rsAttend("FullName"))
			'response.end
			subject			= RemoveAllHTMLTags(RemoveJavascript(subject))


			'#####################'
			'## send email      ##'
			'#####################'
			if not isNumeric(MailMessageOptOut) or MailMessageOptOut="" then
				MailMessageOptOut=0
			end if
			if MailMessageOptOut = 1 then
				if instr(RecipientEMail,"@") > 1 then
					MailContent	= HTML_Top & VbNewline & _
					message & VbNewline & _
					HTML_bottom
					call SendMail("info@ICO.ie", RecipientEMail, subject, MailContent)
				end if
			end if

		rsAttend.Movenext
		Loop
	else
		ThisErr = "Err 6: Your event ("& EventID &") can not be found in the database"
	end if' eof rsAttend
	set rsAttend = nothing
else
	ThisErr = "Err 7: Your event ("& EventID &") can not be found in the database"
end if 'is numeric

response.redirect "pop_mssgeposted.asp"
%>