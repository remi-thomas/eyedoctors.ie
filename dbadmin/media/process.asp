<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/ResizeFunction.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/Publish_function.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<%
WidthPreview	= 0
WidthFile		= 0

Server.ScriptTimeout = 50000
Set Upload = Server.CreateObject("Persits.Upload")
Upload.ProgressID = Request.QueryString("PID")
' we use memory uploads, so we must limit file size
Upload.SetMaxSize 3000000, True
' Save to memory. Path parameter is omitted'
Upload.Save

' Intercept all exceptions to display user-friendly error
'On Error Resume Next


ttlrecord	= Upload.Form("ttlrecord")
if not IsNumeric(ttlrecord) or ttlrecord = "" then
	ttlrecord = 1
end if
media		= Upload.Form("media")

Select Case lcase(media)
	Case "img","image"
		MediaPath = "/medium/images/"
		PreviewPath = "/medium/images/"
		MediaType = 1
	Case "snd","audio"
		MediaPath = "/medium/audio/"
		MediaType = 2
	Case "mov","video"
		MediaPath = "/medium/video/"
		MediaType = 4
	Case "doc","document"
		MediaPath = "/medium/files/"
		MediaType = 3
	Case ".zip"
		MediaPath = "/medium/files/"
		MediaType = 5
	Case ".sit"
		MediaPath = "/medium/files/"
		MediaType = 6
	Case "streaming"
		ttlbox = 4
		MediaTitle = "streaming video"
		MediaType = 7
	Case "pdf"
		MediaPath = "/medium/files/"
		MediaType = 8
	Case Else
		MediaPath = "/medium/files/"
	'	response.Write "error 1: media type undefined"
	'	response.end
End Select

if not IsNUmeric(MediaType) or MediaType="" then
	MediaType	= Upload.Form("MediaType")
end if

For I = 1 to ttlrecord
	resizeme = ""
	Newfile =  ""
	Online	= Upload.Form("Online" & I)
		if not IsNumeric(Online) or Online = "" then
			Online = 0
		else
			Online = 1
		End if
	Caption = left(ValidateText(Upload.Form("Caption" & I)),250)

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~ 1.  Files                         ~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

	fle		= Upload.Form("fle" & I)
	fle		= trim(Replace(fle," ","_"))

	if len(fle) > 4 then
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~ 11. locate media already uploaded ~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		if media = "img" or media="image" then
			Newfile = CleanUpPath(fle)
			response.Write "<br>Newfile " & Newfile
			FileSize = FileSizeFunction(server.mappath(Newfile))
			MediaName = replace(lcase(GetFileFromPath(Newfile)),".jpg","")
			MediaName = left(Replace(trim(MediaName)," ","_"),60)
			MediaName = Replace(MediaName,"'","")
			MediaPath = GetDirectoryFromPath(Newfile)
			PreviewImg = left(MediaName,60) & "_"& GenerateRandomletter() & ".jpg"
			PreviewFile = CleanUpPath(PreviewPath &  PreviewImg)
		else

			if MediaType <> 7 then
				Newfile = CleanUpPath(fle)
				FileSize = FileSizeFunction(server.mappath(Newfile))
				MediaName = lcase(GetFileFromPath(Newfile))
				MediaName = left(Replace(trim(MediaName)," ","_"),60)
				MediaName = Replace(MediaName,"'","")
				'linking to existing file, the path is the directory the file is in'
				MediaPath = GetDirectoryFromPath(Newfile)
			else
				Newfile		= fle
				MediaPath	= fle
			end if

		end if
		If InStr(trim(Newfile)," ") > 1 then
			response.Redirect "default.asp?ThisErr=The File You're trying to map contains a space or an illegal correct, with your ftp client, rename the file, remove spaces and map this file again."
		end if
	else
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~ 12. file ftped using uploader     ~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		set ThisFile = Upload.Files("FILE" & i)

		if Not ThisFile Is Nothing Then
			MediaName = ShortName(ThisFile.FileName)
			MediaName = left(Replace(trim(MediaName)," ","_"),60)
			MediaName = Replace(MediaName,"'","")

			'save the file uder a different name'

			SaveAsFileName = MediaName & "-" & GenerateRandomletter()

			Newfile = CleanUpPath(MediaPath & SaveAsFileName  & lcase(ThisFile.Ext))
			response.Write "<br>Newfile " & Newfile
			'save media'
			ThisFile.SaveAs server.mappath(Newfile)
			' get media size'
			FileSize =  FileSizeFunction(server.mappath(Newfile))

			response.Write "<br>FileSize " & FileSize
			'image or not image.. that is the question.'
			'resize pict to create thumbnail'
			PreviewImg = left(MediaName,60) & "_"& GenerateRandomletter() & lcase(ThisFile.Ext)
			PreviewFile = CleanUpPath(PreviewPath &  PreviewImg)
			'response.write "<hr><br/>Newfile = " & Newfile & "<br/>PreviewFile = " & PreviewFile& "<br/>PreviewImg = " & PreviewImg

		end if ' Not ThisFile Is Nothing
	end if 'len(fle) > 4 = locate media already uploaded


	if len(Newfile) > 5 then
		'~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~~~~~ resize pict ? ~~~~~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~'
		if media = "img" or media="image" then

			resizeme = Upload.Form("resize" & I)
			if not IsNumeric(resizeme) or resizeme = "" then
				resizeme = false
			else
				if resizeme = 1 then
					resizeme = true
				else
					resizeme = false
				end if
			end if

			response.Write "<br/>resizeme ("& I &") " & resizeme

			if resizeme then
				response.write "<br/> Auto resizing to max 160X120"
				call Copy_file(PreviewFile,Newfile)
				call GetSizeFunction(PreviewPath,PreviewImg,ArtPicwidth,ArtPicHeight)
				'store the width for later use'
				WidthFile = ArtPicwidth
				if ArtPicwidth	> 160 or ArtPicHeight > 120 then
					call ResizeFunction(PreviewPath,PreviewImg,ArtPicwidth,ArtPicHeight,160,120,PreviewImg)
				end if
				MediaPath = PreviewPath
			else
				'no resize wanted : keep the same name for db'
				PreviewFile = Newfile
				PreviewImg	= GetFileFromPath(Newfile)
				PreviewPath = MediaPath
			end if


			response.write "<br/>GetSizeFunction " & PreviewPath& "," & PreviewImg & "," & ArtPicwidth & "," & ArtPicHeight

			'get preview width'
			call GetSizeFunction(PreviewPath,PreviewImg,ArtPicwidth,ArtPicHeight)
			WidthPreview = ArtPicwidth
			if WidthPreview="" or not isNumeric(WidthPreview) then
				WidthPreview = 0
			end if
			'make sure width is numeric'
			if WidthFile="" or not isNumeric(WidthFile) then
				WidthFile = 0
			end if
		else
			WidthPreview	= 0
			WidthFile	= 0
		end if 'media is image

		Season = year(now())

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~ 2. DB INSERT                      ~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

		if MediaType = 7 then
			WidthPreview	= 0
			WidthFile	= 0
			sql = "INSERT INTO ComsCentre_Media (Path,FileName,WidthFile,Preview,WidthPreview,FileSize,Caption,season,MediaType,Online,DatePublished,AdminID)" & _
			"VALUES('"& MediaPath &"','"& Replace(GetFileFromPath(Newfile),"'","''") &"',"& WidthFile &",'"& PreviewImg &"',"& WidthPreview &",'"& FileSize &"','"& Caption &"','"& Season &"',"& MediaType & "," &  Online & ",GetDate(),"& Session("AdminID") &")"
			response.Write sql
			sqlconnection.execute(sql)
			'response.end
		else
			if ThisFileExists(Newfile)  then
				sql = "INSERT INTO ComsCentre_Media (Path,FileName,WidthFile,Preview,WidthPreview,FileSize,Caption,season,MediaType,Online,DatePublished,AdminID)" & _
				"VALUES('"& MediaPath &"','"& Replace(GetFileFromPath(Newfile),"'","''") &"',"& WidthFile &",'"& PreviewImg &"',"& WidthPreview &",'"& FileSize &"','"& Caption &"','"& Season &"',"& MediaType & "," &  Online & ",GetDate(),"& Session("AdminID") &")"
				sqlconnection.execute(sql)
			else
				response.Write "<br>" & Newfile & " doesn't seem to exist on the server "
				response.end
			end if
		end If
		

	end if 'file name > 4


		'response.end

		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		'~ 3. HANDLE ERRORS                  ~'
		'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
		' 8 is the number of "File too large" exception
		if Err.Number = 8 Then
		   Response.Write "Your file '"& Newfile &"' ("& i &") is too large. Please try again."
		Else
		   if Err <> 0 Then
			  Response.Write "<br>An error occurred: " & Err.Description
		   Else
				if len(Newfile) > 5 then
					Response.Write "<br> Image "&  i &" Success!"
				end if
		   End if
		End if

		Newfile = ""
		response.Write "<hr>"
Next




response.Redirect "/dbadmin/media/view.asp?MediaType=" & MediaType & "&amp;season=" & season

%>
<!--#include virtual="/ssi/dbclose.asp"-->