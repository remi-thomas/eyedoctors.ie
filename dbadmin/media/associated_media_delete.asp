<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - ICO</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp"-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" name="content">
  <tr valign="top">
    <td bgcolor="#F0F0F0" rowspan="3" class="default" width="2%">&nbsp;</td>
    <td class="default" width="1%" bgcolor="#F0F0F0">&nbsp;</td>
    <td width="97%" bgcolor="#F0F0F0" class="default">&nbsp;</td>
  </tr>
</table>
<br />&nbsp;
<span class="bluetext">
<%
SupportID	= trim(Request.querystring("SupportID"))
If not IsNumeric(SupportID) or SupportID = "" then
	response.Write "Error! Support ID ("& SupportID &") is not numeric : " & IsNumeric(SupportID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

MediaID	= trim(Request.querystring("MediaID"))
If not IsNumeric(SupportID) or SupportID = "" then
	response.Write "Error! Media ID ("& MediaID &") is not numeric : " & IsNumeric(MediaID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

sqlconnection.Execute("delete from ComsCentre_MediaAssociated where MediaID=" & SupportID & " and AssociatedMediaID="& MediaID)
sqlconnection.Execute("delete from ComsCentre_MediaAssociated where MediaID=" & MediaID & " and AssociatedMediaID="& SupportID)
%>
<!--#include virtual="/ssi/dbclose.asp"-->

<%
Season		= Request.querystring("Season")
mypage		= Request.querystring("mypage")
MediaType	= Request.querystring("MediaType")

response.Redirect "associated_media.asp?SupportID=" & SupportID & "&Season=" & Season & "&mypage=" & mypage & "&MediaType=" & MediaType
%>