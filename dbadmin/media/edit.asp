<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>ICO - Website Administration  - Media</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/scripts/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<%
section = "media"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<h1>Edit Media</h1>

<%
MediaID		= trim(Request.querystring("MediaID"))

If not IsNumeric(MediaID) or MediaID = "" then
	response.Write "Error! MediaID ("& MediaID &") is not numeric : " & IsNumeric(MediaID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

set rs = sqlconnection.execute("usp_GET_MediaDetails " & MediaID)
If rs.eof then
	response.Write "Error! sql returns end of file - this media doesn't exist ion  our database" & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

%>
<form name="EditMedia" method="post" action="process_edit.asp" id="mediaedit">
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td valign="top" rowspan="6" style="padding-right:15px"><%
			If rs(4) = 1 then
				ThumbnailPath = rs(0) & rs(2)
				response.Write "<a href=""" &  rs(0) & "jpg/"& rs(1) & """ target=""media""><img src=""" & ThumbnailPath & """ alt=""Click For Full Size"" border=""0""/></a>"
			else
				response.Write MediaTypeGraph(rs(4)) & "<br/>" & VbNewline
				response.Write "<a href=""" & rs(0) & rs(1) & """>"  & rs(0) & rs(1) & "</a>"
			end if
			%>
      </td>
      <td valign="top"><p><strong>Description/Title</strong> <em>(150 characters max)</em><br />
        <input type="text" name="Caption" value="<%=rs(3)%>" class="TextInput" size="90"></p>
      </td>
    </tr>
    <tr>
      <td valign="top"><p><input type="checkbox" name="Online" value="1" <%if rs(5)=1 then response.Write "checked"%> class="TextInput"> Make this media <b>available</b>
        </p></td>
    </tr>
    <tr>
      <td valign="top"><p><%
			response.write"<a href=""JavaScript:openWindowNamed('/dbadmin/media/associated_media.asp?SupportID="& MediaID &"&amp;season=" & rs("season") &"',700,650,'media')"" class=""dbtext"">"
			If rs(4) = 1 then
				if rs("AssociatedMedia") > 0 then
					response.Write "<img src=""/dbadmin/images/ok.gif"" alt="""& AssociatedMedia&" attached media"" width=""16"" height=""12"" border=""0""/> This image is available in another format or linked to other media."
				else
					response.Write "<img src=""/dbadmin/images/plus.gif"" alt=""Link to other Media"" width=""16"" height=""16"" border=""0"" /> "
					response.Write "This image is not linked to any other media."
				end if
			end if
			response.Write "</a>"
			%></p>
      </td>
    </tr>
    <tr>
      <td valign="top"><input type="hidden" name="MediaID" value="<%=MediaID%>">
        <input type="submit" name="Submit" value="Modify &raquo;&raquo;" class="subscrsubmit">
      </td>
    </tr>
  </table>
</form>
<%
'News'
if cint(rs("ttlLinksPR"))  > 0 then
		set rsdoc = sqlconnection.execute("usp_SEL_Coms_PR_MediaList " & MediaID & ",1")
		if not rsdoc.eof then
			%>
<h3>This media is also attached to the following Prs:</h3>
<table cellpadding="2" border="0" cellspacing="0" style="font-size:12px">
<%
				Do until rsdoc.eof
			%>
<tr>
  <td>News ID : <%=rsdoc("PrID")%></td>
  <td><%=DisplayText(rsdoc("Caption"))%></td>
</tr>
<%
			rsdoc.Movenext
			Loop
		end if
		set rsdoc = nothing
end if


set rs = nothing
%>
</table>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
