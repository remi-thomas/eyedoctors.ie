<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<%
	section = "media"

Server.ScriptTimeout = 90000
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = "/dbadmin/ssi/ImgUpl/framebar.asp?to=10&" & PID

media = lcase(request.querystring("media"))

if media="" then
	media="image"
end if

Select Case media
	Case "img","image"
		ttlbox = 5
		MediaTitle = "Image"
		MediaType = 1
	Case "snd","audio"
		ttlbox = 2
		MediaTitle = "Audio"
		MediaType = 2
	Case "mov","video"
		ttlbox = 1
		MediaTitle = "Video"
		MediaType = 4
	Case "doc","document"
		ttlbox = 3
		MediaTitle = "Document"
		MediaType = 3
	Case ".zip"
		ttlbox = 2
		MediaTitle = "PC Compressed File"
		MediaType = 5
	Case ".sit"
		ttlbox = 2
		MediaTitle = "Mac OS Compressed File"
		MediaType = 6
	Case "streaming"
		ttlbox = 4
		MediaTitle = "Streaming Video"
		MediaType = 7
	Case ".pdf"
		ttlbox = 2
		MediaPath = "/media/documents/"
		MediaType = 8
	Case Else
		response.Write "error 1: media type undefined"
		response.end
End Select

if MediaType ="" then
	MediaType = 1
	media = "image"
end if

season = trim(request.querystring("season"))
if len(season) < 3 then
	season = year(now())
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>ICO- Website Administration  - media</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language = "JavaScript" type="text/javascript">
	function ShowProgress()
	{
	  strAppVersion = navigator.appVersion;
	  if (<%
			For i = 1 to ttlbox
				if i> 1  then
					response.Write " || "
				end if
				response.Write "document.UplMedia.FILE"& I &".value != """" "
			Next
		%>)
	  {
		if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
		{
		  winstyle = "dialogWidth=385px; dialogHeight:135px; center:yes";
		  window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
		}
		else
		{
		  window.open('<% = barref %>&b=NN','','width=370,height=115', true);
		}
	  }
	  return true;
	}

	function SelectImage(ThisID)
	{
		var leftPos = (screen.availWidth-500) / 2;
		var topPos = (screen.availHeight-400) / 2;
			ModalWin('/dbadmin/ssi/fct/BrowseDirLink.asp?ThisID=' + ThisID, 'browse', topPos, leftPos, 600, 400, true, true, false);
	}
	function ModalWin(page, name, top, left, width, height, scrollBars, Resizable, Toolbar, StatusBar)
	{
		try
		{
			var strParam = ',height=' + height + ',top=' + top + ',left=' + left + ',menubar=no,';

			if(Toolbar)
			{
				strParam += 'toolbar=yes,';
			}else{
				strParam += 'toolbar=no,';
			}

			if(StatusBar)
			{
				strParam += 'status=yes,';
			}else{
				strParam += 'status=no,';
			}


			if(Resizable)
			{
				strParam += 'resizable=yes,';
			}else{
				strParam += 'resizable=no,';
			}

			if(scrollBars)
			{
				strParam += 'scrollbars=yes';
			}else{
				strParam += 'scrollbars=no';
			}

			MODAL = window.open(page, name, 'width=' + width + strParam);
			MODAL.onblur = RaiseModal;
		}
		catch(er){}
	}

	function RaiseModal()
	{
		try
		{
			MODAL.focus();
		}
		catch(er){}
	}

</script>
</head>
<body class="BODY">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<h1>Add New Media</h1>

<h3>Choose Media Type:</h3>
<p>
  <%
set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
	Do until rs.eof
		If rs(0) = MediaType then
			astyle= "dbText"
		else
			astyle= "bluetext"
		end if

	response.Write "<a href=""default.asp?Media="& trim(lcase(rs(1))) & "&amp;MediaType="& rs(0) &"&amp;season="& season&""" class="""& astyle &""">" & rs(1) & "</a>" & VbNewline & _
				" &nbsp;|&nbsp; " & vbnewline
	rs.Movenext
	Loop
%>
</p>

<br/>
<form name="UplMedia" method="post" ENCTYPE="multipart/form-data" ACTION="process.asp?<% = PID %>" OnSubmit="return ShowProgress();" id="mediaupload">
  <%
	i = 0
	For i = 1 to ttlbox
	If i mod 2 = 0 then
		BgColor = "#ecf2f2"
	else
		BgColor = "#deeaea"
	end if
%>
  <fieldset>
  <legend><b>
  <%
		response.Write MediaTitle & " " & i
	%>
  </b></legend>
  <table border="0" cellspacing="0" cellpadding="0" name="content" width="100%" bgColor="<%=BgColor%>" align="center">
    <tr>
      <td valign="top"> Description/Title </td>
      <td valign="top"><input type="text" name="Caption<%=i%>" value="" class="TextInput" size="90">
        <br />
        (150 characters max)</td>
      </td>
    </tr>
    <%
		if MediaType <> 7  then
	%>
    <tr>
      <td valign="top"> Select <%=MediaTitle%> </td>
      <td align="left"><INPUT TYPE="FILE" SIZE="50" NAME="FILE<%=i%>" class="TextInput" />
      </td>
    </tr>
    <tr>
      <td valign="top"> or Locate <%=MediaTitle%> already on server</td>
      <td><input type="button" name="ImageSelection" id="ImageSelection" value="Browse Directory" class="TextInput" onClick="SelectImage(<%=i%>);"/>
        &nbsp;
        <input type="text" name="fle<%=i%>" SIZE="45" class="TextInput">
      </td>
    </tr>
    <%
		else
	%>
    <tr>
      <td valign="top"> Select <%=MediaTitle%></td>
      <td align="left"><input type="text" name="fle<%=i%>" value="http://www.youtube.com/watch?v=" SIZE="45" class="TextInput" onFocus="this.select();">
      </td>
    </tr>
    <%
		end if
	%>
    <tr>
      <td valign="top">Make this media <b>available</b> as soon as uploaded</td>
      <td><input type="checkbox" name="Online<%=i%>" value="1" checked class="TextInput"></td>
    </tr>
    <%
		If MediaType = 1 then
	%>
    <tr>
      <td valign="top">Create a Thumbnail from this image</td>
      <td><input type="checkbox" name="resize<%=i%>" value="1" checked class="TextInput">
        <span class="dbtext">This will create a second image. It will not reduce the dimension of the current file!</span>
      </td>
    </tr>
    <%
		else
	%>
    <%
		end if
	%>
  </table>
  </fieldset>
  <br />
  <%
	Next
  %>

  <input type="hidden" name="ttlrecord" value="<%=ttlbox%>"/>
  <input type="hidden" name="MediaType" value="<%=MediaType%>"/>
  <input type="hidden" name="media" value="<%=media%>"/>
  <div id="submit"><input type="submit" name="Submit" value="Upload &raquo;&raquo;" class='TextInput'/></div>
</form>
<!--{end] content-->
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
