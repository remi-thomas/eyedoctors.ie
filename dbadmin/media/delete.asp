<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - ICO</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<%
section = "media"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp"-->
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" name="content">
  <tr valign="top">
    <td bgcolor="#F0F0F0" rowspan="3" class="default" width="2%">&nbsp;</td>
    <td class="default" width="1%" bgcolor="#F0F0F0">&nbsp;</td>
    <td width="97%" bgcolor="#F0F0F0" class="default">&nbsp;</td>
  </tr>
</table>
<br />&nbsp;
<span class="bluetext">
<%
MediaID		= trim(Request.querystring("MediaID"))

If not IsNumeric(MediaID) or MediaID = "" then
	response.Write "Error! MediaID ("& MediaID &") is not numeric : " & IsNumeric(MediaID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

set rs = sqlconnection.execute("usp_GET_MediaDetails " & MediaID)
If rs.eof then
	response.Write "Error! sql returns end of file - this media doesn't exist in  our database" & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
else
	FilePath = rs(0) & rs(1)
	ThumbnailPath = rs(0) & rs(2)
end if

mode = trim(request.querystring("mode"))
if mode = "delete" then
	'- 1 delete from table media'
	sqlconnection.execute("Delete from ComsCentre_Media where MediaID=" & MediaID)
	response.Write "<p class=""bluetext"">Media has been removed from the database</p>"
	'- 2 delete from table media'
	if trim(request.querystring("ttl")) > 0 then
		sqlconnection.execute("Delete from ComsCentre_NewsMedia where MediaID=" & MediaID)
		response.Write "<p class=""bluetext"">Media has been unattached from all News</p>"
		sqlconnection.execute("Delete from ComsCentre_DocumentMedia where MediaID=" & MediaID)
		response.Write "<p class=""bluetext"">Media has been removed from all documents</p>"

		sqlconnection.execute("Delete from ComsCentre_MediaAssociated where MediaID=" & MediaID)
		sqlconnection.execute("Delete from ComsCentre_MediaAssociated where AssociatedMediaID=" & MediaID)
		response.Write "<p class=""bluetext"">Media has been removed from Medias Media</p>"

	end if
	'- 2 delete from path'

	delete_file(FilePath)

	' - 22 delete thumbnail if exists'
	if rs(4) = 1 then
		delete_file(ThumbnailPath)
	end if

	response.Write "<p class=""dbNotes"">Media has been physically removed </p>"
else
	%>

	<table>
		<tr>
			<td colspan="3" class="bluetext">You're going to remove the following file. <br/>This action can not be reversed</td>
		</tr>
		<tr>
			<td class="bluetext">
				<% response.write MediaTypeGraph(rs(4)) %>
			</td>
			<td class="bluetext">
				<% response.write DisplayText(rs(3)) %>
			</td>
			<td>
				<a href="<% response.write FilePath %>" class="bluetext"><% response.Write FilePath %></a>
			</td>
		</tr>
	</table>
	<%
	'attached to programme'
	if cint(rs(6)) > 0 then
		set rsdoc = sqlconnection.execute("spListEventPerMedia " & MediaID)
		if not rsdoc.eof then
		%>
		<table>
		<tr>
			<td class="bluetext" colspan="2">WARNING</td>
		</tr>
		<tr>
			<td class="bluetext" colspan="2">This media is attached to the following programme:</td>
		</tr>
		<%
			Do until rsdoc.eof
		%>
		<tr>
			<td  class="bluetext">Event. ID : <%=rsdoc(6)%></td>
			<td  class="bluetext"><%=DisplayText(rsdoc(7))%></td>
		</tr>
		<%
		rsdoc.Movenext
		Loop
		%>
		</table>
		<%
		end if
		set rsdoc = nothing
	end if

	'attached to PR'
	if cint(rs(6)) > 0 then
		set rsdoc = sqlconnection.execute("spListPRPerMedia " & MediaID)
		if not rsdoc.eof then
		%>
		<table>
		<tr>
			<td class="bluetext" colspan="2">WARNING</td>
		</tr>
		<tr>
			<td class="bluetext" colspan="2">This media is attached to the following Press Release:</td>
		</tr>
		<%
			Do until rsdoc.eof
		%>
		<tr>
			<td  class="bluetext">Event. ID : <%=rsdoc(6)%></td>
			<td  class="bluetext"><%=DisplayText(rsdoc(7))%></td>
		</tr>
		<%
		rsdoc.Movenext
		Loop
		%>
		</table>
		<%
		end if
		set rsdoc = nothing
	end if


	%>

	<table>
		<tr>
			<form name="delete" method="get" action="delete.asp">
			<td class="dbtext" colspan="2"><input type="submit" name="Submit" value="Confirm Delete" class="TextInput"></td>
			<input type="hidden" name="MediaID" value="<%=MediaID%>">
			<input type="hidden" name="mode" value="delete">
			<input type="hidden" name="ttl" value="<%=cint(rs(6))%>">
			</form>
		</tr>
	</table>
<%
end if
set rs = nothing
%>
</table>
</span>
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
