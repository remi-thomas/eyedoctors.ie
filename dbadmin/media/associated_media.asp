<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->

<%
cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 20

SupportID		= trim(Request.querystring("SupportID"))
If not IsNumeric(SupportID) or SupportID = "" then
	response.Write "Error! SupportID ("& SupportID &") is not numeric : " & IsNumeric(SupportID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
else
	SupportID = cint(SupportID)
end if

set rsMedia = sqlconnection.execute("usp_GET_MediaDetails " & SupportID )
If rsMedia.eof then
	response.Write "Error! sql returns end of file - this media doesn't exist in  our database" & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

MediaType = trim(request.querystring("MediaType"))
'response.Write MediaType
If isNull(MediaType) or MediaType="" then
	MediaType = 1
end if

FirstLetter = left(trim(request.querystring("FirstLetter")),1)

mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body" onLoad="window.focus()">
<!--#include virtual="/dbadmin/ssi/incl/top_pop.asp"-->
<div id="closebutton"><a href="javascript:window.close();" title="Close this window"><img src="/dbadmin/images/closewindow.gif" width="12" height="12" border="0" alt="Close"></a></div>

<h1>Associate / Link Media</h1>

				<%
				set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
					Do until rs.eof
						If rs(0) = cint(MediaType) then
							astyle= "bluetext"
							MediaCurrentlyDisplay = rs(1)
						else
							astyle= "bluetext"
						end if

					response.Write "<a href=""associated_media.asp?SupportID=" & SupportID & "&amp;Season=" & Season & "&amp;MediaType=" & rs(0) & "&amp;FirstLetter=" & FirstLetter & "&amp;mypage=" & mypage & """ class="""& astyle &""">" & rs(1) & "</a>" & VbNewline & _
								" &nbsp;|&nbsp; " & vbnewline
					rs.Movenext
					Loop
				%>

<h3><%=MediaCurrentlyDisplay%> A to Z</h3>
<p>
		<%
		set rs = sqlconnection.execute("exec usp_SEL_AZ_Media " & MediaType & ",Null")
		Do until rs.eof
			response.Write "<a href=""associated_media.asp?SupportID=" & SupportID & "&amp;Season=" & Season & "&amp;MediaType=" & MediaType & "&amp;FirstLetter=" & rs(0) & "&amp;mypage=" & mypage &""""
			if lcase(FirstLetter) = lcase(rs(0)) then
				response.Write ""
			else
				response.Write "class=""bluetext"""
			end if
			response.Write">" & Ucase(rs(0)) & "</a> &nbsp;|&nbsp; "
		rs.Movenext
		Loop
		%>
</p>

<%
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open "usp_SEL_MediaPerType " & MediaType & ","& SupportID &",'" & FirstLetter & "'", sqlconnection

'response.Write "<hr>usp_SEL_MediaPerType " & MediaType & ","& SupportID &",'" & FirstLetter & "','"& Season &"'<hr>"


If rs.eof then
	response.Write "<p><strong>There are no '"& Lcase(MediaTypeText(MediaType)) &"' files stored in the database.</strong></p>" & VbNewline
	response.end
end if

rec = 1
rs.movefirst
rs.pagesize = maxdisplay
rs.absolutepage = mypage
maxpages = cint(rs.pagecount)
' display the total of records returned by sql query'
intRecordCnt = rs.RecordCount

if maxpages > 1 then
	response.Write "<p>"
	call Paging
	response.Write "</p>" & VbNewline & _
	"<hr class=""hrgrey"">" & VbNewline
end if
%>
<form name="associatedmedia" method="post" action="associated_media_process.asp" onSubmit="return validate_form(this);" style="margin:0;padding:0">
<table width="100%" cellspacing="0" cellpadding="0" border="0" id="medialink">
	<tr>
		<th scope="col"><img src="/dbadmin/images/paperclip.gif" width="20" height="20" alt="Tick to link"></th>
		<th scope="col"><b>Title</b></th>
		<th scope="col"><b>Path</b></th>
		<th scope="col"><b>Size</b></th>
		<th scope="col" width="20"><img src="/dbadmin/images/icon_online.gif" alt="online/offline?" width="20" height="20"/></th>
	</tr>
<%
	Do until rs.eof  or (rec > maxdisplay)
	If rec mod 2 = 0 then
		BgColor = "#deeaea"
	else
		BgColor = "#ecf2f2"
	end if
	'do not link an iamge to itself'
	if rs(0) <> SupportID then
	%>
	<tr>
		<td bgcolor="<%=BgColor%>">
			<%
			'media already linked'
			if rs("AssociatedMedia") = 1 then
			%>
			 <a href="associated_media_delete.asp?page=popmedia&amp;mypage=<%=mypage%>&amp;SupportID=<%=SupportID%>&amp;MediaType=<%=MediaType%>&amp;MediaID=<%=rs(0)%>&amp;section=<%=section%>&amp;Season=<%=Season%>" onClick="return confirm('Are you sure you want to unattach this media?');"><img src="/dbadmin/images/unlink.gif" alt="Unattach this media" hspace="3" border="0" width="16" height="16" /></a>
			<%
			else
			%>
			 <input type="checkbox" class="TextInput" name="MediaID<%=rec%>"  value="<%=rs(0)%>">
			<%
			end if 'media already linked'
			%>
		</td>
		<td bgcolor="<%=BgColor%>"><%=TrimTitle(DisplayText(rs(5)),80)%></td>
		<td valign="top" BgColor="<%=BgColor%>">
			<% response.write rs(1) & rs(2) %>
		</td>
		<td valign="top" BgColor="<%=BgColor%>">
			<%=rs(4)%> KO
		</td>
		<td valign="top" BgColor="<%=BgColor%>" width="10" align="center">
		<%
		if rs(7) = 1 then
			response.Write "<img src=""/dbadmin/images/tick.gif"" width=""10"" alt=""online"">"
		else
			response.Write "<img src=""/dbadmin/images/cross.gif"" height=""10"" alt=""offline"">"
		end if
		%>
			</td>
	</tr>
	<%
	end if' do not link an iamge to itself'
	rs.movenext
	rec = rec + 1
loop
%>
</table>
    <input type="hidden" name="season" value="<%=season%>">
    <input type="hidden" name="mypage" value="<%=mypage%>">
    <input type="hidden" name="MediaType" value="<%=MediaType%>">
    <input type="hidden" name="ttlRec" value="<%=rec%>">
    <input type="hidden" name="SupportID" value="<%=SupportID%>">
    <input type="submit" value="Link Selected Media" tabindex="6" label="" class="subscrsubmit"></td></td>
</form>
<!-- #include virtual="/dbadmin/ssi/incl/base_pop.asp" -->
</body>
</html>
<%
set rsMedia = nothing
set MediaAsso = nothing

sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a  class=""bluetext"" href=""associated_media.asp?mypage=" & counter & "&amp;DocID=" & DocID
				If MediaType <> "" Then
						response.write "&amp;MediaType=" & MediaType
				End if
				If Season <> "" Then
						response.write "&amp;Season=" & Season
				End if
				Response.Write """>" & counter & "</a>"
			else
				Response.Write "<i  class=""bluetext"">" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
	end if
end sub
%>
<!--#include virtual="/ssi/dbclose.asp"-->
<script language="JavaScript" type="text/javascript">
//########################################
//## script copyright fusio.net         ##
//## developped for Dub Theare Festival ##
//## by remi [at] fusio<.>net           ##
//########################################
function validate_form(thisform)
{
	var nbchkbox = 0;
	for (Count = 0; Count < document.associatedmedia.elements.length; Count++)
	{
		if (associatedmedia[Count].checked) nbchkbox++;
	}
	if (nbchkbox == 0)
	{
	alert("No media have been selected");
	return false;
	}
}
</script>