<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<title>ICO - Website Administration  - Media</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/scripts/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<%
section = "media"

cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 40

mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if

MediaType = trim(request.querystring("MediaType"))



If isNull(MediaType) or MediaType="" then
	MediaType = 1
end if


FirstLetter = left(trim(request.querystring("FirstLetter")),1)
%>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>View Media</h1>

<p>
		<%
				set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
					Do until rs.eof
						If rs(0) = cint(MediaType) then
							astyle= "bodytext"
							MediaCurrentlyDisplay = rs(1)
						else
							astyle= "bluetext"
						end if

					response.Write "<a href=""view.asp?MediaType=" & trim(rs(0)) & "&amp;media="& trim(rs(1))&"&amp;season="& season &""" class="""& astyle &""">" & rs(1) & "</a>" & VbNewline & _
								" &nbsp;|&nbsp; " & vbnewline
					rs.Movenext
					Loop
				%>
</p>

<h3><%=MediaCurrentlyDisplay%> A to Z</h3>
<p>
		<%
		set rs = sqlconnection.execute("exec usp_SEL_AZ_Media " & MediaType)
		Do until rs.eof
			response.Write "<a href=""view.asp?FirstLetter=" & Ucase(rs(0)) & "&amp;MediaType="& MediaType &""" "
			if lcase(FirstLetter) = lcase(rs(0)) then
				response.Write ""
			else
				response.Write "class=""bluetext"""
			end if
			response.Write">" & Ucase(rs(0)) & "</a> &nbsp;|&nbsp; "
		rs.Movenext
		Loop
		%>
</p>

<%
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open "usp_SEL_MediaPerType " & MediaType & ",0,'" & FirstLetter & "'" , sqlconnection

	'SELECT m.ID, m.Path,m.FileName,m.Preview,m.FileSize
	',m.Caption,m.MediaType,m.Online ,
	'case when m.ID in (select p.MediaID from ComsCentre_PRMedia p where p.prID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinPR,
	'case when m.ID in (select e.MediaID from EventMedia e where e.eventID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinEvent,
	'case when m.ID in (select am.MediaID from ArtistsMedia am where am.ArtistID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinArtist,
	'(select count(AssociatedMediaID) from MediaAssociated where MediaID=@SupportID)  as AssociatedMedia'



If rs.eof then
	response.Write "<br /><p class=""bluetext"">There are no '"& Lcase(MediaTypeText(MediaType)) &"' files stored in the database. </p>" & VbNewline
	response.end
end if

rec = 1
rs.movefirst
rs.pagesize = maxdisplay
rs.absolutepage = mypage
maxpages = cint(rs.pagecount)
' display the total of records returned by sql query'
intRecordCnt = rs.RecordCount

if maxpages > 1 then
	response.Write "<table width=""100%""><tr><td class=""bluetext"" align=""right"">"
	call Paging
	response.Write "</td></tr></table>" & VbNewline & _
	"<hr class=""hrgrey"">" & VbNewline
end if

%>
<table width="90%" cellpadding="0" cellspacing="0" border="0" id="mediatable">
	<tr>
		<th>Type</td>
		<th>Title</th>
		<th>Path</th>
		<th>Size</th>
		<th width="20"><img src="/dbadmin/images/icon_online.gif" width="20" height="20" alt="Online/Offline" /></th>
		<%
			if MediaType = 1 then
				response.Write "<th width=""200"">Preview</th>" & vbnewline
			end if
		%>
		<th width="20">Link</th>
		<th width="20">Edit</th>
		<th width="30">Delete</th>
	</tr>
<%
	Do until rs.eof  or (rec > maxdisplay)
	If rec mod 2 = 0 then
		BgColor = "#deeaea"
	else
		BgColor = "#ecf2f2"
	end if
	%>
		<tr>
			<td valign="top" bgcolor="<%=BgColor%>"><% response.write MediaTypeGraph(MediaType)%></td>
			<td valign="top" bgcolor="<%=BgColor%>"><%=TrimTitle(DisplayText(rs(5)),80)%></td>
			<td valign="top" bgcolor="<%=BgColor%>"><% response.write rs(1) & rs(2) %></td>
			<td valign="top" bgcolor="<%=BgColor%>"><%=rs("FileSize")%> KB</td>
			<td valign="top" bgcolor="<%=BgColor%>" align="center">
				<%
				response.Write "<a href=""on_off.asp?MediaID=" & rs(0) & "&amp;MediaType="& MediaType &"&amp;mypage="& mypage &""" title=""on/off line"">"
					if rs("online") = 1 then
						response.Write "<img src=""/dbadmin/images/tick.gif"" width=""10"" alt=""online - Click here to put this media offline"" border=""0"" />"
					else
						response.Write "<img src=""/dbadmin/images/cross.gif"" height=""10"" alt=""offline - Click here to put this media online"" border=""0"" />"
					end if
				response.Write"</a>"
				%>
			</td>
			<%
			if MediaType = 1 then
			%>
			<td valign="top" bgcolor="<%=BgColor%>">
			<%
			if Len(rs(3)) > 4 then
			%>
			<a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="click to view image">Preview (on/off)</a>
				<span id="field<%=rec%>" style="display:none;margin:0">
					<br/>
					<%
					if len( rs(2)) > 3 then
					%>
					<a href="<%response.write rs(1) &"jpg/"& rs(2)%>" target="media">
					<img src="<%response.write rs(1) & rs(3)%>" alt="Click For Full Size" style="margin:0" border="0"></a>
					<%
					else
					%>
					<img src="<%response.write rs(1) & rs(3)%>" alt="Click For Full Size" style="margin:0" border="0">
					<%
					end if
					%>
				</span>
			<%end if ' rs(3)) > 4%>
			</td>
			<%end if ' MediaType = 1 = preview%>
			<td valign="top" bgcolor="<%=BgColor%>" align="center">
			<%
			response.write"<a href=""JavaScript:openWindowNamed('/dbadmin/media/associated_media.asp?SupportID="& rs(0) &"&amp;Season="& Season &"',700,550,'media')"">"
				'Other media associated'
			if rs("AssociatedMedia") > 0 or rs("AlreadyAssociated") > 0 then
				response.Write "<img src=""/dbadmin/images/ok.gif"" alt="""& AssociatedMedia&" attached media"" width=""16"" height=""12"" border=""0"" />"
			else
				response.Write "<img src=""/dbadmin/images/plus.gif"" alt=""Link Media"" width=""16"" height=""16"" border=""0"" />"
			end if
			response.write"</a>"

			%>
			</td>
			<td valign="top" align="center" bgcolor="<%=BgColor%>" width="20"><a href="edit.asp?MediaID=<%=rs(0)%>"><img src="/dbadmin/images/edit.gif" alt="Edit Media details" border="0" width="15" height="14" /></a></td>
			<td valign="top" align="center" bgcolor="<%=BgColor%>"><a href="delete.asp?MediaID=<%=rs(0)%>&amp;MediaType=<%=MediaType%>" onClick="return confirm('Are you sure you want to PERMANENTLY delete this media \n* Once performed, this action cannot be reversed!!');"><img src="/dbadmin/images/delete.gif" alt="Delete this media"  border="0" width="13" height="16" /></a></td>
		</tr>
	<%
	rs.movenext
	rec = rec + 1
loop
%>
</table>

	<%
if maxpages > 1 then
	response.Write "<table width=""100%""><tr><td class=""bluetext"" align=""right"">"
	call Paging
	response.Write "</td></tr></table>" & VbNewline & _
	"<hr class=""hrgrey"">" & VbNewline
end if

rs.close
set rs=nothing


sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a  class=""bluetext"" href=""view.asp?mypage=" & counter & "&amp;DocID=" & DocID
				If MediaType <> "" Then
						response.write "&amp;MediaType=" & MediaType
				End if
				If Season <> "" Then
						response.write "&amp;Season=" & Season
				End if
				Response.Write """>" & counter & "</a>"
			else
				Response.Write "<i  class=""bluetext"">" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
	end if
end sub
%>
<script language="JavaScript" type="text/javascript">
//########################################
//## script copyright fusio.net         ##
//## by remi [at] fusio<.>net           ##
//########################################
function ShowMe(ThisLayer)
{
	// expand or contract
	document.getElementById(ThisLayer).style.display = (document.getElementById(ThisLayer).style.display == 'block') ? 'none' : 'block';
	cvalue = (document.getElementById(ThisLayer).style.display == 'block') ? 1 : 2;
}
</script>

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
