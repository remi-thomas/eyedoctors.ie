<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><

<%
StatusRequired = 9
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->

<html>
<head>
<title>News - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script src="/ssi/scripts/members_search_lib.js" type="text/javascript"></script>
</head>
<body class="body">

<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
MemberFirstName = left(trim(Request.querystring("MemberFirstName")),40)
MemberName	= left(trim(Request.querystring("MemberName")),50)
restr		= request.querystring("restr")

%>
<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
<h3>A to Z: 
<%
Set rs = SqlConnection.Execute("usp_sel_admin_AZ_Members")
fl = rs(0) 'give a value to firstletter 
Do Until rs.eof
	response.write "<a href=""list.asp?lt=" & rs(0) & """>" & rs(0) & "</a> | "
rs.movenext
Loop
%>
</h3>
</div>
<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
	<form name="searchmember" action="search.asp?" method="get" onsubmit="return validate_form(this);" style="margin:0;padding:0">
	<input type="text" name="MemberFirstName" size="23" value="
	<% If MemberFirstName <>  "" Then response.write MemberFirstName else response.write ""%>" onfocus="rstfirstname()" onblur="stfirstname()" placeholder="First Name "/>

	<input type="text" name="MemberName" size="26" value="<%=MemberName%>" onfocus="rstname()" onblur="stname()" placeholder="Last Name" />
	<input type="hidden" name="searchusers" value="true"/>
	<input type="hidden" name="restr" value="<%=restr%>"/>
	<input type="submit" value="Search" />
	</form>
</div>



<%

MoreName	= request.querystring("MoreName")
if not IsNumeric(restr) or restr = "" then
	restr = 0
end if
if not IsNumeric(MoreName) or MoreName = "" then
	MoreName = 0
end if
if len(MemberName) > 2 then
	MemberID = 0
else
	if len(MemberFirstName) > 2 then
		MemberName = MemberFirstName
		MemberFirstName = ""
	else
		MemberName = ""
	end if
end if
%>

<%
If request.querystring("searchusers") = "true" then
	if restr = 1 then
		sql = " exec usp_SEL_SearchByName '"& Replace(MemberName,"*","_") &"','"& Replace(MemberFirstName,"*","_") & "',"& MoreName 
	else
		restr = 0
		sql = " exec usp_SEL_SearchByName '"& Replace(MemberName,"*","_") &"','"& Replace(MemberFirstName,"*","_") & "',"& MoreName 
	end if

	'response.Write "<br>line 139 " & sql & "<hr>"
	set rs=sqlconnection.execute(sql)
	if not rs.eof then
		'this is a name search '
		'response.Write "<br/>## test only##" & VbNewline & _
		'"<br/>match = " & rs(26) & VbNewline & _
		'"<br/> near match = " & rs(27) & VbNewline & _
		'"<br/> straight search= " & rs(28)

		ExactMatch	= rs("ExactMatch")
		NearMatch	= rs("NearMatch")
		StraightSearch = rs("StraightSearch")

		'sort out the variables'
		Tailvar = "&amp;disp=src&amp;MemberName=" & MemberName & "&amp;MemberFirstName=" &  MemberFirstName & "&amp;restr=" & restr & "&amp;housetype=" & housetype

		if ExactMatch > 0 and MoreName = 0 then
			if instr(MemberName,"*") > 1 then
				Message = "<h2>Results</h2> <p>Your search returns <strong>" & ExactMatch &" member"
				if ExactMatch > 1 then Message = Message &"s"
				Message = Message &"</strong> based on the following string '" & MemberName & "' <br/><i> where the * symbol can be any letter</i>.</p>"
			else
				Message = "<h2>Results</h2> <p>Your search returned <strong>" & ExactMatch & " member"
				if ExactMatch > 1 then Message = Message &"s"
				Message = Message & "</strong> matching your search criteria "
					if NearMatch = 1 then
						Message = Message & "and <a href=""default.asp?MoreName=1" & Tailvar & """>" & NearMatch & " other name</a> that has a similar spelling or sounds like it."
					elseif NearMatch > 1 then
						Message = Message & "and <a href=""default.asp?MoreName=1" & Tailvar & """>" & NearMatch & " other names</a> that have a similar spelling or sound like it.</p>"
					end if
			end if
		elseif StraightSearch = 0 and NearMatch > 0 and MoreName = 0 then
		' are we looking for a * ?'
			Message = "Your search returns no member matching your search criteria <br/>We found some names which have a similar spelling or sound like it"
		elseif MoreName = 1 then
			Message = "This are the results of your search showing the members for which the name sound similar than the one you query for: '"& MemberName &"' <br/>" & VbNewline & _
			"<a href=default.asp?MoreName=0" & Tailvar & """>Restrict your search to '"& MemberName &"'</a>"& VbNewline
		end if

		'##########################'
		'### start the table    ###'
		'### broaden / restrict ###'
		'##########################'
		if NearMatch > 0 and StraightSearch>0 then
			if InStr(MemberName, "*")= 0 then
				response.write "<br /><p>" & message & "</p>"& VbNewline 
				response.write"</table>" & VbNewline
			else
				response.Write "<p class=""membertext"">" & message & "</p>" & VbNewline
			end if
		else
			response.Write "<p class=""membertext"">" & message & "</p>" & VbNewline
		end If
		
		response.write "<ul>" & vbnewline
		Do until rs.eof
			ThisMemberID = rs("MemberID")	
			membername = rs("Title") & " " & rs("FirstName") & " " & rs("LastName")
				
			response.write "<li><a href=""edituser.asp?memberid=" & rs("MemberID") & """>" & rs("Title") & "&nbsp;" & rs("FirstName") & "&nbsp;" & rs("LastName") &"</a></li>"				

		rs.Movenext
		Loop
		response.write "</ul>" & vbnewline

	else 'eof line 232'
			response.Write "<br />" & vbnewline
			response.Write "<ul><li style=""color:#c00"">Your search for '<em>"
				if len(MemberFirstName) > 2 then
					response.Write MemberFirstName & " "
				end if
				response.Write  MemberName &"'</em> returned no result</li>" & VbNewline
				if InStr (SearchMemberName, "*") = 0 then
					response.Write "<li>If you are unsure of the spelling you can use the wildcard symbol (*) to replace any letter in the name you're looking for.</li>" & VbNewline
				end if
			response.Write "</ul>" & vbnewline
	end if
End If 'request.querystring(searchusers) = true
%>
   
</div>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->
