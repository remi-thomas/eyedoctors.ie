<%
StatusRequired = 9
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/ssi/fct/encryptor.asp" -->
<%
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "filename=Irish_College_Ophthalmology_MEMBERS_"& FileName & ".xls"


Set rs = SqlConnection.Execute("usp_sel_AllMembers")


If Not rs.eof then
%>
<table>
	<tr>
		<th>Email</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Full Name</th>
		<th>Profile URL</th>
	</tr>
	<%
	Do While Not rs.eof
		if len(rs("MemberEmail")) then
	%>
	<tr>
		<td><%=rs("MemberEmail")%></td>
		<td><%=rs("FirstName")%></td>
		<td><%=rs("LastName")%></td>
		<td><%=rs("Title") & " " & rs("FirstName") & " " &  rs("LastName")%></td>
		<td>
		<%
			'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
			' create encrypted URL                                  '
			'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
			dim mdata :  mdata = replace(rs("MemberEmail"),"@","}") & "|" & (rs("MemberID")/100)
			dim mURL : mURL = Encode(mdata)
			mURL = "http://eyedoctors.ie/members/register/default.asp?a=" & mURL
			response.write mURL
		%>
		</td>
	</tr>
	<%
		end if
	rs.movenext
	Loop
	%>

</table>
<%
End If
%>

<!--#include virtual="/ssi/dbclose.asp" -->
