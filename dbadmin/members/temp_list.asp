<%
StatusRequired = 9
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script src="/ssi/scripts/members_search_lib.js" type="text/javascript"></script>

</head>

<body>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->


<%

Set rs = SqlConnection.Execute("usp_sel_Temp_MembersStartingByLetter")

If Not rs.eof then
%>

<h3>Temporarily registered members </h3>
<div id="details">
	<ul>
	<%
	Do While Not rs.eof
		MemberID = rs("MemberID")
	%>
	<li><%=rs("Title")%>&nbsp;<%=rs("FirstName")%>&nbsp;<%=rs("LastName")%> | 
	<%	
	if IsNumeric(MemberID) and MemberID <> "" then
		if MemberID > 0 then
			response.write " <a href=""temp_user.asp?tempid="  & rs("TempID") & """>Review changes </a> |  <a href=""/opthalmologists/eye-doctor.asp?MemberID="& MemberID &""" target=""member"">View Current record</a>) "
		else
			response.write "<a href=""temp_user.asp?tempid=" & rs("TempID") & """ style=""color:red"">Review & Register New Member</a>"

		end if
	end if

	%>
	| <a href="delete_temp_user.asp?TempID=<%=rs("TempID")%>" onclick="return confirm('Are you sure you wish to delete this record?');"><img src="/dbadmin/images/delete.gif" width="13" height="16" border="0" alt="delete" /> </a>
	| Updated <%=FormatDateTimeIRL(rs("RecordDate"),1)%> &nbsp;<%=FormatDateTimeIRL(rs("RecordDate"),4)%> 
	</li>
	<%
	rs.movenext
	Loop
	%>
	</ul>
</div>
<%
End If
%>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<p>
  <!--#include virtual="/ssi/dbclose.asp" -->
</p>
</body>
</html>
