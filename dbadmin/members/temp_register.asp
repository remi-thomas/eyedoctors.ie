<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<%
'This file updates the details for a user'
TempID						= ValidateText(request.form("TempID"))
FirstName 					= ValidateText(request.form("FirstName"))
LastName 					= ValidateText(request.form("LastName"))
Title 						= ValidateText(request.form("Title"))
Diploma						= ValidateText(request.form("Diploma"))
Photo 						= ValidateText(request.form("Photo"))
Biog 						= ValidateText(request.form("Biog"))
Website 					= ValidateText(request.form("Website"))
PrivateEmail 				= ValidateText(request.form("PrivateEmail"))
PrivatePhoneNumber  		= ValidateText(request.form("PrivatePhoneNumber")) 
Membership_of_pro_bodies	= ValidateText(request.form("Membership_of_pro_bodies")) 
Professional_achievements	= ValidateText(request.form("Professional_achievements")) 
Research_interests			= ValidateText(request.form("Research_interests")) 
Recent_Publications			= ValidateText(request.form("Recent_Publications")) 

MemberID 					= ValidateText(request.form("MemberID")) 


'response.write "MemberID " & MemberID
'response.end


if isNUmeric(MemberID) and MemberID <> "" then 
	sql = "UPDATE Members_details SET FirstName= '"& FirstName &"' ,LastName = '"& LastName &"' ,Title = '"& Title &"', Diploma = '"& Diploma &"',  Biog = '"& Biog &"',Membership_of_pro_bodies = '"& Membership_of_pro_bodies &"', Professional_achievements = '"& Professional_achievements &"', Research_interests ='"& Research_interests &"', Recent_Publications = '"& Recent_Publications &"', Website = '"& Website &"', PrivateEmail ='"& PrivateEmail &"', PrivatePhoneNumber = '"& PrivatePhoneNumber&"', RecordUpdateDate=getdate() WHERE MemberID =" & MemberID
		response.write sql
		sqlconnection.execute(sql)
else
	sql = "INSERT INTO Members_details ( FirstName, LastName, Title, Diploma,  Biog, Website, PrivateEmail, PrivatePhoneNumber,Membership_of_pro_bodies,Professional_achievements,Research_interests,Recent_Publications) VALUES ('"& FirstName &"' , '"& LastName &"' , '"& Title &"' , '"& Diploma &"' , '"& Biog &"' , '"& Website &"' , '"& PrivateEmail &"', '"& PrivatePhoneNumber &"', '"& Membership_of_pro_bodies &"', '"& Professional_achievements &"', '"& Research_interests &"', '"& Recent_Publications &"')"
	'response.write sql
	sqlconnection.execute(sql)

	Set sqlNewID = SqlConnection.execute("select @@identity as NewRec")
	MemberID = sqlNewID(0)
	Set sqlNewID = nothing

end if

'### deal with specialty ###'
SpecialtiesSelection = request.form("SpecialtiesSelection")
'esponse.write "<br/>SpecialtiesSelection " & SpecialtiesSelection
'response.end
sqlconnection.execute("delete from Members_to_specialties where MemberID="& MemberID )

If Len(SpecialtiesSelection) > 1 then
	dim SpecialtiesArr
	SpecialtiesArr = Split(SpecialtiesSelection,",")

	For i = 0 to UBound(SpecialtiesArr)
		If SpecialtiesArr(i) <> "" and IsNumeric(SpecialtiesArr(i)) then
			sql="INSERT INTO Members_to_specialties (MemberID, SpecialtyID) VALUES ("& MemberID  &","& SpecialtiesArr(i) &")"
			'response.write "<hr>" & sql
			sqlconnection.execute(sql)
		end if
	Next

End If

'practice
sqlconnection.execute("delete from Members_practice where MemberID=" & MemberID)

For PracticeCounter = 0 to 4

	PracticeID			= request.Form("PracticeID" & PracticeCounter)
	PracticeAddress		= Left(ValidateText(request.Form("Address" & PracticeCounter)),3999)
	PracticeTelephone	= Left(ValidateText(request.Form("Telephone" & PracticeCounter)),50)
	PracticeFax			= Left(ValidateText(request.Form("Fax" & PracticeCounter)),50)
	PracticeEmail		= Left(ValidateText(request.Form("PracticeEmail" & PracticeCounter)),200)
	PracticeWeb			= Left(ValidateText(request.Form("PracticeWeb" & PracticeCounter)),800)
	Opening				= Left(ValidateText(request.Form("Opening" & PracticeCounter)),1000)
	GeoLat				= Left(ValidateText(request.Form("GeoLat" & PracticeCounter)),10)
	GeoLong				= Left(ValidateText(request.Form("GeoLong" & PracticeCounter)),10)
	PracticeNotes		= ValidateText(request.Form("Notes" & PracticeCounter))
	PrivatePublic		= request.Form("PrivatePublic" & PracticeCounter)
	Online				= request.Form("Online" & PracticeCounter)
	CountyID			= request.Form("CountyID" & PracticeCounter)


	response.write "<br/> GeoLat # " & PracticeCounter  & " >> " & GeoLat
	response.write "<br/> Online # " & PracticeCounter  & " >> " & Online


	If Not IsNumeric(PrivatePublic) Or PrivatePublic = "" Then
		PrivatePublic = 0
	End if
	If PrivatePublic > 2 Then
		PrivatePublic = 0
	End If
	'online?
	If Not IsNumeric(Online) Or Online = "" Then
		Online = 0
	End if
	If Online <> 1 Then
		Online = 0
	End If

	response.write "<br/> Online # " & PracticeCounter  & " >> " & Online

	If Not IsNumeric(CountyID) Or CountyID = "" Then
		CountyID = 0
	End if
	if Len(PracticeAddress) > 5 then
		sql = "INSERT INTO Members_practice (MemberID,PracticeAddress,PracticeCountyID,PracticeTelephone,PracticeFax,PracticeEmail,PracticeWeb ,Opening,GeoLat,GeoLong,PracticeNotes,PrivatePublic,Online,RecordDate) VALUES ("&MemberID&",'" &PracticeAddress & "',"&CountyID&",'" & PracticeTelephone & "','" & PracticeFax & "','" & PracticeEmail & "','" & PracticeWeb & "','" & Opening & "','" & GeoLat & "','" & GeoLong & "','"  & PracticeNotes & "',"  & PrivatePublic & "," & Online &",getdate() )"
		response.write "<hr>" & sql
		
		sqlconnection.execute(sql)
	End if

Next

'response.end


sqlconnection.execute("delete from auto_Members_details where TempID=" & TempID & ";delete from auto_Members_practice where TempID=" & TempID & ";delete from auto_Members_to_specialties where TempID=" & TempID )

%>
<!--#include virtual="/ssi/dbclose.asp" -->
<%
Response.Redirect "/dbadmin/members/edituser.asp?memberid=" & MemberID
%>

