<%
StatusRequired = 9
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script src="/ssi/scripts/members_search_lib.js" type="text/javascript"></script>

</head>

<body>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
<h3>A to Z: 
<%
Set rs = SqlConnection.Execute("usp_sel_AZ_Members")
fl = rs(0) 'give a value to firstletter 
Do Until rs.eof
	response.write "<a href=""list.asp?lt=" & rs(0) & """>" & rs(0) & "</a> | "
rs.movenext
Loop
%>
</h3>
</div>
<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
	<form name="searchmember" action="search.asp?" method="get" onsubmit="return validate_form(this);" style="margin:0;padding:0">
	<input type="text" name="MemberFirstName" size="23" value="" onfocus="rstfirstname()" onblur="stfirstname()" />

	<input type="text" name="MemberName" size="26" value="" onfocus="rstname()" onblur="stname()" />
	<input type="hidden" name="searchusers" value="true"/>
	<input type="hidden" name="restr" value=""/>
	<input type="submit" value="Search" />
	</form>
</div>


<%
lt = request.querystring("lt")
If lt = "" Then
	lt = fl ' lt is null, give the value first letter
End If
Set rs = SqlConnection.Execute("usp_sel_admin_MembersStartingByLetter '"&lt&"'")

If Not rs.eof then
%>

<h3>Member starting by <%=lt%> </h3>
<div id="details">
	<ul>
	<%
	Do While Not rs.eof
	%>
	<li>
		<%
		'online / offline ?? '
		if rs("online")=1 Then
			OnlineImg="tick.gif"
			AltTxt="This member is online"
		else
			OnlineImg="cross.gif"
			AltTxt="This member is offline"
		end if	
		response.Write "<a href=""switchstatus.asp?MemberID=" & rs("MemberID") &"&lt="&lt&"""><img src=""/dbadmin/images/" & OnlineImg & """ title=""" & AltTxt &""" width=""10"" height=""10"" border=""0"" ></a>"
		%>


	<a href="edituser.asp?memberid=<%=rs("MemberID")%>"><%=rs("Title")%>&nbsp;<%=rs("FirstName")%>&nbsp;<%=rs("LastName")%></a>
	<a href="delete_user.asp?MemberID=<%=rs("MemberID")%>" onclick="return confirm('Are you sure you wish to delete this record?');"><img src="/dbadmin/images/delete.gif" width="13" height="16" border="0" alt="delete" /> </a></li>
	<%
	rs.movenext
	Loop
	%>
	</ul>
</div>
<%
End If
%>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<p>
  <!--#include virtual="/ssi/dbclose.asp" -->
</p>
</body>
</html>
