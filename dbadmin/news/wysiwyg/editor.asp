<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>News - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script type="text/javascript" src="/dbadmin/ssi/ckeditor 4/ckeditor.js"></script>
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
<script language="JavaScript" TYPE="text/javascript">
<!--
function f_validate(f) {
	var validForm = true;
	var thisE;

	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			thisE = eval("f.v_"+ f.elements(i).name);

			if (f.elements(i).value == "") {
				thisE.value = f.elements(i).label;
				f.elements(i).select();
				validForm = false;
			}
			else {
				thisE.value = "";
			}
		}
	}

	if (!validForm) {
		alert ('Please complete all required fields');
		return false;
	}
	else {
		return true;
	}
}

function f_checker(f, myElement) {
	var thisE;
	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			if (f.elements(i).name != myElement) {
				thisE = eval("f.v_"+ f.elements(i).name);

				if (f.elements(i).value == "") {
					thisE.value = f.elements(i).label;
					return false;
				}
				else {
					thisE.value = "";
				}
			}
			else {
				return false;
			}
		}
	}
}
//-->
</script>
</head>
<%
	section = "News"
%>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp"-->
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<br />
<%
Session.LCID=6153
Server.ScriptTimeout  = 6000
NewsID	= Request.Querystring("ID")

if not IsNull(NewsID) and NewsID <>"" then
	'get Newsess  from the db'
	set rs = sqlconnection.execute("exec usp_SEL_LatestNews "& NewsID &",1" )

	if not rs.eof then
		author			= DisplayText(rs("Author"))
		subject			= DisplayText(rs("Subject"))
		content			= rs("Content")
		PublicationDate = FormatDateTimeIRL(rs("PublicationDate"),1)
		EmbargoDate		= FormatDateTimeIRL(rs("EmbargoDate"),1)
		EmbargoTime		= FormatDateTimeIRL(rs("EmbargoDate"),4)
		Online			= cint(rs("Online"))
'
		if len(mypath) > 5 then
			if ThisFileExists(mypath) then
				content = ReadPage(mypath)
			else
				response.Write "<p class=""redtext"">This file stored at &laquo;" & mypath & "&raquo; can not be found!</p>"
			end if
		end if

	content			= Displaytext(Content)


	end if
else
		section		= ""
		author		= session("AdminUserName")
		subject		= ""
		content		= ""
		d_added		= ""
		PublicationDate = FormatDateTimeIRL(now,1)
		EmbargoDate = FormatDateTimeIRL(now,1)
		EmbargoTime = "12:00"
		Online		= 0
end if
%>


<table cellpadding="1" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="dbText">
		Please complete the form below to add or edit this News for the ICOwebsite
		</td>
	</tr>
	<tr>
		<td><hr class="hrgrey" ID="hr1"></td>
	</tr>
</table>

<form action="get.asp" name="editor" method="post">
	<table cellpadding="3" cellspacing="0" border="0">
		<tr>
			<td class="bluetext">Author</td>
			<td class="dbText">:</td>
			<td class="dbText"><input type="text" name="author" value="<%=author%>" class="TextInput" size="50" label="" tabindex="2"></td>
			<td class="dbText"><input type="text" name="v_author" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="12"></td>
		</tr>
		<tr>
			<td class="bluetext">Subject</td>
			<td class="dbText">:</td>
			<td class="dbText"><input type="text" name="subject" value="<%=subject%>" class="TextInput" size="115" label="required field" tabindex="3" onfocus="f_checker(this.form, this.name);"></td>
			<td class="dbText"><input type="text" name="v_subject" value="<%=v_subject%>" class="dbHidden" size="15" label="" tabindex="11"></td>
		</tr>

		<tr>
			<td class="bluetext">Publication Date</td>
			<td class="dbText">:</td>
			<td class="dbText">
			<%
							response.Write "<select name=""PubDay"" class=""TextInput"">" & vbnewline

							For i=1 to 31
                  						Response.Write "<option"
                  						if Cint(Day(PublicationDate))=i Then Response.Write " selected"
                  						Response.Write ">" & i & "</option>"
							Next
							response.Write "</select>" & VbNewline & _
							"<select name=""PubMonth"" class=""TextInput"">" & vbnewline

							For i=1 to 12
								Response.Write "<option"
								if Cint(Month(PublicationDate))=i Then Response.Write " selected"
								Response.Write ">" & MonthName(i) & "</option>"
							Next
							response.Write "</select>" & VbNewline & _
							"<select name=""PubYear"" class=""TextInput"">" & vbnewline

							For i=1990 to Cint(Year(now()))
								Response.Write "<option"
								if Cint(Year(PublicationDate))=i Then Response.Write " selected"
								Response.Write ">" & i & "</option>"
							Next
							response.Write "</select>" & VbNewline
							%>


			</td>
			<td class="dbText"></td>
		</tr>
			<td class="bluetext">Embargo Date</td>
			<td class="dbText">:</td>
			<td class="dbText">
			<%
			response.Write "<select name=""EmbDay"" class=""TextInput"">" & vbnewline
			For i=1 to 31
				Response.Write "<option"
					if Cint(Day(EmbargoDate))=i Then Response.Write " selected"
                Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbMonth"" class=""TextInput"">" & vbnewline
			For i=1 to 12
				Response.Write "<option"
					if Cint(Month(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & MonthName(i) & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbYear"" class=""TextInput"">" & vbnewline
			For i=1990 to Cint(Year(now()))
				Response.Write "<option"
					if Cint(Year(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline
			'time'
			'@@ get the time Newseviosuly selected, if not set to Noon'
			response.Write VbNewline & _
			"<select name=""EmbTime"" class=""TextInput"">" & vbnewline
			For i=0 to 1440 step 30
				TimeOption = FormatDateTimeIRL(DateAdd ("n", i, "1 january 2000 00:00"),4)
				Response.Write "<option"
					if trim(EmbargoTime) = trim(TimeOption) Then
						Response.Write " selected"
					end if
				Response.Write ">" & TimeOption & "</option>"
			Next
				response.Write "</select>" & VbNewline
			%>
			</td>
			<td class="dbText"></td>
		</tr>
	</table>
	<hr class="hrgrey" ID="hr2">
	<span class="bluetext">News<br/></span>

	<div align="center">
        <textarea cols="65" rows="12" name="Content" id="Content" tabindex="6" class="TextInput" style="border:0px;"><%=Content%></textarea>

	</div>


<table cellpadding="1" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="bluetext">Online:</td>
			<td class="dbText"><input type="radio" value="1" name="online" <%if Online = 1 then response.Write "checked"%> class="TextInput">Yes &nbsp;|&nbsp;<input type="radio" value="0" name="online" <%if Online = 0 then response.Write "checked"%> class="TextInput">No</td>
			<td>&nbsp;</td>
		</tr>
	<tr>
		<td width="40%" colspan="2">&nbsp;</td>
		<td class="dbText">
			<input type="hidden" name="NewsID" value="<%=NewsID%>">
			<input type="hidden" name="mypath" value="<%=mypath%>">
			<input type="submit" value="Update News&raquo;&raquo;" class="TextInput" tabindex="6" label="">
		</td>
	</tr>
</table>
</form>
<%
if not IsNull(NewsID) and NewsID <>"" then
	'display attach media only when NewsID is known'
%>

<table width="100%">
	<tr>
		<td colspan="2"><hr class="hrgrey" ID="hr3"></td>
	</tr>
</table>
<table cellpadding="1" cellspacing="0" border="0">
	<tr>
		<td class="bluetext">Attached Media :</td>
		<td class="dbText" colspan="2" align="left">
		<a href="JavaScript:openwin('/dbadmin/ssi/AttachMedia/pop_media.asp?section=News&SupportID=<%=NewsID%>','browse','scrollbars=yes,resizable=yes,left=10,top=10,screenX=10,screenY=10,width=700,height=560,innerHeight=0,innerWidth=0,outerHeight=0,outerWidth=0')" class="dbText">Add Media</a>
		</td>
	</tr>
		<%call DrawAttachedMediaDB(NewsID,"News")%>
</table>
<hr class="hrgrey" ID="hr4" width="100%">
<% end if %>
</p>
<!--#include virtual="/ssi/dbclose.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->


<script type="text/javascript">
	window.onload = function()
	{
		CKEDITOR.replace( 'Content'
		,
		{
		height:"200"
		}

		);
	};
</script>

</body>
</html>
