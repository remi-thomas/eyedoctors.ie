<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->

<!--#include virtual="/ssi/dbconnect.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


<%
' include at the top '
NewsID = left(request.querystring("NewsID"),6)
if Not IsNumeric(NewsID) or NewsID ="" then
	response.redirect "/dbadmin/news/default.asp?ThisERR=can not process email-news id not numeric"
end If

set News = sqlconnection.execute("exec usp_SEL_News "& NewsID &",0")

if NOT News.eof THEN
	If len(News(2)) > 5 then
		header =   " - " & TrimTitle(DisplayText(News(2)),50)
	end if
	TitlePage = DisplayText(News(1)) & header & " - " & FormatDateTimeIRL(News(7),1)
else
	response.redirect "/dbadmin/news/default.asp?ThisERR=can not process email- eof for news id " & NewsID
end if
%>
<title><%=TitlePage%></title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<body class="body">

<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->



			<%
			if NOT News.eof Then

				HasEditingPriv = false
				If Session("AdminStatus") > 8 Then
					HasEditingPriv = true
				End if
				If Session("AdminID")= News("RecordCreatedBy") Then
					HasEditingPriv = True
				End if


				response.Write "<table cellspacing=""0"" width=""98%"" border=""0"">" & VbNewline & _
				"	<tr>" & VbNewline & _
					"		<td valign=""top"" >"

					if News(9) > 0 then
						response.Write "<table align=""right"" valign=""top"" style=""margin-left:5px""><tr><td>"
						call GetMediaAnySupport("news",News(0),0,0,1)
						response.Write "</td></tr></table>"
					end if

					response.Write "<h2>"

					If HasEditingPriv then
						response.Write	"<br/><a href=""/dbadmin/news/edit.asp?ID=" & News(0) & """><img src=""/dbadmin/images/edit.gif"" width=""15"" height=""14"" border=""0"" align=""left""/></a>&nbsp; "
					End If


					response.Write trim(DisplayText(News(1))) & "</h2> <p>(" & FormatDateTimeIRL(News(7),1)& ")</p>"

					If len(News(4)) > 2 then
						response.Write "<p>By: " & DisplayText(News(4)) & "</p>"
					end if

					if len(News(3)) < 5 then
						response.write "<p id=""newstext"">" & DisplayText(News(2)) & "</p>" & vbnewline
					else
						response.write "<p>" & DisplayText(News(3))& "</p>" & vbnewline
					end if

					response.Write "</td>" & VbNewline & _
					"</tr>" & VbNewline & _
				"</table>" & vbnewline

			else
				response.Write "<p >There are no current News available</p>"
			end if'News eof
			set News = nothing


			%>

			<form name="news" action="mail-exe.asp" method="post" onsubmit="return f_validate(this);">


			</form>


<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
