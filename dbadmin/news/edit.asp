<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Home - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/scripts/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<%
	section = "News"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->

<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp"-->
<%
NewsID	= Request.QueryString("ID")

if IsNull(NewsID) or NewsID="" then
	error	= Request.Form("error")

	response.write "<p class=""dbtext"">" & error & "</p>"

	if not IsNull(error) then
		NewsID	= Request.Form("ID")
		section	= ValidateText(Request.Form("section"))
		author	= ValidateText(Request.Form("author"))
		subject	= ValidateText(Request.Form("subject"))
		heading	= ValidateText(Request.Form("heading"))
		content	= ValidateText(Request.Form("content"))
		d_added	= ValidateText(Request.Form("d_added"))
		mypath	= Request.Form("mypath")

		PubDay		= Request.Form("PubDay")
		PubMonth	= Request.Form("PubMonth")
		PubYear		= Request.Form("PubYear")

		PublicationDate = PubDay & " " & PubMonth & " " & PubYear & " 00:00"
		if not IsNull(PublicationDate) then
			if IsDate(PublicationDate) then
				PublicationDate = FormatDateTimeIRL(PublicationDate,1)
			else
				PublicationDate =  FormatDateTimeIRL(now,1)
			end if
		else
			PublicationDate =  FormatDateTimeIRL(now,1)
		end if

		EmbDay		= Request.Form("EmbDay")
		EmbMonth	= Request.Form("EmbMonth")
		EmbYear		= Request.Form("EmbYear")
		EmbTime		= Request.Form("EmbTime")


		if error=1 then
			if IsNull(author) then v_author="required field"
			if IsNull(subject) then v_subject="required field"
		'	if IsNull(heading) then v_heading="required field"'
			if IsNull(content) then v_content="required field"
		end if
	end if
else
	'get press releases from the db'

	'response.write "exec usp_SEL_News "& NewsID &",1" '

	set rs = sqlconnection.execute("exec usp_SEL_News "& NewsID &",1" )

	if not rs.eof then
		author			= DisplayText(rs("Author"))
		subject			= DisplayText(rs("Subject"))
		'heading			= DisplayText(rs("Header"))'
		content			= DisplayText(rs("Content"))
		PublicationDate = FormatDateTimeIRL(rs("PublicationDate"),1)
		EmbargoDate		= FormatDateTimeIRL(rs("EmbargoDate"),1)
		EmbargoTime		= FormatDateTimeIRL(rs("EmbargoDate"),4)
		mypath			= rs("FileLocation")

		if len(mypath) > 5 then
			if ThisFileExists(mypath) then
				content = ReadPage(mypath)
			else
				response.Write "<p class=""redtext"">This file stored at &laquo;" & mypath & "&raquo; can not be found!</p>"
			end if
		end if

	end if
end if

if not IsDate(PublicationDate) then
	PublicationDate = now()
end if
if not IsDate(EmbargoDate) then
	EmbargoDate = now()
end if

page_content = "Please complete the form below to edit this News"
%>
<script language="JavaScript" TYPE="text/javascript">
<!--
function f_validate(f) {
	var validForm = true;
	var thisE;

	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			thisE = eval("f.v_"+ f.elements(i).name);

			if (f.elements(i).value == "") {
				thisE.value = f.elements(i).label;
				f.elements(i).select();
				validForm = false;
			}
			else {
				thisE.value = "";
			}
		}
	}

	if (!validForm) {
		alert ('Please complete all required fields');
		return false;
	}
	else {
		return true;
	}
}

function f_checker(f, myElement) {
	var thisE;
	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			if (f.elements(i).name != myElement) {
				thisE = eval("f.v_"+ f.elements(i).name);

				if (f.elements(i).value == "") {
					thisE.value = f.elements(i).label;
					return false;
				}
				else {
					thisE.value = "";
				}
			}
			else {
				return false;
			}
		}
	}
}
//-->
</script>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<p class="dbText"><%=page_content%></p>

<form name="news" action="addit.asp" method="post" onsubmit="return f_validate(this);">
	<table cellpadding="3" cellspacing="0" border="0">
		<tr>
			<td class="bluetext">Author</td>
			<td class="dbText">:</td>
			<td class="dbText"><input type="text" name="author" value="<%=author%>" class="dbInput" size="40" label="" tabindex="2" onfocus="f_checker(this.form, this.name);"></td>
			<td class="dbText">
			<input type="text" name="v_author" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="12">
			</td>
		</tr>
		<tr>
			<td class="bluetext">Subject</td>
			<td class="dbText">:</td>
			<td class="dbText"><input type="text" name="subject" value="<%=subject%>" class="dbInput" size="115" label="required field" tabindex="3" onfocus="f_checker(this.form, this.name);"></td>
			<td class="dbText"><input type="text" name="v_subject" value="<%=v_subject%>" class="dbHidden" size="15" label="" tabindex="11"></td>
		</tr>
		<!--tr>
			<td class="bluetext">Heading</td>
			<td class="dbText">:</td>
			<td class="dbText"><input type="text" name="heading" value="<%=heading%>" class="dbInput" size="115" label="required field" tabindex="4" onfocus="f_checker(this.form, this.name);"></td>
			<td class="dbText"><input type="text" name="v_heading" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="13"></td>
		</tr -->
		<tr>
			<td class="bluetext">News Content</td>
			<td class="dbText">:</td>
			<td class="dbText"><textarea name="content" class="dbInput" cols="90" rows="10" label="required field" tabindex="5" onfocus="f_checker(this.form, this.name);"><%=content%></textarea></td>
			<td class="dbText"><input type="text" name="v_content" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="14"></td>
		</tr>
		<tr>
			<td class="bluetext">Publication Date</td>
			<td class="dbText">:</td>
			<td class="dbText">
			<%
				response.Write "<select name=""PubDay"" class=""dbInput"">" & vbnewline
				For i=1 to 31
					Response.Write "<option"
                  		if Cint(Day(PublicationDate))=i Then Response.Write " selected"
                	Response.Write ">" & i & "</option>"
				Next
				response.Write "</select>" & VbNewline & _
				"<select name=""PubMonth"" class=""dbInput"">" & vbnewline
				For i=1 to 12
					Response.Write "<option"
						if Cint(Month(PublicationDate))=i Then Response.Write " selected"
					Response.Write ">" & MonthName(i) & "</option>"
				Next
					response.Write "</select>" & VbNewline & _
				"<select name=""PubYear"" class=""dbInput"">" & vbnewline
				For i=1990 to Cint(Year(now()))
					Response.Write "<option"
						if Cint(Year(PublicationDate))=i Then Response.Write " selected"
					Response.Write ">" & i & "</option>"
				Next
					response.Write "</select>" & VbNewline
				%>
			</td>
		</tr>
		<tr>
			<td class="bluetext">Embargo Date</td>
			<td class="dbText">:</td>
			<td class="dbText">
			<%
			response.Write "<select name=""EmbDay"" class=""dbInput"">" & vbnewline
			For i=1 to 31
				Response.Write "<option"
					if Cint(Day(EmbargoDate))=i Then Response.Write " selected"
                Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbMonth"" class=""dbInput"">" & vbnewline
			For i=1 to 12
				Response.Write "<option"
					if Cint(Month(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & MonthName(i) & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbYear"" class=""dbInput"">" & vbnewline
			For i=1990 to Cint(Year(now()))
				Response.Write "<option"
					if Cint(Year(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline
			'time'
			'@@ get the time previosuly selected, if not set to Noon'
			response.Write VbNewline & _
			"<select name=""EmbTime"" class=""dbInput"">" & vbnewline
			For i=0 to 1440 step 30
				TimeOption = FormatDateTimeIRL(DateAdd ("n", i, "1 january 2000 00:00"),4)
				Response.Write "<option"
					if trim(EmbargoTime) = trim(TimeOption) Then
						Response.Write " selected"
					end if
				Response.Write ">" & TimeOption & "</option>"
			Next
				response.Write "</select>" & VbNewline
			%>

			</td>
			<td class="dbText">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<td class="dbText"  colspan="2">&nbsp;&nbsp;
				<input type="hidden" name="NewsID" value="<%=NewsID%>">
				<input type="hidden" name="mypath" value="<%=mypath%>">
				<input type="submit" value="Update Article" class="TextInput" tabindex="6" label="" onfocus="f_checker(this.form, this.name);">
			</td>
		</tr>
	</table>
</form>
<%
if not IsNull(NewsID) and NewsID <>"" then
	'display attach media only when NewsID is known'
%>
<table width="100%">
	<tr>
		<td colspan="2"><hr class="hrgrey" ID="hr3"></td>
	</tr>
</table>
<table cellpadding="1" cellspacing="0" border="0">
	<tr>
		<td class="bluetext">Attached Media :</td>
		<td class="dbText" colspan="2" align="left">
			<a href="JavaScript:openWindowNamed('/dbadmin/ssi/AttachMedia/pop_media.asp?section=News&SupportID=<%=NewsID%>','700',600,'media')"  class="dbText">Add Media</a>
		</td>
	</tr>
	<%call DrawAttachedMediaDB(NewsID,"News")%>
</table>
<hr class="hrgrey" ID="hr4" width="100%">
<% end if%>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<%
set rsSections = nothing
%>

<!--#include virtual="/ssi/dbclose.asp" -->
