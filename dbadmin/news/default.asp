<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>News - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<%
section = "News"

' include at the top '
mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 30
%>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<%
sql = "exec usp_SEL_LatestNews NULL,1"
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open Sql , sqlConnection


if NOT rs.eof THEN
	cntM=0
	rec = 1
	rs.movefirst
	rs.pagesize = maxdisplay
	rs.absolutepage = mypage
	maxpages = cint(rs.pagecount)
	' display the total of records returned by sql query'
	intRecordCnt = rs.RecordCount

%>

<h1>Manage News</h1>


<div id="docmanage">


	<table  border='0' cellspacing='0' cellpadding='0'>
		<tr>
			<th scope="col"><b>Author</b></td>
			<th scope="col"><b>Media</b></td>
			<th scope="col"><b>Title</b></td>
			<th scope="col" align="center"><b>Publication Date</b></td>
			<th scope="col" align="center"><b>Embargo Date</b></td>
			<th scope="col" align="center"><img src="/dbadmin/images/icon_online.gif" width="20" height="20" alt="Online / Offline" /></td>
			<th scope="col"><%If Session("AdminStatus") > 8 Then response.write "Email"%></td>
			<th scope="col">Edit</td>
			<th scope="col">Delete</td>
		</tr>
		<%
			cnt = 0
			do while not rs.eof and cnt<maxdisplay
			cnt = cnt+1
			If cnt mod 2 = 0 then
				BgColor = "#deeaea"
			else
				BgColor = "#ecf2f2"
			end if

			HasEditingPriv = false
			If Session("AdminStatus") > 8 Then
				HasEditingPriv = true
			End if
			If Session("AdminID")= rs("RecordCreatedBy") Then
				HasEditingPriv = True
			End if


			If HasEditingPriv then
		%>
		<tr>
			<td valign="top" bgcolor="<%=BgColor%>"><%=rs("Author")%></td>
			<td align="left" valign="top" bgcolor="<%=BgColor%>">
		<%
			if rs("Media") > 0 then
				response.Write "<a href=""JavaScript:openWindow('/dbadmin/media/list.asp?SupportID="& rs("NewsID") &"&amp;section=News',700,400)"">"
				response.write DrawAttachedMedia(rs("NewsID"),section)
				response.Write "</a>"
			else
		%>
			<a href="JavaScript:openwin('/dbadmin/ssi/AttachMedia/pop_media.asp?section=News&SupportID=<%=rs("NewsID")%>','browse','scrollbars=yes,resizable=yes,left=10,top=10,screenX=10,screenY=10,width=700,height=560,innerHeight=0,innerWidth=0,outerHeight=0,outerWidth=0')" class="dbText">Add Media</a>

		<%
			end if
		%>
			</td>
			<td valign="top" bgcolor="<%=BgColor%>"><%=DisplayText(rs("Subject"))%></td>
			<td align="center" valign="top" bgcolor="<%=BgColor%>"><%=FormatDateTimeIRL(rs("PublicationDate"),1)%></td>
			<td align="center" valign="top" bgcolor="<%=BgColor%>">
				<%
				response.write FormatDateTimeIRL(rs("PublicationDate"),1) & " " & FormatDateTimeIRL(rs("EmbargoDate"),4)
				%>
			</td>
			<td align="center" valign="top" bgcolor="<%=BgColor%>">
				<%
				'online / offline ?? '
				if rs("online")=1 Then
					OnlineImg="tick.gif"
					AltTxt="This file is available for viewing if its embargo date has been reached - Click here to put it offline"
				else
					OnlineImg="cross.gif"
					AltTxt="This file is offline, it cannot be viewed on the public website - Click here to put it online"
				end if
				response.Write "<a href='switchstatus.asp?NewsID=" & rs("NewsID") & "&amp;mypage="& mypage&"'><img src=""/dbadmin/images/" & OnlineImg & """ title=""" & AltTxt &""" width=""10"" height=""10"" border=""0"" class='txtblack'></a>"
				%>
			</td>
		<td valign="top" bgcolor="<%=BgColor%>">
				<%
				If Session("AdminStatus") > 8 Then
				%>
				<a href="email.asp?NewsID=<%=rs("NewsID")%>" title="Email this News"><img src="/dbadmin/images/EmailIco.gif" alt="Email this News"  border="0" width="18" height="16" /></a>
				<%
				End if
				%>
				</td>
			<td valign="top" bgcolor="<%=BgColor%>">
				<a href="wysiwyg/editor.asp?ID=<%=rs("NewsID")%>" title="Edit this News in a WYSIWYG editor"><img src="/dbadmin/images/wysiwyg_edit.gif" alt="Edit this News in a WYSIWYG editor" border="0" width="48" height="23" /></a>
				|<a href="edit.asp?ID=<%=rs("NewsID")%>" title="Edit this News (raw source)"><img src="/dbadmin/images/html_edit.gif" alt="Edit this News (raw source)" border="0" width="33" height="23" /></a>

				</td>
			<td valign="top" bgcolor="<%=BgColor%>"><a href="delete.asp?NewsID=<%=rs("NewsID")%>&redirURL=<%=server.urlencode(ThisPage)%>" onClick="return confirm('Are you sure you want PERMANENTLY remove this News Release\n* Once performed, this action cannot be reversed!!');"><img src="/dbadmin/images/delete.gif" alt="Delete this News"  border="0" width="13" height="16" /></a></td>
		</tr>
		<%
			End If 'HasEditingPriv
			rs.MoveNext
			Loop
		%>
	</table>
		<%
			else
				response.Write "<p class=""bluetext"">There are no current News in the database</p>"

			end if
			set rs = nothing


			if maxpages > 1 then
				call Paging
			else
			response.write "&nbsp;"
			end if
		%>
</div>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "<span class=""bluetext"">Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a class=""bluetext"" href=""default.asp?mypage=" & counter &""">" & _
				counter & _
				"</a>"
			else
				Response.Write "<i class='bluetext02'>" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
		Response.Write "</span>"
	end if
end sub
%>
