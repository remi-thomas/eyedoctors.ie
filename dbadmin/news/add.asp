<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Home - ICO- Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp"-->
<%
section = "News"


error	= Request.Form("error")
if not IsNull(error) then
	NewsID	= Request.Form("ID")
	author	= Request.Form("author")
	subject		= left(ValidateText(Request.Form("subject")),799)
	heading		= left(ValidateText(Request.Form("heading")),999)
	content	= DisplayText(Request.Form("content"))

	if error=1 then
		if IsNull(author) then v_author="required field"
		if IsNull(subject) then v_subject="required field"
		if IsNull(heading) then v_heading="required field"
		if IsNull(content) then v_content="required field"
	end if
end if

if IsNull(author) or author = "" then
	author = Session("AdminUserName")
end if

PublicationDate = date()
EmbargoDate		= date()

page_content = "Please complete the form below to post a new News Item to the ICO Website Administration "
%>
<script language="JavaScript">
<!--
function f_validate(f) {
	var validForm = true;
	var thisE;

	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			thisE = eval("f.v_"+ f.elements(i).name);

			if (f.elements(i).value == "") {
				thisE.value = f.elements(i).label;
				f.elements(i).select();
				validForm = false;
			}
			else {
				thisE.value = "";
			}
		}
	}

	if (!validForm) {
		alert ('Please complete all required fields');
		return false;
	}
	else {
		return true;
	}
}

function f_checker(f, myElement) {
	var thisE;
	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			if (f.elements(i).name != myElement) {
				thisE = eval("f.v_"+ f.elements(i).name);

				if (f.elements(i).value == "") {
					thisE.value = f.elements(i).label;
					return false;
				}
				else {
					thisE.value = "";
				}
			}
			else {
				return false;
			}
		}
	}
}
//-->
</script>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<h1>Add News</h1>

<p><%=page_content%></p>


<form action="addit.asp" method="post" onsubmit="return f_validate(this);" id="newsform">
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	<th scope="row">Author</th>
	<td><input type="text" name="author" class="TextInput" size="85" label="" tabindex="2" onFocus="f_checker(this.form, this.name);" value="<%=author%>"> <input type="text" name="v_author" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="12"></td>
</tr>
<tr>
	<th scope="row">Title</th>
	<td><input type="text" name="subject" value="<%=subject%>" class="TextInput" size="85" label="required field" tabindex="3" onFocus="f_checker(this.form, this.name);"> <input type="text" name="v_subject" value="<%=v_subject%>" class="dbHidden" size="15" label="" tabindex="11"></td>
</tr>
<tr>
	<th scope="row">Heading</th>
	<td><input type="text" name="heading" class="TextInput" size="85" label="required field" tabindex="4" onFocus="f_checker(this.form, this.name);"> <input type="text" name="v_heading" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="13"></td>
</tr>
<tr>
	<th scope="row">News Release Content</th>
	<td><textarea name="content" class="TextInput" cols="87" rows="10" label="required field" tabindex="5" onFocus="f_checker(this.form, this.name);"><%=content%></textarea> <input type="text" name="v_content" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="14"></td>
</tr>
<tr>
	<th scope="row">Publication Date</th>
	<td>
	<%
		response.Write "<select name=""PubDay"" class=""TextInput"">" & vbnewline
		For i=1 to 31
			Response.Write "<option"
				if Cint(Day(PublicationDate))=i Then Response.Write " selected"
			Response.Write ">" & i & "</option>"
		Next
		response.Write "</select>" & VbNewline & _
		"<select name=""PubMonth"" class=""TextInput"">" & vbnewline
		For i=1 to 12
		Response.Write "<option"
			if Cint(Month(PublicationDate))=i Then Response.Write " selected"
		Response.Write ">" & MonthName(i) & "</option>"
		Next
		response.Write "</select>" & VbNewline & _
		"<select name=""PubYear"" class=""TextInput"">" & vbnewline
		For i=1990 to Cint(Year(now()))
		Response.Write "<option"
			if Cint(Year(PublicationDate))=i Then Response.Write " selected"
		Response.Write ">" & i & "</option>"
		Next
		response.Write "</select>" & VbNewline
	%>
	</td>
</tr>
<tr>
	<th scope="row">Embargo Date</th>
	<td>
			<%
			response.Write "<select name=""EmbDay"" class=""TextInput"">" & vbnewline
			For i=1 to 31
				Response.Write "<option"
					if Cint(Day(EmbargoDate))=i Then Response.Write " selected"
                Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbMonth"" class=""TextInput"">" & vbnewline
			For i=1 to 12
				Response.Write "<option"
					if Cint(Month(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & MonthName(i) & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbYear"" class=""TextInput"">" & vbnewline
			For i=1990 to Cint(Year(now()))
				Response.Write "<option"
					if Cint(Year(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline
			'time'
			'@@ get the time Newseviosuly selected, if not set to Noon'
			response.Write VbNewline & _
			"<select name=""EmbTime"" class=""TextInput"">" & vbnewline
			For i=0 to 1440 step 30
				TimeOption = FormatDateTimeIRL(DateAdd ("n", i, "1 january 2000 00:00"),4)
				Response.Write "<option"
					if trim(EmbargoTime) = trim(TimeOption) Then
						Response.Write " selected"
					end if
				Response.Write ">" & TimeOption & "</option>"
			Next
				response.Write "</select>" & VbNewline
			%>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><div><input type="submit" value="Add News Release" class="TextInput" tabindex="6" label="" onFocus="f_checker(this.form, this.name);"></div></td>
</tr>
</table>
</form>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->