<%
Server.ScriptTimeout  = 6000
'update for each site'
DomainName = "http://ICO.ie/dbadmin/"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp"-->

<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/mailer.asp"-->
<html>
<head>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>

<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>News - mailing</h1>
<%
'get session data'
Subject		= session("emailSubject")
Message		= session("pageHtml")
'end session data'

'get from data
if len(Subject) < 3 then
	Subject = request.Form("subj")
	Message = request.Form("mssge")
	session("emailSubject") = Subject
	session("pageHtml") = Message
end if


if len(Message) < 5 then
	response.Write "Err 1 -> message too short"
	response.end
end if
if len(Subject) < 3 then
	response.Write "Err 1 -> subject too short"
	response.end
end if

HTML_Top =  vbnewline & _
"<html>"  & vbnewline & _
"<head>"  & vbnewline & _
"<title>ICO Website Administration  - Login</title>"  & vbnewline & _
"<style>"  & vbnewline & _
"body {background: #fff;color: #000;font-family: Arial, Helvetica, sans-serif;font-size: 12px;margin: 0;padding: 0;text-align: left;}"  & vbnewline & _
"#container { border:1px solid #ddd; margin:20px 10px 10px; }"   & vbnewline & _
"#header { background:url(http://www.ICO.ie/dbadmin/images/coms.gif) no-repeat top right; height:102px; width:100%; }"   & vbnewline & _
"#header img { margin:15px 0 0 0; }"   & vbnewline & _
"#base { background:#f2f2f2; border-top:0 solid #dfdede; color:#666; padding:5px 10px; position:relative; text-align:left; }}"   & vbnewline & _
"#baselinks a { padding:0 10px; }}"   & vbnewline & _
"</style>"  & vbnewline & _
"<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1""></head>"  & vbnewline & _
"<body>"   & vbnewline & _
"<div id=""container"">"  & vbnewline & _
"<div id=""header"">" & vbnewline & _
"  <a href=""http://www.ICO.ie/comms""><img src=""http://www.ICO.ie/images/top_logo.gif"" alt=""ICO"" width=""260"" height=""79"" border=""0""></a>"  & vbnewline & _
"</div>" & vbnewline



HTML_bottom  =  vbnewline & _
"  <div id=""base"">"  & vbnewline & _
"   <div id=""baselinks""><a href=""http://ICO.ie/comms"">ICO.ie Website Administration </a> </div>"  & vbnewline & _
"</div>"  & vbnewline & _
"</div>"  & vbnewline & _
"</body>"  & vbnewline & _
"</html>"  & vbnewline 



	' get the page number to be processed '
	mypage = request.querystring("mypage")
	if Not IsNumeric(mypage) or mypage ="" Then
		mypage=1
	end if

	Response.write "<font face='verdana, helvetica' size='2'>"
	maxdisplay = 3

	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = 3
	rs.cachesize = maxdisplay

	sqlmail = "SELECT distinct email FROM AdminUsers where CHARINDEX(' ',LTRIM(RTRIM([Email]))) = 0 AND 	LEFT(LTRIM([Email]),1) <> '@'  -- '@' AND 	RIGHT(RTRIM([Email]),1) <> '.' -- '.' AND 	CHARINDEX('.',[Email],CHARINDEX('@',[Email])) - CHARINDEX('@',[Email]) > 1 -- There must be a '.'AND 	LEN(LTRIM(RTRIM([Email]))) - LEN(REPLACE(LTRIM(RTRIM([Email])),'@','')) = 1 AND  HARINDEX('.',REVERSE(LTRIM(RTRIM([Email])))) >= 3 AND 	(CHARINDEX('.@',[Email]) = 0 AND CHARINDEX('..',[Email]) = 0) group by email order by email"

	rs.Open sqlmail , sqlConnection

	if not rs.eof then
		rec = 1

		counter = request.querystring("counter")
		counter = counter + 0

		rs.movefirst
		rs.pagesize = maxdisplay
		rs.absolutepage = mypage
		maxpages = cint(rs.pagecount)
		nbmail = rs.RecordCount
		response.write nbmail & " registered users in the database "
		Response.write "<br />Page " & MyPage & " out of " & maxpages & ". <br><font color='red'>This page will reload " & int(maxpages-Mypage) & " more times </font> <i>(Do not close this window until job is complete)</i><br><br>"

		Do until rs.eof  or (rec > maxdisplay)
			counter = counter + 1
			rec = rec + 1

			ThisSubject = Session("EmailSubject")
			Message = HTML_Top &  session("pageHtml") & HTML_bottom
			SendTo = rs(0)

			if len(Message) < 5 then
				response.Write "email contents is too short"
				response.end
			End if


			'response.Write "<br/>" & UserName

			' UNCOMMENT THIS TO REALLY SEND THE E-MAILS
			if instr(SendTo,"@") > 1 then
				on error resume next
				mail = SendMail ("administratot@ico.ie", SendTo, ThisSubject,"", Message)
				on error goto 0
			end if
			response.write "<br>" & counter & ". " & rs(0)

			rs.MoveNext

			if not rs.eof  and (rec > maxdisplay)  then

				if maxpages > 1  then
				%>
					<meta http-equiv="Refresh" content="1; URL=mail-exe.asp?mypage=<%=mypage+1%>&counter=<%=counter%>">
				<%
				else
					exit do
				end if

			elseif rs.eof Then
				response.write "<hr><br>A total of "& counter & " emails have been sent<br>"
				response.write "<h2>" & Session("EmailSubject") & "</h2>"
				response.write "<p>" & Session("pageHTML") & "</p>"
				Session("EmailSubject")	= ""
				Session("pageHTML")	= ""
			end if
		Loop
	end if
%>

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>