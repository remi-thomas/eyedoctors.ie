<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp " -->
<!--#include virtual="/ssi/fct/common.asp " -->
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/dbadmin/ssi/lib/commonlib.js"></script>
<script language="javascript" type="text/javascript" src="/ssi/scripts/openwindow.js"></script>
<script language="javascript" type="text/javascript" src="/dbadmin/ssi/lib/loaddata.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h2>Welcome back, <%=Session("AdminUserName")%></h2>

<div id="leftcol" style="width:45%; float: left;">

	<%
	'######################'
	'##     Fresh data   ##'
	'######################'
	Set fd = SqlConnection.Execute("exec usp_SEL_FreshDataSinceLastLogin " & Session("AdminID"))
	if not fd.eof then
		LastLogin = fd("LastLogin")
		if not isDate(LastLogin) then
			LastLogin =  now()
		end if

response.write "<h2>" & FormatDateTimeIRL(now(),1) & "</h2>" & vbnewline

	%>
	<p>Your last visit was on <%=FormatDateTimeIRL(LastLogin,1)%> - <%=FormatDateTimeIRL(LastLogin,4)%></p>
	<div id="freshdata">
	  <h1>Documents published since your last visit</h1>
	  <ul>
	    <li><a href="/dbadmin/eventsboard/">News</a>: <%=fd("RecentNews")%></li>
	    <li><a href="/dbadmin/eventsboard/">PRs</a>: <%=fd("RecentPRs")%></li>
	  </ul>
	</div>
	<div id="overalldata">
	  <h1>Documents Online</h1>
	  <ul>
	    <li><a href="/dbadmin/eventsboard/">News</a>: <%=fd("ttlNews")%></li>
	    <li><a href="/dbadmin/PR/">PRs</a>: <%=fd("TtlPRs")%></li>
	  </ul>
	</div>
	<%
	else
		LastLogin = DateAdd ("m", -1, now())
	end if
	%>
	<%
	'######################'
	'##     PRs         ##'
	'######################'
	sql = "SELECT TOP 5 PrID, Subject, [Content], FileLocation, Header, Author, PublicationDate, EmbargoDate, Online FROM PR ORDER BY PrID DESC"

	'response.Write sql

	Set rs = SqlConnection.Execute(sql)
	if not rs.eof then
	%>
	<div id="newnews">
	  <h1>5 Most Recent PRs</h1>
	  <%
		Do until rs.eof

		response.Write "<p>" & VbNewline
			response.Write	"<a href=""/dbadmin/pr/wysiwyg/editor.asp?ID=" & rs("PrID") & """>" & TrimTitle(DisplayText(rs("Subject")),100) & "</a>"
			response.Write	"<br/>" & TrimString(RemoveAllHTMLTags(DisplayText(rs("Content"))),150, "/dbadmin/pr/wysiwyg/editor.asp?ID="& rs(0) )
			response.Write "<br/><span class=""smalltext ""> Published on " & FormatDateTime (rs("PublicationDate"), vblongdate) & "</span>"
		response.Write "<p>" & VbNewline
		rs.Movenext
		Loop
		%>
	</div>
	<%
	end if 'rs.eof news'
	%>
	


</div> <!--end leftcol !-->
<%
'###########################'
'##        calendar       ##'
'###########################'

  nDate = Date()



nDatePrev = DateAdd("m", -1, nDate)
nDateSuiv = DateAdd("m", 1, nDate)
nDay = Day(nDate)
nMonth = Month(nDate)
nYear = Year(nDate)
nDate1 = DateSerial(nYear, nMonth, 1)
Dim nTemp
nTemp = DateAdd("m", 1, nDate1)
nDay2 = Day(DateAdd("d", -1, nTemp))
nDate2 =  DateSerial(nYear, nMonth, nDay2)
nStartDate = WeekDay(nDate1, vbMonday)
nEndDate =  WeekDay(nDate2, vbMonday)
nbrCase = Day(nDate2) + (nStartDate-1) + (7-nEndDate)
nNomJour = 1



%>
<div id="rightcol" style="float:right;width:50%;padding-right:5px;">

<style type="text/css">
th.calMonth {
	text-align:center;
	color:#FFF;
	font-size:14px;
	font-weight:bold;
	background:#5db1b2;
}
td.calSing {
	font-size :10px;
	text-align : center;
	color : #FFF;
	background : #5db1b2;
}
td.calWeek {
	color:#FFF;
	font-weight:bold;
	font-size:12px;
	background:#5db1b2;
}
td.calDay {
	font-size :10px;
	color:#006666;
	font-weight:bold;
	background : #cce4e5;
	height:30px;
	width:100px;
}
td.calDay A {
	font-size : 10px;
	color:#006666;
	font-weight:bold;
	text-decoration: none;
	height:30px;
	width:100px;
}
td.calDay A:hover {
	font-size : 10px;
	color :#006666;
	font-weight:bold;
	text-decoration: underline;
	height:30px;
	width:100px;
}
td.calDayOver {
	font-size : 10px;
	color : #000000;
	background:#f2f2f2;
	height:30px;
	width:100px;
}
td.calDayOver A {
	font-size : 10px;
	color : #000000;
	background:#f2f2f2;
	height:30px;
	text-decoration: none;
}
td.calDayOver A:hover {
	font-size : 10px;
	color :#DD0000;
	background:#f2f2f2;
	height:30px;
	text-decoration: none;
}
</style>

		<table border="0" cellspacing="1" cellpadding="0" width="100%" id="eventscal" >
		  <!-- Start month, year and button header  -->
		  <tr>

		    <th class="calMonth">&nbsp;</th>

		    <th colspan="5" class="calMonth" valign="middle" align="center">

		    </th>
		    <th class="calMonth" valign="middle"><a href="/dbadmin/eventsboard/default.asp?date=<%=nDateSuiv%>"><img src="/dbadmin/eventsboard/b_suiv.gif" width="14" height="13" border="0" alt="Next Month"></a></th>
		  </tr>
		  <!-- End month, year and button header  -->
		  <!-- Start weekday header -->
		  <tr>
		    <% for n = 1 to 7 %>
		    <td width="14%" align="center" valign="middle" class="calWeek"><%=WeekDayName(n, false, vbMonday)%></td>
		    <% next ' Boucle des noms du jour / WeekName for %>
		  </tr>
		  <!-- End weekday header -->
		  <!-- Start day's loop -->
		  <%
		Dim njTemp
		For n = 1 to nbrCase
		  njTemp = n - nStartDate+1
			if nNomJour = 1 then
				response.write "<tr>"
			end if


			if n <  (nStartDate) or n > (nDay2+nStartDate-1) then
				'no day on this cell
				response.write "<td align=""center"" valign=""top"" bgcolor=""#ecf2f2"" valign=""middle"">&nbsp;"
					if n = 1 or (n mod 7 = 0) then
						displayaddevent = displayaddevent + 1
						response.Write "<h3><a href=""/dbadmin/eventsboard/addnewevent.asp""><img src=""/dbadmin/images/addevent.gif"" alt=""Add a new event to calendar"" border=""0"" />  Add a new event</a></h3>"
					end if
			else
				If IsThePast(njTemp,nMonth,nYear) then
					response.write "<td align=""left"" valign=""top"" class=""calDayOver"">"
				else
					response.write "<td align=""left"" valign=""top"" class=""calDay"">"

				end if
				response.write "<p align=""center"" style=""padding:0px;margin:0px;"">" & njTemp & "</p>" & vbnewline

				'sql ="SELECT EventID, Title, UserName,StartDate FROM ComsCentre_EventsCalendar where StartDate >='"& njTemp & " " & MonthName(nMonth) & " " & nYear &" 00:00:00' and  EndDate <='"& njTemp & " " & MonthName(nMonth) & " " & nYear & " 23:59:59'"
			sql ="SELECT EventID, Title, UserName,StartDate FROM ComsCentre_EventsCalendar where ('"& njTemp & " " & MonthName(nMonth) & " " & nYear &" 00:00:01' between StartDate and EndDate) or StartDate >= '"& njTemp & " " & MonthName(nMonth) & " " & nYear &" 00:00:01' and EndDate <= '"& njTemp & " " & MonthName(nMonth) & " " & nYear &" 23:59:59'"

				Set rs = sqlconnection.execute(sql)
				do while NOT rs.eof
					response.Write "<img src='/images/blue02.gif' height='2' width='2'> <a href=""JavaScript:onclick=openWindowNamed('/dbadmin/Eventsboard/pop_event.asp?EventID=" & rs(0) & "','440','460','eboard')"" title=""" & DisplayText(rs(1)) & """>" & TrimTitle(replace(DisplayText(rs(1)),","," "),25) & "</a><br/>" &  vbnewline
				rs.movenext
				loop
			end if

		  response.write "</td>"
			if nNomJour = 7 then
				response.write "</tr>"
			end if
			if nNomJour = 7 then
				nNomJour = 1
			else
				nNomJour = nNomJour + 1
			end if
		next ' Boucle des jour / Day for
		%>
		  <!-- End day's loop -->
		  <tr>
		    <td class="calSing" colspan="7">&nbsp;</td>
		  </tr>
		</table>








</div>

<%
Function IsThePast(DayTemp,MonthTemp,YearTemp)
	dim tempdate, nowdate
	nowdate = FormatDateTimeIRL(now(),2)
	tempdate = DayTemp & "/" & MonthTemp & "/" & YearTemp
	If tempdate="" or not IsDate(tempdate) Then
		IsThePast = true
		Exit Function
	else
		tempdate = FormatDateTimeIRL(tempdate,2)
		'response.write "<br/><span style=""color:green"">"& tempdate &"</span>" & vbewline 
		'response.write "<br/><span style=""color:green"">"& nowdate &"</span>" & vbewline 


		If DateDiff("d",nowdate,tempdate) < 1 then
			IsThePast = true
			Exit Function
		end if
	end if
end function
%>


<br style="clear:both">

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
