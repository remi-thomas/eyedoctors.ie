<%
Server.ScriptTimeout  = 1500
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->

<html>
<head>

<title>ICO - Website Administration</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script type="text/javascript" src="/dbadmin/ssi/ckeditor 4/ckeditor.js"></script>

</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<%
mode = lcase(request.querystring("mode"))
If mode <> "add" then
	mode ="edit"
end if
DoF_File = request("DoF_File")
'response.write "<br/> " & InstrRev(DoF_File,"/")
If InstrRev(DoF_File,"/") = Len(DoF_File) Then
	DoF_File = Left(DoF_File,Len(DoF_File)-1)
End if
if Len(DoF_File) > 5 then
	DocumentName	= GetFileFromPath(DoF_File)
	DocumentPath	= GetDirectoryFromPath(DoF_File)
else
	response.Write "<p class=""bluetext"">Error! DocID ("& DocID &") is not numeric - " & IsNumeric(DocID)
	response.End
End if
content = readpage(DoF_File)
content = DecodeText(content)
%>
<br/>
<fieldset name="PageOption" class="stickiebox" style="width:750px;">
    <legend class="stickieheadline">Page Option</legend>
	<img src="/dbadmin/images/bulb.gif" width="20" height="17" alt="!" align="left" border="0">
	<span class="dbtext">You're currently editing the following file '<i><%=DocumentName%></i>' in this directory '<i><%=DocumentPath%></i>'.
	</span>
</fieldset>
<br/>
<form action="get.asp" method="post" style="margin:0px">
<div align="center" class="admintable">

<br />
<table width="99%" align="center" cellspacing="0" style="top-margin:0px">
	<TR>
		<td>
			<textarea name="editor1"><%=content%>

			</textarea>
		</td>
	</tr>

	<tr>
		<td valign="top" class="dbtext" align="right">
		
			<input type="hidden" name="mode" value="<%=mode%>">
			<input type="hidden" name="DoF_File" value="<%=DoF_File%>">
			<input type="submit" value="Save Document &gt;&gt;"  class="textinput">
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
</form>
<br/>
<script type="text/javascript">
	window.onload = function()
	{
		CKEDITOR.replace( 'editor1' 
		,
		{
		height:"400"
		}
		
		);
	};
</script>
</body>
</html>
