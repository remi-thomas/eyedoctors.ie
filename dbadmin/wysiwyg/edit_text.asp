<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp" -->
<html>
<head>
<title>ICO - Website Administration</title>

<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>

<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
DocID = request("DocID")
if not Isnumeric(DocID) or DocID = "" then
	response.Write "<p class=""bluetext"">Error! DocID ("& DocID &") is not numeric - " & IsNumeric(DocID)
	response.end
else
	set rs = sqlconnection.execute("Select FileLocation from Documents where DocId=" & DocID)
	if rs.eof then
		response.Write "<p class=""bluetext"">Error! sql returns eof for "& DocID &")"
		response.end
	else
		DoF_File		= rs(0)
		DocumentName	= GetFileFromPath(rs(0))
		DocumentPath	= GetDirectoryFromPath(rs(0))
	end if
end if
ThisContent = ReadPage(DoF_File)

'response.Write ThisContent
%>
<br/>&nbsp;<br/>
<fieldset name="PageOption" class="fieldset">
    <legend class="legend">Page Option</legend>
	<img src="/dbadmin/images/bulb.gif" width="20" height="17" alt="!" align="left" border="0">
	<span class="dbtext">You're currently editing the raw html of following  '<i><%=DocumentName%></i>' in this document  '<i><%=DocumentPath%></i>'.
	</span>
</fieldset>
<br/>&nbsp;
<div align="center" class="admintable">
<form action="get.asp" method="post">
	<textarea name="FCKeditor1" cols="140" rows="45" class="TextInput">
		<%=ThisContent%>
	</textarea>
  <br />
  <input type="hidden" name="MyPath" value="<%=DoF_File%>">


			<input type="hidden" value="<%=DocID%>" name="DocID">
			<input type="submit" value="Save Document &gt;&gt;"  class="textinput"><br /><br />
		</form>

</div>

<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>
