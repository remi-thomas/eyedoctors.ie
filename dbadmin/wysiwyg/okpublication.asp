<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->

<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp" -->
<%
DoF_File = request.querystring("DoF_File")
DocumentName	= GetFileFromPath(DoF_File)
DocumentPath	= GetDirectoryFromPath(DoF_File)
%>
<html>
<head>
<title>ICO - Website Administration</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="40" class="bluetext">&nbsp;&nbsp;<b>Administration Section</b> 
  </tr>
</table>
<table border="0" cellspacing="10" cellpadding="0" >
	<tr>
		<td class="dbtext">The file <i><%=DocumentName%></i> in the following directory <i><%=DocumentPath%></i> has been fully updated.</td>
	</tr>
</table>

<br/>

</body>
</html>