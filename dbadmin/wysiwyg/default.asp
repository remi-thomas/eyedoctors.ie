<% @ Language = VBScript %>
<%
Option Explicit
With Response
	.Buffer = True
	.Expires = 0
	.Clear
End With

Dim ThisPage, StatusRequired, RememberLastFolder, TopUserName

'check if the admin has already published and if yes redirect to the last directory'
if Request.querystring("Folder") <> "" then
	session("RememberLastFolder") = unescape(Request.querystring("Folder"))
else
	session("RememberLastFolder") = "/"
end if

%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/dbadmin/ssi/class/browser.asp"-->
<html>
<head>
<title>ICO - Website Administration</title>

<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<%
'################################
Dim oBrws

 'Call DirectoryBrowser Class
Set oBrws = New DirectoryBrowser
With oBrws
	 'set sort property
	.Sort = Request.querystring("Sort")

	 'set path property
	.Path = session("RememberLastFolder")


	 'set restricted browsing to true.
	 'Enable the full version by setting
	 'this property to False.
	.RestrictBrowsing = True

	 'call the method that returns
	 'the display.
	.GetPath
End With

 'Release Class from memory
Set oBrws = Nothing
%>


<br/>
</body>
</html>
