<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/cleantext_function.asp " -->
<%
Title				= ValidateText(request.form("Title"))
StartDate			= request.form("StartDate")
StartTime			= request.form("StartTime")
EndDate				= StartDate
'EndDate				= request.form("EndDate")
EndTime			= request.form("EndTime")

eventdescription	= request.form("eventdescription")
EventID				= ValidateNumeric(request.form("EventID"))
Conferences			= request.form("Conferences")


If Not IsNumeric(Conferences) Or Conferences = "" Then
	Conferences = 0
End if
If Conferences > 1 Then
	Conferences = 0
End if

EventAbroad = request.form("EventAbroad")
If Not IsNumeric(EventAbroad) Or EventAbroad = "" Then
	EventAbroad = 0
End if
If EventAbroad > 1 Then
	EventAbroad = 0
End if


EventExam = request.form("EventExam")
If Not IsNumeric(EventExam) Or EventExam = "" Then
	EventExam = 0
End if
If EventExam > 1 Then
	EventExam = 0
End if

online = request.form("online")
If Not IsNumeric(online) Or online = "" Then
	online = 0
End if
If online > 1 Then
	online = 0
End if

archive = request.form("archive")
If Not IsNumeric(archive) Or archive = "" Then
	archive = 0
End if
If archive > 1 Then
	archive = 0
End if


response.write "<br/>Conferences " & Conferences & " EventAbroad " & EventAbroad & " News " & News & " EventExam " & EventExam & " online " & online
'response.end

If IsDate(StartDate) then
		StartDate	= StartDate & " " & StartTime
		EndDate	= EndDate & " " & EndTime

		Title				= left(RemoveAllHTMLTags(RemoveJavascript(RemoveMsWordTags(Title))),190)

		'eventdescription	= RemoveMsWordTags(eventdescription)
		eventdescription	= validateText(eventdescription)
		'eventdescription	= Replace(eventdescription,chr(10),"<br/>")

		response.write "<br/>eventdescription:<br/>" & eventdescription
		'response.end

		'sproc to insert data into Boardeventdescriptions
		Set adocmd  = Server.CreateObject("ADODB.Command")
		adocmd.ActiveConnection = sqlConnection
		adocmd.CommandText = "usp_INS_PostGraduates_Board"
		adocmd.CommandType = 4

		Set prm1 = adocmd.createparameter("@UserID", 3, 1)
		prm1.value = Session("AdminID")
		adocmd.Parameters.append prm1

		Set prm2 = adocmd.createparameter("@UserName", 200, 1, 70)
		prm2.value = Session("AdminUserName")
		adocmd.Parameters.append prm2

		Set prm3 = adocmd.createparameter("@EventDate", 7, 1)
		prm3.value = StartDate
		adocmd.Parameters.append prm3

		Set prm4 = adocmd.createparameter("@EndDate", 7, 1)
		prm4.value = EndDate
		adocmd.Parameters.append prm4

		Set prm5 = adocmd.createparameter("@Title", 200, 1, 200)
		prm5.value = Title
		adocmd.Parameters.append prm5

		Set prm6 = adocmd.createparameter("@eventdescription", 201, 1, 1073741823)
		prm6.value = eventdescription
		adocmd.Parameters.append prm6

		Set prm7 = adocmd.createparameter("@EventID", 3, 1)
		prm7.value = EventID
		adocmd.Parameters.append prm7

		Set prm8 = adocmd.createparameter("@Conferences", 3, 1)
		prm8.value = Conferences
		adocmd.Parameters.append prm8

		Set prm9 = adocmd.createparameter("@EventAbroad", 3, 1)
		prm9.value = EventAbroad
		adocmd.Parameters.append prm9

		Set prm11 = adocmd.createparameter("@EventExam", 3, 1)
		prm11.value = EventExam
		adocmd.Parameters.append prm11

		Set prm12 = adocmd.createparameter("@online", 3, 1)
		prm12.value = online
		adocmd.Parameters.append prm12

		Set prm13 = adocmd.createparameter("@archive", 3, 1)
		prm13.value = archive
		adocmd.Parameters.append prm13

		adocmd.Execute
else
	ThisErr = "Err 2: Your date is not valid"
end if
%>
<!-- #include virtual="/ssi/dbclose.asp" -->
<%
response.redirect "default.asp?ThisErr=" & Server.UrlEncode(ThisErr)
%>