<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ICO - Website Administration </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body" onLoad="focus();">

<!-- #include virtual="/dbadmin/ssi/incl/top_pop.asp" -->

<div id="closebutton"><a href="javascript:window.close();" title="Close this window"><img src="/dbadmin/images/closewindow.gif" width="12" height="12" border="0" alt="Close"></a></div>
<%
 'Get ID number of event'
EventID=Request.QueryString("EventID")
'if it is numeric then request info from DB'
if IsNumeric(EventID) and EventID <> "" then

	set rs=SQLConnection.execute("Select e.StartDate, e.EndDate, e.Title, e.Description, e.ComCentreUserID, m.FullName from Postgraduates_EventsCalendar e , AdminUsers m Where e.ComCentreUserID=m.AdminID and e.EventID=" & EventID)
	'if we have found info then display it on the screen'
	if NOT rs.eof Then
%>
<h1 style="margin-top:0; padding-top:0"><%=DisplayText(rs(2))%></h1>
<p><% Response.write FormatDateTimeIRL(rs(0),1) & " @ " & FormatDateTimeIRL(rs(0),4) %></p>
<p><%=Replace(rs(3),"''","'") %> </p>
<p>Posted by: <a class="red14" href="JavaScript:onclick=ShowMe(<%=rs(4)%>)"> <%=DisplayText(rs(5)) %></a></p>
<hr class="hrgrey">

<p><a href="addtocal.asp?EventID=<%=EventID %>">Add this event to your calendar <img src="/dbadmin/images/addevent.gif" width="16" height="16" alt="Click me to add this event to your calendar" border="0"></a></p>
<p style="font-size:10px;color:#555;">If you use a VCAL compliant calander application such as MS Outlook then you can use this link to add this event to your calendar.</p>
    
<%
	end if
	end if
%>
<!-- #include virtual="/dbadmin/ssi/incl/base_pop.asp" -->
<!-- #include virtual="/ssi/dbclose.asp" -->
</body>
</html>
