<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->

<%
Response.ContentType = "text/x-vCalendar"
Response.AddHeader "Content-Disposition", "filename=myappointment.vcs"
EventID=Request.QueryString("EventID")
if IsNumeric(EventID) Then
	SQL="Select StartDate, Title,Description from Postgraduates_EventsCalendar where EventID=" & EventID
	Set SQL=sqlConnection.execute(SQL)
	if NOT SQL.eof Then
		ThisMonth=Month(SQL(0))
		if len(ThisMonth) < 2 Then
			ThisMonth="0" & ThisMonth
		end if
		ThisDay=Day(SQL(0))
		if len(ThisDay) < 2 Then
			ThisDay="0" & ThisDay
		end if
		ThisHour=Hour(SQL(0))
		if len(ThisHour) < 2 Then
			ThisHour="0" & ThisHour
		end if
		ThisMinute=Minute(SQL(0))
		if len(ThisMinute) < 2 Then
			ThisMinute="0" & ThisMinute
		end if
		DTSTART=Year(SQL(0)) & ThisMonth & ThisDay & "T" & ThisHour & ThisMinute & "00Z"
		DTEND=DTSTART
		SUMMARY=DisplayText(SQL(1))
		DESCRIPTION=DisplayText(SQL(2))
%>
BEGIN: VCALENDAR
VERSION: 1.0
BEGIN: VEVENT
DTSTART: <%=DTSTART %>
DTEND: <%=DTEND %>
SUMMARY: <%=SUMMARY %>
DESCRIPTION: <%=DESCRIPTION %>
URL:http://maybefriends.com/
LOCATION: Ireland
END: VEVENT
END: VCALENDAR
<%
	end if
end if
%>
<!-- #include virtual="/ssi/dbclose.asp" -->