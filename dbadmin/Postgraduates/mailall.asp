<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/mailer.asp" -->
<%
'Get event ID'
EventID=Request.Form("EventID")
'Is it for real and does it have any attendees'
if IsNumeric(EventID) Then
	sql="Select AdminID from ComsCentre_EventParticipants where EventID=" & EventID
	Set rsPartic=sqlConnection.execute(sql)
	if NOT rsPartic.eof Then
		FromID=Session("MemberID")
		Subject="A reminder about the Event you signed up for!"
		Message= Request.Form("MailContent") 
		Message=Replace((Message),chr(13),"<br>")
		do while NOT rsPartic.eof
		
					Set userdetails = "Select email from AdminUsers where AdminID=" & rsPartic(0)
					If Not userdetails.eof Then
						If InStr(userdetails,"@") > 1 Then
							mail = SendMail ("meeting-reminder@ICO.ie", userdetails(0) , Subject, Attachment, Message)
						End if
					End if
				end if

			end if
			rsPartic.MoveNext
		loop
		'Messages have been sent so return to main event page'
		Response.Redirect("default.asp?Msg=1")
	else
		'No attendees for this event'
		Response.Redirect("default.asp?Msg=3")
	end if
else
	'NonNumeric EventID specified'
	Response.Redirect("default.asp?Msg=4")
end if
%>
<!--#include virtual="/ssi/footer.asp" -->
