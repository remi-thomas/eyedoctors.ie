<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp " -->

<!--#include virtual="/ssi/dbconnect.asp" -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script language="javascript" type="text/javascript" src="/dbadmin/ssi/js/openwindow.js"></script>
<script language="javascript" type="text/javascript" src="/dbadmin/ssi/lib/openclose.js"></script>
<script language="javascript" type="text/javascript" src="/dbadmin/ssi/lib/calendar.js"></script>
<script type="text/javascript" src="/dbadmin/ssi/ckeditor/ckeditor.js"></script>
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<title>ICO - Website Administration </title>
</head>
<body class="body">

<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<%
EventID = request.querystring("EventID")
if IsNumeric(EventID) and EventID <> "" then
	Set rs = sqlconnection.execute("exec usp_SEL_PostGraduates_Board " & EventID & ",1")
	If not rs.eof then
		StartDate			= FormatDateTimeIRL(rs("StartDate"),2)
		StartTime			= FormatDateTimeIRL(rs("StartDate"),3)
		EndDate				= FormatDateTimeIRL(rs("EndDate"),2)
		EndTime				= FormatDateTimeIRL(rs("EndDate"),3)
		eventdescription	= DisplayText(rs("Description"))
		Title				= DisplayText(rs("Title"))
		Conferences			= rs("Conferences")
		EventAbroad			= rs("EventAbroad")

		EventExam			= rs("EventExam")
		Online				= rs("Online")
		Archive				= rs("Archive")
	end if
else
	StartDate	= date() + 1
	StartTime	= date() & " 20:00:00"
	Conferences	= 0
	EventAbroad	= 0

	EventExam	= 0
	Online		= 1
	Archive		= 0
end if
If not isDate(StartDate) then
	StartDate = date() + 1
	StartTime = date() & " 20:00:00"
end If
If not isDate(EndDate) then
	EndDate = date() + 1
	EndTime = date() & " 20:00:00"
end if
%>



<p>
  <%
If IsNumeric(EventID) and EventID <> "" Then
	response.Write "<h1>Edit Event</h1>" & vbnewline
	response.Write session("AdminUserName") & " you can edit your event by using the form below"
Else
	response.Write "<h1>Add new Event</h1>" & vbnewline
	response.Write "To add a new event fill out the form below:"
end if
%>
</p>

<form name="events" action="process.asp" method="post" id="newsform">
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <th scope="row">Event Name:</th>
      <td><input type="text" size="60" name="Title" class="TextInput" value="<%=Title%>" maxlength="60"/>
        <br/>
        Please note that only the first 32 characters of your event will appear on the main <a href="/members/Postgraduates/" >Events Calendar.</a></td>
    </tr>
    <tr>
      <th scope="row">Starting:</th>
      <td><select name="fday" class="TextInput" onchange="updatefdate()">
          <%
				For i = 1 to 31
					response.Write "<option value="""& i &""""
					if i = day(StartDate) then
						response.write " selected "
					end if
					response.Write ">"& i &"</option>"& vbnewline
				Next
				%>
        </select>
        <select name="fmonth" class="TextInput" onchange="updatefdate()">
          <%
				For i = 1 to 12
					response.Write "<option value="""& MonthName(i) &""""
					if i = Month(StartDate) then
						response.write " selected "
					end if
					response.Write ">"& MonthName(i) &"</option>"& vbnewline
				Next
				%>
        </select>
        <select name="fyear" class="TextInput" onchange="updatefdate()">
          <%
				For i = 0 to 2
					startyear = DateAdd ("yyyy", i, now())
					response.Write "<option value="""& year(startyear) &""""
					if year(startyear) = Year(StartDate) then
						response.write " selected "
					end if
					response.Write ">"& year(startyear) &"</option>"& vbnewline
				Next
				%>
        </select>
        <a href="JavaScript:OpenFcalendar()"><img src="/dbadmin/images/cal_icon.gif" alt="pick up a date" width="17" height="17" border="0"></a>
        <input type="hidden" name="StartDate" value="<%= FormatDateTimeIRL(StartDate,1) %>"/>

		<select name="StartTime" class="TextInput">
          <%
						For i=480 to 1410 step 30
							TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
							response.Write "<option value="""& TimVal &""""
							if FormatDateTimeIRL(StartTime,4) = TimVal then
								response.write " selected "
							end if
						response.Write ">"& TimVal &"</option>"& vbnewline

						Next
						%>
        </select>
        <em>(24hr clock!)</em>
      </td>
    </tr>

  <tr>
      <th scope="row">Ending:</th>
      <td>
	  <!-- 
	  select name="tday" class="TextInput" onchange="updateddate()">
          <%
				For i = 1 to 31
					response.Write "<option value="""& i &""""
					if i = day(EndDate) then
						response.write " selected "
					end if
					response.Write ">"& i &"</option>"& vbnewline
				Next
				%>
        </select>
        <select name="tmonth" class="TextInput" onchange="updateddate()">
          <%
				For i = 1 to 12
					response.Write "<option value="""& MonthName(i) &""""
					if i = Month(EndDate) then
						response.write " selected "
					end if
					response.Write ">"& MonthName(i) &"</option>"& vbnewline
				Next
				%>
        </select>
        <select name="tyear" class="TextInput" onchange="updateddate()">
          <%
				For i = 0 to 2
					endyear = DateAdd ("yyyy", i, now())
					response.Write "<option value="""& year(endyear) &""""
					if year(endyear) = Year(StartDate) then
						response.write " selected "
					end if
					response.Write ">"& year(endyear) &"</option>"& vbnewline
				Next
				%>
        </select>
        <a href="JavaScript:OpenTcalendar()"><img src="/dbadmin/images/cal_icon.gif" alt="pick up a date" width="17" height="17" border="0"></a>
        <input type="hidden" name="enddate" value="<%= FormatDateTimeIRL(EndDate,1) %>"/
		
		-->

		<select name="EndTime" class="TextInput">
          <%
						For i=480 to 1410 step 30
							TimVal = FormatDateTimeIRL(DateAdd("n",i,"1/1/2000 00:00"),4)
							response.Write "<option value="""& TimVal &""""
							if FormatDateTimeIRL(EndTime,4) = TimVal then
								response.write " selected "
							end if
						response.Write ">"& TimVal &"</option>"& vbnewline

						Next
						%>
        </select>
        <em>(24hr clock!)</em>
      </td>
    </tr>


    <tr>
      <th scope="row">Description:</th>
      <td>
 

        <textarea cols="65" rows="12" name="eventdescription" id="eventdescription" tabindex="6" class="TextInput" style="border:0px;"><%=eventdescription%></textarea>

       
      </td>
    </tr>


    <tr>
      <th scope="row">
	  <div>Flag as Exam: <input type="checkbox" name="EventExam" value="1" class="smalltextinput" <%If EventExam=1 Then response.write "Checked"%> /></div>
	  </th>
	<td></td>
    </tr>


    <tr>
      <th scope="row">
	  <div>Online:  <input type="checkbox" name="online" value="1" class="smalltextinput" <%If Online=1 Then response.write "Checked"%> /></div>
	  </th>
	<td></td>
    </tr>
    <tr>
      <th scope="row">
	  <div>Archive <input type="checkbox" name="archive" value="1" class="smalltextinput" <%If archive=1 Then response.write "Checked"%> /></div>
	  </th>
	<td></td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>
	  
	  <input type="hidden" name="EventID" value="<%=EventID%>"/>

        <div><input type='submit' class="TextInput" name='Submit' value='<% If EventID > 0 Then response.write "Change" Else response.write "Create"%> Event &raquo;&raquo;'/></div>
      </td>
    </tr>
  </table>
</form>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->

<script type="text/javascript">
	window.onload = function()
	{
		CKEDITOR.replace( 'eventdescription' 
		,
		{
		height:"200"
		}
		
		);
	};
</script>

</body>
</html>
