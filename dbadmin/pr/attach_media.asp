<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - Irish college of Ophthalmologists</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<body class="body" onLoad="setTimeout('CloseNUpdate()',1500)">
<%
	section = "PR"
%>
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<%
DocID		= trim(Request.Form("DocID"))
ttlRec		= trim(Request.Form("ttlRec"))
MediaType	= trim(Request.Form("MediaType"))
mypage		= trim(Request.Form("mypage"))

If (IsNumeric(ttlRec) and ttlRec <> "") AND (IsNumeric(DocID) and DocID <> "") AND (IsNumeric(MediaType) and MediaType <> "") then
	For i = 1 to ttlRec
		MediaID = Request.Form("MediaID" & i)
		If (IsNumeric(MediaID) AND MediaID <> "") then
			sql ="INSERT INTO DocMedia (DocID,MediaID,DocType,DateAdded) VALUES ("& DocID &", "& MediaID &", "& MediaType &",getdate() )"
			response.Write sql
			sqlconnection.execute(sql)
		end if
	Next
end if
%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="stretch" width="145"><img src="/images/top/01.gif" alt="Irish college of Ophthalmologists"></td>
		<td class="stretch"><img src="/images/top/02.gif" alt="Irish college of Ophthalmologists"></td>
	</tr>
</table>
	<p class="txtgold">Files are being updated... please wait </p>
 <p align="right"><a href="JavaScript:onClick=CloseNUpdate()" class="bluetext">[close window]</a></p>


<script language="JavaScript" type="text/javascript">
	function CloseNUpdate()
	{
		//alert("close")
		window.opener.location.reload();
		window.location = "pop_media.asp?mypage=<%=mypage%>&MediaType=<%=MediaType%>&DocID=<%=DocID%>"
	}
</script>
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->