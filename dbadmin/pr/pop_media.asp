<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - Irish college of Ophthalmologists</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<%
	section = "PR"
%>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="stretch" width="145"><img src="/images/top/01.gif" alt="Irish college of Ophthalmologists"></td>
		<td class="stretch"><img src="/images/top/02.gif" alt="Irish college of Ophthalmologists"></td>
	</tr>
</table>
<table>
	<tr>
<%
DocID	= Request.Querystring("DocID")
if not IsNumeric(DocID) or DocID = "" then
	response.Write "<td class=""bluetext""> Doc ID (" & DocID &") is not numeric " & IsNumeric(DocID) & _
	"<br />Close this window and start again! </td>" & VbNewline & _
	"</tr>" & VbNewline & _
	"</table>" & VbNewline
	response.end
end if


MediaType = trim(request.querystring("MediaType"))
'response.Write MediaType

If isNull(MediaType) or MediaType="" then
	MediaType = 1
end if			

set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
	Do until rs.eof 
		If rs(0) = cint(MediaType) then
			astyle= "bluetext"
		else
			astyle= "dbText"
		end if

	response.Write "		<td class=""dbText""><a href=""pop_media.asp?MediaType="& rs(0) & "&amp;DocID="& DocID &""" class="""& astyle &""">" & rs(1) & "</a></td>" & VbNewline & _
				"		<td class=""dbText"">|</td>" & vbnewline	
	rs.Movenext
	Loop
%>
	</tr>
</table>

<%
mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
cnt  = 0
' max display = nb records to be displayed' 
maxdisplay = 10

set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open "usp_SEL_MediaPerType " & MediaType & "," & DocID, sqlconnection

If rs.eof then
	response.Write "<p class=""smlblueb"">This request returns eof <br/> No media type "& MediaType &" stored in db</p>" & VbNewline 
	response.end
end if 

rec = 1
rs.movefirst
rs.pagesize = maxdisplay
rs.absolutepage = mypage
maxpages = cint(rs.pagecount)
' display the total of records returned by sql query'
intRecordCnt = rs.RecordCount

if maxpages > 1 then
	response.Write "<table width=""100%"">" & VbNewline & _
	"<tr>" & VbNewline & _
	"	<td class=""txtgold"" align=""right"">"
	call Paging
	response.Write "</td>" & VbNewline & _
	"<td>&nbsp;</td>"& VbNewline & _
	"</tr>"  & VbNewline & _
	"<tr>" & VbNewline & _
	"	<td class=""dbText""><hr noshade size=1 color=""#e5e5e5""></td>"   & VbNewline & _
	"</tr>" & VbNewline & _
	"</table>" & vbnewline
end if

%>
<form name="attach_media" method="post" action="attach_media.asp?" onSubmit="return validate_form(this);" style="margin:0;padding:0">
<table width="100%">
	<tr>
		<td class="smlblueb"><img src="/dbadmin/images/paperclip.gif" width="20" height="20" alt=""></td>
		<td class="smlblueb">Title</td>
		<td class="smlblueb">Path</td>
		<td class="smlblueb">Size</td>
		<td class="smlblueb">Online</td>
		<%
			if MediaType = 1 then
				response.Write "<td class=""smlblueb"" width=""160"">Preview</td>" & vbnewline
			else
				response.Write "<td>"
			end if
		%>
	</tr>
	<%
	Do until rs.eof  or (rec > maxdisplay)
	%>
	<tr>
		<td class="dbText" valign="top">
				<%
				'media already linked'
				if rs(8)=1 then
				%>
					 <a href="remove_media.asp?page=popmedia&amp;mypage=<%=mypage%>&amp;DocID=<%=DocID%>&amp;MediaType=<%=MediaType%>&amp;MediaID=<%=rs(0)%>" class="dbText" onclick="return confirm('Are you sure you want to remove this media from this PR?');"><img src="/dbadmin/images/delete.gif" alt="unattach this medis"  border="0" width="15" height="15" /></a>
				<%
				else
				%>
					 <input type="checkbox" class="TextInput" name="MediaID<%=rec%>"  value="<%=rs(0)%>">
				<%
				end if 'media already linked'
				%>
			</td>
			<td class="dbText" valign="top">
				<%=TrimTitle(DisplayText(rs(5)),80)%>
			</td>
			<td class="dbText" valign="top">
				<% response.write rs(1) & rs(2) %>
			</td>
			<td class="dbText" valign="top">
				<%=DisplayText(rs(4))%> KO
			</td>
			<td class="dbText" valign="top">
				<%
					if rs(7) = 1 then
						response.Write "<img src=""/dbadmin/images/tick.gif"" width=""10"" alt=""online"">"
					else
						response.Write "<img src=""/dbadmin/images/cross.gif"" height=""10"" alt=""offline"">"
					end if				
				%>
			</td>
			<td class="dbText" valign="top">
			<% 
			if Len(rs(3)) > 4 then
			%>		
			<a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="click to view image">+/- view</a>
				<span id="field<%=rec%>" style="display:none;margin:0">
					<br/><img src="<%response.write rs(1) & rs(3)%>" alt="Media" style="margin:0">
				</span>
			<%end if%>
			</td>
		</tr>
	<%
	rs.movenext
	rec = rec + 1
loop
	%>
	<tr>
		<td colspan="6">
		<input type="hidden" name="mypage" value="<%=mypage%>">
		<input type="hidden" name="MediaType" value="<%=MediaType%>">
		<input type="hidden" name="ttlRec" value="<%=rec%>">
		<input type="hidden" name="DocID" value="<%=DocID%>">
		<input type="submit" value="Attach Selected Media" class="dbButton" tabindex="6" label=""></td></td>
	</tr>
</table>
</form>
	<%
if maxpages > 1 then
	response.Write "<table width=""100%"">" & VbNewline & _
	"<tr>" & VbNewline & _
	"	<td class=""dbText""><hr noshade size=1 color=""#e5e5e5""></td>"   & VbNewline & _
	"</tr>" & VbNewline & _
	"<tr>" & VbNewline & _
	"	<td class=""txtgold"" align=""right"">"
	call Paging
	response.Write "</td>" & VbNewline & _
	"<td>&nbsp;</td>"& VbNewline & _
	"</tr>"  & VbNewline & _
	"</table>" & vbnewline
end if



rs.close
set rs=nothing


sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a class=""dbText"" href=""pop_media.asp?mypage=" & counter & "&amp;DocID=" & DocID
				If MediaType <> "" Then
						response.write "&amp;MediaType=" & MediaType
				End if
				Response.Write """>" & counter & "</a>"
			else
				Response.Write "<i class='dbText'>" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
	end if
end sub
%>
<script language="JavaScript" type="text/javascript">
//########################################
//## script copyright fusio.net         ##
//## developped for Dub Theatre IRL ##
//## by remi [at] fusio<.>net           ##
//########################################
function ShowMe(ThisLayer) 
{ 
	// expand or contract 
	document.getElementById(ThisLayer).style.display = (document.getElementById(ThisLayer).style.display == 'block') ? 'none' : 'block';
	cvalue = (document.getElementById(ThisLayer).style.display == 'block') ? 1 : 2;
}

function validate_form(thisform)
{
	var nbchkbox = 0;
	for (Count = 0; Count < document.attach_media.elements.length; Count++) 
	{
		if (attach_media[Count].checked) nbchkbox++;
	}
	if (nbchkbox == 0) 
	{
	alert("No media have been selected");
	return false;
	}
}

function CloseNUpdate()
	{
		//alert("close")
		window.opener.location.reload();
		window.close()
	}
</script>

 <p align="right"><a href="JavaScript:onClick=CloseNUpdate()" class="dbText">[close window]</a></p>
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->
