<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>PR - Irish college of Ophthalmologists - Web Administration Section</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script language="JavaScript1.2" src="/ssi/js/openwindow.js" TYPE="text/javascript"></script>
</head>
<%
section = "PR"

' include at the top '
mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
cnt  = 0
' max display = nb records to be displayed' 
maxdisplay = 30
%>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>Press Releases</h1>



<%
sql = "exec usp_SEL_PR NULL,1" 
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open Sql , sqlConnection


if NOT rs.eof THEN
	cntM=0
	rec = 1
	rs.movefirst
	rs.pagesize = maxdisplay
	rs.absolutepage = mypage
	maxpages = cint(rs.pagecount)
	' display the total of records returned by sql query'
	intRecordCnt = rs.RecordCount
%>
	<br/>
	<table cellpadding="1" cellspacing="1" border="0" width="98%" class="whiteborder">
		<tr>
			<td class="bluetext" bgcolor="#BBD9F0"><b>AUTHOR</b></td>
			<td class="bluetext" bgcolor="#BBD9F0"><b>MEDIA</b></td>
			<td class="bluetext" bgcolor="#BBD9F0"><b>SUBJECT</b></td>
			<td class="bluetext" bgcolor="#BBD9F0" align="center"><b>PUBLICATION DATE</b></td>
			<td class="bluetext" bgcolor="#BBD9F0" align="center"><b>EMBARGO DATE</b></td>
			<td class="bluetext" bgcolor="#BBD9F0" align="center"><img src="/dbadmin/images/icon_online.gif" width="20" height="20" /></td>
			<td class="bluetext" bgcolor="#BBD9F0"> &nbsp;</td>
			<td class="bluetext" bgcolor="#BBD9F0">&nbsp;</td>
		</tr>
		<%
			cnt = 0
			do while not rs.eof and cnt<maxdisplay
			cnt = cnt+1
			If cnt mod 2 = 0 then
				BgColor = "#D6E8F5"
			else	
				BgColor = "#E4F1FA"
			end if
		%>
		<tr>
			<td class="bluetext" valign="top" bgcolor="<%=BgColor%>"><%=rs("Author")%></td>
			<td class="bluetext" align="left" valign="top" bgcolor="<%=BgColor%>">
		<%
			if rs("Media") > 0 then
				response.Write "<a href=""JavaScript:openWindow('/dbadmin/media/list.asp?SupportID="& rs("PrID") &"&amp;section=PR',700,400)"">"
				response.write DrawAttachedMedia(rs("PrID"),section)
				response.Write "</a>"
			else
		%>
			<a href="JavaScript:openwin('/dbadmin/ssi/AttachMedia/pop_media.asp?section=PR&SupportID=<%=rs("PrID")%>','browse','scrollbars=yes,resizable=yes,left=10,top=10,screenX=10,screenY=10,width=750,height=560,innerHeight=0,innerWidth=0,outerHeight=0,outerWidth=0')" class="bluetext">Add Media</a>
		<%
			end if
		%>
			</td>
			<td class="bluetext" valign="top" bgcolor="<%=BgColor%>"><%=trimtitle(DisplayText(rs("Subject")),88)%></td>
			<td class="bluetext" align="center" valign="top" bgcolor="<%=BgColor%>"><%=FormatDateTimeIRL(rs("PublicationDate"),1)%></td>
			<td class="bluetext" align="center" valign="top" bgcolor="<%=BgColor%>">
				<%
				response.write FormatDateTimeIRL(rs("PublicationDate"),1) & " " & FormatDateTimeIRL(rs("EmbargoDate"),4)
				%>
			</td>
			<td class="bluetext" align="center" valign="top" bgcolor="<%=BgColor%>">
				<%
				'online / offline ?? '
				if rs("online")=1 Then
					OnlineImg="tick.gif"
					AltTxt="This file is available for viewing if its embargo date has been reached - Click here to put it offline"
				else
					OnlineImg="cross.gif"
					AltTxt="This file is offline, it cannot be viewed on the public website - Click here to put it online"
				end if	
				response.Write "<a href='switchstatus.asp?PrID=" & rs("PrID") & "&amp;mypage="& mypage&"'><img src=""/dbadmin/images/" & OnlineImg & """ title=""" & AltTxt &""" width=""10"" height=""10"" border=""0"" class='txtblack'></a>"
				%>
			</td>
			<td class="bluetext" valign="top" bgcolor="<%=BgColor%>"><a href="/dbadmin/pr/wysiwyg/editor.asp?ID=<%=rs("PrID")%>" class="bluetext" title="Edit this PR in a WYSIWYG editor"><img src="/dbadmin/images/wysiwyg_edit.gif" alt="Edit this PR in a WYSIWYG editor" border="0" width="46" height="20" /></a><a href="edit.asp?ID=<%=rs("PrID")%>" class="bluetext" title="Edit this PR (raw source)"><img src="/dbadmin/images/code_edit.gif" alt="Edit this PR (raw source)" border="0" width="28" height="20" /></a></td>
			<td class="bluetext" valign="top" bgcolor="<%=BgColor%>"><a href="delete.asp?prID=<%=rs("PrID")%>&redirURL=<%=server.urlencode(ThisPage)%>" class="bluetext" onclick="return confirm('Are you sure you want PERMANENTLY remove this Press Release\n* Once performed, this action cannot be reversed!!');"><img src="/dbadmin/images/delete.gif" alt="Delete this PR"  border="0" width="13" height="16" /></a></td>
		</tr>
		<%
			rs.MoveNext
			Loop
		%>
	</table>
		<%
			else
				response.Write "<p class=""bluetext"">There are no current PRs in the database"
				
			end if
			set rs = nothing


			if maxpages > 1 then
				call Paging
			else
			response.write "&nbsp;"
			end if
		%>
<br/>&nbsp;
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "<span class=""bluetext"">Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a class=""bluetext"" href=""default.asp?mypage=" & counter &""">" & _
				counter & _
				"</a>"
			else
				Response.Write "<i class='bluetext02'>" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
		Response.Write "</span>"
	end if
end sub
%>
