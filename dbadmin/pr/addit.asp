<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/publish_function.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp" -->
<%
section = "PR"

Server.ScriptTimeout  = 1000 

content		= request.form("content")
prID		= Request.Form("prID")
EmbargoDate	= Request.Form("section")
author		= ValidateText(Request.Form("author"))
subject		= left(ValidateText(Request.Form("subject")),799)
heading		= left(ValidateText(Request.Form("heading")),999)
EmbargoDate	= Request.Form("EmbargoDate")
mypath		= Request.Form("mypath")

PubDay		= Request.Form("PubDay")
PubMonth	= Request.Form("PubMonth")
PubYear		= Request.Form("PubYear")

PublicationDate = PubDay & " " & PubMonth & " " & PubYear & " 00:00"
if not IsNull(PublicationDate) then
	if IsDate(PublicationDate) then
		PublicationDate = FormatDateTimeIRL(PublicationDate,1)
	else
		PublicationDate =  FormatDateTimeIRL(now,1)
	end if
else
	PublicationDate =  FormatDateTimeIRL(now,1)
end if

EmbDay		= Request.Form("EmbDay")
EmbMonth	= Request.Form("EmbMonth")
EmbYear		= Request.Form("EmbYear")
EmbTime		= Request.Form("EmbTime")

EmbargoDate = EmbDay & " " & EmbMonth & " " & EmbYear & " " & EmbTime

if not IsNull(EmbargoDate) then
	if IsDate(EmbargoDate) then
		EmbargoDate = FormatDateTimeIRL(EmbargoDate,1) & " " & FormatDateTimeIRL(EmbargoDate,4)
	else
		EmbargoDate = now()
	end if
else
	EmbargoDate = now()
end if

if len(content) > 0 then
	Content = content
end if

if IsNull(subject) or IsNull(heading) then
	response.Write "ERROR"
	response.Write "<br> subject " & IsNull(subject)	
	response.Write "<br> heading " & IsNull(heading)	
	response.end
end if

if not IsNull(subject) and not IsNull(heading) then

	if len(content) > 7990 then
		FileDirectory = "/media/pr/"
		'path already known : file exist'
		if len(mypath) > 5 then
			FileName = GetFileFromPath(mypath)
		else
			'is this file exist?'
			'will be the case when edit content has added text and over 8000 - '
			'content will then be written to a file and removed from db'
			if IsNumeric(prID) and prID <> "" then
				FileName = prID & "_" & left(replace(replace(subject,"'","")," ",""),15) & ".htm"
				call create_file(FileName,FileDirectory)
				mypath = FileDirectory & FileName
			else
				set newid = sqlconnection.execute("select top 1 ID from PR order by ID desc")
				FileName = (newid(0) + 1) & "_" & left(replace(replace(subject,"'","")," ",""),15) & ".htm"
				call create_file(FileName,FileDirectory)
				mypath = FileDirectory & FileName
			end if
		end if
				
		'write content into a page'
		if ThisFileExists(mypath) then
			response.Write "<br>mypath " & mypath & "<hr>"
			response.Write "<br>file name " & FileName & "<br>dir " & FileDirectory & "<hr>"
			
			call Write_Page(FileName, content, FileDirectory)

		else
			response.Write "<p class=""redtext"">This file stored at &laquo;" & mypath & "&raquo; can not be found!<br/>Content can not be written</p>"
			Response.end
		end if

		'reset content var to blank'
		content = ""
	else
		FileLocation = ""
	end if


	'update press release'
'update / add press release'
	Set cmd  = Server.CreateObject("ADODB.Command")

	Set prmPRID = cmd.createparameter("@PRID", 3, 1)
	prmPRID.value = prID
	cmd.Parameters.append prmPRID

	Set prmSubject = cmd.createparameter("@Subject", 200, 1, 800)
	prmSubject.value = subject
	cmd.Parameters.append prmSubject

	Set prmAuthor = cmd.createparameter("@Author", 200, 1, 250)
	prmAuthor.value = author
	cmd.Parameters.append prmAuthor

	Set prmHeader = cmd.createparameter("@Header", 200, 1, 1000)
	prmHeader.value = heading
	cmd.Parameters.append prmHeader

	Set prmContent = cmd.createparameter("@Content", 201, 1, 8000)
	prmContent.value = content
	cmd.Parameters.append prmContent

	Set FileLocation = cmd.createparameter("@FileLocation", 200, 1, 200)
	FileLocation.value = mypath
	cmd.Parameters.append FileLocation

	Set pubDate = cmd.createparameter("@PublicationDate", 7, 1)
	pubDate.value = PublicationDate
	cmd.Parameters.append pubDate

	Set embDate = cmd.createparameter("@EmbargoDate", 7, 1)
	embDate.value = EmbargoDate
	cmd.Parameters.append embDate

	Set AdminID = cmd.createparameter("@AdminID", 3, 1)
	AdminID.value = Session("AdminID")
	cmd.Parameters.append AdminID


	cmd.ActiveConnection = sqlConnection
	cmd.CommandText = "usp_UPD_PR"
	cmd.CommandType = 4
	cmd.Execute
else
	ErrorCode = 1
end if

if IsNull(ErrorCode) or ErrorCode ="" then
	response.redirect "default.asp?added=1"
else
	form_code = "<input type='hidden' name='section' value="& section &">"
	form_code = form_code &"<input type='hidden' name='author' value="& chr(34) & author & chr(34) &">"
	form_code = form_code &"<input type='hidden' name='subject' value="& chr(34) & subject & chr(34) &">"
	form_code = form_code &"<input type='hidden' name='heading' value="& chr(34) & heading & chr(34) &">"
	form_code = form_code &"<input type='hidden' name='content' value="& chr(34) & content & chr(34) &">"
	form_code = form_code &"<input type='hidden' name='error' value="& chr(34) & ErrorCode & chr(34) &">"

	response.write "<form action='add.asp' method='post' name='thisForm'>"& form_code &"</form>"
	%>
	<script language='JavaScript'>
	<!--
		document.thisForm.submit();
	//-->
	</script>
	<%
end if
%>
<!--#include virtual="/ssi/dbclose.asp" -->