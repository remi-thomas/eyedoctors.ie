<!--#include virtual="/ssi/dbconnect.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/ssi/fct/media.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" language="javascript" src="/ssi/lib/menu_nav.js"></script>
<script language="JavaScript" type="text/javascript">
var menuitem='<%=m%>';
</script>
<%
' include at the top '
PrID = request.querystring("PrID")
if Not IsNumeric(PrID) or PrID ="" then
	PrID=1
end if
set pr = sqlconnection.execute("exec usp_SEL_PR "& prID &",0")

if NOT pr.eof THEN
	If len(pr(2)) > 5 then
		header =   " - " & TrimTitle(DisplayText(pr(2)),50)
	end if
	TitlePage = DisplayText(pr(1)) & header & " - " & FormatDateTimeIRL(pr(7),1)
else
	TitlePage = "Latest Press Releases - Irish college of Ophthalmologists " & FormatDateTimeIRL(now(),1)
end if
%>
<title><%=TitlePage%></title>
<link href="/styles/web.css" rel="stylesheet" type="text/css" />
</head>

<BODY class="bodybits" onload="init();">
<!--#include virtual="/ssi/incl/top.asp"-->
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="134" class="navback" valign="top">
			<!--#include virtual="/ssi/incl/nav.asp"-->
		</td>
		<td width="5%">&nbsp;</td>
		<td valign="top">
			<img src="/images/general/trans.gif" alt="" width="1" height="17" /><br />
			<span class="pagetitle">PRESS RELEASES</span><br />
			<br />
			<br />
			<%
			if NOT pr.eof then
				response.Write "<table width=""96%"">" & VbNewline & _
				"	<tr>" & VbNewline & _
					"		<td valign=""top"" >"& FormatDateTimeIRL(pr(7),1)&" - <b >" & DisplayText(pr(1)) & "</b>"
					
					If len(pr(4)) > 2 then
						response.Write "<br />By: " & DisplayText(pr(4))
					end if

					response.Write"</td>" & VbNewline & _
					"	<tr>" & VbNewline & _
					"		<td valign=""top"" >" & VbNewline
						if len(pr(3)) < 5 then
							response.write "<br/>" & DisplayText(pr(2))
						else
							response.write "<br/>" & DisplayText(pr(3))
						end if
		
					if pr(9) > 0 then
						response.write DrawMediabyPR(prID)
					end if

					response.Write "</td>" & VbNewline & _
					"</tr>" & VbNewline & _
				"</table>" & vbnewline

			else
				response.Write "<p >There are no current press releases available</p>"
			end if'pr eof
			set pr = nothing
		

			if maxpages > 1 then
				call Paging
			else
				response.write "&nbsp;"
			end if
			%>
		</td>
	</tr>
</table>
<!--#include virtual="/ssi/incl/bottom.asp"-->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
