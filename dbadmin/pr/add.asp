<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Home - Irish college of Ophthalmologists - Web Administration Section</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<%
section = "PR"


error	= Request.Form("error")
if not IsNull(error) then
	prID	= Request.Form("ID")
	author	= Request.Form("author")
	subject		= left(ValidateText(Request.Form("subject")),799)
	heading		= left(ValidateText(Request.Form("heading")),999)
	content	= DisplayText(Request.Form("content"))

	if error=1 then
		if IsNull(author) then v_author="required field"
		if IsNull(subject) then v_subject="required field"
		if IsNull(heading) then v_heading="required field"
		if IsNull(content) then v_content="required field"
	end if
end if

if IsNull(author) or author = "" then
	author = session("AdminUserName")
end if

PublicationDate = date()
EmbargoDate		= date()

page_content = "Please complete the form below to add a new Press Release to the Irish college of Ophthalmologists website"
%>
<script language="JavaScript">
<!--
function f_validate(f) {
	var validForm = true;
	var thisE;

	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			thisE = eval("f.v_"+ f.elements(i).name);

			if (f.elements(i).value == "") {
				thisE.value = f.elements(i).label;
				f.elements(i).select();
				validForm = false;
			}
			else {
				thisE.value = "";
			}
		}
	}

	if (!validForm) {
		alert ('Please complete all required fields');
		return false;
	}
	else {
		return true;
	}
}

function f_checker(f, myElement) {
	var thisE;
	for (i=0;i<f.length;i++) {
		if ((f.elements(i).type == 'text' || f.elements(i).type == 'textarea') && (f.elements(i).label != '')) {
			if (f.elements(i).name != myElement) {
				thisE = eval("f.v_"+ f.elements(i).name);

				if (f.elements(i).value == "") {
					thisE.value = f.elements(i).label;
					return false;
				}
				else {
					thisE.value = "";
				}
			}
			else {
				return false;
			}
		}
	}
}
//-->
</script>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->

<p class="bluetext"><%=page_content%></p>


<form action="addit.asp" method="post" onsubmit="return f_validate(this);">
<table cellpadding="3" cellspacing="0" border="0">
<tr>
	<td class="bluetext">Author</td>
	<td class="bluetext">:</td>
	<td class="bluetext"><input type="text" name="author" class="TextInput" size="85" label="" tabindex="2" onfocus="f_checker(this.form, this.name);" value="<%=author%>"></td>
	<td class="bluetext"><input type="text" name="v_author" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="12"></td>
</tr>
<tr>
	<td class="bluetext">Subject</td>
	<td class="bluetext">:</td>
	<td class="bluetext"><input type="text" name="subject" value="<%=subject%>" class="TextInput" size="85" label="required field" tabindex="3" onfocus="f_checker(this.form, this.name);"></td>
	<td class="bluetext"><input type="text" name="v_subject" value="<%=v_subject%>" class="dbHidden" size="15" label="" tabindex="11"></td>
</tr>
<tr>
	<td class="bluetext">Heading</td>
	<td class="bluetext">:</td>
	<td class="bluetext"><input type="text" name="heading" class="TextInput" size="85" label="required field" tabindex="4" onfocus="f_checker(this.form, this.name);"></td>
	<td class="bluetext"><input type="text" name="v_heading" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="13"></td>
</tr>
<tr valign="top">
	<td class="bluetext">Press Release Content</td>
	<td class="bluetext">:</td>
	<td class="bluetext"><textarea name="content" class="TextInput" cols="87" rows="10" label="required field" tabindex="5" onfocus="f_checker(this.form, this.name);"><%=content%></textarea></td>
	<td class="bluetext"><input type="text" name="v_content" value="<%=v_author%>" class="dbHidden" size="15" label="" tabindex="14"></td>
</tr>
<tr>
	<td class="bluetext">Publication Date</td>
	<td class="bluetext">:</td>
	<td class="bluetext">
	<%
		response.Write "<select name=""PubDay"" class=""TextInput"">" & vbnewline
		For i=1 to 31
			Response.Write "<option"
				if Cint(Day(PublicationDate))=i Then Response.Write " selected"              		
			Response.Write ">" & i & "</option>"
		Next
		response.Write "</select>" & VbNewline & _
		"<select name=""PubMonth"" class=""TextInput"">" & vbnewline
		For i=1 to 12
		Response.Write "<option"
			if Cint(Month(PublicationDate))=i Then Response.Write " selected"
		Response.Write ">" & MonthName(i) & "</option>"
		Next
		response.Write "</select>" & VbNewline & _
		"<select name=""PubYear"" class=""TextInput"">" & vbnewline
		For i=1990 to Cint(Year(now()))
		Response.Write "<option"
			if Cint(Year(PublicationDate))=i Then Response.Write " selected"
		Response.Write ">" & i & "</option>"
		Next
		response.Write "</select>" & VbNewline
	%>
	</td>
	<td class="bluetext"></td>
		</tr>
			<td class="bluetext">Embargo Date</td>
			<td class="bluetext">:</td>
			<td class="bluetext">
			<%
			response.Write "<select name=""EmbDay"" class=""TextInput"">" & vbnewline
			For i=1 to 31
				Response.Write "<option"
					if Cint(Day(EmbargoDate))=i Then Response.Write " selected"
                Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbMonth"" class=""TextInput"">" & vbnewline
			For i=1 to 12
				Response.Write "<option"
					if Cint(Month(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & MonthName(i) & "</option>"
			Next
			response.Write "</select>" & VbNewline & _
			"<select name=""EmbYear"" class=""TextInput"">" & vbnewline
			For i=1990 to Cint(Year(now()))
				Response.Write "<option"
					if Cint(Year(EmbargoDate))=i Then Response.Write " selected"
				Response.Write ">" & i & "</option>"
			Next
			response.Write "</select>" & VbNewline
			'time'
			'@@ get the time previosuly selected, if not set to Noon'
			response.Write VbNewline & _
			"<select name=""EmbTime"" class=""TextInput"">" & vbnewline
			For i=0 to 1440 step 30
				TimeOption = FormatDateTimeIRL(DateAdd ("n", i, "1 january 2000 00:00"),4)
				Response.Write "<option"
					if trim(EmbargoTime) = trim(TimeOption) Then 
						Response.Write " selected"
					end if
				Response.Write ">" & TimeOption & "</option>"
			Next
				response.Write "</select>" & VbNewline
			%>
			</td>
			<td class="bluetext"></td>
		</tr>
<tr>
	<td colspan="2">&nbsp;</td>
	<td class="bluetext">&nbsp;&nbsp;<input type="submit" value="Add Press Release" class="TextInput" tabindex="6" label="" onfocus="f_checker(this.form, this.name);"></td>
</tr>
</table>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp" -->