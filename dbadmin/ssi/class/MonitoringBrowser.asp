<%
Class DirectoryBrowser
	Private ThisFileType

	Private tmp1,tmp2,tmp3,sqlConnection,rec,BgColor

	Private Function ThisPage()
		dim t1

		t1 = Request.ServerVariables("SCRIPT_NAME")
		ThisPage = "./" & Right(t1, Len(t1) - InStrRev(t1, "/"))
	End Function

	Public Property Let Path(byVal sPath)
		tmp1 = sPath
	End Property

	Public Property Get Path()
		If tmp1 <> "" Then Path = tmp1 Else Path = "/dbadmin/documents/"
	End Property

	Public Property Let RestrictBrowsing(byVal bRestrict)
		tmp2 = bRestrict
	End Property

	Public Property Get RestrictBrowsing()
		If tmp2 <> "" Then
			tmp2 = CBool(tmp2)
			RestrictBrowsing = tmp2
		Else
			RestrictBrowsing = true
		End If
	End Property

	Public Property Let Sort(byVal st)
		tmp3 = st
	End Property

	Public Property Get Sort()
		If tmp3 <> "" Then Sort = tmp3 Else Sort = "Name ASC"
	End Property

	Public Default Sub GetPath()
		Dim sBaseBrowseDir

		'if you want to change the restricted directory,
		'change the next line - this is only active if
		'a programmer set the RestrictBrowsing property
		'to true.
		sBaseBrowseDir = "/"
		If RestrictBrowsing Then
			If InStr(LCase(Path), sBaseBrowseDir) = 0 Then
				Path = sBaseBrowseDir
			End If


			'------------------------------------------
			' SECURITY HOLE '
			' a path containing a ../ sequence
			' would allow you to bypass the RestrictBrowsing
			' property. For example:
			' a path like:
			' directoryBrowser2.asp?folder=/aspemporium/examples/../
			' allows you to see /aspemporium/

			' This security hole is fixed with the following statement:
			If InStr(LCase(Path), "../") <> 0 Then
				Path = sBaseBrowseDir
			End If
			'------------------------------------------
		End If
		'Call the DisplayCurrentdirectory procedure
		'that does all the work.
		Call DisplayCurrentdirectory()
	End Sub

	Private Sub DisplayCurrentdirectory()
		Dim objDirFSO, objCurrentdir
		Dim objDirFile, objDirSubFolder
		Dim iCurrentFolder, oRs

		iCurrentFolder = Path
		Set objDirFSO = _
			Server.CreateObject("scripting.filesystemobject")
		'retrieve folder
		Set objCurrentdir = objDirFSO.GetFolder( _
			server.mappath(iCurrentFolder))

		'Use the recordset object to emulate a table.
		'That way we can use the enhanced sorting and
		'searching features of SQL, without the overhead
		'of a database system that stores useless info
		'like this.
		Set oRs = CreateObject("ADODB.Recordset")

		'create some fields for the custom recordset:
		'the first argument is fieldName, the second is
		'a constant representing the data type of the
		'field. The ones I use are below:
		' +-----------+-------+----------------------+
		' | CONSTANT  | VALUE |     EQUIVALENCY      |
		' +-----------+-------+----------------------+
		' +-----------+-------+----------------------+
		' | AdInteger |  3    | Number(Access)       |
		' |           |       | Int(SQL)             |
		' +-----------+-------+----------------------+
		' | AdVarChar |  200  | Text(Access)         |
		' |           |       | VarChar(SQL)         |
		' +-----------+-------+----------------------+
		' | AdDBDate  |  133  | Date/Time(Access)    |
		' |           |       | SmallDateTime(SQL)   |
		' +-----------+-------+----------------------+
		' The third argument is a char argument.
		' for example, a type of 200 and a char argument
		' of 50 would be just like saying VarChar(50) in
		' a CREATE TABLE statement.
		oRs.Fields.Append "ID", 3
		oRs.Fields.Append "Name", 200, 100
		oRs.Fields.Append "Type", 200, 100
		oRs.Fields.Append "Size", 3
		oRs.Fields.Append "LastModified", 133
		oRs.Fields.Append "Path", 200, 255

		'once you open the db with custom recordsets,
		'the second you call the Close method, all
		'of the added data and the table structure
		'will be lost...
		oRs.Open

		'loop through the files collection
		For Each objDirFile in objCurrentdir.Files
			'add each file in the chosen directory
			'to the custom recordset object.

			'call addnew
			oRs.AddNew
			'fill in each field
			oRs.Fields("Name").Value = objDirFile.Name
			oRs.Fields("Type").Value = objDirFile.Type
			oRs.Fields("Size").Value = Round(objDirFile.Size / 1024, 1)
			oRs.Fields("LastModified").Value = objDirFile.DateLastModified
			oRs.Fields("Path").Value = UnMappath(objDirFile.Path)
			'call update method to add the custom recordset
			oRs.Update
		Next

		'loop through the subfolders collection
		For Each objDirSubFolder in objCurrentdir.SubFolders
			'add each folder to the custom recordset
			'object

			oRs.AddNew
			oRs.Fields("Name").Value = objDirSubFolder.Name
			oRs.Fields("Type").Value = "&lt;Folder&gt;"
			oRs.Fields("Size").Value = Round(objDirSubFolder.Size / 1024, 1)
			oRs.Fields("LastModified").Value = objDirSubFolder.DateLastModified
			oRs.Fields("Path").Value = UnMappath(objDirSubFolder.Path)
			oRs.Update
		Next

		'Release FSO object
		Set objDirFSO = Nothing
		Set objCurrentdir = Nothing

		AppendDisplay DirectoryOption


		'start the browser display...

		AppendDisplay "<table width='100%' cellspacing='0' cellpadding='0' border='0' align='center' id='doctable' >"
		AppendDisplay	"<tr>"
		AppendDisplay		"<th width=""300"" colspan=""2"">NAME &nbsp;" & _
								"<CODE>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Name ASC") & "'><img src=""/dbadmin/images/orderAsc.gif"" alt=""Order by name Ascending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Name DESC") & "'><img src=""/dbadmin/images/orderDsc.gif"" alt=""Order Descending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								"</CODE>" & _
							"</th>" & vbnewline
		AppendDisplay		"<th width='200'>TYPE &nbsp;" & _
								"<CODE>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Type ASC") & "'><img src=""/dbadmin/images/orderAsc.gif"" alt=""Order Ascending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Type DESC") & "'><img src=""/dbadmin/images/orderDsc.gif"" alt=""Order Descending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								"</CODE>" & _
							"</th>" & vbnewline
		AppendDisplay		"<th width='100'>SIZE &nbsp;" & _
							"<CODE>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Size ASC") & "'><img src=""/dbadmin/images/orderAsc.gif"" alt=""Order Ascending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								" <a bgcolor=""#d4e0e0"" href='" & ThisPage & 	"?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("Size DESC") & "'><img src=""/dbadmin/images/orderDsc.gif"" alt=""Order Descending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								"</CODE>" & _
							"</th>" & vbnewline
		AppendDisplay		"<th>LAST MODIFIED &nbsp;" & _
								"<CODE>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("LastModified ASC") & "'><img src=""/dbadmin/images/orderAsc.gif"" alt=""Order Ascending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								" <a href='" & ThisPage & "?folder=" & Server.URLEncode(Path) & "&sort=" & Server.URLEncode("LastModified DESC") & "'><img src=""/dbadmin/images/orderDsc.gif"" alt=""Order Descending"" width=""10"" height=""11"" border=""0"" /></a>" & _
								"</CODE>" & _
							"</th>" & vbnewline
		AppendDisplay "</tr>" & vbnewline

		'prepare to search the stuff we entered into the
		'custom recordset.
		'As long as we don't call close, the data is in
		'memory and we can display or work with it.


		If Not oRs.BOF Then


			'Sort based on the entered property
			oRs.Sort = Sort

			'call movefirst method to start at the first
			'record. you could also call movelast to start
			'at the last record.
			oRs.MoveFirst

			'loop through all the records and continue creating
			'the browser display.
			while not oRS.EOF

				rec  = rec  + 1
				If rec mod 2 = 0 then
					BgColor = "#ecf2f2"
				else
					BgColor = "#deeaea"
				end if

				'*if* for files or folder to ignore '
				If lcase(Right(oRs.Fields("Name").Value, 3)) = "htm" OR _
					lcase(Right(oRs.Fields("Name").Value, 4)) = "html" OR _
					lcase(Right(oRs.Fields("Name").Value, 3)) = "asp" OR _
					lcase(Right(oRs.Fields("Name").Value, 3)) = "pdf" OR _
					lcase(Right(oRs.Fields("Name").Value, 3)) = "doc" OR _
					lcase(Right(oRs.Fields("Name").Value, 3)) = "xls" OR _
					lcase(Right(oRs.Fields("Name").Value, 3)) = "txt" OR _
					(oRs.Fields("Type").Value = "&lt;Folder&gt;"  AND _
					(lcase(oRs.Fields("Name").Value) <> "dbadmin" AND _
					lcase(oRs.Fields("Name").Value) <> "ssi" AND _
					lcase(oRs.Fields("Name").Value) <> "styles" AND _
					lcase(oRs.Fields("Name").Value) <> "docimages" AND _
					lcase(oRs.Fields("Name").Value) <> "navelements" AND _
					lcase(oRs.Fields("Name").Value) <> "images" )) then

					AppendDisplay "<tr>"
					AppendDisplay "<td bgcolor="""& BgColor &""" class=""dbtext"" width=""20"" valign=""top"">"
					' folder '
					If oRs.Fields("Type").Value = "&lt;Folder&gt;" Then
						AppendDisplay "<img src=""/images/media/folder.gif"" width=""20"" height=""20""></td>" & VbNewline & _
						"<td width=""280"" bgcolor="""& BgColor &""" class=""dbtext"">"
						AppendDisplay "&lt; <b><a class='dbtext ' href='" & ThisPage & "?folder=" & Server.URLEncode(oRs.Fields("Path").Value) & "'>" & _
						oRs.Fields("Name").Value & "</a></b> &gt;"
					Else
					' file '
						AppendDisplay "<img src=""/images/media/"& lcase(Right(oRs.Fields("Name").Value, 3)) &".gif""></td>" & VbNewline & _
						"<td width=""280""  bgcolor="""& BgColor &""" class=""dbtext"">"
						AppendDisplay "<a class=""dbtext"" href='/dbadmin/docpublication/wysiwyg/editor.asp?DoF_File=" & Server.URLEncode(oRs.Fields("Path").Value) & "&file=" & Server.URLEncode(oRs.Fields("Name").Value) &"'>"
						AppendDisplay oRs.Fields("Name").Value
						AppendDisplay "</a>"
						'AppendDisplay "</a><a class=""dbtext"" href='/dbadmin/docpublication/wysiwyg/editor.asp?DoF_File=" & Server.URLEncode(oRs.Fields("Path").Value) & "&file=" & Server.URLEncode(oRs.Fields("Name").Value) &"'>" & _
						'	"<img src='/dbadmin/images/wysiwyg_edit.gif' border='0'>" & _
						'	" EDIT </a>"
					End If
					AppendDisplay "</td>"
					AppendDisplay "<td bgcolor="""& BgColor &""" class=""dbtext"">"
					ThisFileType=Replace(oRs.Fields("Type").Value,"Microsoft HTML Document 5.0","HTML")
					AppendDisplay ThisFileType
					AppendDisplay "</td>"
					AppendDisplay "<td bgcolor="""& BgColor &""" class=""dbtext"">"
					AppendDisplay oRs.Fields("Size").Value
					AppendDisplay " Kb. </td>"
					AppendDisplay "<td bgcolor="""& BgColor &""" class=""dbtext"">"
					AppendDisplay oRs.Fields("LastModified").Value
					AppendDisplay "</td>"
					AppendDisplay "</tr>"

				end if
				oRs.MoveNext
			wend
		End If

		'close the custom recordset. Now everything is gone.
		'All that data has been forgotten and released from
		'memory.
		oRs.Close
		Set oRs = Nothing

		'close table
		AppendDisplay "</table>" & VbNewline


		AppendDisplay  "<br/><table class=""docfolder"" width=""100%"">"  & VbNewline & _
		"<tr>" & VbNewline & _
			"<td width=""200"" align=""left"">" & _
				"<a href='step2.asp?NewFold=false&folder=" & server.URLEncode(Path) & "' class=""createdoc""><img src=""/dbadmin/images/icon-add-doc.gif"" width=""15"" height=""16"" alt=""Create a new document"" border=""0"" /> <strong>Create a new Document</strong></a>  " & _
			"</td>" & VbNewline & _
			"<td align=""right""> " & VbNewline & _
				LinkDisplay(WhereAmI) & _
			"</td>"  & VbNewline & _
			"<td width=""30"" align=""right"">" & _
				"<a href=""step1.asp?NewFold=false&folder=" & server.URLEncode(Path) & """ title=""Move up one directory""><img src=""/dbadmin/images/icon-folder-up.gif"" width=""15"" height=""13"" alt=""Up one directory"" border=""0"" /></a>" & _
			"</td>" & VbNewline & _
			"<td width=""30"" align=""right"">"
			If session("AdminStatus") > 5 Then
				AppendDisplay	"<a href='step1b.asp?NewFold=true&folder=" & server.URLEncode(Path) & "' title=""Add a new folder""><img src=""/dbadmin/images/icon-add-folder.gif"" width=""15"" height=""13"" alt=""Add a new folder"" border=""0"" /></a>"
			End if
		AppendDisplay	"</td>" & VbNewline & _
		"</tr>" & VbNewline & _
		"</table>"

	End Sub


	Private Function WhereAmI()
		'returns the current path based on the
		'Path property
		Dim strCurrentBrowsingLocation, sHost

		sHost = request.serverVariables("HTTP_HOST")
		strCurrentBrowsingLocation = Path
		if strCurrentBrowsingLocation = "" then
			strCurrentBrowsingLocation = _
				"http://" & sHost & "/"
		else
			strCurrentBrowsingLocation = _
				"http://" & sHost & Path
		end if
		WhereAmI = strCurrentBrowsingLocation
	End Function


	Private Function DirectoryOption()
		'if possible, displays a link to the
		'next highest directory in the path
		'structure.
		Dim z, strLoc1, AllLoc, a, sOut, sTmp

		If Path = "/" Then
			DirectoryOption = ""
		Else
			On Error Resume Next
			strLoc1 = Path
			strLoc1 = Replace(strLoc1, "/", "  ")
			strLoc1 = trim(strLoc1)
			strLoc1 = Replace(strLoc1, "  ", "/")
			AllLoc = Split(strLoc1,"/")
			AllLoc(UBOUND(AllLoc)) = ""
			a = Join(AllLoc,"/")
			sOut = "/" & a
			sTmp = sTmp & "<br/><table class=""docfolder"" width=""100%"">" & VbNewline & _
			"<tr>" & VbNewline & _
						"<td width=""200"" align=""left"">" & _
							"<a href='step2.asp?NewFold=false&folder=" & server.URLEncode(Path) & "' class=""createdoc""><img src=""/dbadmin/images/icon-add-doc.gif"" width=""15"" height=""16"" alt=""Create a new document"" border=""0"" /> <strong>Create a new Document</strong></a>  " & _
						"</td>" & VbNewline & _
						"<td align=""right""> " & VbNewline & _
							LinkDisplay(WhereAmI) & _
						"</td>"  & VbNewline & _
						"<td width=""30"" align=""right"">" & _
							"<a href='" & ThisPage & "?folder=" & server.URLEncode(sOut) & "'><img src=""/dbadmin/images/icon-folder-up.gif"" width=""15"" height=""13"" alt=""Up one directory"" border=""0"" /></a></td>" & VbNewline & _
						"<td width=""30"" align=""right"">"

						If session("AdminStatus") > 5 Then
							sTmp = sTmp &	"<a href='step1b.asp?NewFold=true&folder=" & server.URLEncode(Path) & "'><img src=""/dbadmin/images/icon-add-folder.gif"" width=""15"" height=""13"" alt=""Add a new folder"" border=""0"" /></a>"
						End if

						sTmp = sTmp &	"</td>" & VbNewline & _
					"</tr>" & vbnewline
			DirectoryOption = sTmp


		End If
	End Function

	Private Function LinkDisplay(byVal FullPath)
		'Give an entered path the power to
		'become a menu by parsing and
		'linking each directory in the string
		Dim i, j, strLink, strOut

		FullPath = Split(FullPath, "/")
		For i = 1 to UBound( FullPath ) - 1
			j = 0  :  strLink = ""
			do until j > i
				strLink = strLink & _
					Replace( FullPath( j ), " ", "%20" ) & "/"
				j = j + 1
			loop
			If i = UBound( FullPath ) - 1 Then
				strOut = strOut & _
			    		Replace( FullPath( i ), " ", "&nbsp;" )
			Else
				If trim(strLink) <> "" And _
				    strLink <> "http://" Then
					strLink = Replace(strLink, "http://" & _
					    request.serverVariables("HTTP_HOST"), "")
					strOut = strOut & _
					    "<a class='dbtext ' href=""" & ThisPage & "?folder=" & _
					    Server.URLEncode(strLink) & """>" & _
					    Replace( FullPath( i ), " ", _
					    "&nbsp;" ) & "</a>/"
				End If
			End If
		Next
		strOut = trim(strOut)
		If Left(strOut, 1) = "/" Then strOut = Right(strOut, Len(strOut) - 1)
		LinkDisplay = "<strong>Current Folder: </strong>" & strOut
	End Function

	Private Function UnMappath(byVal FullPath)
		'take a mappath-ed directory and make it virtual
		'from -  C:\Windows\Desktop\Folder
		'to   -  /Folder/
		dim root, tmp1, tmp2

		root = lcase(server.mappath("/"))
		FullPath = lcase(FullPath)
		tmp1 = Replace(fullPath, root, "")
		tmp2 = Replace(tmp1, "\", "/")
		if not right(tmp2, 1) = "/" then tmp2 = tmp2 & "/"
		UnMappath = trim(tmp2)
	End Function


	Private Sub AppendDisplay(byVal toAppend)
		'write entered string to browser
		Response.Write(toAppend & vbCrLf)
	End Sub
End Class
%>
