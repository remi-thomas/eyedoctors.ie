function dcToolBar(textarea,format,img_path)
{
	this.addButton		= function() {}
	this.addSpace		= function() {}
	this.draw			= function() {}
	this.btStrong		= function() {}
	this.btEm			= function() {}
	this.btIns			= function() {}
	this.btDel			= function() {}
	this.btQ			= function() {}
	this.btCode			= function() {}
	this.btBr			= function() {}
	this.btPre			= function() {}
	this.btList			= function() {}
	/* nouveaux */
	this.btP			= function() {}
	this.drawWysiwyg	= function() {}
	this.btToggle		= function() {}
	this.wysiwygAble	= function() {}
	this.updateForPost	= function() {}
	this.setWysiwygByDefault = function() {}
	
	if (!document.createElement) {
		return;
	}
	
	if ((typeof(document["selection"]) == "undefined")
	&& (typeof(textarea["setSelectionRange"]) == "undefined")) {
		return;
	}
	
	var toolbar = document.createElement("div");
	toolbar.id = "dctoolbar";
	
	var wysiwygAble = ((document.designMode) && getFormat()=='html');
	//wysiwygAble = wysiwygAble && !(navigator.userAgent.toLowerCase().indexOf("safari") != -1);
	var isWysiwyg;
	var wysiwygIframe;
	var wysiwygIframeId = (textarea.id) ? (textarea.id + 'Wysiwyg') : 'wysiwyg';
	var wysiwygWin;
	var wysiwygContent;
	var wysiwygByDefault = true; // false pour afficher la source par defaut
	
	function getFormat() {
		if (format.value == 'wiki') {
			return 'wiki';
		} else {
			return 'html';
		}
	}
	
	function addButton(src, title, fn) {
		var i = document.createElement('img');
		i.src = src;
		i.title = title;
		i.onclick = function() { try { fn() } catch (e) { } return false };
		i.tabIndex = 400;
		toolbar.appendChild(i);
		addSpace(2);
	}
	
	function addSpace(w)
	{
		var s = document.createElement('span');
		s.style.padding='0 '+w+'px 0 0';
		s.appendChild(document.createTextNode(' '));
		toolbar.appendChild(s);
	}
	
	function encloseSelection(prefix, suffix, fn) {
		textarea.focus();
		var start, end, sel, scrollPos, subst;
		
		if (typeof(document["selection"]) != "undefined") {
			sel = document.selection.createRange().text;
		} else if (typeof(textarea["setSelectionRange"]) != "undefined") {
			start = textarea.selectionStart;
			end = textarea.selectionEnd;
			scrollPos = textarea.scrollTop;
			sel = textarea.value.substring(start, end);
		}
		
		if (sel.match(/ $/)) { // exclude ending space char, if any
			sel = sel.substring(0, sel.length - 1);
			suffix = suffix + " ";
		}
		
		if (typeof(fn) == 'function') {
			var res = (sel) ? fn(sel) : fn('');
		} else {
			var res = (sel) ? sel : '';
		}
		
		subst = prefix + res + suffix;
		
		if (typeof(document["selection"]) != "undefined") {
			var range = document.selection.createRange().text = subst;
			textarea.caretPos -= suffix.length;
		} else if (typeof(textarea["setSelectionRange"]) != "undefined") {
			textarea.value = textarea.value.substring(0, start) + subst +
			textarea.value.substring(end);
			if (sel) {
				textarea.setSelectionRange(start + subst.length, start + subst.length);
			} else {
				textarea.setSelectionRange(start + prefix.length, start + prefix.length);
			}
			textarea.scrollTop = scrollPos;
		}
	}
	
	function draw(msg) {
		var p = document.createElement('em');
		p.style.display='block';
		p.style.margin='-0.5em 0 0.5em 0';
		p.appendChild(document.createTextNode(msg));
		textarea.parentNode.insertBefore(p, textarea);
		textarea.parentNode.insertBefore(toolbar, textarea);
	}
	
	function drawWysiwyg() {
		if (!wysiwygAble) {
			return;
		}
		wysiwygIframe = document.createElement("iframe");
		wysiwygIframe.id = wysiwygIframeId;
		wysiwygIframe.height = textarea.offsetHeight + 0;
		wysiwygIframe.width = textarea.offsetWidth + 0;

		wysiwygIframe.className = textarea.className;
		//wysiwygIframe.className = "TextInput";
		wysiwygIframe.setAttribute("style", "border:1px solid #000;padding:0px none;background:#ffffff")
		
		textarea.parentNode.appendChild(wysiwygIframe);
		
		function initWysiwygControls() {
			if (!document.getElementById(wysiwygIframeId).contentWindow
					) { // gecko ne peut pas immediatement changer le designmode
				setTimeout(initWysiwygControls, 1);
				return;
			}
			wysiwygWin = wysiwygIframe.contentWindow;
			wysiwygWin.document.designMode = "on";

			function initContentWysiwyg() {
				if (!wysiwygWin.document && !wysiwygWin.document.body) {
					setTimeout(initContentWysiwyg, 1);
					return;
				}
				var template = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"' +
					'        "http://www.w3.org/TR/html4/strict.dtd">' +
					'<html><head><title>ndp</title>' +
					'</head><body></body></html>';
				
				wysiwygWin.document.open();
				wysiwygWin.document.write(template);
				wysiwygWin.document.close();
				if (navigator.userAgent.toLowerCase().indexOf("gecko") != -1) {
					addEvent(wysiwygWin.document, "keypress", geckoKeyPress, true);
				}
				wysiwygContent = wysiwygWin.document.body;
				wysiwygContent.innerHTML = textarea.value;
				isWysiwyg = !wysiwygByDefault;
				toggleView();
				var style = wysiwygWin.document.createElement("link");
				style.rel = 'stylesheet';
				style.type = 'text/css';
				style.href = '/ssi/mbf.css';
				wysiwygWin.document.getElementsByTagName("head")[0].appendChild(style);
			}
			setTimeout(initContentWysiwyg, 1); // IE ne peut pas acceder immediatement au contenu
		}
		
		setTimeout(initWysiwygControls, 1);
	}
	
	function geckoKeyPress(evt) {
		//function to add bold, italic, and underline shortcut commands to gecko's
		// from Cross-Browser Rich Text Editor http://www.kevinroth.com/rte/demo.htm
		//var rte = evt.target.id;
		
		if (evt.ctrlKey) {
			var key = String.fromCharCode(evt.charCode).toLowerCase();
			//alert(evt.charCode + " : " + key);
			var cmd = '';
			switch (key) {
				case 'b': cmd = "strong"; break;
				case 'i': cmd = "em"; break;
				case 'u': cmd = "ins"; break;
			};
	
			if (cmd) {
				singleTag('',cmd);
				
				// stop the event bubble
				evt.preventDefault();
				evt.stopPropagation();
			}
		}
	}
	
	function toggleView(){
		var html;
		if (!wysiwygAble) {
			return;
		}		
		if (isWysiwyg) {
			isWysiwyg = false;
			html = wysiwygContent.innerHTML;
			html= cleanHTML(html);
			textarea.value = html;
			wysiwygIframe.style.display = "none";
			textarea.style.display = "inline";
			textarea.focus();
		} else {
			isWysiwyg = true;
			html = textarea.value;
			wysiwygContent.innerHTML = html;
			textarea.style.display = "none";
			wysiwygIframe.style.display = "block";
			wysiwygWin.document.designMode = "on";
			wysiwygWin.focus();
		}
	}
	
	var wysiwyg_elementMap = new Array(
		//control regex, desired regex replacement
		[/<(\/?)(B|b|STRONG)([\s>\/])/g, "<$1strong$3"],
		[/<(\/?)(I|i|EM)([\s>\/])/g, "<$1em$3"],
		[/<IMG ([^>]*?[^\/])>/gi, "<img $1 />"],
		[/<(\/?)U([\s>\/])/gi, "<$1ins$2"],
		[/<(\/?)STRIKE([\s>\/])/gi, "<$1del$2"],
		[/<span style="font-weight: normal;">([\w\W]*?)<\/span>/gm, "$1"],
		[/<span style="font-weight: bold;">([\w\W]*?)<\/span>/gm, "<strong>$1</strong>"],
		[/<span style="font-style: italic;">([\w\W]*?)<\/span>/gm, "<em>$1</em>"],
		[/<span style="text-decoration: underline;">([\w\W]*?)<\/span>/gm, "<ins>$1</ins>"],
		[/<span style="text-decoration: line-through;">([\w\W]*?)<\/span>/gm, "<del>$1</del>"],
		[/<span style="text-decoration: underline line-through;">([\w\W]*?)<\/span>/gm, "<del><ins>$1</ins></del>"],
		[/<span style="(font-weight: bold; ?|font-style: italic; ?){2}">([\w\W]*?)<\/span>/gm, "<strong><em>$2</em></strong>"],
		[/<span style="(font-weight: bold; ?|text-decoration: underline; ?){2}">([\w\W]*?)<\/span>/gm, "<ins><strong>$2</strong></ins>"],
		[/<span style="(font-weight: italic; ?|text-decoration: underline; ?){2}">([\w\W]*?)<\/span>/gm, "<ins><em>$2</em></ins>"],
		[/<span style="(font-weight: bold; ?|text-decoration: line-through; ?){2}">([\w\W]*?)<\/span>/gm, "<del><strong>$2</strong></del>"],
		[/<span style="(font-weight: italic; ?|text-decoration: line-through; ?){2}">([\w\W]*?)<\/span>/gm, "<del><em>$2</em></del>"],
		[/<span style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: underline; ?){3}">([\w\W]*?)<\/span>/gm, "<ins><strong><em>$2</em></strong></ins>"],
		[/<span style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: line-through; ?){3}">([\w\W]*?)<\/span>/gm, "<del><strong><em>$2</em></strong></del>"],
		[/<span style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: underline line-through; ?){3}">([\w\W]*?)<\/span>/gm, "<del><ins><strong><em>$2</em></strong></ins></del>"],
		[/<([a-z]+) style="font-weight: normal;">([\w\W]*?)<\/\1>/gm, "<$1>$2</$1>"],
		[/<([a-z]+) style="font-weight: bold;">([\w\W]*?)<\/\1>/gm, "<$1><strong>$2</strong></$1>"],
		[/<([a-z]+) style="font-style: italic;">([\w\W]*?)<\/\1>/gm, "<$1><em>$2</em></$1>"],
		[/<([a-z]+) style="text-decoration: underline;">([\w\W]*?)<\/\1>/gm, "<ins><$1>$2</$1></ins>"],
		[/<([a-z]+) style="text-decoration: line-through;">([\w\W]*?)<\/\1>/gm, "<del><$1>$2</$1></del>"],
		[/<([a-z]+) style="text-decoration: underline line-through;">([\w\W]*?)<\/\1>/gm, "<del><ins><$1>$2</$1></ins></del>"],
		[/<([a-z]+) style="(font-weight: bold; ?|font-style: italic; ?){2}">([\w\W]*?)<\/\1>/gm, "<$1><strong><em>$3</em></strong></$1>"],
		[/<([a-z]+) style="(font-weight: bold; ?|text-decoration: underline; ?){2}">([\w\W]*?)<\/\1>/gm, "<ins><$1><strong>$3</strong></$1></ins>"],
		[/<([a-z]+) style="(font-weight: italic; ?|text-decoration: underline; ?){2}">([\w\W]*?)<\/\1>/gm, "<ins><$1><em>$3</em></$1></ins>"],
		[/<([a-z]+) style="(font-weight: bold; ?|text-decoration: line-through; ?){2}">([\w\W]*?)<\/\1>/gm, "<del><$1><strong>$3</strong></$1></del>"],
		[/<([a-z]+) style="(font-weight: italic; ?|text-decoration: line-through; ?){2}">([\w\W]*?)<\/\1>/gm, "<del><$1><em>$3</em></$1></del>"],
		[/<([a-z]+) style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: underline; ?){3}">([\w\W]*?)<\/\1>/gm, "<ins><$1><strong><em>$3</em></strong></$1></ins>"],
		[/<([a-z]+) style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: line-through; ?){3}">([\w\W]*?)<\/\1>/gm, "<del><$1><strong><em>$3</em></strong></$1></del>"],
		[/<([a-z]+) style="(font-weight: bold; ?|font-style: italic; ?|text-decoration: underline line-through; ?){3}">([\w\W]*?)<\/\1>/gm, "<del><ins><$1><strong><em>$3</em></strong></$1></ins></del>"],
		/* mise en forme identique contigue */
		[/<\/(strong|em|ins|del|q|code)>(\s*?)<\1>/gim, "$2"],
		[/<(br|BR)>/g, "<br />"],
		/* opera est trop strict ;)) */
		[/([^\s])\/>/g, "$1 />"],
		/* br intempestifs de fin de block */
		[/<br \/>\s*<\/(h1|h2|h3|h4|h5|h6|ul|ol|li|p|blockquote|div)/gi, "</$1"],
		//[/<\/(h1|h2|h3|h4|h5|h6|ul|ol|li|p|blockquote)>([^\n\u000B\r\f])/gi, "</$1>\n$2"],
		[/<(hr|HR)( style="width: 100%; height: 2px;")?>/g, "<hr />"]
	);
	
	function cleanHTML(html) {
		var nb = wysiwyg_elementMap.length;
		for (var foo = 0; foo < nb; foo++) {
			html = html.replace(wysiwyg_elementMap[foo][0], wysiwyg_elementMap[foo][1]);
		}
		/* tags vides */
		while ( /(<[^\/]>|<[^\/][^>]*[^\/]>)\s*<\/[^>]*>/.test(html) ) {
			html = html.replace(/(<[^\/]>|<[^\/][^>]*[^\/]>)\s*<\/[^>]*>/g, "");
		}
		/* tous les tags en minuscule */
		html = html.replace(
					/<(\/?)([A-Z0-9]+)/g,
					function(match0, match1, match2) {
						return "<" + match1 + match2.toLowerCase();
					}
				);
		/* IE laisse souvent des attributs sans guillemets */
		var myRegexp = /<[^>]+((\s+\w+\s*=\s*)([^"'][\w\/]*))[^>]*?>/;
		while ( myRegexp.test(html)) {
			html = html.replace(
				myRegexp,
				function (str, val1, val2, val3){
					var tamponRegex = new RegExp(val1);
					return str.replace(tamponRegex, val2+'"'+val3+'"');
				}
			)
		}
		/* les navigateurs rajoutent une unite aux longueurs css nulles */
		while ( /(<[^>]+style=(["'])[^>]+[\s:]+)0(pt|px)(\2|\s|;)/.test(html)) {
			html = html.replace(/(<[^>]+style=(["'])[^>]+[\s:]+)0(pt|px)(\2|\s|;)/gi, "$1"+"0$4");
		}
		/* correction des fins de lignes : le textarea edite contient des \n
		 * le wysiwyg des \r\n , et le textarea mis a jour SANS etre affiche des \r\n ! */
		html = html.replace(/\r\n/g,"\n");

		return html;
	}

	function IEpasteHTML(range, html) {	
		range.execCommand("delete");
		range.pasteHTML(html);
		range.collapse(false);
		range.select();
	}
	
	function domInsertTag(tag, needClosingTag, attributsArray) {
		if (typeof(needClosingTag)=="undefined") needClosingTag = true;
		var range, l;
		
		if (wysiwygWin.getSelection) { //gecko
			var selection = wysiwygWin.getSelection();
			range = selection.getRangeAt(0);
			var node = wysiwygWin.document.createElement(tag);
			if (attributsArray) {
				for (l=attributsArray.length-1; l>=0; l--) {
					node.setAttribute(attributsArray[l][0], attributsArray[l][1]);
				}
			}
			if (needClosingTag) {
				node.appendChild( range.extractContents() );
			} else {
				range.deleteContents();
			}
			selection.removeAllRanges();
			range.insertNode(node);
			
			range.selectNodeContents(node);
			selection.addRange(range);
			selection.collapseToEnd();

		} else { //ie
			range = wysiwygWin.document.selection.createRange();

			var html = "<"+tag;
			if (attributsArray) {
				for (l=0; l<attributsArray.length; l++) {
					html = html + ' ' + attributsArray[l][0] + '="' + attributsArray[l][1] + '"';
				}
			}
			if (needClosingTag) {
				html = html + ">" +range.htmlText + "</"+tag+">";
			} else {
				html = html + " />";
			}
			IEpasteHTML(range, html);
		}
	}
	
	var commandTag = {
		"strong" : "bold",
		"em" : "italic",
		"ins" : "underline",
		"del" : "strikethrough",
		"ul" : "insertunorderedlist",
		"ol" : "insertorderedlist"

	}
		
	function singleTag(wtag,htag,wetag,option) {
		if (getFormat() == 'wiki') {
			var stag = wtag;
			var etag = (wetag) ? wetag : wtag;
		} else {
			var stag = '<'+htag+'>';
			var etag = '</'+htag+'>';
		}
		if (!isWysiwyg) {
			encloseSelection(stag,etag);
		} else {
			wysiwygWin.focus();
			if(commandTag[htag]) {
				option = (typeof(option) == "undefined") ? null : option;
				wysiwygWin.document.execCommand(commandTag[htag], false, option);
			} else {
				domInsertTag(htag);
			}
		}
	}
	
	function btStrong(label) {
		addButton(img_path+'bt_strong.png',label,
		function() { singleTag('__','strong'); });
	}
	
	function btEm(label) {
		addButton(img_path+'bt_em.png',label,
		function() { singleTag("''",'em'); });
	}
	
	function btIns(label) {
		addButton(img_path+'bt_ins.png',label,
		function() { singleTag('++','ins'); });
	}
	
	function btDel(label) {
		addButton(img_path+'bt_del.png',label,
		function() { singleTag('--','del'); });
	}
	
	function btQ(label) {
		addButton(img_path+'bt_quote.png',label,
		function() { singleTag('{{','q','}}'); });
	}
	
	function btCode(label) {
		addButton(img_path+'bt_code.png',label,
		function() { singleTag('@@','code'); });
	}
	
	function btBr(label) {
		addButton(img_path+'bt_br.png',label,
		function() {
			if (!isWysiwyg) {
				var tag = getFormat() == 'wiki' ? "%%%\n" : "<br />\n";
				encloseSelection('',tag);
			} else {
				domInsertTag("br", false)
			}
		});
	}
	
	function btP(label) {
		addButton(img_path+'bt_para.png',label,
		function() {
			if (!isWysiwyg) {
				return;
			} else {
				wysiwygWin.document.execCommand("formatblock", false, "<p>");
				wysiwygWin.focus();
			}
		});
	}
	
	function btBquote(label) {
		addButton(img_path+'bt_bquote.png',label,
		function() {
			if (!isWysiwyg) {
				encloseSelection("\n",'',
				function(str) {
					if (getFormat() == 'wiki') {
						str = str.replace(/\r/g,'');
						return '> '+str.replace(/\n/g,"\n> ");
					} else {
						return "<blockquote>"+str+"</blockquote>\n";
					}
				});
			} else {
				domInsertTag("blockquote");
			}
		});
	}
	
	function btPre(label) {
		addButton(img_path+'bt_pre.png',label,
		function() {
			if (!isWysiwyg) {
				encloseSelection("\n",'',
				function(str) {
					if (getFormat() == 'wiki') {
						str = str.replace(/\r/g,'');
						return ' '+str.replace(/\n/g,"\n ");
					} else {
						return "<pre>"+str+"</pre>\n";
					}
				});
			} else {
				wysiwygWin.document.execCommand("formatblock", false, "<pre>");
				wysiwygWin.focus();
			}
		});
	}
	
	function btList(label,type) {
		var img = (type == 'ul') ? 'bt_ul.png' : 'bt_ol.png';
		var wtag = (type == 'ul') ? '*' : '#';
		
		addButton(img_path+img,label,
		function() {
			if (!isWysiwyg) {
				encloseSelection("",'',
				function(str) {
					if (getFormat() == 'wiki') {
						str = str.replace(/\r/g,'');
						return wtag+' '+str.replace(/\n/g,"\n"+wtag+' ');
					} else {
						str = str.replace(/\r/g,'');
						str = str.replace(/\n/g,"</li>\n <li>");
						return "<"+type+">\n <li>"+str+"</li>\n</"+type+">";
					}
				});
			} else {
				singleTag('',type);
			}
		});
	}
	


	
	
	
	// methods
	this.addButton		= addButton;
	this.addSpace		= addSpace;
	this.draw			= draw;
	this.btStrong		= btStrong;
	this.btEm			= btEm;
	this.btIns		= btIns;
	this.btDel		= btDel;
	this.btQ			= btQ;
	this.btCode		= btCode;
	this.btBr			= btBr;
	this.btBquote		= btBquote;
	this.btPre		= btPre;
	this.btList		= btList;
	this.btP			= btP;
	this.drawWysiwyg = drawWysiwyg;
	this.wysiwygAble = function() {return wysiwygAble;}
	this.updateForPost = function() {
		if (isWysiwyg) {
			var html = wysiwygContent.innerHTML;
			html= cleanHTML(html);
			textarea.value = html;
		}
	}
	this.setWysiwygByDefault = function (bool) {
		wysiwygByDefault = bool;
	}
}

function addEvent(obj, evType, fn, useCapture){
	if (obj.addEventListener){
		obj.addEventListener(evType, fn, useCapture);
		return true;
	} else if (obj.attachEvent){
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} else {
		//alert("Handler could not be attached");
		return false;
	}
}
