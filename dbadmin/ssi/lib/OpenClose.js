/*######################################
## script copyright fusio.net         ##
## developped for ndp.ie              ##
## by remi [at] fusio<.>net           ##
######################################*/

function ShowMe(ThisLayer) 
{ 
	// expand or contract
	if (document.getElementById(ThisLayer))
	{
		document.getElementById(ThisLayer).style.display = 'block';
	}
}
function HideMe(ThisLayer)
{
	if (document.getElementById(ThisLayer))
	{
	document.getElementById(ThisLayer).style.display = 'none';
	}
	//RefreshiMenu();
	return;
}