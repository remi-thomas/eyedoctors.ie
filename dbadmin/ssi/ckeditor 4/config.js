/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'en-gb';

	// Default setting.
	config.toolbarGroups = [
	    { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
	    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	    { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	    { name: 'insert' },
	    { name: 'about' },
	    '/',
	    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	    { name: 'links' },
	    { name: 'tools' },
	    { name: 'others' },
	    '/',
	    { name: 'styles'}
	];
	config.disableNativeSpellChecker = false;
	config.allowedContent = true;
	config.extraAllowedContent = 'a(*)'
	config.filebrowserImageBrowseUrl = '/dbadmin/ssi/ckfinder/ckfinder.html?type=Images';
	config.filebrowserImageUploadUrl = '/dbadmin/ssi/ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Images';
};
