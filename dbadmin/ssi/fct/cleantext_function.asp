<%
Function ReadInsideBodyTag(ThisContent)
	Dim objRegExp, expressionmatch
	Set objRegExp = New RegExp
	objRegExp.Global = True
	objRegExp.IgnoreCase = True

	'## step 1 :remove everything from  start till <body>'
	objRegExp.Pattern = "<body[^>]*>"
	Set expressionmatch = objRegExp.Execute(ThisContent)
	If expressionmatch.Count > 0 Then
		For Each expressionmatched in expressionmatch
			'Response.Write "<br /><B> value: " & expressionmatched.Value & "</B> was matched at position <B>" & expressionmatched.FirstIndex & "</B><br />"
			'delete everyhting until <body>'
			ThisContent = Right(ThisContent,len(ThisContent)- expressionmatched.FirstIndex)
			'remove <body[^>]*> tag'
			ThisContent = Replace (ThisContent, expressionmatched.Value, "")
		Next
	End If

	'## step 2 :remove everything from  </body> till end'
	objRegExp.Pattern = "<\/body\b>"
	Set expressionmatch = objRegExp.Execute(ThisContent)
	If expressionmatch.Count > 0 Then
		For Each expressionmatched in expressionmatch
			'Response.Write "<br /><B>" & expressionmatched.Value & "</B> was matched at position <B>" & expressionmatched.FirstIndex & "</B><br />"
			ThisContent = left(ThisContent, expressionmatched.FirstIndex)
			ThisContent = Replace (ThisContent, expressionmatched.Value, "")
		Next
	End If

	ThisContent = replace(ThisContent, "�", "'")
	ThisContent = replace(ThisContent, "`", "'")
	ThisContent = replace(ThisContent, "�", "'")

	ReadInsideBodyTag = ThisContent
	Set objRegExp = Nothing
	Set expressionmatch = Nothing
End Function

Function RemoveJavascript(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Pattern = "<script(.|\n)*<\/script\b>"
	objRegExp.Global = True
	RemoveJavascript = objRegExp.Replace(ThisContent, "")
End Function

Function GetImage(ThisContent)
	session("imagesname") = ""
	'Strips the image tags from strHTML
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim objRegExp, strOutput,objCols,objMatch, I, NewContent, aryString
	Set objRegExp = New Regexp
	objRegExp.IgnoreCase = True
	objRegExp.Global = True
	objRegExp.Pattern = "<img[^>]*>"
	' ## do we have a match ? ##'
	if objRegExp.Test(ThisContent) then
		Set objCols = objRegExp.Execute(ThisContent)
		'replace all the img tag by [image]'
		'+ get the image name and store them in a string  *ImagesIncluded*'
		imgcnt = 0
		For Each objMatch in objCols
			'response.Write "<br>- " & GetImagePath(objMatch.Value) & vbnewline
			ThisImgpath = GetImagePath(objMatch.Value)
			ThisImgpath = Replace (ThisImgpath, "../","")
			if InStr (ThisImgpath,"/") <> 1 and left(ThisImgpath,4) <> "http" then
				ThisImgpath = "/" & ThisImgpath
			end if

			'exclude URL starting by http(s)://'
			if left(ThisImgpath,4) <> "http" then
					ThisImgpath = Replace (ThisImgpath, "//","/")
					'response.Write "<br/>" & ThisImgpath
				if not fso.FileExists(Server.MapPath(ThisImgpath)) then
					If imgcnt > 0 then
						session("imagesname") = session("imagesname") & ","
					End if
					'response.Write   "<br />Match found at position " & objMatch.FirstIndex & ". Match Value is '" &   objMatch.Value & "'."
					' replace the image tag with [image] '
					ThisContent = Replace(ThisContent, objMatch.Value,"<!--img" & imgcnt & "-->" & objMatch.Value & "<!/--img" & imgcnt & "-->" )
					ThisContent = Replace(ThisContent, GetImagePath(objMatch.Value),"[images" & imgcnt & "]")
					'response.Write "<br />match("& I & ") " & objMatch.value
					session("imagesname") = session("imagesname") & imgcnt & ":" & GetImagePath(objMatch.Value)
					imgcnt =imgcnt + 1
				end if
			end if
		Next
		GetImage = ThisContent
	else
		'## no image in ThisContent ##'
		GetImage = ThisContent
	end if
	Set objCols = Nothing
	Set objRegExp = Nothing
	Set fso = Nothing
End Function


Function GetImageName(ImgTag)
	Dim ImgRegExp
	Set ImgRegExp = New RegExp
	ImgRegExp.Pattern = "([\w]+\.(gif)|(jpg)|(jpeg)|(bpm))"
	Dim objMatch
	For Each objMatch in ImgRegExp.Execute(ImgTag)
		GetImageName = objMatch.Value
	Next
	Set ImgRegExp = Nothing
End Function

Function GetImagePath(ImgTag)
	ImgTag = replace(ImgTag,"'","""")
	'response.Write "<br>? " & ImgTag
	Dim ImgRegExp
	Set ImgRegExp = New RegExp
	ImgRegExp.IgnoreCase = True
	'need to start at src= to avoid returning the alt="jimmy.gif"'
	ImgRegExp.Pattern = "(src)\s*=\s*[\""]([^\""]*)"
	ImgRegExp.global=true
	Dim objMatch
	For Each objMatch in ImgRegExp.Execute(ImgTag)
		'response.Write "<br>ImgPath " &  objMatch.Value
		GetImagePath = replace(objMatch.Value,"src=","")
		GetImagePath = replace(GetImagePath,chr(34),"")
	Next
	Set ImgRegExp = Nothing
End Function

Function GetImageAlt(ImgTag)
	Dim ImgRegExp
	Set ImgRegExp = New RegExp
	ImgRegExp.IgnoreCase = True
	'need to start at src= to avoid returning the alt="jimmy.gif"'
	ImgRegExp.Pattern = "(alt)\s*=\s*\""([^\""]*)"
	ImgRegExp.global=true
	Dim objMatch
	For Each objMatch in ImgRegExp.Execute(ImgTag)
		GetImageAlt = replace(objMatch.Value,"alt=","")
	Next
	Set ImgRegExp = Nothing
End Function


Function RemoveAllHTMLTags(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Pattern =  "<[\w/]+[^<>]*>"
	objRegExp.global=true
	RemoveAllHTMLTags = objRegExp.replace(ThisContent,"")
end function



Function TrimString(ThisContent, intLength)
	  ThisContent = Replace(ThisContent, vbCrLf, "")
	  If Len(ThisContent) > intLength Then
	    ThisContent = Left(ThisContent, intLength)
	    aryString = Split(ThisContent, " ")
	    ThisContent = ""
	    For a = 0 to UBound(aryString)-1
	      ThisContent = ThisContent & " " & aryString(a)
	    Next
	    ThisContent = Trim(ThisContent)
	  End If
	  TrimString = ThisContent
End Function


Function Remove_aHREF(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Pattern = "<a\b[^>].*>"
	objRegExp.global=true
	Remove_aHREF = objRegExp.replace(ThisContent,"")
end function


Function Create_Abstract(ThisContent)
	ThisContent = RemoveJavascript(ThisContent)
	ThisContent = Remove_aHREF(ThisContent)
	ThisContent = RemoveAllHTMLTags(ThisContent)
	ThisContent = trim(Replace(ThisContent, "  ", " "))
	ThisContent = Replace (ThisContent, chr(253), "")
	ThisContent = TrimString(ThisContent,500)
	Create_Abstract = ThisContent
End Function

Function Replace_aHREF(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.global=true
	'[\S\s] to allow anything between a and href ; ^ not ()() ; +any string until the end of this tag'
	objRegExp.Pattern = "<a[\S\s]href=['|""|"& """"& "[^(htt)|(ftp)|(www)|(mailto)]+[^<>]*>"

	Dim objMatch
		For Each objMatch in objRegExp.Execute(ThisContent)
			ThisLink = objMatch.Value
			'response.Write "<br /> ThisLink " & ThisLink &"_</a><br />"
			Dim objLinkExp
			Set objLinkExp = New RegExp
			objLinkExp.IgnoreCase = True
			objLinkExp.global=true

			objLinkExp.Pattern = "['|""]+[^<>]+['|""]"
			Dim linkMatch
			For Each linkMatch in objLinkExp.Execute(ThisLink)
				'response.Write "<hr>Newlink " & linkMatch.value &"<br />"
				Newlink = Replace(linkMatch.value, """", "")
				Newlink = Replace(Newlink, "'", "")
				Newlink= "[HyperLnk]" & Newlink
				'response.Write "Newlink " & Newlink &"<br />"
				Newlink = """" & Newlink & """"
				ThisContent = replace(ThisContent,linkMatch.value,Newlink)
			next
		Next

	Replace_aHREF = ThisContent
	Set objRegExp = Nothing
end function



Function RemoveMsWordTags(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Global = True

	'remove Ms <! tag'
	objRegExp.Pattern = "<!(.*?)>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	'clean nav bar '
	ThisContent = Replace (ThisContent, "<!-- This is the side table, the first details box  -->", chr(253))
	ThisContent = Replace (ThisContent, "<!-- Bottom line -->", chr(254))
	objRegExp.Pattern = chr(254) &  "(.|\n)*" & chr(253)
	ThisContent = objRegExp.Replace(ThisContent, "")


	'remove <span'
	'objRegExp.Pattern = "<span[^>]*>"
	'ThisContent = objRegExp.Replace(ThisContent, "")

	'remove style=''
	'objRegExp.Pattern = "style=['|""|"& """"& "][^>]*['|""|"& """"& "]"
	'ThisContent = objRegExp.Replace(ThisContent, "")

	'p class=MsoNormal
	objRegExp.Pattern = "p class=[^>]*>"
	ThisContent = objRegExp.Replace(ThisContent, "p>")

	' remove script'
	objRegExp.Pattern = "<script(.|\n)*<\/script\b>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	'remove <div'
	'objRegExp.Pattern = "<div[^>]*>"
	objRegExp.Pattern = "<div(.|\n)*>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	' remove background= '
	objRegExp.Pattern = "background=[^>]*"""
	ThisContent = objRegExp.Replace(ThisContent, "")

	'remove <font'
	objRegExp.Pattern = "<font[^>]*>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	'remove <h'
	objRegExp.Pattern = "<h[^>]*>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	'remove </h'
	objRegExp.Pattern = "<\/h[^>]*>"
	ThisContent = objRegExp.Replace(ThisContent, "")

	'remove class=''
	objRegExp.Pattern = "class=[^>]*['|""|"& """"& "]"
	ThisContent = objRegExp.Replace(ThisContent, "class=""greytext""")


	ThisContent = Replace (ThisContent, " >", ">")
	ThisContent = Replace (ThisContent, "<span>", "")
	'ThisContent = Replace (ThisContent, "</span>", "")
	ThisContent = Replace (ThisContent, "</div>", "")
	ThisContent = Replace (ThisContent, "<o:p>", "")
	ThisContent = Replace (ThisContent, "</o:p>", "")
	ThisContent = Replace (ThisContent, "ALIGN=""JUSTIFY""","")
	ThisContent = Replace (ThisContent, "</font>", "")
	ThisContent = Replace (ThisContent, "<p></p>", "")
	ThisContent = Replace(ThisContent,  chr(253), "")
	ThisContent = Replace(ThisContent,  chr(254), "")
	RemoveMsWordTags = ThisContent
	Set objRegExp = Nothing
End Function

Function Clean_aHREF(ThisContent)
	Dim objRegExp
	Set objRegExp = New RegExp
	objRegExp.IgnoreCase = True
	objRegExp.Pattern = "<a\b[^>].*>"
	objRegExp.global=true

	Set Matches = objRegExp.Execute(ThisContent)
    For Each Match in Matches
		'convert in match.value all &amp; to & '
		'that a bit silly, but it will avoid to have # &amp; # translated as # &amp;amp; # '
		ThisHrf	= EncodeAccent(Match.value)
		ThisHrf	= Replace(Match.value, "&amp;", "&")
		'then translate all # & # in this match
		ThisCleanHrf = DecodeAccent(ThisCleanHrf)
		ThisCleanHrf = Replace(ThisHrf, "&", "&amp;")

		ThisContent = Replace(ThisContent, ThisHrf, ThisCleanHrf)
    Next
		Clean_aHREF = ThisContent
		Set objRegExp = Nothing
end function


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'~ Function StripSpecialChar             ~'															  |
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
Function StripSpecialChar(inFileName)
	dim sOut,strorigFileName,arrSpecialChar,intCounter
	 arrSpecialChar  = Array("%20","%","#","+","(",")","&","$","@","!","*","<",">","?","/","|","\","?",":",",",".",";","'","""")
	 strorigFileName = inFileName
	 intCounter = 0

	 Do Until intCounter > UBound(arrSpecialChar)
	  sOut = replace(strorigFileName,arrSpecialChar(intCounter),"")
	  intCounter = intCounter + 1
	  strorigFileName = sOut
	 Loop
	 'A misunderstnading about what this function does.
	 'If you want to URL encode use CleanURLterms
	 'strorigFileName = Replace(strorigFileName,"'","")
	 'strorigFileName = Replace(strorigFileName," ","-")
	 StripSpecialChar = strorigFileName
End Function
'

'just a function to display array - use for testing '
function ShowArray(byval arr)
	dim item, s
	ctar = 0
	for each item in arr
		s = s & ctar &". " & item & "<br />"
		ctar = ctar + 1
	next
	ShowArray = s
end function
%>

