<script language="VBScript" runat="SERVER">

Function GetSizeFunction(ThisFilePath,ArtPicBig,ArtPicwidth,ArtPicHeight)
	if lcase(right(ArtPicBig,3)) <> "jpg" then
		call gfxSpex(Server.mappath(CleanUpPath(ThisFilePath & "\" & ArtPicBig)), width, height, right(ArtPicBig,3))
		ArtPicwidth = width
		ArtPicHeight = height
		exit function
	end if
	set info = Server.CreateObject("UnitedBinary.AutoImageInfo")  
	info.ReadHeader Server.mappath(CleanUpPath(ThisFilePath & "\" & ArtPicBig))
	ArtPicwidth = info.ImageWidth
	ArtPicHeight = info.ImageHeight
	set info = nothing 
End Function


function gfxSpex(flnm, width, height, strImageType)
     dim strPNG 
     dim strGIF
     dim strBMP
     dim strType
     strType = ""
     strImageType = "(unknown)"

     gfxSpex = False

     strPNG = chr(137) & chr(80) & chr(78)
     strGIF = "GIF"
     strBMP = chr(66) & chr(77)
     strType = GetBytes(flnm, 0, 3)

     if strType = strGIF then				' is GIF
        strImageType = "GIF"
        Width = lngConvert(GetBytes(flnm, 7, 2))
        Height = lngConvert(GetBytes(flnm, 9, 2))
        Depth = 2 ^ ((asc(GetBytes(flnm, 11, 1)) and 7) + 1)
        gfxSpex = True
     elseif left(strType, 2) = strBMP then		' is BMP
        strImageType = "BMP"
        Width = lngConvert(GetBytes(flnm, 19, 2))
        Height = lngConvert(GetBytes(flnm, 23, 2))
        gfxSpex = True

     elseif strType = strPNG then			' Is PNG

        strImageType = "PNG"
        Width = lngConvert2(GetBytes(flnm, 19, 2))
        Height = lngConvert2(GetBytes(flnm, 23, 2))  
     end if
  end function

function GetBytes(flnm, offset, bytes)
     Dim objFSO
     Dim objFTemp
     Dim objTextStream
     Dim lngSize

     Set objFSO = CreateObject("Scripting.FileSystemObject")
     
     ' First, we get the filesize
     Set objFTemp = objFSO.GetFile(flnm)
     lngSize = objFTemp.Size
     set objFTemp = nothing

     fsoForReading = 1
     Set objTextStream = objFSO.OpenTextFile(flnm, fsoForReading)

     if offset > 0 then
        strBuff = objTextStream.Read(offset - 1)
     end if

     if bytes = -1 then		' Get All!
        GetBytes = objTextStream.Read(lngSize)  'ReadAll
     else
        GetBytes = objTextStream.Read(bytes)
     end if

     objTextStream.Close
     set objTextStream = nothing
     set objFSO = nothing
 end function

 function lngConvert(strTemp)
	lngConvert = clng(asc(left(strTemp, 1)) + ((asc(right(strTemp, 1)) * 256)))
 end function

function lngConvert2(strTemp)
	lngConvert2 = clng(asc(right(strTemp, 1)) + ((asc(left(strTemp, 1)) * 256)))
end function


Function ResizeFunction(ThisFilePath,ArtPicBig,ArtPicwidth,ArtPicHeight,ImageWidth,ImageHeight,MyFileName)
	if right(ArtPicBig,3) = "gif" then
		exit function
	end if

	'find out if it s portrait or landscape' 
	'ratioguide is the variable 2=width ; 3=height'
	If ArtPicwidth >= ArtPicHeight then
		'landscape '
		ratioguide = 2
		PictWidth = ImageWidth
		PictHeight = ImageHeight
	else
		'portrait'
		ratioguide = 3
		PictWidth = ImageHeight
		PictHeight = ImageWidth
	end if

	'response.write "<br />ratioguide " & ratioguide & " PictWidth = " & PictWidth & " PictHeight = " & PictHeight

		' ## third step resize'
		set fx = Server.CreateObject("UnitedBinary.AutoImageEffects")   
		fx.regName = "Mr Diarmaid Mac Aonghusa"   
		fx.regCode = "2fcbAv2TWL8jpdZZSDhwxpypC6FfLWmm"    
		'response.Write "<hr>"
		'response.Write "<br/>current " & (CleanUpPath(ThisFilePath & "\" & MyFileName))
		'response.Write "<br>/big " & (CleanUpPath(ThisFilePath & "\" & ArtPicBig))

		fx.filename = Server.mappath(CleanUpPath(ThisFilePath  & "\" & ArtPicBig))
		'response.Write  fx.filename
		fx.LoadImage
		fx.resizeFilter=14
		fx.jpgQuality=100
		fx.aspectRatioGuide = ratioguide
		fx.resize PictWidth,PictHeight
		'fx.AutoMagicallyBrighten
		'fx.Sharpen
				

		fx.filename = Server.mappath(CleanUpPath(ThisFilePath & "\" & MyFileName))
		fx.SaveImage      
		set fx = nothing 
End Function

Function ImageSizeFunction(ThisFile)
	set info = Server.CreateObject("UnitedBinary.AutoImageInfo")  
	info.ReadHeader Server.mappath(ThisFile)
	ImageSizeFunction = info.FileSize
	ImageSizeFunction = Round(ImageSizeFunction/1024) + 1
	set info = nothing 
End Function


Function FileSizeFunction(ThisFile)
	dim fso 
	Set fso = CreateObject("Scripting.FileSystemObject")
	if fso.FileExists(ThisFile) then
		Set File = fso.GetFile(ThisFile)
		FileSizeFunction = file.size
		FileSizeFunction = Round(FileSizeFunction/1024) + 1
	else
		FileSizeFunction = -1
	end if
	Set fso = Nothing
End Function



Function RotateFunction(ThisFile)
	set fx = Server.CreateObject("UnitedBinary.AutoImageEffects")   
	fx.regName = "Mr Diarmaid Mac Aonghusa"   
	fx.regCode = "2fcbAv2TWL8jpdZZSDhwxpypC6FfLWmm"
	fx.filename = ThisFile
	fx.LoadImage
	fx.QuickRotate(270)
	fx.SaveImage
	set fx = nothing 
End Function

Function CleanUpPath(ThisPath)
	CleanUpPath = replace(ThisPath,"\","/")
	CleanUpPath = replace(CleanUpPath,"//","/")
End Function

Function CleanImgName(ThisString)
	CleanImgName = replace(ThisString, "&", "_")
	CleanImgName = replace(ThisString, "&amp;", "_")
	CleanImgName = replace(CleanImgName, "<", "")
	CleanImgName = replace(CleanImgName, ">", "")
	CleanImgName = replace(CleanImgName, chr(34), "")
	CleanImgName = replace(CleanImgName, "�", "")
	CleanImgName = replace(CleanImgName, "`", "")
	CleanImgName = replace(CleanImgName, "�", "")
	CleanImgName = replace(CleanImgName, "'", "")
	CleanImgName = xmlText(CleanImgName)
	CleanImgName = replace(CleanImgName, "__", "_")
	CleanImgName = replace(CleanImgName, " ", "_")
end function

'shorten file from (xxxx.ext) to (xxxx) [before the dot]'
Function ShortName(ImageFile)
	ShortName =left(ImageFile, ( len(ImageFile) - (len(ImageFile)-InStrRev(ImageFile, ".")+1 ) ) )
End Function

</script>
