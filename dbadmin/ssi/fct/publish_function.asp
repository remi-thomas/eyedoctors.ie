<%
Function create_file(FileName,MyPath)
	Set fso = CreateObject("Scripting.FileSystemObject")
	If fso.FileExists(Server.MapPath(MyPath & FileName)) then


		response.Write "<script language=""Javascript"">" & VbNewline & _
		"var where_to= confirm(""A file with that name already exists\nDo you want to\n- use this file name click 'ok'\n- Otherwise click 'cancel'"");" & VbNewline & _
		"if (where_to== true) " & VbNewline & _
		"	{ " & VbNewline & _
		"	window.location="""& Request.ServerVariables("URL") &"?mode="& request("mode") &"&amp;newfile="& newfile&"&amp;folder="& folder &"&amp;skipcreateprocess=true"";" & VbNewline & _
		"	} " & VbNewline & _
		" else " & VbNewline & _
		"	{ " & VbNewline & _
		"	window.location=""/dbadmin/docpublication/wizard/step1.asp"";" & VbNewline & _
		"	} " & VbNewline & _
		"</script>" & VbNewline & _


		response.end
	else
		Set folderObject = fso.GetFolder(Server.Mappath(MyPath))
		Set textStreamObject = folderObject.CreateTextFile(FileName,true,false)
		Set textStreamObject = Nothing
		Set folderObject = Nothing
	end if
	Set fso = Nothing
End Function

Function create_folder(FolderName,MyPath)
	Set fso = CreateObject("Scripting.FileSystemObject")
	if FSO.FolderExists(Server.MapPath(MyPath & FolderName)) Then
		response.Write "<script language=""Javascript"">alert(""This Folder name "& MyPath & FolderName &" already exists"")</script>"
		response.write "<meta http-equiv=""Refresh"" content=""1; URL=/dbadmin/docpublication/newdoc.asp?newfold=true&folder=" & folder & """>"
		response.end
	else
		 FSO.CreateFolder(Server.MapPath(MyPath & FolderName))
	end if
	Set fso = Nothing
End Function


Function Write_Page(FileName, ThisContent, MyPath)
	response.Write MyPath  
	dim fso, folderObject, textStreamObject
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set folderObject = fso.GetFolder(Server.Mappath(MyPath))
	response.Write "<br/>"& folderObject  
	response.Write "<br/>1<br/>"
	Set textStreamObject = folderObject.CreateTextFile(FileName,true,false)
	response.Write "<br/>2<br/>"
	textStreamObject.Write(ThisContent)
	response.Write "<br/>3<br/>"
	textStreamObject.Close ' remember to close it so that it writes the file
	Set textStreamObject = Nothing
	Set folderObject = Nothing
	Set fso = Nothing
end Function

Function ReadPage(MyPath)
	Set fso = CreateObject("Scripting.FileSystemObject")
	dim objFileContents, strFileContents
	set objFileContents = fso.OpenTextFile(server.mappath(MyPath), 1)
	If objFileContents.AtEndOfStream Then
		'Response.Write "file is empty"
		'ReadPage = "file is empty"
	else
		strFileContents = objFileContents.ReadAll
	end if
	ReadPage = strFileContents
	Set objFileContents = Nothing
	Set strFileContents = Nothing
	Set fso = Nothing

end Function

Function move_file(Pagetocopy,FolderFrom,FolderTo)
	'response.Write"<br />MapPath= " &  FolderFrom&Pagetocopy
	'response.Write"<br />FolderTo= " &  FolderTo&Pagetocopy
	filefound=fso.FileExists(Server.MapPath(FolderTo & Pagetocopy))
	If filefound then
		'response.Write "file exists"
		fso.DeleteFile Server.MapPath(FolderTo & Pagetocopy),true
	end if
	fso.MoveFile Server.MapPath(FolderFrom & Pagetocopy), Server.MapPath(FolderTo) & "/"

End Function


Function Copy_file(MyPath,Template)
	Set fso = CreateObject("Scripting.FileSystemObject")
	filefound = fso.FileExists(Server.MapPath(MyPath))
	templatefound = fso.FileExists(Server.MapPath(Template))
	If filefound or templatefound then
		'response.Write "file exists"
		'response.Write "<br>filefound ("& MyPath &") -> " & filefound & " & templatefound (" & Template &  ")"  & templatefound
		FSO.CopyFile Server.MapPath(Template), Server.MapPath(MyPath)
	else
		response.Write "<br>filefound ("& MyPath &") -> " & filefound & " & templatefound (" & Template &  ") ->" & templatefound
		'response.end
	end if
	Set fso = Nothing
End Function


Function GetDirectoryFromPath(MyPath)
	GetDirectoryFromPath = Left (MyPath, InStrRev(MyPath, "/") )
End Function

Function GetFileFromPath(MyPath)
	ThisFile = right(MyPath, len(MyPath) - InStrRev(MyPath, "/") )
	GetFileFromPath = ThisFile
End Function

Function ThisFileExists(MyPath)
	dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	ThisFileExists = fso.FileExists(Server.MapPath(MyPath))
End Function

Function ThisFolderExists(MyPath)
	dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	ThisFolderExists = fso.FolderExists(Server.MapPath(MyPath))
End Function

Function GenerateRandomletter()
	randomize
	GenerateRandomletter = Chr(Int(26 * Rnd + 97))
End Function

function JscriptError(errmessage)
	response.Write "<script type=""text/javascript"">" & VbNewline & _
	"	alert('"& errmessage &"')" & VbNewline & _
	"</script>"
	response.end
end function

Function delete_file(MyPath)
	If Len (MyPath) > 10 then
		Set fso = CreateObject("Scripting.FileSystemObject")
		filefound=fso.FileExists(Server.MapPath(MyPath))
		If filefound then
			'response.Write "file exists"
			fso.DeleteFile Server.MapPath(MyPath)
		end if
	End if
End Function

%>
