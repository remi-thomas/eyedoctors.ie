<%
Function GetMonitorName(UserID)
	if len(UserID) = 0 then
		GetMonitorName = ""
		exit function
	end if
	UserID =  Replace(UserID,",","")

	if not isNumeric(UserID) or UserID = "" then
		GetMonitorName = ""
		exit function
	end if

	Set mn = SqlConnection.Execute("Select Fullname from AdminUsers where AdminID=" & UserID)
	if not mn.eof then
		GetMonitorName = DisplayText(mn(0))
	end if
	Set mn = nothing
End Function

Function ShowVersion(AVersion)
	if not isNumeric(AVersion) or AVersion = "" then
		ShowVersion = "<font color=""#336699"">n/a</font>"
		exit function
	end if
	ShowVersion = left(AVersion,len(AVersion)-1) & "<b>.</b><font color=""#336699"">" & right(AVersion,1) & "</font>"


End Function


'change readpage path + ReadPageTxtOnly path'
Function ReadMonitorDocument(MyPath,Parametres)
	'append parameters if necessary '
	If len(Parametres) < 2 then
		Parametres = ""
	else
		Parametres = Parametres &"&amp;"
	end if

	'make sure MyPath starts by a /
	if left(MyPath,1) <> "/" then
		MyPath = "/" & MyPath
	end if

	Set HttpObj = Server.CreateObject("AspHTTP.Conn")
	rem HttpObj.Port = 80
	rem HttpObj.TimeOut = 80
	HttpObj.FollowRedirects = true
	HttpObj.UserAgent = "Mozilla/2.0 (compatible; MSIE 3.0B-Fusio; Windows NT)"

	HttpObj.Url =   "http://ICO.ie"& MyPath &"?" & Parametres
	HttpObj.RequestMethod = "GET"

	ReadMonitorDocument = HttpObj.GetURL


End Function


Sub DrawDownloadCentre
	response.write"<table width='100%' border='0' cellspacing='0' cellpadding='0' class=""greytext"">" & vbnewline & _
	"	<tr> " & vbnewline & _
	"		<td rowspan=""2"" align='center'>" & vbnewline & _
				"<img src='/images/media/downloadcentre.gif' width='147' height='164' alt='Document Download Centre' /> " & vbnewline & _
	"		</td>" & vbnewline & _
	"		<td valign='top'> " & vbnewline & _
				"<img src='/images/spacer.gif' alt='' width='10' height='10' />" & vbnewline & _
	"		</td>" & vbnewline & _
			"<td valign='top' class=""greytext""> " & vbnewline & _
				"This document is in <b>"& FileTypeDesc &"</b> and can be downloaded from the link below.<br/><br/> " & vbnewline & _
				"<img src="""& FileTypeIcon &""" alt="""& FileTypeDesc &""" align=""left"" />"& vbnewline & _
				"<a href=""" & DisplayFile  & """ target=""_readdoc"" style=""color: #000000;text-decoration:underline"">" & DocumentTitle & "</a>" & vbnewline

				if len(Abstract) > 3 then
					response.Write "<p class=""dbtext"" style=""border-top: 1px solid #DDD""> " & VbNewline & _
					Abstract & VbNewline & _
					"</p>" & VbNewline
				end if

	response.Write VbNewline & _
	"		</td>" & vbnewline & _
	"	</tr>"

	if len(FilePluginURL) > 3 then
		response.Write VbNewline & _
		"	<tr> " & vbnewline & _
		"		<td>&nbsp;</td>" & vbnewline & _
		"		<td>" & _
					"<br/><a href='" & FilePluginURL & "' target='_blank' class=""bluetext"">Download the free reader software for <b>"& FileTypeDesc &"</b></a>" & vbnewline & _
		"		</td>" & vbnewline & _
		"	</tr>"
	end if
	response.Write VbNewline & _
	"	</tr>" & vbnewline & _
	"</table>" & vbnewline
End Sub

Function ValidateNumeric(NumValue)
	if IsNull(NumValue) then
		ValidateNumeric = 0
		exit function
	end if
	if Not IsNumeric(NumValue) or NumValue="" then
		ValidateNumeric = 0
	else
		ValidateNumeric = NumValue
	end if
End Function
%>