<%
Function ThisFileExists(MyPath)
	dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	ThisFileExists = fso.FileExists(Server.MapPath(MyPath))
End Function


Function MediaTypeGraph(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
	Select Case MediaType
		Case 1
			MediaTypeGraph = "<img src=""/dbadmin/images/media/photo.gif"" width=""16"" height=""16"" alt=""photo"" border=""0"" />"
		Case 2
			MediaTypeGraph =  "<img src=""/dbadmin/images/media/audio.gif"" width=""16"" height=""16"" alt=""audio"" border=""0"" />"
		Case 3
			MediaTypeGraph = "<img src=""/dbadmin/images/media/document.gif"" width=""16"" height=""16"" alt=""document"" border=""0"" />"
		Case 4
			MediaTypeGraph = "<img src=""/dbadmin/images/general/video.gif"" width=""70"" height=""18"" alt=""video"" border=""0"" />"
		Case 5
			MediaTypeGraph = "<img src=""/dbadmin/images/media/pczip.gif"" width=""16"" height=""16"" alt=""PC compressed file"" border=""0"" />"
		Case 6
			MediaTypeGraph = "<img src=""/dbadmin/images/media/macsit.gif"" width=""16"" height=""16"" alt=""Mac compressed file"" border=""0"" />"
		Case 7
			MediaTypeGraph = "<img src=""/dbadmin/images/media/streaming.gif"" width=""35"" height=""35"" alt=""Streaming video"" border=""0"" />"
		Case 8
			MediaTypeGraph = "<img src=""/dbadmin/images/media/pdf.gif"" width=""29"" height=""12"" alt=""PDF"" border=""0"" />"
		End Select
End Function

Function DocTypeGraph(MedXtension)
	if isNull(MedXtension) or MedXtension = "" then
		exit function
	end if
	Select Case MedXtension
	Case "doc"
		DocTypeGraph = "<img src=""/dbadmin/images/media/word.gif"" width=""35"" height=""18"" alt=""Word Document"" border=""0"" />"
	Case "pdf"
		DocTypeGraph = "<img src=""/dbadmin/images/media/pdf.gif"" width=""35"" height=""18"" alt=""PDF Document"" border=""0"" />"
	Case "xls"
		DocTypeGraph =  "<img src=""/dbadmin/images/media/xls.gif"" width=""35"" height=""18"" alt=""Excel Document"" border=""0"" />"
	Case "ppt"
		DocTypeGraph = "<img src=""/dbadmin/images/media/ppt.gif"" width=""35"" height=""18"" alt=""Power Point document"" border=""0"" />"
	Case "txt","rtf"
		DocTypeGraph = "<img src=""/dbadmin/images/media/txt.gif"" width=""35"" height=""18"" alt=""Text Document"" border=""0"" />"
	Case Else
		DocTypeGraph =  "<img src=""/dbadmin/images/media/document.gif"" width=""16"" height=""16"" alt=""document "" border=""0"" />"
	End Select

End Function

Function MediaTypeText(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
Select Case MediaType
	Case 1
		MediaTypeText = "IMAGES"
	Case 2
		MediaTypeText =  "AUDIO"
	Case 3
		MediaTypeText = "DOCUMENT"
	Case 4
		MediaTypeText = "VIDEO"
	Case 5,6
		MediaTypeText = "COMPRESSED"
	Case 7
		MediaTypeText = "STREAMING"
	Case 8
		MediaTypeText = "TIFF"
	End Select
End Function

function DrawAttachedMedia (SupportID,section)
	'response.write "usp_GET_MediaType " & SupportID &",'"& ucase(section) &"'"
	if IsNumeric(SupportID) and SupportID <> "" Then
		
		set rsmed = sqlconnection.execute("usp_GET_MediaType " & SupportID &",'"& ucase(section) &"'")
			if not rsmed.eof then
				DrawAttachedMedia = ""
				Do until rsmed.eof
					Select Case rsmed(0)
						Case 1
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/dbadmin/images/media/photo.gif"" width=""16"" height=""16"" alt=""photo attached to this "& section &""" border=""0"" />"
						Case 2
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/dbadmin/images/media/audio.gif"" width=""16"" height=""16"" alt=""audio attached to this "& section &""" border=""0"" />"
						Case 3
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/dbadmin/images/media/document.gif"" width=""16"" height=""16"" alt=""document attached to this Press Release"" border=""0"" />"
						Case 4
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/dbadmin/images/general/video.gif"" width=""70"" height=""18"" alt=""video attached to this "& section &""" border=""0"" />"
						Case 5
							DrawAttachedMedia = DrawAttachedMedia & "<img src=""/dbadmin/images/media/pczip.gif"" width=""16"" height=""16"" alt=""PC compressed file"" border=""0"" />"
						Case 6
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/dbadmin/images/media/macsit.gif"" width=""16"" height=""16"" alt=""Mac compressed file"" border=""0"" />"
						Case 7
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/dbadmin/images/media/streaming.gif"" width=""35"" height=""35"" alt=""Streaming video"" border=""0"" />"
						Case 8
							DrawAttachedMedia = DrawAttachedMedia &  "<img src=""/dbadmin/images/media/tiff.gif"" width=""35"" height=""35"" alt=""Tiff"" border=""0"" />"

						Case Else
							DrawAttachedMedia = DrawAttachedMedia & " "
					End Select
				rsmed.Movenext
				Loop
			end if
			set rsmed = nothing
	end if
End function



function DrawAttachedMediaDB (SupportID,section)
	if not IsNumeric(SupportID) or SupportID = "" then
		exit function
	end if
	if section = "" then
		exit function
	end if
	if lcase(section) = "news" then
		set rsmed = sqlconnection.execute("usp_SEL_Coms_News_MediaList " & SupportID & ",1")
	elseif lcase(section) = "document" then
		set rsmed = sqlconnection.execute("usp_SEL_Coms_Document_MediaList " & SupportID & ",1")
	else 
		exit function
	end if
		if not rsmed.eof then
			DrawAttachedMediaDB = ""
			Do until rsmed.eof
				response.write "<tr>" & VbNewline & _
				"	<td>&nbsp;</td>" & VbNewline & _
				"	<td class=""dbText"" valign=""top"">"
				'online or offline media'
				if isNumeric(rsmed(9)) and rsmed(9) <> "" then
				if cint(rsmed(9)) = 0 then
					response.Write "<img src=""/dbadmin/dbadmin/images/cross.gif"" ALT=""This Media is currently offline - switch it on in the media section!"" width=""10"" height=""10"" border=""0"" class='black10u'>"
				elseif cint(rsmed(9)) = 1 then
					response.Write "<img src=""/dbadmin/dbadmin/images/tick.gif"" ALT=""This Media is currently online"" width=""10"" height=""10"" border=""0"" class='black10u'>"
				end if
				end if	
				response.write MediaTypeGraph(rsmed(7))
				
				response.write " &nbsp;<span class=""smlblue"">" & rsmed(2) & "</span> ("& rsmed(4) &" Kb)"  & _
				" " & TrimTitle(DisplayText(rsmed(5)),80)
				response.write "	</td>" & VbNewline & _
				"	<td valign=""top"">" & VbNewline & _
				"<a href=""/dbadmin/ssi/AttachMedia/remove_media.asp?MediaID="& rsmed(0)&"&amp;SupportID="& SupportID &"&amp;page=edit&amp;section="& section &""" class=""dbText"" onclick=""return confirm('Are you sure you want to remove this media from this event?');""><img src=""/dbadmin/dbadmin/images/delete.gif"" alt=""remove this media from this PR""  border=""0"" width=""15"" height=""15"" /></a>" & VbNewline & _
				"		</td>" & VbNewline & _
				"	</tr>" & VbNewline
			rsmed.Movenext
			Loop
		end if
	set rsmed = nothing
end function



function DrawImageHighlight (EventID)
	if IsNumeric(EventID) and EventID <> "" then
		set rsmed = sqlconnection.execute("usp_Get_ImageHighlight " & EventID )
			if not rsmed.eof then
				DrawImageHighlight = DrawImageHighlight & "<img src=""/dbadmin/images/media/photo.gif"" width=""16"" height=""16"" alt=""There's an image highlight attached to this event"" border=""0"" />"
			end if
		set rsmed = nothing
	end if
End function

function GetImageHighlight (EventID)
	if IsNumeric(EventID) and EventID <> "" then
		set rsmed = sqlconnection.execute("usp_Get_ImageHighlight " & EventID )
			if not rsmed.eof then

			end if
		set rsmed = nothing
	end if
End function


function DrawEventAttachedMediaDB (EventID)
	if not IsNumeric(EventID) or EventID = "" then
		exit function
	end if

	set rsmed = sqlconnection.execute("spGetEventMediaList " & EventID & ",1")
		if not rsmed.eof then
			DrawEventAttachedMediaDB = ""
			Do until rsmed.eof
				response.write "<tr>" & VbNewline & _
				"	<td colspan=""2""></td>" & VbNewline & _
				"	<td class=""dbText"">"

				response.write MediaTypeGraph(rsmed(5))

				response.write " &nbsp;<span >" & rsmed(2) & "</span> ("& rsmed(4) &" Kb)"  & _
				" " & TrimTitle(DisplayText(rsmed(5)),80)
				response.write "	</td>" & VbNewline & _
				"	<td valign=""top"">" & VbNewline & _
				"<a href=""remove_media.asp?MediaID="& rsmed(0)&"&amp;DocId="& EventID &"&amp;page=edit"" class=""dbText"" onclick=""return confirm('Are you sure you want to remove this media from this PR');""><img src=""/dbadmin/images/delete.gif"" alt=""remove this media from this PR""  border=""0"" width=""15"" height=""15"" /></a>" & VbNewline & _
				"		</td>" & VbNewline & _
				"	</tr>" & VbNewline
			rsmed.Movenext
			Loop
		end if
	set rsmed = nothing
end function

Function GetMediaAnySupport(Section,SupportID,MediaType,maxdisplay,preview)
	if not IsNumeric(SupportID) or SupportID = "" then
		exit function
	end if
	if not IsNumeric(maxdisplay) or maxdisplay = "" then
		maxdisplay = 1
	end if
	if not IsNumeric(MediaType) or MediaType = "" then
		MediaType = 0
	end if
	if not IsNumeric(preview) or preview = "" then
		preview = 1
	end if
	if section = "" then
		exit function
	else
		section = Ucase(section)
	end if

	cntf = 0
	'response.write("usp_SEL_ShowMedia '"& section &"'," & SupportID & "," & MediaType)
	'exit function
	set rsmed = sqlconnection.execute("usp_SEL_ShowMedia '"& section &"'," & SupportID & "," & MediaType)
	if not rsmed.eof And not rsmed.bof  then
		Do until rsmed.eof and cntf <= maxdisplay
				Caption = DisplayText(rsmed("Caption"))

				if cint(rsmed("MediaType")) = 1 then
					'image'
					if preview = 1 then
						if len(rsmed(3)) > 4 then
							ImgFile = DisplayText(rsmed(3))
						else
							ImgFile = DisplayText(rsmed(2))
						end if
						response.Write "<img src="""& DisplayText(rsmed(1)) & ImgFile & """ alt="""& Caption &""" border=""0"" />"
					else
						response.Write "<img src="""& DisplayText(rsmed(1)) & DisplayText(rsmed(2)) & """ alt="""& Caption &""" border=""0"" style=""margin:0;padding:4px;border:dotted #808080 1pt;""/>"
					end if
				else
					'document or file ?'
					if IsNumeric(rsmed("MediaType")) and rsmed("MediaType") <> "" then
						Select Case cint(rsmed("MediaType"))
							Case 2
									track = track + 1
									response.Write "<a href=""" & rsmed(1) & rsmed(2) & """ target=""media""  title="""& Caption &""">" & _
									MediaTypeGraph(2)
									if len(Caption) > 3 then
										response.Write trimtitle(Caption,25)
									else
										response.Write "Listen To Track "& track
									end if
									response.Write	"</a>" & vbnewline
							Case 3
								'it s a text document - find correct icon (xls/ppt/pdf)'
								response.Write DocTypeGraph(Right(rsmed(2),3))
							Case Else
								response.Write "<a href=""" & rsmed(1) & rsmed(2) & """ target=""media"" >" & _
								MediaTypeGraph(cint(rsmed(5))) & " " & MediaTypeText(cint(rsmed(5))) & "</a>" & vbnewline
								response.write trimtitle(Caption,25)
								response.write "<A HREF=""/media/download.asp?File=" & Server.urlEncode(rsmed(1) & rsmed(2)) & "&Name=" & Server.urlEncode(rsmed(4)) & "&Size=" &  rsmed(6) & """ onMouseOver=""self.status='" & replace(rsmed(4),"'","\'") & "'; return true;"" onMouseOut=""self.status=''; return true;""><img src=""/images/download.jpg"" alt=""Download "&  DisplayText(rsmed(4)) &"""  height=""20"" border=""0"" alt=""download this media to your HardDrive""/></A>"

						End Select
						end if
				end if
			rsmed.movenext
				if not rsmed.eof then
					response.Write "<br />"
				end if
			loop
	end if
End Function


Function r_MediaTypeGraph(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
Select Case MediaType
	Case 1
		r_MediaTypeGraph = "<img src=""/images/media/htm.gif"" width=""20"" height=""20"" alt=""Html Document"" border=""0"" />"
	Case 2
		r_MediaTypeGraph = "<img src=""/images/media/pdf.gif"" width=""29"" height=""12"" alt=""PDF Document"" border=""0"" />"
	Case 4
		r_MediaTypeGraph = "<img src=""/images/media/doc.gif"" width=""24"" height=""21"" alt=""MS Word Document"" border=""0"" />"
 	Case 5
		r_MediaTypeGraph = "<img src=""/images/media/xls.gif"" width=""24"" height=""21"" alt=""Excel Document or CSV file"" border=""0"" />"
 	Case 6
		r_MediaTypeGraph = "<img src=""/images/media/rtf.gif"" width=""24"" height=""21"" alt=""Rich Text Format Document"" border=""0"" />"
 	Case 7
		r_MediaTypeGraph = "<img src=""/images/media/txt.gif"" width=""27"" height=""12"" alt=""Text Document"" border=""0"" />"
 	Case 8
		r_MediaTypeGraph = "<img src=""/images/media/ppt.gif"" width=""35"" height=""18"" alt=""MS PowerPoint"" border=""0"" />"
	Case 9
		r_MediaTypeGraph = "<img src=""/images/media/pczip.gif"" width=""20"" height=""20"" alt=""PC compressed file"" border=""0"" />"
	Case 10
		r_MediaTypeGraph = "<img src=""/images/media/macsit.gif"" width=""20"" height=""20"" alt=""Mac compressed file"" border=""0"" />"	
	Case 11
		r_MediaTypeGraph =  "<img src=""/images/media/audio.gif"" width=""20"" height=""20"" alt=""audio"" border=""0"" />"
	Case 12
		r_MediaTypeGraph = "<img src=""/images/media/gif.gif"" width=""20"" height=""20"" alt=""photo"" border=""0"" />"
	Case 13
		r_MediaTypeGraph = "<img src=""/images/media/jpg.gif"" width=""20"" height=""20"" alt=""photo"" border=""0"" />"
	Case 14
		r_MediaTypeGraph = "<img src=""/images/media/photo.gif"" width=""20"" height=""20"" alt=""photo"" border=""0"" />"
	Case 15
		r_MediaTypeGraph = "<img src=""/images/media/video.gif"" width=""20"" height=""20"" alt=""video"" border=""0"" />"
	Case 16
		r_MediaTypeGraph = "<img src=""/images/media/quicktime.gif"" width=""20"" height=""20"" alt=""quick time"" border=""0"" />"
	Case 17
		r_MediaTypeGraph = "<img src=""/images/media/txt.gif"" width=""20"" height=""20"" alt=""Text Document"" border=""0"" />"
	Case else
		r_MediaTypeGraph = "<img src=""/images/media/document.gif"" width=""20"" height=""20"" alt=""Rich Text Format Document"" border=""0"" />"
	End Select
End Function

Function r_MediaTypeText(MediaType)
	if not isNumeric(MediaType) or MediaType = "" then
		exit function
	end if
Select Case MediaType
	Case 1
		r_MediaTypeText = "IMAGES"
	Case 2
		r_MediaTypeText = "PDF Document"
	Case 4
		r_MediaTypeText = "Microsoft Word Document"
 	Case 5
		r_MediaTypeText = "Microsoft Excel Document or CSV file"
 	Case 6
		r_MediaTypeText = "Rich Text Format Document"
 	Case 7
		r_MediaTypeText ="Text Document"
 	Case 8
		r_MediaTypeText = "Microsoft PowerPoint"
	Case 9
		r_MediaTypeText = "PC compressed file"
	Case 10
		r_MediaTypeText = "Mac compressed file"
	Case 11
		r_MediaTypeText =  "audio file"
	Case 12
		r_MediaTypeText = "gif file"
	Case 13
		r_MediaTypeText = "jpg file"
	Case 14
		r_MediaTypeText = "Photo file"
	Case 15
		r_MediaTypeText = "video file"
	Case 16
		r_MediaTypeText = "Quick Time file"			
	Case 17
		r_MediaTypeText = "Text Document"	
	End Select
End Function

%>