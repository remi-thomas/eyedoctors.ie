<%
if Session("AdminStatus") > 1 then
%>
<div id="navbar">
  <ul id="nav">
	<li class="small"><a href="/dbadmin/">Home</a></li>
  <li class="small"><a href="/dbadmin/wysiwyg/">CMS</a></li>
  <li class="small"><a href="/dbadmin/HomePageBanner/">HP Banners</a></li>
  <li class="small"><a href="/dbadmin/eventsboard/">Events &amp; News</a></li>
  <li class="more"><span>PRs</span>
      <ul>
        <li><a href="/dbadmin/PR/WYSIWYG/editor.asp">Add New PR</a></li>
        <li><a href="/dbadmin/PR/default.asp">View PRs</a></li>
         <li><a href="/dbadmin/PR/add.asp">Add PRs (source code)</a></li>
      </ul>
    </li>
  <li class="more"><span>Media</span>
      <ul>
        <li><a href="/dbadmin/media/">Add Media</a></li>
        <li><a href="/dbadmin/media.asp">View Media</a></li>
        <%
        If Session("AdminStatus") > 8 Then
        %>
            <li><a href="/dbadmin/media/view.asp">Media Admin</a></li>
        <%
        End If
        %>
         <li><a href="/dbadmin/FlickrUpdate/update.asp">Update Flickr Galleries</a></li>
          </ul>
    </li>


    <!-- li class="small"><a href="/dbadmin/postgraduates/">Postgraduates</a></li -->

    <li class="more"><span>Members Data</span>
      <ul>
        <li><a href="/dbadmin/members/list-all.asp">list (all)</a></li>
	     <li><a href="/dbadmin/members/list.asp">list (alpha.)</a></li>
       <li><a href="/dbadmin/members/search.asp">Search</a></li>
	     <li><a href="/dbadmin/members/edituser.asp">Add</a></li>
	     <li><a href="/dbadmin/members/temp_list.asp">Temporary List</a></li>
       <li><a href="/dbadmin/members/export-all.asp">Export to Excel</a></li>
      </ul>
    </li>

	<%
	if Session("AdminStatus") > 5 then
	%>


    <li class="more"><span>Admin Users</span>
      <ul>
       <li><a href="/dbadmin/useradmin/">Search Profile</a></li>
	    <li><a href="/dbadmin/useradmin/edituser.asp">Add Profile</a></li>
      </ul>
    </li>
	<%
	end if
	%>
	<li class="small"><a href="/dbadmin/logout.asp">Logout</a></li>
  </ul>
</div>
<%
else
%>
<div id="nav">&nbsp;</div>
<%
end if
%>
<div id="content">
