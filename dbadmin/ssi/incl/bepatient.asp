<html>
<head>
<title>ICO  - Navigation - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<body class="body">
<%
CleaningStep = request.querystring("CleaningStep") 
if not Isnumeric(CleaningStep) or CleaningStep = "" then
	CleaningStep = 1
end if
%>
<table align="center" class="admintable">
	<tr class="cellheader">	
		<td><img src="/dbadmin/images/clock.gif"></td>
		<td>Please Be Patient</td>
	</tr>
	<tr class="celldetail">	
		<td colspan="2">Cleaning in Progress...</td>
	</tr>
	<tr>	
		<td colspan="2" class="smallbluetext ">Step <%=CleaningStep%> out of 4</td>
	</tr>
</table>
</body>
</html>
