
      <h3>Find Document By Category</h3>

      <form method="post" action="default.asp" >
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
                Type:<br/>
                <select name="FileType" class="TextInput">
                	<option value="">Any</option>
<%

	SQL="Select Distinct FileTypeID,MediaDesc from FileTypes order by FileTypeID"
	Set rsFileTypes=sqlConnection.execute(SQL)
	'kludge to ensure FileType is treated as an integer'
	FileType=FileType+0
	do while NOT rsFileTypes.eof
		Response.Write "<option value='" & rsFileTypes(0) & "'"
		if FileType=rsFileTypes(0) Then Response.Write "selected"
		Response.Write ">" & Trim(rsFileTypes(1)) & "</option>" & vbcrlf
		rsFileTypes.MoveNext
	Loop
%>
                </select>
			</td>
			<td>
                Category:<br/>
                <select name="DocCat" class="TextInput">
                	<option value="">Any</option>
<%
	SQL="Select Distinct TopicID,TopicTitle from MonitoringTopic order by TopicTitle"
	Set rsCatTypes=sqlConnection.execute(SQL)
	'kludge to ensure DocCat is treated as an integer'
	DocCat=DocCat+0
	do while NOT rsCatTypes.eof
		Response.Write "<option value='" & rsCatTypes(0) & "'"
		if DocCat=rsCatTypes(0) Then Response.Write "selected"
		Response.Write ">" & Trim(rsCatTypes(1)) & "</option>" & vbcrlf
		rsCatTypes.MoveNext
	Loop
%>
                </select>
             </td>
			<td>
                Date Criteria<br/>
                <select name="DateCriteria" class="TextInput">
<%
	if len(DateCriteria) > 1 Then
%>
                	<option selected value="<%=DateCriteria %>">Last <%=DateCriteria %> Days</option>
<%
	end if
%>
                	<option value="">Any</option>
					<option value="10">Last 10 Days</option>
					<option value="30">Last 30 Days</option>
					<option value="60">Last 60 Days</option>
					<option value="90">Last 90 Days</option>
					<option value="120">Last 120 Days</option>
                </select>
			</td>
			<td>
			  Keyword<br/>
			  <input type="text" name="kw" size="18" value="<%=kw %>" class="TextInput">
			</td>
			<td>
			  Doc. ID<br/>
			  <input type="text" name="ComsCentre_DocID" size="5" value="<%=ComsCentre_DocID %>" class="TextInput">
			</td>
			<td >
        <div class="submit"><input type="submit" name="Submit" value="Search" class="TextInput"></div>
      </td>
    </tr>
  </table>
  </form>
