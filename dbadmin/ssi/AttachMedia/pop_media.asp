<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - Dublin Theatre Festival</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body  onload="focus()">
<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<!--#include virtual="/dbadmin/ssi/incl/top_pop.asp"-->
<%
section = lcase(Request.Querystring("section"))

If section = "" then
	response.Write "<table><tr><td class=""bluetext""> section (" & section &") is undefined" & _
	"<br />Close this window and start again! </td>" & VbNewline & _
	"</tr>" & VbNewline & _
	"</table>" & VbNewline
	response.end
end if

SupportID	= Request.Querystring("SupportID")
if not IsNumeric(SupportID) or SupportID = "" then
	response.Write "<table><tr><td class=""bluetext""> Doc ID (" & SupportID &") is not numeric " & IsNumeric(SupportID) & _
	"<br />Close this window and start again! </td>" & VbNewline & _
	"</tr>" & VbNewline & _
	"</table>" & VbNewline
	response.end
end if


MediaType = trim(request.querystring("MediaType"))


If isNull(MediaType) or MediaType="" then
	MediaType = 0
end if


FirstLetter = left(trim(request.querystring("FirstLetter")),1)

%>
<table style="margin:0;padding:0" width="100%" class="whiteborder" border="0">
	<tr>
		<td>
			<table style="margin:0;padding:0">
			<tr>
<%
set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
	Do until rs.eof
		cnt = cnt + 2
		If rs(0) = cint(MediaType) then
			astyle= "bodytext"
			MediaCurrentlyDisplay = rs(1)
		else
			astyle= "bluetext"
		end if

	response.Write "		<td class=""bluetext""><a href=""pop_media.asp?MediaType="& rs(0) & "&amp;SupportID="& SupportID &"&amp;Section="& section &""" class="""& astyle &""">" & rs(1) & "</a></td>" & VbNewline & _
				"		<td class=""bluetext"">|</td>" & vbnewline
	rs.Movenext
	Loop
%>
		</tr>
	</table>
	</tr>
<tr>
	<td class="bodytext" colspan="2">A to Z for <%=MediaCurrentlyDisplay%>
	<span class="bluetext">
		<%
		set rs = sqlconnection.execute("exec usp_SEL_AZ_Media " & MediaType)
		Do until rs.eof
			response.Write "&nbsp;<a href=""pop_media.asp?MediaType="& MediaType & "&amp;SupportID="& SupportID &"&amp;Section="& section &"&amp;FirstLetter=" & Ucase(rs(0)) & """ "
			if lcase(FirstLetter) = lcase(rs(0)) then
				response.Write ""
			else
				response.Write "class=""bluetext"""
			end if
			response.Write">" & Ucase(rs(0)) & "</a>&nbsp;|"
		rs.Movenext
		Loop
		%>
	</span>
	</td>
</tr>
</table>
<hr class="hrgrey">
<%
mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if
cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 13

set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open "usp_SEL_MediaPerType " & MediaType & ","& SupportID &",'" & FirstLetter & "'", sqlconnection


If rs.eof then
	response.Write "<p class=""bluetext"">This request returns eof <br/> No media type "& MediaType &" stored in db</p>" & VbNewline
	response.end
end if

rec = 1
rs.movefirst
rs.pagesize = maxdisplay
rs.absolutepage = mypage
maxpages = cint(rs.pagecount)
' display the total of records returned by sql query'
intRecordCnt = rs.RecordCount

if maxpages > 1 then
	call Paging
end if

%>
<form name="attach_media" method="post" action="attach_media.asp?" onSubmit="return validate_form(this);" style="margin:2;padding:0"  class="whiteborder">
<table width="99%" cellspacing="1" cellpadding="1" border="0" class="whiteborder">
	<tr>
		<td class="bluetext" bgcolor="#BBD9F0"><img src="/dbadmin/images/paperclip.gif" width="20" height="20" alt=""></td>
		<td class="bluetext" bgcolor="#BBD9F0"><b>Title</b></td>
		<td class="bluetext" bgcolor="#BBD9F0"><b>Path</b></td>
		<td class="bluetext" bgcolor="#BBD9F0"><b>Size</b></td>
		<td class="bluetext" width="20" bgcolor="#BBD9F0"><img src="/dbadmin/images/icon_online.gif" alt="online/offline?" width="20" height="20"/></td>
		<%
			if MediaType = 1 then
				response.Write "<td class=""bluetext"" bgcolor=""#BBD9F0""><b>Preview</b></td>" & vbnewline
			else
				response.Write "<td class=""bluetext"" bgcolor=""#BBD9F0""><td>"
			end if
		%>
	</tr>
	<%
	Do until rs.eof  or (rec > maxdisplay)
	MediaType = rs("MediaType")
	if  section = "pr" then
		AlreadyLinked = rs("AlreadySelectedinPR")
	elseif section = "news" then
		AlreadyLinked = rs("AlreadySelectedinNews")
	end if
	If rec mod 2 = 0 then
		BgColor = "#D6E8F5"
	else
		BgColor = "#E4F1FA"
	end if

	%>
	<tr>
		<td class="dbtext" valign="top"  BgColor="<%=BgColor%>">
				<%
				'response.Write cint(rs("AlreadySelectedinEvent"))'

				'media already linked'
				if AlreadyLinked = 1 then
				%>
					 <a href="remove_media.asp?page=popmedia&amp;mypage=<%=mypage%>&amp;SupportID=<%=SupportID%>&amp;MediaType=<%=MediaType%>&amp;MediaID=<%=rs(0)%>&amp;section=<%=section%>" class="dbtext" onclick="return confirm('Are you sure you want to remove this media from this PR?');"><img src="/dbadmin/images/delete.gif" alt="Remove this media (file will NOT be deleted from system)"  border="0" /></a>
				<%
				else
				%>
					 <input type="checkbox" class="TextInput" name="MediaID<%=rec%>"  value="<%=rs(0)%>">
				<%
				end if 'media already linked'
				%>
			</td>
			<td class="dbtext" valign="top" BgColor="<%=BgColor%>">
				<%=TrimTitle(DisplayText(rs(5)),80)%>
			</td>
			<td class="dbtext" valign="top" BgColor="<%=BgColor%>">
				<% response.write rs(1) & rs(2) %>
			</td>
			<td class="dbtext" valign="top" BgColor="<%=BgColor%>">
				<%=rs(4)%> KB
			</td>
			<td class="dbtext" valign="top" align="center" BgColor="<%=BgColor%>">
				<%
					if rs(7) = 1 then
						response.Write "<img src=""/dbadmin/images/tick.gif"" width=""10"" alt=""online"">"
					else
						response.Write "<img src=""/dbadmin/images/cross.gif"" height=""10"" alt=""offline"">"
					end if
				%>
			</td>
			<td class="dbtext" valign="top" BgColor="<%=BgColor%>">
			<%
			if Len(rs(3)) > 4 then
			%>
			<a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="click to view image" class="bluetext">+/- view</a>
				<span id="field<%=rec%>" style="display:none;margin:0">
					<br/><img src="<%response.write rs(1) & rs(3)%>" alt="Media" style="margin:0" width="50">
				</span>
			<%end if%>
			</td>
		</tr>
	<%
	rs.movenext
	rec = rec + 1
loop
	%>
	<tr>
		<td colspan="6">
		<input type="hidden" name="mypage" value="<%=mypage%>">
		<input type="hidden" name="Season" value="<%=Season%>">
		<input type="hidden" name="MediaType" value="<%=MediaType%>">
		<input type="hidden" name="ttlRec" value="<%=rec%>">
		<input type="hidden" name="SupportID" value="<%=SupportID%>">
		<input type="hidden" name="Section" value="<%=Section%>">
		<input type="hidden" name="FirstLetter" value="<%=FirstLetter%>">
		<input type="submit" value="Attach Selected Media" class="TextInput" tabindex="6" label=""></td></td>
	</tr>
</table>
</form>
	<%
if maxpages > 1 then
	call Paging
end if



rs.close
set rs=nothing


sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "<table width=""100%"">" & VbNewline & _
		"<tr><td align=""right"" >Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a class=""bluetext"" href=""pop_media.asp?mypage=" & counter & "&amp;SupportID=" & SupportID & "&amp;section=" & section & "&amp;FirstLetter=" & FirstLetter
				If MediaType <> "" Then
						response.write "&amp;MediaType=" & MediaType
				End if
				If season <> "" Then
						response.write "&amp;season=" & season
				End if
				Response.Write """>" & counter & "</a>"
			else
				Response.Write "<i class='bluetext'>" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
		response.write"</td></tr></table>" & vbnewline
	end if
end sub
%>
<script language="JavaScript" type="text/javascript">
//########################################
//## script copyright fusio.net         ##
//## developped for Dub Theare Festival ##
//## by remi [at] fusio<.>net           ##
//########################################
function ShowMe(ThisLayer)
{
	// expand or contract
	document.getElementById(ThisLayer).style.display = (document.getElementById(ThisLayer).style.display == 'block') ? 'none' : 'block';
	cvalue = (document.getElementById(ThisLayer).style.display == 'block') ? 1 : 2;
}

function validate_form(thisform)
{
	var nbchkbox = 0;
	for (Count = 0; Count < document.attach_media.elements.length; Count++)
	{
		if (attach_media[Count].checked) nbchkbox++;
	}
	if (nbchkbox == 0)
	{
	alert("No media have been selected");
	return false;
	}
}

function CloseNUpdate()
	{
		//alert("close")
		window.opener.location.reload();
		window.close()
	}
</script>

 <p align="right"><a href="#" onClick="CloseNUpdate();" class="bluetext">[close window]</a></p>
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->