<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<%
SupportID	= trim(Request.querystring("SupportID"))
MediaID		= trim(Request.querystring("MediaID"))
page		= trim(Request.querystring("page"))
section		= lcase(trim(Request.querystring("section")))
If not IsNumeric(SupportID) or SupportID = "" then
	response.Write "Error! SupportID ("& SupportID &") is not numeric : " & IsNumeric(SupportID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

If not IsNumeric(MediaID) or MediaID = "" then
	response.Write "Error! MediaID ("& MediaID &") is not numeric : " & IsNumeric(MediaID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

if section="event" then
	TbleToUpdate = "EventMedia"
	ColToUpdate	 =  "EventID"
elseif section="specialevents" then
	TbleToUpdate = "SpecialEventsMedia"
	ColToUpdate	 =  "SpecialEventID"
elseif section="pr" then
	TbleToUpdate = "ComsCentre_PRMedia"
	ColToUpdate	 =  "PrID"
elseif section="review" then
	TbleToUpdate = "ReviewMedia"
	ColToUpdate	 =  "ReviewID"
'kludge to get around typo in hyperlinks - sometimes parameter was artist, other time it is artists - DMA 20060815'
elseif section="artists" or section="artist" then
	section="artists"
	TbleToUpdate = "ArtistsMedia"
	ColToUpdate	 =  "ArtistID"
elseif section="company" then
	TbleToUpdate = "CompaniesMedia"
	ColToUpdate	 =  "CompanyID"
elseif section="article" then
	TbleToUpdate = "articleMedia"
	ColToUpdate	 =  "articleID"
elseif section="news" then
	TbleToUpdate = "NewsMedia"
	ColToUpdate	 =  "newsID"
else
	response.Write "error - section to be updated unknown"
	response.end
end if

'response.Write "Delete from "& TbleToUpdate &" where "& ColToUpdate &" =" & SupportID &" and MediaID=" & MediaID
sqlconnection.execute("Delete from "& TbleToUpdate &" where "& ColToUpdate &" =" & SupportID &" and MediaID=" & MediaID )

'response.end

if page = "edit" then
	if section="event" then
		response.Redirect "/dbadmin/events/addevent.asp?EventID=" & SupportID & "&mode=edit"
	elseif section = "pr" then
		response.Redirect "/dbadmin/pr/wysiwyg/editor.asp?ID=" & SupportID
	elseif section = "artists" then
		response.Redirect "/dbadmin/artists/add.asp?TourID=" & SupportID
	elseif section = "company" then
		response.Redirect "/dbadmin/companies/add.asp?CompanyID=" & SupportID
	elseif section = "review" then
		response.Redirect "/dbadmin/reviews/edit.asp?ReviewID=" & SupportID
	elseif section = "article" then
		response.Redirect "/dbadmin/articles/edit.asp?ArticleID=" & SupportID
	elseif section = "news" then
		response.Redirect "/dbadmin/news/edit.asp?ID=" & SupportID
	ElseIf section ="specialevents" then
		response.Redirect "/dbadmin/specialevents/edit.asp?ID=" & SupportID
	end if
elseif page = "popmedia" then
	mypage = trim(Request.querystring("mypage"))
	MediaType = trim(Request.querystring("MediaType"))
	response.Redirect "pop_media.asp?SupportID=" & SupportID & "&amp;MediaType=" & MediaType & "mypage=" & mypage & "&section=" & section
end if
%>

<!--#include virtual="/ssi/dbclose.asp"-->