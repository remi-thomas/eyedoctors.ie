<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<html>
<head>
<title>Media Centre - Dublin Theatre Festival</title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
</head>
<body class="body" onLoad="setTimeout('CloseNUpdate()',1500)">

<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->
<%
SupportID	= trim(Request.Form("SupportID"))
ttlRec		= trim(Request.Form("ttlRec"))
MediaType	= trim(Request.Form("MediaType"))
mypage		= trim(Request.Form("mypage"))
FirstLetter	= trim(Request.Form("FirstLetter"))
section		= lcase(trim(Request.Form("section")))
Season		= trim(Request.Form("Season"))

if section="pr" then
	TbleToUpdate = "ComsCentre_PRMedia"
	ColToUpdate	 =  "PrID"
elseif section="news" then
	TbleToUpdate = "ComsCentre_NewsMedia"
	ColToUpdate	 =  "newsID"
else
	response.Write "error - section to be updated unknown"
	response.end
end if


If (IsNumeric(ttlRec) and ttlRec <> "") AND (IsNumeric(SupportID) and SupportID <> "") AND (IsNumeric(MediaType) and MediaType <> "") then
	For i = 1 to ttlRec
		MediaID = Request.Form("MediaID" & i)
		If (IsNumeric(MediaID) AND MediaID <> "") then
			sql ="INSERT INTO "& TbleToUpdate &"  ("& ColToUpdate &",MediaID,DocType,DateAdded,AddedBy) VALUES ("& SupportID &", "& MediaID &", "& MediaType &",getdate(),"& Session("AdminID")&")"
			'response.Write sql
			sqlconnection.execute(sql)
		end if
	Next
end if
%>

<hr class="hrgrey">
	<p class="dbtext">Files are being updated... please wait </p>
 <p align="right"><a href="#" onClick="CloseNUpdate()" class="bluetext">[close window]</a></p>



<script language="JavaScript" type="text/javascript">
	function CloseNUpdate()
	{
		//alert("close")
		window.opener.location.reload();
		window.location.href = "pop_media.asp?mypage=<%=mypage%>&MediaType=<%=MediaType%>&SupportID=<%=SupportID%>&section=<%=section%>&FirstLetter=<%=FirstLetter %>&Season=<%=Season%>"
	}
</script>

</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->