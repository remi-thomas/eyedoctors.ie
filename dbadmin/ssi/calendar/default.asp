<html>
<head>
<title>ICO Comms</title>
<%
' '''''''''''''''''''''''
' get the date from URL '
' use now by default    '
' '''''''''''''''''''''''
if not isDate(request.querystring("date")) then
	dddate = request.querystring("dddate")
	if IsDate(dddate) then
			nDate = cDate(dddate)
	else
	  nDate = now()
	end if
else
  nDate = cDate(request.querystring("date"))
end if

'field to update '
fd = request.querystring("fd")
If fd = "" then
	fd = "StartDate"
end if
' this is the end date already on display'
CurrentEndDate = request.querystring("CurrentEndDate")
CurrentEndDate = "1 january 2000"
' ''''''''''''''''''''''
' Config variables     '
' ''''''''''''''''''''''
nURL = nURL & "default.asp?fd=" & fd

If IsDate(CurrentEndDate) then
	nURL = nURL & "&CurrentEndDate=" & CurrentEndDate
else
	CurrentEndDate =  DateAdd ("d",30,now())
end	if



nDatePrev = DateAdd("m", -1, nDate)
nDateSuiv = DateAdd("m", 1, nDate)
nDay = Day(nDate)
nMonth = Month(nDate)
nYear = Year(nDate)
nDate1 = DateSerial(nYear, nMonth, 1)
Dim nTemp
nTemp = DateAdd("m", 1, nDate1)
nDay2 = Day(DateAdd("d", -1, nTemp))
nDate2 =  DateSerial(nYear, nMonth, nDay2)
nStartDate = WeekDay(nDate1, vbMonday)
nEndDate =  WeekDay(nDate2, vbMonday)
nbrCase = Day(nDate2) + (nStartDate-1) + (7-nEndDate)
nNomJour = 1


'response.Write nURL

%>

<SCRIPT language="JavaScript" type="text/javascript">
<!--
// Copyright Fusio Ltd - Dublin
// by Remi for http://ico[.]ie
// info[at]fusio.net

	function SetDate(ThisDate)
	{
		window.parent.opener.document.events.<%=fd%>.reset;
		// update the value
		window.parent.opener.document.events.<%=fd%>.value=ThisDate;

		<%
		'if IsDate(dddate) and dddate <> "" then
			if fd = "StartDate" then
				dayprefix = "f"
			else
				dayprefix = "t"
			end if
		%>
			var ThisDate = new Date(ThisDate);
			//alert(ThisDate + ' ' + ThisDate.getMonth())
			window.parent.opener.document.events.<%=dayprefix%>day.selectedIndex = (ThisDate.getDate()-1);
			window.parent.opener.document.events.<%=dayprefix%>month.selectedIndex = ThisDate.getMonth();
			//window.parent.opener.document.events.<%=dayprefix%>month.selectedIndex = ThisDate.getMonth();

			var Myyear =  ThisDate.getFullYear();

			for (i=0; i<= window.parent.opener.document.events.<%=dayprefix%>year.length; i++)
				{
					if ( window.parent.opener.document.events.<%=dayprefix%>year[i].value == eval(Myyear) )
					{
						window.parent.opener.document.events.<%=dayprefix%>year.selectedIndex = i
						break
					}
				}
		<%
		'end if
		%>

		window.parent.close();
	}

// menu function
	startList = function()
	{
	if (document.all&&document.getElementById)
	{
		navRoot = document.getElementById("dmenu");
		for (i=0; i<navRoot.childNodes.length; i++)
		{
			node = navRoot.childNodes[i];
			if (node.nodeName=="LI")
			{
				node.onmouseover=function()
				{
					this.className+=" over";
				}
				node.onmouseout=function()
				{
					this.className=this.className.replace(" over", "");
				}
			}
		}
	}
}

window.onload=startList;
//-->
</script>
<style type="text/css">
TH.calMonth { font-size : 11px; font-weight: bold; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #ffffff; background : #006666; }
TD.calSing { font-size : 9px; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #fff; background : #006666; }
TD.calWeek { font-size : 9px; font-family : verdana, geneva, helvetica, sans-serif; color : #006666; background : #a9cfd0; }
TD.calDay { font-size : 9px; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #006666; background : #cce4e5; }
TD.calDay A { font-size : 9px; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #006666; background : #cce4e5; text-decoration: underline; }
TD.calDay A:hover { font-size : 9px; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #ffffff; background : #cce4e5; text-decoration: none; }
TD.calDaySel { font-size : 9px; font-weight: bold; text-align : center; font-family : verdana, geneva, helvetica, sans-serif; color : #006666; background : #cce4e5; }
TD.calSing A{color : #fff; text-decoration: none; }

/* menu requirements for functionality */

.topmenuul{	list-style-type:none;margin:0;padding:0;}
.topmenuli{list-style-type:none;float:left;}
.topmenutitle{display:block;}
.submenuul{list-style-type:none;position:absolute;margin:0;padding:0;display:none;}
.submenuli a{display:block;width:55px;}
li:hover ul , li.over ul{ /* lists nested under hovered list items */
	display: block;
}
#dmenu li>ul {top: auto;left: auto;}

/* menu design */

#dmenu {font-family:verdana, geneva, helvetica, sans-serif;font-size:9px;}

.topmenutitle {
text-decoration:underline;
border:0px solid #C0C8D6;
padding:3px;
font-family : verdana, geneva, helvetica, sans-serif;
color : #ffffff;
background : #006666;}

.submenuul {
	margin-top:-1px;
	background-color: #006666;
	background-position: top left;
	background-repeat: repeat-x;
	border-top:1px solid #006666;
	border-bottom:1px solid #FFFFFF;
	border-right:0px;
	border-left:1px solid #FFFFFF;
	font-family:verdana, geneva, helvetica, sans-serif;
	font-size:9px;
}

.submenuli a{
	padding:1px;
	text-decoration: none;
	color: #FFFFFF;

}

.submenuli a:hover{
	text-decoration: none;
	background: #C0C8D6;
	color: #006666;
}

</style>
</head>
<body bgcolor="#ffffff" style="margin=0" onblur="window.focus()" style="scrollbar-face-color:#C0C8D6;scrollbar-arrow-color:#FFFFFF;scrollbar-track-color:#C0C8D6;scrollbar-darkshadow-color:#C0C8D6;">
<center>


<table border="0" cellspacing="1" cellpadding="3" vspace="0" hspace="0">
  <!-- Start month, year and button header  -->
  <tr>
		<%
		If nDate > now() then
		%>
		<th class="calMonth">
		<a href="<%=nURL%>&date=<%=nDatePrev%>"><img src="b_prev.gif" width="14" height="13" border="0" vspace="2" align="absmiddle" alt="Mois précédent"></a>
		</th>
		<%
		else
		%>
		<th class="calMonth">&nbsp;</a></th>
		<% end if%>

  <th colspan="5" class="calMonth">
	  <div id="main">
		<ul class="topmenu" id="dmenu">
			<li class="topmenuli"><a href="#" class="topmenutitle"><%=MonthName(nMonth)%></a>
				<ul class="submenuul">
					<%
					For I = nMonth + 1 to 11 + nMonth
						If I > 12 then
							ThisMonth = abs(12 - I)
						else
							ThisMonth = I
						end if
					%>
					<li class="submenuli"><a href="<%=nURL%>&date=01/<%=ThisMonth%>/<%=nYear%>"><%=MonthName(ThisMonth)%></a></li>
					<% next %>
				</ul>
			</li>
			<li class="topmenuli"><a href="#" class="topmenutitle"><%=nYear%></a>
				<ul class="submenuul">
					<li class="submenuli"><a href="<%=nURL%>&date=01/<%=nMonth%>/<%=Year(now)%>"><%=Year(now)%></a></li>
					<li class="submenuli"><a href="<%=nURL%>&date=01/<%=nMonth%>/<%=Year(now)+1%>"><%=Year(now)+1%></a></li>
					<li class="submenuli"><a href="<%=nURL%>&date=01/<%=nMonth%>/<%=Year(now)+2%>"><%=Year(now)+2%></a></li>
				</ul>
			</li>
	</div>
</th>
<th class="calMonth"><a href="<%=nURL%>&date=<%=nDateSuiv%>"><img src="b_suiv.gif" width="14" height="13" border="0" vspace="2" align="absmiddle" alt="Mois suivant"></a></th>
</tr>
<!-- End month, year and button header  -->
<!-- Start weekday header -->
<tr>
<% for n = 1 to 7 %>
  <td width="14%" align="center" valign="middle" class="calWeek"><%=WeekDayName(n, true, vbMonday)%></td>
<% next ' Boucle des noms du jour / WeekName for %>
</tr>
<!-- End weekday header -->
<!-- Start day's loop -->
<%
Dim njTemp
For n = 1 to nbrCase
  njTemp = n - nStartDate+1
	if nNomJour = 1 then
		response.write "<tr>"
	end if
	If IsThePast(njTemp,nMonth,nYear) then
		response.write "<td align=""center"" valign=""middle"" class=""calDaySel"">"
		setlink = false
	else
		response.write "<td align=""center"" valign=""middle"" class=""calDay"">"
		setlink = true
	end if
	if n <  (nStartDate) or n > (nDay2+nStartDate-1) then
		response.write "&nbsp;"
	else
		if setlink then
			response.write "<a href=""JavaScript:onClick=SetDate('" & FormatDateTimeIRL(DateSerial(nYear, nMonth, njTemp),1) & "')"">"
			response.write (njTemp) & "</a>"
		else
			response.write (njTemp)
		end if
	end if

  response.write "</td>"
	if nNomJour = 7 then
		response.write "</tr>"
	end if
	if nNomJour = 7 then
		nNomJour = 1
	else
		nNomJour = nNomJour + 1
	end if
next ' Boucle des jour / Day for
%>
<!-- End day's loop -->
<tr>
	<td class="calSing" colspan="7"><a href="http://www.fusio.net">&copy fusio.net</a></td>
</tr>
</table>

<%
Function IsThePast(DayTemp,MonthTemp,YearTemp)
	tempdate = DayTemp & "/" & MonthTemp & "/" & YearTemp
	'response.Write "<hr>" & tempdate & " " & IsDate(tempdate)
	If tempdate="" or not IsDate(tempdate) Then
		IsThePast = true
		Exit Function
	else
		tempdate = FormatDateTimeIRL(tempdate,1)
		If DateDiff("d",now,tempdate) < 0  then
			IsThePast = true
			Exit Function
		end if
	end if
end function
%>


</body>
</html>