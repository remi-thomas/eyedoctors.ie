<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html>
<head>
<title>ICO Website Administration  - Login</title>
<!--#include virtual="/ssi/fct/common.asp"-->
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<meta http-equiv="Cache-Control" content="no-cache"/>
<body>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<div id="nav">&nbsp;</div>
<div id="content">
<form method="post" action="login.asp">
  <h1>User Login</h1>
  <p>Please log in to the ICO Website Administration  with your username and password below.</p>
  <div id="login">

    <p><label>Your Email:</label> <input name="email" type="text" class="TextInput" maxlength="75"></p>
    <p><label>Your Password:</label> <input name="pssword" type="password" class="TextInput" maxlength="75"></p>
    <div><input name="Submit" type="submit" class="TextInput" value="LOGIN"></div>
  </div>
  <input name="referer" type="hidden" value="<%=request.querystring("ThisPage")%>">
</form>
<%
errorcode = request("errorcode")
If IsNumeric(errorcode) and errorcode<>"" then
	response.Write "<table width='450' border='0' align='center'>" & VbNewline & _
	"	<tr>"  & VbNewline & _
	"		<td class='bluetext'><b>" & VbNewline

	'Error Codes
	'1 -  invalid username
	'2 -  incorrect password
	'3 -  incorrect password

	Select Case errorcode
		Case 1
			response.Write "Your User Name is incorrect .. please try again "
		Case 2, 3
			response.Write "Your password is incorrect .. please try again "
		Case Else
	End Select

	response.Write "</b><br /><u>The following data has been recorded:</u>" & _
	"<br /> Your IP: " & Request.ServerVariables("REMOTE_ADDR") & _
	"<br /> " & Request.ServerVariables("HTTP_USER_AGENT")

	response.Write"		</td>" & VbNewline & _
	"	</tr>" & VbNewline & _
	"</table> " & VbNewline

end if

%>
<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
