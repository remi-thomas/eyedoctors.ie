<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp" -->
<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/dbadmin/ssi/fct/media.asp"-->
<!--#include virtual="/ssi/fct/common.asp"-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>ICO - Website Administration </title>
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<script src="/dbadmin/ssi/lib/nav.js" type="text/javascript"></script>
</head>
<body class="body">
<%

cnt  = 0
' max display = nb records to be displayed'
maxdisplay = 20

mypage = request("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
	mypage=1
end if

FirstLetter = left(trim(request.querystring("FirstLetter")),1)

MediaType = request.querystring("MediaType")
if not IsNumeric(MediaType) or MediaType = "" then
	MediaType = 0
end if
%>
<!--#include virtual="/dbadmin/ssi/incl/top.asp" -->
<!--#include virtual="/dbadmin/ssi/incl/nav.asp" -->
<h1>View Media</h1>

<div id="mediaoptions">
<h3>View by Type</h3>
<p>
		<%
		set rs =  sqlconnection.execute("SELECT ID, MediaName FROM MediaType order by MediaOrder")
		Do until rs.eof
		If rs(0) = cint(MediaType) then
			MediaCurrentlyDisplay = rs(1)
		else
		end if
		response.Write "<a href=""media.asp?MediaType=" & trim(rs(0)) & "&amp;media="& trim(rs(1))&"&amp;season="& season &""">" & rs(1) & "</a>" & VbNewline & _
		" | " & vbnewline
		rs.Movenext
		Loop
		%>
		<a href="media.asp">Show All</a></p>

<%
set rs = sqlconnection.execute("exec usp_SEL_AZ_Media " & MediaType)
if not rs.eof then
%>
<h3>A to Z</h3>
<% if  MediaCurrentlyDisplay <> "" then response.Write  "for " & MediaCurrentlyDisplay %>
<p>
			<%
			Do until rs.eof
				response.Write "<a href=""media.asp?FirstLetter=" & Ucase(rs(0)) & "&amp;MediaType="& MediaType &""" "
				if lcase(FirstLetter) = lcase(rs(0)) then
					response.Write ""
				else
					response.Write "class=""bluetext"""
				end if
				response.Write">" & Ucase(rs(0)) & "</a> | "
			rs.Movenext
			Loop
			%>
			<a href="media.asp?MediaType=<%=MediaType%>">ALL</a>
</p>
<%
end if
%>
</div>

<%
set rs = Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3
rs.cachesize = maxdisplay
rs.Open "usp_SEL_MediaPerType "& MediaType &",0,'" & FirstLetter & "'", sqlconnection

	'SELECT m.ID, m.Path,m.FileName,m.Preview,m.FileSize
	',m.Caption,m.MediaType,m.Online ,
	'case when m.ID in (select p.MediaID from ComsCentre_PRMedia p where p.prID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinPR,
	'case when m.ID in (select e.MediaID from EventMedia e where e.eventID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinEvent,
	'case when m.ID in (select am.MediaID from ArtistsMedia am where am.ArtistID=@SupportID) then 1
	'else 0 end as  AlreadySelectedinArtist,
	'(select count(AssociatedMediaID) from MediaAssociated where MediaID=@SupportID)  as AssociatedMedia



If rs.eof then
	response.Write "<br /><p class=""bluetext"">There are no '"& Lcase(MediaTypeText(MediaType)) &"' files stored in the database. </p>" & VbNewline
	response.end
end if

rec = 1
rs.movefirst
rs.pagesize = maxdisplay
rs.absolutepage = mypage
maxpages = cint(rs.pagecount)
' display the total of records returned by sql query'
intRecordCnt = rs.RecordCount

if maxpages > 1 then
	response.Write "<p"
	call Paging
	response.Write "</p>" & VbNewline
end if


	Do until rs.eof  or (rec > maxdisplay)
	If rec mod 2 = 0 then
		BgColor = "#D6E8F5"
	else
		BgColor = "#E4F1FA"
	end if
	%>
		<div class="medialist">
			<%
			response.Write "<h3>" & DisplayText(rs("Caption")) & "</h3>"
	


			if rs("MediaType") = 7 then 'youtube / streaming
				response.Write "" & MediaTypeGraph(rs("MediaType")) & ""

				response.Write "Path: " & DisplayText(rs("path"))

				if instr(rs("path"),"youtube.com") > 1 then
					'http://www.youtube.com/watch?v=OkBdv2yKDbw should be http://www.youtube.com/v/OkBdv2yKDbw&hl=en&fs=1&rel=0&color1=0x3a3a3a&color2=0x999999&border=1"
					youtubeembedded = Replace ( DisplayText(rs("path")), "/watch?v=", "/v/")
					Appendyoutubeparam = "&hl=en&fs=1&rel=0&color1=0x3a3a3a&color2=0x999999&border=1"
					%>
					<br/><a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="View media"><strong>Watch (on/off)</strong></a>
					<span id="field<%=rec%>" style="display:none;margin:0">

					<object width="425" height="349">
					<param name="movie" value="<%=youtubeembedded%><%=Appendyoutubeparam%>">
					</param>
					<param name="allowFullScreen" value="true"></param>
					<param name="allowscriptaccess" value="always"></param>
					<embed src="<%=youtubeembedded%><%=Appendyoutubeparam%>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="349"></embed>
					</object>


					</span>
				<%
				elseif instr(rs("path"),"dailymotion.com") > 1 then
					Dmotion = rs("path")
				%>

					<br/><a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="View media"><strong>Watch (on/off)</strong></a>
					<span id="field<%=rec%>" style="display:none;margin:0">
						<div><object width="480" height="381">
						<param name="movie" value="<%=Dmotion%>&colors=background:A9D0D1;special:A9D0D1;&related=0&canvas=medium"></param>
						<param name="allowFullScreen" value="true"></param>
						<param name="allowScriptAccess" value="always"></param>
						<embed src="<%=Dmotion%>&colors=background:A9D0D1;special:A9D0D1;&related=0&canvas=medium" type="application/x-shockwave-flash" width="480" height="381" allowFullScreen="true" allowScriptAccess="always"></embed>
						</object><br />
						</div>
					</span>
				<%
				end if
				%>

			<%
			elseif rs("MediaType") = 1 then 'images
				response.Write "" & MediaTypeGraph(rs("MediaType")) & ""

				if Len(rs(3)) > 4 then
			%>
				<a href="JavaScript:onClick=ShowMe('field<%=rec%>')" title="View media"><strong>Preview (on/off)</strong></a>
				<span id="field<%=rec%>" style="display:none;margin:0">
					<br/>
					<%
					if len( rs(2)) > 3 then
					%>
					<a href="<%response.write rs(1) &"jpg/"& rs(2)%>" target="media">
					<img src="<%response.write rs(1) & rs(3)%>" alt="Click For Full Size" style="margin:0" border="0"></a>
					<%
					else
					%>
					<img src="<%response.write rs(1) & rs(3)%>" alt="Click For Full Size" style="margin:0" border="0">
					<%
					end if
					%>
				</span>
			<%	end if ' rs(3)) > 4%

			else ' mediatype
				response.write DocTypeGraph(Right(rs(2),3))
				'response.write rs(1) & rs(2)
				response.write "<a href=""" & rs(1) & rs(2)  & """ target=""_readdoc"" style=""color: #000000;text-decoration:underline"">" & rs(1) & rs(2) & "</a>" & vbnewline

				response.write " (" & rs("FileSize") & "KB)"

			end if

			response.Write "<br style=""clear:both""><div><em> Published on " & FormatDateTime (rs("DatePublished"), vblongdate) & " by " & rs("Author") & "</em></div>" & VbNewline

			' MediaType = 1 = preview
		%>
		</div>
		<br style="clear:both">
		<%
	rs.movenext
	rec = rec + 1
loop
%>
</table>

	<%
if maxpages > 1 then
	response.Write "<p>"
	call Paging
	response.Write "</p>" & VbNewline
end if

rs.close
set rs=nothing


sub Paging()
	if maxpages > 1 then
		pge = MyPage
		Response.Write "Page "

		for counter = 1 to maxpages
			if counter <> cint(pge) then
				Response.Write "<a  class=""bluetext"" href=""view.asp?mypage=" & counter & "&amp;DocID=" & DocID
				If MediaType <> "" Then
						response.write "&amp;MediaType=" & MediaType
				End if
				If Season <> "" Then
						response.write "&amp;Season=" & Season
				End if
				Response.Write """>" & counter & "</a>"
			else
				Response.Write "<i  class=""bluetext"">" & counter & "</i>"
			end if
			If counter < maxpages then
				Response.write	" � "
			end if
		next
	end if
end sub
%>
<script language="JavaScript" type="text/javascript">
//########################################
//## script copyright fusio.net         ##
//## developped for Dub Theatre IRL ##
//## by remi [at] fusio<.>net           ##
//########################################
function ShowMe(ThisLayer)
{
	// expand or contract
	document.getElementById(ThisLayer).style.display = (document.getElementById(ThisLayer).style.display == 'block') ? 'none' : 'block';
	cvalue = (document.getElementById(ThisLayer).style.display == 'block') ? 1 : 2;
}
</script>

<!--#include virtual="/dbadmin/ssi/incl/base.asp" -->
</body>
</html>
<!--#include virtual="/ssi/dbclose.asp"-->
