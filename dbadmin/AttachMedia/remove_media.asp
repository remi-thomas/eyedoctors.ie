<!--#include virtual="/dbadmin/ssi/fct/chkstatus.asp " -->
<!--#include virtual="/ssi/dbconnect.asp"-->
<%
SupportID	= trim(Request.querystring("SupportID"))
MediaID		= trim(Request.querystring("MediaID"))
page		= trim(Request.querystring("page"))
section		= left(lcase(trim(Request.querystring("section"))),10)
If not IsNumeric(SupportID) or SupportID = "" then
	response.Write "Error! SupportID ("& SupportID &") is not numeric : " & IsNumeric(SupportID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

If not IsNumeric(MediaID) or MediaID = "" then
	response.Write "Error! MediaID ("& MediaID &") is not numeric : " & IsNumeric(MediaID) & vbnewline
	response.Write "<bd /><a href=""javascript:history.go(-1)"">Go back and try again</A>"
	response.end
end if

if section="pr" then
	TbleToUpdate	= "ComsCentre_PRMedia"
	ColToUpdate	=  "PrID"
elseif section="news" then
	TbleToUpdate	= "ComsCentre_NewsMedia"
	ColToUpdate	=  "newsID"
else
	response.Write "error - section to be updated unknown"
	response.end
end if


'response.Write "Delete from "& TbleToUpdate &" where "& ColToUpdate &" =" & SupportID &" and MediaID=" & MediaID
sqlconnection.execute("Delete from "& TbleToUpdate &" where "& ColToUpdate &" =" & SupportID &" and MediaID=" & MediaID )

'response.end

if page = "edit" then
	if section="pr" then
		response.Redirect "/dbadmin/pr/wysiwyg/editor.asp?ID=" & SupportID
	elseif section = "news" then
		response.Redirect "/dbadmin/news/edit.asp?NewsID=" & SupportID
	end if
elseif page = "popmedia" then
	mypage = trim(Request.querystring("mypage"))
	MediaType = trim(Request.querystring("MediaType"))
	response.Redirect "pop_media.asp?SupportID=" & SupportID & "&amp;MediaType=" & MediaType & "mypage=" & mypage & "&section=" & section
end if
%>

<!--#include virtual="/ssi/dbclose.asp"-->