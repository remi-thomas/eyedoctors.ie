<!-- Side: Menu -->
          <dl>
            <dt><a href="/your-eye-health/">Your Eye Health</a></dt>
            <dd>
              <ul>
                <li <%if SiteSubSection="leaflets" then response.write " class=""active"""%>><a href="/your-eye-health/Patient-Information-Leaflets.asp">Patient Information Leaflets</a></li>

              <li <%if SiteSubSection="LookingAfterYourEyes" then response.write " class=""active"""%>><a href="/your-eye-health/looking-after-your-eyes.asp">Looking After Your Eyes</a></li>

              <li <%if SiteSubSection="eyeconditions" then response.write " class=""active"""%>><a href="/your-eye-health/eye-conditions.asp">Eye Conditions</a></li>
              <li <%if SiteSubSection="Children" then response.write " class=""active"""%>><a href="/your-eye-health/screening.asp">Screening</a></li>

            

              <li <%if SiteSubSection="FAQ" then response.write " class=""active"""%>><a href="/your-eye-health/faq.asp">Your Questions Answered</a></li>
              <li <%if SiteSubSection="emergency" then response.write " class=""active"""%>><a href="/your-eye-health/in-an-emergency.asp">In An Emergency</a></li>
              <li <%if SiteSubSection="supportgroup" then response.write " class=""active"""%>><a href="/your-eye-health/support-groups.asp">Patient Support</a></li>
              <li <%if SiteSubSection="LowVisiion" then response.write " class=""active"""%>><a href="/your-eye-health/Low-Vision-services.asp">Low Vision Services</a></li>
              </ul>
            </dd>
          </dl>
