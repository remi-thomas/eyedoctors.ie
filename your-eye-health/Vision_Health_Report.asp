<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
<title>Eye Conditions | Eye Doctors: Irish College of Ophthalmologists</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" href="/css/style.css" type="text/css" />
<script type="text/javascript" src="/ssi/js/jquery-1.3.2.min.js"></script>
<link type="text/css" href="/css/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="/ssi/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/ssi/js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript">
	$(function(){
		// Accordion
		$("#accordion").accordion({
			header: "h2",
			autoHeight: false,
			collapsible: true
		});
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); },
			function() { $(this).removeClass('ui-state-hover'); }
		);
	});
</script>

</head>
<body id="visitors" class="visitors3">
<div id="pagewidth">
  <!--#include virtual="/ssi/incl/header.asp" -->
  <div id="wrapper" class="clearfix">
    <div id="maincol">
      <div id="contentbox">
        <div id="contentbg">
          <h1>Coalition for Vision Health Present Strategic Framework Report at Leinster House</h1>
             <div id="accordion">
              <!--#include file="Vision_Health_Report.htm" -->
		</div>
        </div>
      </div>
    </div>
    <!--#include virtual="/ssi/incl/leftcol.asp" -->
  </div>
  <!--#include virtual="/ssi/incl/footer.asp" -->
</div>
</body>
</html>