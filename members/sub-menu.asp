
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|
dim SubMenu
SubMenu = Lcase(Left(request.querystring("SubMenu"),15))
if len(SubMenu) < 4 then
  SubMenu  ="members"
end if
dim MemberMenuTitle
select case SubMenu
  case "members"
    MemberMenuTitle = "ICO Members"
  case "guidelines"
    MemberMenuTitle = "Guidelines"
end select
'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|

MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" For GP"
Metadescription   = "Useful Links for doctors "
ArticleURL        = DomainName & "members/useful-links.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "menus | Health Service Documents | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Members" ' highlight correct menu tab'
SiteSubSection    = "menus"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "For Doctors"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = MemberMenuTitle
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-12 maincol">
            <%
            select case SubMenu
            case "members"
            %> <ul><!--#include virtual="/ssi/incl/menu-doctors-members.asp" --></ul><%
            case "guidelines"
            %> <!--#include virtual="/ssi/incl/menu-doctors-guidelines.asp" --><%
            case "training"
            %> <!--#include virtual="/ssi/incl/menu-trainee-medical.asp" --> <!--#include virtual="/ssi/incl/menu-trainee-surgical.asp" --><%
            case else
            %> <!--#include virtual="/ssi/incl/menu-doctors-members.asp" --><%
            end select
            %>
          </div>


    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>