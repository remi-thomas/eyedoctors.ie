
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


Dim PHOTOSET_ID, Page
PHOTOSET_ID 	= ValidateText(Request.QueryString("PhotoSet_ID"))

PHOTOSET_ID	= request.querystring("PHOTOSET_ID")
Page		= request.querystring("Page")
If Not IsNumeric(Page) Or Page = "" Then
	Page = 1
End if

If PHOTOSET_ID = "" then
	PHOTOSET_ID = "72157625737596007"
End If

dim FlickrPhotoSet
dim FlickrSetTitle, FlickrSetDesc, FlickrSetTtlPhoto
FlickrPhotoSet = flickrGetSetInfo(PHOTOSET_ID)
if isArray(FlickrPhotoSet) Then
	FlickrSetTitle	= FlickrPhotoSet(0)
	FlickrSetDesc	= FlickrPhotoSet(1)
	FlickrSetTtlPhoto = FlickrPhotoSet(2)
Else
	FlickrSetTitle = "Irish College of Ophthalmologists "  & Year(Now())
	FlickrSetTtlPhoto = ""
End if


MetaAbstract      = FlickrSetDesc & " | Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" | Photo Gallery"
Metadescription   = FlickrSetTitle & " | "& FlickrSetDesc &" |  Photo Gallery "
ArticleURL        = DomainName & "members/Photo-Gallery/default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = FlickrSetTitle & " | Photo Gallery | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Members" ' highlight correct menu tab'
SiteSubSection    = "gallery"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "For Doctors"
BreadCrumbArr (0,1) = "/members/default.asp"
BreadCrumbArr (1,0) = "Photo Gallery"
BreadCrumbArr (1,1) = "/members/photo-gallery/default.asp"
BreadCrumbArr (2,0) = FlickrSetTitle
BreadCrumbArr (2,1) = ""


BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
''

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
          		<div id="contentbg">



          <h1><%=FlickrSetTitle%></h1>
			<%
			If FlickrSetDesc <> "" then
				response.write "<p>" & FlickrSetDesc & "</p>" & vbnewline
			end if	
			Dim Html : Html = ""
			dim FlickrPhotos
			FlickrPhotos = flickrGetPhotosetsPhoto(flickrGetUrlPhotosets("",1,42,Page,"photos",PHOTOSET_ID))
			if isArray(FlickrPhotos) then
				Html = Html & "<ul class=""photoset"">"
				dim i, d
				for i=0 to UBound(FlickrPhotos)
					if isArray(FlickrPhotos(i)) then
					   d = FlickrPhotos(i)
						Html = Html & vbtab & vbtab & vbtab & "<li>"&_
						"<a href=""/members/photo-gallery/image.asp?PhotoSet_ID="& PHOTOSET_ID &"&amp;PhotoID="&d(4)&""" title="""+d(0)+""">"&_
						"<img src="""+d(2)+""" alt="""+d(0)+""" />"&_
						"</a>" &_
						"</li>" & vbnewline
					end if
				next
				Html = Html & "</ul>"
			Else
				response.write "<br/> <span> Err#2 error</span>"
			end if
			Response.Write(Html)

			%>
			<%
			If FlickrSetTtlPhoto <> "" Then
				response.write "<br/ style=""clear:both;""><em>This set contains "& FlickrSetTtlPhoto & " photos.</em>"
			End If
			%>


</div>

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/members/Members-Menu.asp" -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>



<%
' get the API url flickr.photosets.getPhotos
function flickrGetUrlPhotosets(byval extras,byval privacy_filter,byval per_page,byval page,byval media,byval PHOTOSET_ID)
	dim url
	url = "https://www.flickr.com/services/rest/?method=flickr.photosets.getPhotos" &_
						   "&api_key="&API_KEY &_
						   "&photoset_id="&PHOTOSET_ID
	'response.write "<br/>url " & url
	if not isEmpty(per_page) then url = url & "&per_page="&per_page
	if not isEmpty(page) then url = url & "&page="&page
	if not isEmpty(media) then url = url & "&media="&media
	if not isEmpty(privacy_filter) then url = url & "&privacy_filter="&privacy_filter
	if not isEmpty(extras) then url = url & "&extras="&extras
	
	'response.write URL

	flickrGetUrlPhotosets = url

end function

' get the ASPI url flickr.photos.getInfo
function flickrGetUrlPhotoInfo(byval photo_id, byvalsecret)
	url = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo" &_
		"&api_key="&API_KEY &_
		"&photo_id="&photo_id &_
		 "&secret="&secret
	flickrGetUrlPhotoInfo = url
end function

' get the ASPI url flickr.photos.getInfo
function flickrGetUrlPhotoInfo(byval photo_id,byval secret)
	dim url
	url = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo" &_
						   "&api_key="&API_KEY &_
						   "&photo_id="&photo_id &_
						   "&secret="&secret
	flickrGetUrlPhotoInfo = url
end Function

' retrive photo information
' return an array (title, description, thumb, preview)
Function flickrGetPhotoInfo(byval flickrAPIUrl)

	dim xmlhttpPhoto,intTimeout
	intTimeout = 5000
	'on error resume next
	set xmlhttpPhoto = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttpPhoto.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttpPhoto.Open "GET", flickrAPIUrl, false
	xmlhttpPhoto.Send
	'if err <> 0 then
	'	response.write "<br/>Err #3"
	'	exit function
	'end if

	dim xmlPhotoDetail
	set xmlPhotoDetail = Server.CreateObject("Microsoft.XMLDOM")
	xmlPhotoDetail.async = false
	xmlPhotoDetail.setProperty "SelectionLanguage", "XPath"
	xmlPhotoDetail.loadxml(xmlhttpPhoto.ResponseText)

	dim dPhoto(5)
	dim photoNode, photoDetailNode
	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo/title")
	For Each photoDetailNode in photoNode
		dPhoto(0) = photoDetailNode.Text
	next

	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo/description")
	For Each photoDetailNode in photoNode
		dPhoto(1) = photoDetailNode.Text
	next
	dim photourl
	Set photoNode = xmlPhotoDetail.selectNodes("/rsp/photo")
	For Each photoDetailNode in photoNode
		photourl = "http://farm" & photoDetailNode.getAttribute("farm") & ".static.flickr.com/" &_
									photoDetailNode.getAttribute("server") & "/" &_
									photoDetailNode.getAttribute("id") & "_" &_
									photoDetailNode.getAttribute("secret")
		dPhoto(2) = photourl & "_s.jpg"
		dPhoto(3) = photourl & ".jpg"
		dPhoto(4) = photoDetailNode.getAttribute("id")
	next

	Set photoDetailNode = Nothing
	Set photoNode = Nothing
	Set xmlPhotoDetail = Nothing
	flickrGetPhotoInfo = dPhoto
End Function


' retrive photos in a photosets
' return array
function flickrGetPhotosetsPhoto(byval flickrAPIUrl)

	'response.write "<br/>flickrGetPhotosetsPhoto : flickrAPIUrl <span style=""color:blue"">" & flickrAPIUrl & "</span><br/"
	dim result()

	dim xmlhttp,intTimeout
	intTimeout = 5000

	'on error resume next
	set xmlhttp = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttp.Open "GET", flickrAPIUrl, false
	xmlhttp.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttp.Send
	'if err <> 0 then
	'	response.write "<br/><span>ERR#1 -  "& err &" </span>" 
	'	exit function
	'end if
	dim source1
	set source1 = Server.CreateObject("Microsoft.XMLDOM")
	source1.async = false
	source1.setProperty "SelectionLanguage", "XPath"
	source1.loadxml(xmlhttp.ResponseText)
	dim PhotosNodes, PhotoNodeItem
	Set PhotosNodes = source1.selectNodes("/rsp/photoset/photo")

	dim photoCount : photoCount = 0
	For Each PhotoNodeItem in PhotosNodes
		redim preserve result(photoCount+1)
		result(photoCount) = flickrGetPhotoInfo(flickrGetUrlPhotoInfo(PhotoNodeItem.getAttribute("id"),PhotoNodeItem.getAttribute("secret")))
		photoCount = photoCount + 1
	Next

	Set PhotoNodeItem = Nothing
	Set PhotosNodes = Nothing
	Set source1 = Nothing

	flickrGetPhotosetsPhoto = result

end function


' retrive set information
' return an array (set title, description,ttl photos)
Function flickrGetSetInfo(photoset_id)
	dim flickrAPIUrl
	flickrAPIUrl ="https://api.flickr.com/services/rest/?method=flickr.photosets.getInfo&api_key="&API_KEY &"&photoset_id=" & photoset_id
	'response.write "<br/>" & flickrAPIUrl
	dim xmlhttpPhoto,intTimeout
	intTimeout = 5000
	'on error resume next
	set xmlhttpPhoto = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttpPhoto.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttpPhoto.Open "GET", flickrAPIUrl, false
	xmlhttpPhoto.Send
	'if err <> 0 then exit function

	dim xmlSetDetail
	set xmlSetDetail = Server.CreateObject("Microsoft.XMLDOM")
	xmlSetDetail.async = false
	xmlSetDetail.setProperty "SelectionLanguage", "XPath"
	xmlSetDetail.loadxml(xmlhttpPhoto.ResponseText)

	dim dPhoto(3)
	dim photoNode,photoDetailNode
	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset/title")
	For Each photoDetailNode in photoNode
		dPhoto(0) = photoDetailNode.Text
	next

	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset/description")
	For Each photoDetailNode in photoNode
		dPhoto(1) = photoDetailNode.Text
	next

	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset")
	For Each photoDetailNode in photoNode
		dPhoto(2) = photoDetailNode.getAttribute("photos") 
	next

	Set photoDetailNode = Nothing
	Set photoNode = Nothing
	Set xmlSetDetail = Nothing
	flickrGetSetInfo = dPhoto
End Function

%>