

<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'______________________________________________________________________
'| Query string data                                                   |
'______________________________________________________________________|
Dim PHOTOSET_ID, PhotoID,ThisPhotoID, i
PHOTOSET_ID 	= ValidateText(Request.QueryString("PhotoSet_ID"))
PhotoID 	= ValidateText(Request.QueryString("PhotoID"))
ThisPhotoID	= PhotoID

If Not isNumeric(PHOTOSET_ID) OR PHOTOSET_ID = "" Then
	Response.Redirect "default.asp?ThisErr=5&err=Invalid%20galleryID(" & server.urlencode(PHOTOSET_ID) & ")%20-%20image.asp"
   End If
If Not isNumeric(PhotoID) OR PhotoID = "" Then
	Response.Redirect "default.asp?ThisErr=3&err=Invalid%20PhotoID(" & server.urlencode(PhotoID) & ")%20-%20image.asp"
End If
'
'______________________________________________________________________
'| API data                                                            |
'______________________________________________________________________|'
dim flickrUrl
dim FlickrPhotoTitle, FlickrPhotoDesc, FlickrPhotoURL
dim FlickrSetTitle, FlickrSetDesc, FlickrSetTtlPhoto
dim nextURL, prevURL, PrevPhotoID
dim ThisImagei, photos, thistitle
dim xmlhttp
dim xmldom
dim thisPhoto


'## API: get set details ##'
dim FlickrPhotoSet
FlickrPhotoSet = flickrGetSetInfo(PHOTOSET_ID)
if isArray(FlickrPhotoSet) Then
	FlickrSetTitle	= FlickrPhotoSet(0)
	FlickrSetDesc	= FlickrPhotoSet(1)
	FlickrSetTtlPhoto = FlickrPhotoSet(2)
Else
	FlickrSetTitle		= "Irish College of Ophthalmologists " & Year(Now())
	FlickrSetDesc		= ""
	FlickrSetTtlPhoto = ""
End if



flickrUrl = "https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&photo_id=" & PhotoID & "&api_key=" & API_KEY
Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLhttp")
xmlhttp.Open "GET",flickrUrl,False
call xmlhttp.setTimeouts(0, 10000, 10000, 10000)
xmlhttp.Send

'CONVERT TO XML DOM
Set xmldom = Server.CreateObject("Microsoft.XMLDOM")
Set thisPhoto = Server.CreateObject("Microsoft.XMLDOM")
xmldom.async = False
xmldom.loadxml(xmlhttp.ResponseText)
set photos = xmldom.getElementsByTagName("size")
dim Size1, imagepath
for i = 1 to photos.length
	Set thisPhoto = photos.item(i-1)
	Size1 = thisPhoto.attributes.getNamedItem("label").text
	if Size1 = "Medium" then
		imagepath = thisPhoto.attributes.getNamedItem("source").text
		'FlickrPhotoURL = "<img id=""main-image"" src=""" & imagepath & """ title=""" && """ alt="""" />"
	end if
Next


'## API: get info ##'
flickrUrl = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&photo_id=" & PhotoID & "&api_key=" & API_KEY
'response.write "<br/>" & flickrUrl
xmlhttp.Open "GET",flickrUrl,False
xmlhttp.Send
xmldom.loadxml(xmlhttp.ResponseText)
set thisphoto = xmldom.getElementsByTagName("description").item(0)
FlickrPhotoDesc = trim(replace(replace(thisphoto.text,chr(13),""),chr(10)," "))
set thistitle = xmldom.getElementsByTagName("title").item(0)
FlickrPhotoTitle = trim(replace(replace(thistitle.text,chr(13),""),chr(10)," "))

'define photo link here
FlickrPhotoURL = "<img id=""main-image"" src=""" & imagepath & """ title=""" & FlickrPhotoTitle & """ alt=""" & FlickrPhotoTitle & """ class=""img-responsive""/>"


'## API: get gallery ##'
flickrUrl = "https://www.flickr.com/services/rest/?method=flickr.photosets.getPhotos&photoset_id=" & PHOTOSET_ID & "&api_key=" & API_KEY
'response.write " " & flickrUrl
'GET XML VIA HTTP REQUEST
Set xmlhttp = Server.CreateObject("Microsoft.XMLHTTP")
xmlhttp.Open "GET",flickrUrl,False
xmlhttp.Send

 'CONVERT TO XML DOM
Set xmldom = Server.CreateObject("Microsoft.XMLDOM")
Set thisPhoto = Server.CreateObject("Microsoft.XMLDOM")
xmldom.async = False
xmldom.loadxml(xmlhttp.ResponseText)

set photos = xmldom.getElementsByTagName("photo")

nextURL = ""
dim ThisFarmID, ThisServerID, ThisSecret, thumbPath , photoPath
for i = 1 to photos.length
	Set thisPhoto = photos.item(i-1)
	ThisFarmID			= thisPhoto.attributes.getNamedItem("farm").text
	ThisServerID		= thisPhoto.attributes.getNamedItem("server").text
	ThisPhotoID			= thisPhoto.attributes.getNamedItem("id").text
	ThisSecret			= thisPhoto.attributes.getNamedItem("secret").text

	thumbPath = "http://farm" & ThisFarmID & ".static.flickr.com/" & ThisServerID & "/" & ThisPhotoID & "_" & ThisSecret & "_s.jpg"
	photoPath = "http://farm" & ThisFarmID & ".static.flickr.com/" & ThisServerID & "/" & ThisPhotoID & "_" & ThisSecret & ".jpg"

	if PhotoID = ThisPhotoID then
		ThisImagei = i
	else
	end if
	if i = ThisImagei+1 then
		nextURL = "/members/photo-gallery/image.asp?PhotoSet_ID=" & PHOTOSET_ID & "&amp;PhotoID=" & ThisPhotoID
	end if
	if i = ThisImagei Then
		 prevURL = "/members/photo-gallery/image.asp?PhotoSet_ID=" & PHOTOSET_ID & "&amp;PhotoID=" & PrevPhotoID
	end if
	PrevPhotoID = ThisPhotoID
Next


'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|
MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Photo Gallery"
Metadescription   = FlickrPhotoDesc
ArticleURL        = DomainName & "members/Photo-Gallery/image.asp?PhotoSet_ID=" & PHOTOSET_ID & "&amp;PhotoID=" & PhotoID
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = FlickrPhotoTitle & " | Photo Gallery | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Members" ' highlight correct menu tab'
SiteSubSection    = "gallery"
'_______________________________________________________________________'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(3,1)
BreadCrumbArr (0,0) = "For Doctors"
BreadCrumbArr (0,1) = "/members/default.asp"
BreadCrumbArr (1,0) = "Photo Gallery"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = FlickrSetTitle
BreadCrumbArr (2,1) = "display.asp?PHOTOSET_ID=" & PHOTOSET_ID
BreadCrumbArr (3,0) = FlickrPhotoTitle
BreadCrumbArr (3,1) = ""

BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr

%>

<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
	.aligncenter {margin:0px auto; width:650px;}
	.prevphoto {display:inline-block;float:left;padding:0 10px;}
	.photoimg {display:inline-block;float:left;padding:0 10px;}
	.nextphoto {display:inline-block;float:left;padding:0 10px;}

</style>
<script type="text/javascript" language="javascript">
function nextImage()
{
		window.location.href="<%=nextURL%>";
		return false;
}
function prevImage() {
window.location.href="<%=prevURL%>";
return false;
}
</script>

</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
          		<div id="contentbg">

			        <h1><%=FlickrPhotoTitle%></h1>

					<%
					If FlickrPhotoDesc <> "" then
						response.write "<p>" & FlickrPhotoDesc & "</p>" & vbnewline
					end if
					%>

				    <div class="aligncenter">
						<div id="photo">

						<div class="prevphoto">
							<a href="<%=prevURL%>">&laquo;</a>
						</div>
						<div class="photoimg">
							<%
								= FlickrPhotoURL
							%>

						</div>
						<div class="nextphoto">
							<a href="<%=nextURL%>">&raquo;</a>
						</div>
					</div>


				</div>

					<br style="clear:both;" />
					



					<div id="gallery">
						<ul class="microgallery clearfix">
							<%
							'#############################################
							dim MaxImgInGallery : MaxImgInGallery  = 8
							dim cntset
							cntset = ThisImagei	- MaxImgInGallery
					        If cntset < 0 Then
								cntset = photos.length -(MaxImgInGallery/2) - 1
					        End If
							'in case cntset <  0
							If cntset < 0 Then
								cntset = 1
							End If

							'safety in case the ttl photos in gallery is < MaxImgInGallery
							If photos.length < MaxImgInGallery Then
								MaxImgInGallery = photos.length-1
							End If
							'response.write "<br/>" &  cntset
							'response.write "<br/>photos.length " & photos.length &  " | MaxImgInGallery " & MaxImgInGallery

							for i = 0 to MaxImgInGallery
								cntset = cntset + 1
								If cntset => photos.length Then
									cntset = 0
								End if
					           ' response.write "<br/>" &  cntset

								Set thisPhoto = photos.item(cntset)
					            ThisFarmID			= thisPhoto.attributes.getNamedItem("farm").text
					            ThisServerID		= thisPhoto.attributes.getNamedItem("server").text
					            ThisPhotoID			= thisPhoto.attributes.getNamedItem("id").text
					            ThisSecret			= thisPhoto.attributes.getNamedItem("secret").text

					            thumbPath = "http://farm" & ThisFarmID & ".static.flickr.com/" & ThisServerID & "/" & ThisPhotoID & "_" & ThisSecret & "_s.jpg"
					            'photoPath = "http://farm" & ThisFarmID & ".static.flickr.com/" & ThisServerID & "/" & ThisPhotoID & "_" & ThisSecret & ".jpg"

					            response.write vbtab & vbtab & "<li>"
					            if PhotoID = ThisPhotoID then
					                Response.write "<a href=""/members/photo-gallery/image.asp?PhotoSet_ID=" & PHOTOSET_ID & "&amp;PhotoID=" & ThisPhotoID & """><img src=""" & thumbpath & """ class=""currentlyshowing"" alt="""" /></a>"

					            else
					                Response.write "<a href=""/members/photo-gallery/image.asp?PhotoSet_ID=" & PHOTOSET_ID & "&amp;PhotoID=" & ThisPhotoID & """><img src=""" & thumbpath & """ alt="""" /></a>"
					            end If
					            response.write  "</li>" & vbnewline

							Next
						        %>
						</ul>
						<%
						If FlickrSetTtlPhoto <> "" Then
							response.write "<p><em><a href=""display.asp?PhotoSet_ID=" & PHOTOSET_ID & """>This set contains "& FlickrSetTtlPhoto & " photos.</a></em></p>"
						End If

						%>
						<p>
						<br/><%=FlickrSetDesc%>
						</p>



					</div>


				</div>

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
        		 <!--#include virtual="/members/Members-Menu.asp" -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->
<%
' retrieve set information
' return an array (set title, description,ttl photos)
Function flickrGetSetInfo(byval photoset_id)
	dim flickrAPIUrl : 	flickrAPIUrl ="https://api.flickr.com/services/rest/?method=flickr.photosets.getInfo&api_key="&API_KEY &"&photoset_id=" & photoset_id

	'response.write "<br/>" & flickrAPIUrl

	dim xmlhttpPhoto,intTimeout
	intTimeout = 5000
	on error resume next
	set xmlhttpPhoto = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	xmlhttpPhoto.setTimeouts intTimeout, intTimeout, intTimeout, intTimeout
	xmlhttpPhoto.Open "GET", flickrAPIUrl, false
	xmlhttpPhoto.Send
	if err <> 0 then 
			response.write "<br/>err " & err
		 exit function
	end if
	dim xmlSetDetail
	set xmlSetDetail = Server.CreateObject("Microsoft.XMLDOM")
	xmlSetDetail.async = false
	xmlSetDetail.setProperty "SelectionLanguage", "XPath"
	xmlSetDetail.loadxml(xmlhttpPhoto.ResponseText)

	dim dPhoto(3)
	dim photoNode, photoDetailNode
	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset/title")
	For Each photoDetailNode in photoNode
		'response.write "<br/>title " & photoDetailNode.Text
		dPhoto(0) = photoDetailNode.Text
	next

	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset/description")
	For Each photoDetailNode in photoNode
		'response.write "<br/>description " & photoDetailNode.Text
		dPhoto(1) = photoDetailNode.Text
	next

	Set photoNode = xmlSetDetail.selectNodes("/rsp/photoset")
	For Each photoDetailNode in photoNode
		dPhoto(2) = photoDetailNode.getAttribute("photos")
	next

	Set photoDetailNode = Nothing
	Set photoNode = Nothing
	Set xmlSetDetail = Nothing
	flickrGetSetInfo = dPhoto
End Function
%>

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>

