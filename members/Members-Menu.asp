<!-- Side: Menu -->
          <dl>
            <dt><a href="/Members/">For Doctors</a></dt>
            <dd>
              <ul>

                <li class="subsubmenu">
                    <a href="#"><b>ICO Members </b></a>
                      <ul>
                         <li  <%if SiteSubSection="benefits" then response.write " class=""active"""%>> <a href="/members/Benefits-of-Membership.asp">Benefits of membership</a></li>
                          <li  <%if SiteSubSection="codeofconduct" then response.write " class=""active"""%> ><a href="/members/Code-of-Conduct.asp">Code of Conduct</a></li>

   
                          <li  <%if SiteSubSection="competence" then response.write " class=""active"""%> ><a href="/members/Professional-Competence.asp">Professional Competence</a></li>
                          <li  <%if SiteSubSection="specialist" then response.write " class=""active"""%> ><a href="/members/specialist-registration.asp">Specialist Registration</a></li>
     
                          <!-- li < % if SiteSubSection="posts" then response.write " class=""active"""%>><a href="/members/available-posts.asp">Available Posts</a></li -->
                          <li <%if SiteSubSection="PHMP" then response.write " class=""active"""%>><a href="/members/Practitioner-Health-Matters-Prog.asp">Practitioner Health Matters Programme</a></li>
                          <li <%if SiteSubSection="AAO" then response.write " class=""active"""%>><a href="/members/AAO-ONE-Network.asp">AAO ONE Network</a></li>
                          <li  <%if SiteSubSection="publications" then response.write " class=""active"""%> ><a href="/members/publications/">Publications</a></li>
                          <li <%if SiteSubSection="newsletters" then response.write " class=""active"""%> ><a href="/members/ICO-Newsletters.asp">Newsletters</a></li>
                          <li <%if SiteSubSection="medals" then response.write " class=""active"""%>><a href="/members/Honorary-Lectures_ICO-Medal-Winners.asp">Honorary Lectures &amp; <br/>ICO Medal Winners</a></li>
      
                      </ul>
                </li>

                <li class="subsubmenu">
                  <a href="#" ><b>Guidelines </b></a>
                  <ul>
                    <!--#include virtual="/ssi/incl/menu-doctors-guidelines.asp" -->
                  </ul>
                </li>
               <li  <%if SiteSubSection="CPD" then response.write " class=""active"""%> ><a href="/members/CPD_Support_Scheme/">CPD - Support Scheme for NCHDs</a></li>
                <li  <%if SiteSubSection="GP" then response.write " class=""active"""%> ><a href="/members/For-GP.asp">For GP's</a></li>
                <li  <%if SiteSubSection="workinginireland" then response.write " class=""active"""%> ><a href="/working-in-ireland/">Working In Ireland</a></li>
                <li <%if SiteSubSection="sustainability" then response.write " class=""active"""%> ><a href="/sustainability-in-eye-care/">Sustainability Action in Eye Care</a></li>
                <li  <%if SiteSubSection="screening" then response.write " class=""active"""%> ><a href="/members/Retina-Screen.asp">Retina Screen</a></li>
                <li  <%if SiteSubSection="UsefulLinks" then response.write " class=""active"""%> ><a href="/members/useful-links.asp">Useful Links</a></li>
                <li  <%if SiteSubSection="Referring" then response.write " class=""active"""%> ><a href="/members/referring.asp">Referring</a></li>
                
                 <li><a href="/members/Photo-Gallery/">Photo Gallery</a></li>
            </ul>




              </ul>
            </dd>
          </dl>