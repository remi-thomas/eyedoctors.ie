
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Driving Standards Visual Guidelines"
Metadescription   = "Driving Standards Visual Guidelines "
ArticleURL        = DomainName & "members/Fitness-to-Drive-Guidelines.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Driving Standards Visual Guidelines | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Members" ' highlight correct menu tab'
SiteSubSection    = "driving"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)

BreadCrumbArr (0,0) = "For Doctors"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Guidelines"
BreadCrumbArr (1,1) = "sub-menu.asp?SubMenu=guidelines"
BreadCrumbArr (2,0) = "Driving Standards Visual Guidelines"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <!--#include virtual="/Health-Policy/medical-fitness-to-drive.html" -->
                <!--#include file="medical-fitness-to-drive-member-only.html" -->
   

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="Members-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>