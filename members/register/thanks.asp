
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/fct/encryptor.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|

MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "members/register"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Members Registration | Eye Doctors "
SiteSection       = "Members" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Members"
BreadCrumbArr (0,1) = "/members/"
BreadCrumbArr (1,0) = "Registration"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'


dim TempID
TempID = Left(request.querystring("TempID"),5)
If Not IsNumeric(TempID) Or TempID="" Then
	response.redirect "/members/"
	response.end
End If
dim MemberID, FirstName,LastName,Title
dim FullName
set rs=sqlconnection.Execute ("SELECT MemberID, FirstName, LastName, Title, Diploma, Photo, Biog, Website  FROM auto_Members_details where TempID="& TempID)
If Not rs.eof then
	MemberID	= rs("MemberID")
	FirstName	= rs("FirstName")
	LastName	= rs("LastName")
	Title		= rs("Title")
	
	FullName = Trim(Title & " " & FirstName & " " & LastName)
End if

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->

        	<% 
        	if MemberID<>"" and IsNumeric(MemberID) then
			%><h2>Thanks <%=FirstName%> for updating your record</h2><%
        	Else
        	%><h2>Thanks <%=FirstName%> for registering with the ICO</h2><%
        	end if
        	%>


			
			<p>
			<b>Your record will be reviewed within 48 hrs of submission before going live. </b>
			<br/>&nbsp;<br/>If you have any question, feel free to <a href="mailto:info@eyedoctors.ie">contact us</a>.
			<br/>
			<br/>Please <a href="mailto:ciara.keenan@eyedoctors.ie?subject=Member photo | <%=FullName%>" title="email your photo to Ciara">email your photo to Ciara</a> and she will attach it to your record.
			<br/>(All photos are uploaded in black &amp; white)
			</p>


	
    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>