<!--#include virtual="/ssi/dbconnect.asp" -->
<!--#include virtual="/ssi/fct/translator.asp" -->
<!--#include virtual="/ssi/fct/mailer.asp" -->
<!--#include virtual="/ssi/fct/mail_template.asp" -->
<%
'This file updates the details for a user'
MemberID					= request.form("MemberID")
FirstName					= RemoveXSS(ValidateText(request.form("FirstName")))
LastName					= RemoveXSS(ValidateText(request.form("LastName")))
Title						= RemoveXSS(ValidateText(request.form("Title")))
Diploma						= RemoveXSS(ValidateText(request.form("Diploma")))
Photo						= RemoveXSS(ValidateText(request.form("Photo")))
Biog						= RemoveXSS(ValidateText(request.form("Biog")))
Website						= RemoveXSS(ValidateText(request.form("Website")))
PrivateEmail				= ValidateText(request.form("PrivateEmail"))
PrivatePhoneNumber			= RemoveXSS(ValidateText(request.form("PrivatePhoneNumber")))
'
Membership_of_pro_bodies	= RemoveXSS(ValidateText(request.form("Membership_of_pro_bodies")))
Professional_achievements	= RemoveXSS(ValidateText(request.form("Professional_achievements")))
Research_interests			= RemoveXSS(ValidateText(request.form("Research_interests")))
Recent_Publications			= RemoveXSS(ValidateText(request.form("Recent_Publications")))


If Len(FirstName) + Len(LastName) < 5 Then
	Response.write "/"
End if

If Not IsNumeric(MemberID) or MemberID = "" Then
		MemberID = 0
		MailSubject = "Member ICO | New Registration " & LastName & " Temporary ID  #" & TempID
Else
		MailSubject = "Member ICO | " & FirstName & " " & LastName & "  has updated her record | Temporary ID  #" & TempID
end if


		sql = "INSERT INTO auto_Members_details ( MemberID, FirstName, LastName, Title, Diploma, Biog, Website, PrivateEmail, PrivatePhoneNumber,Membership_of_pro_bodies, Professional_achievements, Research_interests, Recent_Publications ) VALUES ("& MemberID &", '"& FirstName &"', '"& LastName &"', '"& Title &"', '"& Diploma &"', '"& Biog &"', '"& Website &"', '"& PrivateEmail &"', '"& PrivatePhoneNumber &"', '"& Membership_of_pro_bodies &"', '"& Professional_achievements &"', '"& Research_interests &"', '"& Recent_Publications &"')"

		'response.write sql
		sqlconnection.execute(sql)


		Set sqlNewID = SqlConnection.execute("select @@identity as NewRec")
		TempID = sqlNewID(0)
		Set sqlNewID = Nothing



		


Message = "<p>" & Title & " " & FirstName & " " & LastName & "</p>"
Message = Message & "<p>" & PrivateEmail & "</p>"


'### deal with specialty ###'
SpecialtiesSelection = RemoveXSS(request.form("SpecialtiesSelection"))
'esponse.write "<br/>SpecialtiesSelection " & SpecialtiesSelection
'response.end
sqlconnection.execute("delete from auto_Members_to_specialties where TempID="& TempID )

If Len(SpecialtiesSelection) > 1 then
	dim SpecialtiesArr
	SpecialtiesArr = Split(SpecialtiesSelection,",")

	For i = 0 to UBound(SpecialtiesArr)
		If SpecialtiesArr(i) <> "" and IsNumeric(SpecialtiesArr(i)) then
			sql="INSERT INTO auto_Members_to_specialties (TempID, SpecialtyID) VALUES ("& TempID  &","& ValidateText(SpecialtiesArr(i)) &")"
			'response.write "<hr>" & sql
			sqlconnection.execute(sql)
		end if
	Next

End If

'practice
sqlconnection.execute("delete from auto_Members_practice where TempID=" & TempID)

For PracticeCounter = 0 to 4

	PracticeID			= request.Form("PracticeID" & PracticeCounter)
	PracticeAddress		= Left(ValidateText(request.Form("Address" & PracticeCounter)),3999)
	PracticeTelephone	= Left(ValidateText(request.Form("Telephone" & PracticeCounter)),50)
	PracticeFax			= Left(ValidateText(request.Form("Fax" & PracticeCounter)),50)
	PracticeEmail		= Left(ValidateText(request.Form("PracticeEmail" & PracticeCounter)),200)
	PracticeWeb			= Left(ValidateText(request.Form("PracticeWeb" & PracticeCounter)),800)
	Opening				= Left(ValidateText(request.Form("Opening" & PracticeCounter)),1000)
	GeoLat				= Left(ValidateText(request.Form("GeoLat" & PracticeCounter)),10)
	GeoLong				= Left(ValidateText(request.Form("GeoLong" & PracticeCounter)),10)
	PracticeNotes		= ValidateText(request.Form("Notes" & PracticeCounter))
	PrivatePublic		= request.Form("PrivatePublic" & PracticeCounter)
	Online				= request.Form("Online" & PracticeCounter)
	CountyID			= request.Form("CountyID" & PracticeCounter)
	If Not IsNumeric(PrivatePublic) Or PrivatePublic = "" Then
		PrivatePublic = 0
	End if
	If PrivatePublic > 2 Then
		PrivatePublic = 0
	End If
	'online?
	If Not IsNumeric(Online) Or Online = "" Then
		Online = 0
	End if
	If Online <> 1 Then
		Online = 0
	End If
	If Not IsNumeric(CountyID) Or CountyID = "" Then
		CountyID = 0
	End if

	if Len(PracticeAddress) > 5 then
		sql = "INSERT INTO auto_Members_practice (TempID,PracticeAddress,PracticeCountyID,PracticeTelephone,PracticeFax,PracticeEmail,PracticeWeb ,Opening,GeoLat,GeoLong,PracticeNotes,PrivatePublic,Online,RecordDate) VALUES ("&TempID&",'" &PracticeAddress & "',"&CountyID&",'" & PracticeTelephone & "','" & PracticeFax & "','" & PracticeEmail & "','" & PracticeWeb & "','" & Opening & "','" & GeoLat & "','" & GeoLong & "','"  & PracticeNotes & "',"  & PrivatePublic & "," & Online &",getdate() )"
		response.write "<hr>" & sql
		sqlconnection.execute(sql)
		Message = Message & "<p>" & PracticeAddress & "<br/>Tel:" & PracticeTelephone & "</p>"
	End If
Next

	Message = HTMLTop & vbnewline & Message  & vbnewline & HTMLBottom
	response.write "<hr/>MailSubject " & MailSubject
	response.write "<br/>Message <br/>" & Message

	call SendMail ("no-reply@eyedoctors.ie", "info@eyedoctors.ie", MailSubject,  Message)
%>

<!--#include virtual="/ssi/dbclose.asp" -->
<%
if request("mode") = "add"  Then
	Response.Redirect "thanks.asp?TempID=" & TempID & "&Thiserr=Record #" & MemberID & " has been created"
Else
	Response.Redirect "thanks.asp?TempID=" & TempID & "&MemberID=" & MemberID & "&Thiserr=Record #" & MemberID & " has been updated"
end if
%>

