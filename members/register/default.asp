
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/fct/encryptor.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|

MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "members/register"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Members Registration | Eye Doctors "
SiteSection       = "Members" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Members"
BreadCrumbArr (0,1) = "/members/"
BreadCrumbArr (1,0) = "Registration"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->

<style>
.practice label {display:block;width:120px;float:left;}
</style>



<script language='JavaScript'>

//#######################################
//##  script copyright fusio.net 2015  ##
//##  by remi at fusio<.>net         ##
//#######################################


function validate_form(form_name)
{
	if (form_name.FirstName.value == "")
	{
		alert("Please enter the member\' first name");
		form_name.FirstName.select();
		return false;
	}

	if (form_name.LastName.value == "")
	{
		alert("Please enter the member\' last name");
		form_name.LastName.select();
		return false;
	}

	if (form_name.Website.value != "")
	{
		var URL = form_name.Website.value;
		//alert("test " + isUrl(URL))
		if (!isUrl(URL))
		{
			alert("The URL you're submitting is not correct.\nAlthough www. is not necessary it must always start by http://");
			form_name.Website.select();
			return false;
		}
	}


	if (form_name.PrivateEmail.value != "")
	{
		
		var emailStr=form_name.PrivateEmail.value;
		var emailPat=/^(.+)@(.+)$/
		var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
		var validChars="\[^\\s" + specialChars + "\]"
		var quotedUser="(\"[^\"]*\")"
		var ipDomainPat=/^\[(\d{1,6})\.(\d{1,6})\.(\d{1,6})\.(\d{1,6})\]$/
		var atom=validChars + '+'
		var word="(" + atom + "|" + quotedUser + ")"
		var word2 = "(" + validChars + "*|" + quotedUser + ")";
		var userPat=new RegExp("^" + word + "(\\." + word2 + ")*$")

		//var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
		var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
		var matchArray=emailStr.match(emailPat)
		if (matchArray==null) {
			alert("Invalid email address! (error1)")
			form_name.PrivateEmail.select();
			return false;
		}
		var user=matchArray[1]
		var domain=matchArray[2]
		if (user.match(userPat)==null) {
			alert("Invalid email address! (error2)")
			form_name.PrivateEmail.select();
			return false;
		}
		var IPArray=domain.match(ipDomainPat)
		if (IPArray!=null) {
			  for (var i=1;i<=4;i++) {
				if (IPArray[i]>255) {
					alert("Invalid email address! (error3)")
				form_name.PrivateEmail.select();
				return false;
				}
			}
			return true;
		}
		var domainArray=domain.match(domainPat)
		if (domainArray==null) {
			alert("Invalid email address! (error4)")
			form_name.PrivateEmail.select();
			return false;
		}
		var atomPat=new RegExp(atom,"g")
		var domArr=domain.match(atomPat)
		var len=domArr.length
		if (domArr[domArr.length-1].length<2 ||
			domArr[domArr.length-1].length>7) {
		   alert("Invalid email address! (error5)")
		   form_name.PrivateEmail.select();
		   return false;
		}
		if (len<2) {
		   alert("Invalid email address! Address Too Short!")
		   form_name.PrivateEmail.select();
		   return false;
		}
	}


}

function moveIt(fbox, tbox) {
var arrFbox = new Array();
var arrTbox = new Array();
var arrLookup = new Array();
var i;

//alert (' tbox.options.length ' +  tbox.options.length)
if (tbox.options.length > 3)
{
 alert('You can choose a maximum of 4 sub specialities!');
 return ;
}



for (i = 0; i < tbox.options.length; i++) {
arrLookup[tbox.options[i].text] = tbox.options[i].value;
arrTbox[i] = tbox.options[i].text;
}
var fLength = 0;
var tLength = arrTbox.length;
for(i = 0; i < fbox.options.length; i++) {
arrLookup[fbox.options[i].text] = fbox.options[i].value;
if (fbox.options[i].selected && fbox.options[i].value != "") {
arrTbox[tLength] = fbox.options[i].text;
tLength++;
}
else {
arrFbox[fLength] = fbox.options[i].text;
fLength++;
   }
}
arrFbox.sort();
arrTbox.sort();
fbox.length = 0;
tbox.length = 0;
var c;
for(c = 0; c < arrFbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrFbox[c]];
no.text = arrFbox[c];
fbox[c] = no;
}
for(c = 0; c < arrTbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrTbox[c]];
no.text = arrTbox[c];
tbox[c] = no;
   }
}


function sendInfo(formVar,box)
	{
	  formVar.length = 0;
	    var strValue = new String();
	    for(var i=0; i<box.length; i++) {
	        strValue += box[i].value;
	        if (i < box.length-1) {
	            strValue += ",";
	        }
	    }
	    formVar.value = strValue
	}



function isUrl(s) 
{
var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return regexp.test(s);

}

function ShowMe(id)
{
	//imgobj = eval('document.IMG' + id);

	Layerobj = document.getElementById("MoreLayer" + id);


	if (document.getElementById)
	{
		obj = document.getElementById(id);

		if (obj.style.display == "none")
		{
			obj.style.display = "";
		}
		else
		{
			obj.style.display = "none";
		}
	}
}


function SetData(AData,Afield)
{
	document.memberdata[Afield].value=AData;
}

function CheckBox(afield)
{
	//alert(afield)
	afield[0].checked = true 
}

</script>



</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
        	<div class="maincol">
<%
Dim formaction, AdminUser
Dim MemberID
Dim FirstName, LastName, Title, Diploma, Biog, Website, PrivateEmail, PrivatePhoneNumber, FullName
Dim Membership_of_pro_bodies, Professional_achievements, Research_interests, Recent_Publications

dim a
a = left(request.querystring("a"),100)
if len(a) then
	'format: me@domain.com|ID'
	a 			= Decode(a)
	'if fusio() then
	'	response.write "<div><p>Fusio only: "
	'	response.write a
	'	response.write "</p></div>"
	'response.end
	'end if



	if instr(a,"|") then
		dim mvar
		mvar 		= split(a,"|")
		MemberID 	= mvar(1)
		dim  UserEmail
		UserEmail 	= mvar(0)
		UserEmail 	= replace(UserEmail,"}","@")

		'response.write "<br/>isNumeric("& MemberID& ")" & isNumeric(MemberID)
		if isNumeric(MemberID) then
			MemberID = MemberID *  100
		end if

		'if fusio() then
		'	response.write "<div><p>Fusio only: "
		'	response.write "<br/> UserEmail " & UserEmail
		'	response.write "<br/> MemberID " & MemberID
		'	response.write "</div>"
			'response.end
		'end if

		'response.write ("<br/>INSERT INTO Members_temp_logins (variables,memberID,MemberEmail,UserIP,UserDetails) values ('" & left(ValidateText(a),600) & "','" & left(ValidateText(MemberID),8) & "','"& left(ValidateText(UserEmail),300) &"','"& left(ValidateText(Request.ServerVariables("REMOTE_ADDR")),16) &"','"& left(ValidateText(Request.ServerVariables("HTTP_USER_AGENT")),200) &"')")
		sqlconnection.execute("INSERT INTO Members_temp_logins (variables,memberID,MemberEmail,UserIP,UserDetails) values ('" & left(ValidateText(a),600) & "','" & left(ValidateText(MemberID),8) & "','"& left(ValidateText(UserEmail),300) &"','"& left(ValidateText(Request.ServerVariables("REMOTE_ADDR")),16) &"','"& left(ValidateText(Request.ServerVariables("HTTP_USER_AGENT")),200) &"')")
	else
		MemberID = ""
	end if

end if 


dim SubmitButton

if IsNumeric (MemberID) and MemberID <> "" then
	set rs=sqlconnection.Execute ("SELECT FirstName, LastName, Title, Diploma, Photo, Biog, Website, Membership_of_pro_bodies,Professional_achievements, Research_interests,Recent_Publications, PrivateEmail, PrivatePhoneNumber  FROM Members_details where MemberID="& MemberID)
 
	If Not rs.eof then
		FirstName			= rs("FirstName")
		LastName			= rs("LastName")
		Title				= rs("Title")
		Diploma				= rs("Diploma")
		Biog				= rs("Biog")
		Website				= rs("Website")
		PrivateEmail 		= rs("PrivateEmail")
		PrivatePhoneNumber	= rs("PrivatePhoneNumber")
		'
		Membership_of_pro_bodies	= rs("Membership_of_pro_bodies")
		Professional_achievements	= rs("Professional_achievements")
		Research_interests			= rs("Research_interests")
		Recent_Publications			= rs("Recent_Publications")
		'#######################################################'
		FullName 			= Trim(Title & " " & FirstName & " " & LastName)
		formaction			= ""
		SubmitButton		= "Update Your Record"
		Response.Write "<h1>Modify Your Record - <i>" & FullName & "</i></h1>"

		'if fusio() then
		'	response.write "<div><p>Fusio only: "
		'	response.write "<br/> FirstName " & FirstName
		'	response.write "<br/> Title " & Title
		'	response.write "</div>"
		'	'response.end
		'end if



	End if

else
	formaction		= "?mode=add"
	AdminUser		= ""
	SubmitButton	= "Register"
	Response.Write "<h1>Register</h1>"
End if
      %>

	<div style="width:90%;background:#ddd;padding:10px;font-weight:bold">
	<%
	If IsNumeric (MemberID) and MemberID <> "" then
	%>
	Modify your details and click submit. <br/><span style="font-weight:normal">Please note all information will be reviewed before being published.</span>
	<%else%>
	To register, enter your details into the form below and click submit.
	<%end if%></div><br/>&nbsp;



<div id="details" class="form-group">

<form name="memberdata" action="process.asp<%=formaction%>" method="post" style="margin:0;padding:0" OnSubmit="return validate_form(this)">
	<input type="hidden" name="MemberID" value="<%=MemberID %>">
	<input type="hidden" name="SpecialtiesSelection" value="">
<h2>Your Details</h2>
<p>
<span style="float:left">
	<label>Title</label>
	<select name="title" class="form-control" style="width:79px;">
		<option value="<%=Title%>"><%=Title%></option>
		<option value="Mr">Mr</option>
		<option value="Ms">Ms</option>
		<option value="Mr">Dr</option>
		<option value="Pr">Pr</option>
		<option value="Mrs">Mrs</option>
		<option value="Miss">Miss</option>
	</select>
</span>
<span style="float:left;padding-left:7px;">
	<label>First Name</label>
	<input name="FirstName" value="<%=FirstName%>" type="text" size="60" class="form-control" style="width:230px;"/>
</span>
<span style="float:left;padding-left:7px;">
	<label>Last Name</label>
	<input name="LastName" value="<%=LastName%>" type="text" size="60" class="form-control" style="width:230px;"/>
</span>
</p>



<p style="clear:both;padding-top:10px;">
<label>Diploma</label>
<input name="Diploma" value="<%=Diploma%>" type="text" size="60" class="form-control" /> M.B., M.Med.Sc, F.R.C.Ophth. (uk) etc..
</p>
<p>
<label>Sub-specialty interests (4 max)</label>
<table style="width:460px;border:0;">
	<tr  style="border:0;">
		<td>
			Specialties</br>
			<select name="SpecialtiesFrom" size=10 style="width:230px;" class="form-control" multiple>
			<%
			If IsNumeric (MemberID) and MemberID <> "" Then
				set rs = sqlconnection.execute("select SpecialtyID,SpecialtyTitle from specialties where SpecialtyID not in (select SpecialtyID from Members_to_specialties where MemberID=" & MemberID & ") order by SpecialtyTitle")	
			else
				set rs = sqlconnection.execute("SELECT SpecialtyID,SpecialtyTitle from specialties order by SpecialtyTitle")
			end if
			Do until rs.eof
				response.Write "<option value=""" & rs(0) & """>"& rs(1) & "</option>" & vbnewline
				rs.movenext
			loop
				%>
			</select>
		</td>
		<td valign="middle" width="20" style="vertical-align:middle">
		<a href="javascript:moveIt(document.memberdata.SpecialtiesFrom,document.memberdata.SpecialtiesTo)">
		<img border="0" hspace="8" vspace="2" src="icon_arrow_right.gif" width="21" height="16"></a>
		<br/>
		<a href="javascript:moveIt(document.memberdata.SpecialtiesTo,document.memberdata.SpecialtiesFrom)">
		<img border="0" hspace="8" vspace="2" src="icon_arrow_left.gif" width="21" height="16"></a>
	</td>
	<td valign="top">
		Selected</br>
		<select name="SpecialtiesTo" multiple size=10 style="width:230px;" class="form-control">
		<%
		If IsNumeric (MemberID) and MemberID <> "" Then
			set rs = sqlconnection.execute("select s.SpecialtyID, s.SpecialtyTitle from specialties s, Members_to_specialties P where s.SpecialtyID = P.SpecialtyID and P.MemberID="& MemberID &" order by s.SpecialtyTitle")
			If Not rs.eof then
			Do until rs.eof
				response.Write "<option value=""" & rs(0) & """>"& DisplayText(rs(1)) & "</option>" & vbnewline
			rs.movenext
			Loop
			End If
		End if
		%>
		</select>
	</td>
	</tr>
	</table>
</p>
<p>
<label>Biog</label>
<textarea name="Biog" cols="60" rows="3" class="form-control"><%=Biog%></textarea>
</p>

<p>
<label>Other professional achievements </label>
<textarea name="Professional_achievements" cols="60" rows="3" class="form-control"><%=Professional_achievements%></textarea>
</p>



<p>
<label>Research interests</label>
<textarea name="Research_interests" cols="60" rows="3" class="form-control"><%=Research_interests%></textarea>
</p>


<!-- p>
<label>Publications</label>
<textarea name="Recent_Publications" cols="60" rows="3" class="form-control"><%=Recent_Publications%></textarea>
</p -->
<input name="Recent_Publications" value="<%=Recent_Publications%>" type="hidden" />


<p>
<label>Membership of professional /national/regional bodies</label>
<input name="Membership_of_pro_bodies" value="<%=Membership_of_pro_bodies%>" type="text" size="60" class="form-control" />
</p>

<p>
<label>Website / LinkedIn</label>
<input name="Website" value="<%=Website%>" type="text" size="60" class="form-control" />
</p>
<p>
<label>Email (*)</label>
<input name="PrivateEmail" value="<%=PrivateEmail%>" type="email" size="60" class="form-control" /><span style="font-size:12px">*This will NOT be published on the web site</span>
</p>
<p>
<label>Phone Number (*)</label>
<input name="PrivatePhoneNumber" value="<%=PrivatePhoneNumber%>" type="text" size="60" class="form-control" /><span style="font-size:12px">*This will NOT be published on the web site</span>
</p>

<%
	dim PracticeCounter, PractCount
	'count the number of practice for this member 
	'5 practices max contains 11 fields
	Dim PractArr(5,12)
	'set PrivatePublic and online to 0 by default (to pass on the cint check later) 
	For PracticeCounter = 0 to 4
		PractArr(PracticeCounter,12) = 0
	next

	dim rspractice
	If IsNumeric (MemberID) and MemberID <> "" then
		Set rspractice = sqlconnection.execute ("SELECT PracticeID,PracticeAddress,PracticeCountyID,PracticeTelephone, PracticeFax ,PracticeWeb ,PracticeEmail, Opening, GeoLat, GeoLong, PracticeNotes ,PrivatePublic, Online FROM Members_practice where MemberID=" & MemberID)
		If rspractice.eof Then
			PractArr(1,0) = 0 'PracticeID
			PractArr(1,1) = "" 'Address
			PractArr(1,2) = "" 'Telephone
			PractArr(1,3) = "" 'Fax
			PractArr(1,4) = "" 'Email
			PractArr(1,5) = "" 'Web
			PractArr(1,6) = "" 'Opening
			PractArr(1,7) = "" 'GeoLat
			PractArr(1,8) = "" 'GeoLong
			PractArr(1,9) = "" 'Notes
			PractArr(1,10) = 0 'PrivatePublic
			PractArr(1,11) = 0 'Online
			PractArr(1,12) = 0 'Practice County ID
			PractCount = 1
		Else
			PractCount = 0
			Do While Not rspractice.eof
				PractArr(PractCount,0) = rspractice("PracticeID") 'PracticeID
				PractArr(PractCount,1) = DisplayText(rspractice("PracticeAddress")) 'Address
				PractArr(PractCount,2) = DisplayText(rspractice("PracticeTelephone")) 'Telephone
				PractArr(PractCount,3) = DisplayText(rspractice("PracticeFax")) 'Fax
				PractArr(PractCount,4) = DisplayText(rspractice("PracticeEmail")) 'Email
				PractArr(PractCount,5) = DisplayText(rspractice("PracticeWeb")) 'Web
				PractArr(PractCount,6) = DisplayText(rspractice("Opening")) 'Opening
				PractArr(PractCount,7) = DisplayText(rspractice("GeoLat")) 'GeoLat
				PractArr(PractCount,8) = DisplayText(rspractice("GeoLong")) 'GeoLong
				PractArr(PractCount,9) = DisplayText(rspractice("PracticeNotes")) 'Notes
				PractArr(PractCount,10) = DisplayText(rspractice("PrivatePublic")) 'PrivatePublic
				PractArr(PractCount,11) = DisplayText(rspractice("Online")) 'Online
				PractArr(PractCount,12)	= rspractice("PracticeCountyID") 'Practice County ID
			rspractice.movenext
				PractCount = PractCount  + 1
			Loop
		End if

	Else
		PractCount = 1
		PractArr(1,0) = 0 'PracticeID
		PractArr(1,1) = "" 'Address
		PractArr(1,2) = "" 'Telephone
		PractArr(1,3) = "" 'Fax
		PractArr(1,4) = "" 'Email
		PractArr(1,5) = "" 'Web
		PractArr(1,6) = "" 'Opening
		PractArr(1,7) = "" 'GeoLat
		PractArr(1,8) = "" 'GeoLong
		PractArr(1,9) = "" 'Notes
		PractArr(1,10) = 0 'PrivatePublic
		PractArr(1,11) = 0 'Online
		PractArr(1,12) = 0 'Practice County ID
	End If
%>
<%
For PracticeCounter = 0 to 4
%>
	<div class="form-group practice" id="<%=PracticeCounter%>" style="display:none;margin-top:20px;padding:10px;width:700px;">
	<h2>Your Practice #<%=PracticeCounter+1%></h2>

	<p style="font-size:12px;">
	If your hospital is listed below, your can populate all the fields in a single click<br/>

	<!-- Beacon -->
	<a href="javascript:onClick=SetData('Beacon Hospital\nSandyford,\nDublin 18','Address<%=PracticeCounter%>');SetData('01 293 6600','Telephone<%=PracticeCounter%>');SetData('http://www.beaconhospital.ie/','PracticeWeb<%=PracticeCounter%>');SetData('53.275631','GeoLat<%=PracticeCounter%>');SetData('-6.2193','GeoLong<%=PracticeCounter%>')" title="Beacon Hospital">Beacon Hospital</a>  |

	<!-- Beaumont -->
	<a href="javascript:onClick=SetData('Beaumont Hospital\nBeaumont Road,\nDublin 9','Address<%=PracticeCounter%>');SetData('01 809 3000 ','Telephone<%=PracticeCounter%>');SetData('http://www.beaumont.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.390433','GeoLat<%=PracticeCounter%>');SetData('-6.223263','GeoLong<%=PracticeCounter%>')" title="Matter">Beaumont</a> |
	<!-- Blackrock -->
	<a href="javascript:onClick=SetData('Blackrock Clinic\nBlackrock,\nCo. Dublin','Address<%=PracticeCounter%>');SetData('01 283 2222','Telephone<%=PracticeCounter%>');SetData('http://www.blackrock-clinic.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.3053286','GeoLat<%=PracticeCounter%>');SetData('-6.1875031','GeoLong<%=PracticeCounter%>')" title="Blackrock climic">Blackrock climic</a>  |


	<!-- Crumlin -->
	<a href="javascript:onClick=SetData('Our Lady\'s Children\'s Hospital For Sick Children\nCooley Road\nCrumlin,\nDublin 12','Address<%=PracticeCounter%>');SetData('01 409 6100  ','Telephone<%=PracticeCounter%>'); SetData('http://www.olchc.ie','PracticeWeb<%=PracticeCounter%>'); SetData('53.325504','GeoLat<%=PracticeCounter%>'); SetData('-6.31812','GeoLong<%=PracticeCounter%>')" title="Crumlin">Crumlin</a>	|

	<!-- Eye&amp;Ear -->
	<a href="javascript:onClick=SetData('Royal Victoria Eye and Ear\nAdelaide Road\nDublin 2','Address<%=PracticeCounter%>');SetData('01 6644600','Telephone<%=PracticeCounter%>');SetData('http://www.rveeh.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.333017','GeoLat<%=PracticeCounter%>');SetData('-6.259509','GeoLong<%=PracticeCounter%>')" title="Eye & Ear">Eye &amp; Ear</a> |
	<!-- Jame's -->
	<a href="javascript:onClick=SetData('St. James\'s Hospital\nJames\'s Street,\nDublin 8','Address<%=PracticeCounter%>');SetData('01 410 3000','Telephone<%=PracticeCounter%>');SetData('http://www.stjames.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.357962','GeoLat<%=PracticeCounter%>');SetData('-6.238773','GeoLong<%=PracticeCounter%>')" title="Jame">James's</a> |
	<!-- Mater -->
	<a href="javascript:onClick=SetData('Mater Misericordia University Hospital\nEccles Street\nDublin 7','Address<%=PracticeCounter%>');SetData('01 8032000','Telephone<%=PracticeCounter%>');SetData('http://www.mater.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.359037','GeoLat<%=PracticeCounter%>');SetData('-6.268334','GeoLong<%=PracticeCounter%>')" title="Mater">Mater</a> |

	<!-- Temple St -->
	<a href="javascript:onClick=SetData('Childrens University Hospital\nTemple Street,\nDublin 1','Address<%=PracticeCounter%>');SetData('01 878 4200  ','Telephone<%=PracticeCounter%>');SetData('http://www.cuh.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.356832','GeoLat<%=PracticeCounter%>');SetData('-6.262207','GeoLong<%=PracticeCounter%>')" title="Temple Street">Temple Street</a> |

	<!-- Vincent's -->
	<a href="javascript:onClick=SetData('St. Vincent\'s University Hospital\nElm park,\nDublin 4','Address<%=PracticeCounter%>');SetData('01 221 4000','Telephone<%=PracticeCounter%>');SetData('http://www.stvincents.ie','PracticeWeb<%=PracticeCounter%>');SetData('53.31695','GeoLat<%=PracticeCounter%>');SetData('-6.21247','GeoLong<%=PracticeCounter%>')" title="Vincent's">Vincent's</a> 
	<br/>



	<!-- Cork University -->
	<a href="javascript:onClick=SetData('Cork University Hospital\nWilton,\nCork','Address<%=PracticeCounter%>');SetData('021 454 6400','Telephone<%=PracticeCounter%>');SetData('http://www.ucc.ie','PracticeWeb<%=PracticeCounter%>');SetData('51.890','GeoLat<%=PracticeCounter%>');SetData('-8.475','GeoLong<%=PracticeCounter%>'); CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" title="Cork">Cork University</a> |
	<!-- Cork Mercy -->
	<a href="javascript:onClick=SetData('Mercy University Hospital\nGrenville Place,\nCork','Address<%=PracticeCounter%>');SetData('021 427 1971 ','Telephone<%=PracticeCounter%>');SetData('http://www.muh.ie','PracticeWeb<%=PracticeCounter%>');SetData('51.899','GeoLat<%=PracticeCounter%>');SetData('-8.483','GeoLong<%=PracticeCounter%>'); CheckBox('document.memberdata.PrivatePublic<%=PracticeCounter%>')" title="Cork">Cork Mercy</a> |
	<!-- Galway -->
	<a href="javascript:onClick=SetData('Galway Univeristy Hospital\nNewcastle Road,\nGalway','Address<%=PracticeCounter%>');SetData('091 544 544','Telephone<%=PracticeCounter%>');SetData('http://www.hse.ie/eng/services/find_a_service/hospscancer/Galway_University_Hospitals/','PracticeWeb<%=PracticeCounter%>');SetData('53.27623','GeoLat<%=PracticeCounter%>');SetData('-9.06774','GeoLong<%=PracticeCounter%>')" title="Galway">Galway</a> |
	<!-- Letterkenny -->
	<a href="javascript:onClick=SetData('Letterkenny General Hospital\nOakland\'s Park,\nCork','Address<%=PracticeCounter%>');SetData('021 454 6400 ','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('54.9609','GeoLat<%=PracticeCounter%>');SetData('-7.73339','GeoLong<%=PracticeCounter%>')" title="Letterkenny">Letterkenny</a> |
	<!-- Limerick -->
	<a href="javascript:onClick=SetData('St. John\'s Hospital\nSt. John\'s Square,\nLimerick','Address<%=PracticeCounter%>');SetData('061 46 2222','Telephone<%=PracticeCounter%>');SetData('http://www.stjohnshospital.ie/','PracticeWeb<%=PracticeCounter%>');SetData('52.472302','GeoLat<%=PracticeCounter%>');SetData('-8.431498','GeoLong<%=PracticeCounter%>')" title="Limeri">Limerick</a> |

	<!-- Midlands Regional Hospital  -->
	<a href="javascript:onClick=SetData('Midlands Regional Hospital\nDublin Road\nPortlaoise','Address<%=PracticeCounter%>');SetData('057 862 1364','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('53.037267','GeoLat<%=PracticeCounter%>');SetData('-8.46494','GeoLong<%=PracticeCounter%>')" title="Midlands Regional Hospital, Portlaoise">Midlands Regional Hospital, Portlaoise</a> |


	<!-- Sligo -->
	<a href="javascript:onClick=SetData('Sligo General Hospital\nThe Mall,\nSligo','Address<%=PracticeCounter%>');SetData('071 917 1111 ','Telephone<%=PracticeCounter%>');SetData('http://www.sgh.ie','PracticeWeb<%=PracticeCounter%>');SetData('54.274448','GeoLat<%=PracticeCounter%>');SetData('-8.46494','GeoLong<%=PracticeCounter%>')" title="Sligo">Sligo</a> |
	<!-- Waterford -->
	<a href="javascript:onClick=SetData('Waterford General Hospital\nDunmore Road,\nWaterford','Address<%=PracticeCounter%>');SetData('051 848 000 ','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('52.248332','GeoLat<%=PracticeCounter%>');SetData('-7.079187','GeoLong<%=PracticeCounter%>')" title="Waterford">Waterford</a> |
	<!-- Whitfield -->
	<a href="javascript:onClick=SetData('Whitfield\nButlerstown North,\Nork Road,\nWaterford','Address<%=PracticeCounter%>');SetData(' 051 337 400','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('52.2402669','GeoLat<%=PracticeCounter%>');SetData('-7.1721418','GeoLong<%=PracticeCounter%>')" title="Waterford">Whitfield Clinic, Waterford</a> 


	<br/>
	<span class="member-atoz" style="font-size:12px;">
	<a href="javascript:onClick=SetData('','Address<%=PracticeCounter%>');SetData('','Telephone<%=PracticeCounter%>');SetData('','PracticeWeb<%=PracticeCounter%>');SetData('','GeoLat<%=PracticeCounter%>');SetData('','GeoLong<%=PracticeCounter%>');" class="btn" style="font-size:12px;">Erase</a>
	</span>
	</p>

	<input name="PracticeID<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,0)%>" type="hidden" />
	<p>
	<label>Address</label>
	<textarea cols="50" rows="4" name="Address<%=PracticeCounter%>" class="form-control"/><%=PractArr(PracticeCounter,1)%>
	</textarea>


	</p>

	<p>
	<label>County</label>
		<select name="CountyID<%=PracticeCounter%>" class="form-control"/>
		<option value="0">-</option>
		<%
		dim co
		Set co = sqlconnection.execute("SELECT c.CountyID, c.County, c.ListOrder FROM County c ORDER BY c.ListOrder ")
			Do While Not co.eof
				response.write "<option value="""& co("CountyID")& """" 
					if CInt(co("CountyID")) = CInt(PractArr(PracticeCounter,12)) Then response.write " selected " 
				response.write " >"& co("County") &"</option>" & vbnewline
			co.movenext
			Loop
		Set co = nothing
		%>
		</select>
	</p>


	<p>
	<label>Telephone</label>
	<input name="Telephone<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,2)%>" type="text" size="60"  class="form-control"/>
	</p>
	<p>
	<label>Fax</label>
	<input name="Fax<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,3)%>" type="text" size="60"  class="form-control"/>
	</p>
	<p>
	<label>Email(*)</label>
	<input name="PracticeEmail<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,4)%>" type="email" size="60"  class="form-control"/>*This will be shown to the public beside the practice information
	</p>
	<p>
	<label>Website(*)</label>
	<input name="PracticeWeb<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,5)%>" type="text" size="60"  class="form-control"/>*This will be shown to the public beside the practice information
	</p>
	<p>
	<label>Opening</label>
	<textarea name="Opening<%=PracticeCounter%>" cols="55" rows="2" class="form-control"><%=PractArr(PracticeCounter,6)%></textarea>
	</p>
	<p >
	<label >Latitude</label>
	<input name="GeoLat<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,7)%>" type="text" size="10"  class="form-control"/>
	</p>
	<p>
		<label > Longitude</label>
		<input name="GeoLong<%=PracticeCounter%>" value="<%=PractArr(PracticeCounter,8)%>" type="text" size="10"  class="form-control"/>

	</p>
	<p style="clear:both;padding-top:5px;">
	<label>Notes</label>
	<textarea name="Notes<%=PracticeCounter%>" cols="55" rows="3" class="form-control"><%=PractArr(PracticeCounter,9)%></textarea>
	</p>
	<p>
	
	
	<p style="float:left;">
	<label>Public</label>
	<input name="PrivatePublic<%=PracticeCounter%>" value="1" type="radio" class="form-control" <% If CInt(PractArr(PracticeCounter,10)) = 1 Then response.write "checked"%> /> 
	</p>
	<p style="float:left;padding-left:12px;">
	<label>Private Room</label>
	<input name="PrivatePublic<%=PracticeCounter%>" value="2" type="radio" class="form-control" <% If CInt(PractArr(PracticeCounter,10)) = 2 Then response.write "checked"%>/> 
	</p>
	</p>

	<input name="Online<%=PracticeCounter%>" value="<% If CInt(PractArr(PracticeCounter,11)) = 1 Then response.write "1" else response.write "0" %>" type="hidden"  class="form-control"/>



	<p style="clear:both;padding-top:5px;" class="member-atoz" ID="MoreLayer<%=PracticeCounter%>">
	<%
	If PracticeCounter < 4 Then
	%>
	<a href="JavaScript:onClick=ShowMe('<%=PracticeCounter+1%>')" >Add another practice</a>
	<%
	End if
	%>
	</p>


	</div>

<%
Next 'PracticeCounter
%>
<p style="padding-left:8px;">
<input type="submit" value="<%=SubmitButton%>" class="btn btn-default" onClick="sendInfo(document.memberdata.SpecialtiesSelection,document.memberdata.SpecialtiesTo);"/>
</p>


</form>



<script language="JavaScript" TYPE="text/javascript">
ShowMe('0')
<%
For PracticeCounter = 1 to 4
	If PractArr(PracticeCounter,0) > 0 then
%>
ShowMe('<%=PracticeCounter%>')
<%
	Else
		Exit for
	End if
Next
%>
ShowMe('<%=PracticeCounter%>')
</script>

</div>

      </form>

</table>

    
    	</div>  <!--{end}  main col -->
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>