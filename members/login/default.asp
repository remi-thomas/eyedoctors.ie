
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Members Login"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "members/default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "For Doctors | Members "
SiteSection       = "For Doctors" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "For Doctors"
BreadCrumbArr (0,1) = "/members/"

BreadCrumbArr (1,0) = "ICO Members"
BreadCrumbArr (1,1) = "/members/sub-menu.asp?SubMenu=members/"

BreadCrumbArr (2,0) = "Login"
BreadCrumbArr (2,1) = ""


BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script src="//www.wildapricot.com/docs/scripts/waloginbox.js"></script>

<style>

  .wildapricot form .form-group { display:block;margin-bottom:5px;}

  .wildapricot form .form-group::after {content: " "; white-space: pre;}

  .wildapricot form input.form-control {width: 60%;}

 .wildapricot form label {color: #b8094d;}
</style>

</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class=" layout">
        <!-- Main Column -->
        <div class="col-md-12 maincol" style="min-height:600px;">



          <div class="wildapricot">

                <form action="https://ico.wildapricot.org/Sys/Login" method="post" class="member-search" role="form" class="form-horizontal">



                  <div class="form-group">
                    <label for="email" class="col-sm-2">Your Email:</label>
                    <div class="col-sm-10">
                       <input name="email" type="email" maxlength="100" class="form-control" id="email" placeholder="name@email.ie"/>
                    </div>
                  </div>

                  <div class="form-group">  
                    <label for="email" class="col-sm-2">Your Password:</label>
                    <div class="col-sm-10">
                     <input type="password" name="password" class="form-control" />
                    </div>
                  </div>

                  <div class="form-check" >
                    <div class="col-sm-offset-2 col-sm-10">
                      <label for="idLoginBoxRememberMeCheckbox">Remember me</label>
                      <input type="checkbox" name="rememberMe" id="idLoginBoxRememberMeCheckbox" />
                    </div>
                 </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <input type="submit" class="btn btn-default" tabindex="4" value="Login" />
                      <input type="hidden" name="ReturnUrl" value="">
                      <input type="hidden" name="browserData" id="idLoginBoxBrowserField">
                         <p ><a href="https://ico.wildapricot.org/Sys/ResetPasswordRequest" tabindex="10">Forgot password</a></p>
                    </div>
                </div>


             


                </form>


          </div>





        </div> <!--{end} maincol -->
      
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>