
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Core Specialist Training"
Metadescription   = "Framework to adopt a Strategic Approach for Vision Health in ireland"
ArticleURL        = DomainName & "Trainees/core-specialist-training.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Wesport Conference | Irish College of Ophthalmologists Annual Conference 2015"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Westport2015"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Wesport 2015"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;overflow:hidden;padding:0; background-image: url('1252westporthouse.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">Annual Conference 2015</h1> 
      </div>


   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="1379landscapeachillsoundmayo.jpg" alt="" width="960" height="58" title="wesport"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Wednesday 13th May</a></h2>
    <div id="collapse13" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im1.jpg" alt="" width="960" height="58" title="wesport"/>
          <!--#include file="13May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14">Thursday 14th May&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse14" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im2.jpg" alt="" width="960" height="58" title="wesport"/>
          <!--#include file="14May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Friday 15th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im3.jpg" alt="" width="960" height="58"  title="wesport"/>
          <!--#include file="15May.html" -->

      </div>
    </div>
  </div>





</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:0; background-image: url('1344landscapewestport.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2015</div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>