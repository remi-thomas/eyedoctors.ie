
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Annual Conference"
Metadescription   = "Annual Conference, Irish College of Ophthalmologists, May " & year(Now())
ArticleURL        = DomainName & "Kilkenny2022"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Kilkenny 2022 | Annual Conference | Irish College of Ophthalmologists Annual Conference"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Kilkenny2022"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Kilkenny 2020"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
/*.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
*/
.ctitle {color: #004b8d;font-weight:normal;}
.ctitle em {font-weight: 500;color:#ccc;;font-size: 2rem;}

.posters ul {list-style-type: none;}
.posters ul  li:nth-child(odd) { background: #eee; }

.modal-body h5 {font-weight: 500;color:#999;;font-size: 1.4rem;}

</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('Kilkenny_960X160.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2022</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOConf18" title="#ICOconf18" target="twiter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ICOconf17" alt="#ICOconf17" style="float:left;">&nbsp;#ICOKilkenny2022</a></p>
      </div>


   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro"><span style="">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_3_960X60.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse16">Monday 16th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_Graiguenamanagh_1_960X60.jpg" alt="" width="960" height="60" alt="Graiguenamanagh, Co Kilkenny" title="Graiguenamanagh, Co Kilkenny"/>
          <!--#include file="16May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse17">Tuesday 17th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse17" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_castle_5_960X60.jpg" alt="" width="960" height="60"  alt="Rose and castle" title="Rose and Castle"/>
          <!--#include file="17May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse18">Wednesday 18th May</a></h2>
    <div id="collapse18" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_Graiguenamanagh_2_960X160.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="18May.html" -->

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse19">Posters</a></h2>
    <div id="collapse19" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_Graiguenamanagh_2_960X160.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="posters.html" -->

      </div>
    </div>
  </div>

  <!--
  div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse16">Posters&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</a></h2>
    <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/Kilkenny2022/Kilkenny_4_960X60.jpg" alt="" width="960" height="60" alt="Killarney" title="Killarney"/>
          <include file="Poster.html">

      </div>
    </div>
  </div

-->

  <!--



  div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#biog">Key Speakers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</a></h2>
    <div id="biog" class="panel-collapse collapse">
        <img src="/Kilkenny2022/Kilkenny_5_960X60.jpg" alt="" width="960" height="60" alt="Graiguenamanagh, Co Kilkenny" title="Graiguenamanagh, Co Kilkenny"/>
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <--include virtual="/Kilkenny2022/biog/cynthia.html"
          <--include virtual="/Kilkenny2022/biog/mike.html"        
          <--include virtual="/Kilkenny2022/biog/patrick.html"
        

    </div>
  </div>
</div
-->


</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:5px 10px 0px; background-image: url('Kilkenny-castle-4-950X60.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2018&nbsp; &nbsp; 
      <br/>
      <span style="font-size:0.7em;">All photos &copy;<a href="http://www.failteireland.ie/" taregt="ext">F&aacute;ilte Ireland</a> 2018&nbsp; &nbsp; 

     </div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->




<div class="modal fade" id="biography" tabindex="-1" role="dialog" aria-labelledby="biographyLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="biographyModalLongTitle">r</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>
$('a[data-toggle="modal"]').click(function(){    
    
    var vtitle = $(this).attr('title');
    console.log(vtitle);
    var vcontent = "";
    var dataURL = 'biog/' + $(this).attr('data-href') + '.html';
    //console.log(dataURL);
    sendInfo(dataURL);

    $('#biography .modal-title').html(vtitle);

    //$('#biography .hide').show();
   //$('#biography').modal();
  
    /*$('#biography').on('hidden', function(){
    console.log("hidden");
      $('#biography').remove();
  });
  */
});


var request;

function sendInfo(url) {

  if (window.XMLHttpRequest) {
  request = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) {
  request = new ActiveXObject("Microsoft.XMLHTTP");
  }

  try {
  request.onreadystatechange = getInfo;
  request.open("GET", url, true);
  request.send();
  }
  catch (e) {
  console.log("Unable to connect to server");
  }
}

function getInfo() {
  if (request.readyState == 4) 
  {
    var vcontent = request.responseText;
  }
  else
  {
    var vcontent = "error";
  }
  //console.log(vcontent);
  $('#biography .modal-body').html(vcontent);
}


</script>
</body>
</html>