﻿
<%
Option Explicit
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Press Releasese"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "press  releases | Eye Doctors "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "pr"

' include at the top '
dim mypage, maxpages
dim rec         : rec = 0
dim maxdisplay  : maxdisplay = 15
mypage = request.querystring("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
  mypage=1
end if
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Press Releases"
BreadCrumbArr (1,1) = "/press-releases/default.asp"
BreadCrumbArr (2,0) = "Page " & mypage
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'



'get the latest PRs'
dim pr
set pr = Server.CreateObject("ADODB.Recordset")
pr.CursorLocation = 3
pr.cachesize = maxdisplay
pr.Open  "exec usp_SEL_PR NULL,0" , sqlConnection

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">
          <h1>Press Releases</h1>
          <!--#include file="press-releases.html" -->
      <%
        if NOT pr.eof THEN
          Dim PrURL 
          rec = 1
          pr.movefirst
          pr.pagesize = maxdisplay
          pr.absolutepage = mypage
          maxpages = cint(pr.pagecount)

          response.Write "<ul class=""news-list"">" & VbNewline
          do until pr.eof or (rec > maxdisplay)
               PrURL= CreateURLFriendlyPR(pr(0),FormatUSDateTime(pr(7)), DisplayText(pr(1)))
            response.Write "<li>" & VbNewline & _
            "<strong><a href=""" & PrURL & """ title=""" & DisplayText(pr(1)) & """ >" & DisplayText(pr(1)) & "</a></strong><br>" & VbNewline & _
            "<em>"& FormatPRDateTime(pr(7))&"</em><br>"
            if Len(pr(2)) > 3 then
            'if we have a header display it totally'
              response.Write  Displaytext(pr(2))
            else
              response.write TrimString(RemoveAllHTMLTags(RemoveAllPs(Displaytext(pr(3)))), 150,PrURL)
            end if
            'if len(pr("FileLocation")) > 3 then
            ' response.Write "<br><a href=""/pr/display.asp?PrID=" & pr(0) & """ >&hellip;</a>"
            'end if
            response.Write "</li>" & VbNewline

            rec = rec  + 1
          pr.movenext
          loop
        response.Write "</ul>" & vbnewline

      else
        response.Write "<p >There are no current press releases available</p>"
      end if'pr eof
      set pr = nothing
      if maxpages > 1 then
        call Paging
      else
        response.write "&nbsp;"
      end if
      %>


        </div> <!--{end} maincol -->
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->

      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
  if maxpages > 1 then
    dim pge : pge = MyPage
    dim counter
    Response.Write "<ul class=""pagination"">"
    for counter = 1 to maxpages
      if counter <> cint(pge) then
        Response.Write "<li><a href=""default.asp?mypage=" & counter & """>" & counter & "</a></li>"
      else
        Response.Write "<li class=""active""><span>" & counter & " <span class=""sr-only"">(current)</span></span></li>"
      end if
      ' If counter < maxpages then
      '   Response.write  " | "
      ' end if
    next
    Response.write "</ul>"
  end if
end sub
%>
</body>
</html>