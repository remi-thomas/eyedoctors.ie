﻿
<%
Option Explicit
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/fct/media.asp" --><%
'___________________________________________________________________
' include at the top '
dim PrID
PrID = request.querystring("PrID")
if Not IsNumeric(PrID) or PrID ="" then
	PrID=1
end if
dim pr
set pr = sqlconnection.execute("exec usp_SEL_PR "& prID &",0")
if pr.eof THEN
	response.redirect "/press-releases/default.asp"
end if

'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|
MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now())
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = pr("Subject") & " " & FormatUSDateTime(pr("PublicationDate")) & "Press releases | Eye Doctors Ireland "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "pr"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(3,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Press Releases"
BreadCrumbArr (1,1) = "/press-releases/default.asp"
BreadCrumbArr (2,0) = FormatDateTimeIRL(pr("PublicationDate"),1)
BreadCrumbArr (2,1) = ""
BreadCrumbArr (3,0) = pr("Subject")
BreadCrumbArr (3,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">
<%
		if NOT pr.eof then
			response.write "<h1>" & DisplayText(pr(1)) & "</h1>" & VbNewline 
			if pr(9) > 0 then
				response.write DrawMediabyPR(prID)
			end if
			if len(pr("FileLocation")) > 3 then
				response.Write ReadPage(DisplayText(pr("FileLocation")))
			else
				response.Write  pr("Content")
			end if
			response.Write "<p><em>" & FormatPRDateTime(pr(7)) & "</em></p>" & VbNewline 
		else
			response.Write "<p >There are no current press releases available</p>"
		end if'pr eof
		set pr = nothing
%>	
         
        </div> <!--{end} maincol -->
		<!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->