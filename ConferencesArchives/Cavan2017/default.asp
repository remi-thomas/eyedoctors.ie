
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Core Specialist Training"
Metadescription   = "Framework to adopt a Strategic Approach for Vision Health in ireland"
ArticleURL        = DomainName & "Trainees/core-specialist-training.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Cavan 2017 | Conference | Irish College of Ophthalmologists Annual Conference 2015"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Westport2015"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Cavan 2017"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="robots" content="noindex">
<style>
.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->
<!--#include virtual="/ssi/incl/metadata.asp" -->
<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('ch_jan07_105.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2017</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOConf17" title="#ICOconf17" target="twiter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ICOconf17" alt="#ICOconf17" style="float:left;">&nbsp;#ICOconf17</a></p>
      </div>


   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro"><span style="">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/cavan2017/im1.jpg" alt="" width="960" height="58" alt="Ross Castle, Killarney, Co. Kerry" title="Ross Castle, Killarney, Co. Kerry"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Wednesday 17th May</a></h2>
    <div id="collapse13" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/cavan2017/im2.jpg" alt="" width="960" height="58" alt="Gap of Dunloe" title="Gap of Dunloe"/>
          <!--#include file="17May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14">Thursday 18th May&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse14" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/cavan2017/im3.jpg" alt="" width="960" height="58"  alt="Purple Mountain over Muckross Lake" title="Purple Mountain over Muckross Lake"/>
          <!--#include file="18May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Friday 19th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/cavan2017/im4.jpg" alt="" width="960" height="58" alt="Lakes of Killarney" title="Lakes of Killarney"/>
          <!--#include file="19May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#biog">Key Speakers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</a></h2>
    <div id="biog" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Jonathan_Crowston.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Augusto_Azuara_Blanco.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/leon-au.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/ian_dooley.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Susan_Kelly.html" -->

          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Pearse_Keane.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/alex_shortt.html" -->

          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Yvonne-Delaney.html" -->


          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Pat_McGettrick.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Patricia_Quinlan.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Angela_Tysall.html" -->


          <!--#include virtual="/ConferencesArchives/cavan2017/biog/douglas_veale.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/richard_farrell.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/hadas_levy.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Stephen_McIvor.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Aydin_Pinar.html" -->
          <!--#include virtual="/ConferencesArchives/cavan2017/biog/Chris_White.html" -->
      </div>
    </div>
  </div>



</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:0; background-image: url('banner5.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2015</div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>