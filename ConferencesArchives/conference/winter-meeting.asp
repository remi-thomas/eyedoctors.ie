
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "company-structure.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "MockUP | Eye Doctors "
SiteSection       = "About" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "About Us"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Bla"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
  .schedule-item h4 {padding:0;margin:0;font-size:1.3em;}
  .schedule-item h5 {padding:0;margin:0;font-size:1.1em;}
  .schedule-item h6 {padding:0;margin:0;font-size:1em;color:#000}
  .schedule-item {border-bottom: 1px solid #cad4f6;padding-top: 15px;padding-bottom: 15px;transition: background-color ease-in-out .3s;}
  .row {display: flex;}
  .schedule-item time {padding-bottom: 5px;display: inline-block;}
  .schedule-item  .programme-row {padding-top:10px;padding-bottom:5px;}
  .description {color: #403f3e; padding-top:5px;}

</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
      

            <h1>ICO Winter Meeting 2019</h1>





<div class="row">
   <div class="col-md-12">
<p>We request that ICO members register for the Winter Meeting and Montgomery Lecture through the <a href="https://www.eyedoctors.ie/members/login/">members portal</a>.</p>
<p class="description">Venue: Chartered Accountants House, 47&nbsp;Pearse Street, Dublin 2</p>
   </div>
</div>


<h2>Morning Programme</h2>
<div class="row schedule-item">
   <div class="col-md-2"><time>9.15&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Registration</h4></div>
</div>
<div class="row schedule-item">
   <div class="col-md-2"><time>9.30&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Welcome</h4>
    <h6>Dr Patricia Quinlan</h6> President, ICO
   </div>
</div>

<div class="row schedule-item">
   <div class="col-md-2"><time>9.35&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Session 1: ICO Professional Competence Scheme</h4>
    <h6>Chair: Dr Grace O&rsquo;Malley</h6> Ethics Committee, ICO

      <div class="programme-row">
            <h5>Getting to Grips with PCS</h5>
            <h5>ICO Online Journal Club </h5>
            <h6>Niamh Coen</h6> Business Process Improvement, ICO
      </div>

      <div class="programme-row">
            <h5>ICO National Crowd Sourced Audit 2019-2020</h5>
            <h6>Dr Maureen Hillery</h6> Ethics Committee, ICO
      </div>

   </div>
</div>

<div class="row schedule-item">
   <div class="col-md-2"><time>10.30&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Q &amp; A</h4>
   </div>
</div>

<div class="row schedule-item">
   <div class="col-md-2"><time>10.45&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Tea / Coffee</h4>
   </div>
</div>



<div class="row schedule-item">
   <div class="col-md-2"><time>11.00&nbsp;AM</time></div>
   <div class="col-md-10"><h4>Session 2: Five Years of Retina Screen &ndash; Progress Report and Future Plan</h4>
    <h6>Chair: Mr John Doris</h6> Scientific &amp; Continuing Professional Development Committee
    <p class="description">The session will include updates from providers Global Vision and Northgate Medical; Treatment Centre Reports; The outcome of Screening for 5 Years; and Programme Developments</p>



      <div class="programme-row">
            <h6>Mr David Keegan</h6> National Clinical Lead for Diabetic Retinopathy, National Diabetic Screening Service
      </div>

      <div class="programme-row">
            <h6>Ms Colette Murphy</h6> Programme Manager, DiabeticRetina Screen, National Diabetic Screening Service</h5>
            Dr Maureen Hillery, Ethics Committee, ICO
      </div>

   </div>
</div>

<div class="row schedule-item">
   <div class="col-md-2"><time>12.25&nbsp;PM</time></div>
   <div class="col-md-10"><h4>Announcement of the ICO / Novartis Eye Research Bursary Recipient 2019/2020</h4>
   </div>
</div>


<div class="row schedule-item">
   <div class="col-md-2"><time>12.30&nbsp;PM</time></div>
   <div class="col-md-10"><h4>Lunch</h4>
   </div>
</div>






<h2>Rami Meeting </h2>

<div class="row schedule-item">
   <div class="col-md-2">1.30&nbsp;PM</div>
    <div class="col-md-10"><h4>RAMI Meeting </h4>
      <p class="description">
             The Section of Ophthalmology of the Royal Academy of Medicine in Ireland are holding their winter meeting on Friday 29th November 2019 at the Chartered Accountants House, 47 Pearse Street, Dublin 2 at&nbsp;1.30pm.
      </p>
      <p><a href="https://www.rami.ie/event/ophthalmology-section-meeting-friday-29th-november-2019/" target="_blank">More info on rami.ie</a></p>

<p>If you are selected to present, you must be a member of RAMI.&nbsp; Join online at <a href="https://www.rami.ie/membership-registration/">Membership Application</a></p>

    </div>
</div>



<h2>Annual Montgomery Lecture 2019 </h2>

<div class="row schedule-item">
   <div class="col-md-2"><time>5.15&nbsp;PM</time></div>
   <div class="col-md-10"><h4>Annual Montgomery Lecture Reception, Chartered Accountants House</h4></div>
</div>

<div class="row schedule-item">
   <div class="col-md-2"><time>6.10&nbsp;PM</time></div>
   <div class="col-md-10"><h4>Old Anatomy Theatre &amp; Museum curated tour</h4>
   <p class="description">Chemistry Building, Trinity College Dublin. Directions <a href="http://www.tcd.ie/Maps/map.php?b=74&amp;i=b35" target="map">Map</a></p>
   </div>
</div>
<div class="row schedule-item">
   <div class="col-md-2"><time>6.30&nbsp;PM</time></div>
    <div class="col-md-10"><h4>Annual Montgomery Lecture 2019 </h4>


      <div class="programme-row">
            <h5>Surgical Errors: Why Things Go Wrong and How To Prevent It</h5>
             <h6>Mr Paul Sullivan</h6>
             Consultant Ophthalmologist and Director of Education, Moorfields Eye Hospital NHS Trust Foundation
      </div>

    </div>
</div>




















          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
        <!-- Side: Menu -->
          <dl>
            <dt><a href="#">News &amp; Events</a></dt>
            <dd>
              <ul>
                <li class="active"><a href="#">Press Releases</a></li>
                <li><a href="#">Events</a></li>
              </ul>
            </dd>
          </dl>
          <!-- Side: Content Box -->
          <h2><a href="#">Your Sight Our Vision</a></h2>
          <div class="box">
            <img src="img/sidepic1.jpg" alt=""/>
            <p>New campaign to hightlight the impact of lifestyle on our eye health.</p>
            <p><a href="#" class="btn">About the Campaign</a></p>
          </div> <!--{end}  box -->
          
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>