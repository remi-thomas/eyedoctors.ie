
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Core Specialist Training"
Metadescription   = "Annual Conference, Irish College of Ophthalmologists, May " & year(Now())
ArticleURL        = DomainName & "Kilkenny2018"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Galway 2019 | Conference | Irish College of Ophthalmologists Annual Conference 2015"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Galway2019"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Galway 2019"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="robots" content="noindex">
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->


<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('/galway2019/photos/ballynahinch_960X160c.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2019</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOConf18" title="#ICOconf18" target="twiter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ICOconf17" alt="#ICOconf19" style="float:left;">&nbsp;#ICOconf19</a></p>
      </div>


      <div style="padding:10px">
<p><a class="btn" href="https://ico.wildapricot.org/event-3206165/Registration" target="wildapricot">Register</a><br />
<span style="color:#666">* Please note non ICO members will need to enter code LS9V2HBB to register for this event</span></p>
    </div>
   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro"><span style="">Introduction &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/Wild-Atlantic-Way2_960X160.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Wednesday 15th May &nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/connemarra_960X160.jpg" alt="" width="960" height="60" alt="Connemarra" title="Connemarra"/>
          <!--#include file="15May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse16">Thursday 16th May&nbsp; &nbsp; &nbsp;&nbsp;</a></h2>
    <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/Wild-Atlantic-Way2_960X160.jpg" alt="" width="960" height="60"  alt="Wild Atlantic Way" title="Wild Atlantic Way"/>
          <!--#include file="16May.html" -->

      </div>
    </div>
  </div>



  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse17">Friday 17th May &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</a></h2>
    <div id="collapse17" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/Fly_fishing_960X60.jpg" alt="" width="960" height="60" alt="Fishing" title="Fishing"/>
          <!--#include file="17May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsePosters">Posters&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</a></h2>
    <div id="collapsePosters" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/RenvyleConnemara_960X60.jpg" alt="" width="960" height="60" alt="Killarney" title="Killarney"/>
          <!--#include file="Poster.html" -->

      </div>
    </div>
  </div>
    


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#biog">Key Speakers &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</a></h2>
    <div id="biog" class="panel-collapse collapse">
        <img src="/ConferencesArchives/Galway2019/photos/galway_connemara_960X60.jpg" alt="" width="960" height="60" alt="" title=""/>
      <div class="panel-body" style="max-width:950px;overflow:hidden">
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/03.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/bayly.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/chamney.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/corr.html" -->
       <!--#include virtual="/ConferencesArchives/Galway2019/biog/hartnett.html" --> 
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/04.html" -->   

      <!--#include virtual="/ConferencesArchives/Galway2019/biog/Jones.html" -->   
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/khan.html" -->   
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/king.html" -->  
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/01.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/quill.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/mullaney.html" -->
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/05.html" -->          
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/ubig.html" -->       
      <!--#include virtual="/ConferencesArchives/Galway2019/biog/02.html" -->    




    </div>
  </div>
</div>




  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#Accommodation">Accommodation &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</a></h2>
    <div id="Accommodation" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/Dun_Aonghasa_960X160.jpg" alt="" width="960" height="60" alt="Killarney" title="Killarney"/>
          <!--#include file="accommodation.html" -->

      </div>
    </div>
</div>

<%
'remove after date ' keeping a reference for nfollowing  conference'
If 2 = 1 then
%>
  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#AbstractSubmissions">Abstract Submissions&nbsp;</a></h2>
    <div id="AbstractSubmissions" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Galway2019/photos/galway-wall_960X160.jpg" alt="" width="960" height="60" alt="Killarney" title="Killarney"/>
          <!--#include file="absctracts-submission.html" -->

      </div>
    </div>
</div>

<%
end if
%>





</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:5px 10px 0px; background-image: url('/galway2019/photos/connemarra_960X60.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2019&nbsp; &nbsp; 
      <br/>
      <span style="font-size:0.7em;">All photos &copy;<a href="http://www.failteireland.ie/" target="ext" style="color:#ffd">F&aacute;ilte Ireland</a> 2019&nbsp; &nbsp; 

     </div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->


</body>
</html>