
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Core Specialist Training"
Metadescription   = "Annual Conference, Irish College of Ophthalmologists, May " & year(Now())
ArticleURL        = DomainName & "Kilkenny2018"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Kilkenny 2018 | Conference | Irish College of Ophthalmologists Annual Conference 2015"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Westport2015"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Kilkenny 2018"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="robots" content="noindex">
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->


<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('Kilkenny_960X160.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2018</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOConf18" title="#ICOconf18" target="twiter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ICOconf17" alt="#ICOconf17" style="float:left;">&nbsp;#ICOconf18</a></p>
      </div>


   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro"><span style="">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_3_960X60.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Wednesday 16th May</a></h2>
    <div id="collapse13" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_Graiguenamanagh_1_960X60.jpg" alt="" width="960" height="60" alt="Graiguenamanagh, Co Kilkenny" title="Graiguenamanagh, Co Kilkenny"/>
          <!--#include file="16May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14">Thursday 17th May&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse14" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_castle_5_960X60.jpg" alt="" width="960" height="60"  alt="Rose and castle" title="Rose and Castle"/>
          <!--#include file="17May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Friday 18th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_Graiguenamanagh_2_960X160.jpg" alt="" width="960" height="60" alt="Castle, Killarney" title="Castle, Killarney"/>
          <!--#include file="18May.html" -->

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse16">Posters&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</a></h2>
    <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_4_960X60.jpg" alt="" width="960" height="60" alt="Killarney" title="Killarney"/>
          <!--#include file="Poster.html" -->

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#biog">Key Speakers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</a></h2>
    <div id="biog" class="panel-collapse collapse">
        <img src="/ConferencesArchives/Kilkenny2018/Kilkenny_5_960X60.jpg" alt="" width="960" height="60" alt="Graiguenamanagh, Co Kilkenny" title="Graiguenamanagh, Co Kilkenny"/>
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/cynthia.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/mike.html" -->           
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/patrick.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/millicent.html" -->       
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/philip.html" -->            
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/eva.html" -->    
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/michelle.html" -->             
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/conor.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/dara.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/rory.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/edward.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/fiona.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/mark.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/susan.html" -->
          <!--#include virtual="/ConferencesArchives/Kilkenny2018/biog/MichealRourke.html" -->


    </div>
  </div>
</div>


</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:5px 10px 0px; background-image: url('Kilkenny-castle-4-950X60.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2018&nbsp; &nbsp; 
      <br/>
      <span style="font-size:0.7em;">All photos &copy;<a href="http://www.failteireland.ie/" taregt="ext">F&aacute;ilte Ireland</a> 2018&nbsp; &nbsp; 

     </div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>