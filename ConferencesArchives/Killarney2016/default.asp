
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Core Specialist Training"
Metadescription   = "Framework to adopt a Strategic Approach for Vision Health in ireland"
ArticleURL        = DomainName & "Trainees/core-specialist-training.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Killarney 2016 ! Conference | Irish College of Ophthalmologists Annual Conference 2015"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "Westport2015"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Killarney 2016"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="robots" content="noindex">
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('banner3.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2016</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOConf2016" title="#ICOconf2016" target="twiter" style="color:#fff;text-decoration: none"><img src="/images/twitter.png" width="20" title="#ICOconf2016" alt="#ICOconf2016" style="float:left;">&nbsp;#ICOconf2016</a></p>
      </div>


   

<div class="panel-group" id="accordion">

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im4.jpg" alt="" width="960" height="58" alt="Ross Castle, Killarney, Co. Kerry" title="Ross Castle, Killarney, Co. Kerry"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Wednesday 18th May</a></h2>
    <div id="collapse13" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im1.jpg" alt="" width="960" height="58" alt="Gap of Dunloe" title="Gap of Dunloe"/>
          <!--#include file="18May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14">Thursday 19th May&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse14" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im2.jpg" alt="" width="960" height="58"  alt="Purple Mountain over Muckross Lake" title="Purple Mountain over Muckross Lake"/>
          <!--#include file="19May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Friday 20th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="im3.jpg" alt="" width="960" height="58" alt="Lakes of Killarney" title="Lakes of Killarney"/>
          <!--#include file="20May.html" -->

      </div>
    </div>
  </div>





</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:0; background-image: url('banner5.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2015</div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->
        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>