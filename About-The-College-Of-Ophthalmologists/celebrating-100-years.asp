
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Specialist Training in Ophthalmology"
Metadescription   = "About the College Of Ophthalmologists: Who We are "
ArticleURL        = DomainName & "About-The-College-Of-Ophthalmologists/who-we-are.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Information about the College &amp; Mission Statement | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Health-Policy" ' highlight correct menu tab'
SiteSubSection    = "100year"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "About The College"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Information about the College &amp; Mission Statement"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <!--#include file="celebrating-100-years.html" -->
   

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="About-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>