<!-- Side: Menu -->
          <dl>
            <dt><a href="/About-The-College-Of-Ophthalmologists/">About Us</a></dt>
            <dd>
              <ul>
              <li <%if SiteSubSection="whoweare" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/who-we-are.asp">Who We Are</a></li>
              
              <li <%if SiteSubSection="strategic" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/ICO-Strategic-Plan.asp">Our Vision - Strategic Plan</a></li>
              <li <%if SiteSubSection="structure" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/our-structure.asp">Our Structure</a></li>

              <li <%if SiteSubSection="governance" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/governance.asp">Governance</a></li>


              <li <%if SiteSubSection="annualreport" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/annual-reports.asp">Annual Reports</a></li>

              <li <%if SiteSubSection="100year" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/celebrating-100-years.asp">Celebrating 100 Years</a></li>

              <li <%if SiteSubSection="pressoffice" then response.write " class=""active"""%>><a href="/About-The-College-Of-Ophthalmologists/Press-office.asp">Press Office</a></li>


              </ul>
            </dd>
          </dl>