﻿
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "A to Z of Irish Ophthalmologists | Eye Doctors: "& year(Now())
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/Ophthalmologists/eye-doctors-list.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "A to Z of Ophthalmologists in Ireland | Eye Doctors "
SiteSection       = "Directory" ' highlight correct menu tab'


dim lt: lt = trim(request.querystring("lt"))
if len(lt) <> 1 then
	response.redirect "default.asp?ThisErr=lt("& lt &") is not valid"
end if

'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "Find an Ophthalmologist"
BreadCrumbArr (0,1) = "/opthalmologists/"
BreadCrumbArr (1,0) = "A to Z"
BreadCrumbArr (1,1) = "eye-doctors-list.asp"
BreadCrumbArr (2,0) = lt
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'


%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
        <div class="col-md-8 maincol col-sm-12">
          <h1>Eye Doctors Directory</h1>

		  <%
            If lt = "" Then
                lt = fl ' lt is null, give the value first letter
            End If
            dim sql
            If lt <> "9" then
                sql = "usp_sel_MembersStartingByLetter '"&lt&"'"
            Else
                sql = "usp_sel_MembersAlphabetical"
            End if

            dim RecordCount : RecordCount = 0
            set rs = Server.CreateObject("ADODB.Recordset")
            rs.CursorLocation = 3
            rs.Open  sql , sqlConnection

            If Not rs.eof then
              RecordCount = rs.RecordCount
              rs.movefirst

              if lt <> "9" then
              %>
              <h2><em><%=lt%></em></h2>
              <%
              else
              %>
              <h2>List of all Ophthalmologists in Ireland</h2>
              <%
              end if
              %>
              <div id="details">
                <ul <% if RecordCount >= 10 then response.write " class=""list-cols"" " %> style="display: table;">
                  <%
                  Do While Not rs.eof
                      ' I need to remove one of the doctors from the web directory - Gerard O Reilly who has passed away. 
                      If rs("MemberID") <> 100 then
                      dim MemberName
                      MemberName = replace(replace(rs("Title"),"Pr","Prof"),"Profof","Prof")  & " " & rs("FirstName") & " " & rs("LastName")
                      %>
                          <li><a href="<%=CreateURLFriendlyDoctor(rs("MemberID"),rs("FirstName") & " " & rs("LastName"),rs("Title"))%>" title="See <%=MemberName%>'s record"><%=MemberName%></a></li>
                      <%
                      End if
                  rs.movenext
                  Loop
                  %>
                </ul>
              </div>
            <%
            Else
                response.write "<em>Sorry, there are no ophthalmologists in Ireland starting with the letter " & lt
            END IF
            %>

        </div> <!-- {end} col md-8 -->
        <!-- Side Column -->
        <div class="col-md-4 sidecol col-sm-12">
	      <div class="member-atoz">
            <h2>A to Z</h2>
	        <div>
  		 	  <%
              dim fl
              Set rs = SqlConnection.Execute("usp_sel_AZ_Members")
              fl = rs(0) 'give a value to firstletter
              Do Until rs.eof
                response.write "<a href=""eye-doctors-list.asp?lt=" & rs(0) & """>" & rs(0) & "</a> <span>|</span> "
              rs.movenext
              Loop
              response.write "<a href=""eye-doctors-list.asp?lt=9"" class=""btn""><em>List all</em></a>"
              %>
            </div>
          </div>
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->
    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>