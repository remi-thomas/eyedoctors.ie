﻿

<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%

dim MemberFirstName, MemberName, restr, MoreName, MemberID, QMemberLastName,  QMemberFirstName
dim SpecialtyID

dim SpecialtyTitle, SpecialtyDescription

MemberFirstName 	= ValidateText(left(trim(Request.querystring("MemberFirstName")),40))
QMemberFirstName	= RemoveXSS(MemberFirstName)
MemberName  		= ValidateText(left(trim(Request.querystring("MemberName")),50))
QMemberLastName		= RemoveXSS(MemberName)
restr   			= ValidateText(request.querystring("restr"))
MoreName			= ValidateText(request.querystring("MoreName"))
SpecialtyID			= ValidateText(left(trim(Request.querystring("SpecialtyID")),5))


if not IsNumeric(SpecialtyID) or SpecialtyID = "" then

	'only one letter - redirect to list
	If Len(MemberName) = 1 And Len(MemberFirstName)=0 Then
	    response.redirect "eye-doctors-list.asp?lt=" & MemberName
	End If
	'only one letter - redirect to list
	If Len(MemberFirstName) = 1 And Len(MemberName)=0 Then
	    response.redirect "eye-doctors-list.asp?lt=" & MemberFirstName
	End If


	'only one letter - redirect to list
	If Len(MemberName) = 1 And Len(MemberFirstName)=0 Then
		response.redirect "eye-doctors-list.asp?lt=" & MemberName
	End If
	'only one letter - redirect to list
	If Len(MemberFirstName) = 1 And Len(MemberName)=0 Then
		response.redirect "eye-doctors-list.asp?lt=" & MemberFirstName
	End If

	if not IsNumeric(restr) or restr = "" then
		restr = 0
	end if
	if not IsNumeric(MoreName) or MoreName = "" then
		MoreName = 0
	end if
	if len(MemberName) > 2 then
		MemberID = 0
	else
		'if len(MemberFirstName) > 2 then
		''	MemberName = MemberFirstName
		''	MemberFirstName = ""
		'else
			MemberName = ""
		'end if
	end if


	if request.querystring("searchusers") = "true" then
		dim sql
		if restr = 1 then
			sql = " exec usp_SEL_SearchByName '"& Replace(MemberName,"*","_") &"','"& Replace(MemberFirstName,"*","_") & "',"& MoreName
		else
			restr = 0
			sql = " exec usp_SEL_SearchByName '"& Replace(MemberName,"*","_") &"','"& Replace(MemberFirstName,"*","_") & "',"& MoreName
		end if
		'response.write sql
		set rs=sqlconnection.execute(sql)
		if not rs.eof then
			'this is a name search '
			'PageHtml = PageHTML & vbnewline &  "<br/>## test only##" & VbNewline & _
			'"<br/>match = " & rs(26) & VbNewline & _
			'"<br/> near match = " & rs(27) & VbNewline & _
			'"<br/> straight search= " & rs(28)
			dim ExactMatch, NearMatch, StraightSearch
			ExactMatch			= rs("ExactMatch")
			NearMatch			= rs("NearMatch")
			StraightSearch 		= rs("StraightSearch")

			dim Tailvar
			Tailvar = "&amp;disp=src&amp;MemberName=" & MemberName & "&amp;MemberFirstName=" &  MemberFirstName & "&amp;restr=" & restr


			dim Message : Message = ""

			if ExactMatch > 0 and MoreName = 0 then
				if instr(MemberName,"*") > 1 then
					Message = "<h3>Results</h3> <p>Your search returns <strong>" & ExactMatch &" member"
					if ExactMatch > 1 then Message = Message & "s"
					Message = Message &"</strong> based on the following string '" & MemberName & "' <br><em> where the * symbol can be any letter</em>.</p>"
				else
					Message = "<h3>Results</h3> <p>Your search returned <strong>" & ExactMatch & " member"
					if ExactMatch > 1 then Message = Message & "s"
					Message = Message & "</strong> matching your search criteria"
						if NearMatch = 1 then
							Message = Message & " and <a href=""/opthalmologists/eye-doctors-database.asp?MoreName=1" & Tailvar & """>" & NearMatch & " other name</a> that has a similar spelling or sounds like it"
						elseif NearMatch > 1 then
							Message = Message & " and <a href=""/opthalmologists/eye-doctors-database.asp?MoreName=1" & Tailvar & """>" & NearMatch & " other names</a> that have a similar spelling or sound like it"
						end if
					Message = Message & ".</p>"
				end if
			elseif StraightSearch = 0 and NearMatch > 0 and MoreName = 0 then
			' are we looking for a * ?'
				Message = "Your search returns no member matching your search criteria <br/>We found some names which have a similar spelling or sound like it"
			elseif MoreName = 1 then
				Message = "This are the results of your search showing the members for which the name sound similar than the one you query for: '"& MemberName &"' <br/>" & VbNewline & _
				"<a href=default.asp?MoreName=0" & Tailvar & """>Restrict your search to '"& MemberName &"'</a>"& VbNewline
			end if

			'##########################'
			'### start the table    ###'
			'### broaden / restrict ###'
			'##########################'
			dim PageHtml : PageHtml = ""
			if NearMatch > 0 and StraightSearch>0 then
				if InStr(MemberName, "*")= 0 then
					PageHtml = PageHTML & vbnewline &  "<br /><p>" & message & "</p>"& VbNewline
					PageHtml = PageHTML & vbnewline & "</table>" & VbNewline
				else
					PageHtml = PageHTML & vbnewline &  "<p class=""membertext"">" & message & "</p>" & VbNewline
				end if
			else
				PageHtml = PageHTML & vbnewline &  "<p class=""membertext"">" & message & "</p>" & VbNewline
			end If

			PageHtml = PageHTML & vbnewline &  "<ul>" & vbnewline
			Do until rs.eof
				MemberID = rs("MemberID")
				MemberName = replace(replace(rs("Title"),"Pr","Prof"),"Profof","Prof")  & " " & rs("FirstName") & " " & rs("LastName")

				PageHtml = PageHTML & vbnewline &  "<li><a href=""" & CreateURLFriendlyDoctor(rs("MemberID"), rs("FirstName") & " " & rs("LastName"), rs("Title")) & """ title=""See " & MemberName & "'s record"">" & MemberName & "</a></li>" & vbnewline

			rs.Movenext
			Loop
			PageHtml = PageHTML & vbnewline &  "</ul>" & vbnewline

		else 'eof
				PageHtml = PageHTML & vbnewline &  "<br />" & vbnewline
				PageHtml = PageHTML & vbnewline &  "<ul><li style=""color:#c00"">Your search for '<em>"
					if len(MemberFirstName) > 2 then
						PageHtml = PageHTML & vbnewline &  MemberFirstName & " "
					end if
					PageHtml = PageHTML & vbnewline &   MemberName &"'</em> returned no result</li>" & VbNewline

				PageHtml = PageHTML & vbnewline &  "</ul>" & vbnewline
		end if
	End If 'request.querystring(searchusers) = true

	'___________________________________________________________________
	'| Variables below for meta data                                       |
	'______________________________________________________________________|
	MetaAbstract      = "Directory of Irish Ophthalmologists | Eye Doctors: "& year(Now())
	Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
	ArticleURL        = DomainName & "/Ophthalmologists/eye-doctors-database.asp"
	ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
	PageTitle         = "Ophthalmologists Directory | Eye Doctors "
	SiteSection       = "Directory" ' highlight correct menu tab'
	'______________________________________________________________________|'
	'| Breadcrumbs                                                         |
	'______________________________________________________________________|
	ReDim BreadCrumbArr(1,1)
	BreadCrumbArr (0,0) = "Find an Ophthalmologist"
	BreadCrumbArr (0,1) = "/opthalmologists/"
	BreadCrumbArr (1,0) = "Results for &quot;" & trim(QMemberFirstName & " " & QMemberLastName) & "&quot;"
	BreadCrumbArr (1,1) = ""

	'|                                                                     |'
	'______________________________________________________________________|'


else

	set rs=sqlconnection.execute("exec usp_SEL_SearchBySpecialities " & SpecialtyID)
	if rs.eof then
		dim sp
		set sp  = sqlconnection.execute("select top 1 s.SpecialtyTitle, s.SpecialtyDescription from  specialties s where S.SpecialtyID=" & SpecialtyID)
		if not sp.eof then
			SpecialtyTitle 			= sp("SpecialtyTitle")
			SpecialtyDescription	= sp("SpecialtyDescription")
		else
			SpecialtyTitle = "" : SpecialtyDescription = ""
		end If
		set sp  = nothing
		Message = "<h3>Results</h3> <p>Your search returns no results for doctors within the speciality '"& SpecialtyTitle &"' </p>"
	else
		SpecialtyTitle 				= rs("SpecialtyTitle")
		SpecialtyDescription	= rs("SpecialtyDescription")
		message 							= "<h3>Eye doctor Directory: " & specialtyTitle  & "</h3>"
	end if


	'___________________________________________________________________
	'| Variables below for meta data                                       |
	'______________________________________________________________________|
	MetaAbstract      = SpecialtyTitle & " | Directory of Irish Ophthalmologists |  Eye Doctors: "& year(Now())
	Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
	ArticleURL        = DomainName & "/Ophthalmologists/eye-doctors-database.asp"
	ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
	PageTitle         = SpecialtyTitle & " | Ophthalmologists Directory | Eye Doctors "
	SiteSection       = "Directory" ' highlight correct menu tab'
	'______________________________________________________________________|'
	'| Breadcrumbs                                                         |
	'______________________________________________________________________|
	ReDim BreadCrumbArr(1,1)
	BreadCrumbArr (0,0) = "Find an Ophthalmologist"
	BreadCrumbArr (0,1) = "/opthalmologists/"
	BreadCrumbArr (1,0) = SpecialtyTitle 
	BreadCrumbArr (1,1) = ""

	'|                                                                     |'
	'______________________________________________________________________|'



			PageHtml = PageHTML & vbnewline &  "<p class=""membertext"">" & message & "</p>" & VbNewline
			PageHtml = PageHTML & vbnewline &  "<ul>" & vbnewline
			Do until rs.eof
				MemberID = rs("MemberID")
				MemberName = replace(replace(rs("Title"),"Pr","Prof"),"Profof","Prof")  & " " & rs("FirstName") & " " & rs("LastName")

				PageHtml = PageHTML & vbnewline &  "<li><a href=""" & CreateURLFriendlyDoctor(rs("MemberID"), rs("FirstName") & " " & rs("LastName"), rs("Title")) & """ title=""See " & MemberName & "'s record"">" & MemberName & "</a></li>" & vbnewline

			rs.Movenext
			Loop
			PageHtml = PageHTML & vbnewline &  "</ul>" & vbnewline




end if


BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr


%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
        <div class="col-md-8 maincol">
          <h1>Eye Doctors Directory</h1>

          <%
          'fom for members'
          if not IsNumeric(SpecialtyID) or SpecialtyID = "" then
          %>
          <form name="searchmember" action="eye-doctors-database.asp" method="get" onsubmit="return validate_form(this);" class="form-inline member-search" role="form">
            <h2>Search by name</h2>
            <div class="form-group">
              <label class="sr-only" for="MemberFirstName">First Name</label>
              <input type="text" name="MemberFirstName" size="23" value="<%=QMemberFirstName%>" onfocus="rstfirstname()" onblur="stfirstname()" class="form-control" placeholder="First Name">
            </div>
            <div class="form-group">
              <label class="sr-only" for="MemberName">Last Name</label>
              <input type="text" name="MemberName" size="26" value="<%=QMemberLastName%>" onfocus="rstname()" onblur="stname()" class="form-control" placeholder="Last Name">
            </div>
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="<%=restr%>">
            <input type="submit" value="Search" class="btn btn-default">
            <p><em>You can use the * symbol as a wildcard character</em></p>
          </form>

  		<%
          'fom for members'
          elseif IsNumeric(SpecialtyID) and SpecialtyID <> "" then
          set sp = sqlconnection.execute("SELECT SpecialtyID,SpecialtyTitle from specialties order by SpecialtyTitle")
	    %>
	     <form name="searchspecialities" action="eye-doctors-database.asp" method="get" onsubmit="submit(this);" class="form-inline member-search" role="form">
	            <div class="form-group">
	                    <select name="SpecialtyID" onchange="submit()" class="form-control">
	                        <option value="">Any Specialities</option>
	                        <% 
	                        do until sp.eof
	                            response.write vbtab & "<option value="""& sp("SpecialtyID") &""">"& sp("SpecialtyTitle") &"</option>" & vbnewline
	                        sp.movenext
	                        Loop
	                        %>

	                    </select>
	            </div>
	            
	            <input type="hidden" name="MemberName" value="">
	            <input type="hidden" name="searchusers" value="true">
	            <input type="hidden" name="restr" value="">
	            <input type="submit" value="Search" class="btn btn-default">
	       </form>
          <%
            Set sp = Nothing
          %>

          <%
          end if
          %>


          <div>
            	<% response.write PageHtml %>
          </div>

        </div> <!-- {end} col md-8 -->

        <!-- Side Column -->
        <div class="col-md-4 sidecol">
          <!-- Side: Menu -->        
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->
    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>