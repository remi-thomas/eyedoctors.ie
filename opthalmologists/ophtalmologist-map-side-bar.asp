 <form name="searchcounty" class="member-county" action="List-eye-doctors-by-county.asp" method="post" onsubmit="return validate_form(this);">
    <h2>List Eye Doctors by County</h2>
	<select name="CountyID" onchange="submit()" class="form-control">
      <option value="">All counties</option>
          <% 
          dim co
          Set co = sqlconnection.execute("SELECT c.CountyID, c.County, c.ListOrder FROM  County AS c INNER JOIN   Members_practice AS p ON c.CountyID = p.PracticeCountyID GROUP BY c.CountyID, c.County, c.ListOrder ORDER BY c.ListOrder ")
            Do While Not co.eof
              response.write "<option value="""& co("CountyID")& """>"& co("County") &"</option>" & vbnewline
            co.movenext
            Loop
            Set co = nothing
      %>
      </select>
     <img src="/images/ireland-map-300.png" alt="Map of Ireland" usemap="#Map">
	  <map name="Map" id="Map">
        <area title="Monaghan" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=23" coords="206,98,200,107,194,124,206,127,225,144,233,131,227,116,214,100" shape="poly">
        <area title="Galway" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=12" coords="35,175,49,176,83,190,95,174,119,167,134,186,141,203,147,209,152,211,140,224,130,236,118,231,103,234,95,221,96,209,62,210,30,191" shape="poly">
        <area title="Waterford" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=29" coords="146,315,161,337,175,334,181,324,195,319,214,318,212,309,198,307,189,300,170,300,169,311" shape="poly">
        <area title="Longford" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=19" coords="172,144,160,156,153,169,157,183,172,180,188,160" shape="poly">
        <area title="Limerick" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=18" coords="76,277,74,293,81,303,95,300,107,296,120,304,142,304,135,290,130,284,138,279,139,270,124,265,99,269" shape="poly">
        <area title="Clare" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=5" coords="77,220,91,220,101,234,116,234,127,237,134,238,128,245,121,265,110,266,96,264,94,258,84,271,66,271,39,277,69,248,69,237" shape="poly">
        <area title="mayo" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=21" coords="80,107,89,120,90,132,111,141,113,154,106,169,94,171,87,187,63,175,45,172,43,158,63,156,60,144,42,145,29,132,43,105" shape="poly">
        <area title="Meath" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=22" coords="226,148,215,150,213,161,196,161,212,172,203,194,207,197,218,195,229,194,240,197,248,184,244,177,251,176,255,170,252,163,241,163" shape="poly">
        <area title="Leitrim" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=17" coords="133,88,131,99,134,119,147,128,152,145,159,155,173,142,172,132,161,125,149,118,153,106,144,93" shape="poly">
        <area title="Kildare" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=14" coords="210,198,233,197,240,201,240,212,226,229,226,245,216,246,210,236,207,215" shape="poly">
        <area title="Offaly" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=24" coords="156,202,152,208,155,212,148,217,158,224,153,241,158,245,164,238,171,237,178,225,176,219,190,214,193,219,206,217,207,198,200,199,189,204" shape="poly">
        <area title="Dublin" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=10" coords="256,171,247,179,249,186,240,202,242,211,256,212,264,210,262,177" shape="poly">
        <area title="Carlow" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=3" coords="208,252,208,261,213,263,219,286,236,264,233,252,240,247,229,243,222,249" shape="poly">
        <area title="Tipperary" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=27" coords="146,219,136,234,131,243,125,264,142,269,141,280,131,285,138,291,145,305,148,313,168,309,167,299,189,299,188,272,174,258,169,240,164,240,156,247,151,240,157,226" shape="poly">
        <area title="Cavan" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=4" coords="153,108,151,117,175,134,177,148,186,156,213,158,212,149,220,146,205,128,179,126" shape="poly">
        <area title="Louth" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=20" coords="261,131,249,126,235,129,228,143,233,154,244,162,254,160,249,138" shape="poly">
        <area title="Donegal" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=8" coords="188,1,212,14,185,39,179,64,161,63,158,69,169,76,139,91,140,73,109,71,137,23" shape="poly">
        <area title="Wexford" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=31" coords="253,308,246,295,256,281,260,259,252,256,241,267,238,265,222,285,219,292,213,306,219,314" shape="poly">
        <area title="Wicklow" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=32" coords="243,212,253,215,266,212,269,238,262,257,251,254,240,266,234,253,243,249,237,243,228,238,232,227" shape="poly">
        <area title="Laois" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=16" coords="170,238,172,256,181,253,195,247,206,251,213,247,206,231,205,219,194,221,188,217,179,221,183,225" shape="poly">
        <area title="Cork" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=6" coords="80,306,86,333,76,343,67,354,44,362,39,360,25,369,66,383,112,369,160,339,145,316,141,305,118,306,108,299" shape="poly">
        <area title="Roscommon" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=25" coords="116,142,127,147,141,128,150,145,158,157,151,170,157,196,148,207,136,179,121,164,110,168,115,152" shape="poly">
        <area title="Kilkenny" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=15" coords="177,257,191,250,206,251,206,261,212,266,217,289,212,307,200,307,192,299,190,270" shape="poly">
        <area title="Westmeath" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=30" coords="192,160,209,172,199,196,186,204,178,202,166,201,159,201,156,186,172,183" shape="poly">
        <area title="Sligo" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=26" coords="129,91,131,120,139,127,129,143,104,138,93,130,93,107,121,114,118,99" shape="poly">
        <area title="Kerry" href="/opthalmologists/List-eye-doctors-by-county.asp?CountyID=13" coords="74,277,54,277,45,288,42,299,49,307,18,305,6,318,14,319,44,319,36,326,16,333,9,345,16,352,29,354,42,355,45,360,70,347,84,332,77,304" shape="poly">
      </map>
</form>

