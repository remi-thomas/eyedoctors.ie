﻿
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
dim MemberID
dim Title,FirstName,LastName,Diploma,Biog,Website,Membership_of_pro_bodies,Professional_achievements,Research_interests,Recent_Publications,Photo
MemberID = Left(request.querystring("MemberID"),5)
If Not IsNumeric(MemberID) Or MemberID="" Then
	response.redirect "default.asp?ThisErr=#1 ("& MemberID &")"
	response.end
End If
set rs=sqlconnection.Execute ("SELECT FirstName, LastName, Title, Diploma, Photo, Biog, Website,  Membership_of_pro_bodies,Professional_achievements,Research_interests,Recent_Publications  FROM Members_details where MemberID="& MemberID & " and  Online=1")
If Not rs.eof then
	FirstName	= rs("FirstName")
	LastName	= rs("LastName")
	Title		= replace(replace(rs("Title"),"Pr","Prof"),"Profof","Prof") 
	Diploma		= rs("Diploma")
	Biog		= rs("Biog")
	Website		= rs("Website")
	dim FullName
    FullName = Trim(Title & " " & FirstName & " " & LastName)
    ''
    Membership_of_pro_bodies    = rs("Membership_of_pro_bodies")
    Professional_achievements   = rs("Professional_achievements")
    Research_interests          = rs("Research_interests")
    Recent_Publications         = rs("Recent_Publications")
     '
    Photo                       = rs("Photo")



Else
	response.redirect "http://www.eyedoctors.ie/opthalmologists/?ThisErr=#2 (eof for "& MemberID &")"
	response.end
End if

'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = FullName & " Ophthalmologist member of the ICO | Eye Doctor "& year(Now())
Metadescription   = FullName & " Eye doctor, member of the Irish College of Ophthalmologists. ICO is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & CreateURLFriendlyDoctor(MemberID,FullName,Title)
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = FullName & " | Ophthalmologist in Ireland | Eye Doctors " & year(Now())
SiteSection       = "Directory" ' highlight correct menu tab'

'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Find an Ophthalmologist"
BreadCrumbArr (0,1) = "/opthalmologists/"
BreadCrumbArr (1,0) = FullName
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'


%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
        <div class="col-md-2 maincol">
               <% 
                if len(photo) then
                    response.write "<img src=""/membersphoto/"& photo & """ alt=""" & FullName & """  title=""" & FullName & """ class=""img-responsive"" />"
                end if
                %>
        </div>
          <div class="col-md-6 maincol">
          <h1><%=FullName%></h1>
	  	  <dl class="member-details">
            <dt>Details</dt>
            <%
            If Diploma <> "" Then response.write "<dd>" & Diploma & "</dd>" & vbnewline
            If Biog <> "" Then response.write "<dd>" & Trim(Biog) & "</dd>" & vbnewline
            If Website <> "" Then 
                If InStr(Website,"http") > 0 then
                    response.write "<dd><a href="""& Website &""" target=""Ext-Site"" title=""Go to "& Website &""">" & Website & "</a></dd>" & vbnewline
                Else
                    response.write "<dd>" & Website & "</dd>" & vbnewline
                End if
            End if
            'Membership of professional /national/regional bodies'
            if len(Membership_of_pro_bodies) then
                response.write "<dt>Current Membership of professional, national or regional bodies</dt>" & vbnewline
                 response.write "<dd>" & Membership_of_pro_bodies & "</dd>" & vbnewline
            end if
            'Other professional achievements'
            if len(Professional_achievements) then
                response.write "<dt>Other professional achievements</dt>" & vbnewline
                 response.write "<dd>" & Professional_achievements & "</dd>" & vbnewline
            end if
            'Research interests'
            if len(Research_interests) then
                response.write "<dt>Research interests</dt>" & vbnewline
                 response.write "<dd>" & Research_interests & "</dd>" & vbnewline
            end if

            'specialties
            set rs = sqlconnection.execute("select s.SpecialtyID, s.SpecialtyTitle from specialties s, Members_to_specialties P where s.SpecialtyID = P.SpecialtyID and P.MemberID="& MemberID &" order by p.SpecialtyOrder, s.SpecialtyTitle")	
            if Not rs.eof Then
                response.write "<dt>Sub-Specialty Interests</dt>" & vbnewline
                response.write "<dd>" & vbnewline
                response.write "<ul>" & vbnewline
                Do While Not rs.eof
                    response.write "<li>" & Displaytext(rs("SpecialtyTitle")) & "</li>"
                rs.movenext
                Loop
                response.write "</ul>" & vbnewline
                response.write "</dd>" & vbnewline
    
            End If 'rs specialties eof
            %>
            <%
            'Practice 1 for Public ;2 for Private ; 0 is unknown
            Set rs = sqlconnection.execute ("SELECT PracticeID,PracticeAddress,PracticeTelephone, PracticeFax ,PracticeWeb ,PracticeEmail, Opening, GeoLat, GeoLong, PracticeNotes ,PrivatePublic  FROM Members_practice where MemberID=" & MemberID & " and online=1 and PracticeAddress<> '' order by DisplaYOrder, PrivatePublic")
            dim CurrentPractice, CntPractice, PublicPractice, PrivatePractice
            If Not rs.eof Then
                CurrentPractice		= ""
                CntPractice			= 0
                PublicPractice		= 0
                PrivatePractice		= 0

                Do While Not rs.eof
            

                    response.write "<dt>"
                        Select Case rs("PrivatePublic")
                            Case 1
                                PublicPractice = PublicPractice + 1
                                response.write "Public Clinic #" & PublicPractice
                            Case 2
                                PrivatePractice = PrivatePractice + 1
                                response.write "Private Rooms #" & PrivatePractice
                            Case Else
                                response.write "Practice "
                                If CurrentPractice = rs("PrivatePublic") Then response.write "#" & CntPractice + 1
                        End Select
                    response.write"</dt>" & vbnewline		
                    response.write "<dd>" & vbnewline
                    response.write rs("PracticeAddress")

                    If rs("GeoLat") <> "" And rs("GeoLong") <> "" then
                        response.write "&nbsp;(<a href=""/opthalmologists/map.asp?PracticeID="& rs("PracticeID") &""">location map</a>)"
                    End if
                    If rs("PracticeTelephone") <> "" Then response.write "<br>Tel: <a href=""tel:"& rs("PracticeTelephone") &""" class=""phonenumber"">" & rs("PracticeTelephone") & "</a>"
                    If rs("PracticeFax") <> "" Then response.write "<br>Fax: " & rs("PracticeFax")
                    If rs("PracticeWeb") <> "" Then 
                        If InStr(rs("PracticeWeb"),"http") > 0 then
                            response.write "<br>Web: <a href="""& rs("PracticeWeb") &""" target=""Ext-Site"" title=""Go to "& rs("PracticeWeb") &""">" & rs("PracticeWeb") & "</a>" & vbnewline
                        Else
                            response.write "<br>" & rs("PracticeWeb")  & vbnewline
                        End if
                    End If ' PracticeWeb
                    
            
                    If rs("PracticeEmail") <> "" Then response.write "<br>Email: <a href=""mailto:"& rs("PracticeEmail") & """>" & DisplayText(rs("PracticeEmail")) & "</a>"
                    If rs("PracticeNotes") <> "" Then response.write "<br>" & DisplayText(rs("PracticeNotes"))

                    If rs("Opening") <> "" Then response.write "<br>Days: <em>"& DisplayText(rs("Opening")) & "</em>"
                

                    CurrentPractice = rs("PrivatePublic")
                    CntPractice		= CntPractice + 1

                rs.movenext
                Loop
                response.write "</dd>" & vbnewline
            End If 'rspractice.eof
            %>
	  	  </dl>

		  <%
		  Set rs = Nothing
          dim sp
          set sp = sqlconnection.execute("SELECT SpecialtyID,SpecialtyTitle from specialties order by SpecialtyTitle")
		  %>

        </div> <!-- {end} col md-8 -->
        <!-- Side Column -->
        <div class="col-md-4 sidecol">



          <form name="searchspecialities" action="/opthalmologists/eye-doctors-database.asp" method="get" onsubmit="submit(this);" class="form-inline member-search" role="form">
                  <h2>List Doctors by Sub Specialty Interest</h2>
            <div class="form-group">
                    <select name="SpecialtyID" onchange="submit()" class="form-control">
                        <option value="">Any Sub-Specialty Interests</option>
                        <% 
                        do until sp.eof
                            response.write vbtab & "<option value="""& sp("SpecialtyID") &""">"& sp("SpecialtyTitle") &"</option>" & vbnewline
                        sp.movenext
                        Loop
                        %>

                    </select>
            </div>
            
            <input type="hidden" name="MemberName" value="">
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="">
            <input type="submit" value="Search" class="btn btn-default">
          </form>
          <%
            Set sp = Nothing
          %>
          <form name="searchmember" action="/opthalmologists/eye-doctors-database.asp" method="get" onsubmit="return validate_form(this);" class="form-inline member-search" role="form">


         


            <h2>Search by Name</h2>
            <div class="form-group">
              <label class="sr-only" for="MemberName">Last Name</label>
              <input type="text" name="MemberName" class="form-control" placeholder="Last Name">
            </div>
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="0">
            <input type="hidden" name="SpecialtyID" value="">
            <input type="submit" value="Search" class="btn btn-default">
          </form>
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->
 
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>