<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%

dim PracticeID
PracticeID = Left(ValidateText(request.querystring("PracticeID")),4)
if not IsNumeric(PracticeID) or PracticeID = "" then
	response.Redirect "default.asp?thisErr=PracticeID-"& PracticeID &"- is numeric=" & IsNumeric(PracticeID)
end if
set rs = sqlconnection.execute("SELECT p.PracticeAddress, p.PracticeTelephone, p.PracticeFax , p.PracticeWeb , p.PracticeEmail, p.GeoLat, p.GeoLong, p.PracticeNotes , p.PrivatePublic, m.FirstName, m.LastName, m.Title, m.MemberID FROM Members_practice p left join Members_details m on m.MemberID=p.MemberID where p.PracticeID=" & PracticeID )
if rs.eof then
	response.Redirect "default.asp?thisErr=Query for PracticeID-"& PracticeID &"- returns nothing"
end if

dim VenueAddress, VenueTelephone, Venuefax, VenueEmail, VenueUrl, VenueYCoord, VenueXCoord
dim MemberTitle,MemberFirstName,MemberLastName, MemberID

VenueAddress    = DisplayText(rs("PracticeAddress"))
VenueTelephone	= DisplayText(rs("PracticeTelephone"))
Venuefax        = DisplayText(rs("PracticeFax"))
VenueEmail      = DisplayText(rs("PracticeEmail"))
VenueUrl        = DisplayText(rs("PracticeWeb"))
VenueYCoord			= DisplayText(rs("GeoLat"))
VenueXCoord			= DisplayText(rs("GeoLong"))
'----------------------------------------------------------------------'
MemberTitle     = DisplayText(rs("Title"))
MemberFirstName = DisplayText(rs("FirstName"))
MemberLastName  = DisplayText(rs("LastName"))
MemberID        = DisplayText(rs("MemberID"))

Dim MemberName  :  MemberName = replace(replace(MemberTitle,"Pr","Prof"),"Profof","Prof") & " " & MemberFirstName & " " & MemberLastName

'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = VenueAddress & " | " &  MemberName &" | Eye Doctor : "& year(Now())
Metadescription   = VenueAddress & ", "& MemberName &" Eye doctor, member of the Irish College of Ophthalmologists. ICO is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/Ophthalmologists/map.asp?PracticeID=" & PracticeID
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = VenueAddress & " | Ophthalmologist in Ireland | Eye Doctors "
SiteSection       = "Directory" ' highlight correct menu tab'

'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "Find an Ophthalmologist"
BreadCrumbArr (0,1) = "/opthalmologists/"
BreadCrumbArr (1,0) = MemberName
BreadCrumbArr (1,1) = "/opthalmologists/masp.asp?PracticeID=" & PracticeID
BreadCrumbArr (2,0) = VenueAddress
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>

<!-- generate key http://code.google.com/apis/maps/signup.html -->
 <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAAFuUw6vh4rjZRb6mgLYbSQxTWoAyVU0dvmZOO5xGOumuLaiaZVxS3DRYok_rHErrr3co-Cosapvts6g" type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[
     function load() {
      if (GBrowserIsCompatible()) {

        // Creates a marker at the given point with the given number label
        function createMarker(point, number) {
          var marker = new GMarker(point);
          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml(desc);
          });
          return marker;
        }

        var myTextField = document.getElementById('hidY');
        ewC = myTextField.value

        var myTextField1 = document.getElementById('hidX');
        nsC = myTextField1.value

        var myTextField2 = document.getElementById('hidDesc');
        desc = '<span class=gmap>' + myTextField2.value + '</span>'

        if (ewC != "" && nsC != "") {
            var map = new GMap2(document.getElementById("map"));
            map.addControl(new GSmallMapControl());
            map.addControl(new GMapTypeControl());
            map.setCenter(new GLatLng(ewC, nsC), 15);

            // Add marker to the map
            var bounds = map.getBounds();
            var point = new GLatLng(ewC, nsC);

            map.addOverlay(createMarker(point, 1));

            }

        else {
            document.getElementById('map').style.display = "none";
        }
      }
    }
    //]]>
    </script>

</head>
<body onload="load()" onunload="GUnload()">
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
          <div class="col-md-8 maincol">
          <h1><%=VenueAddress%></h1>

  
<div id="map" style="width: 600px; height: 500px;"></div>
	<input type="hidden" id="hidX" name="hidX" value="<%=VenueXCoord %>" />
	<input type="hidden" id="hidY" name="hidY" value="<%=VenueYCoord %>" />
		  <input type="hidden" id="hidDesc" name="hidDesc" value="<%=VenueAddress %><br /><%=VenueTelephone %><br /><a href='mailto:<%=VenueEmail %>'><%=VenueEmail %></a><br /><a href='<%=VenueUrl %>'><%=VenueUrl %>" />


<ul>
<li><%=VenueAddress%></li>
<li><a href="<%=CreateURLFriendlyDoctor(MemberID,MemberName,NULL)%>" title="<%=MemberName%>"><%=MemberName%></a></li>
<% 
If VenueTelephone <> "" Then 
  response.write "<li>T: <a href=""tel:"& VenueTelephone &""" class=""phonenumber"">" & VenueTelephone & "</a></li>" & vbnewline
end if
If Venuefax <> "" Then 
  response.write "<li>F: " & Venuefax & "</li>" & vbnewline
end if
If VenueEmail <> "" Then 
  response.write "<li>E: <a href=""mailto:""" & VenueEmail & """>" & VenueEmail & "</a></li>" & vbnewline
end if
If VenueUrl <> "" Then 
  response.write "<li>W: <a href=""" & VenueUrl & """ target=""extsite"">" & VenueUrl & "</a></li>" & vbnewline
end if
%>
</ul>



        </div> <!-- {end} col md-8 -->
        <!-- Side Column -->
        <div class="col-md-4 sidecol">
          <form name="searchmember" action="eye-doctors-database.asp" method="get" onsubmit="return validate_form(this);" class="form-inline member-search" role="form">
            <h2>Search by Name</h2>
            <div class="form-group">
              <label class="sr-only" for="MemberName">Last Name</label>
              <input type="text" name="MemberName" class="form-control" placeholder="Last Name">
            </div>
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="0">
            <input type="submit" value="Search" class="btn btn-default">
          </form>
          <!-- Side: Menu -->        
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->
 
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>