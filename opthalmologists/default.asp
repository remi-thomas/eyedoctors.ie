
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Directory of Irish Ophthalmologists | Eye Doctors: "& year(Now())
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/Ophthalmologists/default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Ophthalmologists Directory | Eye Doctors "
SiteSection       = "Directory" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(0,1)
BreadCrumbArr (0,0) = "Find an Ophthalmologist"
BreadCrumbArr (0,1) = "/opthalmologists/"

BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'

dim MemberFirstName, MemberName, restr

MemberFirstName   = left(trim(Request.querystring("MemberFirstName")),40)
MemberName        = left(trim(Request.querystring("MemberName")),50)
restr             = request.querystring("restr")

'only one letter - redirect to list
If Len(MemberName) = 1 And Len(MemberFirstName)=0 Then
    response.redirect "eye-doctors-list.asp?lt=" & MemberName
End If
'only one letter - redirect to list
If Len(MemberFirstName) = 1 And Len(MemberName)=0 Then
    response.redirect "eye-doctors-list.asp?lt=" & MemberFirstName
End If
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
        <div class="col-md-8 maincol">
          <h1>Eye Doctors Directory</h1>
          <p>In this directory you will find contact details for ophthalmologists practicing in the Republic of Ireland who are members of the <abbr title="Irish College of Ophthalmologists">ICO</abbr>. The directory is searchable by county, by first and last name or by first initial of surname.</p>

          <form name="searchmember" action="eye-doctors-database.asp" method="get" onsubmit="return validate_form(this);" class="form-inline member-search" role="form">
            <h2>Search by name</h2>
            <div class="form-group">
              <label class="sr-only" for="MemberFirstName">First Name</label>
              <input type="text" name="MemberFirstName" size="23" value="<%=MemberFirstName%>" onfocus="rstfirstname()" onblur="stfirstname()" class="form-control" placeholder="First Name"> <!--<% If MemberFirstName = "" Then response.write "First Name (optional)" Else response.write Trim(MemberFirstName)%>-->
            </div>
            <div class="form-group">
              <label class="sr-only" for="MemberName">Last Name</label>
              <input type="text" name="MemberName" size="26" value="<%=MemberName%>" onfocus="rstname()" onblur="stname()" class="form-control" placeholder="Last Name">
            </div>
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="<%=restr%>">
            <input type="submit" value="Search" class="btn btn-default">
          </form>

	      <div class="member-atoz">
            <h2>A to Z</h2>
	        <div>
  		 	  <%
              dim fl
              Set rs = SqlConnection.Execute("usp_sel_AZ_Members")
              fl = rs(0) 'give a value to firstletter
              Do Until rs.eof
                response.write "<a href=""eye-doctors-list.asp?lt=" & rs(0) & """>" & rs(0) & "</a> <span>|</span> "
              rs.movenext
              Loop
              response.write "<a href=""eye-doctors-list.asp?lt=9"" class=""btn""><em>List all</em></a>"
              %>
            </div>
          </div>




      <%

          dim sp
          set sp = sqlconnection.execute("SELECT SpecialtyID,SpecialtyTitle from specialties order by SpecialtyTitle")
      %>

    



          <form name="searchspecialities" action="/opthalmologists/eye-doctors-database.asp" method="get" onsubmit="submit(this);" class="form-inline member-search" role="form">
                  <h2>List Doctors by Sub Specialty Interest</h2>
            <div class="form-group">
                    <select name="SpecialtyID" onchange="submit()" class="form-control">
                        <option value="">Any Sub-Specialty Interests</option>
                        <% 
                        do until sp.eof
                            response.write vbtab & "<option value="""& sp("SpecialtyID") &""">"& sp("SpecialtyTitle") &"</option>" & vbnewline
                        sp.movenext
                        Loop
                        %>

                    </select>
            </div>
            
            <input type="hidden" name="MemberName" value="">
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="">
            <input type="submit" value="Search" class="btn btn-default">
          </form>
          <%
            Set sp = Nothing
          %>




		</div> <!-- {end} col md-8 -->
        
        <!-- Side Column -->
        <div class="col-md-4 sidecol">
          <!-- Side: Menu -->        
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->
    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>