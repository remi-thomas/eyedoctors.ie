﻿<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
dim CountyID, CountyName

CountyID = Left(request.Form("CountyID"),2)
If CountyID = "" or Not IsNumeric(CountyID) Then
	CountyID = Left(request.querystring("CountyID"),2)
	If CountyID = "" or Not IsNumeric(CountyID) Then
		CountyID = "0"
	End If
End If

If CountyID > 0 Then
	dim rsCountyName
	Set rsCountyName = sqlconnection.execute("select county from county where CountyID=" & CountyID)
	If Not rsCountyName.eof Then
		CountyName	= " County " & rsCountyName(0) 
	End If
	Set rsCountyName = Nothing
Else
	CountyID 	= ""
	CountyName = " Ireland"
End if

'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "A to Z of Irish Ophthalmologists | Eye Doctors: "& year(Now())
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/Ophthalmologists/eye-doctors-list.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = CountyName & " | Ophthalmologists | Eye Doctors "
SiteSection       = "Directory" ' highlight correct menu tab'



'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "Find an Ophthalmologist"
BreadCrumbArr (0,1) = "/opthalmologists/"
BreadCrumbArr (1,0) = "Search by Location"
BreadCrumbArr (1,1) = "List-eye-doctors-by-county.asp"
BreadCrumbArr (2,0) = CountyName
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<script type="text/javascript" src="/ssi/js/members_search_lib.js" ></script>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout" style="min-height:700px;">
        <!-- Main Column -->
        <div class="col-md-8 maincol">
          <h1>Eye Doctors Directory</h1>

		  <%
          dim sql
          dim MemberName
          if not isNumeric(CountyID) or CountyID ="" then
              sql = "usp_sel_MembersAlphabetical"
          Else
              sql = "exec usp_sel_MembersbyCounty " & CountyID
          end if
        
          dim RecordCount : RecordCount = 0
          set rs = Server.CreateObject("ADODB.Recordset")
          rs.CursorLocation = 3
          rs.Open  sql , sqlConnection

          If Not rs.eof then
            RecordCount = rs.RecordCount
            rs.movefirst

          %>
          <h2>Ophthalmologists in <%=CountyName%> </h2>
          <div id="details">
           
            <ul <% if RecordCount >= 10 then response.write " class=""list-cols"" " %>>
            <%
            Do While Not rs.eof
                MemberName = replace(replace(rs("Title"),"Pr","Prof"),"Profof","Prof") & " " & rs("FirstName") & " " & rs("LastName")
            %>
              <li><a href="<%=CreateURLFriendlyDoctor(rs("MemberID"),rs("FirstName") & " " & rs("LastName"), rs("Title"))%>" title="See <%=MemberName%>'s record"><%=MemberName%></a></li>
            <%
            rs.movenext
            Loop
            %>
            </ul>
          </div>
          <%
            Else
                response.write "<p>Sorry, we have no member yet in " & CountyName &".</p>"
            End If
          %>
	
        </div> <!-- {end} col md-8 -->
        <!-- Side Column -->
        <div class="col-md-4 sidecol">
          <form name="searchmember" action="eye-doctors-database.asp" method="get" onsubmit="return validate_form(this);" class="form-inline member-search" role="form">
            <h2>Search by Name</h2>
            <div class="form-group">
              <label class="sr-only" for="MemberName">Last Name</label>
              <input type="text" name="MemberName" class="form-control" placeholder="Last Name">
            </div>
            <input type="hidden" name="searchusers" value="true">
            <input type="hidden" name="restr" value="0">
            <input type="submit" value="Search" class="btn btn-default">
          </form>
          <!--#include file="ophtalmologist-map-side-bar.asp" -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>