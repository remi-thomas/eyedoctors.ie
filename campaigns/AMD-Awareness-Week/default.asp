
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "AMD Awareness Week | Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Looking After Your Eyes"
Metadescription   = "AMD Awareness Week | The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "campaigns/AMD-Awareness-Week/"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "AMD Awareness Week | Eye Doctors "
SiteSection       = "Campaign" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Campaigns"
BreadCrumbArr (0,1) = "/campaigns/default.asp"
BreadCrumbArr (1,0) = "Age-related Macular Degeneration (AMD) Awareness Week"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">
          <h1>AMD Awareness week - September <%=year(now())%></h1>
	
      			<p>
      			<!--#include file ="amd_week.htm"-->
      			</p>
			       <a href="more-about-amd.asp" class="btn btn-arrow">Read more about AMD (Age-related Macular Degeneration)</a>
          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>