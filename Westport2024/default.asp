
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|
' photos from https://www.irelandscontentpool.com/'

MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Annual Conference"
Metadescription   = "Annual Conference, Irish College of Ophthalmologists, May " & year(Now())
ArticleURL        = DomainName & "westport2024"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Westport 2024 | Annual Conference | Irish College of Ophthalmologists Annual Conference"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "westport2024"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Westport 2024"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'

dim ShowProgramme : ShowProgramme = false
if Now() > Cdate("15 May 2024 07:59") then
  ShowProgramme = true
end if
if request("admin") = 1 then
  ShowProgramme = true
end if

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
/*.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
*/
.ctitle {color: #004b8d;font-weight:normal;}
.ctitle em {font-weight: 500;color:#ccc;;font-size: 2rem;}

.posters ul {list-style-type: none;}
.posters ul  li:nth-child(odd) { background: #eee; }

.modal-body h5 {font-weight: 500;color:#999;;font-size: 1.4rem;}


.svg-icon {display: inline-flex; align-self: center;}
.svg-icon svg {height:1.2em;  width:1.2em;}
.svg-icon.svg-baseline svg {top: .125em;position: relative;}

.portrait-icon {max-height: 15px;float: left;padding-right: 0.3rem;}
</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('westporthouse.jpg')" >
        <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2024</h1> 
        <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ICOWestport2024" title="#ICOWestport2024" target="twitter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ico2024" alt="#ico2024" style="float:left;">&nbsp;#ICOWestport2024</a></p>
      </div>

<div class="" style="background-color: #1b4c8b; height: 100%; padding: 1rem 2rem; border-top: 3px solid #fff;">
    <a class="h3" href="https://ico.wildapricot.org/event-5559447" target="book" style="color: #fff; font-weight: bold; display: flex; margin: 0;"><i class="glyphicon glyphicon-circle-arrow-right" style="margin-right: 10px; text-decoration: none;"></i> Register Now</a>
</div>


<div class="panel-group" id="accordion">
     <% if ShowProgramme then%>

       <div class="panel panel-default svg-icon svg-baseline">
          <a href="/medium/files/ICO_Conference_book_2024.pdf" title="Download the ICO conference programme as a PDF document" download style="font-size:15px;font-weight: bold;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" style="fill:#8a0036;"><g><path d="M30.56 8.47a8 8 0 0 0-7-7 64.29 64.29 0 0 0-15.06 0 8 8 0 0 0-7 7 64.29 64.29 0 0 0 0 15.06 8 8 0 0 0 7 7 64.29 64.29 0 0 0 15.06 0 8 8 0 0 0 7-7 64.29 64.29 0 0 0 0-15.06zm-2 14.83a6 6 0 0 1-5.28 5.28 63.65 63.65 0 0 1-14.6 0 6 6 0 0 1-5.26-5.28 63.65 63.65 0 0 1 0-14.6A6 6 0 0 1 8.7 3.42a63.65 63.65 0 0 1 14.6 0 6 6 0 0 1 5.28 5.28 63.65 63.65 0 0 1 0 14.6z"/><path d="M24 21a1 1 0 0 0-1 1v1a1 1 0 0 1-1 1H10a1 1 0 0 1-1-1v-1a1 1 0 0 0-2 0v1a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-1a1 1 0 0 0-1-1z"/><path d="M15.23 21.64a1 1 0 0 0 1.54 0l5-6A1 1 0 0 0 21 14h-2V8a2 2 0 0 0-2-2h-2a2 2 0 0 0-2 2v6h-2a1 1 0 0 0-.77 1.64zM14 16a1 1 0 0 0 1-1V8h2v7a1 1 0 0 0 1 1h.86L16 19.44 13.14 16z"/></g></svg> Download the Conference programme (PDF)</a>
      </div>
      <% end if%>

 <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapseIntro"><span style="">Introduction&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
    <div id="collapseIntro" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="westport1.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
          <!--#include file="introduction.html" -->

      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <h2 class="panel-title" ><a data-toggle="collapse" data-parent="#accordion" href="#collapse15">Wednesday 15th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse15" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="westport11.jpg" alt="" width="960" height="60"  alt="Westport" title="Westport"/>
          <!--#include file="15May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse16">Thursday 16th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="westport13.jpg" alt="" width="960" height="60"  alt="Westport" title="Westport"/>
          <!--#include file="16May.html" -->

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse17">Friday 17th May&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse17" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="westport5.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
          <!--#include file="17May.html" -->

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <h2 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse19">Posters&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h2>
    <div id="collapse19" class="panel-collapse collapse">
      <div class="panel-body" style="max-width:950px;overflow:hidden">
          <img src="westport16.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>

          <!--#include file="posters.html" -->

      </div>
    </div>
  </div>




</div> <!-- accordion -->



     <div style="max-width:950px;min-height:60px;overflow:hidden;padding:5px 10px 0px; background-image: url('westport4.jpg');color:#fff; text-align: right;" >&copy;Irish College of Ophthalmology 2024&nbsp; &nbsp; 
      <br/>
      <span style="font-size:0.7em;">All photos &copy;<a href="http://www.failteireland.ie/"  style="color:#fff;" target="ext">F&aacute;ilte Ireland</a> 2024&nbsp; &nbsp; 

     </div>



    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">

          <% if ShowProgramme then%>
            <dl>
              <dt style="width:100%;background-color:rgb(184, 9, 77);">
                <div  class="svg-icon svg-baseline">
                <a href="/medium/files/ICO_Conference_book_2024.pdf" title="Download the ICO conference programme as a PDF document" download><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" style="fill:#fff;">&nbsp;<g><path d="M30.56 8.47a8 8 0 0 0-7-7 64.29 64.29 0 0 0-15.06 0 8 8 0 0 0-7 7 64.29 64.29 0 0 0 0 15.06 8 8 0 0 0 7 7 64.29 64.29 0 0 0 15.06 0 8 8 0 0 0 7-7 64.29 64.29 0 0 0 0-15.06zm-2 14.83a6 6 0 0 1-5.28 5.28 63.65 63.65 0 0 1-14.6 0 6 6 0 0 1-5.26-5.28 63.65 63.65 0 0 1 0-14.6A6 6 0 0 1 8.7 3.42a63.65 63.65 0 0 1 14.6 0 6 6 0 0 1 5.28 5.28 63.65 63.65 0 0 1 0 14.6z"/><path d="M24 21a1 1 0 0 0-1 1v1a1 1 0 0 1-1 1H10a1 1 0 0 1-1-1v-1a1 1 0 0 0-2 0v1a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-1a1 1 0 0 0-1-1z"/><path d="M15.23 21.64a1 1 0 0 0 1.54 0l5-6A1 1 0 0 0 21 14h-2V8a2 2 0 0 0-2-2h-2a2 2 0 0 0-2 2v6h-2a1 1 0 0 0-.77 1.64zM14 16a1 1 0 0 0 1-1V8h2v7a1 1 0 0 0 1 1h.86L16 19.44 13.14 16z"/></g></svg>&nbsp; Conference programme</a></div></dt>
            </dl>
            <% end if%>

            <!--#include virtual="/news-events/news-events-Menu.asp" -->

<div class="" style="background-color: #1b4c8b; height: 100%; padding: 1rem 2rem; border-top: 3px solid #fff;">
    <a class="h3" href="https://ico.wildapricot.org/event-5559447" target="book" style="color: #fff; font-weight: bold; display: flex; margin: 0;"><i class="glyphicon glyphicon-circle-arrow-right" style="margin-right: 10px; text-decoration: none;"></i> Register Now</a>
</div>
  

        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->




<div class="modal fade" id="biography" tabindex="-1" role="dialog" aria-labelledby="biographyLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="biographyModalLongTitle">r</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>
$('a[data-toggle="modal"]').click(function(){    
    
    var vtitle = $(this).attr('title');
    //console.log(vtitle);
    var vcontent = "";
    var dataURL = 'biog/' + $(this).attr('data-href') + '.html?v=' + Date.now() ;
    console.log(dataURL);
    sendInfo(dataURL);

    $('#biography .modal-title').html(vtitle);

    //$('#biography .hide').show();
   //$('#biography').modal();
  
    /*$('#biography').on('hidden', function(){
    console.log("hidden");
      $('#biography').remove();
  });
  */
});


var request;

function sendInfo(url) {

  if (window.XMLHttpRequest) {
  request = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) {
  request = new ActiveXObject("Microsoft.XMLHTTP");
  }

  try {
  request.onreadystatechange = getInfo;
  request.open("GET", url, true);
  request.send();
  }
  catch (e) {
  console.log("Unable to connect to server");
  }
}

function getInfo() {
  if (request.readyState == 4) 
  {
    var vcontent = request.responseText;
  }
  else
  {
    var vcontent = "error";
  }
  //console.log(vcontent);
  $('#biography .modal-body').html(vcontent);
}


</script>
</body>
</html>