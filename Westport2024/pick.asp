<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|
' photos from https://www.irelandscontentpool.com/'

MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Annual Conference"
Metadescription   = "Annual Conference, Irish College of Ophthalmologists, May " & year(Now())
ArticleURL        = DomainName & "westport2024"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Westport 2024 | Annual Conference | Irish College of Ophthalmologists Annual Conference"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "westport2024"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Annual Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Westport 2024"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
/*.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
*/
.ctitle {color: #004b8d;font-weight:normal;}
.ctitle em {font-weight: 500;color:#ccc;;font-size: 2rem;}

.posters ul {list-style-type: none;}
.posters ul  li:nth-child(odd) { background: #eee; }

.modal-body h5 {font-weight: 500;color:#999;;font-size: 1.4rem;}


.svg-icon {display: inline-flex; align-self: center;}
.svg-icon svg {height:1.2em;  width:1.2em;}
.svg-icon.svg-baseline svg {top: .125em;position: relative;}

</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


          <div style="max-width:950px;min-height:160px;height:160px;overflow:hidden;padding:0; background-image: url('westporthouse.jpg')" >
            <h1 style="color:#fff;padding-top:10px;padding-left:10px;font-size:200%;">ICO Annual Conference 2024</h1> 
            <p  style="color:#fff;padding-top:0px;padding-bottom:0px;padding-left:10px;font-size:100%;margin:0;"><a href="https://twitter.com/search?q=ico2024" title="#ico2024" target="twiter" style="color:#fff;text-decoration: none; font-weight:bold; font-size:16px"><img src="/images/twitter.png" width="20" title="#ico2024" alt="#ico2024" style="float:left;">&nbsp;#ICO2024</a></p>
          </div>


          <div class="panel-group" id="accordion">

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">1. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport1.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">2. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport2.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">3. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport3.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">4. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport4.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">5. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport5.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            


           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">10. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport10.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>

            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">11. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport11.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">12. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport12.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">13. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport13.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">14. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport14.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">15. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport15.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">16. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport16.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


            

           <div class="panel panel-default">
              <h2 class="panel-title" ><a href="#"><span style="">17. Lorem Ipsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a></h2>
              <div id="collapseIntro" class="panel-collapse">
                <div class="panel-body" style="max-width:950px;overflow:hidden">
                    <img src="westport17.jpg" alt="" width="960" height="60" alt="Westport" title="Westport"/>
                </div>
              </div>
            </div>


          </div> <!-- accordion -->



 


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->



</body>
</html>