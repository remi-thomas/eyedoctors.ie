
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" FAQ"
Metadescription   = "Ophthalmologists Training - Frequently Asked Questions "
ArticleURL        = DomainName & "Trainees/FAQ.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Ophthalmologists Training - Frequently Asked Question  | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Health-Policy" ' highlight correct menu tab'
SiteSubSection    = "FAQ"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Trainees"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Careers Information"
BreadCrumbArr (1,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->

  
  <style>
    .faq {
      counter-reset: my-badass-counter;
    }
    .faq dt {
      position: relative;
      font: 16px;
      font-weight: bold;
      padding: 4px 0 10px 0;
      color: #004b8d;
    }
    .faq dt:before {
      content: counter(my-badass-counter);
      counter-increment: my-badass-counter;
      position: absolute;
      left: 0;
      top: 0;
      font: bold 50px/1 Sans-Serif;
      color: #004b8d;
    }
    .faq dd {
      margin: 0 0 50px 0;
      text-align: justify;
    } 
    .faq dt, .faq dd {
      padding-left: 60px;
    }

    

  </style>



</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <!--#include file="careers-information.html" -->
   

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="Trainee-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
            
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>