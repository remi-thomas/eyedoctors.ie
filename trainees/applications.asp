
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Health Policy"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/trainees/Applications.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Applications for Training Programmes | Trainees | Eye Doctors "
SiteSection       = "trainee" ' highlight correct menu tab'
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(1,1)
BreadCrumbArr (0,0) = "Trainees"
BreadCrumbArr (0,1) = "/Trainees/"
BreadCrumbArr (1,0) = "Applications for Training Programmes"
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
  dt {color:#004b8d; font-size:24px; margin:0 0 23px; }
</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class=" layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <!--#include file="application.html" -->
            <% if now()<cdate("6 November 2022 8:00am") then %>
              <h2>Upcoming ICO Career Talks</h2>
              <p>Please come along to meet with us at these face to face talks if within your network / school of medicine</p>
            <% end if %>

            <% if now()<cdate("21 October 2022 8:00am") then %>
              <h3 style="color:#b8094d;">Thursday, 20th October 2022</h3>
              <h4>Mid-Western Intern Network (University of Limerick) </h4>
              <p><b>Intern Weekly Teaching Session</b>
                <br/>Time:   1pm -2pm
                <br/>Location: CERC building (Clinical Education and Research Centre), University Hospital Limerick
              </p>

              <h4>West / North West (NUIG) Intern Network </h4>
              <p><b>Galway University Hospitals Medical Careers Event for Interns & NCHDs </b>
              <br/>Time:    5pm-6pm
              <br/>Location: Nurses Home Building, University Hospital Galway
            <% end if %>

            <% if now()<cdate("6 November 2022 8:00am") then %>
              <h3 style="color:#b8094d;">Saturday, 5th November 2022</h3>
              <h4>Dublin / Mid Leinster (UCD) Intern Network </h4>
              <p><b>Careers event for interns & NCHDs</b>
                <br/>Time:   11am 
                <br/>Location: Catherine McCauley Education & Research Centre, Mater University Hospital

              </p>
            <% end if %>
          </div>
          <!-- Side Column -->
          <div class="col-md-3 sidecol">
              <!--#include file="Trainee-Menu.asp" -->
              <!-- Side: Content Box -->
              <!--#include virtual="/ssi/incl/adverts.asp" -->
              <!--{end}  box -->
              
          </div> <!--{end}  sidecol -->



   
        </div> <!--{end} maincol -->
      
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>