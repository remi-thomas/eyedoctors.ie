<!-- Side: Menu -->
          <dl>
            <dt><a href="/Trainees/">Trainees</a></dt>
            <dd>
              <ul>
                  <li class="subsubmenu">
                      <a href="#"><b>Medical Ophthalmology</b></a>
                    <ul>
                      <li <%if SiteSubSection="ctmo" then response.write " class=""active"""%>>
                      <a href="/trainees/Basic-Training-in-Medical-Ophthalmology.asp">
                      Basic Training in Medical Ophthalmology</a></li> 

                      <li <%if SiteSubSection="stmo" then response.write " class=""active"""%>>
                      <a href="/trainees/Specialist-Training-in-Medical-Ophthalmology.asp">
                      
                     Higher Specialist Training in Medical Ophthalmology</a></li>
                    </ul>
                  </li>
                  <li class="subsubmenu">
                      <a href="#"><b>Surgical Ophthalmology</b></a>
                    <uL>
                      <li <%if SiteSubSection="ctso" then response.write " class=""active"""%>><a href="/trainees/Basic-Training-in-Surgical-Ophthalmology.asp">Basic Training in Surgical Ophthalmology</a></li>
                      <li <%if SiteSubSection="stso" then response.write " class=""active"""%>><a href="/trainees/Specialist-Training-in-Surgical-Ophthalmology.asp">Higher Specialist Training in Surgical Ophthalmology</a></li>
                    </uL>
                  </li>

                  <li <%if SiteSubSection="imgt" then response.write " class=""active"""%>><a href="/trainees/International-Medical-Graduates-Training-programme.asp">International Medical Graduates Training Programme</a></li>
                  <li <%if SiteSubSection="elr" then response.write " class=""active"""%>><a href="/trainees/english-language-requirement/">English language requirement</a></li>

              </ul>
            </dd>
          </dl>