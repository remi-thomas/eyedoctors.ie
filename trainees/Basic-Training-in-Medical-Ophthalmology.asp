
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &"  Basic Training in Medical Ophthalmology "
Metadescription   = "Basic Training in Medical Ophthalmology "
ArticleURL        = DomainName & "Trainees/Basic-Training-in-Medical-Ophthalmology.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = " Basic Training in Medical Ophthalmology  | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "traineed" ' highlight correct menu tab'
SiteSubSection    = "ctmo"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "Trainees"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Medical Ophthalmology"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Basic Training in Medical Ophthalmology"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <%
            if now() < Cdate("3 February 2023 00:00") then
            %>
            <p style="background:#800636;color: #fff; padding:15px;font-size:15px;"><b>The closing date for applications for Basic Training in Medical Ophthalmology, commencing July 2023, has been extended to 4pm Thursday, 2nd February 2023</b></p>
            <%
            end if
            %>
            <!--#include file="Core-Training-in-Medical-Ophthalmology.html" -->
   
            <!--#include file="ndtp-credits.html" -->
          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="Trainee-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
            
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>