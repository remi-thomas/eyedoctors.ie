
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" | Higher Specialist Training in Surgical Ophthalmology"
Metadescription   = "Higher Specialist Training in Medical Ophthalmology"
ArticleURL        = DomainName & "trainees/Specialist-Training-in-Surgical-Ophthalmology.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Higher Specialist Training in Surgical Ophthalmology | Eye Doctors  &copy;" & Year(Now())
SiteSection       = "Health-Policy" ' highlight correct menu tab'
SiteSubSection    = "stmo"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "Trainees"
BreadCrumbArr (0,1) = "default.asp"
BreadCrumbArr (1,0) = "Surgical Ophthalmology"
BreadCrumbArr (1,1) = "Trainee-Menu.asp"
BreadCrumbArr (2,0) = "Higher Specialist Training in Surgical Ophthalmology"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">

            <!--#include file="Specialist-Training-in-Surgical-Ophthalmology.html" -->
              <!--#include file="ndtp-credits.html" -->

          </div>
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="Trainee-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->
        </div> <!--{end}  sidecol -->

    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
</body>
</html>