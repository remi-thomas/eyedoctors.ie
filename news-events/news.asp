﻿
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'______________________________________________________________________
'| Request variables                                                   '
'______________________________________________________________________
Dim NewsID : NewsID = request.querystring("NewsID")
if NewsID="" or Not IsNumeric(NewsID) then
  response.redirect "/news-events/default.asp?ThisErr=Err#1 N-ID#" & NewsID & " is not valid"
end if
set rs = sqlconnection.execute("usp_sel_forthcoming_events " & NewsID)
if rs.eof then
  response.redirect "/news-events/default.asp?ThisErr=Err#2 eof for " & NewsID
end if
'______________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = rs("Title") & " " & FormatDateTimeIRL(rs("StartDate"),1) & "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Latest News"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & CreateNewsFriendyURL(rs("EventID"),rs("StartDate"),rs("Title"))
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = rs("Title") & " | Eye Doctors "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "news"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(3,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Latest News &amp; Events"
BreadCrumbArr (1,1) = "/news-events/default.asp"
BreadCrumbArr (2,0) = replace(FormatDateTimeIRL(rs("StartDate"),2),"/","-")
            if Cdate(FormatDateTimeIRL(rs("StartDate"),2)) <> Cdate(FormatDateTimeIRL(rs("EndDate"),2)) then
                 BreadCrumbArr (2,0) = BreadCrumbArr (2,0) &  " / " & replace(FormatDateTimeIRL(rs("EndDate"),2),"/","-")
            end if


BreadCrumbArr (2,1) = ""
BreadCrumbArr (3,0) = rs("Title")
BreadCrumbArr (3,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr                                              
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
              <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 col-sm-12 maincol">
            <h1><%=rs("Title")%></h1>
            <h2><%
            response.write FormatDateTimeIRL(rs("StartDate"),1)
             if Cdate(FormatDateTimeIRL(rs("StartDate"),2)) <> Cdate(FormatDateTimeIRL(rs("EndDate"),2)) then
                 response.write " - " & FormatDateTimeIRL(rs("EndDate"),1)
            end if
            %></h2>
            <div><%=rs("Description")%></div>
          </div>
        <!-- Side Column -->
 
        <div class="col-md-3 col-sm-12 sidecol">
            <%
              if NewsID=669 then
            %>
              <dl>
            <dt><a class="btn" href="https://ico.wildapricot.org/event-5770748"  target="_blank">Register Now</a></dt></dl>
            <%
              end if
            %>


            <!--#include file="news-events-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->

        </div> <!--{end}  sidecol -->


    
      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>


