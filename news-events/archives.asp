﻿
<%
Option Explicit
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists- News Archives"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/news-events/archives.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "News Archives | Eye Doctors "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "news"
' include at the top '
dim mypage, maxpages
dim rec         : rec = 0
dim maxdisplay  : maxdisplay = 10
mypage = request.querystring("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
  mypage=1
end if
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(3,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Events"
BreadCrumbArr (1,1) = "/news-events/"
BreadCrumbArr (2,0) = "Archive"
BreadCrumbArr (2,1) = "/news-events/archives.asp"
BreadCrumbArr (3,0) = "Page " & mypage
BreadCrumbArr (3,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'




'get the latest PRs'
dim nws
set nws = Server.CreateObject("ADODB.Recordset")
nws.CursorLocation = 3
nws.cachesize = maxdisplay
nws.Open  "exec usp_sel_past_events " , sqlConnection

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">
          <h1>Events Archive</h1>
          <!--#include file="news-intro.html" -->
		  <%
            if NOT nws.eof THEN
              Dim PrURL 
              rec = 1
              nws.movefirst
              nws.pagesize = maxdisplay
              nws.absolutepage = mypage
              maxpages = cint(nws.pagecount)
    
              response.Write "<ul class=""events-list"">" & VbNewline
              do until nws.eof or (rec > maxdisplay)
                   PrURL= CreateNewsArchiveFriendyURL(nws("EventID"),FormatUSDateTime(nws("StartDate")),nws("Title"))
                response.Write  "<li>" & VbNewline & _
                "<em>"& FormatPRDateTime(nws("StartDate"))
                if datediff("d",nws("StartDate"),nws("EndDate")) > 0 then
                  response.write " - " &  FormatPRDateTime(nws("EndDate"))
                end if
    
                response.Write "</em>" & VbNewline & _
                "<br><strong><a href=""" & PrURL & """ title="""& DisplayText(nws("Title")) &""">" & DisplayText(nws("Title")) & "</a></strong>" & VbNewline & _
                "<br>"
                response.write TrimString(RemoveAllHTMLTags(RemoveAllPs(Displaytext(nws("Description")))), 250,PrURL)
                response.Write  "</li>" & VbNewline 
                rec = rec  + 1
              nws.movenext
              loop
            response.Write "</ul>" & vbnewline
    
          else
            response.Write "<p >There are no archives available</p>"
          end if'pr eof
          set nws = nothing
          if maxpages > 1 then
            call Paging
          else
            response.write "&nbsp;"
          end if
          %>

        </div> <!--{end} maincol -->
      
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="news-events-Menu.asp" -->
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->

        </div> <!--{end}  sidecol -->


      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
  if maxpages > 1 then
    dim pge : pge = MyPage
    dim counter
    Response.Write "<ul class=""pagination"">"
    for counter = 1 to maxpages
      if counter <> cint(pge) then
        Response.Write "<li><a href=""archives.asp?mypage=" & counter & """>" & counter & "</a></li>"
      else
        Response.Write "<li class=""active""><span>" & counter & " <span class=""sr-only"">(current)</span></span></li>"
      end if
      ' If counter < maxpages then
      '   Response.write  " | "
      ' end if
    next
    Response.Write "<li class=""paging-btn""><a href=""/news-events/"" class=""btn"">Upcoming Events</a></li>"
    Response.write "</ul>"
  end if
end sub
%>
</body>
</html>