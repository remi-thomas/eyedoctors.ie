﻿
<%
Option Explicit
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" List-eye-doctors-by-county.asp"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "/news-events/default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "List-eye-doctors-by-county.asp &amp; Events | "& FormatUSDateTime(Now()) &" | Eye Doctors "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "news"
' include at the top '
dim mypage, maxpages
dim rec         : rec = 0
dim maxdisplay  : maxdisplay = 25
mypage = request.querystring("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
  mypage=1
end if
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Latest Events"
BreadCrumbArr (1,1) = "/news-events/default.asp"
BreadCrumbArr (2,0) = "Page " & mypage
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'




'get the latest PRs'
dim nws
set nws = Server.CreateObject("ADODB.Recordset")
nws.CursorLocation = 3
nws.cachesize = maxdisplay
nws.Open  "exec usp_sel_forthcoming_events NULL" , sqlConnection

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">
          <h1>Upcoming Events</h1>
          <!--#include file="news-intro.html" -->
		  <%
            if NOT nws.eof THEN
              Dim PrURL 
              rec = 1
              nws.movefirst
              nws.pagesize = maxdisplay
              nws.absolutepage = mypage
              maxpages = cint(nws.pagecount)
    
              response.Write "<ul class=""events-list"">" & VbNewline
              do until nws.eof or (rec > maxdisplay)
                   PrURL= CreateNewsFriendyURL(nws("EventID"),FormatUSDateTime(nws("StartDate")),nws("Title"))
                response.Write  "<li>" & VbNewline & _
                "<em>"& FormatPRDateTime(nws("StartDate"))
                if datediff("d",nws("StartDate"),nws("EndDate")) > 0 then
                  response.write " - " &  FormatPRDateTime(nws("EndDate"))
                end if
                response.Write "</em><br>" & VbNewline & _
                "<strong><a href=""" & PrURL & """ title="""& DisplayText(nws("Title")) &""">" & DisplayText(nws("Title")) & "</a></strong><br>" & VbNewline & _
                ""
                response.write TrimString(RemoveAllHTMLTags(RemoveAllPs(Displaytext(nws("Description")))), 150,PrURL)
                response.Write  "</li>" & VbNewline 
                rec = rec  + 1
              nws.movenext
              loop
            response.Write "</ul>" & vbnewline
    
          else
            response.Write "<p>There are no current events available.</p>"
          end if'pr eof
          set nws = nothing
         
          call Paging
          %>

        </div> <!--{end} maincol -->
      
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include file="news-events-Menu.asp" -->
              <!-- Side: Content Box -->
              <!--#include virtual="/ssi/incl/adverts.asp" -->
              <!--{end}  box -->

        </div> <!--{end}  sidecol -->

      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
<%
sub Paging()
  if maxpages > 1 then
    dim pge : pge = MyPage
    dim counter
    Response.Write "<ul class=""pagination"">"
    for counter = 1 to maxpages
      if counter <> cint(pge) then
        Response.Write "<li><a href=""default.asp?mypage=" & counter & """>" & counter & "</a></li>"
      else
        Response.Write "<li class=""active""><span>" & counter & " <span class=""sr-only"">(current)</span></span></li>"
      end if
      ' If counter < maxpages then
      '   Response.write  " | "
      ' end if
    next
    Response.Write "<li class=""paging-btn""><a href=""/news-events/archives.asp"" class=""btn"">Events Archive</a></li>"
    Response.write "</ul>"
  else 
    Response.Write "<ul class=""pagination"">"
    Response.Write "<li class=""paging-btn""><a href=""/news-events/archives.asp"" class=""btn"">Events Archive</a></li>"
    Response.write "</ul>"
  end if
end sub
%>

</body>
</html>