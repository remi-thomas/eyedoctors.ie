<!-- Side: Menu -->
          <dl>
            <dt><a href="/news-events/">News &amp; Events</a></dt>
            <dd>
              <ul>
             	<%
             	if Now() < Cdate("5 October 2022 23:00") then
             	%>
     				<li <%if SiteSubSection="EyeCareinFocus" then response.write " class=""active"""%>><a href="/eyecareinfocus/" title="Events">Eye Care In Focus - 4 Oct. 22</a></li>
     				<% end if %>


              <li <%if SiteSubSection="pr" then response.write " class=""active"""%>><a href="/press-releases/" title="Press Releases">Press Releases</a></li>
              <li <%if SiteSubSection="news" then response.write " class=""active"""%>><a href="/news-events/" title="Events">Events</a></li>

              </ul>
            </dd>
          </dl>


          <%
          if Now() < Cdate("5 October 2022 23:00") then
          %>
          <p align="center">
            <a href="/EyeCareInFocus/"><img src="/EyeCareInFocus/ICO_Shared Learnings_pullup.jpg" title="Eye Care in Focus - Shared Learning" alt="Shared Learning logo" class="img-responsive"/></a>
          </p>

          <%
          end if
          %>