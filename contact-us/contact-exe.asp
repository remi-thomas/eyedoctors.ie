<!--#include virtual="/ssi/fct/mailer.asp"-->
<!--#include virtual="/ssi/fct/mail_template.asp"-->
<!--#include virtual="/ssi/fct/aspJSON1.17.asp" -->

<%
	RecipEmail =  "info@eyedoctors.ie"


	name		=left(Request.Form("name"),500)
	email		=left(Request.Form("email"),300)
	phone		=left(Request.Form("phone"),500)
	message		=left(Request.Form("message"),1000)
	recaptcha_response =left(Request.Form("recaptcha_response"),1000)
	

	dim recaptcha_url : recaptcha_url = "https://www.google.com/recaptcha/api/siteverify"
    dim recaptcha_secret : recaptcha_secret = "6LdWjfAUAAAAAOK_pJEAMsJMvmUCMWFYBa8T_20I"
   	recaptcha_url = recaptcha_url & "?secret=" & recaptcha_secret & "&response=" & recaptcha_response
   	'response.write "recaptcha_url " & recaptcha_url & "<br/>"
   	Dim CaptchaResults
   	CaptchaResults = GetCaptchaJsonResults(recaptcha_url)



Set oJSON = New aspJSON
oJSON.loadJSON(CaptchaResults)
'Response.Write " success " & oJSON.data("success") & "<br>"


if oJSON.data("success") then

	If Len(name) < 2 Or Len(email) < 2 Then
		response.redirect "default.asp?ThisErr=Name or email not valid"
	End if


	Subject = name & " (" & email & ") has sent comments on eyedoctors.ie website."

	BodyText = HTMLTop & vbnewline & _
	"Comments From: <br/>"
	BodyText=BodyText & name & "<br/>"
	BodyText=BodyText & email & "<br/>"
	BodyText=BodyText & phone & "<br/>"
	BodyText=BodyText & message & "<br/>" & vbnewline & _
	HTMLBottom

	'response.write BodyText
	mail = SendMail("no-reply@eyedoctors.ie", RecipEmail, Subject,  BodyText)

end if
response.redirect "thankyou.asp"

function GetCaptchaJsonResults(byval recaptcha_url)

	dim xmlhttp 
	set xmlhttp = server.Createobject("MSXML2.ServerXMLHTTP")
	xmlhttp.Open "POST",recaptcha_url,false
	xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	xmlhttp.send
	Response.ContentType = "text/xml"
	GetCaptchaJsonResults = xmlhttp.responseText
	Set xmlhttp = nothing
end function

%>
