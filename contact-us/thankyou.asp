<%
Option Explicit
%><!--#include virtual="/ssi/dbconnect.asp" --><%
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Press Releasese"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "press  releases | Eye Doctors "
SiteSection       = "NewsEvents" ' highlight correct menu tab
SiteSubSection    = "pr"

' include at the top '
dim mypage, maxpages
dim rec         : rec = 0
dim maxdisplay  : maxdisplay = 5
mypage = request.querystring("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
  mypage=1
end if
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "Press Releases"
BreadCrumbArr (1,1) = "/press-releases/default.asp"
BreadCrumbArr (2,0) = "Page " & mypage
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'



'get the latest PRs'
dim pr
set pr = Server.CreateObject("ADODB.Recordset")
pr.CursorLocation = 3
pr.cachesize = maxdisplay
pr.Open  "exec usp_SEL_PR NULL,1" , sqlConnection

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">
          <h1>Contact Us</h1>
          <!-- #include file ="thanks.html" -->
   

        </div> <!--{end} maincol -->
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!-- Side: Content Box -->
            <!--#include virtual="/ssi/incl/adverts.asp" -->
            <!--{end}  box -->

        </div> <!--{end}  sidecol -->

      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
<!--#include virtual="/ssi/dbclose.asp" -->
</body>
</html>