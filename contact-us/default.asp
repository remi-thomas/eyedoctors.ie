<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Press Releasese"
Metadescription   = "The Irish College of Ophthalmologists (ICO) is the recognised training and professional body for medical and surgical eye doctors in Ireland"
ArticleURL        = DomainName & "contact-us/default.asp"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Contact Us | Eye Doctors "
SiteSection       = "Contact" ' highlight correct menu tab
SiteSubSection    = "Contact"

' include at the top '
dim mypage, maxpages
dim rec         : rec = 0
dim maxdisplay  : maxdisplay = 5
mypage = request.querystring("mypage")
if Not IsNumeric(mypage) or mypage ="" Then
  mypage=1
end if
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(0,1)
BreadCrumbArr (0,0) = "Contact Us"
BreadCrumbArr (0,1) = "/contact-us/"

BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'

%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcHykuw8eGO130mbPF3uzfptzeYhJnr34&callback=initMap" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LdWjfAUAAAAAOM2vjbBIJ-zWJie2XxtOHmENG13"></script>

<script type="text/javascript">
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 53.33918, lng:-6.26145};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 15, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
</script>
<script  type="text/javascript">
grecaptcha.ready(function() {
    grecaptcha.execute('6LdWjfAUAAAAAOM2vjbBIJ-zWJie2XxtOHmENG13', {action: 'contact'}).then(function(token) {
    var recaptchaResponse = document.getElementById('recaptchaResponse');
    recaptchaResponse.value = token;
    });
});
</script>





<body id="contact" >

<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
      <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
      <div class="row layout">
        <!-- Main Column -->
        <div class="col-md-9 maincol">


          <h1>Contact Us</h1>

          <div class="row" >
              <h2>Address</h2>
            <div class="col-md-5">
                <div id="address" >
   
                  <p>Irish College of Ophthalmologists<br />
                    CRO 151473<br />
                    VAT IE6548873V<br />
                    121 St Stephen&rsquo;s Green<br />
                    Dublin 2<br />
                    D02 H903<br />
                    Ireland<br />
      			       <a href="https://twitter.com/eyedoctorsIRL" target="twit"><img src="/images/twitter.png" height="12">@eyedoctorsirl</a>
                    <br /><br />
                    <abbr title="Telephone Number">Tel</abbr>: <a href="tel:0035314022777" class="phonenumber">+353 (0)1 402 2777</a>
                    <br />
                    Email: <a href="mailto:info@eyedoctors.ie">info@eyedoctors.ie</a></p>
                  </div>
              </div>
              <div class="col-md-7">
                <div id="map" style="width:360px; height:190px"></div>
              </div>
            </div>

          <div class="row" >

          <h2>Get in touch with us</h2>
        		  <form name="contact" action="contact-exe.asp" method ="post" />
                  <div id="contactform">
                    <div>
                      <label for="name" class="col-md-2 control-label">Name:</label>
                      <input name="name" type="text" id="name" size="57"  placeholder="Your Name"/>
                    </div>
                    <div>
                      <label for="email" class="col-md-2 control-label" >Email:</label>
                      <input name="email" type="text" id="email" size="57" placeholder="Your Email"/>
                    </div>
                    <div>
                      <label for="phone" class="col-md-2 control-label" >Phone:</label>
                      <input name="phone" type="text" id="phone" size="57" placeholder="Your Phone (+353) 1 402 2777"/>
                    </div>
                    <div>
                      <label for="message" class="col-md-2  control-label">Message:</label>
                      <textarea name="message" rows="6" cols="55" id="message"></textarea>
                    </div>
                    <div>
                        <p class="col-md-2"><input type="hidden" name="recaptcha_response" id="recaptchaResponse" /></p>
                        <p class="col-md-4">
                          <input type="submit" name="submit" value="Send Message" class=" btn btn-default" />
                        </p>
                    </div>
                 </div>
        			</form>
          </div>

        </div> <!--{end} maincol -->
        <!-- Side Column -->
        <div class="col-md-3 sidecol">
          <!-- Side: Content Box -->
          <!--#include virtual="/ssi/incl/adverts.asp" -->
          <!--{end}  box -->

        </div> <!--{end}  sidecol -->

      </div> <!--{end}  layout -->
    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->
