
<%
Option Explicit
%><!--#include virtual="/ssi/fct/common.asp" --><%
%><!--#include virtual="/ssi/fct/translator.asp" --><%
'___________________________________________________________________
'| Variables below for meta data                                       |
'______________________________________________________________________|


MetaAbstract      = "Eye Doctors: Irish College of Ophthalmologists-"& year(Now()) &" Eye Care in Focus"
Metadescription   = "Eye Care in Focus, Irish College of Ophthalmologists, October " & year(Now())
ArticleURL        = DomainName & "EyeCareinFocus"
ImgSocialMediaURL = DomainName &  "css/img/eye-doctors.png"
PageTitle         = "Eye Care in Focus | Conference | 1 October 2024 | Irish College of Ophthalmologists"
SiteSection       = "Conference" ' highlight correct menu tab'
SiteSubSection    = "EyeCareinFocus"
'______________________________________________________________________|'
'| Breadcrumbs                                                         |
'______________________________________________________________________|
ReDim BreadCrumbArr(2,1)
BreadCrumbArr (0,0) = "News &amp; Events"
BreadCrumbArr (0,1) = "/news-events/"
BreadCrumbArr (1,0) = "ICO Conference"
BreadCrumbArr (1,1) = "default.asp"
BreadCrumbArr (2,0) = "Eye Care in Focus - 1 October 2024"
BreadCrumbArr (2,1) = ""
BreadCrumbHTML = WriteBreadCrumb(BreadCrumbArr)
Erase BreadCrumbArr
'|                                                                     |'
'______________________________________________________________________|'
%>
<!DOCTYPE html>
<html lang="en">
<head>
<!--#include virtual="/ssi/incl/metadata.asp" -->
<style>
/*.biog {border-bottom: 1px solid #ddd;}
.biog h3 {color: #999; }
.biogtitles {color: #999; padding:10px;}
.biog img {padding-right:15px;}
*/
.ctitle {color: #004b8d;font-weight:normal;}
.ctitle em {font-weight: 500;color:#ccc;;font-size: 2rem;}

.posters ul {list-style-type: none;}
.posters ul  li:nth-child(odd) { background: i#eee; }

.modal-body h5 {font-weight: 500;color:#999;;font-size: 1.4rem;}
.maincol  h1 {font-size:200%;padding-top:10px;}
.maincol .twitter {color:#004b8d;padding-top:0px;padding-bottom:0px;font-size:105%;margin:0;}
.hotel {font-size:105%;font-weight: bold; color: #999;}
.panel {margin-top:10px;}
@media only screen and (max-width: 600px)
{
  .maincol h1 {font-size:130%;padding-top:0px;}
  .maincol h2 {font-size:105%;}
  .maincol .twitter {font-size:90%;}
  .hotel {font-size:90%;}
  .date {display: none;}
  .panel {margin-top:4px;}
}



</style>
</head>
<body>
<!-- HEADER ================================================== -->
<!--#include virtual="/ssi/incl/header.asp" -->

<!-- CONTENT AREA ============================================ -->
<div class="content">
  <div class="content2">
    <div class="container">
      <!-- Breadcrumb -->
        <% =BreadCrumbHTML %>
      <!-- 2 Column Layout -->
        <div class="row layout">
        <!-- Main Column -->
          <div class="col-md-9 maincol">


      <div style="max-width:950px;min-height:160px;overflow:hidden;padding:0;">
        <div class="col-md-3 col-sm-5 col-xs-4">
          <img src="Shared_Learning.png" title="Shared Learning" alt="Shared Learning logo" class="img-responsive"/>
        </div>

        <div class="col-md-8 col-sm-7 col-xs-8">
          <h1 style="color:#004b8d;">Eye Care In Focus</h1>
          <h2>Tuesday, 1st October 2024</h2>
          <p class="hotel">The Gibson Hotel, Dublin</p>
          <p class="twitter"><a href="https://x.com/hashtag/EyeCareInFocus24" title="#EyeCareInFocus24" target="X" style="color:#004b8d;text-decoration: none; font-weight:bold; "><img src="/images/twitter.png" width="20" title="#EyeCareInFocus23" alt="#EyeCareInFocus24" style="float:left;">&nbsp;#EyeCareInFocus24</a></p>
        </div>

      </div>


   
  <div class="panel panel-default">

    <div style="padding:1.3rem">
      <!--#include file="introduction.html" -->
    </div>

  <%
  if Now() < Cdate("30 September 2024 23:00") then
  %>
  <p style="padding-left:10px;float:left;"><a href="https://ico.wildapricot.org/event-5770748" class="btn" role="button" aria-label="Register for the Eye  Care in Focus today" title="Register for the Eye  Care in Focus today (this link opens a new window)" target="ext">Register Now</a></p>

 <p style="padding-left:10px;float:left;"><a href="ICO_Eyecare_conf_bklet_2024.pdf" class="btn" role="button" aria-label="Download the Programme as a PDF Document" title="Download the Programme as a PDF Document">Download the programme as PDF</a></p>

 <br style="clear:both;">

  <%
  end if
  %>


      <div class="panel-body" style="max-width:950px;overflow:hidden">
    
          <!--#include file="1october.html" -->
 <p style="padding-left:10px;"><a href="ICO_Eyecare_conf_bklet_2024.pdf" class="btn" role="button" aria-label="Download the Programme as a PDF Document" title="Download the Programme as a PDF Document">Download the programme as PDF</a></p>
      </div>




  </div>








    
      </div> <!--{end}  layout -->
    
 <!-- Side Column -->
        <div class="col-md-3 sidecol">
            <!--#include virtual="/news-events/news-events-Menu.asp" -->

        </div> <!--{end}  sidecol -->


    
    </div> <!-- {end}  container -->
  </div> <!-- {end}  content2 -->
</div> <!-- {end}  content -->

<!-- FOOTER ================================================== -->
<!--#include virtual="/ssi/incl/footer.asp" -->




<div class="modal fade" id="biography" tabindex="-1" role="dialog" aria-labelledby="biographyLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="biographyModalLongTitle">r</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>
$('a[data-toggle="modal"]').click(function(){    
    
    var vtitle = $(this).attr('title');
    console.log(vtitle);
    var vcontent = "";
    var dataURL = '/biographies/' + $(this).attr('data-href') + '.html';
    //console.log(dataURL);
    sendInfo(dataURL);

    $('#biography .modal-title').html(vtitle);

    //$('#biography .hide').show();
   //$('#biography').modal();
  
    /*$('#biography').on('hidden', function(){
    console.log("hidden");
      $('#biography').remove();
  });
  */
});


var request;

function sendInfo(url) {

  if (window.XMLHttpRequest) {
  request = new XMLHttpRequest();
  }
  else if (window.ActiveXObject) {
  request = new ActiveXObject("Microsoft.XMLHTTP");
  }

  try {
  request.onreadystatechange = getInfo;
  request.open("GET", url, true);
  request.send();
  }
  catch (e) {
  console.log("Unable to connect to server");
  }
}

function getInfo() {
  if (request.readyState == 4) 
  {
    var vcontent = request.responseText;
  }
  else
  {
    var vcontent = "error";
  }
  //console.log(vcontent);
  $('#biography .modal-body').html(vcontent);
}


</script>
</body>
</html>