<!-- Side: Menu -->
          <dl>
            <dt><a href="/Health-Policy/">Health Policy</a></dt>
            <dd>
              <ul>
             <li <%if SiteSubSection="PrimaryCare" then response.write " class=""active"""%>><a href="/Health-Policy/HSE-Primary-Care-Eye-Services-Report.asp"> HSE Primary Care Eye Services Report.</a></li>

              <li <%if SiteSubSection="MedicalFitness" then response.write " class=""active"""%>><a href="/Health-Policy/medical-fitness-to-drive.asp">Medical Fitness to Drive Guidelines</a></li>

              
              <li class="subsubmenu">
                  <a href="#"><b>Clinical Programme</b></a>
                  <ul>
                        <Li <%if SiteSubSection="clinicalprogramme" then response.write " class=""active"""%>><a href="/Health-Policy/clinical-programme.asp">Model of Care</a></li>
                       <Li<%if SiteSubSection="integratedeyecare" then response.write " class=""active"""%>><a href="/Health-Policy/Education-Series-for-the-Integrated-Eye-Care-Team.asp">National Education Series for the Integrated Eye Care Team</a></li>
                  </ul>
              </li>

              <li><a href="/press-release/February-8-2015/ICO-Publish-Guidelines-for-Refractive-Eye-Surgery/26.html">Refractive Surgery Guideline</a></li>

              <li <%if SiteSubSection="CostofEyeDiseases" then response.write " class=""active"""%>><a href="/Health-Policy/Economic-cost-of-Eye-Diseases.asp">The Economic Cost of Eye Diseases</a></li>
              <li <%if SiteSubSection="TobaccoControl" then response.write " class=""active"""%>><a href="/Health-Policy/Tobacco-Control.asp">Tobacco Control</a></li>
              <li <%if SiteSubSection="AdvertisingStandards" then response.write " class=""active"""%>><a href="/Health-Policy/Medical-Advertising-Standards.asp">Direct to Patient Advertising</a></li>
               <!-- li <%if SiteSubSection="laser" then response.write " class=""active"""%> ><a href="/Health-Policy/Refractive-Surgery-Guidelines.asp">Refractive Surgery Guidelines</a></li -->

              </ul>
            </dd>
          </dl>
